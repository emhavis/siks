console.log("okok");

function MyForm(settings)
{
  this.is_edit = false;
  this.is_edit_first_load = false;
  
  this.formSettings = settings;
  if(this.formSettings.is_edit=="true")
  {
    this.is_edit = true;
    this.is_edit_first_load = true;
  }

  this.formObject = $("#"+this.formSettings.id);
  this.formValues = new Array();
  this.messageErrors = new Array();
  this.elementClasses = new Object();
  this.elementTypes = new Object();
  this.elementNames = new Object();

  this.parsingFormElement = function()
  {
    var select2 = new Array();
    var multiselect2 = new Array();
    var ckeditor = new Array();
    var datepicker = new Array();
    var uploadimage = new Array();
    var uploadfile = new Array();
    var typeInputs = new Array();
    var typeTextareas = new Array();
    var typeSelects = new Array();
    var typeFiles = new Array();
    var elementNames = new Array();

    $("input", this.formObject).each(function(k,element)
    {
      var id = $(element).attr("id");
      elementNames[id] = element;
      var type = $(element).attr("type");
      if(type)
      {
        switch(type)
        {
          case "text": typeInputs.push(element); break;
          case "file": typeFiles.push(element); break;
        }
      }
    });

    $("select", this.formObject).each(function(k,element)
    {
      var id = $(element).attr("id");
      elementNames[id] = element;
      typeSelects.push(element);
    });

    $("textarea", this.formObject).each(function(k,element)
    {
      var id = $(element).attr("id");
      elementNames[id] = element;
      typeTextareas.push(element);
    });

    $("input, select, textarea", this.formObject).each(function(k,element)
    {
      var kelases = $(element).attr("class");

      if(kelases)
      {
        kelases = kelases.split(" ");

        $.each(kelases,function(i,kelas){
          switch(kelas)
          {
            case "select2": select2.push(element); break;
            case "multiselect2": multiselect2.push(element); break;
            case "ckeditor": ckeditor.push(element); break;
            case "datepicker": datepicker.push(element); break;
            case "uploadimage": uploadimage.push(element); break;
            case "uploadfile": uploadfile.push(element); break;
            case "numeric": 
              $(element).inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'});
              break;
          }
        });
      }
    });

    this.elementClasses["select2"] = select2;
    this.elementClasses["multiselect2"] = multiselect2;
    this.elementClasses["ckeditor"] = ckeditor;
    this.elementClasses["datepicker"] = datepicker;
    this.elementClasses["uploadfile"] = uploadfile;
    this.elementClasses["uploadimage"] = uploadimage;
    this.elementTypes["input"] = typeInputs;
    this.elementTypes["file"] = typeFiles;
    this.elementTypes["select"] = typeSelects;
    this.elementTypes["textarea"] = typeTextareas;
    this.elementNames = elementNames;
  }

  this.getElementNames = function()
  {
    return this.elementNames;
  }

  this.setFormValues = function(param)
  {
    this.formValues = JSON.parse(param);
  }

  this.setFormFormat = function()
  {
    $.each(this.elementClasses["select2"],function(i,element)
    {
      $(element).select2();
    });

    $.each(this.elementClasses["multiselect2"],function(i,element)
    {
      $(element).select2();
    });

    $.each(this.elementClasses["datepicker"],function(i,element)
    {
      $(element).datepicker({autoClose:true,dateFormat:'yy-mm-dd'});
    });

    $.each(this.elementClasses["ckeditor"],function(i,element)
    {
      $(element).CKEDITOR();
      var elementName = $(element).attr("name");
      CKEDITOR.replace( elementName, {
          height : 200,
          extraPlugins : 'uploadimage',
          uploadUrl : "ticket/uploadImage?command=QuickUpload&type=Drop&responseType=json",
          // filebrowserBrowseUrl: "<?php echo base_url('public/files/request'); ?>?type=Files",
          filebrowserUploadUrl: "ticket/uploadFile?type=File",
          filebrowserImageUploadUrl: "ticket/uploadImage?command=QuickUpload&type=Image"
      });
    });

    // $.each(this.elementClasses["ckeditor"],function(i,element)
    // {
    //   $(element).attr(index).select2("val", this.formValues[index]);
    // });
  }

  this.setElementValueWithData = function(data)
  {
    var elementNames = this.elementNames;
    $.each(data,function(key,value)
    {
      $(elementNames[key]).val(value);
    });
  };

  this.setElementValues = function()
  {
    var formValues = this.formValues;
    $.each(this.elementClasses["select2"],function(i,element)
    {
      var elementName = $(element).attr("name");
      $(element).attr("id",elementName).select2("val", formValues[elementName]);
    });

    $.each(this.elementClasses["multiselect2"],function(i,element)
    {
      var elementName = $(element).attr("name");
      $(element).attr("id",elementName).select2("val", JSON.parse("["+formValues[elementName]+"]"));
    });

    $.each(this.elementClasses["ckeditor"],function(i,element)
    {
      var elementName = $(element).attr("name");
      CKEDITOR.instances[elementName].setData(formValues[elementName]);
    });

    $.each(this.elementTypes["textarea"],function(i,element)
    {
      var elementName = $(element).attr("name");
      $(element).attr("id",elementName).text(formValues[elementName]);
    });

    $.each(this.elementTypes["input"],function(i,element)
    {
      var elementName = $(element).attr("name");
      $(element).attr("id",elementName).val(formValues[elementName]);
    });
  }

  this.showFormError = function()
  {
    // this.messageErrors = param;
    var messages = this.messageErrors[1];

    $(".alert-error", this.formObject).remove();
    $.each(this.elementTypes["input"],function(i,element)
    {
      var elementName = $(element).attr("name");
      if(messages[elementName])
      {
        $(element).parent().append('<label class="alert-error">'+messages[elementName]+"</label>");
      }
    });

    $.each(this.elementTypes["select"],function(i,element)
    {
      var elementName = $(element).attr("name");
      if(messages[elementName])
      {
        $(element).parent().append('<label class="alert-error">'+messages[elementName]+"</label>");
      }
    });

    $.each(this.elementTypes["textarea"],function(i,element)
    {
      var elementName = $(element).attr("name");
      if(messages[elementName])
      {
        $(element).parent().append('<label class="alert-error">'+messages[elementName]+"</label>");
      }
    });

    this.showFormAlert();
  }

  this.showFormAlert = function()
  {
    var alert = new Object();
    alert.confirmButtonText = "OK";
    alert.timer = 10000;
    alert.timerProgressBar = true;

    if(this.messageErrors[0]==false)
    {
      alert.html = "Mohon periksa kembali!";
      alert.title = "Error";
      alert.icon = "error";
    }
    else
    {
      alert.html = "Data berhasil disimpan!";
      alert.title = "Sukses";
      alert.icon = "success";
    }

    $.each(this.messageErrors[1],function(i,v)
    {
      switch(i)
      {
        case "alert-danger":
          alert.html = v;
          break;
        case "alert-success":
          alert.html = v;
          break;
      }
    });

    Swal.fire(alert);
  }

  this.setMessageErrors = function(param)
  {
    this.messageErrors = param;
  }

  this.getIsEditFirstLoad = function()
  {
    return this.is_edit_first_load;
  }

  this.setIsEditFirstLoad = function(status)
  {
    this.is_edit_first_load = status;
  }

  this.getIsEdit = function()
  {
    return this.is_edit;
  }
}

var setter = false;
function after_change_qty_harga_satuan(changes, source)
{
  // if( arguments[1] != "loadData" )
  // {
    if(!changes)
    {
      return;
    }

    if (!setter) {
      var qty = init.hot01.getDataAtCell(changes[0][0],3);
      mysum.setQty(changes[0][0],qty);
      var harga = init.hot01.getDataAtCell(changes[0][0],5);
      mysum.setPrice(changes[0][0],harga);
      var subtotal = qty*harga;
      mysum.countSubtotal();
      mysum.setToElement();
      setter = true;
      init.hot01.setDataAtCell(changes[0][0],6,(qty*harga));
    } else {
      setter = false;
    }
  // }
}

function cara_bayar_change_expdate(obj)
{
  var trxdate = $("#trxdate").val();
  var date = new Date(trxdate);
  var cara_bayar = $(obj).val();
  switch(cara_bayar)
  {
    case "1": adddays=0; break;
    case "2": adddays=30; break;
    case "3": adddays=60; break;
    case "4": adddays=90; break;
  }
  date.setDate(date.getDate() + adddays);

  var yyyy = date.getFullYear();
  var mm = date.getMonth() + 1; //January is 0!
  var dd = date.getDate();

  if (dd < 10)
  {
    dd = '0' + dd;
  } 
  if (mm < 10)
  {
    mm = '0' + mm;
  }

  var expdate = yyyy+"-"+mm+"-"+dd;

  $("#expdate").val(expdate);
};

function MySummary()
{
  this.qty = new Array();
  this.price = new Array();
  this.tax = 0; //dalam persen
  this.taxrp = 0;
  this.total = 0;
  this.subtotal = 0;
  this.cost_shipment = 0;
  this.cost_diskon = 0;

  this.recountSubtotal = function()
  {
    this.qty = init.hot01.getDataAtCol(3);
    this.price = init.hot01.getDataAtCol(5);
    this.countSubtotal();
    this.setToElement();
  }

  this.setQty = function(i,qty)
  {
    this.qty[i] = qty;
  }

  this.setPrice = function(i,price)
  {
    this.price[i] = price;
  }

  this.countSubtotal = function()
  {
    this.subtotal = 0;
    this.total = 0;
    for(var i=0; i<this.qty.length; i++)
    {
      this.subtotal += this.qty[i]*this.price[i];
    }

    var elements = myform.getElementNames();
    if(elements["cost_shipment"])
    {
      var cost = $(elements["cost_shipment"]).val();
      this.cost_shipment = parseFloat(cost.replace(/,/g,""));
    } 
    if(elements["cost_diskon"])
    {
      var cost = $(elements["cost_diskon"]).val();
      this.cost_diskon = parseFloat(cost.replace(/,/g,""));
    }
    this.taxrp = (this.tax/100)*this.subtotal;
    this.total = (this.taxrp+this.cost_shipment-this.cost_diskon)+this.subtotal;
  }

  this.setToElement = function()
  {
    $("span#subtotal").text(this.formatMoney(this.subtotal));
    $("span#taxrp").text(this.formatMoney(this.taxrp));
    $("span#total").text(this.formatMoney(this.total));
  }

  this.setTax = function(tax)
  {
    this.tax = tax;
  }

  this.formatMoney = function(num)
  {
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }
}
