@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')
<style>
</style>
@endsection

@section('content')

<div class="row">
    {!! Form::open(array('id' => 'form_confirm_payment')) !!}
        
        @if ($serviceRequest->lokasi_pengujian == 'luar' && $serviceRequest->spuh_inv_price > 0
            && $doc->invoiced_price > 0)
        <div class="panel panel-filled" id="panel_create">
            <div class="loader">
                <div class="loader-bar"></div>
            </div>      
            <div class="panel-heading" >
                <h4>Informasi Billing SPUH</h4>
            </div>
            <div class="panel-body">
                <!--
                <div class="form-group">
                    <label for="payment_date">Tanggal Pembayaran</label> 
                    <input id="payment_date" name="payment_date" class="form-control" type="text" autocomplete="off" placeholder="Tanggal Pembayaran" required value="<?php echo date("Y-m-d")?>">
                </div>
                -->
                <div class="form-group">
                    <label for="spuh_price">Tagihan SPUH</label> 
                    <input id="spuh_price" name="spuh_price" class="form-control" type="text" 
                        value="{{ number_format($serviceRequest->spuh_add_price > 0 ? $serviceRequest->spuh_add_price : $serviceRequest->spuh_inv_price,'2',',','.') }}"
+                       placeholder="Tagihan SPUH" autocomplete="off" readonly>
                </div>
                <div class="form-group">
                    <label for="spuh_no">No SPUH</label> 
                    <input id="spuh_no" name="spuh_no" class="form-control" type="text" 
                        value="{{ $serviceRequest->spuh_no }}"
                        placeholder="Tagihan SPUH" autocomplete="off" readonly>
                </div>
                
            </div>
        </div>
        @endif

        <div class="text-right">
            {!! Form::submit('Simpan', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_simpan', 'name' => 'submitbutton', 'value' => 'save']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){

    $('input#payment_date, #billing_to_date').datepicker({
        format:"dd-mm-yyyy",
        autoclose:true
    });

    $("#btn_simpan").click(function(e){
        e.preventDefault();

        $("#panel_create").toggleClass("ld-loading");

        var form_data = $("#form_confirm_payment").serialize();
        form_data += "&id={{ $id }}";
        $.post('{{route('requestluaruut.paymenttuhpsave')}}',form_data,function(res)
        {
            if(res[0]==false)
            {
                var msg = show_notice(res[1]);
                $("#panel_create").toggleClass("ld-loading");
            }
            else
            {
                window.location = '{{ route('requestluaruut') }}';
            }
        },"json");
    });
});

function show_notice(msg)
{
    var errmessage = {
        'payment_date':'Tanggal input wajib diisi',
        'payment_code':'Nomor pembayarn(struk bayar) wajib diisi'
    };

    $(document).find("small.wajib-isi").remove();
            var str = '<div class="panel panel-filled panel-c-danger">'+
        '<div class="panel-body">'+
            '<b>Terjadi kesalahan.</b>'+
            'Telah terjadi kesalahan, hubungi administrator untuk info lebih lanjut.'+
            '<ul>';
            $.each(msg,function(i,v){
                str += '<li>'+errmessage[i]+'</li>';
                $(document).find("label[for='"+i+"']").append("<small class='text-warning wajib-isi'> wajib diisi</small>");
            });
    str += '</ul>'+
        '</div>'+
    '</div>';

    return str;
}
</script>
@endsection