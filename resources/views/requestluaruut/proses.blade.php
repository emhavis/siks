@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Dinas Luar</h4>
            </div>
            <div class="panel-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="label_sertifikat">Nama Pemilik</label>
                            {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Pemilik</label>
                            {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>  
                </div>
            </div>
        </div>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pemohon</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Nama Pemohon</label>
                            {!! Form::text('pic_name', $request->requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Jenis Tanda Pengenal</label>
                            {!! Form::text('id_type_id', $request->requestor->kantor == "Perusahaan" ? "NIB" : "NPWP", ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                            {!! Form::text('pic_id_no', $request->requestor->kantor == "Perusahaan" ? $request->requestor->nib : $request->requestor->npwp, ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_phone_no">Nomor Telepon</label>
                            {!! Form::text('pic_phone_no', $request->requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_email">Alamat Email</label>
                            {!! Form::text('pic_email', $request->requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_from">Usulan Jadwal Mulai</label>
                            {!! Form::text('scheduled_test_date_from', date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_from', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_to">Usulan Jadwal Selesai</label>
                            {!! Form::text('scheduled_test_date_to', date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_to', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lokasi_pengujian">Lokasi Pengujian</label>
                            {!! Form::text('lokasi_pengujian', $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' , ['class' => 'form-control','id' => 'lokasi_pengujian', 'readonly']) !!}
                             
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Item Pengujian</h4>
            </div>

            <div class="panel-body">

                @foreach($request->items as $item)
                <div class="panel panel-filled panel-c-danger">
                    <div class="panel-body">
                        <form id="form_item_{{ $item->id }}">
                        {!! Form::hidden('id', $item->id) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jenis</label>
                                    {!! Form::text('serial_no', $item->uuts->stdtype->uut_type, ['class' => 'form-control','id' => 'serial_no', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Merek</label>
                                    {!! Form::text('tool_brand', $item->uuts->tool_brand, ['class' => 'form-control','id' => 'tool_brand', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Model/Tipe</label>
                                    {!! Form::text('tool_model', $item->uuts->tool_model, ['class' => 'form-control','id' => 'tool_model', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nomor Seri</label>
                                    {!! Form::text('serial_no', $item->uuts->serial_no, ['class' => 'form-control','id' => 'serial_no', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Media Uji/Komoditas</label>
                                    {!! Form::text('tool_media', $item->uuts->tool_media, ['class' => 'form-control','id' => 'tool_media', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Kapasitas Maksimum</label>
                                    {!! Form::text('tool_capacity', $item->uuts->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity', 'readonly' ]) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Kapasitas Daya Baca</label>
                                    {!! Form::text('tool_dayabaca', $item->uuts->tool_dayabaca, ['class' => 'form-control','id' => 'tool_dayabaca', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Satuan Kapasitas</label>
                                    {!! Form::text('tool_capacity_unit', $item->uuts->tool_capacity_unit, ['class' => 'form-control','id' => 'tool_capacity_unit', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Buatan</label>
                                    {!! Form::text('tool_made_in', $item->uuts->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pabrikan</label>
                                    {!! Form::text('tool_factory', $item->uuts->tool_factory, ['class' => 'form-control','id' => 'tool_factory', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Alamat Pabrikan</label>
                                    {!! Form::textarea('tool_factory_address', $item->uuts->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>

                        @if($request->lokasi_pengujian == 'luar' && $item->location != null)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Lokasi Pengujian</label>
                                    {!! Form::textarea('location', $item->location, ['class' => 'form-control','id' => 'location', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                                    {!! Form::text('location_kabkot_id', $item->ServiceRequest->inspectionKabkot->nama . ', ' . $item->ServiceRequest->inspectionProv->nama, ['class' => 'form-control','id' => 'location', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        @if(count($item->perlengkapans) > 0)
                        
                        @endif
                        @endif
                       
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Jumlah Pengujian</label>
                                    {!! Form::text('quantity', $item->quantity, ['class' => 'form-control','id' => 'quantity', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        </form>

                        <div class="row">
                            <div class="col-md-12">
                                <table id="standard_item_inspeksi" class="table table-responsive-sm input-table">
                                    <thead>
                                        <tr>
                                            <th>Jenis Pengujian</th>
                                            <th>Jumlah</th>
                                            <th>Satuan</th>
                                            <th>Harga Satuan</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($item->inspections as $inspection)
                                        <tr>
                                            <td>{{ $inspection->inspectionPrice->inspection_type }}</td>
                                            <td>{{ $inspection->quantity }}</td>
                                            <td>{{ $inspection->inspectionPrice->unit }}</td>
                                            <td>{{ number_format($inspection->price, 2, ',', '.') }}</td>
                                            <td>{{ number_format($inspection->quantity * $inspection->price, 2, ',', '.') }}</td>
                                           
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td>Subtotal: </td>
                                            <td><input readonly type="text" name="subtotal[]" id="subtotal" class="form-control" value="{{ number_format($item->subtotal, 2, ',', '.') }}"/></td>
                                            <td></td>
                                        </tr>                            
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>    
                @endforeach

            </div>
        </div>
        
        {!! Form::open(['url' => route('requestluaruut.saveproses', $request->id), 
            'files' => true, 'id' => 'form_create_request'])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Penugasan dan Penjadwalan</h4>
            </div>
            <div class="panel-body" id="panel_staff">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="spuh_spt">No Surat Tugas</label>
                            {!! Form::text('spuh_spt', $request->spuh_spt,
                                ['class' => 'form-control','id' => 'spuh_spt', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="spuh_file">Surat Tugas</label>
                            {!! Form::file('spuh_file', null,
                                ['class' => 'form-control','id' => 'spuh_file']) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inspection_prov_id">Alamat Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                            {!! Form::text('inspection_prov_id', 
                                $request->inspectionKabkot->nama .', '. $request->inspectionProv->nama,
                                ['class' => 'form-control','id' => 'inspection_prov_id', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="spuh_rate">Uang Harian</label>
                            {!! Form::hidden('spuh_rate_id', $request->spuh_rate_id,
                                ['class' => 'form-control','id' => 'spuh_rate_id', 'readonly']) !!}
                            {!! Form::text('spuh_rate', number_format($request->spuh_rate, 2, ",", "."),
                                ['class' => 'form-control','id' => 'spuh_rate', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="spuh_price">Total Uang Harian</label>
                            {!! Form::text('spuh_price', number_format($request->spuh_price, 2, ",", "." ),
                                ['class' => 'form-control','id' => 'spuh_price', 'readonly']) !!}
                        </div>
                    </div>
                </div>

                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_from">Jadwal Waktu Berangkat</label>
                            {!! Form::text('scheduled_test_date_from', 
                                date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))),
                                ['class' => 'form-control','id' => 'scheduled_test_date_from', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_to">Jadwal Waktu Pulang</label>
                            {!! Form::text('scheduled_test_date_to', 
                                date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))),
                                ['class' => 'form-control','id' => 'scheduled_test_date_to', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <table id="staff" class="table table-responsive-sm input-table">
                            <thead>
                                <tr>
                                    <th>Nama Penguji/Pemeriksa</th>
                                <tr>
                            </thead>
                            <tbody>
                                @foreach($staffes as $staff)
                                <tr>
                                    <td>{{ $staff->scheduledStaff->nama }}
                                    </td>
                                </tr>   
                                @endforeach
                            <tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>  
        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Konfirmasi</button>  
        
        </form>

        
        </div>
    </div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        
        $("#form_create_request").validate({
            rules: {
                
                spuh_file: {
                    required: true,
                },
            },
            messages: {
                spuh_file: 'File surat tugas harus diupload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
      
    });

</script>
@endsection