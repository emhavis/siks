@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">

    <ul class="nav nav-tabs" role="tablist" id="tabs">
        <li role="presentation" class="active">
            <a href="#kn" aria-controls="kn" role="tab" data-toggle="tab" 
                class="badge-notif-tab">Dalam Kantor</a>
        </li>
        <li role="presentation">
            <a href="#dl" aria-controls="dl" role="tab" data-toggle="tab" 
                class="badge-notif-tab">Dinas Luar</a>
        </li>
    </ul>
    
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="kn">
            <br/>  
            <div class="panel panel-filled table-area">
                <div class="panel-body">
                    
                    <table id="data_table_kn" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Tgl Mulai SLA</th>
                                <th>Status SLA (hari)</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_kn as $row)
                            <tr id="{{ $row->id }}">
                                <td>{{ $row->no_order }}</td>
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>{{ $row->lab_staff }}</td>
                                <td>{{ $row->test_by_1 ? $row->test_by_1 : "" }} {{ $row->test_by_2 ? ' & '.$row->test_by_2 : "" }}</td>
                                <td>{{ date('d-m-Y', strtotime($row->staff_entry_datein)) }}</td>
                                <td>
                                    @if($row->cancel_at == null)
                                    {{ date('d-m-Y', strtotime($row->staff_entry_dateout)) }}
                                    @else
                                    BATAL: {{ date('d-m-Y', strtotime($row->cancel_at)) }}
                                    @endif
                                </td>
                                <td>{{ date('d-m-Y', strtotime($row->order_at)) }}</td>
                                <td>
                                    @if($row->sla_status_day == 'GREEN')
                                    <span class="label label-success">
                                    @elseif($row->sla_status_day == 'YELLOW')
                                    <span class="label label-warning">
                                    @else
                                    <span class="label label-danger">
                                    @endif
                                    {{ $row->sla_days }}</span>
                                </td>
                                <td>
                                {{ $row->status }}<br/>
                                    Draft: {{ $row->stat_sertifikat == 3 ? 'Sudah Disetujui Ka Balai' :
                                        ($row->stat_sertifikat == 2 ? 'Sudah Disetujui Ketua Tim' : 'Penyusunan Draft' )}}<br/>
                                    Alat: {{ $row->stat_warehouse == 2 ? 'Sudah Diserahterimakan dengan Pemohon' : 
                                                ($row->stat_warehouse == 1 ? 'Berada di Gudang' : 'Berada di Instalasi')}}
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="dl">
            <br/>  
            <div class="panel panel-filled table-area">
                <div class="panel-body">
                    
                    <table id="data_table_dl" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Tgl Mulai SLA</th>
                                <th>Status SLA (hari)</th>
                                <th>Status</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_dl as $row)
                            <tr id="{{ $row->id }}">
                                <td>{{ $row->no_order }}</td>
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>{{ $row->lab_staff }}</td>
                                <td>{{ $row->test_by_1 ? $row->test_by_1 : "" }} {{ $row->test_by_2 ? ' & '.$row->test_by_2 : "" }}</td>
                                <td>{{ date('d-m-Y', strtotime($row->staff_entry_datein)) }}</td>
                                <td>
                                    @if($row->cancel_at == null)
                                    {{ date('d-m-Y', strtotime($row->staff_entry_dateout)) }}
                                    @else
                                    BATAL: {{ date('d-m-Y', strtotime($row->cancel_at)) }}
                                    @endif
                                </td>
                                <td>{{ date('d-m-Y', strtotime($row->order_at)) }}</td>
                                <td>
                                    @if($row->sla_status_day == 'GREEN')
                                    <span class="label label-success">
                                    @elseif($row->sla_status_day == 'YELLOW')
                                    <span class="label label-warning">
                                    @else
                                    <span class="label label-danger">
                                    @endif
                                    {{ $row->sla_days }}</span>
                                </td>
                                <td>
                                {{ $row->status }}<br/>
                                    Draft: {{ $row->stat_sertifikat == 3 ? 'Sudah Disetujui Ka Balai' :
                                        ($row->stat_sertifikat == 2 ? 'Sudah Disetujui Ketua Tim' : 'Penyusunan Draft' )}}
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    let table = $('#data_table_dl, #data_table_kn').DataTable({
        scrollX: true,
    });

   
});

</script>
@endsection