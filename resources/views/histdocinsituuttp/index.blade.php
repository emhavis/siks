@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<style type"text/css"> 
    .badge-notif-tab {
            position:relative;
    }
    .badge-notif-tab[data-badge]:after {
            content:attr(data-badge);
            position:absolute;
            top:-10px;
            right:-5px;
            font-size:.7em;
            background:#f3a709;
            color:white;
            width:18px;
            height:18px;
            text-align:center;
            line-height:18px;
            border-radius: 50%;
    }
</style>
@endsection

@section('content')
<div class="row">

            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    
                    <table id="table_data_all" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Surat Tugas</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>
                                    {{ $row->doc_no ?? 'N/A' }}
                                </td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor_name ?? 'N/A' }}</td>
                                <td>
                                    {{ $row->status_name ?? 'N/A' }}
                                    @if($row->status_revisi_spt == 1)
                                    <br/>
                                    <div class="alert alert-danger" role="alert">
                                        Perlu revisi SPT
                                    </div>
                                    @elseif($row->status_revisi_spt == 2)
                                    <br/>
                                    <div class="alert alert-warning" role="alert">
                                        Revisi SPT sedang dalam proses persetujuan
                                    </div>
                                    @endif
                                </td>
                                <td>
                                    @if($row->spuh_doc_id != null)
                                    <a href="{{ route('schedulinguttp.surattugas', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Surat Tugas Awal</a>
                                    @endif
                                    <a href="{{ route('histdocinsituuttp.list', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Daftar Surat Tugas</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_all").DataTable();
        
    });
</script>
@endsection