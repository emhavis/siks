@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<a href="{{ route('doctuinsituuttp.list', ['id' => $request->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Daftar Revisi Surat Tugas</a>
<br/><br/>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
     
        {!! Form::open(['url' => route('doctuinsituuttp.store', $request->id), 'files' => true, 'id' => 'form_result'])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Penugasan dan Penjadwalan</h4>
            </div>
            <div class="panel-body" id="panel_staff">
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="doc_no">No Surat Tugas (Revisi)</label>
                            {!! Form::text('doc_no', $request->spuh_spt,
                                ['class' => 'form-control','id' => 'doc_no']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="accepted_date">Tanggal Surat</label>
                            {!! Form::text('accepted_date', 
                                date("d-m-Y", strtotime(isset($request->accepted_date) ? $request->accepted_date : date("Y-m-d"))),
                                ['class' => 'date form-control','id' => 'accepted_date']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="accepted_by_id">Ditandatangani oleh</label>
                            {!! Form::select('accepted_by_id', $kabalais, null, 
                                        ['class' => 'form-control select2', 'id'=>'accepted_by_id']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inspection_prov_id">Alamat Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                            {!! Form::text('inspection_prov_id', 
                                $request->inspectionKabkot->nama .', '. $request->inspectionProv->nama,
                                ['class' => 'form-control','id' => 'inspection_prov_id', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="spuh_rate">Uang Harian</label>
                            {!! Form::hidden('spuh_rate_id', $request->spuh_rate_id,
                                ['class' => 'form-control','id' => 'spuh_rate_id', 'readonly']) !!}
                            {!! Form::text('spuh_rate', $request->spuh_rate,
                                ['class' => 'form-control','id' => 'spuh_rate', 'readonly']) !!}
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date_from">Jadwal Waktu Pengujian, Mulai</label>
                            {!! Form::text('date_from', 
                                date("d-m-Y", strtotime(isset($doc->date_from) ? $doc->date_from : date("Y-m-d"))),
                                ['class' => 'form-control','id' => 'date_from']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date_to">Jadwal Waktu Pengujian, Selesai</label>
                            {!! Form::text('date_to', 
                                date("d-m-Y", strtotime(isset($doc->date_to) ? $doc->date_to : date("Y-m-d"))),
                                ['class' => 'form-control','id' => 'date_to']) !!}
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_id_1">Penguji/Pemeriksa</label>
                            <select class="form-control select2 scheduled_id" name="scheduled_id_1" id="scheduled_id_1">
                                @if(count($staffes) > 0)
                                <option value="{{ $staffes[0]->scheduledStaff->id }}" selected="selected">{{ $staffes[0]->scheduledStaff->nama }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_id_2">Penguji/Pemeriksa</label>
                            <select class="form-control select2 scheduled_id" name="scheduled_id_2" id="scheduled_id_2">
                                @if(count($staffes) > 1 && $staffes[1]->scheduledStaff != null)
                                <option value="{{ $staffes[1]->scheduledStaff->id }}" selected="selected">{{ $staffes[1]->scheduledStaff->nama }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!--
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="file_spt">Surat Tugas *</label>
                            {!! Form::file('file_spt', 
                                ['class' => 'form-control','id' => 'file_spt', 'accept'=> 'application/pdf,image/png, image/jpeg']) !!}
                        </div>
                    </div>
                    -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="file_spuh">SPUH</label>
                            {!! Form::file('file_spuh', 
                                ['class' => 'form-control','id' => 'file_spuh', 'accept'=> 'application/pdf,image/png, image/jpeg']) !!}
                        </div>
                    </div>
                <!--
                </div>
                <div class="row">
                --> 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="file_bukti_bayar">Bukti Bayar</label>
                            {!! Form::file('file_bukti_bayar', 
                                ['class' => 'form-control','id' => 'file_bukti_bayar', 'accept'=> 'application/pdf,image/png, image/jpeg']) !!}
                        </div>
                    </div>
                </div>
                
            </div>
        </div>  

        <button type="button" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button>  
        </form>

        
        
        </div>
    </div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/jquery-validation/additional-methods.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function ()
    {
        $('#date_from, #date_to, #accepted_date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });

        $('#accepted_by_id').select2()

        $('.scheduled_id').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true,
            ajax: {
                url: "{{ route('schedulinguttp.staff') }}",
                data: function (params) {
                    console.log($('#scheduled_test_date_from').val());

                    var query = {
                        search: params.term,
                        from: $('#scheduled_test_date_from').val(),
                        to: $('#scheduled_test_date_to').val(),
                    }

                    return query;
                },
                processResults: function (data) {
                    console.log(data.data);
                    return {
                        results: $.map(data.data, function(obj, index) {
                            return { id: obj.id, text: obj.nama };
                        })
                    };
                },
            }
        });

        $("#form_result").validate({
            rules: {
                file_spt: {
                    required: true,
                    accept: "application/pdf,image/png, image/jpeg"
                },
                file_spuh: {
                    accept: "application/pdf,image/png, image/jpeg"
                },
                file_bukti_bayar: {
                    accept: "application/pdf,image/png, image/jpeg"
                },
                accepted_date: {
                    required: true,
                },
                date_from: {
                    required: true,
                },
                date_to: {
                    required: true,
                },
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
        
        $("#btn_simpan").click(function(e){
            console.log('ndeksini');

            var form = $("#form_result");
            console.log(form);
            console.log('ok');
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
        });
        

    });

</script>
@endsection