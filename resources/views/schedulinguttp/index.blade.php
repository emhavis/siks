@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">

    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            
            <table id="table_data_penjadwalan" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Pendaftaran</th>
                        <th>Nama Pemilik</th>
                        <th>Nama Pemohon</th>
                        <th>Tanggal Permohonan</th>
                        <th>Usulan Penjadwalan</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->no_register }}</td>
                        <td>{{ $row->label_sertifikat }}</td>
                        <td>{{ $row->requestor->full_name }}</td>
                        <td>{{ date("d-m-Y",  strtotime($row->booking->updated_at)) }}</td>
                        <td>{{ date("d-m-Y",  strtotime($row->scheduled_test_date_from)) }} s/d {{ date("d-m-Y",  strtotime($row->scheduled_test_date_to)) }}</td>
                        <td>{{ $row->status->status }} 
                            @if($row->spuh_doc_id != null)
                            @if($row->spuhDoc->keterangan_tidak_siap != null)
                            <div class="alert alert-warning" role="alert">
                                Petugas Tidak Siap: {{ $row->spuhDoc->keterangan_tidak_siap }}
                            </div>
                            @endif
                            @if($row->spuhDoc->keterangan_tidak_lengkap != null)
                            <div class="alert alert-danger" role="alert">
                                Tidak Lengkap: {{ $row->spuhDoc->keterangan_tidak_lengkap }}
                            </div>
                            @endif
                            @if($row->spuhDoc->approval_note != null)
                            <div class="alert alert-danger" role="alert">
                                Tidak Lengkap: {{ $row->spuhDoc->approval_note }}
                            </div>
                            @endif
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('schedulinguttp.schedule', $row->id) }}" class="btn btn-warning btn-sm">Penjadwalan</a>
                            @if($row->spuh_doc_id != null)
                            <a href="{{ route('schedulinguttp.surattugas', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Draft Surat Tugas</a>
                            @if($row->spuhDoc->keterangan_tidak_lengkap != null || $row->spuhDoc->keterangan_tidak_siap != null)
                            <a href="{{ route('schedulinguttp.cancel', ['id' => $row->id]) }}" class="btn btn-warning btn-sm">Batal</a>
                            @endif
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
       
    
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $('.btn-simpan').click(function(e){
            e.preventDefault();
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });
    });
</script>
@endsection