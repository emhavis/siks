<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta name="author" content="Direktorat Metrologi" />    
	<meta name="description" content="SIMPEL UPTP IV">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Document title -->
    <title>SIMPEL UPTP IV</title>
    <!-- Stylesheets & Fonts -->
    <link href="{{ asset('assets/alt/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alt/style.css') }}" rel="stylesheet">
    <style>
        .footer-sticky {
            position: absolute !important;
            bottom: 0;
            width: 100%;
        }
    </style>
</head>

<body>


    <!-- Body Inner -->
    <div class="body-inner">

    <!-- Header -->
        <header id="header" data-transparent="true" class="header-logo-center">
            <div class="header-inner m-t-40">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <span class="logo-default m-t-40">
                            <img id="imgLogo" src="{{ asset('assets/images/logo/logo_v1.0.png') }}" style="width:100px;"/>
                        </span>
                    </div>
                    <!--End: Logo-->
                    
                </div>
            </div>
        </header>
        <!-- end: Header -->

        @if($doc != null)
        <section>
            <div class="container">
                <div class="heading-text heading-section text-center m-b-40" data-animate="fadeInUp">
                    <span class="lead">Surat Tugas tervalidasi untuk rincian sebagai berikut:</span>
                </div>

                <div class="row">
                    <div class="content col-lg-12">
                        
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td width="20%">Nomor Surat Tugas </td>
                                        <td>{{ $doc->doc_no }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Tanggal Terbit</td>
                                        <td>{{ date("d M Y", strtotime($doc->accepted_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Disahkan Oleh</td>
                                        <td>{{ $doc->acceptedBy->PetugasUttp->nama }}, Kepala Balai Pengujian UTTP, a.n. Direktur Metrologi</td>
                                    </tr>
                                   
                                    <tr>
                                        <td width="20%">Jenis Layanan</td>
                                        <td>{{ $doc->request->jenis_layanan }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Pemilik</td>
                                        <td>{{ $doc->request->label_sertifikat }}</td>
                                    </tr>
                                    @foreach($doc->listOfStaffs as $staff)
                                    @if ($loop->first)
                                    <tr>
                                        <td width="20%">Petugas</td>
                                        <td>1. {{ $staff->scheduledStaff->nama }}<br/>{{ $staff->scheduledStaff->jabatan }}<br/>
                                        NIP. {{ $staff->scheduledStaff->nip }}<br/>
                                        Pangkat/Golongan {{ $staff->scheduledStaff->pangkat }}
                                        </td>
                                    </tr>
                                    @else
                                    @if( $staff->scheduledStaff != null )
                                    <tr>
                                        <td width="20%"></td>
                                        <td>2. {{ $staff->scheduledStaff->nama }}<br/>{{ $staff->scheduledStaff->jabatan }}<br/>
                                        NIP. {{ $staff->scheduledStaff->nip }}<br/>
                                        Pangkat/Golongan {{ $staff->scheduledStaff->pangkat }}
                                        </td>
                                    </tr>
                                    @endif
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        @else 
        <section>
            <div class="container">
                <div class="heading-text heading-section text-center m-b-40" data-animate="fadeInUp">
                    <span class="lead">Surat Tugas tidak valid</span>
                </div>
            </div>
        </section>
        @endif
        <!-- END WHAT WE DO -->


        <!-- Footer -->
        <footer id="footer" class="<?= $doc == null ? 'footer-sticky' : '' ?>">
            <div class="copyright-content">
                <div class="container">
                    <div class="copyright-text text-center">&copy; 2021 SIMPEL UPTP IV </div>
                </div>
            </div>
        </footer>
        <!-- end: Footer -->

    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>
    <!--Plugins-->
    <script src="{{ asset('assets/alt/jquery.js') }}"></script>
    <script src="{{ asset('assets/alt/plugins.js') }}"></script>

    <!--Template functions-->
    <script src="{{ asset('assets/alt/functions.js') }}"></script>




</body>

</html>