<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Tugas</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-draft-uttp.png") : public_path("assets/images/logo/letterhead-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
        .table td .eng {
            font-size:8pt;
            font-style:italic;
        }
    </style>
</head>

<body class="A4">

    <section class="sheet {{ $doc->is_accepted  ? 'letterhead' : 'letterhead-draft' }}"> 
        
        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;"></div>

        <div class="text-center">
            <div class="title"><strong>SURAT TUGAS</strong></div>
            <div class="subtitle">Nomor: {{ $doc->doc_no }}</div>
        </div>


        <br/><br/>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 20mm;">Menimbang</td>
                    <td style="width: 2mm;">:</td>
                    <td style="width: 2mm;">a.</td>
                    <td colspan="3">bahwa pelaksanaan Tugas dan Fungsi Balai Pengujian Alat Ukur, Alat Takar, Alat Timbang, dan Alat Perlengkapan 
                        mencakup kegiatan di dalam dan di luar kantor;</td>
                </tr>
                <tr>
                    <td style="width: 20mm;"></td>
                    <td style="width: 2mm;"></td>
                    <td style="width: 2mm;">b.</td>
                    <td colspan="3">bahwa dalam rangka kegiatan di luar kantor perlu diterbitkan Surat Tugas.</td>
                </tr>
                
                <tr>
                    <td style="width: 20mm;">Dasar</td>
                    <td style="width: 2mm;">:</td>
                    <td style="width: 2mm;">1.</td>
                    <td colspan="3">Peraturan Menteri Keuangan Republik Indonesia Nomor 140 Tahun 2023 tentang Jenis dan Tarif 
                        Atas Jenis Penerimaan Negara Bukan Pajak yang Bersifat Volatil yang Berlaku Pada Kementerian Perdagangan;</td>
                </tr>
                <tr>
                    <td style="width: 20mm;"></td>
                    <td style="width: 2mm;"></td>
                    <td style="width: 2mm;">2.</td>
                    <td colspan="3">Peraturan Menteri Perdagangan Republik Indonesia Nomor 55 Tahun 2022 tentang 
                        Organisasi dan Tata Kerja Unit Pelaksana Teknis Kementerian Perdagangan;</td>
                </tr>
                <tr>
                    <td style="width: 20mm;"></td>
                    <td style="width: 2mm;"></td>
                    <td style="width: 2mm;">3.</td>
                    <td colspan="3">Surat permohonan dari {{ $request->label_sertifikat }} 
                        nomor {{ $request->no_surat_permohonan }} 
                        tanggal {{ $request->tgl_surat_permohonan ? format_long_date($request->tgl_surat_permohonan) : format_long_date(date("d-m-Y")) }}.</td>
                </tr>

                <tr><td colspan="6">&nbsp;</td></tr>
                <tr><td colspan="6" style="text-align: center">Memberi Tugas</td></tr>

                @foreach($staffes as $staff)
                @if ($loop->first)
                <tr>
                    <td style="width: 20mm;">Kepada</td>
                    <td style="width: 2mm;">:</td>
                    <td style="width: 2mm;">1.</td>
                    <td>{{ $staff->scheduledStaff->nama }}<br/>{{ $staff->scheduledStaff->jabatan }}</td>
                    <td>NIP. <br/>{{ $staff->scheduledStaff->nip }}</td>
                    <td>Pangkat/Golongan<br/>{{ $staff->scheduledStaff->pangkat }}</td>
                </tr>
                @else
                @if( $staff->scheduledStaff != null )
                <tr>
                    <td style="width: 20mm;"></td>
                    <td style="width: 2mm;"></td>
                    <td style="width: 2mm;">{{ $loop->iteration }}.</td>
                    <td>{{ $staff->scheduledStaff->nama }}<br/>{{ $staff->scheduledStaff->jabatan }}</td>
                    <td>NIP. <br/>{{ $staff->scheduledStaff->nip }}</td>
                    <td>Pangkat/Golongan<br/>{{ $staff->scheduledStaff->pangkat }}</td>
                </tr>
                @endif
                @endif
                @endforeach

                @if ($request->lokasi_dl == 'dalam')
                <tr>
                    <td style="width: 20mm;">Untuk</td>
                    <td style="width: 2mm;">:</td>
                    <td style="width: 2mm;">1.</td>
                    @if($doc->date_to > $doc->date_from)
                    <td colspan="3">Melaksanakan {{ $request->jenis_layanan }} milik {{ $request->label_sertifikat }}
                        di {{ $request->inspectionProv->nama }} selama {{ $doc->days }} hari dari tanggal 
                        {{ format_long_date($doc->date_from) }} sampai dengan tanggal {{ format_long_date($doc->date_to) }};</td>
                    @else
                    <td colspan="3">Melaksanakan {{ $request->jenis_layanan }} milik {{ $request->label_sertifikat }}
                        di {{ $request->inspectionProv->nama }} selama {{ $doc->days }} hari pada tanggal 
                        {{ format_long_date($doc->date_from) }};</td>
                    @endif
                </tr>
                @else
                <tr>
                    <td style="width: 20mm;">Untuk</td>
                    <td style="width: 2mm;">:</td>
                    <td style="width: 2mm;">1.</td>
                    @if($doc->date_to > $doc->date_from)
                    <td colspan="3">Melaksanakan {{ $request->jenis_layanan }} milik {{ $request->label_sertifikat }}
                        di {{ $request->inspectionNegara->nama_negara }} selama {{ $doc->days }} hari dari tanggal 
                        {{ format_long_date($doc->date_from) }} sampai dengan tanggal {{ format_long_date($doc->date_to) }};</td>
                    @else
                    <td colspan="3">Melaksanakan {{ $request->jenis_layanan }} milik {{ $request->label_sertifikat }}
                        di {{ $request->inspectionNegara->nama_negara }} selama {{ $doc->days }} hari pada tanggal 
                        {{ format_long_date($doc->date_from) }};</td>
                    @endif
                </tr>
                @endif

                <tr>
                    <td style="width: 20mm;"></td>
                    <td style="width: 2mm;"></td>
                    <td style="width: 2mm;">2.</td>
                    <td colspan="3">Biaya yang berkaitan dengan pelaksanaan tugas ini dibebankan pada {{ $request->label_sertifikat }}.</td>
                </tr>

            </tbody>
        <table>

        <br/>
        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        Dalam melaksanakan tugas seluruh Pejabat dan Pegawai <strong>"DILARANG MENERIMA GRATIFIKASI DALAM BENTUK APAPUN"</strong>.
        Setelah pelaksanan tugas tersebut selesai agar menyampaikan laporan kepada 
        Kepala Balai Pengujian Alat Ukur, Alat Takar, Alat Timbang, dan Alat Perlengkapan.
        </p>
        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        Demikian untuk dilaksanakan dengan penuh rasa tanggung jawab.
        </p>

        <br/>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm; width: 100%;">
            <tbody>
                <tr>
                    @if($doc->jenis == "revisi" && $doc->keterangan_revisi != null && $doc->keterangan_revisi != '')
                    <td rowspan="5" >Catatan: <br/>
                        Surat Tugas No: {{ $doc_inisial->doc_no }} <br/>
                        diperbarui dengan Surat Tugas ini karena <br/>
                        {{ $doc->keterangan_revisi }}
                    </td>
                    @else
                    <td rowspan="5" >&nbsp;</td>
                    @endif
                    <td style="width: 220px;">Bandung, {{ $doc->accepted_date ? format_long_date($doc->accepted_date) : format_long_date(date("d-m-Y")) }}
                        <br/>Kepala Balai Pengujian
                        <br/>Alat Ukur, Alat Takar, Alat Timbang,
                        <br/>dan Alat Perlengkapan,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($doc->acceptedBy != null)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} ' 
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $doc->acceptedBy != null && $doc->acceptedBy->PetugasUttp ? $doc->acceptedBy->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}<br/>
                        NIP: {{ $doc->acceptedBy != null && $doc->acceptedBy->PetugasUttp ? $doc->acceptedBy->PetugasUttp->nip : '' }}
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        No. Order: {{ $request->no_order }} 
                    </td>
                </tr>
            </tbody>
        </table>
        
    </section>


</body>

</html>