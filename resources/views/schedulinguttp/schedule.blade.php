@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
    </div>
    <div class="panel-body">
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Booking</h4>
            </div>
            <div class="panel-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="label_sertifikat">Nama Pemilik</label>
                            {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Pemilik</label>
                            {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>  
                </div>
            </div>
        </div>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pemohon</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Nama Pemohon</label>
                            {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Jenis Tanda Pengenal</label>
                            {!! Form::text('id_type_id', $requestor->kantor == "Perusahaan" ? "NIB" : "NPWP", ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                            {!! Form::text('pic_id_no', $requestor->kantor == "Perusahaan" ? $requestor->nib : $requestor->npwp, ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_phone_no">Nomor Telepon</label>
                            {!! Form::text('pic_phone_no', $requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_email">Alamat Email</label>
                            {!! Form::text('pic_email', $requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_surat_permohonan">No Surat Permohonan</label>
                            {!! Form::text('no_surat_permohonan', $request->no_surat_permohonan, ['class' => 'form-control','id' => 'no_surat_permohonan', 'placeholder' => 'No Surat Permohonan', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tgl_surat_permohonan">Tanggal Surat Permohonan</label>
                            {!! Form::text('tgl_surat_permohonan', date("d-m-Y", strtotime(isset($request->tgl_surat_permohonan) ? $request->tgl_surat_permohonan : date("Y-m-d"))), ['class' => 'form-control','id' => 'tgl_surat_permohonan', 'placeholder' => 'Tanggal Surat Permohonan', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="file_surat_permohonan">Surat Permohonan</label>
                            @if(!empty($request->file_surat_permohonan))
                            <div class="input-group">
                                <a href="<?= config('app.siks_url') ?>/tracking/download_permohonan/{{ $request->id }}" class="btn btn-warning btn-sm">DOWNLOAD</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_from">Usulan Jadwal Mulai</label>
                            {!! Form::text('scheduled_test_date_from', date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_from_0', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_to">Usulan Jadwal Selesai</label>
                            {!! Form::text('scheduled_test_date_to', date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_to_0', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <!--
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="received_date">Tanggal Masuk Alat</label>
                            {!! Form::text('received_date', date("d-m-Y", strtotime(isset($request->received_date) ? $request->received_date : date("Y-m-d"))), ['class' => 'date form-control','id' => 'receipt_date', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="estimate_date">Tanggal Perkiraan Selesai</label>
                            {!! Form::text('estimated_date', date("d-m-Y", strtotime(isset($request->estimated_date) ? $request->estimated_date : date("Y-m-d"))), ['class' => 'date form-control','id' => 'estimated_date', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lokasi_pengujian">Lokasi Pengujian</label>
                            {!! Form::text('lokasi_pengujian', 
                                ($request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor') . ($request->lokasi_dl == 'dalam' ? ' (Dalam Negeri)' : ' (Luar Negeri)') , 
                                ['class' => 'form-control','id' => 'lokasi_pengujian', 'readonly']) !!}
                             
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Alat</h4>
            </div>

            <div class="panel-body">

                <table class="table table-striped table-hover table-responsive-sm" id="t-roll">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Jenis Alat Ukur</th>
                        <th>Merek</th>
                        <th>Model/Tipe</th>
                        <th>No.Seri</th>
                        <th>Media Uji</th>
                        <th>Max.Capacity</th>
                        <th>Min.Capacity</th>
                        <th>Satuan</th>
                        <th>Buatan</th>
                        <th>Pabrikan</th>
                        <th>Alamat Pabrik</th>
                        @if ($request->lokasi_pengujian == 'luar')
                        <th style="width: 200px;">Alamat Lokasi Pengujian</th>
                        @else 
                        @if ($request->service_type_id == 4 || $request->service_type_id == 5)
                        <th style="width: 200px;">Lokasi Alat</th>
                        @endif
                        @endif
                        <th>Kelengkapan Persyaratan (Break Down) </th>
                        <th>Perlengkapan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $num=0 ?>
                    @foreach($request->items as $item)
                        <?php $num +=1 ?>
                        <tr>

                                <td>{{ $num}}</td>
                                <td>{{ $item->uttp->type->uttp_type}}</td>
                                <td>{{ $item->uttp->tool_brand}}</td>
                                <td>{{ $item->uttp->tool_model}}</td>
                                <td>{{ $item->uttp->serial_no}}</td>
                                <td>{{ $item->uttp->tool_media}}</td>
                                <td>{{ $item->uttp->tool_capacity}}
                                <td>{{ $item->uttp->tool_capacity_min}}</td>
                                <td>{{ $item->uttp->tool_capacity_unit }}</td>
                                <td>{{ $item->uttp->tool_made_in }}</td>
                                <td>{{ $item->uttp->tool_factory }}</td>
                                <td>{{ $item->uttp->tool_factory_address }}</td>
                                @if ($request->lokasi_pengujian == 'luar')
                                <td>
                                    @if ($request->lokasi_dl == 'dalam')
                                    {{ $item->location }}, {{ ($item->kabkot ? $item->kabkot->nama : '') . ', ' . ($item->provinsi ? $item->provinsi->nama : '') }}
                                    <br/>
                                    ({{ $item->location_lat }}, {{ $item->location_long }})
                                    @else 
                                    {{ $item->location }}, {{ $item->negara ? $item->negara->nama_negara : '' }}
                                    @endif
                                </td>
                                @else 
                                @if ($request->service_type_id == 4 || $request->service_type_id == 5)
                                <td>{{ $item->location }}</td>
                                @endif
                                @endif
                                <td> 
                                    @if ($request->service_type_id == 4)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/type_approval_certificate/{{ $item->id }}"
                                        class="btn btn-default btn-sm faa-parent animated-hover" >
                                        <i class="fa fa-download faa-flash"></i> Surat Persetujuan Tipe
                                    </a>
                                    @elseif ($request->service_type_id == 5)
                                    No Sertifikat Sebelumnya: {{ $item->reference_no }}<br/>
                                    Tgl Sertifikat Sebelumnya: {{ $item->reference_date }}<br/>
                                    @if ($request->path_last_certificate != null)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $item->id }}"
                                        class="btn btn-default btn-sm faa-parent animated-hover" >
                                        <i class="fa fa-download faa-flash"></i> Sertifikat Sebelumnya
                                    </a>
                                    @endif
                                    @elseif ($request->service_type_id == 6 || $request->service_type_id == 7)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $item->id }}"
                                        class="btn btn-default btn-sm faa-parent animated-hover" >
                                        <i class="fa fa-download faa-flash"></i> Surat Permohonan
                                    </a>
                                    @if ($request->path_calibration_manual != null)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/calibration_manual/{{ $item->id }}"
                                        class="btn btn-default btn-sm faa-parent animated-hover" >
                                        <i class="fa fa-download faa-flash"></i> Manual Kalibrasi
                                    </a>
                                    @endif
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/manual_book/{{ $item->id }}"
                                        class="btn btn-default btn-sm faa-parent animated-hover" >
                                        <i class="fa fa-download faa-flash"></i> Buku Manual
                                    </a>
                                    @endif
                                </td>
                                <td>
                                    @foreach($item->perlengkapans as $tool)
                                        {{ $tool->uttp->serial_no }}/{{ $tool->uttp->tool_brand }}/{{ $tool->uttp->tool_model }} ({{ $tool->uttp->type->uttp_type }})<br/>
                                    @endforeach
                                </td>
                        </tr>
                    @endforeach
                </tbody>
                </table>

            </div>
        </div>

        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Penugasan dan Penjadwalan</h4>
            </div>
            <div class="panel-body" id="panel_staff">
                <!--
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="spuh_spt">No Surat Tugas</label>
                            {!! Form::text('spuh_spt', $request->spuh_spt,
                                ['class' => 'form-control','id' => 'spuh_spt']) !!}
                        </div>
                    </div>
                </div>
                -->
                <form id="form_create_request">
                <div class="row">
                    @if ($request->lokasi_dl == 'dalam')
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inspection_prov_id">Alamat Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                            {!! Form::text('inspection_prov_id', 
                                $request->inspectionKabkot->nama .', '. $request->inspectionProv->nama,
                                ['class' => 'form-control','id' => 'inspection_prov_id', 'readonly']) !!}
                        </div>
                    </div>
                    @else 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inspection_prinspection_negara_idov_id">Alamat Lokasi Pengujian, Negara</label>
                            {!! Form::text('inspection_negara_id', 
                                $request->inspectionNegara->nama_negara,
                                ['class' => 'form-control','id' => 'inspection_negara_id', 'readonly']) !!}
                        </div>
                    </div>
                    @endif
                    @if ($request->lokasi_dl == 'dalam')
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="spuh_rate">Uang Harian</label>
                            {!! Form::hidden('spuh_rate_id', $request->spuh_rate_id,
                                ['class' => 'form-control','id' => 'spuh_rate_id', 'readonly']) !!}
                            {!! Form::text('spuh_rate', $request->spuh_rate,
                                ['class' => 'form-control','id' => 'spuh_rate', 'readonly']) !!}
                        </div>
                    </div>
                    @else 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="spuh_rate">Uang Harian</label>
                            {!! Form::hidden('spuh_rate__negara_id', $request->spuh_rate__negara_id,
                                ['class' => 'form-control','id' => 'spuh_rate__negara_id', 'readonly']) !!}
                            {!! Form::text('spuh_rate', $request->spuh_rate,
                                ['class' => 'form-control','id' => 'spuh_rate', 'readonly']) !!}
                        </div>
                    </div>
                    @endif
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_from">Jadwal Waktu Pengujian, Mulai</label>
                            {!! Form::text('scheduled_test_date_from', 
                                date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))),
                                ['class' => 'date form-control','id' => 'scheduled_test_date_from']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_to">Jadwal Waktu Pengujian, Selesai</label>
                            {!! Form::text('scheduled_test_date_to', 
                                date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))),
                                ['class' => 'date form-control','id' => 'scheduled_test_date_to']) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <p>Daftar petugas dapat dilihat <a href="{{ route('petugasdl') }}" target="_blank">disini</a>.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_id_1">Penguji/Pemeriksa</label>
                            <select class="form-control select2 scheduled_id" name="scheduled_id_1" id="scheduled_id_1">
                                @if(count($staffes) > 0 && $staffes[0]->scheduledStaff != null)
                                <option value="{{ $staffes[0]->scheduledStaff->id }}" selected="selected">{{ $staffes[0]->scheduledStaff->nama }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_id_2">Penguji/Pemeriksa</label>
                            <select class="form-control select2 scheduled_id" name="scheduled_id_2" id="scheduled_id_2">
                                @if(count($staffes) > 1 && $staffes[1]->scheduledStaff != null)
                                <option value="{{ $staffes[1]->scheduledStaff->id }}" selected="selected">{{ $staffes[1]->scheduledStaff->nama }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="status_submit" id="status_submit" value="0"/>
                </form>

            </div>
        </div>  


        <button type="button" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button>  
        <button type="button" class="btn btn-w-md btn-accent" id="btn_simpan_submit">Simpan dan Konfirmasi</button>  
        
    </div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    var dt_from = null;
    var dt_to = null;

    $(document).ready(function ()
    {
        $('#t-roll').DataTable({
            "scrollX": true
        });

        $('#scheduled_test_date_from, #scheduled_test_date_to').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });

        $('.scheduled_id').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true,
            ajax: {
                url: "{{ route('schedulinguttp.staff') }}",
                data: function (params) {
                    console.log($('#scheduled_test_date_from').val());

                    var query = {
                        search: params.term,
                        from: $('#scheduled_test_date_from').val(),
                        to: $('#scheduled_test_date_to').val(),
                    }

                    return query;
                },
                processResults: function (data) {
                    console.log(data.data);
                    return {
                        results: $.map(data.data, function(obj, index) {
                            return { id: obj.id, text: obj.nama };
                        })
                    };
                },
            }
        });
        


        $('#btn_add_staff').click(function(e) {
            e.preventDefault();

            $('.scheduled_id').select2('destroy');

            var divstaff = $('#template_id').clone();
            divstaff.append('<td><button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button></td>');
            var table = $("#staff");
            table.append(divstaff);

            $('.scheduled_id').select2({
                // placeholder: "- Pilih UML -",
                allowClear: true,
                ajax: {
                    url: "{{ route('schedulinguttp.staff') }}",
                    data: function (params) {
                        console.log($('#scheduled_test_date_from').val());

                        var query = {
                            search: params.term,
                            from: $('#scheduled_test_date_from').val(),
                            to: $('#scheduled_test_date_to').val(),
                        }

                        return query;
                    },
                    processResults: function (data) {
                        console.log(data.data);
                        return {
                            results: $.map(data.data, function(obj, index) {
                                return { id: obj.id, text: obj.nama };
                            })
                        };
                    },
                }
            });
        });

        $('#staff').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });

        $('#btn_simpan').click(function(e){;
            e.preventDefault();

            $('#btn_simpan').attr('disabled', true);

            toastr["warning"]("Sedang menyimpan. Harap tunggu.");
            
            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';

            $.post('{{ route('schedulinguttp.confirmschedule', $request->id) }}',form_data,function(response)
            {
                $('#btn_simpan').attr('disabled', false);

                if(response.status===true)
                {
                    window.location = '{{ route('schedulinguttp') }}';
                }
                else
                {
                    //var msg = show_notice(response.messages);
                    toastr["error"]("Mohon periksa kembali","Form Invalid");
                }
            });
        });

        $('#btn_simpan_submit').click(function(e){;
            e.preventDefault();

            $("#status_submit").val(1);

            $('#btn_simpan_submit').attr('disabled', true);

            toastr["warning"]("Sedang menyimpan. Harap tunggu.");
            
            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';

            $.post('{{ route('schedulinguttp.confirmschedule', $request->id) }}',form_data,function(response)
            {
                $('#btn_simpan_submit').attr('disabled', false);

                if(response.status===true)
                {
                    window.location = '{{ route('schedulinguttp') }}';
                }
                else
                {
                    //var msg = show_notice(response.messages);
                    toastr["error"]("Mohon periksa kembali","Form Invalid");
                }
            });
        });

      
    });

</script>
@endsection