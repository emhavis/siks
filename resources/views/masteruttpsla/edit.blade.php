@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row col-md-6">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
            <div class="panel-body">
                {!! Form::model($row, ['method' => 'PATCH', 'route' => ['uttpsla.update', $row->id],
                    'id' => 'form_update_owner', 'enctype' => "multipart/form-data"]) !!}
                    <div class="form-group">
                        <label for="kelompok">Kelompok</label> 
                        {!!
                            Form::text("kelompok",$row?$row->kelompok:'',[
                            'class' => 'form-control',
                            'id' => 'kelompok',
                            'placeholder' => 'Nama Kelompok',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="jenis_pengujian">Jenis Pengujian</label> 
                        {!!
                            Form::text("jenis_pengujian",$row?$row->jenis_pengujian:'',[
                            'class' => 'form-control',
                            'id' => 'jenis_pengujian',
                            'placeholder' => 'Test Type',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="sla">SLA</label> 
                        {!!
                            Form::text("sla",$row?$row->sla:'',[
                            'class' => 'form-control',
                            'id' => 'sla',
                            'placeholder' => 'Service Level Agreement',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_template_id">Nama Layanan</label> 
                        {!!
                            Form::select("service_type_id",
                            $services,
                            $row?$row->service_type_id:'',[
                            'class' => 'form-control',
                            'id' => 'service_type_id',
                            'placeholder' => 'Service Type ',
                            'required'
                            ]);
                        !!}
                    </div>
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection