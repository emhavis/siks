@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row col-md-6">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup" action="{{ route('uttpsla.store')}}" enctype="multipart/form-data">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="kelompok">Kelompok</label> 
                        {!!
                            Form::text("kelompok",$row?$row->kelompok:'',[
                            'class' => 'form-control',
                            'id' => 'kelompok',
                            'placeholder' => 'Nama Kelompok',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="jenis_pengujian">Jenis Pengujian</label> 
                        {!!
                            Form::text("jenis_pengujian",$row?$row->jenis_pengujian:'',[
                            'class' => 'form-control',
                            'id' => 'jenis_pengujian',
                            'placeholder' => 'Test Type',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="sla">SLA</label> 
                        {!!
                            Form::text("sla",$row?$row->sla:'',[
                            'class' => 'form-control',
                            'id' => 'sla',
                            'placeholder' => 'Service Level Agreement',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_template_id">Nama Layanan</label> 
                        {!!
                            Form::select("service_type_id",
                            $services,
                            $row?$row->service_type_id:'',[
                            'class' => 'form-control',
                            'id' => 'service_type_id',
                            'placeholder' => 'Service Type ',
                            'required'
                            ]);
                        !!}
                    </div>
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function ()
{
    document.getElementById('addForm').style.display= 'none';  
    var checkbox = document.querySelector("input[name=has_range]");
    checkbox.checked =''

    checkbox.addEventListener('change', function() {
    if (this.checked) {
        document.getElementById('addForm').style.display= 'block';; 
    } else {
        document.getElementById('addForm').style.display= 'none';  
    }
    });
    $('#template_id').select2({
    });
    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post("{{ route('uttpsla.store') }}",formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            console.log(response.messages)
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = "{{ route('uttpsla') }}";
            }
        });

    });
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection