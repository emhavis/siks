@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled col-12" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading" >
            <h4>Informasi Pelanggan</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="full_name">Nama Pelanggan</label>
                        {!! Form::text('full_name', $row->full_name, ['class' => 'form-control','id' => 'full_name', 'readonly']) !!} 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="kantor">Jenis</label>
                        {!! Form::text('kantor', $row->kantor, ['class' => 'form-control','id' => 'kantor', 'readonly']) !!} 
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nib">NIB</label>
                        {!! Form::text('nib', $row->nib, ['class' => 'form-control','id' => 'nib', 'readonly']) !!} 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="npwp">NPWP</label>
                        {!! Form::text('npwp', $row->npwp, ['class' => 'form-control','id' => 'npwp', 'readonly']) !!} 
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        {!! Form::textarea('alamat', $row->alamat, ['class' => 'form-control','id' => 'alamat', 'readonly']) !!} 
                    </div>
                </div>  
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="kota">Kabupaten/Kota</label>
                        {!! Form::text('kota', $row->kota, ['class' => 'form-control','id' => 'kota', 'readonly']) !!} 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="provinsi">Provinsi</label>
                        {!! Form::text('provinsi', $row->provinsi, ['class' => 'form-control','id' => 'provinsi', 'readonly']) !!} 
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        {!! Form::text('email', $row->email, ['class' => 'form-control','id' => 'email', 'readonly']) !!} 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone">Telepon</label>
                        {!! Form::text('phone', $row->phone, ['class' => 'form-control','id' => 'phone', 'readonly']) !!} 
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="panel panel-filled col-12" id="panel_uttp_owner">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading" >
            <h5>Pemilik UTTP</h5>
        </div>
        <div class="panel-body">
            @foreach($uttp_owners as $uttp_owner)
            <div class="panel panel-filled panel-c-danger">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Nama Pemilik</label>
                                {!! Form::text('nama', $uttp_owner->nama, ['class' => 'form-control','id' => 'nama', 'readonly']) !!} 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="bentuk">Bentuk Badan Usaha</label>
                                {!! Form::text('bentuk', $uttp_owner->bentuk, ['class' => 'form-control','id' => 'bentuk', 'readonly']) !!} 
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nib">NIB</label>
                                {!! Form::text('nib', $uttp_owner->nib, ['class' => 'form-control','id' => 'nib', 'readonly']) !!} 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="npwp">NPWP</label>
                                {!! Form::text('npwp', $uttp_owner->npwp, ['class' => 'form-control','id' => 'npwp', 'readonly']) !!} 
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                {!! Form::textarea('alamat', $uttp_owner->alamat, ['class' => 'form-control','id' => 'alamat', 'readonly']) !!} 
                            </div>
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kota">Kabupaten/Kota</label>
                                {!! Form::text('kota', $uttp_owner->kota, ['class' => 'form-control','id' => 'kota', 'readonly']) !!} 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="provinsi">Provinsi</label>
                                {!! Form::text('provinsi', $uttp_owner->provinsi, ['class' => 'form-control','id' => 'provinsi', 'readonly']) !!} 
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                {!! Form::text('email', $uttp_owner->email, ['class' => 'form-control','id' => 'email', 'readonly']) !!} 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telepon">Telepon</label>
                                {!! Form::text('telepon', $uttp_owner->telepon, ['class' => 'form-control','id' => 'telepon', 'readonly']) !!} 
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="penanggung_jawab">Penanggung Jawab</label>
                                {!! Form::text('penanggung_jawab', $uttp_owner->penanggung_jawab, ['class' => 'form-control','id' => 'penanggung_jawab', 'readonly']) !!} 
                            </div>
                        </div>
                    </div>

                    <br/>
                    <h5>Daftar UTTP</h5>
                    <table id="table_uttp" class="table table-striped table-hover table-responsive-sm">
                    <thead>
                        <tr>
                            <th>Tipe</th>
                            <th>No Seri</th>
                            <th>Merek</th>
                            <th>Model/Tipe</th>
                            <th>Media Uji/Komoditas</th>
                            <th>Kapasitas Maksimal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($uttps->filter(function($item, $key) use ($uttp_owner) { return $item->owner_id == $uttp_owner->id; }) as $uttp)
                        <tr>
                            <td>{{ $uttp->type->uttp_type }}</td>
                            <td>{{ $uttp->serial_no }}</td>
                            <td>{{ $uttp->tool_brand }}</td>
                            <td>{{ $uttp->tool_model }}</td>
                            <td>{{ $uttp->tool_capacity }} {{ $uttp->tool_capacity_unit }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script>
$(document).ready(function ()
{
    $('#user_delegatee_id').select2({
        placeholder: "- Pilih Pegawai -",
        allowClear: true
    });

    var date = new Date();
    $('#berlaku_mulai, #berlaku_sampai').datepicker({
        format:"dd-mm-yyyy",
        autoclose: true,
        startDate: '-d',
    });
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection