@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-md-12"> 
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createUser">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="full_name">Nama Lengkap</label>
                            {!!
                                Form::text("full_name",$row?$row->full_name:'',[
                                'class' => 'form-control',
                                'id' => 'full_name',
                                'placeholder' => 'Nama Lengkap',
                                'required'
                                ]);
                            !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="username">Username</label>
                            {!!
                                Form::text("username",$row?$row->username:'',[
                                'class' => 'form-control',
                                'id' => 'username',
                                'placeholder' => 'Nama Akun',
                                'required'
                                ]);
                            !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            {!!
                                Form::email("email",$row?$row->email:'',[
                                'class' => 'form-control',
                                'id' => 'email',
                                'placeholder' => 'Email',
                                'required'
                                ]);
                            !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="user_role">Peran/Role</label>
                            {!! Form::select('user_role', $mRole, $row?$row->user_role:null,
                                [
                                'class' => 'form-control',
                                'id' => 'user_role',
                                'placeholder' => '- Pilih Role -',
                                'required'
                                ]) !!}  

                            {!! Form::hidden('tipe', null, ['id' => 'tipe']) !!}
                        </div>
                    </div>
                    <div class="row" id="uttp">
                        <div class="form-group col-md-6">
                            <label for="laboratory_id">Laboratorium</label>
                            {!! Form::select('laboratory_id', $mLabUttp, $row?$row->laboratory_id:null,
                                ['class' => 'form-control',
                                    'id' => 'laboratory_id',
                                    'placeholder' => '- Pilih Laboratorium -',
                                    'required']) !!}
                        </div>
                        <div class="form-group col-md-6" >
                            <label for="petugas_uttp_id">Petugas/Staf</label>
                            {!! Form::select('petugas_uttp_id', $mPetugasUttp, $row?$row->petugas_uttp_id:null,
                                ['class' => 'form-control',
                                    'id' => 'petugas_uttp_id',
                                    'placeholder' => '- Pilih Nama Petugas (UTTP) -',]) !!}
                        </div>
                    </div>
                    <div class="row" id="snsu">
                        <div class="form-group col-md-6">
                            <label for="laboratory_id">Laboratorium</label>
                            {!! Form::select('snsu_laboratory_id', $mLabSnsu, $row?$row->laboratory_id:null,
                                ['class' => 'form-control',
                                    'id' => 'snsu_laboratory_id',
                                    'placeholder' => '- Pilih Laboratorium -',
                                    'required']) !!}
                        </div>
                        <div class="form-group col-md-6" >
                            <label for="petugas_uttp_id">Petugas/Staf</label>
                            {!! Form::select('snsu_petugas_uttp_id', $mPetugasSnsu, $row?$row->petugas_uttp_id:null,
                                ['class' => 'form-control',
                                    'id' => 'snsu_petugas_uttp_id',
                                    'placeholder' => '- Pilih Nama Petugas (SNSU) -',]) !!}
                        </div>
                    </div>
                </form>
                <button type="button" id="submit" class="btn btn-default">Simpan</button>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script>
$(document).ready(function ()
{
    $("#user_role,#uml_id,#laboratory_id,#petugas_uttp_id,#snsu_laboratory_id,#snsu_petugas_uttp_id").select2();

    $("#snsu").hide();
    $("#uttp").hide();

    $("#user_role").change(function(){
        var user_role = $(this).val();

        if(user_role=="5" || user_role=="14") {
            $("#tipe").val(null);
        } else if(user_role=="3" || user_role=="4" || user_role=="15" || user_role=="16" || user_role=="22" || user_role=="25") {
            $("#tipe").val('snsu');
        } else {
            $("#tipe").val('uttp');
        }

        if ($("#tipe").val() == 'snsu') {
            $("#snsu").show();
            $("#uttp").hide();
        } else if ($("#tipe").val() == 'uttp') {
            $("#snsu").hide();
            $("#uttp").show();
        } else {
            $("#snsu").hide();
            $("#uttp").hide();
        }
    });

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createUser").serialize();

        $(document).find("small.text-warning").remove();
        $("#panel_create").toggleClass("ld-loading");

        $.post('{{ route('user.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                toastr["success"]("Berhasil disimpan ", "",{"positionClass": "toast-top-center"});
                setTimeout(function(){
                    window.location = '{{ route('user') }}';
                }
                ,3000);
            }
        });

    });
});

</script>
@endsection
