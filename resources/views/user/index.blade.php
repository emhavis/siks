@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <a href="{{ route('user.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUser" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Nama Akun</th>
                        <th>Nama Lengkap</th>
                        <th>Email</th>
                        <th>Tgl. Registrasi</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($users->count())
                    @foreach($users as $user)
                    <tr>
                        <td>{{ ucfirst($user->username) }}</td>
                        <td>{{ $user->full_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->is_active=="t"?"Aktif":"Non Aktif" }}</td>
                        <td>
                            <a href="{{ route('user.create', $user->id) }}" class="btn btn-info btn-sm">Edit</a>
                            @if($user->is_active!="t")
                            <a href="#" id="action" data-id="{{ $user->id }}" data-action="delete" class="btn btn-warning btn-sm">Delete</a>
                            <a href="#" id="action" data-id="{{ $user->id }}" data-action="aktif" class="btn btn-warning btn-sm">Aktif</a>
                            @endif
                            @if($user->is_active=="t")
                            <a href="#" id="action" data-id="{{ $user->id }}" data-action="nonaktif" class="btn btn-warning btn-sm">Non Aktif</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function () {
    
    var mytable = $('#tUser').DataTable();

    $("a#action").click(function()
    {
        var data = $(this).data();
        data._token = "{{ csrf_token() }}";
        $.post("{{ route('user.action') }}",data,function(response)
        {
            window.location.href = "{{ route("user") }}";
        });
    });

});
</script>
@endsection