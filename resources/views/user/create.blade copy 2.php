@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-md-12"> 
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createUser">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="full_name">Nama Lengkap</label>
                            {!!
                                Form::text("full_name",$row?$row->full_name:'',[
                                'class' => 'form-control',
                                'id' => 'full_name',
                                'placeholder' => 'Nama Lengkap',
                                'required'
                                ]);
                            !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="username">Username</label>
                            {!!
                                Form::text("username",$row?$row->username:'',[
                                'class' => 'form-control',
                                'id' => 'username',
                                'placeholder' => 'Nama Akun',
                                'required'
                                ]);
                            !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            {!!
                                Form::email("email",$row?$row->email:'',[
                                'class' => 'form-control',
                                'id' => 'email',
                                'placeholder' => 'Email',
                                'required'
                                ]);
                            !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="user_role">Peran/Role</label>
                            {!! Form::select('user_role', $mRole, $row?$row->user_role:null,
                                [
                                'class' => 'form-control',
                                'id' => 'user_role',
                                'placeholder' => '- Pilih Role -',
                                'required'
                                ]) !!}  
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6" id="div_laboratory_id">
                            <label for="laboratory_id">Laboratorium</label>
                            {!! Form::select('laboratory_id', $mLab, $row?$row->laboratory_id:null,
                                ['class' => 'form-control',
                                    'id' => 'laboratory_id',
                                    'placeholder' => '- Pilih Laboratorium -',
                                    'required']) !!}
                        </div>
                        <div class="form-group col-md-6" id="div_uml_id">
                            <label for="uml_id">Nama UML</label>
                            {!! Form::select('uml_id', $mUml, $row?$row->uml_id:null,
                                ['class' => 'form-control',
                                    'id' => 'uml_id',
                                    'placeholder' => '- Pilih UML -',
                                    'required']) !!}
                        </div>
                        <div class="form-group col-md-6" >
                            <label for="petugas_uttp_id">Petugas UTTP</label>
                            {!! Form::select('petugas_uttp_id', $mPetugasUttp, $row?$row->petugas_uttp_id:null,
                                ['class' => 'form-control',
                                    'id' => 'petugas_uttp_id',
                                    'placeholder' => '- Pilih Nama Petugas (UTTP) -',]) !!}
                        </div>
                    </div>
                </form>
                <button type="button" id="submit" class="btn btn-default">Simpan</button>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script>
$(document).ready(function ()
{
    $("#user_role,#uml_id,#laboratory_id").select2();

    $("#div_laboratory_id,#div_uml_id").hide();

    $("#user_role").change(function(){
        var user_role = $(this).val();

        if(user_role=="3" || user_role=="4")
        {
            $("#div_laboratory_id").show();
        }
        else
        {
            $("#div_laboratory_id").hide();
        }
        if(user_role=="7")
        {
            $("#div_uml_id").show();
        }
        else
        {
            $("#div_uml_id").hide();
        }
    });

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createUser").serialize();

        $(document).find("small.text-warning").remove();
        $("#panel_create").toggleClass("ld-loading");

        $.post('{{ route('user.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                toastr["success"]("Berhasil disimpan ", "",{"positionClass": "toast-top-center"});
                setTimeout(function(){
                    window.location = '{{ route('user') }}';
                }
                ,3000);
            }
        });

    });
});

</script>
@endsection
