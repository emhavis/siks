@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">

@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="service_type_id">Jenis UTTP</label> 
                        {!! Form::select('uttp_type_id', 
                            $types,
                            $row?$row->uttp_type_id:'', 
                            ['class' => 'form-control', 'id' => 'uttp_type_id', 'placeholder' => '- Pilih Jenis UTTP -', 'required']) !!}
                    </div>
                    
                    <div class="form-group">
                        <label for="inspection_price_id">Jenis Pengujian dan Harga</label> 
                        {!! Form::select('inspection_price_id', 
                            $prices,
                            $row?$row->inspection_price_id:'', 
                            ['class' => 'form-control', 'id' => 'inspection_price_id', 'placeholder' => '- Pilih Pengujian dan Harga -', 'required']) !!}
                    </div>
                    
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')

<script>
$(document).ready(function ()
{
    

    $('#inspection_price_id,#uttp_type_id').select2({
        // placeholder: "- Pilih UML -",
        allowClear: true
    });

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('uttpinspectionprice.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('uttpinspectionprice') }}';
            }
        });

    });
});

</script>
@endsection