@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
            
        </div>
        <div class="panel-body">
            {!! Form::model($row, ['method' => 'PATCH', 'route' => ['uttptype.update', $row->id],
                'id' => 'form_update_owner', 'enctype' => "multipart/form-data"]) !!}
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="uttp_type">Jenis Uttp</label> 
                        {!!
                            Form::text("uttp_type",$row?$row->uttp_type:'',[
                            'class' => 'form-control',
                            'id' => 'uttp_type',
                            'placeholder' => 'Uttp Type',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-6">
                        <label for="uttp_type_certificate">Nama di Sertifikat</label> 
                        {!!
                            Form::text("uttp_type_certificate",$row?$row->uttp_type_certificate:'',[
                            'class' => 'form-control',
                            'id' => 'uttp_type_certificate',
                            'placeholder' => 'Uttp Type',
                            'required'
                            ]);
                        !!}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="kelompok">Kelompok UTTP (untuk Tera-Tera Ulang)</label> 
                        {!!
                            Form::select("kelompok", 
                            [
                                'Automatic Level Gauge' => 'Automatic Level Gauge',
                                'Meter Air' => 'Meter Air',
                                'Meter Gas' => 'Meter Gas',
                                'Meter Gas (USM WetCal)' => 'Meter Gas (USM WetCal)',
                                'Meter Arus BBM' => 'Meter Arus BBM',
                                'Depth Tape' => 'Depth Tape',
                                'UTI Meter' => 'UTI Meter',
                                'Pelat Orifice' => 'Pelat Orifice',
                                'Pompa Ukur BBM' => 'Pompa Ukur BBM',   
                            ],
                            $row?$row->kelompok:'',[
                            'class' => 'form-control',
                            'id' => 'kelompok',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-2">
                        <label for="kelompok">Uji Tipe</label> 
                        {!!
                            Form::checkbox("is_ujitipe",true,$row->is_ujitipe,
                            [
                            'id' => 'is_ujitipe',
                            'placeholder' => 'Berlaku untuk Uji Tipe',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-2">
                        <label for="is_tera">Tera/Tera Ulang</label> 
                        {!!
                            Form::checkbox("is_tera",true,$row->is_tera,
                            [
                            'id' => 'is_tera',
                            'placeholder' => 'Berlaku untuk Tera/Tera Ulang',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-2">
                        <label for="is_active">Aktif</label> 
                        {!!
                            Form::checkbox("is_active",true,$row->is_active,
                            [
                            'id' => 'is_active',
                            'placeholder' => 'Berlaku untuk Tera/Tera Ulang',
                            'required'
                            ]);
                        !!}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="instalasi_id">Instalasi</label> 
                        {!!
                            Form::select("instalasi_id", 
                            $instalasi,
                            $row?$row->instalasi_id:'',[
                            'class' => 'form-control',
                            'id' => 'instalasi_id',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-6">
                        <label for="syarat_teknis">Syarat Teknis</label>   
                        {!!
                            Form::textarea('syarat_teknis', $row->syarat_teknis,[
                            'class' => 'form-control ckeditor',
                            'id' => 'syarat_teknis',
                            'placeholder' => 'Isi Syarat Teknis',
                            'required'
                            ]);
                        !!}
                    </div>
                </div>
                <button type="submit" id="submit" class="btn btn-default">Submit</button>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
</div>

@foreach($kajiUlangs as $kajiUlang)
<div class="row">
    <div class="col-md-12">
    <div class="panel panel-filled" id="panel_kaji_ulang_{{ $kajiUlang->id }}">  
        <div class="loader">
            <div class="loader-bar"></div>
        </div>   
        <div class="panel-heading">
            <h4>Formulir Kaji Ulang</h4>
        </div>
        <div class="panel-body">
            <form id="kajiUlang" class="form-kaji-ulang">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $kajiUlang->id }}">
                <input type="hidden" name="uttp_type_id" value="{{ $kajiUlang->uttp_type_id }}">
                <input type="hidden" name="service_type_id" value="{{ $kajiUlang->service_type_id }}">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="service_type">Jenis Layanan</label> 
                        {!!
                            Form::text("service_type",$kajiUlang->service_type,[
                            'class' => 'form-control',
                            'id' => 'service_type',
                            'required', 'readonly',
                            ]);
                        !!}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="kaji_ulang">Kaji Ulang</label>   
                        {!!
                            Form::textarea('kaji_ulang', $kajiUlang->kaji_ulang,[
                            'class' => 'form-control ckeditor',
                            'id' => 'kaji_ulang_' . $kajiUlang->id,
                            'placeholder' => 'Isi Kaji Ulang',
                            'required'
                            ]);
                        !!}
                    </div>
                </div>

                <button type="submit" id="simpan-kaji-ulang" class="btn btn-default simpan-kaji-ulang">Simpan</button>
            </form>
        </div>
    </div>
    </div>
</div>
@endforeach
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}"></script>
<script>
$(document).ready(function ()
{

    $('#instalasi_id, #kelompok').select2({
        // placeholder: "- Pilih UML -",
        allowClear: true
    });

    $(".simpan-kaji-ulang").click(function(e)
    {
        e.preventDefault();

        var form = $(this).parents('form:first');
        var id = form.find('input[name=id]').val();

        var editorText = CKEDITOR.instances['kaji_ulang_' + id].getData();        
        form.find('textarea[name=kaji_ulang]').html(editorText);

        console.log(editorText);

        var formData = form.serialize();

        $("#panel_kaji_ulang_" + id).toggleClass("ld-loading");
        $.post('{{ route('uttptype.saveKajiUlang') }}',formData,function(response)
        {
            $("#panel_kaji_ulang_" + id).toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('instalasi') }}';
            }
        });


    });
});
@endsection