@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
        </div>
        <div class="panel-body">
            <form id="createGroup" action="{{ route('uttptype.store')}}" enctype="multipart/form-data">
                @if($id)
                <input type="hidden" name="id" value="{{ $id }}">
                @endif
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="uttp_type">Jenis Uttp</label> 
                        {!!
                            Form::text("uttp_type",$row?$row->uttp_type:'',[
                            'class' => 'form-control',
                            'id' => 'uttp_type',
                            'placeholder' => 'Uttp Type',
                            'required'
                            ]);
                        !!}
                    </div>
                </div>
                <button type="submit" id="submit" class="btn btn-default">Submit</button>
            </form> 
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function ()
{
    document.getElementById('addForm').style.display= 'none';  
    var checkbox = document.querySelector("input[name=has_range]");
    checkbox.checked =''

    checkbox.addEventListener('change', function() {
    if (this.checked) {
        document.getElementById('addForm').style.display= 'block';; 
    } else {
        document.getElementById('addForm').style.display= 'none';  
    }
    });
    $('#template_id').select2({
    });
    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post("{{ route('uttptype.store') }}",formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            console.log(response.messages)
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = "{{ route('uttptype') }}";
            }
        });

    });
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection