@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">

    <ul class="nav nav-tabs" role="tablist" id="tabs">
        <li role="presentation" class="active">
            <a href="#kn" aria-controls="kn" role="tab" data-toggle="tab" 
                class="badge-notif-tab">Dalam Kantor</a>
        </li>
        <li role="presentation">
            <a href="#dl" aria-controls="dl" role="tab" data-toggle="tab" 
                class="badge-notif-tab">Dinas Luar</a>
        </li>
    </ul>

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="kn">
            <br/>
            <div class="panel panel-filled table-area">
    
            <div class="panel-body">
                
                <table id="data_table_kn" class="table table-striped table-hover table-responsive-sm">
                    <thead>
                        <tr>
                            <th style="display:none"></th>
                            <th style="width:'100px'">No Order</th>
                            <th>Alat</th>
                            <th>Diterima Oleh</th>
                            <th>Diuji Oleh</th>
                            <th>Tgl Terima</th>
                            <th>Tgl Selesai Uji</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="dl">
            <br/>
            <div class="panel panel-filled table-area">
    
            <div class="panel-body">
                
                <table id="data_table_dl" class="table table-striped table-hover table-responsive-sm">
                    <thead>
                        <tr>
                            <th style="display:none"></th>
                            <th style="width:'100px'">No Order</th>
                            <th>Alat</th>
                            <th>Diterima Oleh</th>
                            <th>Diuji Oleh</th>
                            <th>Tgl Terima</th>
                            <th>Tgl Selesai Uji</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('#data_table_kn').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ route('history.listJson') }}?tipe=kn',
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function(data) {
                data.cSearch = $("#search").val();
            }
        },
        order: ['1', 'DESC'],
        pageLength: 10,
        searching: true,
        aoColumns: [
            {
                data: 'id',
                "visible": false,
                "searchable": false
            },
            {
                data: 'service_request_item.no_order',
            },
            {
                data: 'alat',
            },
            {
                data: 'diterima_oleh',
            },
            {
                data: 'testby'
            },
            {
                data: 'staff_entry_datein',
                width: "20%",
            },
            {
                data: 'staff_entry_dateout',
                width: "20%",
            },
            {
                data: 'status',
                width: "20%",
            },
            {
                data: 'id',
                width: "20%",
                render: function(data, type, row,file_skhp) {
                    let buttons = '';
                    if(data.stat_service_order == 2){
                        if(row.file_skhp != null){
                            buttons +=`<a href="{{ route('approveuut.preview',`+row.id+`)}}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>`;
                            buttons +=`<a href="{{ route('serviceuut.download',`+row.id+`)}}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>`;
                        }
                        if(row.path_lampiran != null){
                            buttons +=`<a href="{{ route('serviceuut.downloadLampiran',`+row.id+`)}}" class="btn btn-warning btn-sm">LIHAT LAMPIRAN</a>`;
                        }
                        if(row.cancelation_file != null){
                            buttons +=`<a href="{{ route('serviceuut.cancelation_download',`+row.id+`)}}" class="btn btn-warning btn-sm">LIHAT PEMBATALAN</a>`;
                        }
                        if(row.ujitipe_completed == true){
                            buttons +=`<a href="{{ route('approveuut.preview',`+row.id+`)}}" class="btn btn-warning btn-sm">LIHAT SKHP TIPE</a>`;
                            buttons +=`<a href="{{ route('approveuut.preview',`+row.id+`)}}" class="btn btn-warning btn-sm">DOWNLOAD SKHP TIPE</a>`;
                        }
                        if(row.kabalai_id > 0){
                            buttons +=`<a href="{{ route('approveuut.preview',`+data+`)}}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>`;
                        }
                        if(row.ujitipe_completed == true){}
                        if(row.file_path_lampiran != null){}
                    }else if(row.stat_service_order==3){
                        if(row.file_skhp != null){
                            buttons += "<a href='{{ url('/')}}/approveuut/preview/"+data+"' class='btn btn-warning btn-sm'>LIHAT SERTIFIKAT 01</a>";
                        }
                        if(row.file_skhp != null && row.kabalai_id > 0){
                            buttons +="<a href='{{ url('/')}}/serviceuut/print/"+data+"' class='btn btn-warning btn-sm'>DOWNLOAD SERTIFIKAT</a>";
                        }
                        if(row.path_lampiran != null){
                            buttons +="<a href='{{ url('/')}}/serviceuut/downloadLampiran/"+data+"' class='btn btn-warning btn-sm'>LIHAT LAMPIRAN</a>";
                        }
                        if(row.ujitipe_completed == true){
                            buttons +="<a href='{{ url('/')}}/erviceuut/previewTipe/"+data+"' class='btn btn-warning btn-sm'>LIHAT SKHP TIPE</a>";
                            buttons +="<a href='{{ url('/')}}/erviceuut/printTipe/"+data+"'  class='btn btn-warning btn-sm'>DOWNLOAD SKHP TIPE</a>";
                        }
                    }else if(row.stat_service_order==4){
                        buttons +=`<a href="{{ route('approveuut.preview',`+row.id+`)}}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>`;
                    }else if(row.is_finish=="0"){
                        if(row.file_skhp != null){
                            buttons += "<a href='{{ url('/')}}/approveuut/preview/"+data+"' class='btn btn-warning btn-sm'>LIHAT SERTIFIKAT 01</a>";
                        }
                        if(row.file_skhp != null && row.kabalai_id > 0){
                            buttons += "<a href='{{ url('/')}}/approveuut/download/"+data+"' class='btn btn-warning btn-sm'>DOWNLOAD SERTIFIKAT</a>";
                        }
                        if(row.path_lampiran != null){
                            buttons +="<a href='{{ url('/')}}/serviceuut/downloadLampiran/"+data+"' class='btn btn-warning btn-sm'>LIHAT LAMPIRAN</a>";
                        }
                    }
                    buttons.replace("::id",row.id)
                    return buttons;
                }
            }
        ]
    });

    $('#data_table_dl').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ route('history.listJson') }}?tipe=dl',
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function(data) {
                data.cSearch = $("#search").val();
            }
        },
        order: ['1', 'DESC'],
        pageLength: 10,
        searching: true,
        aoColumns: [
            {
                data: 'id',
                "visible": false,
                "searchable": false
            },
            {
                data: 'service_request_item.no_order',
            },
            {
                data: 'alat',
            },
            {
                data: 'diterima_oleh',
            },
            {
                data: 'testby'
            },
            {
                data: 'staff_entry_datein',
                width: "20%",
            },
            {
                data: 'staff_entry_dateout',
                width: "20%",
            },
            {
                data: 'status',
                width: "20%",
            },
            {
                data: 'id',
                width: "20%",
                render: function(data, type, row,file_skhp) {
                    let buttons = '';
                    if(data.stat_service_order == 2){
                        if(row.file_skhp != null){
                            buttons +=`<a href="{{ route('approveuut.preview',`+row.id+`)}}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>`;
                            buttons +=`<a href="{{ route('serviceuut.download',`+row.id+`)}}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>`;
                        }
                        if(row.path_lampiran != null){
                            buttons +=`<a href="{{ route('serviceuut.downloadLampiran',`+row.id+`)}}" class="btn btn-warning btn-sm">LIHAT LAMPIRAN</a>`;
                        }
                        if(row.cancelation_file != null){
                            buttons +=`<a href="{{ route('serviceuut.cancelation_download',`+row.id+`)}}" class="btn btn-warning btn-sm">LIHAT PEMBATALAN</a>`;
                        }
                        if(row.ujitipe_completed == true){
                            buttons +=`<a href="{{ route('approveuut.preview',`+row.id+`)}}" class="btn btn-warning btn-sm">LIHAT SKHP TIPE</a>`;
                            buttons +=`<a href="{{ route('approveuut.preview',`+row.id+`)}}" class="btn btn-warning btn-sm">DOWNLOAD SKHP TIPE</a>`;
                        }
                        if(row.kabalai_id > 0){
                            buttons +=`<a href="{{ route('approveuut.preview',`+data+`)}}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>`;
                        }
                        if(row.ujitipe_completed == true){}
                        if(row.file_path_lampiran != null){}
                    }else if(row.stat_service_order==3){
                        if(row.file_skhp != null){
                            buttons += "<a href='{{ url('/')}}/approveuut/preview/"+data+"' class='btn btn-warning btn-sm'>LIHAT SERTIFIKAT 01</a>";
                        }
                        if(row.file_skhp != null && row.kabalai_id > 0){
                            buttons +="<a href='{{ url('/')}}/serviceuut/print/"+data+"' class='btn btn-warning btn-sm'>DOWNLOAD SERTIFIKAT</a>";
                        }
                        if(row.path_lampiran != null){
                            buttons +="<a href='{{ url('/')}}/serviceuut/downloadLampiran/"+data+"' class='btn btn-warning btn-sm'>LIHAT LAMPIRAN</a>";
                        }
                        if(row.ujitipe_completed == true){
                            buttons +="<a href='{{ url('/')}}/erviceuut/previewTipe/"+data+"' class='btn btn-warning btn-sm'>LIHAT SKHP TIPE</a>";
                            buttons +="<a href='{{ url('/')}}/erviceuut/printTipe/"+data+"'  class='btn btn-warning btn-sm'>DOWNLOAD SKHP TIPE</a>";
                        }
                    }else if(row.stat_service_order==4){
                        buttons +=`<a href="{{ route('approveuut.preview',`+row.id+`)}}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>`;
                    }else if(row.is_finish=="0"){
                        if(row.file_skhp != null){
                            buttons += "<a href='{{ url('/')}}/approveuut/preview/"+data+"' class='btn btn-warning btn-sm'>LIHAT SERTIFIKAT 01</a>";
                        }
                        if(row.file_skhp != null && row.kabalai_id > 0){
                            buttons += "<a href='{{ url('/')}}/approveuut/download/"+data+"' class='btn btn-warning btn-sm'>DOWNLOAD SERTIFIKAT</a>";
                        }
                        if(row.path_lampiran != null){
                            buttons +="<a href='{{ url('/')}}/serviceuut/downloadLampiran/"+data+"' class='btn btn-warning btn-sm'>LIHAT LAMPIRAN</a>";
                        }
                    }
                    buttons.replace("::id",row.id)
                    return buttons;
                }
            }
        ]
    });
});

</script>
@endsection