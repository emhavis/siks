@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        {!! Form::open(['url' => route('serviceuut.resultupload', $serviceOrder->id), 'files' => true])  !!}
        
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_uut">Jenis uut</label>
                    {!! Form::text('jenis_uut', $serviceOrder->ServiceRequestItem->uut->type->uut_type, ['class' => 'form-control','id' => 'jenis_uut', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas</label>
                    {!! Form::text('kapasitas', $serviceOrder->ServiceRequestItem->uut->tool_capacity, ['class' => 'form-control','id' => 'kapasitas', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Merek/Model/Tipe</label>
                    {!! Form::text('merek', 
                        $serviceOrder->ServiceRequestItem->uut->tool_brand . '/' . 
                        $serviceOrder->ServiceRequestItem->uut->tool_model . '/' .
                        $serviceOrder->ServiceRequestItem->uut->tool_type,
                        ['class' => 'form-control','id' => 'merek', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->ServiceRequestItem->uut->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan', 'readonly']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemohon</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemohon</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory">Nama Pabrikan</label>
                    {!! Form::text('factory', 
                        $serviceOrder->ServiceRequestItem->uut->tool_factory,
                        ['class' => 'form-control','id' => 'factory', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory_addr">Alamat Pabrikan</label>
                    {!! Form::text('factory_addr', 
                        $serviceOrder->ServiceRequestItem->uut->tool_factory_address,
                        ['class' => 'form-control','id' => 'factory_addr', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Dasar Pengujian, Nomor</label>
                    {!! Form::text('reference_no', 
                        $serviceOrder->ServiceRequestItem->reference_no,
                        ['class' => 'form-control','id' => 'reference_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_date">Dasar Pengujian, Tanggal</label>
                    {!! Form::text('reference_date', 
                        date("d-m-Y", strtotime(isset($serviceOrder->ServiceRequestItem->reference_date) ? $serviceOrder->ServiceRequestItem->reference_date : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'reference_date', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_1">Diuji Oleh (1)</label>
                    {!! Form::text('test_by_1', 
                        $user->full_name,
                        ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_2">Diuji Oleh (2)</label>
                    {!! Form::select('test_by_2', $users, null, 
                        ['class' => 'form-control select2', 'id' => 'test_by_2']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_datein">Waktu Pengujian, Mulai</label>
                    {!! Form::text('staff_entry_datein', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_datein) ? $serviceOrder->staff_entry_datein : date("Y-m-d"))),
                        ['class' => 'date form-control','id' => 'staff_entry_datein', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_dateout">Waktu Pengujian, Selesai</label>
                    {!! Form::text('staff_entry_dateout', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_dateout) ? $serviceOrder->staff_entry_dateout : date("Y-m-d"))),
                        ['class' => 'date form-control','id' => 'staff_entry_dateout', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="persyaratan_teknis">Persyaratan Teknis</label>
                    {!! Form::select('persyaratan_teknis_id', $oimls, null, 
                        ['class' => 'form-control select2','id' => 'persyaratan_teknis_id']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="file_skhp">Lampiran Cerapan</label>
                    {!! Form::file('file_skhp', null,
                        ['class' => 'form-control','id' => 'file_skhp']) !!}
                </div>
            </div>
        </div>

        <table id="data_table" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Pemeriksaan/Pengujian</th>
                    <th>Pemenuhan Persyaratan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($inspectionItems as $item)
                <tr>
                    <td>{{ $item->inspectionItem->name }}</td>
                    <td>
                    @if($item->inspectionItem->is_tested)
                    {!! Form::select('is_accepted_'.$item->id, ['ya'=>'Ya','tidak'=>'Tidak', ''=>'N/A'], null, 
                        ['class' => 'form-control select2','id' => 'is_accepted_'.$item->id, 'style' => 'width:100%']) !!}
                    @endif
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>

        <button role="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan Hasil Uji</button>
        {!! Form::close() !!}
        
        
        
       
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">

    $(document).ready(function ()
    {
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
    }); 
</script>
@endsection