@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    
    <div class="panel panel-filled table-area">
        <div class="panel-body">
            
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th style="display:none"></th>
                        <th style="width:'100px'">No Order</th>
                        <th>Alat</th>
                        <th>Diterima Oleh</th>
                        <th>Diuji Oleh</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr id="{{ $row->id }}">
                        <td style="display:none">{{ $row->id }}</td>
                        <td class="text-left">{{ $row->ServiceRequestItem->no_order ? $row->ServiceRequestItem->no_order :'' }}</td>
                        <td>{{ $row->ServiceRequestItem->uuts->tool_brand }}/{{ $row->ServiceRequestItem->uuts->tool_model }}/{{ $row->ServiceRequestItem->uuts->tool_type }} ({{ $row->ServiceRequestItem->uuts->serial_no ? $row->ServiceRequestItem->uuts->serial_no : '-' }})</td> 
                        <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : ''}}</td>
                        <td>{{ $row->test1 ? $row->test1->full_name : "" }} {{ $row->test2 ? ' & '.$row->test2->full_name : "" }}</td>
                        <td>{{ $row->staff_entry_datein }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>{{ $row->ServiceRequestItem->status ? $row->ServiceRequestItem->status->status :'' }}
                        <td>  
                        @if($row->stat_service_order=="2")
                            @if($row->file_skhp!==null)
                                <a href="{{ route('approveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                <a href="{{ route('serviceuut.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD CERAPAN</a>
                                @if($row->kabalai_id != null || $row->kabalai_id >= 1)
                                    <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                @endif
                                @if($row->ujitipe_completed==true)
                                    <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP TIPE</a>
                                    <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP TIPE</a>
                                @endif
                                    <a href="{{ route('serviceuut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                            @endif
                        
                        @elseif($row->stat_service_order=="3")
                            @if($row->cancelation_file !=null)
                                <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">SURAT PEMBATALAN</a>
                            @endif
                            @if($row->cancelation_file !=null)
                                <a href="{{ route('serviceuut.cancelation_download', $row->id) }}" class="btn btn-warning btn-sm">LAMPIRAN</a>
                            @endif
                            @if($row->file_skhp!==null)
                                <a href="{{ route('approveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                @if($row->kabalai_id != null || $row->kabalai_id >= 1)
                                    <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                @endif
                                <a href="{{ route('file.show', [$row->id,'cerapan']) }}" class="btn btn-warning btn-sm">LIHAT CERAPAN</a>
                                @if($row->ujitipe_completed==true)
                                    <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP TIPE</a>
                                    <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP TIPE</a>
                                @endif
                                @if($row->path_lampiran)
                                    <a href="{{ route('serviceuut.downloadLampiran', [$row->id,'lampiran']) }}" class="btn btn-warning btn-sm">LIHAT LAMPIRAN</a>
                                @endif
                            @endif
                        @elseif($row->stat_service_order=="4")
                            <a href="{{ route('approveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                
                        @elseif($row->is_finish=="0")
                            @if($row->file_skhp!==null)
                                <a href="{{ route('approveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                @if($row->kabalai_id != null || $row->kabalai_id >=1)
                                    <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>\
                                @endif
                                @if($row->ujitipe_completed==true)
                                    <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP TIPE</a>
                                    <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP TIPE</a>
                                @endif
                                    <a href="{{ route('serviceuut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                @endif
                        @elseif($row->is_finish=="1")
                        SELESAI
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    let table = $('#data_table').DataTable({
        scrollX: true,
        "order": [0],
    });

   
});

</script>
@endsection