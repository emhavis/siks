@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">

            <div class="row">
                <div class="col-md-12">
                    <form id="form"  >
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="no_order">Serah Terima Pengembalian Alat Berdasar QR Code</label>
                                    {!! Form::text('no_order', null, ['class' => 'form-control no_order','id' => 'no_order', 'autofocus' => 'autofocus']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="error_notes" clsas=""></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/>   

            <table id="data_table" class="table table-striped table-hover table-responsive-sm data-table">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th style="width=40px !important">Alat</th>
                        <th>Nama Pemilik</th>
                        <th>Nama Pemohon</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequestItem->no_order }} / {{ count($row->ServiceRequest ? $row->ServiceRequest->items : []) }}</td>
                        <td>{{ $row->uutType->uut_type }}<br/>
                            {{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                        <td>{{ $row->ServiceRequest ? $row->ServiceRequest->label_sertifikat : ''}}</td>
                        <td>{{ isset($row->ServiceRequest->requestor) ? $row->ServiceRequest->requestor->full_name : '' }}</td>
                        <td>{{ $row->staff_entry_datein }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>
                            <button class="btn btn-warning btn-sm btn-mdl" 
                                    data-id="{{ $row->id }}" 
                                    data-noorder="{{ $row->ServiceRequestItem->no_order }} / {{ count(isset($row->ServiceRequest->items) ? $row->ServiceRequest->items : []) }}"
                                    data-pemilik="{{isset( $row->serviceRequest) ? $row->serviceRequest->label_sertifikat : ''}}"
                                    data-pemohon="{{ isset($row->serviceRequest) ? $row->serviceRequest->requestor->full_name : ''}}"
                                    data-requestid="{{ isset($row->serviceRequest) ? $row->serviceRequest->id : 0 }}"
                                    data-labid="{{ $row->laboratory_id }}"
                                    data-jenis="{{ $row->uutType->uut_type }}"
                                    data-alat="{{ $row->tool_brand . '/' . $row->tool_model . '/' . $row->tool_type . ' (' . ($row->tool_serial_no ? $row->tool_serial_no : '') . ')' }}">SERAH TERIMA</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Serah Terima Alat ke Pemohon</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <div class="form-group">
                                <input class ="form-control" type="hidden" name="id" id="id"/>
                                <input class="form-control" type="hidden" name="requestid" id="requestid"/>
                                <input class= "form-control" type="hidden" name="labid" id="labid"/>
                                <input type="hidden" name="instalasiid" id="instalasiid"/>
                            </div>
                            <div class="form-group">
                                <label>No Order</label>
                                <input type="text" name="order_no" id="order_no" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Jenis Alat</label>
                                <input type="text" name="jenis" id="jenis" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Alat</label>
                                <input type="text" name="alat" id="alat" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemilik</label>
                                <input type="text" name="pemilik" id="pemilik" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemohon</label>
                                <input type="text" name="pemohon" id="pemohon" class="form-control" readonly required />
                            </div>

                            <div class="form-group">
                                <label>Nama Penerima Alat</label>
                                <input type="text" name="warehouse_out_nama" id="warehouse_out_nama" class="form-control"  required />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">SERAH TERIMA</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function (){
    $('.data-table').DataTable();
    // $('#data_table-test').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     search: {
    //         return: true
    //     },
    //     ajax: {
    //         url: '{{ route('warehouseuut.listData') }}',
    //         type: "POST",
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         data: function(data) {
    //             data.cSearch = $("#search").val();
    //         }
    //     },
    //     order: ['1', 'DESC'],
    //     // pageLength: 1000,
    //     searching: true,
    //     aoColumns: [
    //         {
    //             data: 'id',
    //             "visible": false,
    //             "searchable": false
    //         },
    //         {
    //             data:'id',
    //             width: "30px",
    //             // orderable:false,
    //             render: function(data, type, row){
    //                 let dt = '';
    //                 return dt += row.service_request_item.no_order +'/'+ (row.service_request.items).length
    //             }
    //         },
    //         {
    //             render: function(data,type,row){
    //                 return row.uut_type.uut_type + '<br/>' 
    //                     + row.tool_brand + row.tool_model + row.tool_type + row.tool_serial_no
    //             }
    //         },
    //         {
    //             render: function(data,type,row){
    //                 return row.service_request.label_sertifikat
    //             } 
    //         },
    //         {
    //             render: function(data,type,row){
    //                 return row.service_request.requestor.full_name
    //             } 
    //         },
    //         {data: 'staff_entry_datein'},
    //         {data: 'staff_entry_dateout'},
    //         {
    //             data: 'id',
    //             width: "20%",
    //             render: function(data, type, row,file_skhp) {
    //                 let buttons = '';
    //                 buttons +=  "<button class='btn btn-warning btn-sm btn-mdl'"
    //                                 + "data-id='" + data + "'"
    //                                 + "data-noorder='" + row.service_request_item.no_order + "'"
    //                                 + "data-pemilik='" + row.service_request.label_sertifikat + "'"
    //                                 + "data-pemohon='" + row.service_request.requestor.full_name + "'"
    //                                 + "data-requestid='" + row.service_request.id + "'"
    //                                 + "data-labid='" + row.laboratory_id + "'"
    //                                 + "data-jenis='" + row.uut_type.uut_type + "'"
    //                                 + "data-alat='" + row.tool_brand + "/" + row.tool_model + "/" + row.tool_type + "(" + row.tool_serial_no  + ")" + "'>SERAH TERIMA</button>";
    //                 return buttons;
    //             }
    //         }
    //     ]
    // });
    // end table

    $('#data_table tbody').on( 'click', 'button.btn-mdl', function (e) {
    // $("button.btn-mdl").click(function(e){
        e.preventDefault();
        var id = $(this).data().id;
        var requestid = $(this).data().requestid;
        var labid = $(this).data().labid;
        var instalasiid = $(this).data().instalasiid;
        var alat = $(this).data().alat;
        var pemilik = $(this).data().pemilik;
        var pemohon = $(this).data().pemohon;
        var noorder = $(this).data().noorder;
        var jenis = $(this).data().jenis;

        $("#id").val(id);
        $("#requestid").val(requestid);
        $("#labid").val(labid);
        $("#instalasiid").val(instalasiid);
        $("#alat").val(alat);
        $("#pemilik").val(pemilik);
        $("#pemohon").val(pemohon);
        $("#order_no").val(noorder);
        $("#jenis").val(jenis);

        var route = "{{ route('laboratory.getbyid', ':id') }}";
        route = route.replace(':id', labid);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#lab_nama").val(res.lab.nama_lab);
                
            });
        },"json");
    });

    var form =  $("#form_modal");
    form.validate({
        rules: {
            warehouse_out_nama: {
                required: true,
            },
        },
        messages: {
            warehouse_out_nama: 'Nama penerima wajib diisi',
        },
        errorClass: "help-block error",
        highlight: function(e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function(e) {
            $(e).closest(".form-group").removeClass("has-error")
        },
    });

    $("#simpan").click(function(e) {
        e.preventDefault();

        var id = $('#id').val();
        var requestid = $('#requestid').val();
        
        //var route = "{{ route('receiveuttp.proses', ':id') }}";
        //route = route.replace(':id', id);
        //window.location = route;

        // var form = $('#form_modal');
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        $.post('{{ route('warehouseuut.warehouse') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                // location.reload();
            }
            else
            {
                // var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksa kembali","Form Invalid");
            }
        });
        
        $("#prosesmodal").modal('hide');
    });

    $("#no_order").change(function(e) {

        // $("#id").attr("value",$row->id);
        // $("#requestid").attr("value", $row->serviceRequest->id );
        // $("#labid").attr("value", $row->laboratory_id );
        var id = $("#id").val()
        var requestid = $("#requestid").val()
        var labid = $("#labid").val()

        var no_order = $(this).val();
        var form = $(this).closest("form");
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        $.post('{{ route("warehouseuut.getdata") }}',form_data,function(response)
        {
            if(response.status===true)
            {
                var order = response.data;
                $("#alat").val(order.tool_brand + '/' + order.tool_model + ' (' + order.tool_serial_no + ')');
                $("#pemilik").val(order.pemilik);
                $("#pemohon").val(order.pemohon);
                $("#order_no").val(order.no_order);

                $("#prosesmodal").modal('show');
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
    });
});

</script>
@endsection