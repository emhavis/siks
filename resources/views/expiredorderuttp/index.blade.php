@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<form id="createGroup" action="{{ route('expiredorderuttp')}}" method="GET" >
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="masa_berlaku">Masa Berlaku</label>
                {!! Form::select('masa_berlaku', $kategori, $masa_berlaku, ['class' => 'form-control','id' => 'masa_berlaku']) !!}
            </div> 
        </div>
        <div class="col-md-2">
            {!! Form::submit('Filter', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_filter']) !!}
        </div>
        <div class="col-md-5"></div>
        <div class="col-md-2 text-right">
            <button class="btn btn-success" id="btn1">Export Data</button>
        </div>
    </div>
</form>

<div class="row">

    <div class="panel panel-filled table-area">
        <div class="panel-body">
            
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Alat</th>
                        <th>Jenis</th>
                        <th>Pemilik</th>
                        <th>Lokasi</th>
                        <th>Koordinat</th>
                        <th>No SKHP</th>
                        <th>Tgl SKHP</th>
                        <th>Tgl Masa Berlaku</th>
                        <th>Masa Berlaku (hari)</th>
                        <th>Notifikasi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr id="{{ $row->id }}">
                        <td>{{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                        <td>{{ $row->uttpType->uttp_type }}</td>
                        <td>{{ $row->ServiceRequest->label_sertifikat }}</td>
                        <td>{{ $row->location }}</td>
                        <td>{{ $row->location_lat }}, {{ $row->location_long }}</td>
                        <td>{{ $row->no_sertifikat }}</td>
                        <td>{{ date('d-m-Y', strtotime($row->kabalai_date)) }}</td>
                        <td>{{ date('d-m-Y', strtotime($row->sertifikat_expired_at)) }}</td>
                        <td>@if($row->date_diff >= 60)
                            <span class="label label-success">
                            @elseif($row->date_diff >= 30)
                            <span class="label label-warning">
                            @else
                            <span class="label label-danger">
                            @endif
                            {{ (int)$row->date_diff }}</span>
                        </td>
                        <td>
                            <input type="checkbox" id="notif1" name="notif1" class="form-check-input text-success" 
                            {{ $row->expired_notif1 == true ? 'checked' : '' }} disabled /> 60 Hari<br/>
                            <input type="checkbox" id="notif2" name="notif2" class="form-check-input text-success" 
                            {{ $row->expired_notif2 == true ? 'checked' : '' }} disabled /> 30 Hari
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('#masa_berlaku').select2();

    let table = $('#data_table').DataTable({
        scrollX: true,
    });

    $("#btn1").on('click',function(e) {
        // console.log('console');
        var _token = $('#_token').val();
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $('#createGroup');
        console.log(form.serialize());

        var route = "{{route('expiredorderuttp.export')}}?" + form.serialize();
        location.assign(route);

    });
});

</script>
@endsection