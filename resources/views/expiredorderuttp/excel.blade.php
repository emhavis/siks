
<table id="data_table" class="table table-striped table-hover table-responsive-sm">
    <thead>
        <tr>
            <th>Alat</th>
            <th>Jenis</th>
            <th>Pemilik</th>
            <th>Lokasi</th>
            <th>Koordinat</th>
            <th>No SKHP</th>
            <th>Tgl SKHP</th>
            <th>Tgl Masa Berlaku</th>
            <th>Masa Berlaku (hari)</th>
            <th>Notifikasi</th>
        </tr>
    </thead>
    <tbody>
        @foreach($rows as $row)
        <tr>
            <td>{{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
            <td>{{ $row->uttpType->uttp_type }}</td>
            <td>{{ $row->ServiceRequest->label_sertifikat }}</td>
            <td>{{ $row->location }}</td>
            <td>{{ $row->location_lat }}, {{ $row->location_long }}</td>
            <td>{{ $row->no_sertifikat }}</td>
            <td>{{ date('d-m-Y', strtotime($row->kabalai_date)) }}</td>
            <td>{{ date('d-m-Y', strtotime($row->sertifikat_expired_at)) }}</td>
            <td>@if($row->date_diff >= 60)
                <span class="label label-success">
                @elseif($row->date_diff >= 30)
                <span class="label label-warning">
                @else
                <span class="label label-danger">
                @endif
                {{ (int)$row->date_diff }}</span>
            </td>
            <td>
                {{ $row->expired_notif2 == true ? 'Sudah' : 'Belum' }} Notif 60 Hari<br/>
                {{ $row->expired_notif2 == true ? 'Sudah' : 'Belum' }} Notif 30 Hari
            </td>
        </tr>
        @endforeach                
    </tbody>
</table>