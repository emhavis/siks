@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">

@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="nama">Nama Hari Libur</label> 
                        {!!
                            Form::text("name",$row?$row->name:'',[
                            'class' => 'form-control',
                            'id' => 'name',
                            'placeholder' => 'Hari Libur',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="holiday_date">Tanggal</label> 
                        {!! 
                            Form::text('holiday_date', 
                            $row ? date("d-m-Y", strtotime($row->holiday_date)) : '', 
                            [
                                'class' => 'date form-control',
                                'id' => 'holiday_date', 
                                'placeholder' => 'Tanggal', 
                                'required'
                            ]) 
                        !!}
                    </div>
                    
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>

<script>
$(document).ready(function ()
{
    $('input#holiday_date').datepicker({
      format:"yyyy-mm-dd",
      autoclose:true
    });

    $('#lab_id').select2({
        // placeholder: "- Pilih UML -",
        allowClear: true
    });

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('holiday.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('holiday') }}';
            }
        });

    });
});

</script>
@endsection