@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>QR Code</th>
                        <th>Diterima Oleh</th>
                        <th>Diuji Oleh</th>
                        <th>Disetujui Oleh</th>
                        <th>Diketahui Oleh</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th>Tgl Disetujui</th>
                        <th>Tgl Sertifikasi</th>
                        <th>No. Sertifikat</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($data_list->count())
                    @foreach($data_list as $labout)
                    <tr>
                        <td>{{ $labout->ServiceRequest->no_order }}</td>
                        <td>
                            <a onclick="showToolCodeInfo('{{ $labout->ServiceRequestItem->Standard->tool_code }}')" href="#">{{ $labout->ServiceRequestItem->Standard->tool_code }}</a>
                        </td> 
                        <td>{{ $labout->MasterUsers->full_name }}</td>
                        <td>{{ $labout->LabStaffOut?$labout->LabStaffOut->full_name:"" }}</td>
                        <td>{{ $labout->SupervisorStaff?$labout->SupervisorStaff->full_name:"" }}</td>
                        <td>{{ $labout->DitmetStaff?$labout->DitmetStaff->full_name:"" }}</td>
                        <td>{{ $labout->staff_entry_datein }}</td>
                        <td>{{ $labout->staff_entry_dateout }}</td>
                        <td>{{ $labout->supervisor_entry_date }}</td>
                        <td>{{ $labout->ditmet_entry_date }}</td>
                        <td>{{ $labout->no_sertifikat }}</td>
                        <td>
                        @if($labout->stat_service_order=="2")
                        <a href="{{ route('finished.proses', $labout->id) }}" class="btn btn-warning btn-sm">APPROVE</a>
                        @endif
                        @if($labout->stat_service_order=="3")
                        <a target="_blank" href="{{ route('finished.sertifikat', $labout->id) }}" class="btn btn-info btn-sm" target="_blank">Print Sertifikat</a>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('#data_table').DataTable();
});

</script>
@endsection