@extends('layouts.app')
<style type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        @if($serviceOrder->subkoordinator_notes != null)
        <div class="alert alert-info" role="alert">
            {{ $serviceOrder->subkoordinator_notes }}
        </div>
        @endif

        {!! Form::open(['url' => route('servicerevisionluaruttp.resultupload', $serviceOrder->id), 'files' => true, 'id' => 'form_result'])  !!}
        
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="spuh_spt">No Surat Tugas</label>
                    {!! Form::text('spuh_spt', $serviceOrder->ServiceRequest->spuh_spt,
                        ['class' => 'form-control','id' => 'spuh_spt', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="mulai_uji">Waktu Pengujian, Mulai</label>
                    {!! Form::text('mulai_uji', 
                        date("d-m-Y", strtotime(isset($serviceOrder->mulai_uji) ? $serviceOrder->mulai_uji : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'mulai_uji', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="selesai_uji">Waktu Pengujian, Selesai</label>
                    {!! Form::text('selesai_uji', 
                        date("d-m-Y", strtotime(isset($serviceOrder->selesai_uji) ? $serviceOrder->selesai_uji : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'selesai_uji', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_uttp">Jenis UTTP</label>
                    {!! Form::text('jenis_uttp', 
                        $serviceOrder->uttp != null ? 
                        $serviceOrder->uttp->type->uttp_type :
                        $serviceOrder->ServiceRequestItem->uttp->type->uttp_type, ['class' => 'form-control','id' => 'jenis_uttp', 'readonly']) !!}
                </div>
            </div>
        </div>

        <hr/>

        <h4>Laporan Pelaksanaan Tugas</h4>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="ringkasan">Ringkasan Pelaksanaan Pengujian</label>
                    <textarea name="ringkasan" id="ringkasan"
                        class="form-control">{!! $laporan != null ? $laporan->ringkasan : '' !!}</textarea>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kendala_teknis">Kendala Teknis</label>
                    <textarea name="kendala_teknis" id="kendala_teknis"
                        class="form-control">{!! $laporan != null ? $laporan->kendala_teknis : '' !!}</textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kendala_non_teknis">Kendala Non Teknis</label>
                    <textarea name="kendala_non_teknis" id="kendala_non_teknis"
                            class="form-control">{!! $laporan != null ? $laporan->kendala_non_teknis : '' !!}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="metode_tindakan">Metode/Tindakan yang Dilakukan</label>
                    <textarea name="metode_tindakan" id="metode_tindakan"
                        class="form-control">{!! $laporan != null ? $laporan->metode_tindakan : '' !!}</textarea>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="saran_masukan">Saran/Masukan</label>
                    <textarea name="saran_masukan" id="saran_masukan"
                        class="form-control">{!! $laporan != null ? $laporan->saran_masukan : '' !!}</textarea>
                </div>  
            </div>
        </div>

        <hr/>

        <h4>Hasil Uji</h4>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas Maksimum</label>
                    {!! Form::text('tool_capacity', $serviceOrder->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity' ]) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tool_capacity_min">Kapasitas Minimum</label>
                    {!! Form::text('tool_capacity_min', $serviceOrder->tool_capacity_min, ['class' => 'form-control','id' => 'tool_capacity_min']) !!} 
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Satuan Kapasitas</label>
                    {!! Form::select('tool_capacity_unit', $units, $serviceOrder->tool_capacity_unit, ['class' => 'form-control','id' => 'tool_capacity_unit']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Merek</label>
                    {!! Form::text('tool_brand', $serviceOrder->tool_brand, ['class' => 'form-control','id' => 'tool_brand' ]) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Model/Tipe</label>
                    {!! Form::text('tool_model', $serviceOrder->tool_model, ['class' => 'form-control','id' => 'tool_model']) !!} 
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>No Seri</label>
                    {!! Form::text('tool_serial_no', $serviceOrder->tool_serial_no, ['class' => 'form-control','id' => 'tool_serial_no']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Media Uji/Komoditas</label>
                    {!! Form::text('tool_media', $serviceOrder->tool_media, ['class' => 'form-control','id' => 'tool_media']) !!} 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tool_made_in_id">Buatan</label>
                    {!! Form::select('tool_made_in_id', $negara, $serviceOrder->tool_made_in_id, ['class' => 'form-control','id' => 'tool_made_in_id']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="location_alat">Lokasi Alat</label>
                    {!! Form::text('location_alat', $serviceOrder->location_alat, ['class' => 'form-control','id' => 'location_alat']) !!} 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Koordinat Lokasi Alat (Latitude)</label>
                    {!! Form::text('location_lat', $serviceOrder->location_lat, ['class' => 'form-control','id' => 'location_lat']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Koordinat Lokasi Alat (Longitude)</label>
                    {!! Form::text('location_long', $serviceOrder->location_long, ['class' => 'form-control','id' => 'location_long']) !!} 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="location">Lokasi Pengujian</label>
                    {!! Form::text('location', $serviceOrder->location, ['class' => 'form-control','id' => 'location']) !!} 
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="tool_factory">Nama Pabrikan</label>
                    {!! Form::text('tool_factory', 
                        $serviceOrder->tool_factory,
                        ['class' => 'form-control','id' => 'tool_factory']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="tool_factory_address">Alamat Pabrikan</label>
                    {!! Form::text('tool_factory_address', 
                        $serviceOrder->tool_factory_address,
                        ['class' => 'form-control','id' => 'tool_factory_address']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Dasar Pemeriksaan/Pengujian/Verifikasi, Nomor</label>
                    {!! Form::text('reference_no', 
                        $serviceOrder->ServiceRequestItem->reference_no,
                        ['class' => 'form-control','id' => 'reference_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_date">Dasar Pemeriksaan/Pengujian/Verifikasi, Tanggal</label>
                    {!! Form::text('reference_date', 
                        date("d-m-Y", strtotime(isset($serviceOrder->ServiceRequestItem->reference_date) ? $serviceOrder->ServiceRequestItem->reference_date : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'reference_date', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_1">Diuji Oleh (1)</label>
                    {!! Form::text('test_by_1', 
                        $user->full_name,
                        ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                </div>
            </div>
            @if ($user2 != null)
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_2">Diuji Oleh (2)</label>
                    {!! Form::hidden('test_by_2', 
                        $user2->id) !!}
                    {!! Form::text('test_by_2_name', 
                        $user2->full_name,
                        ['class' => 'form-control','id' => 'test_by_2_name', 'readonly']) !!}
                    <div class="form-check">
                        <input type="checkbox" name="test_by_2_sertifikat" value="test_by_2_sertifikat" id="test_by_2_sertifikat" {{ $serviceOrder->test_by_2_sertifikat ? 'checked' : '' }} />
                        Ditampilkan pada Sertifikat 
                    </div>
                </div>
            </div>
            @endif
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Item Pengujian/Pemeriksaan</label>
                    <?php
                        $inspections = [];
                        foreach($serviceOrder->ServiceRequestItem->inspections as $inspection) {
                            $inspections[] = $inspection->inspectionPrice->inspection_type;
                        }
                    ?>
                    <textarea name="inspections" id="inspections" readonly
                        class="form-control">{!! implode("\n", $inspections) !!}</textarea>
                </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
        @elseif($serviceOrder->ServiceRequest->service_type_id == 4 || $serviceOrder->ServiceRequest->service_type_id == 5)

        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapal">Nama Kapal</label>
                    {!! Form::text('kapal', 
                        $ttu ? $ttu->kapal : '',
                        ['class' => 'form-control','id' => 'kapal']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="catatan_kapasitas">Catatan Kapasitas</label>
                    {!! Form::text('catatan_kapasitas', 
                        $ttu ? $ttu->catatan_kapasitas : '',
                        ['class' => 'form-control','id' => 'catatan_kapasitas']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="panjang">Panjang</label>
                    {!! Form::text('panjang', 
                        $ttu ? $ttu->panjang : '',
                        ['class' => 'form-control','id' => 'panjang']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="lebar">Lebar</label>
                    {!! Form::text('lebar', 
                        $ttu ? $ttu->lebar : '',
                        ['class' => 'form-control','id' => 'lebar']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kedalaman">Kedalaman</label>
                    {!! Form::text('kedalaman', 
                        $ttu ? $ttu->kedalaman : '',
                        ['class' => 'form-control','id' => 'kedalaman']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="user">Pemakai</label>
                    {!! Form::text('user', 
                        $ttu ? $ttu->user : '',
                        ['class' => 'form-control','id' => 'user']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="operator">Operator</label>
                    {!! Form::text('operator', 
                        $ttu ? $ttu->operator : '',
                        ['class' => 'form-control','id' => 'operator']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jumlah_tangki">Jumlah Tangki</label>
                    {!! Form::text('jumlah_tangki', 
                        $ttu ? $ttu->jumlah_tangki : '',
                        ['class' => 'form-control','id' => 'jumlah_tangki']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="checkbox" name="tank_volume_table" value="tank_volume_table" id="tank_volume_table" {{ $ttuCTMS != null && $ttuCTMS->tank_volume_table ? 'checked' : '' }} />
                    <label for="tank_volume_table">Tank Volume Table</label>
                </div>
            </div>
        </div>

        <table id="data_table_ttu_tank" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Instansi</th>
                    <th>No. Sertifikat</th>
                    <th>Tanggal</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <?php
                        $pembuat = $ttuCTMSTank->first(function ($value, $key) {
                            return $value->jenis == 'Pembuat';
                        });
                    ?>
                    <td>Pembuat<input type="hidden" value="Pembuat" name="jenis[]"/></td>
                    <td><input type="text" step="any" class="form-control" name="instansi[]" value="{{ $pembuat ? $pembuat->instansi : '' }}"/></td>
                    <td><input type="text" step="any" class="form-control" name="no_sertifikat[]" value="{{ $pembuat ? $pembuat->no_sertifikat : '' }}"/></td>
                    <td><input type="text" step="any" class="form-control ctms_tgl" name="tanggal[]" value="{{ $pembuat && $pembuat->tanggal != null ? date('d-m-Y', strtotime($pembuat->tanggal)) : '' }}"/></td> 
                </tr>
                <tr>
                    <?php
                        $inspeksi = $ttuCTMSTank->first(function ($value, $key) {
                            return $value->jenis == 'Inspeksi';
                        });
                    ?>
                    <td>Diinspeksi oleh<input type="hidden" value="Inspeksi" name="jenis[]"/></td>
                    <td><input type="text" step="any" class="form-control" name="instansi[]" value="{{ $inspeksi ? $inspeksi->instansi : '' }}"/></td>
                    <td><input type="text" step="any" class="form-control" name="no_sertifikat[]" value="{{ $inspeksi ? $inspeksi->no_sertifikat : '' }}"/></td>
                    <td><input type="text" step="any" class="form-control ctms_tgl" name="tanggal[]" value="{{ $inspeksi && $inspeksi->tanggal != null ? date('d-m-Y', strtotime($inspeksi->tanggal)) : '' }}"/></td> 
                </tr>
                <tr>
                    <?php
                        $verifikasi = $ttuCTMSTank->first(function ($value, $key) {
                            return $value->jenis == 'Verifikasi';
                        });
                    ?>
                    <td>Diverifikasi oleh<input type="hidden" value="Verifikasi" name="jenis[]"/></td>
                    <td><input type="text" step="any" class="form-control" name="instansi[]" value="{{ $verifikasi ? $verifikasi->instansi : '' }}"/></td>
                    <td><input type="text" step="any" class="form-control" name="no_sertifikat[]" value="{{ $verifikasi ? $verifikasi->no_sertifikat : '' }}"/></td>
                    <td><input type="text" step="any" class="form-control ctms_tgl" name="tanggal[]" value="{{ $verifikasi && $verifikasi->tanggal != null ? date('d-m-Y', strtotime($verifikasi->tanggal)) : '' }}"/></td> 
                </tr>
            <tbody>
        </table>

        @if($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'CTMS Gas') 
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="checkbox" name="radar_level_gauge" value="radar_level_gauge" id="radar_level_gauge" {{ $ttuCTMS != null && $ttuCTMS->radar_level_gauge ? 'checked' : '' }} />
                    <label for="radar_level_gauge">Radar Level Gauge</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="rlg_no_sertifikat">No Sertifikat</label>
                    {!! Form::text('rlg_no_sertifikat', 
                        $ttuCTMSPerlengkapan_RLG ? $ttuCTMSPerlengkapan_RLG->no_sertifikat : '',
                        ['class' => 'form-control','id' => 'rlg_no_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="rlg_merek">Merek</label>
                    {!! Form::text('rlg_merek', 
                        $ttuCTMSPerlengkapan_RLG ? $ttuCTMSPerlengkapan_RLG->merek : '',
                        ['class' => 'form-control','id' => 'rlg_merek']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="rlg_tipe">Tipe</label>
                    {!! Form::text('rlg_tipe', 
                        $ttuCTMSPerlengkapan_RLG ? $ttuCTMSPerlengkapan_RLG->tipe : '',
                        ['class' => 'form-control','id' => 'rlg_tipe']) !!}
                </div>
            </div>
        </div>
        <table id="data_table_ttu_rlg" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Tank</th>
                    <th>No. Seri</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuCTMSGauge_RLG as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="rlg_tank[]" value="{{ $ttuItem->tank }}"/></td>
                    <td><input type="text" class="form-control" name="rlg_serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="rlg_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="checkbox" name="capacitance_level_gauge" value="capacitance_level_gauge" id="capacitance_level_gauge" {{ $ttuCTMS != null && $ttuCTMS->capacitance_level_gauge ? 'checked' : '' }} />
                    <label for="capacitance_level_gauge">Capacitance Level Gauge</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="clg_no_sertifikat">No Sertifikat</label>
                    {!! Form::text('clg_no_sertifikat', 
                        $ttuCTMSPerlengkapan_CLG ? $ttuCTMSPerlengkapan_CLG->no_sertifikat : '',
                        ['class' => 'form-control','id' => 'clg_no_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="clg_merek">Merek</label>
                    {!! Form::text('clg_merek', 
                        $ttuCTMSPerlengkapan_CLG ? $ttuCTMSPerlengkapan_CLG->merek : '',
                        ['class' => 'form-control','id' => 'clg_merek']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="clg_tipe">Tipe</label>
                    {!! Form::text('clg_tipe', 
                        $ttuCTMSPerlengkapan_CLG ? $ttuCTMSPerlengkapan_CLG->tipe : '',
                        ['class' => 'form-control','id' => 'clg_tipe']) !!}
                </div>
            </div>
        </div>
        <table id="data_table_ttu_clg_main" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="3">Electrode (Main)</th>
                </tr>
                <tr>
                    <th>Tank</th>
                    <th>No. Seri (Segmen)</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuCTMSGauge_CLG_Main as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="clg_main_tank[]" value="{{ $ttuItem->tank }}"/></td>
                    <td><input type="text" class="form-control" name="clg_main_serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="clg_main_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <table id="data_table_ttu_clg_backup" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="3">Electrode (Backup)</th>
                </tr>
                <tr>
                    <th>Tank</th>
                    <th>No. Seri (Segmen)</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuCTMSGauge_CLG_Backup as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="clg_backup_tank[]" value="{{ $ttuItem->tank }}"/></td>
                    <td><input type="text" class="form-control" name="clg_backup_serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="clg_backup_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="checkbox" name="float_level_gauge" value="float_level_gauge" id="float_level_gauge" {{ $ttuCTMS != null && $ttuCTMS->float_level_gauge ? 'checked' : '' }} />
                    <label for="float_level_gauge">Float Level Gauge</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="flg_no_sertifikat">No Sertifikat</label>
                    {!! Form::text('flg_no_sertifikat', 
                        $ttuCTMSPerlengkapan_FLG ? $ttuCTMSPerlengkapan_FLG->no_sertifikat : '',
                        ['class' => 'form-control','id' => 'flg_no_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="flg_merek">Merek</label>
                    {!! Form::text('flg_merek', 
                        $ttuCTMSPerlengkapan_FLG ? $ttuCTMSPerlengkapan_FLG->merek : '',
                        ['class' => 'form-control','id' => 'flg_merek']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="flg_tipe">Tipe</label>
                    {!! Form::text('flg_tipe', 
                        $ttuCTMSPerlengkapan_FLG ? $ttuCTMSPerlengkapan_FLG->tipe : '',
                        ['class' => 'form-control','id' => 'flg_tipe']) !!}
                </div>
            </div>
        </div>
        <table id="data_table_ttu_flg" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Tank</th>
                    <th>No. Seri</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuCTMSGauge_FLG as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="flg_tank[]" value="{{ $ttuItem->tank }}"/></td>
                    <td><input type="text" class="form-control" name="flg_serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="flg_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="checkbox" name="pressure_measuring_systems" value="pressure_measuring_systems" id="pressure_measuring_systems" {{ $ttuCTMS != null && $ttuCTMS->pressure_measuring_systems ? 'checked' : '' }} />
                    <label for="pressure_measuring_systems">Pressure Measuring Systems</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pms_no_sertifikat">No Sertifikat</label>
                    {!! Form::text('pms_no_sertifikat', 
                        $ttuCTMSPerlengkapan_PMS ? $ttuCTMSPerlengkapan_PMS->no_sertifikat : '',
                        ['class' => 'form-control','id' => 'pms_no_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pms_merek">Merek</label>
                    {!! Form::text('pms_merek', 
                        $ttuCTMSPerlengkapan_PMS ? $ttuCTMSPerlengkapan_PMS->merek : '',
                        ['class' => 'form-control','id' => 'pms_merek']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pms_tipe">Tipe</label>
                    {!! Form::text('pms_tipe', 
                        $ttuCTMSPerlengkapan_PMS ? $ttuCTMSPerlengkapan_PMS->tipe : '',
                        ['class' => 'form-control','id' => 'pms_tipe']) !!}
                </div>
            </div>
        </div>
        <table id="data_table_ttu_pms" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Tank</th>
                    <th>No. Seri</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuCTMSGauge_PMS as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="pms_tank[]" value="{{ $ttuItem->tank }}"/></td>
                    <td><input type="text" class="form-control" name="pms_serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="pms_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="checkbox" name="temperature_measuring_systems" value="temperature_measuring_systems" id="temperature_measuring_systems" {{ $ttuCTMS != null && $ttuCTMS->temperature_measuring_systems ? 'checked' : '' }} />
                    <label for="temperature_measuring_systems">Temperature Measuring Systems</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tms_no_sertifikat">No Sertifikat</label>
                    {!! Form::text('tms_no_sertifikat', 
                        $ttuCTMSPerlengkapan_TMS ? $ttuCTMSPerlengkapan_TMS->no_sertifikat : '',
                        ['class' => 'form-control','id' => 'tms_no_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tms_merek">Merek</label>
                    {!! Form::text('tms_merek', 
                        $ttuCTMSPerlengkapan_TMS ? $ttuCTMSPerlengkapan_TMS->merek : '',
                        ['class' => 'form-control','id' => 'tms_merek']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tms_tipe">Tipe</label>
                    {!! Form::text('tms_tipe', 
                        $ttuCTMSPerlengkapan_TMS ? $ttuCTMSPerlengkapan_TMS->tipe : '',
                        ['class' => 'form-control','id' => 'tms_tipe']) !!}
                </div>
            </div>
        </div>

        <table id="data_table_ttu_tms_main" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="3">Temperature Sensor (Main)</th>
                </tr>
                <tr>
                    <th>Tank</th>
                    <th>No. Seri (Section)</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuCTMSGauge_TMS_Main as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="tms_main_tank[]" value="{{ $ttuItem->tank }}"/></td>
                    <td><input type="text" class="form-control" name="tms_main_serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="tms_main_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <table id="data_table_ttu_tms_backup" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="3">Temperature Sensor (Backup)</th>
                </tr>
                <tr>
                    <th>Tank</th>
                    <th>No. Seri (Section)</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuCTMSGauge_TMS_Backup as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="tms_backup_tank[]" value="{{ $ttuItem->tank }}"/></td>
                    <td><input type="text" class="form-control" name="tms_backup_serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="tms_backup_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="checkbox" name="trim_list_inclinometer" value="trim_list_inclinometer" id="trim_list_inclinometer" {{ $ttuCTMS != null && $ttuCTMS->trim_list_inclinometer ? 'checked' : '' }} />
                    <label for="trim_list_inclinometer">Trim/List Inclinometer</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tli_no_sertifikat">No Sertifikat</label>
                    {!! Form::text('tli_no_sertifikat', 
                        $ttuCTMSPerlengkapan_TLI ? $ttuCTMSPerlengkapan_TLI->no_sertifikat : '',
                        ['class' => 'form-control','id' => 'tli_no_sertifikat']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tli_serial_no">No Seri</label>
                    {!! Form::text('tli_serial_no', 
                        $ttuCTMSPerlengkapan_TLI ? $ttuCTMSPerlengkapan_TLI->serial_no : '',
                        ['class' => 'form-control','id' => 'tli_serial_no']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tli_merek">Merek</label>
                    {!! Form::text('tli_merek', 
                        $ttuCTMSPerlengkapan_TLI ? $ttuCTMSPerlengkapan_TLI->merek : '',
                        ['class' => 'form-control','id' => 'tli_merek']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tli_tipe">Tipe</label>
                    {!! Form::text('tli_tipe', 
                        $ttuCTMSPerlengkapan_TLI ? $ttuCTMSPerlengkapan_TLI->tipe : '',
                        ['class' => 'form-control','id' => 'tli_tipe']) !!}
                </div>
            </div>
        </div>
                
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'CTMS BBM') 

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="checkbox" name="uti_meter" value="uti_meter" id="uti_meter" {{ $ttuCTMS != null && $ttuCTMS->uti_meter ? 'checked' : '' }} />
                    <label for="uti_meter">Ullage Temperature Interface (UTI) Meter</label>
                </div>
            </div>
        </div>

        <table id="data_table_ttu_uti" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>No. Sertifikat</th>
                    <th>Merek</th>
                    <th>Tipe</th>
                    <th>No. Seri</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuCTMSPerlengkapan_UTI as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="uti_no_sertifikat[]" value="{{ $ttuItem->no_sertifikat }}"/></td>
                    <td><input type="text" class="form-control" name="uti_merek[]" value="{{ $ttuItem->merek }}"/></td> 
                    <td><input type="text" class="form-control" name="uti_tipe[]" value="{{ $ttuItem->tipe }}"/></td> 
                    <td><input type="text" class="form-control" name="uti_serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="uti_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="checkbox" name="depth_tape" value="depth_tape" id="depth_tape" {{ $ttuCTMS != null && $ttuCTMS->depth_tape ? 'checked' : '' }} />
                    <label for="depth_tape">Depth Tape</label>
                </div>
            </div>
        </div>

        <table id="data_table_ttu_dt" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>No. Sertifikat</th>
                    <th>Merek</th>
                    <th>Tipe</th>
                    <th>No. Seri</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuCTMSPerlengkapan_DT as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="dt_no_sertifikat[]" value="{{ $ttuItem->no_sertifikat }}"/></td>
                    <td><input type="text" class="form-control" name="dt_merek[]" value="{{ $ttuItem->merek }}"/></td> 
                    <td><input type="text" class="form-control" name="dt_tipe[]" value="{{ $ttuItem->tipe }}"/></td> 
                    <td><input type="text" step="any" class="form-control" name="dt_serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="dt_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="checkbox" name="sistem_meter_arus_kerja" value="sistem_meter_arus_kerja" id="sistem_meter_arus_kerja" {{ $ttuCTMS != null && $ttuCTMS->sistem_meter_arus_kerja ? 'checked' : '' }} />
                    <label for="sistem_meter_arus_kerja">Sistem Meter Arus Kerja</label>
                </div>
            </div>
        </div>

        <table id="data_table_ttu_sistem" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th rowspan="2">No. Sertifikat</th>
                    <th colspan="4">Nomor Seri</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>Meter Arus</th>
                    <th>Pressure Temperature</th>
                    <th>Temperature Transmitter</th>
                    <th>Flow Computer</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuCTMSSistem as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="sistem_no_sertifikat[]" value="{{ $ttuItem->no_sertifikat }}"/></td>
                    <td><input type="text" class="form-control" name="sistem_serial_no_ma[]" value="{{ $ttuItem->serial_no_ma }}"/></td> 
                    <td><input type="text" class="form-control" name="sistem_serial_no_pt[]" value="{{ $ttuItem->serial_no_pt }}"/></td> 
                    <td><input type="text" class="form-control" name="sistem_serial_no_tt[]" value="{{ $ttuItem->serial_no_tt }}"/></td> 
                    <td><input type="text" ßclass="form-control" name="sistem_serial_no_fc[]" value="{{ $ttuItem->serial_no_fc }}"/></td> 
                    <td>
                        <input type="hidden" name="sistem_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @endif

        @if($serviceOrder->ServiceRequest->lokasi_pengujian == 'luar' && $serviceOrder->ServiceRequestItem->location != null)
        @if(count($serviceOrder->ttuPerlengkapans) > 0)
        <h5>Perlengkapan</h5>
        <div class="row">
            <div class="col-md-12">
                <table id="perlengkapan" class="table table-responsive-sm input-table">
                    <thead>
                        <tr>
                            <th>Jenis</th>
                            <th>Merek</th>
                            <th>Model/Tipe</th>
                            <th>Nomor Seri</th>
                            <th>Tag No</th>
                            <th>Keterangan/Penyegelan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($serviceOrder->ttuPerlengkapans as $perlengkapan)
                        <tr>
                            <td>{{ $perlengkapan->uttp->type->uttp_type }}</td>
                            <td>{{ $perlengkapan->uttp->tool_brand }}</td>
                            <td>{{ $perlengkapan->uttp->tool_model }}</td>
                            <td>{{ $perlengkapan->uttp->serial_no }}</td>
                            <td>
                                <input type="text" class="form-control" name="perlangkapan_tag[]" 
                                    value="{{ $perlengkapan->tag }}"/>
                            </td>
                            <td>
                                <input type="hidden" name="perlengkapan_id[]" />
                                <input type="hidden" name="perlengkapan_uttp_id[]" value="{{ $perlengkapan->uttp_id }}"/>
                                <input type="text" class="form-control" name="perlangkapan_keterangan[]" 
                                    value="{{ $perlengkapan->keterangan }}"/>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @endif

        @endif

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sertifikat_expired_at">Masa Berlaku (klik untuk mengubah)</label>
                    {!! Form::text('sertifikat_expired_at', 
                        date("d-m-Y", strtotime(isset($serviceOrder->sertifikat_expired_at) ? $serviceOrder->sertifikat_expired_at : $sertifikat_expired_at)),
                        ['class' => 'form-control','id' => 'sertifikat_expired_at', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="hasil_uji_memenuhi">Hasil Uji *</label>
                    {!! Form::select('hasil_uji_memenuhi', ['' => '', 'sah'=>'Sah','batal'=>'Batal'], 
                        ($serviceOrder->hasil_uji_memenuhi != null ? ($serviceOrder->hasil_uji_memenuhi ? 'sah' : 'batal') : ''), 
                        ['class' => 'form-control select2','id' => 'hasil_uji_memenuhi', 'style' => 'width:100%']) !!}
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tanda_pegawai_berhak">Tanda Pegawai Berhak</label>
                    {!! Form::text('tanda_pegawai_berhak', $serviceOrder->tanda_pegawai_berhak,
                        ['class' => 'form-control','id' => 'tanda_pegawai_berhak']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tanda_daerah">Tanda Daerah</label>
                    {!! Form::text('tanda_daerah', $serviceOrder->tanda_daerah,
                        ['class' => 'form-control','id' => 'tanda_daerah']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tanda_sah">Tanda Sah</label>
                    {!! Form::text('tanda_sah', $serviceOrder->tanda_sah,
                        ['class' => 'form-control','id' => 'tanda_sah']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="persyaratan_teknis">Persyaratan Teknis</label>
                    {!! Form::hidden('persyaratan_teknis_id', 
                        $serviceOrder->ServiceRequestItem->uttp->type->oiml_id,
                        ['class' => 'form-control','id' => 'persyaratan_teknis_id', 'readonly']) !!}
                    <textarea name="persyaratan_teknis_id_str" id="persyaratan_teknis_id_str" readonly
                        class="form-control">{!! $serviceOrder->ServiceRequestItem->uttp->type->syarat_teknis !!}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="file_skhp">Lampiran Evaluasi *</label>
                    {!! Form::file('file_skhp', 
                        ['class' => 'form-control','id' => 'file_skhp', 'accept'=> 'application/pdf,image/png, image/jpeg']) !!}
                </div>
            </div>
            <div class="cal-md-6">
                @if($serviceOrder->path_skhp !=null)
                <div class="input-group">
                    <label style="padding-top:30px"></label>
                    <a href="{{ route('serviceuttp.download', $serviceOrder->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                    <label>&nbsp; Nama File:  {{$serviceOrder->file_skhp}}</label>
                </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="catatan_hasil_pemeriksaan">Catatan (diisi jika batal atau jika terdapat catatan atas hasil pemeriksaan)</label>
                    <textarea name="catatan_hasil_pemeriksaan" id="catatan_hasil_pemeriksaan"
                        class="form-control">{!! $serviceOrder->catatan_hasil_pemeriksaan !!}</textarea>
                </div>
            </div>
        </div>

        <input type="hidden" name="stat_sertifikat" id="stat_sertifikat" value="0"/>
        <button class="btn btn-w-md btn-accent" id="btn_simpan">Simpan Hasil Uji</button>

        @if($serviceOrder->preview_count > 0)
        <button class="btn btn-w-md btn-success" id="btn_simpan_submit">Kirim Hasil Uji</button>
        @else
        <br/>
        <span>Pastikan untuk me-review draft sebelum mengirim hasil uji</span>
        @endif

        {!! Form::close() !!}
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/jquery-validation/additional-methods.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">

    $(document).ready(function ()
    {
        $('.select2, #hasil_uji_memenuhi').select2({
            allowClear: true
        });

        $('#data_table_ttu_rlg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_rlg");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="rlg_tank[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="rlg_serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="rlg_item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_rlg').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_clg_main').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_clg_main");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="clg_main_tank[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="clg_main_serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="clg_main_item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_clg_main').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_clg_backup').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_clg_backup");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="clg_backup_tank[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="clg_backup_serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="clg_backup_item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_clg_backup').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_flg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_flg");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="flg_tank[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="flg_serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="flg_item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_flg').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_pms').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_pms");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="pms_tank[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="pms_serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="pms_item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_pms').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_tms_main').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_tms_main");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="tms_main_tank[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="tms_main_serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="tms_main_item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_tms_main').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_tms_backup').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_tms_backup");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="tms_backup_tank[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="tms_backup_serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="tms_backup_item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_tms_backup').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_uti').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_uti");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="uti_no_sertifikat[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="uti_merek[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="uti_tipe[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="uti_serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="uti_item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_uti').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_dt').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_dt");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="dt_no_sertifikat[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="dt_merek[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="dt_tipe[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="dt_serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="dt_item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_dt').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_sistem').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_sistem");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="sistem_no_sertifikat[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="sistem_serial_no_ma[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="sistem_serial_no_pt[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="sistem_serial_no_tt[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="sistem_serial_no_fc[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="sistem_item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_sistem').on("click","button#btn_remove",function()
        {
            $(this).closest("tr").remove();
        });

        @if($serviceOrder->ServiceRequest->service_type_id == 4 || $serviceOrder->ServiceRequest->service_type_id == 5)
        $('input#sertifikat_expired_at').datepicker({
            format:"dd-mm-yyyy",
            autoclose:true,
            startDate: new Date("{{ $serviceOrder->staff_entry_datein }}"),
        });
        $('input.ctms_tgl').datepicker({
            format:"dd-mm-yyyy",
            autoclose:true,
        });
        @endif

        $("#btn_simpan").click(function(e){
            e.preventDefault();

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            $("#stat_sertifikat").val(1);

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        jQuery.validator.addMethod("require_from_group", function (value, element, options) {
            var numberRequired = options[0];
            var selector = options[1];
            var fields = $(selector, element.form);
            var filled_fields = fields.filter(function () {
                // it's more clear to compare with empty string
                return $(this).val() != "";
            });
            var empty_fields = fields.not(filled_fields);
            // we will mark only first empty field as invalid
            if (filled_fields.length < numberRequired && empty_fields[0] == element) {
                return false;
            }
            return true;
            // {0} below is the 0th item in the options field
        }, "Salah satu dari data ini harus diisi");


        $("#form_result").validate({
            rules: {
                @if($serviceOrder->path_skhp == null)
                file_skhp: {
                    required: true,
                    accept: "application/pdf,image/png, image/jpeg"
                },
                @else
                file_skhp: {
                    accept: "application/pdf,image/png, image/jpeg"
                },
                @endif
                hasil_uji_memenuhi: {
                    required: true,
                    minlength: 1,
                },
            },
            messages: {
                file_skhp: 'File lampiran harus diupload dalam format pdf, png atau jpeg',
                hasil_uji_memenuhi: 'Hasil uji harus dipilih',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });

    });

</script>
@endsection