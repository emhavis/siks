@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>QR Code</th>
                        <th>Tgl Order</th>
                        <th>Tgl Proses Lab.</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequest->no_order }}</td>
                        <td>
                            @if($row->Standard)
                            <a onclick="showToolCodeInfo({{ $row->Standard->tool_code }})" href="#">{{ $row->Standard->tool_code }}</a>
                            @else
                            -
                            @endif
                        </td>
                        <td>{{ $row->ServiceRequest->receipt_date }}</td>
                        <td>
                        <a href="{{ route('order.proses',[$row->id,$row->ServiceRequest->id]) }}" class="btn btn-warning btn-sm">PROSES</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $('#data_table').DataTable();
});

</script>
@endsection