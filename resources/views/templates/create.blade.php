@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>        
        <div class="panel-heading">
            <div class="panel-body">
                {!! Form::open(array('route' => 'standard.store','id' => 'form_create_standard', 'enctype' => "multipart/form-data")) !!}
                    <div class="form-group">
                        <label for="sumber_id">Laboratorium</label>
                        {!! Form::select('laboratory_id', $laboratories, null,
                            ['class' => 'form-control select2',
                             'id' => 'laboratory_id',
                             'placeholder' => '- Pilih Laboratorium -',
                             'required']) !!}

                    </div>
                    <div class="form-group">
                        <label for="brand">Nama Uji</label>
                        {!! Form::text('nama_uji', null,
                                       ['class' => 'form-control',
                                        'id' => 'nama_uji',
                                        'placeholder' => 'Nama Uji',
                                        'required']) !!}
                    </div>

                    <button type="button" id="btn_cancel" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</button>
                    <button type="button" id="btn_submit" class="btn btn-warning"><i class="fa fa-save"></i> Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.min.js') }}"></script>
<script type='text/javascript'>

var el = new Object();

$(document).ready(function()
{
    $("#btn_submit").click(function(e)
    {
        e.preventDefault();
        $("#alert_board").empty();

        var form_object = $("#form_create_standard");
        var form_data = $("#form_create_standard").serialize();

        $("#panel_create").toggleClass("ld-loading");

        $.post('',form_data,function(response){
            if(response[0]==false)
            {
                var msg = show_notice(response[1]);
                $("#alert_board").html(msg);
                $("#panel_create").toggleClass("ld-loading");
            }
            else
            {
                window.location = '';
            }
        },"json");
    });

});

</script>

@endsection
