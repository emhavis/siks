@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled col-12" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading" >
            <h4>Artikel/Posting</h4>
        </div>
        <div class="panel-body">
            <form id="createGroup" action="{{ route('article.update', $row->id )}}" method="POST" enctype="multipart/form-data">
            
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="kelompok">Judul</label> 
                            {!!
                                Form::text("title", $row->title, [
                                'class' => 'form-control',
                                'id' => 'title',
                                'placeholder' => 'Judul',
                                'required'
                                ]);
                            !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="cover_image">Gambar Cover</label> 
                            {!! Form::file('cover_image', null,
                                ['class' => 'form-control','id' => 'cover_image']) !!}    
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="content">Ringkasan</label> 
                            {!!
                                Form::textarea("excerpt", $row->excerpt,[
                                'class' => 'form-control',
                                'id' => 'excerpt',
                                'placeholder' => 'Ringkasan',
                                'required'
                                ]);
                            !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="jenis_pengujian">Isi Artikel</label> 
                            {!!
                                Form::textarea("content", $row->content,[
                                'class' => 'form-control',
                                'id' => 'content',
                                'placeholder' => 'Isi Artikel',
                                'required'
                                ]);
                            !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="kelompok">Dipublikasikan?</label> 
                            {!!
                                Form::checkbox("published", null ,$row->published);
                            !!}
                        </div>
                    </div>
                </div>

                
                <button type="submit" id="submit" class="btn btn-default">Submit</button>
            </form> 
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}"></script>
<script>
$(document).ready(function ()
{
    CKEDITOR.replace('content');
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection