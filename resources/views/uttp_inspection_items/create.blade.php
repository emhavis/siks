@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row col-md-6">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="role_name">Nama Inspeksi</label> 
                        {!!
                            Form::text("name",$row?$row->name:'',[
                            'class' => 'form-control',
                            'id' => 'name',
                            'placeholder' => 'Inspection Name',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="role_name">Nomor Order</label> 
                        {!!
                            Form::text("order_no",$row?$row->order_no:'',[
                            'class' => 'form-control',
                            'id' => 'order_no',
                            'placeholder' => 'Nomor Order',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="role_name">Nomor</label> 
                        {!!
                            Form::text("no",$row?$row->no:'',[
                            'class' => 'form-control',
                            'id' => 'no',
                            'placeholder' => 'Nomor',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="template_id">Template</label> 
                        {!!
                            Form::select("template_id",
                            $templates,
                            $row?$row->template_id:'',[
                            'class' => 'form-control',
                            'id' => 'template_id',
                            'placeholder' => 'Template ',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        
                    </div>
                    <div class="form-group col-md-12">
                        <div class="row col-12">
                            <div class="col-6">
                                <label for="role_name">Jadikan Header ?</label>    &nbsp; &nbsp; 
                                {!!
                                    Form::checkbox("is_header",$row?$row->is_header:'',[
                                    'class' => 'form-control',
                                    'id' => 'is_header',
                                    'placeholder' => 'Jadikan Header',
                                    'required'
                                    ]);
                                !!}
                            </div>
                            <div class="col-6">
                                <label for="role_name">Sudah dites ?</label>   &nbsp; &nbsp;  &nbsp; &nbsp;&nbsp; &nbsp; 
                                {!!
                                    Form::checkbox("is_tested",$row?$row->is_tested:'',[
                                    'class' => 'form-control',
                                    'id' => 'is_tested',
                                    'placeholder' => 'Sudah melewati tes',
                                    'required'
                                    ]);
                                !!}
                            </div>
                        </div>
                    </div>
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function ()
{
    $('#template_id').select2({
    });
    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('insitems.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('insitems') }}';
            }
        });

    });
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection