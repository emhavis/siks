@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <a href="{{ route('insitems.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUser" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Nomor Order</th>
                        <th>Nomor</th>
                        <th>Nama Grup</th>
                        <th>Template</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($data as $row)
                    <tr>
                        <td>{{ $row->order_no }}</td>
                        <td>{{ $row->no }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->title_tmp}}</td>
                        <td>{{ $row->template_id}}</td>
                        <td>
                            <a title="ubah" href="{{ route('insitems.edit')}}" class="btn btn-accent btn-sm" style="margin-right:2px;">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="#" data-id="{{$row->id}}" 
                            class="btn btn-accent btn-sm delete btn-hapus" 
                                data-toggle="modal" 
                                data-target="#deleteModal"> <i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>        
    </div>
    </div>
</div>
<!-- Delete Warning Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('insitems.delete', 'id') }}" method="get">
                <div class="modal-body">
                    @csrf
                    @method('DELETE')
                    <h5 class="text-center">Are you sure you want to delete this data?</h5></br>
                    <input type="hidden" id="id" name="id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-accent btn-sm">Yes, Delete Contact</button>
                </div>
            </form>
        </div>
    </div>
</div>
        <!-- End Delete Modal --> 
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(function() {
        $('.btn-hapus').click(function(){
            let strid = "idDelete";
            let id = $(this).attr('data-id');
            strid.replace(strid, id);
            console.log(this);
            $('#id').val($(this).attr('data-id'));
            $('#confirmModal').modal('show');
        });
    });
</script> 
@endsection