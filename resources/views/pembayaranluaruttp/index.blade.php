@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<style type"text/css"> 
    .badge-notif-tab {
            position:relative;
    }
    .badge-notif-tab[data-badge]:after {
            content:attr(data-badge);
            position:absolute;
            top:-10px;
            right:-5px;
            font-size:.7em;
            background:#f3a709;
            color:white;
            width:18px;
            height:18px;
            text-align:center;
            line-height:18px;
            border-radius: 50%;
    }
</style>
@endsection

@section('content')
<div class="row">
    <!-- <a href="{{ route('requestuttp.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Pendaftaran</a> -->
    <!-- <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a> -->

    <ul class="nav nav-tabs" role="tablist" id="tabs">
        
        <li role="presentation" class="active">
            <a href="#frontdesk_penagihan_luar" aria-controls="frontdesk_penagihan_luar" role="tab" data-toggle="tab"
                class="badge-notif-tab" 
                <?php if ((count($rows_penagihan_luar)) > 0 ){ ?> 
                    data-badge="{{count($rows_penagihan_luar)}}" 
                <?php }else{ } ?>
            >Invoice PNBP</a>
        </li>
        <li role="presentation" >
            <a href="#frontdesk_pembayaran_luar" aria-controls="frontdesk_pembayaran_luar" role="tab" data-toggle="tab"
                class="badge-notif-tab" 
                <?php if ((count($rows_pembayaran_luar)) > 0 ){ ?> 
                    data-badge="{{count($rows_pembayaran_luar)}}" 
                <?php }else{ } ?>
            >Dalam Pembayaran</a>
        </li>
        <li role="presentation">
            <a href="#frontdesk_validasi_luar" aria-controls="frontdesk_validasi_luar" role="tab" data-toggle="tab"
                class="badge-notif-tab" 
                <?php if ((count($rows_validasi_luar)) > 0 ){ ?> 
                    data-badge="{{count($rows_validasi_luar)}}" 
                <?php }else{ } ?>
            >Validasi PNBP</a>
        </li>       
    </ul>
    <div class="tab-content" id="nav-tabContent">
        
        <div role="tabpanel" class="tab-pane active" id="frontdesk_penagihan_luar">
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <table id="table_data_penagihan_luar" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Lokasi DL</th>
                                <th>Lokasi, Provinsi/Negara</th>
                                <th>Penguji/Pemeriksa</th>
                                <th>Waktu Pengujian</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_penagihan_luar as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->no_order }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->lokasi_dl == 'luar' ? 'Luar Negeri' : 'Dalam Negeri' }}</td>
                                <td>{{ $row->lokasi_dl == 'luar' ? $row->inspectionNegara->nama_negara : $row->inspectionProv->nama }}</td>
                                <td>
                                    @foreach($row->spuhDoc->listOfStaffs as $staf)
                                    @if($staf->scheduledStaff != null)
                                    {{ $staf->scheduledStaff->nama }}</br>
                                    @endif
                                    @endforeach
                                </td>
                                <td>{{ date("d-m-Y", strtotime($row->spuhDoc->date_from)) }} s/d {{ date("d-m-Y", strtotime($row->spuhDoc->date_to)) }}</td>
                                <td>
                                    @if($row->payment_status_id == 5 && $row->billing_to_date == null)
                                    <a href="{{ route('requestluaruttp.editpnbp', $row->id) }}" class="btn btn-warning btn-sm">Edit PNBP</a>
                                    <a target="_blank" href="{{ route('requestluaruttp.pdf', $row->id) }}" class="btn btn-warning btn-sm">Draft Invoice PNBP</a>
                                    @endif
                                    @if($row->payment_status_id == 5 && $row->total_price > 0 && $row->billing_to_date == null)
                                    <a href="{{ route('requestluaruttp.payment', $row->id) }}"
                                        class="btn btn-warning btn-sm btn-mdl" >Billing PNBP</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="frontdesk_pembayaran_luar">
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <table id="table_data_pembayaran_luar" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Lokasi DL</th>
                                <th>Lokasi, Provinsi</th>
                                <th>Waktu Pengujian</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_pembayaran_luar as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->no_order }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->lokasi_dl == 'luar' ? 'Luar Negeri' : 'Dalam Negeri' }}</td>
                                <td>{{ $row->lokasi_dl == 'luar' ? $row->inspectionNegara->nama_negara : $row->inspectionProv->nama }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->received_date)) }} s/d {{ date("d-m-Y", strtotime($row->estimated_date)) }}</td>
                                <td>
                                    @if($row->payment_status_id == 6 && $row->billing_to_date != null)
                                    <a target="_blank" href="{{ route('requestluaruttp.pdf', $row->id) }}" class="btn btn-warning btn-sm">Invoice PNBP</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="frontdesk_validasi_luar">
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <table id="table_data_validasi_luar" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Lokasi DL</th>
                                <th>Lokasi, Provinsi</th>
                                <th>Waktu Pengujian</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_validasi_luar as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->lokasi_dl == 'luar' ? 'Luar Negeri' : 'Dalam Negeri' }}</td>
                                <td>{{ $row->lokasi_dl == 'luar' ? $row->inspectionNegara->nama_negara : $row->inspectionProv->nama }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->received_date)) }} s/d {{ date("d-m-Y", strtotime($row->estimated_date)) }}</td>
                                <td>
                                    @if($row->payment_status_id == 7 && $row->total_price > 0 && $row->payment_date != null && $row->payment_valid_date == null)
                                    <a href="{{ route('requestluaruttp.valid', $row->id) }}"
                                        class="btn btn-warning btn-sm btn-mdl" >Validasi PNBP</a>
                                    @endif
                                    
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
</div>

<div class="modal" tabindex="-1" role="dialog" id="invoiceDelete">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Yakin menghapus data ini ?</p>
        <input type="hidden" name="ReqInvid" id="ReqInvid"/>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary">Ya</button> -->
        {!! Form::button('Hapus', ['class' => 'btn btn-warning btn-sm btn-hapus-inv', 'id' => 'invHapus','name' => 'invHapus', 'value' => 'Hapus',
            'data-toggle' => "modal", 'data-target' => "#invoiceDelete"]) !!}
        <button type="button" class="btn btn-secondary" id="batalHapus" data-dismiss="modal">Tidak</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penagihan_luar,#table_data_validasi_luar,#table_data_pembayaran_luar").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });
        $('#table_data tbody').on( 'click', 'button.btn-simpan', function (e) {
        //$('.btn-simpan').click(function(e){
            e.preventDefault();

            $(this).attr('disabled', true);
            toastr["warning"]("Konfirmasi booking sedang diproses","Form Valid");
  
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {

                $(this).attr('disabled', false);

                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon periksa kembali","Form Invalid");
                }
            });

        });

        $('#table_data_booking_luar tbody').on( 'click', 'button.btn-simpan-luar', function (e) {
        //$('.btn-simpan').click(function(e){
            e.preventDefault();

            $(this).attr('disabled', true);
            toastr["warning"]("Konfirmasi booking sedang diproses","Form Valid");
  
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {

                $(this).attr('disabled', false);

                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon periksa kembali","Form Invalid");
                }
            });

        });

        $('.btn-hapus').click(function(e){
            $(this).hide();
        });
        $("#table_data_pendaftaran tbody").on("click","button.hapus",function(e){
            <?php $id; ?>
            console.log('ini di klick');
            e.preventDefault();
            var iddelete = $(this).data().id;
            var noreg = $(this).data().noreg;
            $('#noregistrasi').val(noreg);
            console.log(iddelete);
            
            $("#deletemodal").modal().on('shown.bs.modal', function ()
            {
                $('#no_registrasi').val(noreg);

                var route = "{{ route('requestuttp.destroy', ':id') }}";
                route = route.replace(':id', iddelete);

                $('#hapus').click(function(){
                    $.get(route,function(response)
                    {
                        
                        console.log(response.status)
                        console.log('{{ route('requestuut.destroy','id') }}');
                        if(response.status == true){
                            $("#deletemodal").modal().hide();
                            toastr["success"](response.messages,"Form Status : Ok");
                            location.reload();
                        }else{
                            toastr["error"](response.messages,"Form Invalid Status : "+response.status);
                        }
                    });
                });
                
            });
            
        });

        $('.btn-hapus-penagihan').click(function(e){
            console.log($(this).data("id"));
        // $('#table_data_pendaftaran tbody').on( 'click', 'button.btn-hapus', function (e) {
            e.preventDefault();
            var s = $("#invoiceDelete").show();
            var idHapus = $(this).data("id")    
            $(this).attr('disabled', true);
            var form = $(this).parents('form:first');

            var form_data = form.serialize();
            form_data += 'id=' +$(this).data("id") + '&_token={{ csrf_token() }}';

            // var reqId = form_data.id;
            // const form = form_data;
            // const data = new URLSearchParams(new FormData(form).entries());
                
            // var idHapus = document.getElementsByName("hapus_data_id");

            $("#Reqid").val(idHapus);
        });

        $("#hapusMod").click(function(e){
            var id = $("#Reqid").val();
            e.preventDefault();

            $("#hapusModal").hide();
            $(this).attr('disabled', true);

            var form = $(this).parents('form:first');

            var form_hapus = form.serialize();
            form_hapus += 'id=' +$(this).data("id") + '&_token={{ csrf_token() }}';

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax(
                {
                    type: "GET",
                    url: 'requestuttp/destroy/' + id,
                    data: form_hapus,
                    dataType: 'json',
                    success: function (response) {
                        if(response.status == true){
                            toastr["success"](response.messages,"Form Valid");
                            location.reload();
                        }else{
                            toastr["error"](response.messages,"Invalid");
                            location.reload();
                        }
                    },
                    error: function(xhr) {
                    toastr["error"]("Data gagal dihapus","Form Invalid");
                }
                });
            console.log(id)
        });

        $("#invHapus").click(function(e){
            var id = $("#Reqid").val();
            e.preventDefault();

            $("#invoiceDelete").hide();
            $(this).attr('disabled', true);

            var form = $(this).parents('form:first');

            var form_hapus = form.serialize();
            form_hapus += 'id=' +$(this).data("id") + '&_token={{ csrf_token() }}';

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax(
                {
                    type: "GET",
                    url: 'requestuttp/destroy/' + id,
                    data: form_hapus,
                    dataType: 'json',
                    success: function (response) {
                        if(response.status == true){
                            toastr["success"](response.messages,"Form Valid");
                            location.reload();
                        }else{
                            toastr["error"](response.messages,"Invalid");
                            location.reload();
                        }
                    },
                    error: function(xhr) {
                    toastr["error"]("Data gagal dihapus","Form Invalid");
                }
                });
            console.log(id)
        });

        //$('.btn-cetak-tag').click(function(e){
        $('#table_data_kirim tbody').on( 'click', 'button.btn-cetak-tag', function (e) {
            e.preventDefault();

            var url = '{{ route("requestuttp.tag", ":id") }}';
            url = url.replace(':id', $(this).data("id"));

            window.open(url, '_blank');
            window.focus();
            //location.reload();
        });

        //$('.btn-kirim-alat').click(function(e){
        $('#table_data_kirim tbody').on( 'click', 'button.btn-kirim-alat', function (e) {
            e.preventDefault();
            
            var url = '{{ route("requestuttp.instalasi") }}';
            //url = url.replace(':id', $(this).data("id"));

            //location.reload();

            var form_data = 'id=' +$(this).data("id") + '&_token={{ csrf_token() }}';

            $.post(url,form_data,function(response)
            {
                if(response.status===true)
                {
                    //location.reload();
                    toastr["success"]("Berhasil dikirim. Silakan refresh jika diperlukan.","Form Valid");
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksa kembali","Form Invalid");
                }
            });
        });

        $("#no_order").change(function(e) {        
            e.preventDefault();
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.instalasiqr') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    //location.reload();
                    toastr["success"]("Berhasil dikirim","Form Valid");
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksa kembali","Form Invalid");
                }
            });
        });

        $('#refresh_btn').click(function(e) {
            e.preventDefault();
            location.reload();
        });

        // TABS
        $('#tabs a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });

        var hash = window.location.hash;
        $('#tabs a[href="' + hash + '"]').tab('show');
        // END TABS
    });
</script>
@endsection