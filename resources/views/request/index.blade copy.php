@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <a href="{{ route('request.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Buat Baru</a>
    <a href="{{ route('request.createbooking') }}" class="btn btn-w-md btn-primary" id="btn_create">Pendaftaran Mandiri</a>
    <!-- <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a> -->

    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Pendaftaran</th>
                        <th>No Order</th>
                        <th>UML</th>
                        <th>Nominal Order</th>
                        <th>Nama Pemesan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->no_register }}</td>
                        <td>
                        @if($row->payment_code==null)
                        xx-xxxx-xx-xxx
                        @endif
                        @if($row->payment_code!=null)
                        {{ $row->no_order }}
                        @endif
                        </td>
                        <td>{{ $row->MasterUml ? $row->MasterUml->uml_name : '' }}</td>
                        <td>{{ number_format($row->total_price,0,',','.') }}</td>
                        <td>{{ $row->pic_name }}</td>
                        <td>
                            <a target="_blank" href="{{ route('request.pdf', $row->id) }}" class="btn btn-warning btn-sm">Cetak Invoive/Order</a>
                            @if($row->payment_code===null)
                            <a href="{{ route('request.payment', $row->id) }}" class="btn btn-warning btn-sm">Bayar</a>
                            <a href="{{ route('request.destroy', $row->id) }}" class="btn btn-warning btn-sm">Hapus</a>
                            @else
                                @if($row->stat_service_request===1)
                                <a href="{{ route('request.paymentcancel', $row->id) }}" class="btn btn-warning btn-sm">Batal</a>
                                @endif
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data").DataTable();
    });
</script>
@endsection