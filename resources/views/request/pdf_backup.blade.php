{{ dd($requests) }}
<html>
<head>
    <title>INVOICE</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table th{text-align: center;}
        table th,td{padding: 5px 10px;}
        table.f9 td,tr.f9 td, .f9{font-size: 9pt;}
        table.f10 td,th{font-size: 10pt;}
        table.f11 td{font-size: 11pt;}
        table.f12 td{font-size: 12pt;}
        p.small{
            font-size: 7pt;
        }
    </style>
</head>
<body>
    <center>
        <p class="f9">Sistem Informasi Pelayanan UPTP IV</p>
    </center>
    <table border="0">
        <tr>
            <td style="width: 20mm;">
                <img src="{{ asset('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
            </td>
            <td>
                <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN<br />TERTIB NIAGA DIREKTORAT METROLOGI</p>
                <p class="f9">Jl. Parteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
            </td>
        </tr>
    </table>
    <center>
        <h4><b>INVOICE</b></h4>
    </center>
    <table style="width: 100%;" class="f10">
        <tr>
            <td colspan="3" style="text-align: right;"><b>NO. PENDAFTARAN : {{$requests["no_register"]}}</b></td>
        </tr>
        <tr>
            <td style="width: 30mm;"><b>Perusahaan</b></td>
            <td style="width: 80mm;">: {{$requests["master_uml"]["uml_name"]}}</td>
            <td rowspan="3" style="text-align: right;vertical-align: top;"><b>NO. ORDER : 
            @php
            if($mode=='register'){
            echo 'xxxx-xx-xxx';
            }else if($mode=='order'){
            echo $requests["no_order"];
            }
            @endphp
            </b></td>
        </tr>
        <tr>
            <td><b>Alamat</b></td>
            <td>: {{$requests["master_uml"]["address"]}}</td>
        </tr>
        <tr>
            <td><b>Jenis Pesanan</b></td>
            <td>: {{$requests["service_request_item"][0]["standard"]["master_sub_uml"]["uml_sub_name"]}}</td>
        </tr>
    </table>
    <br />
    <table style="width: 100%;" border="1" class="f10">
        <tr>
            <th>No.</th>
            <th>Nama Alat</th>
            <th>Jumlah Alat Per Unit/Set</th>
            <th>Banyak Set/Unit</th>
            <th>Objek Pengujian</th>
            <th>TOTAL BIAYA</th>
        </tr>
        @php
        $i=1;
        $total = 0;
        @endphp
        @foreach($requests["service_request_item"] as $request)
        <tr>
            <td>{{$i}}</td>
            <td>{{$request["standard"]["standard_tool_type"]["attribute_name"]}}</td>
            <td>{{$request["service_request_item_inspection"][0]["qty_inspection"]}} item per 1 set</td>
            <td>{{$request["standard"]["jumlah_per_set"]}}</td>
            <td>{{$request["service_request_item_inspection"][0]["standard_inspection_price"]["inspection_type"]}}</td>
            <td style="text-align: right;">Rp. {{number_format($request["subtotal_price"],2,',','.')}}</td>
        </tr>
        @php
        $i++;
        $total = $total + $request->subtotal_price;
        @endphp
        @endforeach
        <tr>
            <td colspan="5" style="text-align: right;">Total Keseluruhan</td>
            <td style="text-align: right;">Rp. {{number_format($total,2,',','.')}}</td>
        </tr>
    </table>
    <br />
    <table style="width: 100%;" class="f10" border="1">
        <tr>
            <th colspan="3">Tanggal</th>
            <th rowspan="2">Penerima</th>
            <th rowspan="2">Pemohon</th>
            <th rowspan="2">Keterangan</th>
        </tr>
        <tr>
            <th>Terima</th>
            <th>Proses</th>
            <th>Selesai</th>
        </tr>
        <tr class="text-center">
            <td rowspan="2" style="height: 20mm;">{{date("d M Y",strtotime($requests["receipt_date"]))}}</td>
            <td rowspan="2"></td>
            <td rowspan="2"></td>
            <td style="height: 20mm;"></td>
            <td></td>
            <td rowspan="2"></td>
        </tr>
        <tr>            
            <td style="text-align: center;">{{$requests["full_name"]}}</td>
            <td style="text-align: center;">{{$requests["pic_name"]}}</td>
        </tr>
    </table>
</body>
</html>