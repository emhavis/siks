@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        @if($serviceOrder->subkoordinator_notes != null)
        <div class="alert alert-info" role="alert">
            {{ $serviceOrder->subkoordinator_notes }}
        </div>
        @endif

        {!! Form::open(['url' => route('servicerevisionuttp.resultupload', $serviceOrder->id), 'files' => true, 'id' => 'form_result'])  !!}
        
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_uttp">Jenis UTTP</label>
                    {!! Form::text('jenis_uttp', $serviceOrder->ServiceRequestItem->uttp->type->uttp_type, ['class' => 'form-control','id' => 'jenis_uttp', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas Minimum</label>
                    {!! Form::text('kapasitas', $serviceOrder->ServiceRequestItem->uttp->tool_capacity_min . ' ' . $serviceOrder->ServiceRequestItem->uttp->tool_capacity_unit, ['class' => 'form-control','id' => 'kapasitas', 'readonly']) !!}
                </div>  
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas Maksimum</label>
                    {!! Form::text('kapasitas', $serviceOrder->ServiceRequestItem->uttp->tool_capacity . ' ' . $serviceOrder->ServiceRequestItem->uttp->tool_capacity_unit, ['class' => 'form-control','id' => 'kapasitas', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Merek/Model/Tipe/Nomor Seri</label>
                    {!! Form::text('merek', 
                        $serviceOrder->ServiceRequestItem->uttp->tool_brand . '/' . 
                        $serviceOrder->ServiceRequestItem->uttp->tool_model . '/' .
                        $serviceOrder->ServiceRequestItem->uttp->tool_type . '/' .
                        $serviceOrder->ServiceRequestItem->uttp->serial_no,
                        ['class' => 'form-control','id' => 'merek', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->ServiceRequestItem->uttp->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan', 'readonly']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>

        <!-- 
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory">Nama Pabrikan</label>
                    {!! Form::text('factory', 
                        $serviceOrder->ServiceRequestItem->uttp->tool_factory,
                        ['class' => 'form-control','id' => 'factory', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory_addr">Alamat Pabrikan</label>
                    {!! Form::text('factory_addr', 
                        $serviceOrder->ServiceRequestItem->uttp->tool_factory_address,
                        ['class' => 'form-control','id' => 'factory_addr', 'readonly']) !!}
                </div>
            </div>
        </div>
        -->

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Dasar Pengujian, Nomor</label>
                    {!! Form::text('reference_no', 
                        $serviceOrder->ServiceRequestItem->reference_no,
                        ['class' => 'form-control','id' => 'reference_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_date">Dasar Pengujian, Tanggal</label>
                    {!! Form::text('reference_date', 
                        date("d-m-Y", strtotime(isset($serviceOrder->ServiceRequestItem->reference_date) ? $serviceOrder->ServiceRequestItem->reference_date : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'reference_date', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_1">Diuji Oleh (1)</label>
                    {!! Form::text('test_by_1', 
                        $user->full_name,
                        ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_2">Diuji Oleh (2)</label>
                    {!! Form::select('test_by_2', $users, $serviceOrder->test_by_2, 
                        ['class' => 'form-control select2', 'id' => 'test_by_2']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_datein">Waktu Pengujian, Mulai</label>
                    {!! Form::text('staff_entry_datein', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_datein) ? $serviceOrder->staff_entry_datein : date("Y-m-d"))),
                        ['class' => 'date form-control','id' => 'staff_entry_datein', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_dateout">Waktu Pengujian, Selesai</label>
                    {!! Form::text('staff_entry_dateout', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_dateout) ? $serviceOrder->staff_entry_dateout : date("Y-m-d"))),
                        ['class' => 'date form-control','id' => 'staff_entry_dateout', 'readonly']) !!}
                </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Daya Baca</label>
                    {!! Form::text('daya_baca', $serviceOrder->daya_baca, ['class' => 'form-control','id' => 'daya_baca']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kelas Keakurasian</label>
                    {!! Form::text('kelas_keakurasian', $serviceOrder->kelas_keakurasian, ['class' => 'form-control','id' => 'kelas_keakurasian']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Interval Skala Verifikasi</label>
                    {!! Form::text('interval_skala_verifikasi', $serviceOrder->interval_skala_verifikasi, ['class' => 'form-control','id' => 'interval_skala_verifikasi']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Konstanta</label>
                    {!! Form::text('konstanta', $serviceOrder->konstanta, ['class' => 'form-control','id' => 'konstanta']) !!} 
                </div>
            </div>
        </div>

        @elseif($serviceOrder->ServiceRequest->service_type_id == 4 || $serviceOrder->ServiceRequest->service_type_id == 5)
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sertifikat_expired_at">Masa Berlaku</label>
                    {!! Form::text('sertifikat_expired_at', 
                        date("d-m-Y", strtotime(isset($serviceOrder->sertifikat_expired_at) ? $serviceOrder->sertifikat_expired_at : $sertifikat_expired_at)),
                        ['class' => 'date form-control','id' => 'sertifikat_expired_at', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sertifikat_expired_at">Tanda Pegawai Berhak</label>
                    {!! Form::text('tanda_pegawai_berhak', $serviceOrder->tanda_pegawai_berhak,
                        ['class' => 'date form-control','id' => 'tanda_pegawai_berhak']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sertifikat_expired_at">Tanda Daerah</label>
                    {!! Form::text('tanda_daerah', $serviceOrder->tanda_daerah,
                        ['class' => 'date form-control','id' => 'tanda_daerah']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sertifikat_expired_at">Tanda Sah</label>
                    {!! Form::text('tanda_sah', $serviceOrder->tanda_sah,
                        ['class' => 'date form-control','id' => 'tanda_sah']) !!}
                </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->lokasi_pengujian == 'luar' && $serviceOrder->ServiceRequestItem->location != null)
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Lokasi Penempatan</label>
                    {!! Form::text('location', $serviceOrder->ServiceRequestItem->location, ['class' => 'form-control','id' => 'location', 'readonly']) !!} 
                </div>
            </div>
        </div>
        @if(count($serviceOrder->ttuPerlengkapans) > 0)
        <h5>Perlengkapan</h5>
        <div class="row">
            <div class="col-md-12">
                <table id="perlengkapan" class="table table-responsive-sm input-table">
                    <thead>
                        <tr>
                            <th>Jenis</th>
                            <th>Merek</th>
                            <th>Model/Tipe</th>
                            <th>Nomor Seri</th>
                            <th>Keterangan/Penyegelan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($serviceOrder->ttuPerlengkapans as $perlengkapan)
                        <tr>
                            <td>{{ $perlengkapan->uttp->type->uttp_type }}</td>
                            <td>{{ $perlengkapan->uttp->tool_brand }}</td>
                            <td>{{ $perlengkapan->uttp->tool_model }}</td>
                            <td>{{ $perlengkapan->uttp->serial_no }}</td>
                            <td>
                                <input type="hidden" name="perlengkapan_id[]" />
                                <input type="hidden" name="perlengkapan_uttp_id[]" value="{{ $perlengkapan->uttp_id }}"/>
                                <input type="text" class="form-control" name="perlangkapan_keterangan[]" 
                                    value="{{ $perlengkapan->keterangan }}"/>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @endif
        
        @if($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
        <table id="data_table_ttu_alg_badanhitung" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr colspan="3">Identitas Badan Hitung</tr>
                <tr>
                    <th>Tipe</th>
                    <th>No Seri</th>
                    <th>
                        <button type="button" id="btn_add_badanhitung" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($badanHitungs as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="type[]" value="{{ $ttuItem->type }}"/></td>
                    <td><input type="text" class="form-control" name="serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="badanhitung_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM')
        <table id="data_table_ttu_mabbm_badanhitung" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr colspan="4">Identitas Badan Hitung</tr>
                <tr>
                    <th>Merek</th>
                    <th>Tipe</th>
                    <th>No Seri</th>
                    <th>
                        <button type="button" id="btn_add_badanhitung" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($badanHitungs as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="brand[]" value="{{ $ttuItem->brand }}"/></td>
                    <td><input type="text" class="form-control" name="type[]" value="{{ $ttuItem->type }}"/></td>
                    <td><input type="text" class="form-control" name="serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="badanhitung_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endif

        @if($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tank_no">Nomor Tangki</label>
                    {!! Form::text('tank_no', 
                        $ttu ? $ttu->tank_no : '',
                        ['class' => 'form-control','id' => 'tank_no']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tag_no">Nomor Tag</label>
                    {!! Form::text('tag_no', 
                        $ttu ? $ttu->tag_no : '',
                        ['class' => 'form-control','id' => 'tag_no']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Air')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="totalisator">Totalisator</label>
                    {!! Form::text('totalisator', 
                        $ttu ? $ttu->totalisator : '',
                        ['class' => 'form-control','id' => 'totalisator']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' ||
            $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' ||
            $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)'
        )
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tank_no">Totalisator</label>
                    {!! Form::text('totalisator', 
                        $ttu ? $ttu->totalisator : '',
                        ['class' => 'form-control','id' => 'totalisator']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kfactor">K Faktor</label>
                    {!! Form::text('kfactor', 
                        $ttu ? $ttu->kfactor : '',
                        ['class' => 'form-control','id' => 'kfactor']) !!}
                </div>
            </div>
        </div>
        @endif

        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="persyaratan_teknis">Persyaratan Teknis</label>
                    {!! Form::hidden('persyaratan_teknis_id', 
                        $serviceOrder->ServiceRequestItem->uttp->type->oiml_id,
                        ['class' => 'form-control','id' => 'persyaratan_teknis_id', 'readonly']) !!}
                    {!! Form::textarea('persyaratan_teknis_id_str', 
                        $serviceOrder->ServiceRequestItem->uttp->type->syarat_teknis,
                        ['class' => 'form-control','id' => 'persyaratan_teknis_id_str', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="file_skhp">Lampiran Evaluasi</label>
                    {!! Form::file('file_skhp', null,
                        ['class' => 'form-control','id' => 'file_skhp']) !!}
                </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
        <table id="data_table" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Pemeriksaan/Pengujian</th>
                    <th>Pemenuhan Persyaratan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($inspectionItems as $item)
                <tr>
                    <td>{{ $item->inspectionItem->name }}</td>
                    <td>
                    @if($item->inspectionItem->is_tested)
                    {!! Form::select('is_accepted_'.$item->id, ['ya'=>'Ya','tidak'=>'Tidak', ''=>'N/A'], $item->is_accepted, 
                        ['class' => 'form-control select2','id' => 'is_accepted_'.$item->id, 'style' => 'width:100%']) !!}
                    @endif
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequest->service_type_id == 4 || $serviceOrder->ServiceRequest->service_type_id == 5)
        
        @if($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
        <table id="data_table_ttu_alg" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th rowspan="2">Input Level (mm)</th>
                    <th colspan="2">Kesalahan Penunjukan</th>
                    <th rowspan="2">Kesalahan Histerisis (mm)</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>Naik (mm)</th>
                    <th>Turun (mm)<th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="input_level[]" value="{{ $ttuItem->input_level }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error_up[]" value="{{ $ttuItem->error_up }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="error_down[]" value="{{ $ttuItem->error_down }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="error_hysteresis[]" value="{{ $ttuItem->error_hysteresis }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM')
        <table id="data_table_ttu_mabbm" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Kecepatan Alir (L/menit)</th>
                    <th>Kesalahan (%) BKD: &#177;0,5%</th>
                    <th>Meter Factor BKD: 1&#177;0,005</th>
                    <th>Kemampuan Ulang BKD: &#177;0,1%</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="meter_factor[]" value="{{ $ttuItem->meter_factor }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatability[]" value="{{ $ttuItem->repeatability }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas')
        <table id="data_table_ttu_mg" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Kecepatan Alir (m3/h)</th>
                    <th>Kesalahan (%)</th>
                    <th>Kemampuan Ulang %</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatibility[]" value="{{ $ttuItem->repeatibility }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Air')
        <table id="data_table_ttu_ma" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Kecepatan Alir (L/menit)</th>
                    <th>Kesalahan (%) BKD: &#177;0,5%</th>
                    <th>Kemampuan Ulang BKD: &#177;0,1%</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatability[]" value="{{ $ttuItem->repeatability }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)')
        <table id="data_table_ttu_mguwc" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th rowspan="2">Kecepatan Alir (m3/h)</th>
                    <th colspan="2">Kesalahan</th>
                    <th colspan="2">Kemampuan Ulang</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>Nilai (%)</th>
                    <th>BKD (%)</th>
                    <th>Nilai</th>
                    <th>BKD</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="error_bkd[]" value="{{ $ttuItem->error_bkd }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatability[]" value="{{ $ttuItem->repeatability }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="repeatability_bkd[]" value="{{ $ttuItem->repeatability_bkd }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Depth tanda_pegawai_berhak')
        <table id="data_table_ttu_dt" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Rentang Ukur (m)</th>
                    <th>Suhu Dasar (C)</th>
                    <th>Panjang Sebenarnya (mm)</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="rentang_ukur[]" value="{{ $ttuItem->rentang_ukur }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="suhu_dasar[]" value="{{ $ttuItem->suhu_dasar }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="panjang_sebenarnya[]" value="{{ $ttuItem->panjang_sebenarnya }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @endif
        
        @else
        @endif

        <input type="hidden" name="stat_sertifikat" id="stat_sertifikat" value="0"/>
        <button role="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan Hasil Uji</button>
        <button class="btn btn-w-md btn-success" id="btn_simpan_submit">Kirim Hasil Uji</button>
        {!! Form::close() !!}
        
        
        
       
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">

    $(document).ready(function ()
    {
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });
        /*
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        */

        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_alg_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" step="any" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_mabbm_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" step="any" class="form-control" name="brand[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_alg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_alg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="input_level[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_up[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_down[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_hysteresis[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mabbm");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="meter_factor[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_ma').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_ma");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_ma').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mguwc').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mguwc");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_bkd[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="repeatability_bkd[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mguwc').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_dt').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_dt");
            var insp_item = '<tr>' + 
                '   <td><input type="text" step="any" class="form-control" name="rentang_ukur[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="suhu_dasar[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="panjang_sebenarnya[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_dt').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            $("#stat_sertifikat").val(1);

            var form = $("#form_result");
            form[0].submit();
        });
    }); 
</script>
@endsection