@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled">
        <div class="panel-heading">
            <div class="panel-body">
                {!! Form::open(array('route' => 'service.store','id' => 'form_create_request', 'enctype' => "multipart/form-data")) !!}
                    <div class="form-group">
                        <label for="uml_name">Nama UML</label>
                        {!! Form::text('uml_name', null, ['class' => 'form-control','id' => 'uml_name', 'placeholder' => 'Nama UML', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="address">Alamat</label> 
                        {!! Form::text('address', null, ['class' => 'form-control','id' => 'address', 'placeholder' => 'Alamat', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group">
                        <label for="phone_no">No. Telepon</label>
                        {!! Form::text('phone_no', null, ['class' => 'form-control','id' => 'phone_no', 'placeholder' => 'Nomor Telepon', 'autocomplete' => 'off']) !!}
                    </div>
                    {!! Form::submit('Simpan', ['class' => 'btn btn-w-md btn-accent', 'name' => 'submitbutton']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection