@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <a href="{{ route('uml.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUML" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Nama UML</th>
                        <th>Alamat</th>
                        <th>No. Telepon</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($umls->count())
                    @foreach($umls as $uml)
                    <tr>
                        <td>{{ ucfirst($uml->uml_name) }}</td>
                        <td>{{ $uml->address }}</td>
                        <td>{{ $uml->phone_no }}</td>
                        <td>
                            <a title="ubah" href="{{ route('uml.edit', $uml->id) }}" class="btn btn-accent btn-sm" style="margin-right:2px;">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function () {
        
        $('#tUML').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ]
        });

    });
</script>
@endsection