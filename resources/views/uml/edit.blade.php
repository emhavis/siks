@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>Master Data<br> <span class="c-white">UML</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-add-user"></i>
            </div>
            <div class="header-title">
                <h3>UML</h3>
                <small>
                    Halaman informasi UML.
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
    <div class="panel panel-filled">
        <div class="panel-heading">
            <div class="panel-body">
                {!! Form::model($uml, ['method' => 'PATCH', 'route' => ['uml.update', $uml->id],'id' => 'form_update_uml', 'enctype' => "multipart/form-data"]) !!}
                    <div class="form-group">
                        <label for="uml_name">Nama UML</label>
                        {!! Form::text('uml_name', $uml->uml_name, ['class' => 'form-control','id' => 'uml_name', 'placeholder' => 'Nama UML', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="address">Alamat</label> 
                        {!! Form::text('address', $uml->address, ['class' => 'form-control','id' => 'address', 'placeholder' => 'Alamat', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group">
                        <label for="phone_no">No. Telepon</label>
                        {!! Form::text('phone_no', $uml->phone_no, ['class' => 'form-control','id' => 'phone_no', 'placeholder' => 'Nomor Telepon', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="text-right margin-t-30">
                        {!! Form::submit('Simpan', ['class' => 'btn btn-w-md btn-accent', 'name' => 'submitbutton']) !!}
                    </div>
                    
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection