<html>
<head>
    <title>{{ $row->payment_code?'ORDER':'INVOICE' }}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table th{text-align: center;}
        table th,td{padding: 5px 10px;}
        table.f9 td,tr.f9 td, .f9{font-size: 9pt;}
        table.f10 td,th{font-size: 10pt;}
        table.f11 td{font-size: 11pt;}
        table.f12 td{font-size: 12pt;}
        p.small{
            font-size: 7pt;
        }
    </style>
</head>
<body>
    @if ($row->lokasi_pengujian == 'luar')
    @foreach($docs as $doc)
        <div style="page-break-after: always;"></div>

    
        <table border="0">
            <tr>
                <td style="width: 20mm;">
                    <img src="{{ asset('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
                </td>
                <td>
                    <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN TERTIB NIAGA <br/>DIREKTORAT METROLOGI</p>
                    <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
                </td>
            </tr>
        </table>
        <center>
            <h4><b>TAGIHAN UANG HARIAN PETUGAS</b></h4>
            <h6>(Sesuai Peraturan Menteri Keuangan Republik Indonesia Nomor 39 Tahun 2024 Tentang Standar Biaya Masukan Tahun Anggaran 2025)</h6>
        </center>
        <table style="width: 100%;" class="f10">
            <tr>
                <td colspan="3" style="text-align: right;"><b>NO. PENDAFTARAN : {{$row->no_register}}</b></td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Perusahaan</b></td>
                <td style="width: 80mm;">: {{ $row->requestor->full_name }}</td>
                <td style="text-align: right;vertical-align: top;"><b>NO. ORDER : 
                {{ isset($row->no_order) ? $row->no_order :'xxxx-xx-xxx' }}
                </b></td>
            </tr>
            <tr>
                <td><b>Alamat</b></td>
                <td>: {{ $row->requestor->alamat }}</td>
                <td style="text-align: right;vertical-align: top;"><b>NO. SPUH : 
                {{ $doc->doc_no }}
                </b></td>
            </tr>
            <tr>
                <td><b>Jenis Pesanan</b></td>
                <td>: {{ $row->jenis_layanan }}</td>
                <td style="text-align: right;vertical-align: top;"><b>TANGGAL : 
                {{ $doc->billing_date ? date("d M Y", strtotime($doc->billing_date)) : date("d M Y") }}
                </b></td>
            </tr>
            <tr>
                <td style="vertical-align: top;"><b>Nama Petugas</b></td>
                <td>: 
                      @foreach($doc->listOfStaffs as $idx=>$staff)
                      @if ($idx == 0)
                      {{ $idx + 1 }}. {{ $staff->scheduledStaff->nama }} (NIP: {{ $staff->scheduledStaff->nip }})<br/>
                      @else
                        @if($staff->scheduledStaff != null)
                      &nbsp; {{ $idx + 1 }}. {{ $staff->scheduledStaff->nama }} (NIP: {{ $staff->scheduledStaff->nip }})<br/>
                        @endif
                      @endif
                      @endforeach
                </td>
            </tr>
            <tr>
                <td><b>Tanggal Pelaksanaan</b></td>
                <td>: {{ date("d M Y", strtotime($doc->date_from)) }} - {{ date("d M Y", strtotime($doc->date_to)) }}</td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;" border="1" class="f10">
            <tr>
                <th>No.</th>
                <th>Keterangan</th>
                <th>Rincian</th>
                <th>JUMLAH</th>
            </tr>
            <tr>
                <td style="vertical-align:top;">1</td>
                @if($row->lokasi_dl == 'dalam'):
                <td style="vertical-align:top;">Uang Harian ({{ $row->inspectionKabkot->nama }}, {{ $row->inspectionProv->nama }})
                    @if($doc->jenis == 'revisi')
                    , Kekurangan
                    @endif
                </td>
                @else 
                <td style="vertical-align:top;">Uang Harian ({{ $row->inspectionNegara->nama_negara }})
                    @if($doc->jenis == 'revisi')
                    , Kekurangan
                    @endif
                </td>
                @endif
                <td style="vertical-align:top;">
                    <?php
                        $dt1 = new DateTime($doc->date_from);
                        $dt2 = new DateTime($doc->date_to);
                        $interval = $dt1->diff($dt2);
                        $int_interval = ((int)$interval->format('%a') + 1);
                        if ($doc->jenis == 'revisi') {
                            $int_interval = $doc->add_days;
                        }
                    ?>
                    <?php
                        $staffes = $doc->listOfStaffs->filter(function ($value, $key) {
                            return $value->scheduledStaff != null;
                        });
                    ?>
                    
                    @if ($row->lokasi_dl == 'dalam')
                    {{ count($staffes) }} orang x {{ $int_interval }} hari x Rp. {{ number_format($doc->rate,2,',','.') }}
                    @else
                    1 orang x ({{ $int_interval - 1 }} hari x USD. {{ number_format($doc->spuh_rate1,2,',','.') }} + 1 hari x 40% x  USD. {{ number_format($doc->spuh_rate1,2,',','.') }}) 
                    @if ($row->spuh_rate2 > 0)
                    + 1 orang x ({{ $int_interval - 1 }} hari x USD. {{ number_format($doc->spuh_rate2,2,',','.') }} + 1 hari x 40% x  USD. {{ number_format($doc->spuh_rate2,2,',','.') }})
                    @endif
                    @endif
                </td>
                
                <td style="vertical-align:top;text-align: right;">
                @if ($row->lokasi_dl == 'dalam')
                Rp. {{number_format($doc->price,2,',','.')}}
                @else
                USD. {{number_format($doc->price,2,',','.')}}
                @endif
                </td>
            </tr>
            
            <tr>
                <td colspan="3" style="text-align: center;"><b>Total Uang Harian</b></td>
                @if ($row->lokasi_dl == 'dalam')
                <td style="text-align: right;">Rp. {{number_format($doc->invoiced_price,2,',','.')}}</td>
                @else
                <td style="text-align: right;">USD. {{number_format($doc->invoiced_price,2,',','.')}}</td>
                @endif
            </tr>
        </table>

        <table style="width: 100%;" class="f10" border="1">
            <tr>
                 <td style="text-align: center; border: none; color:red;">
                Pembayaran uang harian hanya dilakukan melalui Transfer ke Rekening Penerimaan Lainnya (RPL) Balai Pengujian UTTP<br/>
                <strong>Dilarang transfer melalui rekening petugas langsung!</strong>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border: none;">
                <strong>Bank Mandiri Cabang RSHS Bandung</strong><br/>
                <strong>a.n. RPL 022 PS BPUTTP UTK UHPP</strong></br/>
                <strong>Nomor Rekening 132-00-2419193-5</strong><br/>
                <strong>(Cantumkan No. Tagihan atau No. Pendaftaran)</strong>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border: none; padding-top: 10px;">
                Konfirmasi Pembayaran melalui aplikasi <strong>SIMPEL UPTP IV</strong>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border: none;">
                <strong>Pembayaran dilakukan paling lambat 1 (satu) hari sebelum tanggal pelaksanaan</strong>
                </td>
            </tr>
        </table>

    @endforeach
    @endif
</body>
</html>