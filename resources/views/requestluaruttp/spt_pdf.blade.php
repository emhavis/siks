<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Tugas</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-draft-uttp.png") : public_path("assets/images/logo/letterhead-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
        .table td .eng {
            font-size:8pt;
            font-style:italic;
        }
    </style>
</head>

<body class="A4">

    <section class="sheet letterhead }}"> 

        <div class="text-center" style="padding-top: 35mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="title"><strong>SURAT TUGAS</strong></div>
            <div class="subtitle">Nomor: {{ $request->spuh_spt }}</div>
        </div>

        <br/><br/>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td>Menimbang</div></td>
                    <td style="width: 3mm;">:</td>
                    <td>a.</td>
                    <td colspan="3">Bahwa pelaksanaan Tugas dan Fungsi Balai Pengujian UTTP mencakup kegiatan
di dalam dan di luar kantor;</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>b.</td>
                    <td colspan="3">Bahwa dalam rangka kegiatan di luar kantor perlu diterbitkan Surat Tugas.</td>
                </tr>
                <tr>
                    <td>Dasar</div></td>
                    <td style="width: 3mm;">:</td>
                    <td>1.</td>
                    <td colspan="3">Peraturan Pemerintah Nomor 31 Tahun 2017 tentang Jenis dan Tarif atas Jenis
Penerimaan Negara Bukan Pajak yang Berlaku pada Kementerian Perdagangan;</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>2.</td>
                    <td colspan="3">Peraturan Menteri Perdagangan Republik Indonesia Nomor 81 Tahun 2020
tentang Organisasi dan Tata Kerja Unit Pelaksana Teknis Kementerian
Perdagangan;</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>3.</td>
                    <td colspan="3">Surat permohonan dari {{ $request->label_sertifikat }} Nomor Registrasi {{ $request->no_register }} Tanggal {{ format_long_date($request->received_date) }}.</td>
                </tr>
                <tr><td colspan="6">&nbsp;</td></tr>
                <tr>
                    <td class="text-center" colspan="6">Memberi Tugas</td>
                </tr>
                @foreach($request->spuhDoc->listOfStaffs as $staff)
                @if($loop->first)
                <tr>
                    <td>Kepada</div></td>
                    <td style="width: 3mm;">:</td>
                    <td>{{ $loop->index + 1 }}.</td>
                    <td style="width: 50mm;">{{ $staff->scheduledStaff->nama }}<br/>Penera Ahli</td>
                    <td>NIP. <br/>{{ $staff->scheduledStaff->nip }}</td>
                    <td>Pangkat/Gol. <br/>Penata Muda</td>
                </tr>
                @else
                <tr>
                    <td></div></td>
                    <td style="width: 3mm;"></td>
                    <td>{{ $loop->index + 1 }}.</td>
                    <td style="width: 50mm;">{{ $staff->scheduledStaff->nama }}<br/>Penera Ahli</td>
                    <td>NIP. <br/>{{ $staff->scheduledStaff->nip }}</td>
                    <td>Pangkat/Gol. <br/>Penata Muda</td>
                </tr>
                @endif
                @endforeach
                <?php
                    $listItems = [];
                    foreach($request->items as $item) {
                        $listItems[]= $item->uttp->type->uttp_type;
                    }

                    $items = implode(", ", $listItems);
                ?>
                <tr>
                    <td>Untuk</div></td>
                    <td style="width: 3mm;">:</td>
                    <td>1.</td>
                    <td colspan="3">Melaksanakan {{ $request->jenis_layanan }} {{ $items }} milik {{ $request->label_sertifikat }} selama {{ $request->spuhDoc->days }} hari dari tanggal {{ format_long_date($request->spuhDoc->date_from) }} sampai dengan tanggal {{ format_long_date($request->spuhDoc->date_to) }};</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>2.</td>
                    <td colspan="3">Biaya yang berkaitan dengan pelaksanaan tugas ini dibebankan pada {{ $request->label_sertifikat }}.</td>
                </tr>
            </tbody>
        <table>

        <br />
        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">Dalam melaksanakan tugas seluruh Pejabat dan Pegawai <strong>"DILARANG MENERIMA GRATIFIKASI
DALAM BENTUK APAPUN"</strong>. Setelah pelaksanaan tugas tersebut selesai agar menyampaikan
laporan kepada Kepala Balai Pengujian UTTP.</p>
        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">Demikian untuk dilaksanakan dengan penuh rasa tanggung jawab.</p>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 40mm;">Bandung, {{ $request->spuhDoc != null && $request->spuhDoc->scheduled_date ? format_long_date($request->spuhDoc->scheduled_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Kepala Balai Pengujian UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                    </td>
                </tr>
                <tr>
                    <td>
                        Kepala Balai Pengujian UTTP
                        <br/>NIP:
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>
</body>

</html>