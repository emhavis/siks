@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(['url' => route('requestluaruttp.simpanbookinginspection', $item->id)])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pengujian/Pemeriksaan</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inspection_price_id">Jenis Pengujian</label>
                            {!! Form::select('inspection_price_id', $prices, null, 
                                ['class' => 'form-control select2','id' => 'inspection_price_id']) !!}
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="quantity">Jumlah</label>
                            {!! Form::text('quantity', '', ['class' => 'form-control','id' => 'quantity']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="satuan">Satuan</label>
                            {!! Form::text('satuan', '', ['class' => 'form-control','id' => 'satuan',  'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="price">Harga Satuan</label>
                            {!! Form::text('price', '', ['class' => 'form-control','id' => 'price', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="subtotal">Subtotal</label>
                            {!! Form::text('subtotal', '', ['class' => 'form-control','id' => 'subtotal', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Lanjut</button> 
        </form>
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        
        $('#inspection_price_id').select2().on("select2:select", function (e) { 
            var route = "{{ route('requestuttp.getprices', ':id') }}";
            route = route.replace(':id', e.params.data.id);
            $.get(route,function(res)
            {
                //res = JSON.parse(res);
                if (res) {
                    $("#quantity").attr('readonly', false);
                    if (res.price.has_range) {
                        $('#price').val(res.ranges[0].price);
                        $('#satuan').val(res.ranges[0].price_unit);

                        if (res.price.unit == 'pengujian') {
                            $("#quantity").val(1).trigger('change');
                            $("#quantity").attr('readonly', true);
                        } 

                        priceRanges = res.ranges;
                    } else {
                        $('#price').val(res.price.price);
                        $('#satuan').val(res.price.unit);

                        if (res.price.unit == 'pengujian') {
                            $("#quantity").val(1).trigger('change');
                            $("#quantity").attr('readonly', true);
                        }
                    }
                }
            });

            /*
            if (e.params.data.id == 159) {
                
                var price = arrPrices.find(x => e.params.data.id == 159);
                $('#price').val(price.price);
                $('#satuan').val(price.unit)

                $('#quantity').val(3).trigger('change');
            } else {
                
            }
            */
        });

        $('#quantity').change(function() {
            var qty = $('#quantity').val();
            $('#subtotal').val(qty * $('#price').val());
        })
    });

</script>
@endsection