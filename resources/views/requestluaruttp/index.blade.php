@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
<div class="row">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#berangkat" aria-controls="berangkat" role="tab" data-toggle="tab">Berangkat</a></li>
        <li role="presentation"><a href="#selesai" aria-controls="selesai" role="tab" data-toggle="tab">Selesai</a></li>
        <!--
        <li role="presentation"><a href="#penagihan" aria-controls="penagihan" role="tab" data-toggle="tab">Invoice</a></li>
        <li role="presentation"><a href="#validasi" aria-controls="validasi" role="tab" data-toggle="tab">Validasi</a></li>
        -->
    </ul>
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="berangkat">
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Jadwal Pengujian</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_berangkat as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->scheduled_test_date_from)) }} s/d {{ date("d-m-Y", strtotime($row->scheduled_test_date_to)) }}</td>
                                <td>
                                <a href="{{ route('requestluaruttp.proses', $row->id) }}"
                                    class="btn btn-warning btn-sm" >PROSES</button>
                                @if($row->pending_status=="0" || $row->pending_status==null)
                                <a href="{{ route('requestluaruttp.pending', $row->id) }}"
                                    class="btn btn-danger btn-sm" >TUNDA UJI</button>
                                @else
                                <a href="{{ route('requestluaruttp.continue', $row->id) }}"
                                    class="btn btn-success btn-sm" >LANJUT UJI</button>
                                @endif
                                <a href="{{ route('requestluaruttp.cancel', $row->id) }}"
                                    class="btn btn-danger btn-sm" >BATAL UJI</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="selesai">
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <table id="data_table_selesai" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Jadwal Pengujian</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_selesai as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->scheduled_test_date_from)) }} s/d {{ date("d-m-Y", strtotime($row->scheduled_test_date_to)) }}</td>
                                <td>
                                    <!--
                                    <a href="{{ route('requestluaruttp.extend', $row->id) }}"
                                        class="btn btn-warning btn-sm btn-mdl" >TAMBAH WAKTU</a>
                                    -->
                                    <a href="{{ route('requestluaruttp.selesai', $row->id) }}"
                                        class="btn btn-warning btn-sm btn-mdl" >SELESAI</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--
        <div role="tabpanel" class="tab-pane" id="penagihan">
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <table id="data_table_penagihan" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Lokasi, Provinsi</th>
                                <th>Jadwal Pengujian</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_penagihan as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->inspectionProv->nama }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->scheduled_test_date_from)) }} s/d {{ date("d-m-Y", strtotime($row->scheduled_test_date_to)) }}</td>
                                <td>
                                    @if($row->total_price > 0 && $row->billing_to_date == null)
                                    <a target="_blank" href="{{ route('requestluaruttp.pdf', $row->id) }}" class="btn btn-warning btn-sm">Draft Invoice PNBP</a>
                                    @endif
                                    @if($row->spuhDoc != null)
                                    @if(($row->spuhDoc->invoiced_price > 0 && $row->spuhDoc->billing_date == null) || 
                                        ($row->spuhDoc->invoiced_price > 0 && $row->spuhDoc->act_billing_date == null))
                                    <a target="_blank" href="{{ route('requestluaruttp.pdftuhp', $row->id) }}" class="btn btn-warning btn-sm">Draft Invoice SPUH</a>
                                    @endif
                                    @endif

                                    @if($row->total_price > 0 && $row->billing_to_date == null)
                                    <a href="{{ route('requestluaruttp.payment', $row->id) }}"
                                        class="btn btn-warning btn-sm btn-mdl" >Billing PNBP</a>
                                    @endif
                                    @if($row->spuhDoc != null)
                                    @if(($row->spuhDoc->invoiced_price > 0 && $row->spuhDoc->billing_date == null) || 
                                        ($row->spuhDoc->invoiced_price > 0 && $row->spuhDoc->act_billing_date == null))
                                    <a href="{{ route('requestluaruttp.paymenttuhp', $row->id) }}"
                                        class="btn btn-warning btn-sm btn-mdl" >Kirim SPUH</a>
                                    @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="validasi">
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <table id="data_table_validasi" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Lokasi, Provinsi</th>
                                <th>Jadwal Pengujian</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_validasi as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->inspectionProv->nama }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->scheduled_test_date_from)) }} s/d {{ date("d-m-Y", strtotime($row->scheduled_test_date_to)) }}</td>
                                <td>
                                    @if($row->total_price > 0 && $row->payment_date != null && $row->payment_valid_date == null)
                                    <a href="{{ route('requestluaruttp.valid', $row->id) }}"
                                        class="btn btn-warning btn-sm btn-mdl" >Validasi PNBP</a>
                                    @endif
                                    @if($row->spuhDoc != null)
                                    @if($row->spuhDoc->invoiced_price > 0 && $row->spuh_payment_date != null && $row->spuh_payment_valid_date == null)
                                    <a href="{{ route('requestluaruttp.validtuhp', $row->id) }}"
                                        class="btn btn-warning btn-sm btn-mdl" >Validasi SPUH</a>
                                    @endif
                                    @endif
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        -->
    </div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $('#data_table, #data_table_selesai,#data_table_penagihan,#data_table_validasi').DataTable();

});

</script>
@endsection