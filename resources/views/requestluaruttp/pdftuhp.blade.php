<html>
<head>
    <title>INVOICE</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table th{text-align: center;}
        table th,td{padding: 5px 10px;}
        table.f9 td,tr.f9 td, .f9{font-size: 9pt;}
        table.f10 td,th{font-size: 10pt;}
        table.f11 td{font-size: 11pt;}
        table.f12 td{font-size: 12pt;}
        p.small{
            font-size: 7pt;
        }
    </style>
</head>
<body>
    <center>
        <p class="f9">Sistem Informasi Pelayanan UPTP IV</p>
    </center>

    @if ($row->lokasi_pengujian == 'luar')
    @foreach($docs as $doc)
        <table border="0">
            <tr>
                <td style="width: 20mm;">
                    <img src="{{ asset('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
                </td>
                <td>
                    <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN TERTIB NIAGA <br/>DIREKTORAT METROLOGI</p>
                    <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
                </td>
            </tr>
        </table>
        <center>
            <h4><b>TAGIHAN UANG HARIAN PETUGAS</b></h4>
            <h6>(Sesuai PMK Nomor 60/PMK.02/2021 tentang Standar Biaya Masukan Tahun Anggaran 2022 )</h6>
        </center>
        <table style="width: 100%;" class="f10">
            <tr>
                <td colspan="3" style="text-align: right;"><b>NO. PENDAFTARAN : {{$row->no_register}}</b></td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Perusahaan</b></td>
                <td style="width: 80mm;">: {{ $row->requestor->full_name }}</td>
                <td style="text-align: right;vertical-align: top;"><b>NO. ORDER : 
                {{ $row->payment_code?$row->no_order:'xxxx-xx-xxx' }}
                </b></td>
            </tr>
            <tr>
                <td><b>Alamat</b></td>
                <td>: {{ $row->requestor->alamat }}</td>
                <td style="text-align: right;vertical-align: top;"><b>NO. SPUH : 
                {{ $row->spuh_no }}
                </b></td>
            </tr>
            <tr>
                <td><b>Jenis Pesanan</b></td>
                <td>: {{ $row->jenis_layanan }}</td>
                <td style="text-align: right;vertical-align: top;"><b>TANGGAL : 
                {{ $doc->billing_date ? date("d M Y", strtotime($doc->billing_date)) : date("d M Y") }}
                </b></td>
            </tr>
            <tr>
                <td style="vertical-align: top;"><b>Nama Petugas</b></td>
                <td>: 
                      @foreach($doc->listOfStaffs as $idx=>$staff)
                      @if ($idx == 0)
                      {{ $idx + 1 }}. {{ $staff->scheduledStaff->nama }} (NIP: {{ $staff->scheduledStaff->nip }})<br/>
                      @else
                        @if($staff->scheduledStaff != null)
                      &nbsp; {{ $idx + 1 }}. {{ $staff->scheduledStaff->nama }} (NIP: {{ $staff->scheduledStaff->nip }})<br/>
                        @endif
                      @endif
                      @endforeach
                </td>
            </tr>
            <tr>
                <td><b>Tanggal Pelaksanaan</b></td>
                <td>: {{ date("d M Y", strtotime($doc->date_from)) }} - {{ date("d M Y", strtotime($doc->date_to)) }}</td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;" border="1" class="f10">
            <tr>
                <th>No.</th>
                <th>Keterangan</th>
                <th>Rincian</th>
                <th>JUMLAH</th>
            </tr>
            <tr>
                <td style="vertical-align:top;">1</td>
                <td style="vertical-align:top;">Uang Harian ({{ $row->inspectionProv->nama }})</td>
                <td style="vertical-align:top;">
                    <?php
                        $dt1 = new DateTime($doc->date_from);
                        $dt2 = new DateTime($doc->date_to);
                        $interval = $dt1->diff($dt2);
                    ?>
                    {{ count($doc->listOfStaffs) }} orang x {{ ((int)$interval->format('%a') + 1) }} hari x Rp. {{ number_format($row->spuh_rate,2,',','.') }}
                </td>
                
                <td style="vertical-align:top;text-align: right;">
                Rp. {{number_format($doc->price,2,',','.')}}
                </td>
            </tr>
            @if($doc->price > $doc->invoiced_price)
            <tr>
                <td style="vertical-align:top;"></td>
                <td style="vertical-align:top;">Sudah diterima pembayaran</td>
                <td style="vertical-align:top;"></td>
                
                <td style="vertical-align:top;text-align: right;">
                (Rp. {{number_format($doc->price - $doc->invoiced_price,2,',','.')}})
                </td>
            </tr>
            @endif
            <tr>
                <td colspan="3" style="text-align: center;"><b>Total Uang Harian</b></td>
                <td style="text-align: right;">Rp. {{number_format($doc->invoiced_price,2,',','.')}}</td>
            </tr>
        </table>

        <table style="width: 100%;" class="f10" border="1">
            <tr>
                <td style="text-align: center; border: none;">
                Pembayaran dilakukan melalui Transfer ke Rekening Penerimaan Lainnya Balai Pengujian UTTP
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border: none;">
                <strong>Bank Mandiri Cabang RSHS Bandung</strong><br/>
                <strong>a.n. RPL 022 PS BPUTTP UTK UHPP</strong></br/>
                <strong>Nomor Rekening 132-00-2419193-5</strong><br/>
                <strong>(Cantumkan No. Tagihan atau No. Pendaftaran)</strong>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border: none; padding-top: 10px;">
                Konfirmasi Pembayaran melalui aplikasi <strong>SIMPEL UPTP IV</strong>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border: none;">
                <strong>Pembayaran dilakukan paling lambat 1 (satu) hari sebelum tanggal pelaksanaan</strong>
                </td>
            </tr>
        </table>

        @if($doc->act_price != null && $doc->act_price - $doc->price > 0)
        <div style="page-break-after: always;"></div>
    
        <table border="0">
            <tr>
                <td style="width: 20mm;">
                    <img src="{{ asset('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
                </td>
                <td>
                    <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN<br />TERTIB NIAGA DIREKTORAT METROLOGI</p>
                    <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
                </td>
            </tr>
        </table>
        <center>
            <h4><b>TAGIHAN UANG HARIAN PETUGAS</b></h4>
            <h6>(Sesuai PMK Nomor 119/PMK.02/2020 tentang Standar Biaya Masukan Tahun Anggaran 2021 )</h6>
        </center>
        <table style="width: 100%;" class="f10">
            <tr>
                <td colspan="3" style="text-align: right;"><b>NO. PENDAFTARAN : {{$row->no_register}}</b></td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Perusahaan</b></td>
                <td style="width: 80mm;">: {{ $row->requestor->full_name }}</td>
                <td style="text-align: right;vertical-align: top;"><b>NO. ORDER : 
                {{ $row->payment_code?$row->no_order:'xxxx-xx-xxx' }}
                </b></td>
            </tr>
            <tr>
                <td><b>Alamat</b></td>
                <td>: {{ $row->requestor->alamat }}</td>
                <td style="text-align: right;vertical-align: top;"><b>NO. SPUH : 
                {{ $row->spuh_no }}
                </b></td>
            </tr>
            <tr>
                <td><b>Jenis Pesanan</b></td>
                <td>: {{ $row->jenis_layanan }}</td>
                <td style="text-align: right;vertical-align: top;"><b>TANGGAL : 
                {{ $doc->billing_date ? date("d M Y", strtotime($doc->billing_date)) : date("d M Y") }}
                </b></td>
            </tr>
            <tr>
                <td style="vertical-align: top;"><b>Nama Petugas</b></td>
                <td>: 
                      @foreach($doc->listOfStaffs as $idx=>$staff)
                      @if ($idx == 0)
                      {{ $idx + 1 }}. {{ $staff->scheduledStaff->nama }} (NIP: {{ $staff->scheduledStaff->nip }})<br/>
                      @else
                      &nbsp; {{ $idx + 1 }}. {{ $staff->scheduledStaff->nama }} (NIP: {{ $staff->scheduledStaff->nip }})<br/>
                      @endif
                      @endforeach
                </td>
            </tr>
            <tr>
                <td><b>Tanggal Pelaksanaan</b></td>
                <td>: {{ date("d M Y", strtotime($doc->act_date_from)) }} - {{ date("d M Y", strtotime($doc->act_date_to)) }}</td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;" border="1" class="f10">
            <tr>
                <th>No.</th>
                <th>Keterangan</th>
                <th>Rincian</th>
                <th>JUMLAH</th>
            </tr>
            <tr>
                <td style="vertical-align:top;">1</td>
                <td style="vertical-align:top;">Uang Harian Aktual ({{ $row->inspectionProv->nama }})</td>
                <td style="vertical-align:top;">
                    <?php
                        $dt1 = new DateTime($doc->act_date_from);
                        $dt2 = new DateTime($doc->act_date_to);
                        $interval = $dt1->diff($dt2);
                    ?>
                    {{ count($doc->listOfStaffs) }} orang x {{ ((int)$interval->format('%a') + 1) }} hari x Rp. {{ number_format($row->spuh_rate,2,',','.') }}
                </td>
                
                <td style="vertical-align:top;text-align: right;">
                Rp. {{number_format($doc->act_price,2,',','.')}}
                </td>
            </tr>
            <tr>
                <td style="vertical-align:top;"></td>
                <td style="vertical-align:top;">Sudah diterima pembayaran</td>
                <td style="vertical-align:top;"></td>
                
                <td style="vertical-align:top;text-align: right;">
                (Rp. {{number_format($doc->price,2,',','.')}})
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center;"><b>Total Uang Harian</b></td>
                <td style="text-align: right;">Rp. {{number_format($doc->act_price - $doc->price,2,',','.')}}</td>
            </tr>
        </table>

        <table style="width: 100%;" class="f10" border="1">
            <tr>
                <td style="text-align: center; border: none;">
                Pembayaran dilakukan melalui Transfer ke Rekening Penerimaan Lainnya Balai Pengujian UTTP
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border: none;">
                <strong>Bank Mandiri Cabang RSHS Bandung</strong><br/>
                <strong>a.n. RPL 022 PS BPUTTP UTK UHPP</strong></br/>
                <strong>Nomor Rekening 132-00-2419193-5</strong><br/>
                <strong>(Cantumkan No. Tagihan atau No. Pendaftaran)</strong>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border: none; padding-top: 10px;">
                Konfirmasi Pembayaran melalui aplikasi <strong>SIMPEL UPTP IV</strong>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border: none;">
                <strong>Pembayaran dilakukan paling lambat 1 (satu) hari sebelum tanggal pelaksanaan</strong>
                </td>
            </tr>
        </table>

        @if(!$loop->last)
        <div style="page-break-after: always;"></div>
        @endif
        

    @endif
    @endforeach
    @endif
</body>
</html>