@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(['url' => route('requestluaruttp.simpanbookinginspection', $item->id)])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Item Pengujian/pemeriksaan</h4>
            </div>
            <div class="panel-body">
                <table id="table" class="table table-responsive-sm input-table">
                    <thead>
                        <tr>
                            <th>Diuji</th>
                            <th>Jenis Pengujian</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                            <th>Harga Satuan</th>
                            <th>Potongan</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($prices as $price)
                        <tr>
                            <td>
                                <?php
                                    $checked = false;
                                    $quantity = null;
                                    $price_price = $price->price;
                                    foreach($item->inspections as $inspection) {
                                        if ($inspection->inspection_price_id == $price->id) {
                                            $checked = true;
                                            $quantity = $inspection->quantity;
                                            $price_price = (double)$inspection->price;
                                            break;
                                        }
                                    } 
                                ?>
                                <input type="hidden" name="id[]" value="{{ $price->id }}" />
                                <input type="checkbox" name="price_selected[]" data-id="{{ $price->id }}" class="price_selected" 
                                    {{ $checked ? 'checked' : '' }}
                                />
                            </td>
                            <td>{{ $price->inspection_type }}</td>
                            <td>
                                <input {{ $checked ? '' : 'readonly="readonly"' }} type="number" name="quantity[]" id="quantity" data-id="{{ $price->id }}" class="form-control quantity" 
                                    value="{{ $quantity }}"
                                />
                            </td>
                            <td>{{ $price->unit }}</td>
                            <td>
                                <input {{ $checked ? '' : 'readonly="readonly"' }} type="number" name="price[]" id="price" data-id="{{ $price->id }}" class="form-control price" 
                                    value="{{ $price_price }}"/>
                                <input type="hidden" name="hidden_price[]" id="hidden_price" data-id="{{ $price->id }}" class="form-control hidden_price" 
                                    value="{{ $price_price }}"/>
                            </td>
                            <td>
                                <input readonly type="number" name="subtotal[]" id="subtotal" data-id="{{ $price->id }}" class="form-control subtotal" 
                                value="{{ (double)($quantity * $price_price) }}"/>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4"></td>
                            <td>Total: </td>
                            <td><input readonly type="number" name="subtotal_item" id="subtotal_item" class="form-control" value="{{ number_format( ((double)$item->subtotal),0,'.','') }}"/></td>
                        </tr>                            
                    </tfoot>
                </table>
            </div>
        </div>

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Lanjut</button> 
        </form>
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        
        $('.price_selected').click(function(e) {
            console.log($(this).is(':checked'));
            var id = $(this).data('id');
            qty = $('.quantity[data-id=' + id + ']');
            if ($(this).is(':checked')) {
                $('.quantity[data-id=' + id + ']').attr('readonly', false);
                $('.price[data-id=' + id + ']').attr('readonly', false);
            } else {
                $('.quantity[data-id=' + id + ']').attr('readonly', true);
                $('.quantity[data-id=' + id + ']').val(null).trigger('change');
                $('.price[data-id=' + id + ']').attr('readonly', true);
                $('.price[data-id=' + id + ']').val($('.hidden_price[data-id=' + id + ']').val()).trigger('change');
            }
        })

        $('.quantity').change(function() {
            
            var id = $(this).data('id');
            
            var qty = $(this).val();
            if (qty == null) {
                qty = 0;
            }
            console.log(qty);
            $('.subtotal[data-id=' + id + ']').val(qty * $('.price[data-id=' + id + ']').val());

            var sum = 0;
            $('.subtotal').each(function(){
                sum += parseFloat($(this).val());  
            });
            console.log(sum);
            $('#subtotal_item').val(sum);
        })

        $('.price').change(function() {
            
            var id = $(this).data('id');
            
            var prc = $(this).val();
            if (prc == null) {
                prc = 0;
            }
            console.log(prc);
            $('.subtotal[data-id=' + id + ']').val(prc * $('.quantity[data-id=' + id + ']').val());

            var sum = 0;
            $('.subtotal').each(function(){
                sum += parseFloat($(this).val());  
            });
            console.log(sum);
            $('#subtotal_item').val(sum);
        })
    });

</script>
@endsection