@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <form id="form_create_request">
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pemilik</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="uttp_owner_id">Nama Pemilik</label>
                            {!! Form::hidden('uttp_owner_id', $request->uttp_owner_id, ['id' => 'uttp_owner_id']) !!}
                            {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Pemilik</label>
                            {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>  
                </div>
            </div>
        </div>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pemohon</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Nama Pemohon</label>
                            {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                        </div>
                    </div>
                    
                </div>
                
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_surat_permohonan">No Surat Permohonan</label>
                            {!! Form::text('no_surat_permohonan', $request->no_surat_permohonan, ['class' => 'form-control','id' => 'no_surat_permohonan', 'placeholder' => 'No Surat Permohonan', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tgl_surat_permohonan">Tanggal Surat Permohonan</label>
                            {!! Form::text('tgl_surat_permohonan', date("d-m-Y", strtotime(isset($request->tgl_surat_permohonan) ? $request->tgl_surat_permohonan : date("Y-m-d"))), ['class' => 'form-control','id' => 'tgl_surat_permohonan', 'placeholder' => 'Tanggal Surat Permohonan', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="file_surat_permohonan">Surat Permohonan</label>
                            <div class="input-group">
                                <a href="<?= config('app.siks_url') ?>/tracking/download_permohonan/{{ $request->id }}" class="btn btn-warning btn-sm">DOWNLOAD</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_from">Usulan Jadwal Mulai</label>
                            {!! Form::text('scheduled_test_date_from', date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_from', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_to">Usulan Jadwal Selesai</label>
                            {!! Form::text('scheduled_test_date_to', date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_to', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lokasi_pengujian">Lokasi Pengujian</label>
                            {!! Form::text('jenis_layanan', $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor', ['class' => 'form-control','id' => 'lokasi_pengujian_view', 'readonly']) !!}
                            <!--
                            <select class="form-control" name="lokasi_pengujian" id="lokasi_pengujian">
                                <option value="dalam" <?= $request->lokasi_pengujian == 'dalam' ? 'selected' : '' ?> >Dalam Kantor</option>
                                <option value="luar" <?= $request->lokasi_pengujian == 'luar' ? 'selected' : '' ?>>Luar Kantor</option>  
                            </select>
                            -->
                        </div>
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="jenis_layanan">Potongan Tersedia</label>
                            
                        </div>
                    </div>
                    <form id="potong" name="potong" methode="POST" action="{{}}">
                        {!! Form::hidden('req_id', $request->id, ['id' => 'req_id']) !!}
                        {!! Form::hidden('origin_price', number_format(isset($request->origin_price) ? $request->origin_price : 0 ,'2',',','.'), ['id' => 'origin_price']) !!}
                        {!! Form::hidden('total',number_format(($request->total_price),2,',','.'), ['id' => 'total']) !!}
                        <div class="col-md-7">
                            <div class="form-group">
                                {!! Form::checkbox('potongan[]',"50",isset($request->persen_potongan) && $request->persen_potongan > 0 ? $request->persen_potongan : '', ['id' => 'potongan_val', 'readonly']) !!} 50%
                            </div>
                            
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                            <a id="btn_potongan" name="btn_potongan" class="btn btn-warning btn-sm">Simpan</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        

        <!-- Tes-->

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Item Pengujian</h4>
            </div>

            <div class="panel-body">
                <table class="table table-striped table-hover table-responsive-sm" id="t-roll">
                <thead style="font-weight:bold; color:white;">
                    <tr>
                        <th>No</th>
                        <th>Jenis Alat Ukur</th>
                        <th>Merek</th>
                        <th>Model/Tipe</th>
                        <th>No.Seri</th>
                        <th>Media Uji</th>
                        <th>Max.Capacity</th>
                        <th>Min.Capacity</th>
                        <th>Satuan</th>
                        <th>Buatan</th>
                        <th>Pabrikan</th>
                        <th>Alamat Pabrik</th>
                        <th style="width: 200px;">Alamat Lokasi Pengujian</th>
                        <th style="width: 200px;">Perlengkapan</th>
                        <th style="width: 200px;">Item Pengujian</th>
                        <th>Total Biaya</th>
                        <th></th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php $num=0 ?>
                    @foreach($request->items as $item)
                        <?php $num +=1 ?>
                        <tr>
                            <form id="form_item_{{ $item->id }}">
                                {!! Form::hidden('id', $item->id) !!}
                                {!! Form::hidden('uttp_id', $item->uttp_id) !!}
                                {!! Form::hidden('uttp_type_id', $item->uttp->type_id) !!}
                                <td>{{ $num}}</td>
                                <td>{{ $item->uttp->type->uttp_type}}</td>
                                <td>{{ $item->uttp->tool_brand}}</td>
                                <td>{{ $item->uttp->tool_model}}</td>
                                <td>{{ $item->uttp->serial_no}}</td>
                                <td>{{ $item->uttp->tool_media}}</td>
                                <td>{{ $item->uttp->tool_capacity}}
                                <td>{{ $item->uttp->tool_capacity_min}}</td>
                                <td>{{ $item->uttp->tool_capacity_unit }}</td>
                                <td>{{ $item->uttp->tool_made_in }}</td>
                                <td>{{ $item->uttp->tool_factory }}</td>
                                <td>{{ $item->uttp->tool_factory_address }}</td>
                                <td>{{ $item->location }}, {{ ($item->kabkot ? $item->kabkot->nama : '') . ', ' . ($item->provinsi ? $item->provinsi->nama : '') }}</td>
                                <td>
                                    @foreach($item->perlengkapans as $perlengkapan)
                                        {{ $perlengkapan->uttp->type->uttp_type }}: {{ $perlengkapan->uttp->serial_no }} ({{ $perlengkapan->uttp->tool_brand }}/{{ $perlengkapan->uttp->tool_model }})
                                        @if($loop->remaining)
                                        <br/>
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($item->inspections as $inspection)
                                        {{ $inspection->inspectionPrice->inspection_type }}
                                    @endforeach
                                </td>
                                <td>{{ number_format($item->subtotal, 2, ',', '.') }}</td>
                                <td> <a href="{{ route('requestluaruttp.createbookinginspection', $item->id) }}"
                                class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_add_inspeksi">
                                <i class="fa fa-edit faa-flash"></i> Item Uji</a> </td>
                                
                            </form>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            
        </div>

        </form>

        <a href="{{ route('pembayaranluaruttp') }}#frontdesk_penagihan_luar"
                                class="btn btn-w-md btn-accent">Kembali</a> 
    </div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        var id = $("#req_id").val();
        var data = 0;
        var origin_price = $("#origin_price").val();
        var to_disc =  $('#total').val();
        var url = "{{url('requestluaruttp/disc/:id/:dis')}}".replace(':id', id).replace(':dis', data);
        $("form div #btn_potongan").click(function(e)
        {   if( $("#potongan_val").prop("checked") ){
                e.preventDefault();
                id = $("#req_id").val();
                data = $("#potongan_val").val();
                url = "{{url('requestluaruttp/disc/:id/:dis')}}".replace(':id', id).replace(':dis', data)
            }
            console.log(url);
            $.ajax({
                type: "POST",
                url: url,
                data:{
                    price:data,
                    req_id:id,
                    disc : $("#potongan_val").prop("checked") ? $('#potongan_val').val() : null,
                    origin_price: origin_price,
                    total: to_disc,
                    _token:"{{ csrf_token() }}",
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data[1])
                    if(data[1]=='success')
                    {
                        toastr["success"]("Data berhasil diubah", "Success");
                        setTimeout(function()
                        {
                            window.location.href = "{{url('/requestluaruttp/editpnbp/')}}/" + id;
                        },1000);
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $('#t-roll').DataTable({
            "scrollX": true
        });
        $('#booking_id, #lokasi_pengujian, .negara, #is_complete').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });
        /*
        $('#uttp_owner_id').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true,
            ajax: {
                url: '{{ route('requestuttp.getowners') }}',
                data: function (params) {
                    var query = {
                        q: params.term,
                    }
                    
                    return query;
                },
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.nama };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            var route = "{{ route('requestuttp.getowner', ':id') }}";
            route = route.replace(':id', e.params.data.id);
            $.get(route,function(res)
            {
                $('#addr_sertifikat').val(res.alamat);
            });
        });
        */

        $('.satuan').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true,
            tags: true,
            ajax: {
                url: function (params) {
                    var form = $(this).closest('form');
                    var type = form.find('input:hidden[name=uttp_type_id]').val();
                    var route = "{{ route('requestuttp.getunits', ':id') }}";
                    route = route.replace(':id', type);
                    return route;
                },
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.unit, text: obj.unit };
                        })
                    };
                },
            }
        });

        /*
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        */

        $('#btn_simpan').click(function(e){
            e.preventDefault();
            
            $('#btn_submit').attr('disabled', true);
            $('#btn_simpan').attr('disabled', true);

            toastr["warning"]("Sedang menyimpan. Harap tunggu.");

            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuttp.simpaneditbooking', $request->id) }}',form_data,function(response)
            {

                $('#btn_submit').attr('disabled', false);
                $('#btn_simpan').attr('disabled', false);

                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}';
                }
                else
                {
                    //var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksa kembali","Form Invalid");
                }
            });

        });

        $('#btn_submit').click(function(e){
            e.preventDefault();

            $('#btn_submit').attr('disabled', true);
            $('#btn_simpan').attr('disabled', true);

            toastr["warning"]("Sedang menyimpan. Harap tunggu.");
            
            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuttp.submitbooking', $request->id) }}',form_data,function(response)
            {
                $('#btn_submit').attr('disabled', false);
                $('#btn_simpan').attr('disabled', false);

                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}';
                }
                else
                {
                    //var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksa kembali. Pilih item uji.","Form Invalid");
                }
            });

            
        });

        $('.btn-save-edit').click(function(e){
            e.preventDefault();
            var id = $(this).data().id;
            
            var form_data = $("#form_item_" + id).serialize();

            form_data += '&_token={{ csrf_token() }}';

            var postroute = "{{ route('requestuttp.edititem', ':id') }}";
            postroute = postroute.replace(':id', id);
            
            $.post(postroute,form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = "{{ route('requestuttp.editbooking', $request->id) }}";
                }
                else
                {
                    //var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('.btn-history').click(function(e){
            e.preventDefault();
            var id = $(this).data().id;
            
            var route = "{{ route('requestuttp.gethistory', ':id') }}";
            route = route.replace(':id', id);
            //$.get(route,function(res)
            //{
                
            //});
            $("#inspection_history").DataTable().destroy();
            $("#inspection_history").DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: route,
                    type: 'GET'
                },
                "ajax": function(data, callback, settings) {
                    
                    $.ajax({
                        url: route,
                        type: "GET",
                        success: function(res) {
                            console.log(res); 
                            callback({
                                recordsTotal: res.length,
                                recordsFiltered: res.length,
                                data: res
                            });
                        }
                    });

                },
                "columns": [
                    { data: 'no_register' },
                    { data: 'no_order' },
                    { data: 'no_sertifikat' },
                    { data: 'inspections' },
                    { data: 'staff_entry_datein' },
                    { data: 'staff_entry_dateout' },
                ],
            });

            $('#historyModal').modal('show');
            

        });
    });

</script>
@endsection