@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>Master Data<br> <span class="c-white">Menu</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-user"></i>
            </div>
            <div class="header-title">
                <h3>Menu</h3>
                <small>
                    Halaman manajemen menu.
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tMenu" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Nama Menu</th>
                        <th>URL</th>
                    </tr>
                </thead>
                <tbody>
                    @if($users->count())
                    @foreach($users as $user)
                    <tr>
                        <td>{{ ucfirst($user->username) }}</td>
                        <td>{{ $user->full_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->updated_at }}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function () {
        
        $('#tUser').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ]
        });

    });
</script>
@endsection