@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    
    <div class="panel panel-filled table-area">
        <div class="panel-body">
            
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Diterima Oleh</th>
                        <th>Diuji Oleh</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr id="{{ $row->id }}">
                        <td>{{ $row->ServiceRequestItem->no_order }}</td>
                        <td>{{ $row->ServiceRequestItem->uttp->tool_brand }}/{{ $row->ServiceRequestItem->uttp->tool_model }}/{{ $row->ServiceRequestItem->uttp->tool_type }} ({{ $row->ServiceRequestItem->uttp->serial_no ? $row->ServiceRequestItem->uttp->serial_no : '-' }})</td> 
                        <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : '' }}</td>
                        <td>{{ $row->TestBy1 ? $row->TestBy1->full_name : "" }} {{ $row->TestBy2 ? ' & '.$row->TestBy2->full_name : "" }}</td>
                        <td>{{ $row->staff_entry_datein }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>{{ $row->ServiceRequestItem->status->status }}
                        <td>
                        @if($row->stat_service_order=="2")
                            @if($row->file_skhp!==null)
                            <a href="{{ route('approveuttp.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('approveuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuttp.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP TIPE</a>
                            <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP TIPE</a>
                            @endif
                            <a href="{{ route('approveuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                            @endif
                        @elseif($row->stat_service_order=="4")
                            <a href="{{ route('approveuttp.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('approveuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                        @elseif($row->is_finish=="0")
                            @if($row->file_skhp!==null)
                            <a href="{{ route('approveuttp.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('approveuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuttp.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP TIPE</a>
                            <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP TIPE</a>
                            @endif
                            <a href="{{ route('approveuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                            @endif
                        @elseif($row->is_finish=="1")
                        SELESAI
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    let table = $('#data_table').DataTable({
        scrollX: true,
    });

   
});

</script>
@endsection