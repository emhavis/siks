@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">

    <ul class="nav nav-tabs" role="tablist" id="tabs">
        <li role="presentation" class="active">
            <a href="#kn" aria-controls="kn" role="tab" data-toggle="tab" 
                class="badge-notif-tab">Dalam Kantor</a>
        </li>
        <li role="presentation">
            <a href="#dl" aria-controls="dl" role="tab" data-toggle="tab" 
                class="badge-notif-tab">Dinas Luar</a>
        </li>
    </ul>
    
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="kn">
            <br/>  
            <div class="panel panel-filled table-area">
                <div class="panel-body">
                    
                    <table id="data_table_kn" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Tgl Mulai SLA</th>
                                <th>Status SLA (hari)</th>
                                <th>Status</th>
                                <th>Berkas</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_kn as $row)
                            <tr id="{{ $row->id }}">
                                <td>{{ $row->no_order }}</td>
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>{{ $row->lab_staff }}</td>
                                <td>{{ $row->test_by_1 ? $row->test_by_1 : "" }} {{ $row->test_by_2 ? ' & '.$row->test_by_2 : "" }}</td>
                                <td>{{ date('d-m-Y', strtotime($row->staff_entry_datein)) }}</td>
                                <td>
                                    @if($row->cancel_at == null)
                                    {{ date('d-m-Y', strtotime($row->staff_entry_dateout)) }}
                                    @else
                                    BATAL: {{ date('d-m-Y', strtotime($row->cancel_at)) }}
                                    @endif
                                </td>
                                <td>{{ date('d-m-Y', strtotime($row->order_at)) }}</td>
                                <td>
                                    @if($row->sla_status_day == 'GREEN')
                                    <span class="label label-success">
                                    @elseif($row->sla_status_day == 'YELLOW')
                                    <span class="label label-warning">
                                    @else
                                    <span class="label label-danger">
                                    @endif
                                    {{ $row->sla_days }}</span>
                                </td>
                                <td>
                                {{ $row->status }}<br/>
                                    Draft: {{ $row->stat_sertifikat == 3 ? 'Sudah Disetujui Ka Balai' :
                                        ($row->stat_sertifikat == 2 ? 'Sudah Disetujui Ketua Tim' : 'Penyusunan Draft' )}}<br/>
                                    Alat: {{ $row->stat_warehouse == 2 ? 'Sudah Diserahterimakan dengan Pemohon' : 
                                                ($row->stat_warehouse == 1 ? 'Berada di Gudang' : 'Berada di Instalasi')}}
                                </td>
                                <td>
                                    <a target="_blank" href="{{ route('requestuttp.pdf', $row->request_id) }}" class="btn btn-warning btn-sm">Invoive</a>
                                    <a target="_blank" href="{{ route('requestuttp.buktiorder', $row->request_id) }}" class="btn btn-warning btn-sm">Order</a>
                                    <a target="_blank" href="{{ route('requestuttp.kuitansi', $row->request_id) }}" class="btn btn-warning btn-sm">Kuitansi</a>
                                    @if($row->stat_warehouse ==2)
                                        <a target="_blank" href="{{ route('warehousehistoryuttp.print', $row->id) }}" class="btn btn-warning btn-sm">Bukti Serah Terima</a>
                                    @endif
                                </td>
                                <td>
                                    @if($row->cancel_at != null)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->file_skhp!==null)
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->service_type_id == 6 || $row->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                    <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="dl">
            <br/>  
            <div class="panel panel-filled table-area">
                <div class="panel-body">
                    
                    <table id="data_table_dl" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Tgl Mulai SLA</th>
                                <th>Status SLA (hari)</th>
                                <th>Status</th>
                                <th>Berkas</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_dl as $row)
                            <tr id="{{ $row->id }}">
                                <td>{{ $row->no_order }}</td>
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>{{ $row->lab_staff }}</td>
                                <td>{{ $row->test_by_1 ? $row->test_by_1 : "" }} {{ $row->test_by_2 ? ' & '.$row->test_by_2 : "" }}</td>
                                <td>{{ date('d-m-Y', strtotime($row->staff_entry_datein)) }}</td>
                                <td>
                                    @if($row->cancel_at == null)
                                    {{ date('d-m-Y', strtotime($row->staff_entry_dateout)) }}
                                    @else
                                    BATAL: {{ date('d-m-Y', strtotime($row->cancel_at)) }}
                                    @endif
                                </td>
                                <td>{{ date('d-m-Y', strtotime($row->order_at)) }}</td>
                                <td>
                                    @if($row->sla_status_day == 'GREEN')
                                    <span class="label label-success">
                                    @elseif($row->sla_status_day == 'YELLOW')
                                    <span class="label label-warning">
                                    @else
                                    <span class="label label-danger">
                                    @endif
                                    {{ $row->sla_days }}</span>
                                </td>
                                <td>
                                {{ $row->status }}<br/>
                                    Draft: {{ $row->stat_sertifikat == 3 ? 'Sudah Disetujui Ka Balai' :
                                        ($row->stat_sertifikat == 2 ? 'Sudah Disetujui Ketua Tim' : 'Penyusunan Draft' )}}
                                </td>
                                <td>
                                    <a target="_blank" href="{{ route('requestluaruttp.pdf', $row->request_id) }}" class="btn btn-warning btn-sm">Invoive</a>
                                    <a target="_blank" href="{{ route('requestuttp.buktiorder', $row->request_id) }}" class="btn btn-warning btn-sm">Order</a>
                                    <a target="_blank" href="{{ route('requestuttp.kuitansi', $row->request_id) }}" class="btn btn-warning btn-sm">Kuitansi</a>
                                    @if($row->lokasi_pengujian == 'luar')
                                        <a target="_blank" href="{{ env('SKHP_URL') . '/publicterm/create/'.$row->booking_id }}" class="btn btn-warning btn-sm">Kaji Ulang</a>
                                    @endif
                                    @if($row->spuh_doc_id != null)
                                    <a href="{{ route('schedulinguttp.surattugas', ['id' => $row->request_id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Surat Tugas</a>
                                    @endif
                                    @if($row->is_integritas == true)
                                    <a target="_blank" href="{{ route('docinsituuttp.integritas', $row->request_id) }}" class="btn btn-warning btn-sm">Pakta Integritas</a>
                                    @endif
                                </td>
                                <td>
                                    @if($row->cancel_at != null)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->file_skhp!==null)
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->service_type_id == 6 || $row->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                    <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    let table = $('#data_table_dl, #data_table_kn').DataTable({
        scrollX: true,
    });

   
});

</script>
@endsection