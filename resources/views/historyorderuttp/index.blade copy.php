@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    
    <div class="panel panel-filled table-area">
        <div class="panel-body">

            {!! Form::open(['url' => route('approveuttp.approvesubmitall'), 'files' => true])  !!}
            <input type="hidden" name="ids" id="ids" />
            <button role="submit" class="btn btn-w-md btn-success" id="btn_simpan" disabled>Setujui Terpilih</button>
            {!! Form::close() !!}

            <div class="row"><div class="col">&nbsp;</div></div>
            
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th data-orderable="false">{!! Form::checkbox('select_all', 'select_all', false, ['class' => 'form-check-input', 'id' => 'select_all' ]) !!}</th>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Diterima Oleh</th>
                        <th>Diuji Oleh</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr id="{{ $row->id }}">
                        <td>{!! Form::checkbox('row_selected', 'row_selected', false, ['class' => 'form-control', ]) !!}</td>
                        <td>{{ $row->ServiceRequestItem->no_order }}</td>
                        <td><a onclick="showToolCodeInfo('{{ $row->ServiceRequestItem->uttp->id }}')" href="#">
                            {{ $row->ServiceRequestItem->uttp->tool_brand }}/{{ $row->ServiceRequestItem->uttp->tool_model }}/{{ $row->ServiceRequestItem->uttp->tool_type }} ({{ $row->ServiceRequestItem->uttp->serial_no ? $row->ServiceRequestItem->uttp->serial_no : '-' }})
                        </a></td> 
                        <td>{{ $row->MasterUsers->full_name }}</td>
                        <td>{{ $row->TestBy1 ? $row->TestBy1->full_name : "" }} {{ $row->TestBy2 ? ' & '.$row->TestBy2->full_name : "" }}</td>
                        <td>{{ $row->staff_entry_datein }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>
                            @if($row->stat_sertifikat==null)
                                @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                <a href="{{ route('approveuttp.approvekalab', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN 1</a>
                                @elseif($row->ServiceRequest->service_type_id == 4 || $row->ServiceRequest->service_type_id == 5)
                                <a href="{{ route('approveuttp.approvesubko', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN 1</a>
                                @endif
                            @elseif($row->stat_sertifikat==1)
                            <a href="{{ route('approveuttp.approvesubko', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN 2</a>
                            @elseif($row->stat_sertifikat==2)
                            <a href="{{ route('approveuttp.approve', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN 2</a>
                            @endif
                        </td>
                        <td>
                        @if($row->stat_service_order=="2")
                            @if($row->file_skhp!==null)
                            <a href="{{ route('approveuttp.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('approveuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuttp.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP TIPE</a>
                            <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP TIPE</a>
                            @endif
                            <a href="{{ route('approveuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                            @endif
                        @elseif($row->stat_service_order=="4")
                            <a href="{{ route('approveuttp.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('approveuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                        @elseif($row->is_finish=="0")
                            @if($row->file_skhp!==null)
                            <a href="{{ route('approveuttp.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('approveuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuttp.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP TIPE</a>
                            <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP TIPE</a>
                            @endif
                            <a href="{{ route('approveuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                            @endif
                        @elseif($row->is_finish=="1")
                        SELESAI
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <small class="stat-label">Jenis Besaran</small>
                        <h4 class="m-t-xs standard_measurement_type_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Jenis Alat</small>
                        <h4 class="m-t-xs standard_tool_type_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Rincian Alat</small>
                        <h4 class="m-t-xs standard_detail_type_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Merk / Buatan</small>
                        <h4 class="m-t-xs brand-made_in"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Model / Tipe</small>
                        <h4 class="m-t-xs model-tipe"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">No. Seri / No. Identitas</small>
                        <h4 class="m-t-xs no_seri-no_identitas"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Kapasitas / Daya Baca</small>
                        <h4 class="m-t-xs capacity-daya_baca"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Kelas</small>
                        <h4 class="m-t-xs kelas"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Sumber</small>
                        <h4 class="m-t-xs sumber_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Jumlah Per Set</small>
                        <h4 class="m-t-xs jumlah_per_set"></h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    let table = $('#data_table').DataTable({
        "order": [[ 1, "asc" ]],
        columnDefs: [ {
            targets: 0,
            orderable: false,
            searchable: false,
        } ],
        select: {
            style: 'multi',
            selector: 'td:first-child',
        },
        scrollX: true,
    });

    $('#data_table tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');

        //console.log(table.rows('.selected').data());
        //console.log(table.rows('.selected').data().length +' row(s) selected');
        var cb = $(this).find("> td:first-child > input:checkbox");
        
        //console.log($(this).hasClass('selected'));
        cb.prop('checked', $(this).hasClass('selected'));

        var rows = table.rows('.selected').ids();
        $('#ids').val(rows.join(","));
        $('#btn_simpan').prop("disabled", false);
    } );

    $("#select_all").change(function() {
        //console.log(table.rows());
        //console.log(table.rows('.selected').data().length +' row(s) selected');

        if ($(this).is( ":checked" )) {
            table.rows().nodes().to$().addClass('selected');     
            table.rows().nodes().to$().find("> td:first-child > input:checkbox").prop('checked', true);
        } else {
            table.rows().nodes().to$().removeClass('selected');
            table.rows().nodes().to$().find("> td:first-child > input:checkbox").prop('checked', false);
        }

        var rows = table.rows('.selected').ids();
        $('#ids').val(rows.join(","));
        $('#btn_simpan').prop("disabled", false);
    });
});

function showToolCodeInfo(toolCode)
{
    $.post("{{ route('standard.tool_code') }}",{"tool_code":toolCode},function(response){
    
    var res = response[0];
    $("#myModal h4.modal-title").text(res.tool_code);
    $("#myModal h4.standard_measurement_type_id").text(res.standard_type);
    $("#myModal h4.standard_tool_type_id").text(res.attribute_name);
    $("#myModal h4.standard_detail_type_id").text(res.standard_detail_type_name);
    $("#myModal h4.brand-made_in").text(res.brand+" / "+res.nama_negara);
    $("#myModal h4.model-tipe").text(res.model+" / "+res.tipe);
    $("#myModal h4.no_seri-no_identitas").text(res.no_seri+" / "+res.no_identitas);
    $("#myModal h4.capacity-daya_baca").text(res.capacity+" / "+res.daya_baca);
    $("#myModal h4.kelas").text(res.class);
    $("#myModal h4.sumber_id").text(res.nama_sumber);
    $("#myModal h4.jumlah_per_set").text(res.jumlah_per_set);
    $("#myModal").modal();

    },"json");
}
</script>
@endsection