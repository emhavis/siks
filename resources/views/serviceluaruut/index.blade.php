@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-12">
                {!! Form::open(['url' => route('serviceluaruut.savetestqr'), 'id' => 'qr'])  !!}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="no_order">Selesai Uji Berdasar QR Code</label>
                                    {!! Form::text('no_order', null, ['class' => 'form-control','id' => 'no_order', 'autofocus' => 'autofocus']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="error_notes" clsas=""></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/>
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Pengujian</th>
                        <th>Diterima Oleh</th>
                        <th>Diuji Oleh</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th>Berkas</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequestItem ? $row->ServiceRequestItem->no_order :'' }}</td>
                        <td>{{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                        <td>
                            @foreach($row->ServiceRequestItem->inspections as $inspection)
                            {{ $inspection->inspectionPrice->inspection_type }}
                            <br/>
                            @endforeach
                        </td>
                        <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : "" }}</td>
                        <td>{{ $row->test1 ? $row->test1->full_name : "" }} {{ $row->test2 ? ' & '.$row->test2->full_name : "" }}</td>
                        <td>{{ date("d-m-Y", strtotime($row->staff_entry_datein)) }}</td>
                        <td>{{ $row->staff_entry_dateout != null ? date("d-m-Y", strtotime($row->staff_entry_dateout)) : '' }}</td>
                        
                        <td>
                            <?php if ($row->ServiceRequest->service_type_id == 1 || $row->ServiceRequest->service_type_id == 2): ?>
                                <!-- <a href="<?= config('app.siks_url') ?>/tracking/download_uut/type_approval_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Persetujuan Tipe</a> -->
                                <?php if($row->ServiceRequestItem->file_manual_book !=''): ?>
                                    <a href="<?= config('app.siks_url') ?>/tracking/download_uut/manual_book/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Buku Manual</a>
                                <?php endif ?>

                                <?php if($row->ServiceRequest->booking->file_certificate !=null): ?>
                                    <a href="<?= config('app.siks_url') ?>/booking/download_permohonan/certificate/{{ $row->ServiceRequest->booking->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                                <?php endif ?>

                                
                            <?php elseif ($row->ServiceRequest->service_type_id == 5): ?>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Sertifikat Sebelumnya</a>
                            <?php elseif ($row->ServiceRequest->service_type_id == 1): ?>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                            <?php endif ?>
                        </td>
                        <td>
                        @if($row->stat_service_order=="0")
                            @if($row->pending_status=="0" || $row->pending_status==null)
                            <a href="{{ route('serviceluaruut.test', $row->id) }}" class="btn btn-warning btn-sm">SELESAI UJI</a>
                            <a href="{{ route('serviceluaruut.pending', $row->id) }}" class="btn btn-danger btn-sm">TUNDA</a>
                            @else
                            <a href="{{ route('serviceluaruut.continue', $row->id) }}" class="btn btn-info btn-sm">LANJUT UJI</a>
                            @endif
                            <a href="{{ route('serviceluaruut.cancel', $row->id) }}" class="btn btn-danger btn-sm">BATAL UJI</a>
                        @elseif($row->stat_service_order=="1")
                            @if($row->stat_sertifikat==0)
                            <a href="{{ route('serviceluaruut.result', $row->id) }}" class="btn btn-warning btn-sm">HASIL UJI</a>
                            @endif
                            @if($row->file_skhp!==null)
                            <a href="{{ route('serviceluaruut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                            <a href="{{ route('serviceluaruut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceluaruut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                            <a href="{{ route('serviceluaruut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                            @endif
                            <a href="{{ route('serviceluaruut.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD CERAPAN</a>
                                @if($row->file_lampiran!==null)
                                <a href="{{ route('serviceluaruut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                @endif
                            @endif
                            <!-- @if($row->stat_warehouse ==null || $row->stat_warehouse ==0)
                                <a href="{{ route('serviceluaruut.warehouse', $row->id) }}" class="btn btn-warning btn-sm">GUDANG ALAT SELESAI</a>
                            @endif -->
                        @elseif($row->stat_service_order=="2")
                            <a href="{{ route('serviceluaruut.result', $row->id) }}" class="btn btn-warning btn-sm">HASIL UJI</a>
                            @if($row->file_skhp!==null)
                            <a href="{{ route('serviceluaruut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                            <a href="{{ route('serviceluaruut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceluaruut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                            <a href="{{ route('serviceluaruut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                            @endif
                            <a href="{{ route('serviceluaruut.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD CERAPAN</a>
                                @if($row->file_lampiran!==null)
                                <a href="{{ route('serviceluaruut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                @endif
                            @endif

                            <!-- @if($row->stat_warehouse =="0" || $row->stat_warehouse ==null)
                                <a href="{{ route('serviceluaruut.warehouse', $row->id) }}" class="btn btn-warning btn-sm">GUDANG ALAT SELESAI</a>
                            @endif -->
                        @elseif($row->stat_service_order=="4")
                            <a href="{{ route('serviceluaruut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                            <a href="{{ route('serviceluaruut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceluaruut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                            <a href="{{ route('serviceluaruut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                            @endif
                        @elseif($row->is_finish=="0")
                            @if($row->file_skhp!==null)
                                <a href="{{ route('serviceluaruut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                <a href="{{ route('serviceluaruut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                @if($row->ujitipe_completed==true)
                                    <a href="{{ route('serviceluaruut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceluaruut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                @endif
                                <a href="{{ route('serviceluaruut.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD CERAPAN</a>
                                @if($row->file_lampiran!==null)
                                <a href="{{ route('serviceluaruut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                @endif
                            @endif
                            <!-- @if($row->stat_warehouse =="0" || $row->stat_warehouse ==null)
                                <a href="{{ route('serviceluaruut.warehouse', $row->id) }}" class="btn btn-warning btn-sm">GUDANG ALAT SELESAI</a>
                            @endif -->
                        @elseif($row->is_finish=="1")
                        SELESAI
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('#data_table').DataTable({
        scrollX: true,
    });

    $("#no_order").change(function() {
        var no_order = $(this).val();

        var form = $('#qr');
        form[0].submit();

        /*
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{ route('orderuttp.proses') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
        */
    });
});

</script>
@endsection