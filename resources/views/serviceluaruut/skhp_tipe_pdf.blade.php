<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Sertifikat {{ $order->ServiceRequestItem->ServiceRequest->svc ? $order->ServiceRequestItem->ServiceRequest->svc->service_type :'' }}</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: Arial;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
            /* border :2px solid;
            color :red; */
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        /* @media print {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }

            button {display: none;}

            .page{border: #fff solid 0px;}

            body {margin: 0;}
        } */
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-nodraft-snsu.png") : public_path("assets/images/logo/letterhead-nodraft-snsu.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-draft-snsu.png") : public_path("assets/images/logo/letterhead-draft-snsu.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            opacity: 0.6;
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
        .table td .eng {
            font-size:8pt;
            font-style:italic;
        }

        .eng {
            font-size:10pt;
            font-style:italic;
        }

        #underline-gap {
            text-decoration: underline;
            text-underline-position: under;
        }
    </style>
</head>

<body class="A4">

    <section class="sheet {{ $order->kabalai_date ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order != null ? $order->ServiceRequestItem->no_order : 'X-XXX-XXX'}}</div>

        <div class="text-center">
            <div class="title">
                <strong>SERTIFIKAT {{ strtoupper($order->ServiceRequestItem->ServiceRequest->svc ? $order->ServiceRequestItem->ServiceRequest->svc->service_type :'') }}</strong>
            </div>
            <div class="subtitle">
                <i>{{ strtoupper($order->ServiceRequestItem->ServiceRequest->svc->service_type) =='VERIFIKASI' ? 'Verification' :'Calibration'}} certificate </i>
            </div>
        </div>
        </br>
        <div class="text-center">
            <div class="subtitle"><strong>Nomor : {{ $order->no_sertifikat !='' ? $order->no_sertifikat : ' 0000/00/00/00/00 '}}</strong></div>
        </div>

        <br/><br/>
    
        <table class="table table-borderless" width="100%" cellpadding="2px" cellspacing="0"s
         style="padding-left: 25.4mm; padding-right: 25.4mm; ">
            <tbody style="padding-bottom: 25.4mm; margin-bottom:40px">
                <tr style="border-spacing: 40px;">
                    <td class="title" style="min-width: 40mm;"><strong> Nama Alat </strong><div class="eng">Measuring Instrument</div></td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="4" class="title"><strong>{{ $order->ServiceRequestItem->uuts->tool_name }}</strong></td>
                </tr>
                <tr style="height: 10px;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Merk / Buatan<div class="eng">Trade Mark / Manufactured by</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_brand }} / {{$order->ServiceRequestItem->uuts->tool_made_in ? $order->ServiceRequestItem->uuts->tool_made_in : '-'}}</td>
                </tr>
                
                <tr>
                    <td style="padding-left: 15px;">Model/Tipe<div class="eng">Model/Type</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_model }} / {{ $order->ServiceRequestItem->uuts->tool_type ? $order->ServiceRequestItem->uuts->tool_type : '-' }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">No. Seri/No.Identitas<div class="eng">Series Number</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->serial_no }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Kapasitas / Daya Baca<div class="eng">Maximum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_capacity }} {{ $order->ServiceRequestItem->uuts->tool_capacity_unit }} / {{ $order->ServiceRequestItem->uuts->tool_dayabaca }} {{ $order->ServiceRequestItem->uuts->tool_dayabaca_unit }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Kelas / Jumlah<div class="eng">Class </div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->class ? $order->ServiceRequestItem->uuts->class :'-' }} {{ $order->hasil_uji_memenuhi ==TRUE ? '' : '( Spesifikasi Pabrik)' }} / {{$order->tool_jumlah}}</td>
                </tr>
                <tr style="height: 30mm;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <!-- <tr>
                    <td class="title"><strong>Pemilik/Pemakai</strong><div class="eng">Owner/User</div></td>
                    <td>:</td>
                    <td class ="title" colspan="4"><strong>{{ $order->ServiceRequest->label_sertifikat }}</strong></td>
                </tr> -->
                <tr>
                        <td width="20%" style="font-size: 16px !important; font-weight:regular;"><strong>Pemilik</strong><div class="eng">Owner/User</div></td>
                        <td width="2%" style="font-size: 16px !important; font-weight:bold;">:</td>
                        <td style="font-size: 16px !important; font-weight:bold;" colspan="4">
                            {{ $order->ServiceRequest->label_sertifikat }}<br>
                            {{ $order->ServiceRequest->addr_sertifikat }}
                        </td>
                </tr>
                
                <tr colspan="4" style="height: 30mm;">
                    <td><br/></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="4">
                        @if($order->ServiceRequestItem->ServiceRequest->svc->id !=2)
                            <strong>
                                Sertifikat SUML ini berlaku sampai dengan {{ ($order->sertifikat_expired_at) ? into_tanggal($order->sertifikat_expired_at) :  date('d F Y', strtotime('1 Years')) }}
                            </strong>
                            <div class="eng">This certificate is valid until {{ ($order->sertifikat_expired_at) ? date('d F Y', strtotime($order->sertifikat_expired_at)) :  date('d F Y', strtotime('1 Years')) }}</div>
                        @endif
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height: 30mm;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="4"><strong>Sertifikat ini terdiri dari 2 (dua) halaman</strong> <div class="eng">This certificate consist of 2 (two) pages</div></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height: 30mm;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>

                <!-- <tr>
                    <td>Hasil<div class="eng">Result</div></td>
                    <td>:</td>
                    <td colspan="4">
                        @if($order->hasil_uji_memenuhi == true)
                        Disahkan berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal.
                        @else
                        Batal berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal. 
                        @endif  
                    </td>
                </tr> -->
            </tbody>
        <table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td class="text-left">Bandung, {{ $order->kabalai_date ? into_tanggal($order->kabalai_date) : into_tanggal(date("d F Y")) }}</td>
                </tr>
                <tr>
                    <td class="text-left">Kepala Balai Pengelolaan Standar</td>
                </tr>
                <tr>
                    <td class="text-left">Nasional Satuan Ukuran,</td>
                </tr>
                <tr>
                    <td class="text-left" style="height: 30mm; padding: top 4px;">
                        @if($order->KaBalai != null)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr style="padding-top: -10px !important;">
                    <td class="text-left" style="padding-top: -19px !important;">
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUut ? $order->KaBalai->PetugasUut->nama : 'Kepala Balai' }}</br>
                        NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUut ? $order->KaBalai->PetugasUut->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
    @if($order->cancel_at == null)
    <!--
    <section class="sheet watermark">

        <div class="text-left" style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="title"><strong>METODE, STANDAR DAN TELUSURAN</strong></div>
            <div class="subtitle"><div class="eng">Method, Standard dan Traceability</div></div>
        </div>
        <table class="table table-borderless"
         style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr style="height: 10px;"></tr>
                <tr>
                    <td style="padding-left: 15px;">- Metode</td>
                    <td>:</td>
                    <td colspan="4"> - ISO 4788 ( 2005-05-01 "Laboratory Glassware-Graduated Measuring Cylinder"</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;"></td>
                    <td></td>
                    <td colspan="4"> - IK-SNSU.501 Instruksi Kerja Kalibrasi Gelas Ukur</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">- Standar</td>
                    <td>:</td>
                    <td colspan="4"> - {{ $order->ServiceRequestItem->uuts->stdtype->uut_type }} / {{ $order->ServiceRequestItem->uuts->tool_type ? $order->ServiceRequestItem->uuts->tool_type : '-' }}</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 15px;"> - Telusuran ke Satuan SI melalui Direktorat Metrologi</td>
                     <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr style="height: 10px;"></tr>
                <tr>
                    <td class="title" style="min-width: 40mm;"><strong> DATA KALIBRASI </strong><div class="eng">Calibration data</div></td>
                    <td style="width: 3mm;"></td>
                    <td colspan="4"></td>
                </tr>
                <tr style="height: 10px;"></tr>
                <tr>
                    <td style="padding-left: 15px;"> Tanggal Pengujian</td>
                    <td>:</td>
                    <td colspan="4"> {{ $order->kabalai_date ? date("d F Y", strtotime($order->kabalai_date)) : date("d F Y") }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Penguji</td>
                    <td></td>
                    <td colspan="4"> - IK-SNSU.501 Instruksi Kerja Kalibrasi Gelas Ukur</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Lokasi</td>
                    <td>:</td>
                    <td colspan="4"> - {{ $order->ServiceRequestItem->uuts->stdtype->uut_type }} / {{ $order->ServiceRequestItem->uuts->tool_type ? $order->ServiceRequestItem->uuts->tool_type : '-' }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Kondisi ruangan</td>
                    <td>:</td>
                    <td colspan="4"> Suhu : (21,4 &plusmn; 0,1)&deg;C</td>
                </tr>

                <tr>
                    <td style="padding-left: 15px;"></td>
                    <td></td>
                    <td colspan="4"> Suhu : (65,8 &plusmn; 1,9)%</td>
                </tr>
                
            </tbody>
        <table>

        <div class="text-left" style="padding-top: 5.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="title"><strong>HASIL</strong></div>
            <div class="subtitle"><div class="eng">Result</div></div>
        </div>
        <br/><br/>
        <table class="table table-bordered" style="padding-left: 20%; padding-right: 25.4mm;">
            <thead>
                <tr>
                    <th rowspan="1" class="text-center">Nominal</th>
                    <th colspan="1" class="text-center">Volume Sebenarnya (mL)</th>
                    <th rowspan="1" class="text-center">Ketidakpastian (mL)</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=0;
                foreach($order->inspections as $inspection){ ?>
                <tr>
                    
                    <td class="text-center">100
                    </td>
                    <td class="text-center">99,6
                        
                    </td>
                    <td class="text-center">2,3
                        
                    </td>
                    
                </tr>
                <?php if(++$i > 2) break;}?>
            </tbody>
        </table> 
        <div class="text-left" style="padding-top: 10.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="title">Catatan</div>
            <div class="subtitle"> - Hasil yang dinyatakan dalam sertifikat terkait dengan</div>
            <div class="subtitle">  &nbsp; barang yang dikalibrasi</div>
            <div class="subtitle"> - Ketidak pastian pengukuran dinyatakan pada tingkat </div>
            <div class="subtitle">  &nbsp; kepercayaan sekitar 95% dengan faktor cakupan k=2,00.</div>
        </div>
        </br>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td class="text-center">Sub Koordinator Pelayanan</td>
                </tr>
                <tr>
                    <td class="text-center">Kalibrasi Alat Ukur</td>
                </tr>
                <tr>
                    <td class="text-center" style="height: 30mm;">
                        @if($order->KaBalai != null)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(123)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border-bottom: 1px solid #000 padding-bottom: 3px">
                        <u id="underline-gap">{{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai' }} </u>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                    NIP :
                    </td>
                </tr>
            </tbody>
        </table>
    
    </section> -->
    @endif


</body>

</html>