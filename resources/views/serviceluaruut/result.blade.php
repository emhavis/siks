@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        @if($serviceOrder->kalab_notes != null)
        <div class="alert alert-info" role="alert">
            {{ $serviceOrder->kalab_notes }}
        </div>
        @endif

        {!! Form::open(['url' => route('serviceluaruut.resultupload', $serviceOrder->id), 'files' => true, 'id' => 'form_result'])  !!}
        
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label><h2>{{$serviceOrder->serviceRequest->svc->service_type}} ( {{$serviceOrder->serviceRequestItem->no_order}}) </h2></p> Di order pada tanggal : {{ date("d-m-Y", strtotime($serviceOrder->ServiceRequestItem->order_at)) }}</label>
                </div>
            </div>
        </div>
        <hr/>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="spuh_spt">No Surat Tugas</label>
                    {!! Form::text('spuh_spt', $serviceOrder->ServiceRequest->spuh_spt,
                        ['class' => 'form-control','id' => 'spuh_spt', 'readonly']) !!}
                </div>
            </div>
        </div>

        <h4>Laporan Pelaksanaan Tugas</h4>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="ringkasan">Ringkasan Pelaksanaan Pengujian</label>
                    <textarea name="ringkasan" id="ringkasan"
                        class="form-control">{!! $laporan != null ? $laporan->ringkasan : '' !!}</textarea>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kendala_teknis">Kendala Teknis</label>
                    <textarea name="kendala_teknis" id="kendala_teknis"
                        class="form-control">{!! $laporan != null ? $laporan->kendala_teknis : '' !!}</textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kendala_non_teknis">Kendala Non Teknis</label>
                    <textarea name="kendala_non_teknis" id="kendala_non_teknis"
                            class="form-control">{!! $laporan != null ? $laporan->kendala_non_teknis : '' !!}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="metode_tindakan">Metode/Tindakan yang Dilakukan</label>
                    <textarea name="metode_tindakan" id="metode_tindakan"
                        class="form-control">{!! $laporan != null ? $laporan->metode_tindakan : '' !!}</textarea>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="saran_masukan">Saran/Masukan</label>
                    <textarea name="saran_masukan" id="saran_masukan"
                        class="form-control">{!! $laporan != null ? $laporan->saran_masukan : '' !!}</textarea>
                </div>  
            </div>
        </div>

        <hr/>

        <h4>Hasil Uji</h4>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_uttp">Jenis UUT</label>
                    {!! Form::text('jenis_uttp', $serviceOrder->ServiceRequestItem->uuts->stdtype->uut_type, ['class' => 'form-control','id' => 'jenis_uut', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tool_name">Nama Alat</label>
                    {!! Form::text('tool_name', $serviceOrder->tool_name, ['class' => 'form-control','id' => 'tool_name', ]) !!}
                </div>
            </div>
        </div>

        @if($serviceOrder->tool_capacity_min >0)
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="tool_capacity_min">Kapasitas Minimal</label>
                    {!! Form::text('tool_capacity_min', $serviceOrder->tool_capacity_min, ['class' => 'form-control','id' => 'tool_capacity_min']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="tool_capacity_min_unit">Satuan</label>
                    {!! Form::text('tool_capacity_min_unit', $serviceOrder->tool_capacity_min_unit, ['class' => 'form-control','id' => 'tool_capacity_min_unit']) !!}
                </div>
            </div>
        </div>

        @endif

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas</label>
                    {!! Form::text('kapasitas', $serviceOrder->tool_capacity, ['class' => 'form-control','id' => 'kapasitas']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="kapasitas">Satuan</label>
                    {!! Form::text('satuan', $serviceOrder->tool_capacity_unit, ['class' => 'form-control','id' => 'kapasitas']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="tool_dayabaca">Daya Baca</label>
                    {!! Form::text('tool_dayabaca', $serviceOrder->tool_dayabaca, ['class' => 'form-control','id' => 'tool_dayabaca']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="tool_dayabaca_unit">Satuan</label>
                    {!! Form::text('tool_dayabaca_unit', $serviceOrder->tool_dayabaca_unit, ['class' => 'form-control','id' => 'tool_dayabaca_unit']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="tool_brand">Merek</label>
                    {!! Form::text('tool_brand', $serviceOrder->tool_brand, ['class' => 'form-control','id' => 'tool_brand']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="tool_model">Model/Tipe</label>
                    {!! Form::text('tool_model', $serviceOrder->tool_model, ['class' => 'form-control','id' => 'tool_model']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="tool_serial_no">No Seri/Identitas</label>
                    {!! Form::text('tool_serial_no', $serviceOrder->tool_serial_no, ['class' => 'form-control','id' => 'serial_no']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="tool_class">Kelas</label>
                    {!! Form::text('tool_class', $serviceOrder->tool_class, ['class' => 'form-control','id' => 'tool_class']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="tool_jumlah">Jumlah</label>
                    {!! Form::text('tool_jumlah', $serviceOrder->tool_jumlah, ['class' => 'form-control','id' => 'tool_jumlah']) !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan']) !!}
                </div>
            </div>
            
        </div>

        <!--
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Merek/Model/Tipe/Nomor Seri</label>
                    {!! Form::text('merek', 
                        $serviceOrder->ServiceRequestItem->uuts->tool_brand . '/' . 
                        $serviceOrder->ServiceRequestItem->uuts->tool_model . '/' .
                        $serviceOrder->ServiceRequestItem->uuts->tool_type . '/' .
                        $serviceOrder->ServiceRequestItem->uuts->serial_no,
                        ['class' => 'form-control','id' => 'merek', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan']) !!}
                </div>
            </div>
        </div>
        -->
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat']) !!}
                </div>
            </div>
        </div>

        <hr/> <h4>
            Keterangan Sertifikat </h4>
        <hr/>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_1">Diuji Oleh (1)</label>
                    {!! Form::text('test_by_1', 
                        $user->full_name,
                        ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                </div>
            </div>
            @if ($user2 != null)
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_2">Diuji Oleh (2)</label>
                    {!! Form::hidden('test_by_2', 
                        $user2->id) !!}
                    {!! Form::text('test_by_2_name', 
                        $user2->full_name,
                        ['class' => 'form-control','id' => 'test_by_2_name', 'readonly']) !!}
                    <div class="form-check">
                        <input type="checkbox" name="test_by_2_sertifikat" value="test_by_2_sertifikat" id="test_by_2_sertifikat" {{ $serviceOrder->test_by_2_sertifikat ? 'checked' : '' }} />
                        Ditampilkan pada Sertifikat 
                    </div>
                </div>
            </div>
            @endif
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_datein">Waktu Pengujian, Mulai</label>
                    {!! Form::text('staff_entry_datein', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_datein) ? $serviceOrder->staff_entry_datein : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'staff_entry_datein', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_dateout">Waktu Pengujian, Selesai</label>
                    {!! Form::text('staff_entry_dateout', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_dateout) ? $serviceOrder->staff_entry_dateout : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'staff_entry_dateout', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                    <div class="form-group">
                        <label for="hasil_uji_memenuhi">Status Akreditasi Sertifikat</label>
                        {!! Form::select('is_certified', [False=>'Tidak', True=>'Ya'], $serviceOrder->is_certified, 
                            ['class' => 'form-control select2', 'id' => 'is_certified']) !!}
                    </div>
            </div>
            <div class="col-md-6">
                    <div class="form-group col-md-6">
                        <label for="hasil_uji_memenuhi">Halaman Sertifikat</label>
                        {!! Form::text('page_num', $serviceOrder->page_num ,
                            ['class' => 'form-control', 'id' => 'page_num']) !!}
                    </div>
                    <div class="form-group col-md-6">
                        <label for="sertifikat_expired_at">Berlaku Sampai (dd-mm-yyyy)</label>
                        {!! Form::text('sertifikat_expired_at', $serviceOrder->sertifikat_expired_at,
                        ['class' => 'form-control','id' => 'sertifikat_expired_at']) !!}
                    </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->service_type_id == 1)
        <div class="row">
            <div class="col-md-6">
                    <div class="form-group">
                        <label for="hasil_uji_memenuhi">Memenuhi Syarat</label>
                        {!! Form::select('hasil_uji_memenuhi', [True=>'Ya', False=>'Tidak'], $serviceOrder->hasil_uji_memenuhi, 
                            ['class' => 'form-control select2', 'id' => 'hasil_uji_memenuhi']) !!}
                    </div>
            </div>
            <div class="col-md-6">
                <!-- <div class="form-group berlaku">
                    <label for="sertifikat_expired_at">Berlaku Sampai (dd-mm-yyyy)</label>
                    {!! Form::text('sertifikat_expired_at', null,
                    ['class' => 'form-control','id' => 'sertifikat_expired_at']) !!}
                </div> -->
                <div class="form-group berlaku">
                    <label for="sertifikat_expired_at">Alasan Pembatalan <strong style="color:red;">&nbsp;*</strong></label>
                    {!! Form::textArea('cancel_notes', $serviceOrder->cancel_notes,
                    ['class' => 'form-control','id' => 'cancel_notes','rows'=>4]) !!}
                </div>
            </div>
        </div>
        @endif
        @if($serviceOrder->ServiceRequest->service_type_id == 2)
        <div class="row">
            <div class="col-md-6">
                    <div class="form-group">
                        <label for="hasil_uji_memenuhi">Memenuhi Syarat</label>
                        {!! Form::select('hasil_uji_memenuhi', [True=>'Ya', False=>'Tidak'], null, 
                            ['class' => 'form-control select2', 'id' => 'hasil_uji_memenuhi']) !!}
                    </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group berlaku">
                <label for="sertifikat_expired_at">Alasan Pembatalan <strong style="color:red;">&nbsp;*</strong></label>
                {!! Form::textArea('cancel_notes', $serviceOrder->cancel_notes,
                ['class' => 'form-control','id' => 'cancel_notes','rows'=>4]) !!}
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <div class ="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="file_skhp">Lampiran Cerapan<strong style="color:red;">&nbsp;*</strong></label>
                            {!! Form::file('file_skhp', null,
                                ['class' => 'form-control','id' => 'file_skhp']) !!}
                        </div>
                    </div>
                    <div class="cal-md-8">
                        @if($serviceOrder->path_skhp !=null)
                        <div class="input-group">
                            <label style="padding-top:30px"></label>
                            <a  class="btn btn-danger" href="{{ route('file.show', [$serviceOrder->id,'cerapan']) }}">Lihat</a>
                            <label> {{$serviceOrder->path_skhp}}</label>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class ="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label id="lblC" name ="lblC" for="file_lampiran">Lampiran Sertifikat <strong style="color:red;">&nbsp;*</strong></label>
                            {!! Form::file('file_lampiran', null,
                                ['class' => 'form-control','id' => 'file_lampiran']) !!}
                        </div>
                    </div>
                    <div class="cal-md-8">
                        @if($serviceOrder->path_skhp !=null)
                        <div class="input-group">
                            <label style="padding-top:30px"></label>
                            <a  class="btn btn-danger" href="{{ route('file.show', [$serviceOrder->id,'lampiran']) }}">Lihat</a>
                            <label> {{$serviceOrder->path_lampiran}}</label>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>


        <input type="hidden" name="stat_sertifikat" id="stat_sertifikat" value="0"/>
        <button role="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan Hasil Uji</button>
        <button class="btn btn-w-md btn-success" id="btn_simpan_submit">Kirim Hasil Uji</button>
        {!! Form::close() !!}
        
        
        
       
    </div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function ()
    {
        <?php 
            if(($serviceOrder->hasil_uji_memenuhi == null && $serviceOrder->cancel_notes == null) || $serviceOrder->hasil_uji_memenuhi == 0 || $serviceOrder->hasil_uji_memenuhi == true){ ?>
               $(".berlaku").hide(); 
                document.getElementById('lblC').innerHTML = 'Lampiran Sertifikat <strong style="color:red;">&nbsp;*</strong>';
            <?php } ?>
            <?php if($serviceOrder->hasil_uji_memenuhi ==null && $serviceOrder->hasil_uji_memenuhi >=1 && ($serviceOrder->hasil_uji_memenuhi ==False || ($serviceOrder->cancel_notes !=null|| $serviceOrder->cancel_notes !=''))){ ?>

                $(".berlaku").show();
                document.getElementById('lblC').innerHTML = 'Lampiran Sertifikat <strong style="color:red;">&nbsp;</strong>';
            <?php } else{?>
        
                $(".berlaku").hide(); 
                document.getElementById('lblC').innerHTML = 'Lampiran Sertifikat <strong style="color:red;">&nbsp;*</strong>';
            <?php }?>
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });
        /*
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        */

        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_alg_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_mabbm_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="brand[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_alg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_alg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="input_level[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_up[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_down[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_hysteresis[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mabbm");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="meter_factor[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_ma').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_ma");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_ma').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mguwc').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mguwc");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_bkd[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td><input type="number" class="form-control" name="repeatability_bkd[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mguwc').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $("#btn_simpan").click(function(e){
            e.preventDefault();

            $("#stat_sertifikat").val(null);

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            $("#stat_sertifikat").val(1);

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#hasil_uji_memenuhi").on("change", function(){
            if($(this).val() ==false){
                $(".berlaku").show();
                document.getElementById('lblC').innerHTML = 'Lampiran Sertifikat <strong style="color:red;">&nbsp;</strong>';
            }else{
                $(".berlaku").hide();
                document.getElementById('lblC').innerHTML = 'Lampiran Sertifikat <strong style="color:red;">&nbsp;*</strong>';
            }
        });
    }); 
</script>
@endsection