<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Keterangan Hasil Pengujian</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: Arial;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
            /* page-break-after: always; */
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-draft-uttp.png") : public_path("assets/images/logo/letterhead-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
        .table td .eng {
            font-size:8pt;
            font-style:italic;
        }
    </style>
</head>

<body class="A4">

    <section class="sheet {{ $order->kabalai_date ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->no_service != null ? $order->no_service : 'X-XXX-XXX'}}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN HASIL PENGUJIAN</strong></div>
            <div class="subtitle"><i>Certificate of Testing Result</i></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="min-width: 40mm;">Jenis uuts<div class="eng">Measuring Instrument</div></td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->type->uut_type_certificate }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Merk<div class="eng">Trade Mark</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_brand }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Model/Tipe<div class="eng">Model/Type</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_model }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">No. Seri<div class="eng">Series Number</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->serial_no }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Maksimum<div class="eng">Maximum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_capacity }} {{ $order->ServiceRequestItem->uuts->tool_capacity_unit }}</td>
                </tr>
                @if($order->ServiceRequestItem->uuts->tool_capacity_min > 0)
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Minimum<div class="eng">Minimum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_capacity_min }} {{ $order->ServiceRequestItem->uuts->tool_capacity_unit }}</td>
                </tr>
                @endif
                <tr>
                    <td>Pemilik/Pemakai<div class="eng">Owner/User</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                <tr>
                    <td>Lokasi Alat<div class="eng">Location</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequest->addr_sertifikat }}</td>
                </tr>

                <tr>
                    <td>Hasil<div class="eng">Result</div></td>
                    <td>:</td>
                    <td colspan="4">
                        @if($order->hasil_uji_memenuhi == true)
                        Disahkan berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal.
                        @else
                        Batal berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal. 
                        @endif  
                    </td>
                </tr>
            </tbody>
        <table>

        <br />
        
        <br/>
        
        
        
    </section>

    

</html>