@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
@endsection

@section('content')
<form id="createGroup" action="{{ route('dashboardtu')}}" method="GET" >
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="start_date">Periode</label> 
                <div class="input-group input-daterange" id="dt_range">
                    <input type="text" class="form-control" id="start_date" name="start_date"
                    value="{{ $start != null ? date("d-m-Y", strtotime($start)) : '' }}">
                    <div class="input-group-addon">s/d</div>
                    <input type="text" class="form-control" id="end_date" name="end_date"
                    value="{{ $end != null ? date("d-m-Y", strtotime($end)) : '' }}">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            {!! Form::submit('Filter', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_filter']) !!}
        </div>
    </div>
</form>
<div class="row">
    <ul class="nav nav-tabs" role="tablist" id="nav">
        <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">Keseluruhan</a></li>
        
        <li role="presentation"><a href="#ttu" aria-controls="ttu" role="tab" data-toggle="tab">Tera/Tera Ulang</a></li>
        <li role="presentation"><a href="#tipe" aria-controls="tipe" role="tab" data-toggle="tab">Evaluasi Tipe</a></li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="all">
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <canvas id="chartAll"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Instalasi</th>
                                    <th>Jumlah UTTP</th>
                                    <th>UTTP Dalam Proses</th>
                                    <th>UTTP Selesai Sesuai SLA</th>
                                    <th>UTTP Selesai Melebihi SLA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summaries as $row)
                                <tr>
                                    <td>{{ $row->nama_instalasi }}</td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="all" 
                                        data-sla="semua">{{ $row->jumlah }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="all" 
                                        data-sla="proses">{{ $row->jumlah_proses }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="all" 
                                        data-sla="sesuai">{{ $row->jumlah_sesuai_sla }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="all" 
                                        data-sla="lebih">{{ $row->jumlah_lebih_sla }}</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="ttu">
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <canvas id="chartTTU"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <table id="data_table_ttu" class="table table-striped table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Instalasi</th>
                                    <th>Jumlah UTTP</th>
                                    <th>UTTP Dalam Proses</th>
                                    <th>UTTP Selesai Sesuai SLA</th>
                                    <th>UTTP Selesai Melebihi SLA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summaries_ttu as $row)
                                <tr>
                                    <td>{{ $row->nama_instalasi }}</td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="ttu" 
                                        data-sla="semua">{{ $row->jumlah }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="ttu" 
                                        data-sla="proses">{{ $row->jumlah_proses }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="ttu" 
                                        data-sla="sesuai">{{ $row->jumlah_sesuai_sla }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="ttu" 
                                        data-sla="lebih">{{ $row->jumlah_lebih_sla }}</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tipe">
        <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <canvas id="chartTipe"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <table id="data_table_tipe" class="table table-striped table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Instalasi</th>
                                    <th>Jumlah UTTP</th>
                                    <th>UTTP Dalam Proses</th>
                                    <th>UTTP Selesai Sesuai SLA</th>
                                    <th>UTTP Selesai Melebihi SLA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summaries_tipe as $row)
                                <tr>
                                    <td>{{ $row->nama_instalasi }}</td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="tipe" 
                                        data-sla="semua">{{ $row->jumlah }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="tipe" 
                                        data-sla="proses">{{ $row->jumlah_proses }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="tipe" 
                                        data-sla="sesuai">{{ $row->jumlah_sesuai_sla }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-instalasi="{{ $row->instalasi_id }}" 
                                        data-type="tipe" 
                                        data-sla="lebih">{{ $row->jumlah_lebih_sla }}</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="data_table_details" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>No Register</th>
                        <th>Layanan</th>
                        <th>Instalasi</th>
                        <th>Jenis Alat</th>
                        <th>Terima</th>
                        <th>Selesai</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/chart.js/dist/Chart.min.js') }}"></script>
<script>
$(document).ready(function()
{
    $('#data_table, #data_table_ttu, #data_table_tipe').DataTable();
    $('.input-daterange input').each(function() {
        $(this).datepicker({
            format:"dd-mm-yyyy",
        });
    });

    var tbl_details = $('#data_table_details').DataTable();

    var ctx = $("#chartAll");
    const labels = @json($instalasi);
    const data = @json($summaries);
    const config = {
        type: 'bar',
        options: {
            onClick: function(e) {
                barOnClick('all', e);
            },
        },
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Jumlah UTTP',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(54, 162, 235)'
                }, 
                {
                    label: 'UTTP dalam Proses',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah_proses',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(255, 159, 64)'
                }, 
                {
                    label: 'UTTP Selesai Sesuai SLA',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah_sesuai_sla',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(75, 192, 192)'
                }, 
                {
                    label: 'UTTP Selesai Melebihi SLA',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah_lebih_sla',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(255, 99, 132)'
                }
            ]
        }
    };
    var chart = new Chart(ctx, config);

    var ctx_ttu = $("#chartTTU");
    const labels_ttu = @json($instalasi_ttu);
    const data_ttu = @json($summaries_ttu);
    const config_ttu = {
        type: 'bar',
        data: {
            labels: labels_ttu,
            datasets: [
                {
                    label: 'Jumlah UTTP',
                    data: data_ttu,
                    parsing: {
                        yAxisKey: 'jumlah',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(54, 162, 235)'
                }, 
                {
                    label: 'UTTP dalam Proses',
                    data: data_ttu,
                    parsing: {
                        yAxisKey: 'jumlah_proses',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(255, 159, 64)'
                }, 
                {
                    label: 'UTTP Selesai Sesuai SLA',
                    data: data_ttu,
                    parsing: {
                        yAxisKey: 'jumlah_sesuai_sla',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(75, 192, 192)'
                }, 
                {
                    label: 'UTTP Selesai Melebihi SLA',
                    data: data_ttu,
                    parsing: {
                        yAxisKey: 'jumlah_lebih_sla',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(255, 99, 132)'
                }
            ]
        }
    };
    var chart_ttu = new Chart(ctx_ttu, config_ttu);

    var ctx_tipe = $("#chartTipe");
    const labels_tipe = @json($instalasi_tipe);
    const data_tipe = @json($summaries_tipe);
    const config_tipe = {
        type: 'bar',
        data: {
            labels: labels_tipe,
            datasets: [
                {
                    label: 'Jumlah UTTP',
                    data: data_tipe,
                    parsing: {
                        yAxisKey: 'jumlah',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(54, 162, 235)'
                }, 
                {
                    label: 'UTTP dalam Proses',
                    data: data_tipe,
                    parsing: {
                        yAxisKey: 'jumlah_proses',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(255, 159, 64)'
                }, 
                {
                    label: 'UTTP Selesai Sesuai SLA',
                    data: data_tipe,
                    parsing: {
                        yAxisKey: 'jumlah_sesuai_sla',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(75, 192, 192)'
                }, 
                {
                    label: 'UTTP Selesai Melebihi SLA',
                    data: data_tipe,
                    parsing: {
                        yAxisKey: 'jumlah_lebih_sla',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(255, 99, 132)'
                }
            ]
        }
    };
    var chart_tipe = new Chart(ctx_tipe, config_tipe);

    $('.details').on('click', function(e) {
        e.preventDefault();
       
        var data = { 
                start_date: $('#start_date').val(),
                end_date: $('#end_date').val(),
                instalasi: $(this).data('instalasi'),
                type: $(this).data('type'),
                sla: $(this).data('sla'),
            };
        
        getAndRedraw(data);
    });

    $('#nav').on('click', function(e) {
        tbl_details.clear().draw();
    });

    function barOnClick(type, e) {
        var cht = e.chart;
        
        const points = cht.getElementsAtEventForMode(e, 'nearest', { intersect: true }, true);

        if (points.length) {
            const firstPoint = points[0];
            const label = cht.data.labels[firstPoint.index];
            const value = cht.data.datasets[firstPoint.datasetIndex].data[firstPoint.index];
            const y = cht.data.datasets[firstPoint.datasetIndex].parsing.yAxisKey;

            switch(y) {
                case 'jumlah':
                    var sla = 'semua';
                    break;
                case 'jumlah_proses':
                    var sla = 'proses';
                    break;
                case 'jumlah_sesuai_sla':
                    var sla = 'sesuai';
                    break;
                case 'jumlah_lebih_sla':
                    var sla = 'lebih';
                    break;
                default:
                    var sla = 'semua';
            }

            var data = { 
                start_date: $('#start_date').val(),
                end_date: $('#end_date').val(),
                instalasi: value.instalasi_id,
                type: type,
                sla: sla,
            };
        
            getAndRedraw(data);
        }
    }

    function getAndRedraw(data) {

        $.get( "{{ route('dashboardmonitoruttp.details') }}", data)
            .done(function(result) {
                tbl_details.clear();
                $.each(result, function(key, item) {
                    tbl_details.row.add([
                        item.no_order,
                        item.no_register,
                        item.jenis_layanan,
                        item.nama_instalasi,
                        item.jenis,
                        item.received_date,
                        item.staff_entry_dateout
                    ]);
                });
                tbl_details.draw();
            });
    }
});
</script>
@endsection