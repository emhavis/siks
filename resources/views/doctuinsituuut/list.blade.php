@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div id="alert_board"></div>
    <a href="{{ route('doctuinsituuut') }}" class="btn btn-warning btn-sm">Daftar Surat Tugas</a>
    <br/><br/>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
            
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Booking</h4>
                    </div>
                    
                    <div class="panel-body">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="label_sertifikat">Nama Pemilik</label>
                                    {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="addr_sertifikat">Alamat Pemilik</label>
                                    {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                                </div>
                            </div>  
                        </div>

                    </div>
                </div>

                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Pemohon</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_name">Nama Pemohon</label>
                                    {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_name">Jenis Tanda Pengenal</label>
                                    {!! Form::text('id_type_id', $requestor->kantor == "Perusahaan" ? "NIB" : "NPWP", ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                                    {!! Form::text('pic_id_no', $requestor->kantor == "Perusahaan" ? $requestor->nib : $requestor->npwp, ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_phone_no">Nomor Telepon</label>
                                    {!! Form::text('pic_phone_no', $requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_email">Alamat Email</label>
                                    {!! Form::text('pic_email', $requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_from">Usulan Jadwal Mulai</label>
                                    {!! Form::text('scheduled_test_date_from', date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_from', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_to">Usulan Jadwal Selesai</label>
                                    {!! Form::text('scheduled_test_date_to', date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_to', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis_layanan">Jenis Layanan</label>
                                    {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lokasi_pengujian">Lokasi Pengujian</label>
                                    {!! Form::text('lokasi_pengujian', $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' , ['class' => 'form-control','id' => 'lokasi_pengujian', 'readonly']) !!}
                                    
                                </div>
                                
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    @if($request->total_price > 0)
                                    <a target="_blank" href="{{ route('requestluaruut.pdf', $request->id) }}" class="btn btn-warning btn-sm">Invoice PNBP</a>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    @if ($request->file_payment_ref != null)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download_paymentref/payment_ref/{{ $request->id }}" class="btn btn-sm btn-warning">Bukti Bayar PNBP</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Penugasan dan Penjadwalan</h4>
                    </div>
                    <div class="panel-body" id="panel_staff">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inspection_prov_id">Alamat Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                                    {!! Form::text('inspection_prov_id', 
                                        $request->inspectionKabkot->nama .', '. $request->inspectionProv->nama,
                                        ['class' => 'form-control','id' => 'inspection_prov_id', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="spuh_rate">Uang Harian</label>
                                    {!! Form::hidden('spuh_rate_id', $request->spuh_rate_id,
                                        ['class' => 'form-control','id' => 'spuh_rate_id', 'readonly']) !!}
                                    {!! Form::text('spuh_rate', $request->spuh_rate,
                                        ['class' => 'form-control','id' => 'spuh_rate', 'readonly']) !!}
                                </div>
                            </div>
                        </div>

                        

                        @if($request->status_revisi_spt == 1)    
                        <div class="row">
                        <a href="{{ route('doctuinsituuut.create', ['id' => $request->id]) }}" class="btn btn-w-md btn-accent">Tambah Revisi</a>
                        <br/><br/>
                        </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table_data_penjadwalan" class="table table-striped table-hover table-responsive-sm">
                                    <thead>
                                        <tr>
                                            <th>Nomor</th>
                                            <th>Tanggal</th>
                                            <th>Jenis</th>
                                            <th>Tanggal Penugasan</th>
                                            <th>Penguji/Pemeriksa</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($docs as $doc)
                                        <tr>
                                            <td>{{ $doc->doc_no }}</td>
                                            <td>{{ date("d-m-Y", strtotime($doc->accepted_date)) }}</td>
                                            <td>{{ $doc->jenis == 'inisial' ? 'Awal' : 'Revisi'}}</td>
                                            <td>{{ date("d-m-Y", strtotime($doc->date_from)) }} s/d {{ date("d-m-Y", strtotime($doc->date_to)) }}</td>
                                            <td>
                                                @foreach($doc->listOfStaffs as $staf)
                                                @if($staf->scheduledStaff != null)
                                                    {{ $staf->scheduledStaff->nama }} ({{ $staf->scheduledStaff->nip ?? '-' }})
                                                    @if($loop->remaining)
                                                        <br/>
                                                    @endif
                                                @endif
                                                @endforeach
                                            </td>
                                            <td>
                                                @if($doc->jenis == 'inisial')
                                                <a href="{{ route('schedulinguut.surattugas', ['id' => $doc->request_id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Surat Tugas</a>

                                                @if(($doc->invoiced_price > 0))
                                                <a target="_blank" href="{{ route('requestluaruut.pdftuhp', $request->id) }}" class="btn btn-warning btn-sm">SPUH</a>
                                                @endif
                                                @if($doc->invoiced_price > 0 && $request->spuh_payment_date != null && $request->spuh_payment_valid_date != null && $request->file_spuh_payment_ref != null)
                                                <a href="<?= config('app.siks_url') ?>/tracking/download_paymentref/spuh_payment_ref/{{ $request->id }}" class="btn btn-warning btn-sm">Bukti Bayar</a>
                                                @endif

                                                @else
                                                <a href="{{ route('schedulinguut.surattugas', ['id' => $doc->request_id, 'stream' => 1, 'docId' => $doc->id]) }}" class="btn btn-warning btn-sm">Surat Tugas</a>
                                                
                                                @if($doc->file_spuh != null)
                                                <a href="{{ route('doctuinsituuut.download', ['id' => $doc->id, 'tipe' => 'spuh']) }}" class="btn btn-warning btn-sm">SPUH</a>
                                                @endif
                                                @if($doc->file_bukti_bayar != null)
                                                <a href="{{ route('doctuinsituuut.download', ['id' => $doc->id, 'tipe' => 'bukti_bayar']) }}" class="btn btn-warning btn-sm">Bukti Bayar</a>
                                                @endif
                                                @if($doc->file_spuh == null || $doc->file_bukti_bayar == null)
                                                <a href="{{ route('doctuinsituuut.edit', ['id' => $doc->id]) }}" class="btn btn-info btn-sm">Edit</a>
                                                @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                

            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/jquery-validation/additional-methods.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan, #data_table_check").DataTable();

        $('#is_approved').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        }).on('select2:select', function (e) {
            var data = e.params.data;
            var stat = $('#status').val();

            if (data.id == 'tidak') {
                $('#btn_simpan_submit').text('Surat Tugas Tidak Lengkap');
                $('#btn_simpan_submit').removeClass('btn-success');
                $('#btn_simpan_submit').addClass('btn-danger');

                $("#notes").prop("readonly", false); 
            } else {
                $('#btn_simpan_submit').text('Setujui Surat Tugas');
                $('#btn_simpan_submit').removeClass('btn-danger');
                $('#btn_simpan_submit').addClass('btn-success');

                $("#notes").val("");
                $("#notes").prop("readonly", true); 
            }
        });

        $("#form_approve").validate({
            rules: {
                notes: {
                    required: function(element) {
                        return ($("#is_approved").val() == 'tidak');
                    },
                },
                /*
                file_review: {
                    required: function(element) {
                        return ($("#is_approved").val() == 'tidak');
                    },
                },
                */
            },
            messages: {
                notes: 'Catatan perbaikan harus diisi',
                //file_review: 'File lampiran perbaikan harus diupload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    });
</script>
@endsection