@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<style type"text/css"> 
    .badge-notif-tab {
            position:relative;
    }
    .badge-notif-tab[data-badge]:after {
            content:attr(data-badge);
            position:absolute;
            top:-10px;
            right:-5px;
            font-size:.7em;
            background:#f3a709;
            color:white;
            width:18px;
            height:18px;
            text-align:center;
            line-height:18px;
            border-radius: 50%;
    }
</style>
@endsection

@section('content')
<div class="row">

    <ul class="nav nav-tabs" role="tablist" id="tabs">
        <li role="presentation" class="active">
            <a href="#revisi" aria-controls="revisi" role="tab" data-toggle="tab" 
                class="badge-notif-tab" 
                <?php if ((count($rows_status)) > 0 ){ ?> 
                    data-badge="{{count($rows_status)}}" 
                <?php }else{ } ?>
            >Revisi SPT</a>
        </li>
        <li role="presentation">
            <a href="#bukti" aria-controls="bukti" role="tab" data-toggle="tab" 
                class="badge-notif-tab" 
                <?php if ((count($rows_bukti)) > 0 ){ ?> 
                    data-badge="{{count($rows_bukti)}}" 
                <?php }else{ } ?>
            >Bukti SPUH</a>
        </li>
        <li role="presentation">
            <a href="#all" aria-controls="all" role="tab" data-toggle="tab" >Semua</a>
        </li>
    </ul>

    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="revisi">
            <br/>   

            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    
                    <table id="table_data_status" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Surat Tugas</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_status as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->spuh_spt }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>
                                    {{ $row->status->status }}
                                    @if($row->status_revisi_spt == 1)
                                    <br/>
                                    <div class="alert alert-danger" role="alert">
                                        Perlu revisi SPT
                                    </div>
                                    @endif
                                </td>
                                <td>
                                    @if($row->spuh_doc_id != null)
                                    <a href="{{ route('schedulinguut.surattugas', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Surat Tugas Awal</a>
                                    @endif
                                    <a href="{{ route('doctuinsituuut.list', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Revisi Surat Tugas</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="bukti">
            <br/>   

            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    
                    <table id="table_data_bukti" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Surat Tugas</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_bukti as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->spuh_spt }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>
                                    {{ $row->status->status }}
                                    @if($row->status_revisi_spt == 2)
                                    <br/>
                                    <div class="alert alert-warning" role="alert">
                                        Revisi SPT perlu dilengkapi dengan SPUH dan Bukti Bayar
                                    </div>
                                    @endif
                                </td>
                                <td>
                                    @if($row->spuh_doc_id != null)
                                    <a href="{{ route('schedulinguut.surattugas', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Surat Tugas Awal</a>
                                    @endif
                                    <a href="{{ route('doctuinsituuut.list', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Daftar Surat Tugas</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="all">
            <br/>   

            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    
                    <table id="table_data_all" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Surat Tugas</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->spuh_spt }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>
                                    {{ $row->status->status }}
                                    @if($row->status_revisi_spt == 1)
                                    <br/>
                                    <div class="alert alert-danger" role="alert">
                                        Perlu revisi SPT
                                    </div>
                                    @elseif($row->status_revisi_spt == 2)
                                    <br/>
                                    <div class="alert alert-warning" role="alert">
                                        Revisi SPT perlu dilengkapi dengan SPUH dan Bukti Bayar
                                    </div>
                                    @endif
                                </td>
                                <td>
                                    @if($row->spuh_doc_id != null)
                                    <a href="{{ route('schedulinguut.surattugas', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Surat Tugas Awal</a>
                                    @endif
                                    <a href="{{ route('doctuinsituuut.list', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Daftar Surat Tugas</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_all, #table_data_status, #table_data_bukti").DataTable();
        
    });
</script>
@endsection