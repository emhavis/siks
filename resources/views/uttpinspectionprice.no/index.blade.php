@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <a href="{{ route('uttpinspectionprice.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUser" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Jenis Layanan</th>
                        <th>Pengujian/Pemeriksaan</th>
                        <th>Harga</th>
                        <th>Unit</th>
                        <th>Instalasi</th>
                        <th>Template</th>
                        <th>Berlaku Rentang?</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($prices->count())
                    @foreach($prices as $price)
                    <tr>
                        <td>{{ $price->serviceType->service_type }}</td>
                        <td>{{ $price->inspection_type }}</td>
                        <td>{{ $price->price }}</td>
                        <td>{{ $price->unit }}</td>
                        <td>{{ $price->instalasi->nama_instalasi }}</td>
                        <td>{{ $price->inspection_template_id }}</td>
                        <td>{{ $price->has_range }}</td>
                        <td>
                            <a href="{{ route('uttpinspectionprice.create', $price->id) }}" class="btn btn-info btn-sm">Edit</a>
                            <a href="#" id="action" data-id="{{ $price->id }}" data-action="delete" class="btn btn-warning btn-sm">Delete</a>
                            
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function () {
    
    $('#tUser').DataTable();

    $("a#action").click(function(e)
    {
        e.preventDefault();
        var data = $(this).data();
        data._token = "{{ csrf_token() }}";
        $.post("{{ route('uttpinspectionprice.action') }}",data,function(response)
        {
            if(response.status==true)
            {
                toastr["info"]("Data berhasil dihapus", "Info");
                setTimeout(function()
                {
                    window.location.href = "{{ route("uttpinspectionprice") }}";
                },3000);
            }
            else
            {
                toastr["error"](response["message"], "Error");
            }
        });
    });

});
</script>
@endsection