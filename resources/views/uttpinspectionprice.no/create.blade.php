@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">

@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="service_type_id">Jenis Layanan</label> 
                        {!! Form::select('service_type_id', 
                            $types,
                            $row?$row->service_type_id:'', 
                            ['class' => 'form-control', 'id' => 'service_type_id', 'placeholder' => '- Pilih Layanan -', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_type">Jenis Pengujian</label> 
                        {!!
                            Form::text("inspection_type",$row?$row->inspection_type:'',[
                            'class' => 'form-control',
                            'id' => 'inspection_type',
                            'placeholder' => 'Jenis Pengujian',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="price">Harga</label> 
                        {!!
                            Form::number("price",$row?$row->price:'',[
                            'class' => 'form-control',
                            'id' => 'price',
                            'placeholder' => 'Harga',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="unit">Unit</label> 
                        {!!
                            Form::text("unit",$row?$row->unit:'',[
                            'class' => 'form-control',
                            'id' => 'unit',
                            'placeholder' => 'Unit',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="instalasi_id">Nama Instalasi</label> 
                        {!! Form::select('instalasi_id', 
                            $instalasi,
                            $row?$row->instalasi_id:'', 
                            ['class' => 'form-control', 'id' => 'instalasi_id', 'placeholder' => '- Pilih Instalasi -', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_template_id">ID Template</label> 
                        {!!
                            Form::number("inspection_template_id",$row?$row->inspection_template_id:'',[
                            'class' => 'form-control',
                            'id' => 'inspection_template_id',
                            'placeholder' => 'ID Template',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="has_range">Berlaku Rentang?</label> 
                        {!! Form::select("has_range", ['ya'=>'Ya','tidak'=>'Tidak'], $row? ($row->has_range ? 'ya' :'tidak') : 'tidak', 
                        ['class' => 'form-control select2','id' => 'has_range', 'required']) !!}
                    </div>
                    
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')

<script>
$(document).ready(function ()
{
    

    $('#instalasi_id,#service_type_id,#has_range').select2({
        // placeholder: "- Pilih UML -",
        allowClear: true
    });

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('uttpinspectionprice.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('uttpinspectionprice') }}';
            }
        });

    });
});

</script>
@endsection