@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            
            <br/>
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Tgl Terima</th>
                        <th>Pemilik</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    // $('#data_table').DataTable({
    //     scrollX: true,
    // });

    // $("#no_order").change(function() {
    //     var no_order = $(this).val();

    //     var form = $('#qr');
    //     form[0].submit();
    // });


    let table2 = $('#data_table').DataTable( {
                responsive: true,
                processing: true,
                serverSide: true,
                destroy:true,
                searching: {
                    return: true
                },
                ajax: {
                    url: '{{ route('servicerevisionuut.getorders') }}',
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: function(data) {
                        data.cSearch = $("#search").val();
                    }
                },
                order: ['1', 'DESC'],
                pageLength: 20,
                // searching: true,
                aoColumns: [
                    {
                        data: 'id',
                        "visible": true,
                        "searchable": false
                    },
                    {
                        data: 'service_request_item.no_order',
                        "searchable": true,
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        render: function(data,type, row){
                            return row.tool_brand + '/' + row.tool_model + '/' + row.tool_type + row.tool_serial_no + '<br/> Kapasitas: ' + row.tool_capacity + row.tool_capacity_unit;
                        },
                        "searchable": true
                    },
                    {
                        data: 'received_at',
                        "searchable": false,
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        data: 'service_request.requestor.full_name',
                        "searchable": false,
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        data: 'stat_sertifikat',
                        "searchable": false,
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        data: 'id',
                        data: function(data,type,row){
                            let url = `<a href="{{ route('servicerevisionuut.sendtolab',['id'=>'row::id','oid'=>'row::oid']) }}" class="btn btn-warning btn-sm">Kembalikan</a>`;
                            url = url.replace('row::id', data.service_request_item.id);
                            url = url.replace('row::oid', data.id);
                            console.log(url)
                            return  url;
                        }
                    },
                    
                    
                ]
            });
            table2.columns.adjust().responsive.recalc();
});

</script>
@endsection