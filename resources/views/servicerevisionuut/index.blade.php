@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <!-- <div class="tab-content" id="nav-tabContent">
        <div >
            <br/>
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
 
                    <table id="data_table" class="table table-striped table-hover table-responsive-sm data-table">
                        <thead>
                            <tr>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Status Alat</th>
                                <th>Catatan Revisi</th>
                                <th>Berkas</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows as $row)
                            <tr>
                                <td>{{ $row->ServiceRequestItem->no_order }}</td>
                                <td>{{ $row->ServiceRequestItem->uuts->tool_brand }}/{{ $row->ServiceRequestItem->uuts->tool_model }}/{{ $row->ServiceRequestItem->uuts->tool_type }} ({{ $row->ServiceRequestItem->uuts->serial_no ? $row->ServiceRequestItem->uuts->serial_no : '-' }})</td> 
                                <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : "" }}</td>
                                <td>{{ $row->TestBy1 ? $row->TestBy1->full_name : "" }} {{ $row->TestBy2 ? ' & '.$row->TestBy2->full_name : "" }}</td>
                                <td>{{ $row->staff_entry_datein }}</td>
                                <td>{{ $row->staff_entry_dateout }}</td>
                                <td>
                                    {{ $row->stat_warehouse == 2 ? 'Sudah Diserahterimakan dengan Pemohon' : 
                                        ($row->stat_warehouse == 1 ? 'Berada di Gudang' : 'Berada di Instalasi')}}
                                </td>
                                <td>{{ $row->subkoordinator_notes }}
                                    @if ($row->file_review_subkoordinator != null)
                                    <a href="{{ route('approveuttp.downloadReview', ['type'=>'subko', 'id'=>$row->id]) }}" class="btn btn-warning btn-sm">DOWNLOAD REVIEW SUBKO</a>
                                    @endif
                                    @if ($row->file_review_kabalai != null)
                                    <a href="{{ route('approveuttp.downloadReview', ['type'=>'kabalai', 'id'=>$row->id]) }}" class="btn btn-warning btn-sm">DOWNLOAD REVIEW KABALAI</a>
                                    @endif
                                </td>
                                <td>
                                    @if ($row->ServiceRequest->service_type_id == 4)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/type_approval_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Persetujuan Tipe</a>
                                    @elseif ($row->ServiceRequest->service_type_id == 5)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Sertifikat Sebelumnya</a>
                                    @elseif ($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                                        @if ($row->ServiceRequestItem->path_calibration_manual != null)
                                        <a href="<?= config('app.siks_url') ?>/tracking/download/calibration_manual/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Manual Kalibrasi</a>
                                        @endif
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/manual_book/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Buku Manual</a>
                                    @endif
                                </td>
                                <td>
                                @if($row->stat_service_order=="0")
                                @elseif($row->stat_service_order=="1")
                                    @if($row->stat_sertifikat==0)
                                    <a href="{{ route('servicerevisionuttp.result', $row->id) }}" class="btn btn-warning btn-sm">PERBAIKI HASIL UJI</a>
                                    @endif
                                    @if($row->file_skhp!==null)
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif<a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                    @endif
                                @elseif($row->stat_service_order=="2")
                                    <a href="{{ route('servicerevisionuttp.result', $row->id) }}" class="btn btn-warning btn-sm">PERBAIKI HASIL UJI</a>
                                    @if($row->file_skhp!==null)
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                    <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                    @endif
                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table> -->
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-12">
                {!! Form::open(['url' => route('serviceuut.savetestqr'), 'id' => 'qr'])  !!}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="no_order">Selesai Uji Berdasar QR Code</label>
                                    {!! Form::text('no_order', null, ['class' => 'form-control','id' => 'no_order', 'autofocus' => 'autofocus']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="error_notes" clsas=""></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/>
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Pengujian</th>
                        <th>Diterima Oleh</th>
                        <th>Diuji Oleh</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th>Status Alat</th>
                        <th>Catatan Revisi</th>
                        <th>Berkas</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequestItem ? $row->ServiceRequestItem->no_order :'' }}</td>
                        <td>{{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                        <td>
                            @foreach($row->ServiceRequestItem->inspections as $inspection)
                            {{ $inspection->inspectionPrice->inspection_type }}
                            <br/>
                            @endforeach
                        </td>
                        <td>{{ $row->MasterUsers->full_name ?? ""}}</td>
                        <td>{{ $row->test1 ? $row->test1->full_name : "" }} {{ $row->test2 ? ' & '. $row->test2->full_name : "" }}</td>
                        <td>{{ date("d-m-Y", strtotime($row->staff_entry_datein)) }}</td>
                        <td>{{ $row->staff_entry_dateout != null ? date("d-m-Y", strtotime($row->staff_entry_dateout)) : '' }}</td>
                        <td>
                            {{ $row->stat_warehouse == 2 ? 'Sudah Diserahterimakan dengan Pemohon' : 
                                ($row->stat_warehouse == 1 ? 'Berada di Gudang' : 'Berada di Instalasi')}}
                        </td>
                        <td>{{ $row->subkoordinator_notes }}
                            @if ($row->file_review_subkoordinator != null)
                            <a href="{{ route('approveuut.downloadReview', ['type'=>'subko', 'id'=>$row->id]) }}" class="btn btn-warning btn-sm">DOWNLOAD REVIEW SUBKO</a>
                            @endif
                            @if ($row->file_review_kabalai != null)
                            <a href="{{ route('approveuut.downloadReview', ['type'=>'kabalai', 'id'=>$row->id]) }}" class="btn btn-warning btn-sm">DOWNLOAD REVIEW KABALAI</a>
                            @endif
                        </td>
                        <td>
                            <?php if ($row->ServiceRequest->service_type_id == 1 || $row->ServiceRequest->service_type_id == 2): ?>
                                <!-- <a href="<?= config('app.siks_url') ?>/tracking/download_uut/type_approval_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Persetujuan Tipe</a> -->
                                <?php if($row->ServiceRequestItem->file_manual_book !=''): ?>
                                    <a href="<?= config('app.siks_url') ?>/tracking/download_uut/manual_book/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Buku Manual</a>
                                <?php endif ?>

                                <?php if($row->ServiceRequest->booking->file_certificate !=null): ?>
                                    <a href="<?= config('app.siks_url') ?>/booking/download_permohonan/certificate/{{ $row->ServiceRequest->booking->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                                <?php endif ?>

                                
                            <?php elseif ($row->ServiceRequest->service_type_id == 5): ?>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Sertifikat Sebelumnya</a>
                            <?php elseif ($row->ServiceRequest->service_type_id == 1): ?>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                            <?php endif ?>
                        </td>
                        <td>
                        @if($row->stat_service_order=="0")
                        @elseif($row->stat_service_order=="1")
                            @if($row->stat_sertifikat==0)
                            <a href="{{ route('servicerevisionuut.result', $row->id) }}" class="btn btn-warning btn-sm">PERBAIKI HASIL UJI</a>
                            @endif
                            @if($row->file_skhp!==null)
                            <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                            <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                            
                            <a href="{{ route('serviceuut.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD CERAPAN</a>
                                @if($row->file_lampiran!==null)
                                <a href="{{ route('serviceuut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                @endif
                            @endif
                        @elseif($row->stat_service_order=="2")
                            <a href="{{ route('servicerevisionuut.result', $row->id) }}" class="btn btn-warning btn-sm">PERBAIKI HASIL UJI</a>
                            @if($row->file_skhp!==null)
                            <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                            <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                            
                            <a href="{{ route('serviceuut.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD CERAPAN</a>
                                @if($row->file_lampiran!==null)
                                <a href="{{ route('serviceuut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                @endif
                            @endif
                        @elseif($row->stat_service_order=="4")
                            <a href="{{ route('servicerevisionuut.result', $row->id) }}" class="btn btn-warning btn-sm">PERBAIKI HASIL UJI</a>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('#data_table').DataTable({
        scrollX: true,
    });

    $("#no_order").change(function() {
        var no_order = $(this).val();

        var form = $('#qr');
        form[0].submit();

        /*
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{ route('orderuttp.proses') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
        */
    });
});

</script>
@endsection