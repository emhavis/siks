@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <div class="tab-content" id="nav-tabContent">
        <div >
            <br/>
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
 
                    <table id="data_table" class="table table-striped table-hover table-responsive-sm data-table">
                        <thead>
                            <tr>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Pengujian</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Status Alat</th>
                                <th>Catatan Revisi</th>
                                <th>Berkas</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows as $row)
                            <tr>
                                <td>{{ $row->ServiceRequestItem->no_order }}</td>
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>
                                    @foreach($row->ServiceRequestItem->inspections as $inspection)
                                    {{ $inspection->inspectionPrice->inspection_type }}
                                    <br/>
                                    @endforeach
                                </td>
                                <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : "" }}</td>
                                <td>{{ $row->test1 ? $row->test1->full_name : "" }} {{ $row->test2 ? ' & '.$row->test2->full_name : "" }}</td>
                                <td>{{ $row->staff_entry_datein }}</td>
                                <td>{{ $row->staff_entry_dateout }}</td>
                                <td>
                                    {{ $row->stat_warehouse == 2 ? 'Sudah Diserahterimakan dengan Pemohon' : 
                                        ($row->stat_warehouse == 1 ? 'Berada di Gudang' : 'Berada di Instalasi')}}
                                </td>
                                <td>{{ $row->subkoordinator_notes }}
                                    @if ($row->file_review_subkoordinator != null)
                                    <a href="{{ route('approveuttp.downloadReview', ['type'=>'subko', 'id'=>$row->id]) }}" class="btn btn-warning btn-sm">DOWNLOAD REVIEW SUBKO</a>
                                    @endif
                                    @if ($row->file_review_kabalai != null)
                                    <a href="{{ route('approveuttp.downloadReview', ['type'=>'kabalai', 'id'=>$row->id]) }}" class="btn btn-warning btn-sm">DOWNLOAD REVIEW KABALAI</a>
                                    @endif
                                </td>
                                <td>
                                    @if ($row->ServiceRequest->service_type_id == 4)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/type_approval_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Persetujuan Tipe</a>
                                    @elseif ($row->ServiceRequest->service_type_id == 5)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Sertifikat Sebelumnya</a>
                                    @elseif ($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                                        @if ($row->ServiceRequestItem->path_calibration_manual != null)
                                        <a href="<?= config('app.siks_url') ?>/tracking/download/calibration_manual/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Manual Kalibrasi</a>
                                        @endif
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/manual_book/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Buku Manual</a>
                                    @endif
                                </td>
                                <td>
                                @if($row->stat_service_order=="0")
                                @elseif($row->stat_service_order=="1")
                                    @if($row->stat_sertifikat==0)
                                    <a href="{{ route('servicerevisionuttp.result', $row->id) }}" class="btn btn-warning btn-sm">PERBAIKI HASIL UJI</a>
                                    @endif
                                    @if($row->file_skhp!==null)
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif<a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                    @endif
                                @elseif($row->stat_service_order=="2")
                                    <a href="{{ route('servicerevisionuttp.result', $row->id) }}" class="btn btn-warning btn-sm">PERBAIKI HASIL UJI</a>
                                    @if($row->file_skhp!==null)
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                    <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                    @endif
                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('.data-table').DataTable({
        scrollX: true,
    });
});

</script>
@endsection