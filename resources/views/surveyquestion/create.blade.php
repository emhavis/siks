@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row col-md-6">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup" action="{{ route('question.store')}}" enctype="multipart/form-data">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="page_id">Halaman Survey</label> 
                        {!!
                            Form:: select("page_id",$page,$row?$row->page_id:'',[
                            'class' => 'form-control',
                            'id' => 'page_id',
                            'placeholder' => 'Page',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="sequence">Squence</label> 
                        {!!
                            Form::text("sequence",$row?$row->sequence:'',[
                            'class' => 'form-control',
                            'id' => 'sequence',
                            'placeholder' => 'Sequence',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="question">Pertanyaan</label> 
                        {!!
                            Form::textArea("question",$row?$row->question:'',[
                            'class' => 'form-control',
                            'id' => 'question',
                            'rows' => 2,
                            'placeholder' => 'Question',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="instalasi_id">Deskripsi Pertanyaan</label> 
                        {!!
                            Form :: textArea('description',$row?$row->description:'',[
                                'class' => 'form-control',
                                'id' => 'description',
                                'rows' => 4,
                                'placeholder' => 'Descriptions',
                                'required'
                                ]);

                        !!}
                    </div>
                    <div class="form-group">
                        <label for="question_type">Jenis Pertanyaan</label> 
                        {!!
                            Form::text("question_type",$row?$row->question_type:'',[
                            'class' => 'form-control',
                            'id' => 'question_type',
                            'placeholder' => 'Type Of Question',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-12">
                        <div class="row col-12">
                            <div class="col-6">
                                <label for="possible_answer">Kemungkinan Jawaban</label>    &nbsp; &nbsp; 
                                {!!
                                    Form::text("possible_answers",$row?$row->possible_answers:'',[
                                    'class' => 'form-control',
                                    'id' => 'possible_answers',
                                    'placeholder' => 'Possible Answers',
                                    'checked' => 'false'
                                    ]);
                                !!}
                            </div>
                        </div>
                    </div>
                    <div class="regForm">

                    </div>
                    <div id="addForm">
                    
                    </div>
                    
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    
$(document).ready(function ()
{
    document.getElementById('question').style.resize = false;
    document.getElementById('addForm').style.display= 'none';  
    var checkbox = document.querySelector("input[name=has_range]");
    checkbox.checked =''

    checkbox.addEventListener('change', function() {
    if (this.checked) {
        document.getElementById('addForm').style.display= 'block';; 
    } else {
        document.getElementById('addForm').style.display= 'none';  
    }
    });
    $('#template_id').select2({
    });
    
    // $("#submit").click(function(e)
    // {
    //     e.preventDefault();

    //     var formData = $("#createGroup").serialize();

    //     $("#panel_create").toggleClass("ld-loading");
    //     $.post("{{ route('insprice.store') }}",formData,function(response)
    //     {
    //         $("#panel_create").toggleClass("ld-loading");
    //         console.log(response.messages)
    //         if(response.status==false)
    //         {
    //             var msg = show_notice(response.messages);
    //         }
    //         else
    //         {
    //             window.location = "{{ route('insprice') }}";
    //         }
    //     });

    // });
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection