@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUser" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Nama Lengkap</th>
                        <th>Jenis</th>
                        <th>Aktif?</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->full_name }}</td>
                        <td>{{ $row->kantor }}</td>
                        <td>{{ $row->is_active ? 'Ya' : 'Tidak' }}</td>
                        <td>
                            <button class="btn btn-accent btn-sm btn-mdl" style="margin-right:2px;"
                                    data-id="{{ $row->id }}" 
                                    data-full_name="{{ $row->full_name }}"
                                    data-kantor="{{ $row->kantor }}"
                                    data-email="{{ $row->email }}"
                                    data-phone="{{ $row->phone }}"
                                    >
                                <i class="fa fa-info"></i>
                            </button>
                    
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>        
    </div>
</div>
</div>

 <div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Data Customer</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <input type="hidden" name="id" id="id"/>

                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" id="full_name" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Jenis</label>
                                <input type="text" id="kantor" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" id="email" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Telepon</label>
                                <input type="text" id="phone" class="form-control" readonly required />
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>       <!-- End Delete Modal --> 
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script>
$(document).ready(function () {
    
    $('#tUser').DataTable();

    $('#tUser tbody').on( 'click', 'button', function (e) {
        e.preventDefault();
        var id = $(this).data().id;
        var full_name = $(this).data().full_name;
        var kantor = $(this).data().kantor;
        var email = $(this).data().email;
        var phone = $(this).data().phone;

        $("#id").val(id);
        $("#full_name").val(full_name);
        $("#kantor").val(kantor);
        $("#email").val(email);
        $("#phone").val(phone);
        

        

        $("#prosesmodal").modal('show');

    });
});
</script>
@endsection