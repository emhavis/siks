@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <!-- <a href="{{ route('requestuttp.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Pendaftaran</a> -->
    <!-- <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a> -->

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#frontdesk" aria-controls="frontdesk" role="tab" data-toggle="tab">Pendaftaran</a></li>
        <li role="presentation"><a href="#processing" aria-controls="processing" role="tab" data-toggle="tab">Proses</a></li>
        <li role="presentation"><a href="#done" aria-controls="done" role="tab" data-toggle="tab">Selesai</a></li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="frontdesk">
            
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    

                    <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Sertifikat</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_daftar as $row)
                            <tr>
                                <td>{{ $row->order ? $row->order->no_sertifikat : ''}}</td>
                                <td>{{ $row->request ? $row->request->label_sertifikat : ''}}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->masterstatus->status }}</td>
                                <td>
                                    <a href="{{ route('revisionuttp.edit', $row->id) }}" class="btn btn-warning btn-sm">Terima</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="processing">
            <div class="panel panel-filled table-area">
                <br/>
                <div class="panel-heading">
                    <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Sertifikat</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_proses as $row)
                            <tr>
                                <td>{{ $row->order ? $row->order->no_sertifikat : '' }}</td>
                                <td>{{ $row->request ? $row->request->label_sertifikat : ''}}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->masterstatus->status }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="done">
            <div class="panel panel-filled table-area">
                <br/>
                <div class="panel-heading">
                    <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Sertifikat</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_selesai as $row)
                            <tr>
                                <td>{{ $row->order ? $row->order->no_sertifikat : '' }}</td>
                                <td>{{ $row->request ? $row->request->label_sertifikat : '' }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->masterstatus->status }}</td>
                                <td>
                                    @if($row->status == 4 && $row->status_approval == 2)
                                    @if (!$row->order->is_skhpt)
                                    <a href="{{ route('revisiontechorderuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('revisiontechorderuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->request->service_type_id == 6 || $row->request->service_type_id == 7)
                                    <a href="{{ route('revisiontechorderuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('revisiontechorderuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->order->ujitipe_completed==true || $row->order->has_set)
                                    <a href="{{ route('revisiontechorderuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('revisiontechorderuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data,#table_data_process,#table_data_done").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $('.btn-simpan').click(function(e){
            e.preventDefault();
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });
    });
</script>
@endsection