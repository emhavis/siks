@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">

        {!! Form::open(['id' => 'form_create_request', 'route' => ['revisionuttp.simpanedit', $revision->id]]) !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Perbaikan Sertifikat</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', $revision->request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_sertifikat">No Order</label>
                            {!! Form::text('no_sertifikat', $revision->order->ServiceRequestItem->no_order, ['class' => 'form-control','id' => 'no_order', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_sertifikat">No SKHP</label>
                            {!! Form::text('no_sertifikat', $revision->order->no_sertifikat, ['class' => 'form-control','id' => 'no_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @if($revision->request->service_type_id == 6 || $revision->request->service_type_id == 7)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_sertifikat">No SKHPT</label>
                            {!! Form::text('no_sertifikat', $revision->order->no_sertifikat_tipe, ['class' => 'form-control','id' => 'no_sertifikat_tipe', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_sertifikat">No SET</label>
                            {!! Form::text('no_sertifikat', $revision->order->np_surat_tipe, ['class' => 'form-control','id' => 'np_surat_tipe', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="label_sertifikat">Label Sertifikat</label>
                            {!! Form::text('label_sertifikat', $revision->request->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Sertifikat</label>
                            {!! Form::text('addr_sertifikat', $revision->request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                </div>


                @if($revision->request->service_type_id == 6 || $revision->request->service_type_id == 7)
                <div class="row">
                    @if($revision->order->no_sertifikat_tipe != null)
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_sertifikat">No SKHPT</label>
                            {!! Form::text('no_sertifikat', $revision->order->no_sertifikat_tipe, ['class' => 'form-control','id' => 'no_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                    @endif
                    @if($revision->order->no_surat_tipe != null)
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_sertifikat">No SET</label>
                            {!! Form::text('no_sertifikat', $revision->order->no_surat_tipe, ['class' => 'form-control','id' => 'no_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                    @endif
                </div>
                @endif
                
                @if($revision->uttp_type_id != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="uttp_type_id_old">Jenis Alat (Tertulis)</label>
                            {!! Form::text('uttp_type_id_old', $revision->order->ServiceRequestItem->uttp->type->uttp_type, ['class' => 'form-control','id' => 'uttp_type_id_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="uttp_type_id_new">Jenis Alat (Pembetulan)</label>
                            {!! Form::text('uttp_type_id_new', $revision->uttpType->uttp_type, ['class' => 'form-control','id' => 'uttp_type_id_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif

                @if($revision->tool_serial_no != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_serial_no_old">No Seri (Tertulis)</label>
                            {!! Form::text('tool_serial_no_old', $revision->order->tool_serial_no, ['class' => 'form-control','id' => 'tool_serial_no_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_serial_no_new">No Seri (Pembetulan)</label>
                            {!! Form::text('tool_serial_new', $revision->tool_serial_no, ['class' => 'form-control','id' => 'tool_serial_no_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_brand != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_brand_old">Merek (Tertulis)</label>
                            {!! Form::text('tool_brand_old', $revision->order->tool_brand, ['class' => 'form-control','id' => 'tool_brand_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_brand_new">Merek (Pembetulan)</label>
                            {!! Form::text('tool_brand_new', $revision->tool_brand, ['class' => 'form-control','id' => 'tool_brand_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_model != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_model_old">Model/Tipe (Tertulis)</label>
                            {!! Form::text('tool_model_old', $revision->order->tool_model, ['class' => 'form-control','id' => 'tool_model_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_model_new">Model/Tipe (Pembetulan)</label>
                            {!! Form::text('tool_model_new', $revision->tool_model, ['class' => 'form-control','id' => 'tool_model_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_type != null)
                <!--
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_type_old">Tipe (Tertulis)</label>
                            {!! Form::text('tool_type_old', $revision->order->ServiceRequestItem->uttp->tool_type, ['class' => 'form-control','id' => 'tool_type_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_type_new">Tipe (Pembetulan)</label>
                            {!! Form::text('tool_type_new', $revision->tool_type, ['class' => 'form-control','id' => 'tool_type_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                -->
                @endif
                @if($revision->tool_capacity != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_capacity_old">Kapasitas Maksimal (Tertulis)</label>
                            {!! Form::text('tool_capacity_old', $revision->order->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_capacity_new">Kapasitas Maksimal (Pembetulan)</label>
                            {!! Form::text('tool_capacity_new', $revision->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_capacity_min != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_capacity_min_old">Kapasitas Minimal (Tertulis)</label>
                            {!! Form::text('tool_capacity_min_old', $revision->order->tool_capacity_min, ['class' => 'form-control','id' => 'tool_capacity_min_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_capacity_min_new">Kapasitas Minimal (Pembetulan)</label>
                            {!! Form::text('tool_capacity_min_new', $revision->tool_capacity_min, ['class' => 'form-control','id' => 'tool_capacity_min_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_media != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_media_old">Media (Tertulis)</label>
                            {!! Form::text('tool_media_old', $revision->order->tool_media, ['class' => 'form-control','id' => 'tool_media_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_media_new">Media (Pembetulan)</label>
                            {!! Form::text('tool_media_new', $revision->tool_media, ['class' => 'form-control','id' => 'tool_media_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                
                @if($revision->tool_made_in != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_made_in_old">Buatan (Tertulis)</label>
                            {!! Form::text('tool_made_in_old', $revision->order->ServiceRequestItem->uttp->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_made_in_new">Buatan (Pembetulan)</label>
                            {!! Form::text('tool_made_in_new', $revision->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif

                @if($revision->tool_factory != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_old">Pabrikan (Tertulis)</label>
                            {!! Form::text('tool_factory_old', $revision->order->tool_factory, ['class' => 'form-control','id' => 'tool_factory_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_new">Pabrikan (Pembetulan)</label>
                            {!! Form::text('tool_factory_new', $revision->tool_factory, ['class' => 'form-control','id' => 'tool_factory_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_factory_address != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_address_old">Alamat Pabrikan (Tertulis)</label>
                            {!! Form::textarea('tool_factory_address_old', $revision->order->ServiceRequestItem->uttp->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_address_new">Alamat Pabrikan (Pembetulan)</label>
                            {!! Form::textarea('tool_factory_address_new', $revision->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif

                @if($revision->label_sertifikat != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="label_sertifikat_old">Pemohon (Tertulis)</label>
                            {!! Form::text('label_sertifikat_old', $revision->request->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="label_sertifikat_new">Pemohon (Pembetulan)</label>
                            {!! Form::text('label_sertifikat_new', $revision->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->addr_sertifikat != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="addr_sertifikat_old">Alamat Pemohon (Tertulis)</label>
                            {!! Form::textarea('addr_sertifikat_old', $revision->request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="addr_sertifikat_new">Alamat Pemohon (Pembetulan)</label>
                            {!! Form::textarea('addr_sertifikat_new', $revision->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif


                @if($revision->others != null)
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="others">Catatan Pembetulan Lainnya</label>
                            {!! Form::textarea('others', $revision->others, ['class' => 'form-control','id' => 'others', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>

        

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_submit">Simpan dan Konfirmasi</button> 
        
        {!! Form::close() !!}

        </div>
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">


</script>
@endsection