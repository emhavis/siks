@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <a href="{{ route('petugas.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUser" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Status</th>
                        <th>Penugasan</th>
                    </tr>
                </thead>
                <tbody>
                    @if($petugas->count())
                    @foreach($petugas as $p)
                    <tr>
                        <td>{{ $p->nip }}</td>
                        <td>{{ $p->nama }}</td>
                        <td>
                            @if($p->scheduled_id == null)
                            <button class="btn btn-sm btn-success">Belum ada penugasan</button>
                            @else
                            <button class="btn btn-sm btn-danger">Sudah ada penugasan</button>
                            @endif
                        </td>
                        <td>
                            @if($p->scheduled_id != null)
                            Surat Tugas No: {{ $p->doc_no }}, Tanggal {{ date('d-m-Y', strtotime($p->accepted_date)) }}<br/>
                            Tanggal Dinas: {{ date('d-m-Y', strtotime($p->date_from)) }} s/d {{ date('d-m-Y', strtotime($p->date_to)) }}<br/>
                            Lokasi: {{ $p->kabkot }}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function () {
    
    $('#tUser').DataTable();

    $("a#action").click(function(e)
    {
        e.preventDefault();
        var data = $(this).data();
        data._token = "{{ csrf_token() }}";
        $.post("{{ route('petugas.action') }}",data,function(response)
        {
            if(response.status==true)
            {
                toastr["info"]("Data berhasil dihapus", "Info");
                setTimeout(function()
                {
                    window.location.href = "{{ route("petugas") }}";
                },3000);
            }
            else
            {
                toastr["error"](response["message"], "Error");
            }
        });
    });

});
</script>
@endsection