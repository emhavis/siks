<p>Yth. Bapak/Ibu, {{ $customer->full_name }},</p>

<p>Kami beritahukan bahwa kami telah menerima permohonan perubahan password akun pada SIMPEL UPTP IV sebagai berikut:</p>
		
<p>
Nama Lengkap: {{ $customer->full_name }}<br/>
Username: {{ $customer->username }}</p>

<p>Ubah password user Anda <a href="{{ $url }}">disini</a> atau salin alamat ini: 
<a href="{{ $url }}">{{ $url }}</a> pada browser yang biasa Anda gunakan.</p>

<p>Direktorat Metrologi</p>

<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>
