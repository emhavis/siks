<p>
Yth. Bapak/Ibu Pimpinan<br/>
{{ $order->uttp->owner->nama }}<br/>
Di Tempat
</p>

<p>Bersama ini kami informasikan bahwa alat ukur, alat takar, alat timbang, dan alat perlengkapan (UTTP) milik Saudara sebagai berikut:</p>

<p>
Jenis Alat Ukur: {{ $order->ServiceRequestItem->uttp->type->uttp_type }}<br/>
Merk: {{ $order->ServiceRequestItem->uttp->tool_brand }}<br/>
Model/Tipe: {{ $order->ServiceRequestItem->uttp->tool_model }}<br/>
No Seri/No Tag: {{ $order->ServiceRequestItem->uttp->serial_no }}<br/>
</p>

<p>Masa berlaku tanda tera UTTP tersebut akan/telah habis pada {{ date('d-m-Y', strtotime($order->sertifikat_expired_at)) }}</p>

<p>
Hormat kami,<br/>
UPTP IV <br/>
Direktorat Metrologi Kementerian Perdagangan
</p>

<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>