<p>Yth. Bapak/Ibu, {{ $order->ServiceRequest->requestor->full_name }}</p>

<p>Kami beritahukan bahwa pengujian/pemeriksaan untuk permohonan sebagai berikut:</p>

<p>
Nomor Pendaftaran: {{ $order->ServiceRequest->no_register }}<br/>
@if($order->ServiceRequest->Owner != null)
Nama Perusahaan: {{ $order->ServiceRequest->Owner->nama }}<br/>
Alamat Perusahaan: {{ $order->ServiceRequest->Owner->alamat }}<br/>
NIB Perusahaan: {{ $order->ServiceRequest->Owner->nib }}<br/>
NPWP Perusahaan: {{ $order->ServiceRequest->Owner->npwp }}<br/>
Telepon: {{ $order->ServiceRequest->Owner->telepon }}<br/>
Penanggung Jawab: {{ $order->ServiceRequest->Owner->penanggung_jawab }}<br/>
E-mail: {{ $order->ServiceRequest->Owner->email }}<br/>
@endif
Nama Alat: {{ $order->ServiceRequestItem->uttp->type->uttp_type }}<br/>
Merk Alat: {{ $order->ServiceRequestItem->uttp->tool_brand }}<br/>
Model/Type Alat: {{ $order->ServiceRequestItem->uttp->tool_model }}<br/>
No Seri Alat: {{ $order->ServiceRequestItem->uttp->serial_no }}<br/>
Nama Pemilik Alat pada Sertifikat: {{ $order->ServiceRequest->label_sertifikat }}<br/>
Alamat Pemilik Alat pada Sertifikat: {{ $order->ServiceRequest->addr_sertifikat }}<br/>
</p>

<p><strong>mengalami penundaan</strong> pengerjaan dikarenakan {{ $order->pending_notes }}.</p>

<p>Direktorat Metrologi</p>

<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>