<p>Yth. Bapak/Ibu, {{ $customer->full_name }},</p>

<p>Kami beritahukan bahwa akun pengguna Anda pada SIMPEL UPTP IV telah aktif. </p>
		
<p>Klik <a href="{{ $url }}">disini</a> untuk masuk ke aplikasi SIMPEL UPTP IV.</p>

<p>Direktorat Metrologi</p>

<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>
