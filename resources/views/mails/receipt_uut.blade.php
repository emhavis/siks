<p>Yth. Bapak/Ibu {{ $srvReq->requestor->full_name }}, </p>

<p>Kami beritahukan bahwa kami telah menerbitkan <strong>kuitansi</strong> atas pembayaran layanan sesuai permohonan pengujian/pemeriksaan sebagai berikut:</p>

<p>
Nomor Pendaftaran: {{ $srvReq->no_register }}<br/>
@if($srvReq->Owner != null)
Nama Perusahaan: {{ $srvReq->Owner->nama }}<br/>
Alamat Perusahaan: {{ $srvReq->Owner->alamat }}<br/>
NIB Perusahaan: {{ $srvReq->Owner->nib }}<br/>
NPWP Perusahaan: {{ $srvReq->Owner->npwp }}<br/>
Telepon: {{ $srvReq->Owner->telepon }}<br/>
Penanggung Jawab: {{ $srvReq->Owner->penanggung_jawab }}<br/>
E-mail: {{ $srvReq->Owner->email }}<br/>
@endif
@foreach($srvReq->items as $item)
<br/>
Nama Alat: {{ $item->uuts->tool_name }}<br/>
Merk Alat: {{ $item->uuts->tool_brand }}<br/>
Model/Type Alat: {{ $item->uuts->tool_model }}/{{ $item->uuts->tool_type }}<br/>
No Seri Alat: {{ $item->uuts->serial_no }}<br/>
@endforeach
<br/>
Nama Pemilik Alat pada Sertifikat: {{ $srvReq->label_sertifikat }}<br/>
Alamat Pemilik Alat pada Sertifikat: {{ $srvReq->addr_sertifikat }}<br/>
<br/>
Jumlah Pembayaran: {{ $srvReq->total_price }}<br/>
Kode Billing: {{ $srvReq->billing_code }}<br/>
Tanggal Pembayaran: {{ date("d M y", strtotime($srvReq->payment_date)) }}<br/>
Nomor Pembayaran: {{ $srvReq->payment_code }}<br/>
</p>

<p>Direktorat Metrologi</p>

<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>