@if($petugas != null)
<p>Yth. Bapak/Ibu {{ null != $petugas ? $petugas->nama : '' }}, </p>
@else 
<p>Yth. Bapak/Ibu {{ $srvReq->requestor->full_name }}, </p>
@endif

<p>Kami informasikan bahwa untuk layanan Pengujian/Pemeriksaan atas:</p>

<p>
Nomor Pendaftaran: {{ $srvReq->no_register }}<br/>
@if($srvReq->Owner != null)
Nama Perusahaan: {{ null != $srvReq->Owner ? $srvReq->Owner->nama : '' }}<br/>
Alamat Perusahaan: {{ $srvReq->Owner->alamat }}<br/>
NIB Perusahaan: {{ $srvReq->Owner->nib }}<br/>
NPWP Perusahaan: {{ $srvReq->Owner->npwp }}<br/>
Telepon: {{ $srvReq->Owner->telepon }}<br/>
Penanggung Jawab: {{ $srvReq->Owner->penanggung_jawab }}<br/>
E-mail: {{ $srvReq->Owner->email }}<br/>
@endif
@foreach($srvReq->items as $item)
<br/>
Nama Alat: {{ $item->uuts->stdtype->uut_type }}<br/>
Merk Alat: {{ $item->uuts->tool_brand }}<br/>
Model/Type Alat: {{ $item->uuts->tool_model }}<br/>
No Seri Alat: {{ $item->uuts->serial_no }}<br/>
@endforeach
<br/>
Nama Pemilik Alat pada Sertifikat: {{ $srvReq->label_sertifikat }}<br/>
Alamat Pemilik Alat pada Sertifikat: {{ $srvReq->addr_sertifikat }}<br/>
<br/>
akan dilaksanakan oleh: 
<br/>
@foreach($staffes as $staff)
Petugas {{ $loop-> iteration }}: {{ null != $staff->scheduledStaff ? $staff->scheduledStaff->nama : '' }} (NIP: {{ null != $staff->scheduledStaff ? $staff->scheduledStaff->nip : '' }})
@endforeach
<br/>
berdasarkan Surat Tugas Nomor {{ $srvReq->spuhDoc->doc_no }} tertanggal {{ $doc->accepted_date ? format_long_date($doc->accepted_date) : format_long_date(date("d-m-Y")) }}.
</p>

<p>Direktorat Metrologi</p>

<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>