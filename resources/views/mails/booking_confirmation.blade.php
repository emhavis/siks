<p>Yth. Bapak/Ibu {{ $srvReq->requestor->full_name }}, </p>

<p>Kami beritahukan bahwa kami telah mengkonfirmasi Booking Anda untuk permohonan sebagai berikut:</p>

<p>
Nomor Pendaftaran: {{ $srvReq->no_register }}<br/>
@if($srvReq->Owner != null)
Nama Perusahaan: {{ $srvReq->Owner->nama }}<br/>
Alamat Perusahaan: {{ $srvReq->Owner->alamat }}<br/>
NIB Perusahaan: {{ $srvReq->Owner->nib }}<br/>
NPWP Perusahaan: {{ $srvReq->Owner->npwp }}<br/>
Telepon: {{ $srvReq->Owner->telepon }}<br/>
Penanggung Jawab: {{ $srvReq->Owner->penanggung_jawab }}<br/>
E-mail: {{ $srvReq->Owner->email }}<br/>
@endif
@foreach($srvReq->items as $item)
<br/>
Nama Alat: {{ $item->uttp->type->uttp_type }}<br/>
Merk Alat: {{ $item->uttp->tool_brand }}<br/>
Model/Type Alat: {{ $item->uttp->tool_model }}<br/>
No Seri Alat: {{ $item->uttp->serial_no }}<br/>
@endforeach
<br/>
Nama Pemilik Alat pada Sertifikat: {{ $srvReq->label_sertifikat }}<br/>
Alamat Pemilik Alat pada Sertifikat: {{ $srvReq->addr_sertifikat }}<br/>
<br/>
<br/>
Silakan melakukan konfirmasi dan persetujuan Kaji Ulang pada aplikasi SIMPEL UPTP IV.
</p>

<p>Direktorat Metrologi</p>

<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>