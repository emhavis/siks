<p>Yth. Bapak/Ibu {{ $petugas->nama }}, </p>

<p>Kami beritahukan bahwa Anda dijadwalkan untuk melakukan Pengujian/Pemeriksaan Insitu atas:</p>

<p>
Nomor Pendaftaran: {{ $srvReq->no_register }}<br/>
@if($srvReq->Owner != null)
Nama Perusahaan: {{ $srvReq->Owner->nama }}<br/>
Alamat Perusahaan: {{ $srvReq->Owner->alamat }}<br/>
NIB Perusahaan: {{ $srvReq->Owner->nib }}<br/>
NPWP Perusahaan: {{ $srvReq->Owner->npwp }}<br/>
Telepon: {{ $srvReq->Owner->telepon }}<br/>
Penanggung Jawab: {{ $srvReq->Owner->penanggung_jawab }}<br/>
E-mail: {{ $srvReq->Owner->email }}<br/>
@endif
@foreach($srvReq->items as $item)
<br/>
Nama Alat: {{ $item->uuts->tool_name ? $item->uuts->tool_name : '' }}<br/>
Merk Alat: {{ $item->uttp->tool_brand }}<br/>
Model/Type Alat: {{ $item->uuts->tool_model }} / {{$item->uuts->stdtype->uut_type }}<br/>
No Seri Alat: {{ $item->uuts->serial_no }}<br/>
@endforeach
<br/>
Nama Pemilik Alat pada Sertifikat: {{ $srvReq->label_sertifikat }}<br/>
Alamat Pemilik Alat pada Sertifikat: {{ $srvReq->addr_sertifikat }}<br/>
<br/>
<br/>
Silakan melakukan pemeriksaan awal atas permohonan ini pada aplikasi SIMPEL UPTP IV.
</p>

<p>Direktorat Metrologi</p>

<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>