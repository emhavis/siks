@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Pengujian</th>
                        <th>Tgl Order</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->item->serviceRequest->no_order }}</td>
                        <td>
                            @if($row->item->uttp)
                            {{ $row->item->uttp->tool_brand }}/{{ $row->item->uttp->tool_model }}/{{ $row->item->uttp->tool_type}} ({{ $row->item->uttp->serial_no ? $row->item->uttp->serial_no : ''}})
                            @else
                            -
                            @endif
                        </td>
                        <td>{{ $row->inspectionPrice->inspection_type }}</td>
                        <td>{{ $row->item->serviceRequest->received_date }}</td>
                        <td>
                        <button class="btn btn-warning btn-sm btn-mdl" 
                            data-id="{{ $row->id }}" 
                            data-requestid="{{ $row->item->serviceRequest->id }}"
                            data-labid="{{ $laboratory_id }}"
                            data-instalasiid="{{ $row->inspectionPrice->instalasi_id }}">PROSES</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Proses Order</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                            <input type="hidden" name="id" id="id"/>
                            <input type="hidden" name="requestid" id="requestid"/>
                            <input type="hidden" name="labid" id="labid"/>
                            <input type="hidden" name="instalasiid" id="instalasiid"/>
                            <div class="form-group">
                                <label>Laboratorium</label>
                                <input type="text" name="lab_nama" id="lab_nama" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Instalasi</label>
                                <input type="text" name="instalasi_nama" id="instalasi_nama" class="form-control" readonly required />
                                <!--
                                <select name="instalasi_id" id="instalasi_id" class="form-control select2" required></select>
                                -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">PROSES</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $('#data_table').DataTable();

    $("button.btn-mdl").click(function(e){
        e.preventDefault();
        var id = $(this).data().id;
        var requestid = $(this).data().requestid;
        var labid = $(this).data().labid;
        var instalasiid = $(this).data().instalasiid;

        $("#id").val(id);
        $("#requestid").val(requestid);
        $("#labid").val(labid);
        $("#instalasiid").val(instalasiid);

        var route = "{{ route('laboratory.getbyid', ':id') }}";
        route = route.replace(':id', labid);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#lab_nama").val(res.lab.nama_lab);
                $('#instalasi_id').empty();

                $.each(res.instalasi, function(i, iteminstalasi) {
                    if (iteminstalasi.id == instalasiid) {
                        $('#instalasi_nama').val(iteminstalasi.nama_instalasi);
                    }
                    /*
                    $('#instalasi_id').append(`<option value="${iteminstalasi.id}">
                                       ${iteminstalasi.nama_instalasi}
                                  </option>`);
                    */
                });
            });
        },"json");
    });

    $("#simpan").click(function(e) {
        e.preventDefault();

        var id = $('#id').val();
        var requestid = $('#requestid').val();
        var instalasiid = $('#instalasiid').val();
        
        var route = "{{ route('orderluaruttp.proses', ':id') }}";
        route = route.replace(':id', id);

        $("#prosesmodal").modal('hide');
        
        window.location = route;
        
    });
});

</script>
@endsection