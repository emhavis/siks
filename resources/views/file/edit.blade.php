@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled col-12" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading" >
            <h4>Artikel/Posting</h4>
        </div>
        <div class="panel-body">
            <form id="createGroup" action="{{ route('file.update', $row->id )}}" method="POST" enctype="multipart/form-data">
            
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="kelompok">Judul</label> 
                            {!!
                                Form::text("title", $row->title, [
                                'class' => 'form-control',
                                'id' => 'title',
                                'placeholder' => 'Judul',
                                'required'
                                ]);
                            !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Ketegori</label> 
                            {!!
                                Form::select("type_id", $types, $row->type_id, [
                                'class' => 'form-control',
                                'id' => 'type_id',
                                'required'
                                ]);
                            !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="file_name">File</label> 
                            {!! Form::file('file_name', null,
                                ['class' => 'form-control','id' => 'file_name']) !!}    
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="kelompok">Dipublikasikan?</label> 
                            {!!
                                Form::checkbox("published", 'published' ,$row->published);
                            !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="published">Konten Unggulan?</label> 
                            {!!
                                Form::checkbox("featured", 'featured', $row->featured);
                            !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="priority_index">Indeks Prioritas</label> 
                            {!!
                                Form::number("priority_index", $row->priority_index, [
                                'class' => 'form-control',
                                'id' => 'title',
                                'placeholder' => 'Indeks Prioritas',
                                ]);
                            !!}
                        </div>
                    </div>
                </div>

                
                <button type="submit" id="submit" class="btn btn-default">Submit</button>
            </form> 
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}"></script>
<script>
$(document).ready(function ()
{
    CKEDITOR.replace('content');
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection