@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        @if($serviceOrder->kalab_notes != null)
        <div class="alert alert-info" role="alert">
            {{ $serviceOrder->kalab_notes }}
        </div>
        @endif
        @if($serviceOrder->subkoordinator_notes != null)
        <div class="alert alert-warning" role="alert">
            {{ $serviceOrder->subkoordinator_notes }}
        </div>
        @endif
        @if($serviceOrder->kabalai_notes != null)
        <div class="alert alert-danger" role="alert">
            {{ $serviceOrder->kabalai_notes }}
        </div>
        @endif
        @if($serviceOrder->cancel_at != null)
        <div class="alert alert-danger" role="alert">
            Batal Uji
        </div>
        @endif
        @if($serviceOrder->stat_sertifikat==null || $serviceOrder->stat_sertifikat==0 || $serviceOrder->stat_sertifikat==1)
        {!! Form::open(['url' => route('approveuut.approvesubmitsubko', $serviceOrder->id), 'files' => true])  !!}
        <!-- if($serviceOrder->stat_sertifikat==0 || $serviceOrder->stat_sertifikat==null)
        {!! Form::open(['url' => route('approveuut.approvesubmitsubko', $serviceOrder->id), 'files' => true])  !!} -->
        @elseif($serviceOrder->stat_sertifikat==2)
        {!! Form::open(['url' => route('approveuut.approvesubmit', $serviceOrder->id), 'files' => true])  !!}
        @endif

        @if($serviceOrder->ServiceRequest->lokasi_pengujian == 'luar')
        <h4>Laporan Pelaksanaan Tugas</h4>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="ringkasan">Ringkasan Pelaksanaan Pengujian</label>
                    <textarea name="ringkasan" id="ringkasan" disabled
                        class="form-control">{!! $laporan != null ? $laporan->ringkasan : '' !!}</textarea>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kendala_teknis">Kendala Teknis</label>
                    <textarea name="kendala_teknis" id="kendala_teknis" disabled
                        class="form-control">{!! $laporan != null ? $laporan->kendala_teknis : '' !!}</textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kendala_non_teknis">Kendala Non Teknis</label>
                    <textarea name="kendala_non_teknis" id="kendala_non_teknis" disabled
                            class="form-control">{!! $laporan != null ? $laporan->kendala_non_teknis : '' !!}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="metode_tindakan">Metode/Tindakan yang Dilakukan</label>
                    <textarea name="metode_tindakan" id="metode_tindakan" disabled
                        class="form-control">{!! $laporan != null ? $laporan->metode_tindakan : '' !!}</textarea>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="saran_masukan">Saran/Masukan</label>
                    <textarea name="saran_masukan" id="saran_masukan" disabled
                        class="form-control">{!! $laporan != null ? $laporan->saran_masukan : '' !!}</textarea>
                </div>  
            </div>
        </div>
        <hr/>
        @endif

        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_uut">Jenis UUT</label>
                    {!! Form::text('jenis_uut', $serviceOrder->ServiceRequestItem->uuts->stdtype->uut_type, ['class' => 'form-control','id' => 'jenis_uut', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas</label>
                    {!! Form::text('kapasitas', $serviceOrder->ServiceRequestItem->uuts->tool_capacity, ['class' => 'form-control','id' => 'kapasitas', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Merek/Model/Tipe</label>
                    {!! Form::text('merek', 
                        $serviceOrder->ServiceRequestItem->uuts->tool_brand . '/' . 
                        $serviceOrder->ServiceRequestItem->uuts->tool_model . '/' .
                        $serviceOrder->ServiceRequestItem->uuts->tool_type,
                        ['class' => 'form-control','id' => 'merek', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->ServiceRequestItem->uuts->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan', 'readonly']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>

        <!--
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory">Nama Pabrikan</label>
                    {!! Form::text('factory', 
                        $serviceOrder->ServiceRequestItem->uuts->tool_factory,
                        ['class' => 'form-control','id' => 'factory', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory_addr">Alamat Pabrikan</label>
                    {!! Form::text('factory_addr', 
                        $serviceOrder->ServiceRequestItem->uuts->tool_factory_address,
                        ['class' => 'form-control','id' => 'factory_addr', 'readonly']) !!}
                </div>
            </div>
        </div>
        -->

        <!--
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Dasar Pengujian, Nomor</label>
                    {!! Form::text('reference_no', 
                        $serviceOrder->ServiceRequestItem->reference_no,
                        ['class' => 'form-control','id' => 'reference_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_date">Dasar Pengujian, Tanggal</label>
                    {!! Form::text('reference_date', 
                        date("d-m-Y", strtotime(isset($serviceOrder->ServiceRequestItem->reference_date) ? $serviceOrder->ServiceRequestItem->reference_date : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'reference_date', 'readonly']) !!}
                </div>
            </div>
        </div>
        -->

        @if($serviceOrder->cancel_at == null)
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_1">Diuji Oleh (1)</label>
                    {!! Form::text('test_by_1', 
                        $serviceOrder->test1 != null ? $serviceOrder->test1->full_name : '',
                        ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_2">Diuji Oleh (2)</label>
                    {!! Form::text('test_by_1', 
                        $serviceOrder->test2 != null ? $serviceOrder->test2->full_name : '',
                        ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_datein">Waktu Pengujian, Mulai</label>
                    {!! Form::text('staff_entry_datein', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_datein) ? $serviceOrder->staff_entry_datein : date("Y-m-d"))),
                        ['class' => 'date form-control','id' => 'staff_entry_datein', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_dateout">Waktu Pengujian, Selesai</label>
                    {!! Form::text('staff_entry_dateout', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_dateout) ? $serviceOrder->staff_entry_dateout : date("Y-m-d"))),
                        ['class' => 'date form-control','id' => 'staff_entry_dateout', 'readonly']) !!}
                </div>
            </div>
        </div>
        

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="persyaratan_teknis">Persyaratan Teknis</label>
                    {!! Form::text('persyaratan_teknis', 
                        $serviceOrder->persyaratanTeknis != null ? $serviceOrder->persyaratanTeknis->oiml_name : '',
                        ['class' => 'form-control','id' => 'persyaratan_teknis', 'readonly']) !!}
                </div>
            </div>
        </div>
        

        <!-- <table id="data_table" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Pemeriksaan/Pengujian</th>
                    <th>Pemenuhan Persyaratan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($serviceOrder->inspections as $item)
                <tr>
                    <td>{{ $item->inspectionItem->name }}</td>
                    <td>
                    @if($item->inspectionItem->is_tested)
                    {!! Form::text('is_accepted_'.$item->id, 
                        $item->is_accepted != null ? ($item->is_accepted ? 'Ya' : 'Tidak') : 'N/A',
                        ['class' => 'form-control','id' => 'is_accepted_'.$item->id, 'readonly']) !!}
                    @endif
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table> -->

        @else
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="cancel_at">Batal Uji pada Tanggal</label>
                    {!! Form::text('cancel_at', 
                        date("d-m-Y", strtotime(isset($serviceOrder->cancel_at) ? $serviceOrder->cancel_at : date("Y-m-d"))),
                        ['class' => 'date form-control','id' => 'cancel_at', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="cancel_notes">Alasan Pembatalan</label>
                    {!! Form::text('cancel_notes', 
                       $serviceOrder->cancel_notes,
                        ['class' => 'date form-control','id' => 'cancel_notes', 'readonly']) !!}
                </div>
            </div>
        </div>

        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                <label for="file_lampiran_persetujuan">Lampiran Perbaikan</label>
                {!! Form::file('file_lampiran_kalab', null,
                        ['class' => 'form-control','id' => 'file_lampiran_kalab']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                <label for="is_approved">Persetujuan</label>
                    {!! Form::select('is_approved', ['ya'=>'Ya, Setuju','tidak'=>'Perbaiki'], null, 
                        ['class' => 'form-control select2','id' => 'is_approved', 'style' => 'width:100%']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                <label for="notes">Catatan Perbaikan</label>
                    {!! Form::textarea('notes', null,
                        ['class' => 'form-control','id' => 'notes', 'readonly']) !!}       
                </div>
            </div>
        </div>
        <button role="submit" class="btn btn-w-md btn-success" id="btn_simpan">Setujui Hasil Uji</button>
        {!! Form::close() !!}
        

       
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">

    $(document).ready(function ()
    {
        $('#is_approved').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        }).on('select2:select', function (e) {
            var data = e.params.data;
            if (data.id == 'tidak') {
                $('#btn_simpan').text('Perbaiki Hasil Uji');
                $('#btn_simpan').removeClass('btn-success');
                $('#btn_simpan').addClass('btn-danger');

                $("#notes").prop("readonly", false); 
            } else {
                $('#btn_simpan').text('Setujui Hasil Uji');
                $('#btn_simpan').removeClass('btn-danger');
                $('#btn_simpan').addClass('btn-success');

                $("#notes").val("");
                $("#notes").prop("readonly", true); 
            }
        });
    }); 
</script>
@endsection