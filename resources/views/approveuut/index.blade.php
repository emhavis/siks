@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<style type"text/css"> 
    .badge-notif-tab {
            position:relative;
    }
    .badge-notif-tab[data-badge]:after {
            content:attr(data-badge);
            position:absolute;
            top:-10px;
            right:-5px;
            font-size:.7em;
            background:#f3a709;
            color:white;
            width:18px;
            height:18px;
            text-align:center;
            line-height:18px;
            border-radius: 50%;
    }
</style>
@endsection

@section('content') 
<div class="row">

    <ul class="nav nav-tabs" role="tablist" id="tabs">
        <li role="presentation" class="active">
            <a href="#kn" aria-controls="kn" role="tab" data-toggle="tab" 
                class="badge-notif-tab" 
                <?php if ((count($rows_kn)) > 0 ){ ?> 
                    data-badge="{{count($rows_kn)}}" 
                <?php }else{ } ?>
            >Dalam Kantor</a>
        </li>
        <li role="presentation">
            <a href="#dl" aria-controls="dl" role="tab" data-toggle="tab" 
                class="badge-notif-tab" 
                <?php if ((count($rows_dl)) > 0 ){ ?> 
                    data-badge="{{count($rows_dl)}}" 
                <?php }else{ } ?>
            >Dinas Luar</a>
        </li>
    </ul>
    
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="kn">
            <br/>    

            <div class="panel panel-filled table-area">
                <div class="panel-body">

                    <button class="btn btn-w-md btn-success" id="btn_simpan" disabled >Setujui Terpilih</button>
                    
                    <div class="row"><div class="col">&nbsp;</div></div>
                    
                    <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th data-orderable="false">{!! Form::checkbox('select_all', 'select_all', false, ['class' => 'form-check-input', 'id' => 'select_all' ]) !!}</th>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Tgl Order</th>
                                <th>Target Selesai</th>
                                <th>Catatan Revisi</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_kn as $row)
                            <tr id="{{ $row->id }}">
                                <td>{!! Form::checkbox('row_selected', 'row_selected', false, ['class' => 'form-control', ]) !!}</td>
                                <td>{{ $row->ServiceRequestItem->no_order }}</td>
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : '' }}</td>
                                <td>{{ $row->test1 ? $row->test1->full_name : "" }} {{ $row->test2 ? ' & '.$row->test2->full_name : "" }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->staff_entry_datein)) }}</td>
                                <td>{{ $row->staff_entry_dateout != null ? date("d-m-Y", strtotime($row->staff_entry_dateout)) : '' }}</td>
                                <td>{{ $row->order_at != null ? date("d-m-Y", strtotime($row->order_at)) : '' }}</td>
                                <td>{{ $row->ServiceRequestItem->order_at != null ? date('d-m-Y',strtotime($row->date_line.'+1 day')) :''
                                    }}
                                </td>
                                <td>
                                    @if($row->subkoordinator_notes != null)
                                    <div class="alert alert-warning" role="alert">
                                        Catatan Subkoordinator: {{ $row->subkoordinator_notes }}
                                    </div>
                                    @endif
                                    @if($row->kabalai_notes != null)
                                    <div class="alert alert-danger" role="alert">
                                        Catatan Kabalai: {{ $row->kabalai_notes }}
                                    </div>
                                    @endif
                                </td>
                                <td>
                                    @if($row->stat_sertifikat==null || $row->stat_sertifikat == 1 )
                                    <a href="{{ route('approveuut.approvesubko', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                    <!-- elseif($row->stat_sertifikat==1)
                                    <a href="{{ route('approveuut.approvesubko', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN 2</a> -->
                                    @elseif($row->stat_sertifikat==2 ||$row->stat_sertifikat==2)
                                    <a href="{{ route('approveuut.approve', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                    @endif
                                    <!-- if($approval_before_delegasi != null && $row->stat_sertifikat == $approval_before_delegasi && $delegator != null)
                                    Delegasi dari  $delegator != null ? $delegator : '' 
                                    endif -->
                                </td>
                                <td>
                                @if($row->stat_service_order=="2")
                                    @if($row->file_skhp!==null)
                                    <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                        @if($row->kabalai_id !=null || $row->kabalai_date !=null)
                                        <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                        <!-- else
                                        <a href="{{ route('approveuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a> -->
                                        @endif 
                                    <!-- <a href="{{ route('historyorderuut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD REV. KALAB</a> -->
                                    @if($row->ujitipe_completed==true)
                                    <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT TIPE</a>
                                    <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT TIPE</a>
                                    @endif
                                    <a href="{{ route('file.show', [$row->id,'cerapan']) }}" class="btn btn-warning btn-sm">LIHAT CERAPAN</a>
                                        @if($row->file_lampiran!==null)
                                        <a href="{{ route('file.show', [$row->id,'lampiran']) }}" class="btn btn-warning btn-sm">LIHAT LAMPIRAN</a>
                                        @endif
                                    @endif
                                @elseif($row->stat_service_order=="3")
                                    <a href="{{ route('approveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                        <!-- <a href="{{ route('approveuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a> -->
                                        @if($row->file_lampiran_kalab!==null)
                                            <a href="{{ route('serviceuut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN KALAB</a>
                                        @endif
                                        @if($row->file_review_subkoordinator!==null)
                                            <a href="{{ route('serviceuut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN REV.SUBKO</a>
                                        @endif
                                @elseif($row->stat_service_order=="4")
                                    <a href="{{ route('approveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">SURAT PEMBATALAN</a>
                                    @if($row->kabalai_date)
                                        <a href="{{ route('approveuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                    @endif
                                    @if($row->cancelation_file !=null)
                                        <a href="{{ route('serviceuut.cancelation_download', $row->id) }}" class="btn btn-warning btn-sm">LAMPIRAN PEBATALAN</a>
                                    @endif
                                @elseif($row->is_finish=="0")
                                    @if($row->file_skhp!==null)
                                        <a href="{{ route('approveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                        @if($row->kabalai_date)
                                            <a href="{{ route('approveuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                        @endif
                                        @if($row->ujitipe_completed==true)
                                            <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT TIPE</a>
                                            <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT TIPE</a>
                                        @endif
                                        <!-- if($row->cancelation_file !=null)
                                            <a href="{{ route('serviceuut.download', $row->id) }}" class="btn btn-warning btn-sm">LAMPIRAN PEMBATALAN</a>
                                        endif -->
                                        <a href="{{ route('file.show', [$row->id,'cerapan']) }}" class="btn btn-warning btn-sm">LIHAT CERAPAN</a>
                                        @if($row->file_lampiran!==null)
                                        <a href="{{ route('file.show',[ $row->id,'lampiran']) }}" class="btn btn-warning btn-sm">LIHAT LAMPIRAN</a>
                                        @endif
                                        @if($row->file_lampiran_kalab !==null)
                                        <a href="" class="btn btn-warning btn-sm">DOWNLOAD PERSETUJUAN KALAB</a>
                                        @endif
                                    @endif
                                @elseif($row->is_finish=="1")
                                SELESAI
                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="dl">
            <br/> 

            <div class="panel panel-filled table-area">
                <div class="panel-body">

                    <button class="btn btn-w-md btn-success" id="btn_simpan" disabled >Setujui Terpilih</button>
                    
                    <div class="row"><div class="col">&nbsp;</div></div>
                    
                    <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th data-orderable="false">{!! Form::checkbox('select_all', 'select_all', false, ['class' => 'form-check-input', 'id' => 'select_all' ]) !!}</th>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Tgl Order</th>
                                <th>Target Selesai</th>
                                <th>Catatan Revisi</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_dl as $row)
                            <tr id="{{ $row->id }}">
                                <td>{!! Form::checkbox('row_selected', 'row_selected', false, ['class' => 'form-control', ]) !!}</td>
                                <td>{{ $row->ServiceRequestItem->no_order }}</td>
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : '' }}</td>
                                <td>{{ $row->test1 ? $row->test1->full_name : "" }} {{ $row->test2 ? ' & '.$row->test2->full_name : "" }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->staff_entry_datein)) }}</td>
                                <td>{{ $row->staff_entry_dateout != null ? date("d-m-Y", strtotime($row->staff_entry_dateout)) : '' }}</td>
                                <td>{{ $row->order_at != null ? date("d-m-Y", strtotime($row->order_at)) : '' }}</td>
                                <td>{{ $row->ServiceRequestItem->order_at != null ? date('d-m-Y',strtotime($row->date_line.'+1 day')) :''
                                    }}
                                </td>
                                <td>
                                    @if($row->subkoordinator_notes != null)
                                    <div class="alert alert-warning" role="alert">
                                        Catatan Subkoordinator: {{ $row->subkoordinator_notes }}
                                    </div>
                                    @endif
                                    @if($row->kabalai_notes != null)
                                    <div class="alert alert-danger" role="alert">
                                        Catatan Kabalai: {{ $row->kabalai_notes }}
                                    </div>
                                    @endif
                                </td>
                                <td>
                                    @if($row->stat_sertifikat==null || $row->stat_sertifikat == 1 )
                                        <a href="{{ route('approveuut.approvesubko', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                    @elseif($row->stat_sertifikat==2 ||$row->stat_sertifikat==2)
                                        <a href="{{ route('approveuut.approve', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                    @endif
                                </td>
                                <td>
                                    @if($row->ServiceRequest->spuh_doc_id != null)
                                        <a href="{{ route('schedulinguut.surattugas', ['id' => $row->ServiceRequest->id, 'stream' => 1]) }}" 
                                            class="btn btn-info btn-sm">Surat Tugas</a>
                                    @endif
                                    <!-- <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">Draft Sertifikat</a> -->
                                    @if($row->status_revisi_spt == 3)
                                        <a href="{{ route('serviceprocessluaruut.check', $row->id) }}" class="btn btn-warning btn-sm">Order</a>
                                    @endif
                                    
                                    @if($row->stat_service_order=="2")
                                        @if($row->file_skhp!==null)
                                            <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                            @if($row->kabalai_id !=null || $row->kabalai_date !=null)
                                                <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                            @endif 
                                            @if($row->ujitipe_completed==true)
                                                <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT TIPE</a>
                                                <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT TIPE</a>
                                            @endif
                                            <a href="{{ route('file.show', [$row->id,'cerapan']) }}" class="btn btn-warning btn-sm">LIHAT CERAPAN</a>
                                            @if($row->file_lampiran!==null)
                                                <a href="{{ route('file.show', [$row->id,'lampiran']) }}" class="btn btn-warning btn-sm">LIHAT LAMPIRAN</a>
                                            @endif
                                        @endif
                                    @elseif($row->stat_service_order=="3")
                                        <a href="{{ route('approveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                            <!-- <a href="{{ route('approveuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a> -->
                                            @if($row->file_lampiran_kalab!==null)
                                                <a href="{{ route('serviceuut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN KALAB</a>
                                            @endif
                                            @if($row->file_review_subkoordinator!==null)
                                                <a href="{{ route('serviceuut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN REV.SUBKO</a>
                                            @endif
                                    @elseif($row->stat_service_order=="4")
                                        <a href="{{ route('approveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">SURAT PEMBATALAN</a>
                                        @if($row->kabalai_date)
                                            <a href="{{ route('approveuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                        @endif
                                        @if($row->cancelation_file !=null)
                                            <a href="{{ route('serviceuut.cancelation_download', $row->id) }}" class="btn btn-warning btn-sm">LAMPIRAN PEBATALAN</a>
                                        @endif
                                    @elseif($row->is_finish=="0")
                                        @if($row->file_skhp!==null)
                                        <a href="{{ route('approveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                        @if($row->kabalai_date)
                                            <a href="{{ route('approveuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                        @endif
                                        @if($row->ujitipe_completed==true)
                                        <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT TIPE</a>
                                        <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT TIPE</a>
                                        @endif
                                            <!-- if($row->cancelation_file !=null)
                                                <a href="{{ route('serviceuut.download', $row->id) }}" class="btn btn-warning btn-sm">LAMPIRAN PEMBATALAN</a>
                                            endif -->
                                            <a href="{{ route('file.show', [$row->id,'cerapan']) }}" class="btn btn-warning btn-sm">LIHAT CERAPAN</a>
                                            @if($row->file_lampiran!==null)
                                            <a href="{{ route('file.show',[ $row->id,'lampiran']) }}" class="btn btn-warning btn-sm">LIHAT LAMPIRAN</a>
                                            @endif
                                            @if($row->file_lampiran_kalab !==null)
                                            <a href="" class="btn btn-warning btn-sm">DOWNLOAD PERSETUJUAN KALAB</a>
                                            @endif
                                        @endif
                                    @elseif($row->is_finish=="1")
                                    SELESAI
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
            
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-body">
                <h4 class="m-t-none">Konfirmasi Persetujuan</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <p>Apakah benar akan menyetujui semua data yang dipilih?</p>
            </div>
            <div class="modal-footer">
            {!! Form::open(['url' => route('approveuut.approvesubmitall'), 'files' => true])  !!}
                <input type="hidden" name="ids" id="ids" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" id="simpan" class="btn btn-accent">SETUJU</button>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    let table = $('#data_table').DataTable({
        "order": [[ 1, "asc" ]],
        columnDefs: [ {
            targets: 0,
            orderable: false,
            searchable: false,
        } ],
        select: {
            style: 'multi',
            selector: 'td:first-child',
        },
        scrollX: true,
    });

    $('#data_table tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');

        //console.log(table.rows('.selected').data());
        //console.log(table.rows('.selected').data().length +' row(s) selected');
        var cb = $(this).find("> td:first-child > input:checkbox");
        
        //console.log($(this).hasClass('selected'));
        cb.prop('checked', $(this).hasClass('selected'));

        var rows = table.rows('.selected').ids();
        $('#ids').val(rows.join(","));
        $('#btn_simpan').prop("disabled", false);
    } );

    $("#btn_simpan").click(function(e){
        e.preventDefault();
        $("#prosesmodal").modal('show');
    });

    $("#select_all").change(function() {
        //console.log(table.rows());
        //console.log(table.rows('.selected').data().length +' row(s) selected');

        if ($(this).is( ":checked" )) {
            table.rows().nodes().to$().addClass('selected');     
            table.rows().nodes().to$().find("> td:first-child > input:checkbox").prop('checked', true);
        } else {
            table.rows().nodes().to$().removeClass('selected');
            table.rows().nodes().to$().find("> td:first-child > input:checkbox").prop('checked', false);
        }

        var rows = table.rows('.selected').ids();
        $('#ids').val(rows.join(","));
        $('#btn_simpan').prop("disabled", false);
    });
});

</script>
@endsection