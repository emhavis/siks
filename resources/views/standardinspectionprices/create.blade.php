@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row col-md-6">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="role_name">Tipe Standar</label> 
                        {!!
                            Form::select("standard_detail_type_id",
                            $standarddetails,
                            $row?$row->standard_detail_type_id:'',[
                            'class' => 'form-control',
                            'id' => 'standard_detail_type_id',
                            'placeholder' => 'Standard Types ',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="role_name">Inspection Type</label> 
                        {!!
                            Form::text("inspection_type",$row?$row->inspection_type:'',[
                            'class' => 'form-control',
                            'id' => 'inspection_type',
                            'placeholder' => 'Inspection Type',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="role_name">Harga</label> 
                        {!!
                            Form::text("no",$row?$row->no:'',[
                            'class' => 'form-control',
                            'id' => 'no',
                            'placeholder' => 'Price',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="template_id">Valid Sampai</label> 
                        {!!
                            Form::text("valid_period",
                            $row?$row->valid_period:'',[
                            'class' => 'form-control',
                            'id' => 'valid_period',
                            'placeholder' => 'Valid Periode ',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="role_name">Tipe Unit Ukuran</label> 
                        {!!
                            Form::select("standard_measurement_unit_id",
                            $measurementtype,
                            $row?$row->standard_measurement_unit_id:'',[
                            'class' => 'form-control',
                            'id' => 'standard_measurement_unit_id',
                            'placeholder' => 'Stnadard Measurement Type ',
                            'required'
                            ]);
                        !!}
                    </div>
                    
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function ()
{
    $('#valid_period').datepicker({
        format: "yyyy/mm/dd",
        autoclose: true
    });
    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('stinsprice.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('stinsprice') }}';
            }
        });

    });
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection