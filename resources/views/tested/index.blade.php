@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>QR Code</th>
                        <th>Diterima Oleh</th>
                        <th>Diuji Oleh</th>
                        <th>Disetujui Oleh</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th>Tgl Disetujui</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequest->no_order }}</td>
                        <td>
                            <a onclick="showToolCodeInfo('{{ $row->ServiceRequestItem->Standard->tool_code }}')" href="#">{{ $row->ServiceRequestItem->Standard->tool_code }}</a>
                        </td> 
                        <td>{{ $row->MasterUsers->full_name }}</td>
                        <td>{{ $row->LabStaffOut?$row->LabStaffOut->full_name:"" }}</td>
                        <td>{{ $row->SupervisorStaff?$row->SupervisorStaff->full_name:"" }}</td>
                        <td>{{ $row->staff_entry_datein }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>{{ $row->supervisor_entry_date }}</td>
                        <td>
                        <a target="_blank" href="{{ route('tested.sertifikat', $row->id) }}" class="btn btn-info btn-sm mb-2" target="_blank">PREVIEW</a>
                        @if($row->stat_service_order=="1")
                        <a href="{{ route('tested.proses', $row->id) }}" class="btn btn-warning btn-sm">APPROVE</a>
                        <a href="{{ route('tested.cancel', $row->id) }}" class="btn btn-warning btn-sm">BATAL</a>
                        @endif
                        @if($row->stat_service_order=="2")
                        APPROVED
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('#data_table').DataTable();
});

</script>
@endsection