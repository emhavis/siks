@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>
        <div class="panel-heading">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <form id="createUser">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="full_name">Nama Lengkap</label>
                                {!!
                                    Form::text("full_name",$row->full_name,[
                                    'class' => 'form-control',
                                    'id' => 'full_name',
                                    'placeholder' => 'Nama Lengkap',
                                    'required',
                                    'readonly',
                                    ]);
                                !!}
                            </div>
                            <div class="form-group">
                                <label for="username">Nama Akun</label>
                                {!!
                                    Form::text("username",$row->username,[
                                    'class' => 'form-control',
                                    'id' => 'username',
                                    'placeholder' => 'Nama Akun',
                                    'required',
                                    'readonly',
                                    ]);
                                !!}
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                {!!
                                    Form::email("email",$row->email,[
                                    'class' => 'form-control',
                                    'id' => 'email',
                                    'placeholder' => 'Email',
                                    'required',
                                    'readonly',
                                    ]);
                                !!}
                            </div>
                            <div class="form-group">
                                <label for="laboratory_id">Laboratorium</label>
                                {!!
                                    Form::text("laboratory_id",$row->MasterLaboratory ? $row->MasterLaboratory->nama_lab : '',[
                                    'class' => 'form-control',
                                    'id' => 'laboratory_id',
                                    'required',
                                    'readonly',
                                    ]);
                                !!}
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <!--
                        <button type="button" id="submit" class="btn btn-warning">Simpan</button>
                        -->
                        <button type="button" id="changePassword" class="btn btn-warning">Ubah Password</button>
                    </div>
                </div>                        
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Ubah Password</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <div class="form-group">
                                <label>Password Lama</label>
                                <input type="password" name="password" id="password" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label>Password Baru</label>
                                <input type="password" name="newpassword" id="newpassword" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label>Konfirmasi Password Baru</label>
                                <input type="password" name="newpassword_confirmation" id="newpassword_confirmation" class="form-control" required />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">UBAH PASSWORD</button>
            </div>
            
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script>
$(document).ready(function ()
{
    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createUser").serialize();

        $(document).find("small.text-warning").remove();
        $("#panel_create").toggleClass("ld-loading");
  
        $.post('{{ route('userprofil.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
        });

    });

    $("#changePassword").click(function(e)
    {
        $("#prosesmodal").modal('show');
    });

    $("#form_modal").validate({
        rules: {
            password: "required",
            newpassword: "required",
            newpassword_confirmation: {
                equalTo: "#newpassword",
            },
        },
        messages: {
            password: 'Password lama harus diisi',
            newpassword: 'Password baru harus diisi',
            newpassword_confirmation: "Konfirmasi password baru harus sama"
        },
        errorClass: "help-block error",
        highlight: function(e) {
            $(e).closest(".form-group.row").addClass("has-error")
        },
        unhighlight: function(e) {
            $(e).closest(".form-group.row").removeClass("has-error")
        },
    });

    $("#simpan").click(function(e){
        e.preventDefault();

        var form = $("#form_modal");
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        if (form.valid()) {
            toastr["warning"]("Sedang menyimpan. Harap tunggu.");
            
            $.post('{{ route('userprofil.storePassword') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    location.reload();
                } else 
                {
                    toastr["error"](response.messages, "Form Invalid");
                }
            });
        }
    });
});

</script>

@endsection
