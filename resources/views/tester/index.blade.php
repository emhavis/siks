<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">

<!-- Vendor styles -->
<link rel="stylesheet" href="{{ asset('assets/vendor/fontawesome/css/font-awesome.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/animate.css/animate.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/toastr/toastr.min.css') }}"/>

<!-- App styles -->
<link rel="stylesheet" href="{{ asset('assets/styles/pe-icons/pe-icon-7-stroke.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/styles/pe-icons/helper.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/styles/stroke-icons/style.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/styles/style.css') }}">

<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
            
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Tester</h4>
                    </div>
                    <div class="panel-body">

                        {!! Form::open(['url' => route('tester.save'), 'files' => true, 'id' => 'form_result'])  !!}
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="data_tester">Data</label>
                                    {!! Form::text('data_tester', '',
                                        ['class' => 'form-control','id' => 'data_tester']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="file_tester">Upload</label>
                                    {!! Form::file('file_tester', 
                                        ['class' => 'form-control','id' => 'file_tester', 'accept'=> 'application/pdf,image/png, image/jpeg']) !!}
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button>  

                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/vendor/pacejs/pace.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sparkline/index.js') }}"></script>
<script src="{{ asset('assets/vendor/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('assets/vendor/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('assets/vendor/flot/jquery.flot.spline.js') }}"></script>

<!-- App scripts -->
<script src="{{ asset('assets/scripts/luna.js') }}"></script>

<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{ asset('assets/vendor/functions.js') }}"></script>

<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/jquery-validation/additional-methods.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#btn_simpan").click(function(e){
            e.preventDefault();

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });
    });
</script>