@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">

@endsection

@section('content')
<!--
<dic class="row">
    <div class="col-6">
            <div class="input-group rounded">
                <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                aria-describedby="search-addon" />
                <span class="input-group-text border-0" id="search-addon">
                    <i class="fas fa-search"></i>
                </span>
            </div>
        </div>
</div>
-->
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <form id="createGroup" action="{{ route('resumeuttp')}}" method="GET" >
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <!-- <label for="start_date">Periode</label>  -->
                            <div class="input-group input-daterange" id="dt_range">
                                <div class="input-group-addon"> Dari </div>
                                <input type="text" class="form-control" id="start_date" name="start_date"
                                value="{{ $start != null ? date("d-m-Y", strtotime($start)) : '' }}">
                                <div class="input-group-addon">s/d</div>
                                <input type="text" class="form-control" id="end_date" name="end_date"
                                value="{{ $end != null ? date("d-m-Y", strtotime($end)) : '' }}">
                                <!-- // filter instalasi -->
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-2">
                         {!!
                            Form::select("instalasi",
                            $instalasi,
                            $ins?$ins:'',[
                            'class' => 'form-control',
                            'id' => 'service_type_id',
                            'placeholder' => 'Instalasi '
                            ]);
                        !!}
                    </div>
                    <div class="col-md-2">
                        {!! Form::submit('Filter', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_filter']) !!}
                    </div>
                    <div class="col-md-4 text-right">
                        <a class="btn btn-success" href="{{ route('file-export').'?start_date='.$start.'&end_date='.$end.'&instalasi='.$ins}}" >Export data</a>
                    </div>

                </div>
            </form>    
            <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Nama Pemilik</th>
                        <th>Nama Pemesan</th>
                        <th>Jumlah Alat</th>
                        <th>Rincian Alat</th>
                        <th>Obyek Pengujian</th>
                        <th>Tarif PNBP</th>
                        <th>Total PNBP</th>
                        <th>Billing Code</th>
                        <th>NTPN</th>
                        <th>No. Kuitansi</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai</th>
                        <th>Status SLA</th>
                        <th>Instalasi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequest->no_order }}</td>
                        <td>{{ $row->ServiceRequest->label_sertifikat }}</td>
                        <td>{{ $row->ServiceRequest->requestor->full_name }}</td>
                        <td>{{ count($row->ServiceRequest->items) }}</td>
                        <td>{{ $row->ServiceRequestItem->uttp->type->uttp_type }}</td>
                        <td>{{ $row->ServiceRequestItemInspection->inspectionPrice->inspection_type }}</td>
                        <td>{{ number_format($row->ServiceRequestItemInspection->price * $row->ServiceRequestItemInspection->quantity, 2, ',', '.') }}</td>
                        <td>{{ number_format($row->ServiceRequest->total_price, 2, ',', '.') }}</td>
                        <td>{{ $row->ServiceRequest->billing_code }}</td>
                        <td>{{ $row->ServiceRequest->payment_code }}</td>
                        <td>{{ $row->ServiceRequest->no_register }}</td>
                        <td>{{ $row->ServiceRequest->received_date }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>{{ $row->sla_status == 'ONP' ? 'Dalam Proses' : ( $row->sla_status == 'ONT' ? 'Baik' : 'Terlambat' ) }}</td>
                        <td>{{ $row->instalasi->nama_instalasi }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>  
    
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function ()
    {
        $('#instalasi').select2({
        });
        $("#btn1").on('click',function(e) {
            // console.log('console');
            var _token = $('#_token').val();
            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $('#createGroup');
            // form.append("_token", _token);
            $.ajax({
                type: "GET",
                url: "{{route('file-export')}}",
                data: form.serialize(), // serializes the form's elements.
                success: function(data)
                {
                    console.log(data); // show response from the php script.
                }
            });

        });
        // $("#table_data").DataTable({
        //     scrollX: true,
        // });
        
        $('.input-daterange input').each(function() {
            $(this).datepicker({
                format:"dd-mm-yyyy",
            });
        });

        // data table will generated
        var table = $('#table_data').DataTable({
            scrollX : true,
        });
    });
</script>
@endsection