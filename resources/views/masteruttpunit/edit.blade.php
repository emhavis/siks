@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row col-md-6">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
            <div class="panel-body">
                {!! Form::model($row, ['method' => 'PATCH', 'route' => ['uttpunit.update', $row->id],
                    'id' => 'form_update_owner', 'enctype' => "multipart/form-data"]) !!}
                    <div class="form-group">
                        <label for="kelompok">Unit</label> 
                        {!!
                            Form::text("unit",$row?$row->unit:'',[
                            'class' => 'form-control',
                            'id' => 'unit',
                            'placeholder' => 'Uttp Unit',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="kelompok">Jenis Uttp</label> 
                        {!!
                            Form::select("uttp_type_id",$types, $row?$row->uttp_type_id:'',[
                            'class' => 'form-control',
                            'id' => 'uttp_type_id',
                            'placeholder' => 'Uttp Type Name',
                            'required'
                            ]);
                        !!}
                    </div>
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection