@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">

    <div class="panel panel-filled table-area">
        <div class="panel-body">

            <form id="createGroup" action="{{ route('evaluasiorderinsituuttp')}}" method="GET" >
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="start_date">Periode</label>
                            <div class="input-group input-daterange" id="dt_range">
                                <div class="input-group-addon"> Dari </div>
                                <input type="text" class="form-control" id="start_date" name="start_date"
                                value="{{ $start != null ? date("d-m-Y", strtotime($start)) : '' }}">
                                <div class="input-group-addon">s/d</div>
                                <input type="text" class="form-control" id="end_date" name="end_date"
                                value="{{ $end != null ? date("d-m-Y", strtotime($end)) : '' }}">
                                <!-- // filter instalasi -->
                            </div>
                        </div> 
                    </div>
                    
                    
                    
                    <div class="col-md-2">
                        {!! Form::submit('Filter', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_filter']) !!}
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-2 text-right">
                        <button class="btn btn-success" id="btn1">Export Data</button>
                    </div>

                </div>
            </form> 
            
            <table id="data_table_dl" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Layanan</th>
                        <th>No Surat Tugas</th>
                        <th>Nama Pemilik</th>
                        <th>Nama Pemohon</th>
                        <th>Alat</th>
                        <th>Lokasi</th>
                        <th>Penguji/Pemeriksa</th>
                        <th>Ringkasan</th>
                        <th>Kendala Teknis</th>
                        <th>Kendala Non Teknis</th>
                        <th>Metode/Tindakan</th>
                        <th>Saran/Masukan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr id="{{ $row->id }}">
                        <td>{{ $row->no_order }}</td>
                        <td>{{ $row->service_type }}</td>
                        <td>{{ $row->spuh_spt }}</td>
                        <td>{{ $row->label_sertifikat }}</td>
                        <td>{{ $row->requestor }}</td>
                        <td>{{ $row->alat }}</td> 
                        <td>{{ $row->provinsi }}</td>
                        <td>{{ $row->penguji }}</td>
                        <td>{{ $row->ringkasan }}</td>
                        <td>{{ $row->kendala_teknis }}</td>
                        <td>{{ $row->kendala_non_teknis }}</td>
                        <td>{{ $row->metode_tindakan }}</td>
                        <td>{{ $row->saran_masukan }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    let table = $('#data_table_dl').DataTable({
        scrollX: true,
    });

    $("#btn1").on('click',function(e) {
        // console.log('console');
        var _token = $('#_token').val();
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $('#createGroup');
        console.log(form.serialize());
        
        var route = "{{route('evaluasiorderinsituuttp.export')}}?" + form.serialize();
        location.assign(route);

    });

    $('.input-daterange input').each(function() {
        $(this).datepicker({
            format:"dd-mm-yyyy",
        });
    });
});

</script>
@endsection