@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
@endsection

@section('content')
<form id="createGroup" action="{{ route('dashboardtusnsu')}}" method="GET" >
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="start_date">Periode</label> 
                <div class="input-group input-daterange" id="dt_range">
                    <input type="text" class="form-control" id="start_date" name="start_date"
                    value="{{ $start != null ? date("d-m-Y", strtotime($start)) : '' }}">
                    <div class="input-group-addon">s/d</div>
                    <input type="text" class="form-control" id="end_date" name="end_date"
                    value="{{ $end != null ? date("d-m-Y", strtotime($end)) : '' }}">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            {!! Form::submit('Filter', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_filter']) !!}
        </div>
    </div>
</form>
<div class="row">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">Keseluruhan</a></li>
        
        <li role="presentation"><a href="#ttu" aria-controls="ttu" role="tab" data-toggle="tab">Kalibrasi</a></li>
        <li role="presentation"><a href="#tipe" aria-controls="tipe" role="tab" data-toggle="tab">Verifikasi</a></li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="all">
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <canvas id="chartAll"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Instalasi</th>
                                    <th>Jumlah UTTP</th>
                                    <th>Dalam Proses</th>
                                    <th>Selesai Sesuai SLA</th>
                                    <th>Selesai Melebihi SLA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summaries as $row)
                                <tr>
                                    <td>{{ $row->nama_lab }}</td>
                                    <td>{{ $row->jumlah }}</td>
                                    <td>{{ $row->jumlah_proses }}</td>
                                    <td>{{ $row->jumlah_sesuai_sla }}</td>
                                    <td>{{ $row->jumlah_lebih_sla }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="ttu">
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <canvas id="chartTTU"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <table id="data_table_ttu" class="table table-striped table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Instalasi</th>
                                    <th>Jumlah UTTP</th>
                                    <th>UTTP Dalam Proses</th>
                                    <th>UTTP Selesai Sesuai SLA</th>
                                    <th>UTTP Selesai Melebihi SLA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summaries_ttu as $row)
                                <tr>
                                    <td>{{ $row->nama_lab }}</td>
                                    <td>{{ $row->jumlah }}</td>
                                    <td>{{ $row->jumlah_proses }}</td>
                                    <td>{{ $row->jumlah_sesuai_sla }}</td>
                                    <td>{{ $row->jumlah_lebih_sla }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tipe">
        <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <canvas id="chartTipe"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <table id="data_table_tipe" class="table table-striped table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Lab</th>
                                    <th>Jumlah UTTP</th>
                                    <th>UTTP Dalam Proses</th>
                                    <th>UTTP Selesai Sesuai SLA</th>
                                    <th>UTTP Selesai Melebihi SLA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summaries_tipe as $row)
                                <tr>
                                    <td>{{ $row->nama_lab }}</td>
                                    <td>{{ $row->jumlah }}</td>
                                    <td>{{ $row->jumlah_proses }}</td>
                                    <td>{{ $row->jumlah_sesuai_sla }}</td>
                                    <td>{{ $row->jumlah_lebih_sla }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/chart.js/dist/Chart.min.js') }}"></script>
<script>
$(document).ready(function()
{
    $('#data_table, #data_table_ttu, #data_table_tipe').DataTable();
    $('.input-daterange input').each(function() {
        $(this).datepicker({
            format:"dd-mm-yyyy",
        });
    });
    
    var ctx = $("#chartAll");
    const labels = @json($lab);
    const data = @json($summaries);
    const config = {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Jumlah UTTP',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(54, 162, 235)'
                }, 
                {
                    label: 'UTTP dalam Proses',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah_proses',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(255, 159, 64)'
                }, 
                {
                    label: 'UTTP Selesai Sesuai SLA',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah_sesuai_sla',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(75, 192, 192)'
                }, 
                {
                    label: 'UTTP Selesai Melebihi SLA',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah_lebih_sla',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(255, 99, 132)'
                }
            ]
        }
    };
    var chart = new Chart(ctx, config);

    var ctx_ttu = $("#chartTTU");
    const labels_ttu = @json($lab_ttu);
    const data_ttu = @json($summaries_ttu);
    const config_ttu = {
        type: 'bar',
        data: {
            labels: labels_ttu,
            datasets: [
                {
                    label: 'Jumlah UTTP',
                    data: data_ttu,
                    parsing: {
                        yAxisKey: 'jumlah',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(54, 162, 235)'
                }, 
                {
                    label: 'UTTP dalam Proses',
                    data: data_ttu,
                    parsing: {
                        yAxisKey: 'jumlah_proses',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(255, 159, 64)'
                }, 
                {
                    label: 'UTTP Selesai Sesuai SLA',
                    data: data_ttu,
                    parsing: {
                        yAxisKey: 'jumlah_sesuai_sla',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(75, 192, 192)'
                }, 
                {
                    label: 'UTTP Selesai Melebihi SLA',
                    data: data_ttu,
                    parsing: {
                        yAxisKey: 'jumlah_lebih_sla',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(255, 99, 132)'
                }
            ]
        }
    };
    var chart_ttu = new Chart(ctx_ttu, config_ttu);

    var ctx_tipe = $("#chartTipe");
    const labels_tipe = @json($lab_tipe);
    const data_tipe = @json($summaries_tipe);
    const config_tipe = {
        type: 'bar',
        data: {
            labels: labels_tipe,
            datasets: [
                {
                    label: 'Jumlah UTTP',
                    data: data_tipe,
                    parsing: {
                        yAxisKey: 'jumlah',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(54, 162, 235)'
                }, 
                {
                    label: 'UTTP dalam Proses',
                    data: data_tipe,
                    parsing: {
                        yAxisKey: 'jumlah_proses',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(255, 159, 64)'
                }, 
                {
                    label: 'UTTP Selesai Sesuai SLA',
                    data: data_tipe,
                    parsing: {
                        yAxisKey: 'jumlah_sesuai_sla',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(75, 192, 192)'
                }, 
                {
                    label: 'UTTP Selesai Melebihi SLA',
                    data: data_tipe,
                    parsing: {
                        yAxisKey: 'jumlah_lebih_sla',
                        xAxisKey: 'nama_instalasi'
                    },
                    backgroundColor: 'rgb(255, 99, 132)'
                }
            ]
        }
    };
    var chart_tipe = new Chart(ctx_tipe, config_tipe);
});
</script>
@endsection