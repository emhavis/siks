@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">

            <div class="row">
                <div class="col-md-12">
                    <small>Daftar ini akan di-refresh otomatis</small>
                </div>
            </div>
            <br/>   

            <table id="data_table" class="table table-striped table-hover table-responsive-sm data-table">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Instalasi</th>
                        <th>Tgl Selesai Uji</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequestItem->no_order }}</td>
                        <td>{{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                        <td>{{ $row->instalasi->nama_instalasi }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">

$(document).ready(function (){
    $('.data-table').DataTable();

    setInterval(function() {
        window.location = window.location.href;
    }, 15*60*1000);
});

</script>
@endsection