@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                    <div class="form-group">
                        <label for="provinsi_id">Provinsi</label> 
                        {!! Form::select('provinsi_id', $provinsiList,
                            $row?$row->provinsi_id:'', 
                            ['class' => 'form-control', 'id' => 'provinsi_id', 'placeholder' => '- Pilih Provinsi -', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="luar">Uang Harian Luar Kota</label> 
                        {!!
                            Form::number("luar",$row?$row->luar:'',[
                            'class' => 'form-control',
                            'id' => 'luar',
                            'placeholder' => 'Uang Harian Luar Kota',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="dalam">Uang Harian Dalam Kota</label> 
                        {!!
                            Form::number("dalam",$row?$row->dalam:'',[
                            'class' => 'form-control',
                            'id' => 'dalam',
                            'placeholder' => 'Uang Harian Dalam Kota',
                            'required'
                            ]);
                        !!}
                    </div>
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function ()
{
    $('#provinsi_id').select2({
        // placeholder: "- Pilih UML -",
        allowClear: true
    });

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('sbm.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('sbm') }}';
            }
        });

    });
});

</script>
@endsection