@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <a href="{{ route('sbm.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUser" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Provinsi</th>
                        <th>Uang Harian Luar Kota</th>
                        <th>Uang Harian Dalam Kota</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($sbmList->count())
                    @foreach($sbmList as $sbm)
                    <tr>
                        <td>{{ $sbm->provinsi->nama }}</td>
                        <td>{{ $sbm->luar }}</td>
                        <td>{{ $sbm->dalam }}</td>
                        <td>
                            <a href="{{ route('sbm.create', $sbm->id) }}" class="btn btn-info btn-sm">Edit</a>
                            <a href="#" id="action" data-id="{{ $sbm->id }}" data-action="delete" class="btn btn-warning btn-sm">Delete</a>
                            
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function () {
    
    $('#tUser').DataTable();

    $('#tUser').on("click","a#action",function(e)
    //$("a.action").click(function(e)
    {
        e.preventDefault();
        var data = $(this).data();
        data._token = "{{ csrf_token() }}";
        $.post("{{ route('sbm.action') }}",data,function(response)
        {
            if(response.status==true)
            {
                toastr["info"]("Data berhasil dihapus", "Info");
                setTimeout(function()
                {
                    window.location.href = "{{ route("sbm") }}";
                },3000);
            }
            else
            {
                toastr["error"](response["message"], "Error");
            }
        });
    });

});
</script>
@endsection