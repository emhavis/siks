@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">

    <ul class="nav nav-tabs" role="tablist" id="tabs">
        @foreach($instalasiList as $instalasi)
        @if ($loop->first)
        <li role="presentation" class="active"><a href="#order_{{ $instalasi->id }}" aria-controls="order_{{ $instalasi->id }}" role="tab" data-toggle="tab">{{ $instalasi->nama_instalasi }}</a></li>
        @else
        <li role="presentation"><a href="#order_{{ $instalasi->id }}" aria-controls="order_{{ $instalasi->id }}" role="tab" data-toggle="tab">{{ $instalasi->nama_instalasi }}</a></li>
        @endif
        @endforeach
    </ul>

    <div class="tab-content" id="nav-tabContent">
        @foreach($instalasiList as $instalasi)
        <div role="tabpanel" class="tab-pane {{ ($loop->first) ? 'active' : '' }}" id="order_{{ $instalasi->id }}">
            <br/>
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
 
                    <table id="table_data" class="table table-striped table-hover table-responsive-sm data-table">
                        <thead>
                            <tr>
                                <th>No Sertifikat</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemesan</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_processing[$instalasi->id] as $row)
                            <tr>
                                <td>{{ $row->order->no_sertifikat }}</td>
                                <td>{{ $row->request->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->masterstatus->status }}</td>
                                <td>
                                    <a href="{{ route('revisiontechorderuttp.proses', $row->id) }}" class="btn btn-warning btn-sm">Proses Perbaikan</a>

                                    @if (!$row->order->is_skhpt)
                                    <a href="{{ route('revisiontechorderuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('revisiontechorderuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->request->service_type_id == 6 || $row->request->service_type_id == 7)
                                    <a href="{{ route('revisiontechorderuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('revisiontechorderuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->order->ujitipe_completed==true || $row->order->has_set)
                                    <a href="{{ route('revisiontechorderuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('revisiontechorderuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endforeach
    </div>

</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $('.data-table').DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $('.btn-simpan').click(function(e){
            e.preventDefault();
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });
    });
</script>
@endsection