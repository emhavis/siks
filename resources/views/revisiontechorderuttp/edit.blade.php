@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>


<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Informasi Perubahan</a></li>
    <li role="presentation"><a href="#processing" aria-controls="processing" role="tab" data-toggle="tab">Proses Perubahan</a></li>
    
</ul>

<div class="loader">
    <div class="loader-bar"></div>
</div>  

<div class="tab-content" id="nav-tabContent">
    <div role="tabpanel" class="tab-pane active" id="info">
        <div class="panel panel-filled" id="panel_create">
                
            <div class="panel-heading">
                <div class="panel-body">
                
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Perbaikan</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis_layanan">Jenis Layanan</label>
                                    {!! Form::text('jenis_layanan', $revision->request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="no_sertifikat">No Order</label>
                                    {!! Form::text('no_sertifikat', $revision->order->ServiceRequestItem->no_order, ['class' => 'form-control','id' => 'no_order', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="no_sertifikat">No SKHP</label>
                                    {!! Form::text('no_sertifikat', $revision->order->no_sertifikat, ['class' => 'form-control','id' => 'no_sertifikat', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                
                        @if($revision->request->service_type_id == 6 || $revision->request->service_type_id == 7)
                        <div class="row">
                            @if($revision->order->no_sertifikat_tipe != null)
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="no_sertifikat_tipe">No SKHPT</label>
                                    {!! Form::text('no_sertifikat_tipe', $revision->order->no_sertifikat_tipe, ['class' => 'form-control','id' => 'no_sertifikat_tipe', 'readonly']) !!}
                                </div>
                            </div>
                            @endif
                            @if($revision->order->no_surat_tipe != null)
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="no_surat_tipe">No SET</label>
                                    {!! Form::text('no_surat_tipe', $revision->order->no_surat_tipe, ['class' => 'form-control','id' => 'no_surat_tipe', 'readonly']) !!}
                                </div>
                            </div>
                            @endif
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="label_sertifikat">Label Sertifikat</label>
                                    {!! Form::text('label_sertifikat', $revision->request->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="addr_sertifikat">Alamat Sertifikat</label>
                                    {!! Form::text('addr_sertifikat', $revision->request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="uttp_type_id_old">Jenis Alat (Tertulis)</label>
                                    {!! Form::text('uttp_type_id_old', $revision->order->ServiceRequestItem->uttp->type->uttp_type, ['class' => 'form-control','id' => 'uttp_type_id_old', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="uttp_type_id_new">Jenis Alat (Pembetulan)</label>
                                    {!! Form::text('uttp_type_id_new', $revision->uttpType != null ? $revision->uttpType->uttp_type : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'uttp_type_id_new', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_brand_old">Merek (Tertulis)</label>
                                    {!! Form::text('tool_brand_old', $revision->order->ServiceRequestItem->uttp->tool_brand, ['class' => 'form-control','id' => 'tool_brand_old', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_brand_new">Merek (Pembetulan)</label>
                                    {!! Form::text('tool_brand_new', $revision->tool_brand != null ? $revision->tool_brand : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_brand_new', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_model_old">Model (Tertulis)</label>
                                    {!! Form::text('tool_model_old', $revision->order->ServiceRequestItem->uttp->tool_model, ['class' => 'form-control','id' => 'tool_model_old', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_model_new">Model (Pembetulan)</label>
                                    {!! Form::text('tool_model_new', $revision->tool_model != null ? $revision->tool_model : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_model_new', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_type_old">Tipe (Tertulis)</label>
                                    {!! Form::text('tool_type_old', $revision->order->ServiceRequestItem->uttp->tool_type, ['class' => 'form-control','id' => 'tool_type_old', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_type_new">Tipe (Pembetulan)</label>
                                    {!! Form::text('tool_type_new', $revision->tool_type != null ? $revision->tool_type : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_type_new', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_capacity_old">Kapasitas (Tertulis)</label>
                                    {!! Form::text('tool_capacity_old', $revision->order->ServiceRequestItem->uttp->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity_old', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_capacity_new">Kapasitas (Pembetulan)</label>
                                    {!! Form::text('tool_capacity_new', $revision->tool_capacity != null ? $revision->tool_capacity : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_capacity_new', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_made_in_old">Buatan (Tertulis)</label>
                                    {!! Form::text('tool_made_in_old', $revision->order->ServiceRequestItem->uttp->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in_old', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_made_in_new">Buatan (Pembetulan)</label>
                                    {!! Form::text('tool_made_in_new', $revision->tool_made_in != null ? $revision->tool_made_in : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_made_in_new', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_factory_old">Pabrikan (Tertulis)</label>
                                    {!! Form::text('tool_factory_old', $revision->order->ServiceRequestItem->uttp->tool_factory, ['class' => 'form-control','id' => 'tool_factory_old', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_factory_new">Pabrikan (Pembetulan)</label>
                                    {!! Form::text('tool_factory_new', $revision->tool_factory != null ? $revision->tool_factory : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_factory_new', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_factory_address_old">Alamat Pabrikan (Tertulis)</label>
                                    {!! Form::textarea('tool_factory_address_old', $revision->order->ServiceRequestItem->uttp->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address_old', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tool_factory_address_new">Alamat Pabrikan (Pembetulan)</label>
                                    {!! Form::textarea('tool_factory_address_new', $revision->tool_factory_address != null ? $revision->tool_factory_address : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_factory_address_new', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="label_sertifikat_old">Pemohon (Tertulis)</label>
                                    {!! Form::text('label_sertifikat_old', $revision->request->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat_old', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="label_sertifikat_new">Pemohon (Pembetulan)</label>
                                    {!! Form::text('label_sertifikat_new', $revision->label_sertifikat != null ? $revision->label_sertifikat : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'label_sertifikat_new', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="addr_sertifikat_old">Alamat Pemohon (Tertulis)</label>
                                    {!! Form::textarea('addr_sertifikat_old', $revision->request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat_old', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="addr_sertifikat_new">Alamat Pemohon (Pembetulan)</label>
                                    {!! Form::textarea('addr_sertifikat_new', $revision->addr_sertifikat != null ? $revision->addr_sertifikat : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'addr_sertifikat_new', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="others">Lainnya (Catatan untuk perbaikan selain pada halaman pertama SKHP)</label>
                                    {!! Form::textarea('others', $revision->others, ['class' => 'form-control','id' => 'others', 'readonly']) !!}
                                </div>
                            </div>
                        </div>

                        

                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="processing">
        <div class="panel panel-filled" id="panel_process">
                
            <div class="panel-heading">
                <div class="panel-body">

                {!! Form::open(['id' => 'form_result', 'route' => ['revisiontechorderuttp.simpanproses', $revision->id], 'files' => true]) !!}
                                        
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_uttp">Jenis UTTP    
                                @if($revision->uttpType != null)
                                <span style="color:#1bbf89;">(Berubah)</span>
                                @else
                                <span>(Tetap)</span>
                                @endif
                            </label>
                            {!! Form::text('jenis_uttp', 
                                $revision->uttpType != null ? 
                                $revision->uttpType->uttp_type :
                                $revision->order->uttp->type->uttp_type, 
                                ['class' => 'form-control','id' => 'jenis_uttp', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Kapasitas Maksimum
                                @if($revision->tool_capacity != null)
                                <span style="color:#1bbf89;">(Berubah)</span>
                                @else
                                <span>(Tetap)</span>
                                @endif
                            </label>
                            {!! Form::text('tool_capacity', $revision->tool_capacity != null ? $revision->tool_capacity : $revision->order->tool_capacity, 
                                ['class' => 'form-control','id' => 'tool_capacity', 'readonly' ]) !!} 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Kapasitas Minimum
                                @if($revision->tool_capacity_min != null)
                                <span style="color:#1bbf89;">(Berubah)</span>
                                @else
                                <span>(Tetap)</span>
                                @endif
                            </label>
                            {!! Form::text('tool_capacity_min', $revision->tool_capacity_min != null ? $revision->tool_capacity_min : $revision->order->tool_capacity_min, 
                                ['class' => 'form-control','id' => 'tool_capacity_min', 'readonly']) !!} 
                        </div>  
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Merek
                                @if($revision->tool_brand != null)
                                <span style="color:#1bbf89;">(Berubah)</span>
                                @else
                                <span>(Tetap)</span>
                                @endif
                            </label>
                            {!! Form::text('tool_brand', $revision->tool_brand != null ? $revision->tool_brand : $revision->order->tool_brand, 
                                ['class' => 'form-control','id' => 'tool_brand', 'readonly' ]) !!} 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Model/Tipe
                                @if($revision->tool_model != null)
                                <span style="color:#1bbf89;">(Berubah)</span>
                                @else
                                <span>(Tetap)</span>
                                @endif
                            </label>
                            {!! Form::text('tool_model', $revision->tool_model != null ? $revision->tool_model : $revision->order->tool_model, 
                                ['class' => 'form-control','id' => 'tool_model', 'readonly' ]) !!} 
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No Seri
                                @if($revision->tool_serial_no != null)
                                <span style="color:#1bbf89;">(Berubah)</span>
                                @else
                                <span>(Tetap)</span>
                                @endif
                            </label>
                            {!! Form::text('tool_serial_no', $revision->tool_serial_no != null ? $revision->tool_serial_no : $revision->order->tool_serial_no, 
                                ['class' => 'form-control','id' => 'tool_serial_no', 'readonly']) !!} 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Media Uji/Komoditas
                                @if($revision->tool_media != null)
                                <span style="color:#1bbf89;">(Berubah)</span>
                                @else
                                <span>(Tetap)</span>
                                @endif
                            </label>
                            {!! Form::text('tool_media', $revision->tool_media != null ? $revision->tool_media : $revision->order->tool_media, 
                                ['class' => 'form-control','id' => 'tool_media', 'readonly']) !!} 
                        </div>
                    </div>
                </div>

                @if($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Kadar Air')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Metode Pengukuran</label>
                            {!! Form::select('tool_media_pengukuran', ['Kapasitansi'=>'Kapasitansi','Resistansi'=>'Resistansi'], 
                                $revision->order->tool_media_pengukuran, 
                                ['class' => 'form-control select2','id' => 'tool_media_pengukuran', 'style' => 'width:100%']) !!}
                        </div>
                    </div>
                </div>
                @endif

                <!--
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_made_in_id">Buatan
                                @if($revision->tool_made_in != null)
                                <span style="color:#1bbf89;">(Berubah)</span>
                                @else
                                <span>(Tetap)</span>
                                @endif
                            </label>
                            {!! Form::text('tool_made_in_id', $revision->tool_made_in != null ? $revision->tool_made_in : $revision->order->tool_made_in, 
                                ['class' => 'form-control','id' => 'tool_made_in_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="label_sertifikat">Pemilik
                                @if($revision->label_sertifikat != null)
                                <span style="color:#1bbf89;">(Berubah)</span>
                                @else
                                <span>(Tetap)</span>
                                @endif
                            </label>
                            {!! Form::text('label_sertifikat', 
                                $revision->label_sertifikat != null ? $revision->label_sertifikat : $revision->request->label_sertifikat,
                                ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Pemilik
                                @if($revision->addr_sertifikat != null)
                                <span style="color:#1bbf89;">(Berubah)</span>
                                @else
                                <span>(Tetap)</span>
                                @endif
                            </label>
                            {!! Form::text('addr_sertifikat', 
                                $revision->addr_sertifikat != null ? $revision->addr_sertifikat : $revision->request->addr_sertifikat,
                                ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                
                @if($revision->request->service_type_id == 6 || $revision->request->service_type_id == 7)
                <div class="row">
                    <div class="col-md-12">
                        <label for="dasar_pemeriksaan">Dasar Pemeriksaan/Pengujian/Verifikasi Lainnya (jika ada)</label>
                        <textarea name="dasar_pemeriksaan" id="dasar_pemeriksaan" readonly
                                class="form-control">{!! $revision->order->dasar_pemeriksaan !!}</textarea>
                    </div>
                </div>
                @endif

                <!--
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="test_by_1">Diuji Oleh (1)</label>
                            {!! Form::text('test_by_1', 
                                $revision->order->TestBy1 != null && $revision->order->TestBy1->PetugasUttp != null ? 
                                $revision->order->TestBy1->PetugasUttp->nama : '',
                                ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="test_by_2">Diuji Oleh (2)</label>
                            {!! Form::text('test_by_2', 
                                $revision->order->TestBy2 != null && $revision->order->TestBy2->PetugasUttp != null  ?
                                $revision->order->TestBy2->PetugasUttp->nama : '',
                                ['class' => 'form-control','id' => 'test_by_2', 'readonly']) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="mulai_uji">Waktu Pengujian, Mulai</label>
                            {!! Form::text('mulai_uji', 
                                date("d-m-Y", strtotime(isset($revision->order->mulai_uji) ? $revision->order->mulai_uji : date("Y-m-d"))),
                                ['class' => 'form-control','id' => 'mulai_uji', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="selesai_uji">Waktu Pengujian, Selesai</label>
                            {!! Form::text('selesai_uji', 
                                date("d-m-Y", strtotime(isset($revision->order->selesai_uji) ? $revision->order->selesai_uji : date("Y-m-d"))),
                                ['class' => 'form-control','id' => 'selesai_uji', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                -->

                <!--
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="file_skhp">Lampiran Evaluasi</label>
                            {!! Form::file('file_skhp', null,
                                ['class' => 'form-control','id' => 'file_skhp']) !!}
                        </div>
                    </div>
                </div>
                -->
                
                <input type="hidden" name="status_revision" id="status_revision" value="0"/>
                <button class="btn btn-w-md btn-accent" id="btn_simpan">Simpan Perbaikan</button>

                @if($revision->preview_count > 0)
                <button class="btn btn-w-md btn-success" id="btn_simpan_submit">Kirim Perbaikan</button>
                @else
                <br/>
                <span>Pastikan untuk me-review draft sebelum mengirim hasil uji</span>
                @endif

                
                {!! Form::close() !!}
            <div>
        </div>
    </div>
</div>

</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function ()
    {

        $("#btn_simpan").click(function(e){
            e.preventDefault();

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            $("#status_revision").val(1);

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

    });
</script>
@endsection