@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
<div class="row">
    <ul class="nav nav-tabs" role="tablist" id="tabs">
        @foreach($labs as $lab)
        @if ($loop->first)
        <li role="presentation" class="active"><a href="#order_{{ $lab->id }}" aria-controls="order_{{ $lab->id }}" role="tab" data-toggle="tab">{{ $lab->nama_lab }}</a></li>
        @else
        <li role="presentation"><a href="#order_{{ $lab->id }}" aria-controls="order_{{ $lab->id }}" role="tab" data-toggle="tab">{{ $lab->nama_lab }}</a></li>
        @endif
        @endforeach
    </ul>
    <div class="tab-content" id="nav-tabContent">
        @foreach($labs as $lab)
        <div role="tabpanel" class="tab-pane {{ ($loop->first) ? 'active' : '' }}" id="order_{{ $lab->id }}">
            <br/>

            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="qr_{{ $lab->d }}"  >
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="no_order">Terima Alat Berdasar QR Code</label>
                                            <input type="hidden" name="instalasi_id" value="{{ $lab->id }}" />
                                            {!! Form::text('no_order', null, ['class' => 'form-control no_order','id' => 'no_order_'.$lab->id, 'autofocus' => 'autofocus']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="error_notes" clsas=""></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="data_table" class="table table-striped table-hover table-responsive-sm data-table">
                                <thead>
                                    <tr>
                                        <th>No Order</th>
                                        <th>Nama Pemilik</th>
                                        <th>Nama Pemohon</th>
                                        <th>Alat</th>
                                        <th> Kapasitas/ Dayabaca</th>
                                        <th>Pengujian</th>
                                        <th>Tgl Order</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rows[$lab->id] as $row)
                                    <tr>
                                        <td>{{ $row->ServiceRequestItem->no_order }}</td>
                                        <td>{{ $row->ServiceRequest->label_sertifikat }}</td>
                                        <td>{{ $row->ServiceRequest->requestor->full_name }}</td>
                                        <td>{{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type}} ({{ $row->tool_serial_no ? $row->tool_serial_no : ''}})</td>
                                        <td>{{ $row && isset($row->tool_capacity_min) ? $row->tool_capacity_min : ''}}{{ $row ? $row->tool_capacity_min_unit : ''}} - {{$row ? $row->tool_capacity : ''}}{{ $row ? $row->tool_capacity_unit : ''}} / {{ $row ? $row->tool_dayabaca : ''}}{{ $row ? $row->tool_dayabaca_unit : ''}}</td>
                                        <td>
                                            @foreach($row->ServiceRequestItem->inspections as $inspection)
                                            {{ $inspection->inspectionPrice->inspection_type }}
                                            <br/>
                                            @endforeach
                                        </td>
                                        <td>{{ date("d-m-Y", strtotime($row->ServiceRequestItem->order_at)) }}</td>
                                        <td>
                                        
                                            <button class="btn btn-warning btn-sm btn-mdl" 
                                                data-id="{{ $row->ServiceRequestItem->id }}" 
                                                data-requestid="{{ $row->ServiceRequest->id }}"
                                                data-labid="{{ $laboratory_id }}"
                                                data-instalasiid="{{ $row->instalasi_id }}"
                                                data-alat="{{ $row->tool_brand . '/' . $row->tool_model . ' (' . ($row->tool_serial_no ? $row->tool_serial_no : '') . ')' }}">TERIMA ALAT</button>
                                        
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Terima Alat</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <input type="hidden" name="id" id="id"/>
                            <input type="hidden" name="requestid" id="requestid"/>
                            <input type="hidden" name="labid" id="labid"/>
                            <input type="hidden" name="instalasiid" id="instalasiid"/>

                            <div class="form-group">
                                <label>Alat</label>
                                <input type="text" name="alat" id="alat" class="form-control" readonly required />
                            </div>

                            <div class="form-group">
                                <label>Laboratorium</label>
                                <input type="text" name="lab_nama" id="lab_nama" class="form-control" readonly required />
                            </div>
                            <!-- <div class="form-group">
                                <label>Instalasi</label>
                                <input type="text" name="instalasi_nama" id="instalasi_nama" class="form-control" readonly required />
                                <select name="instalasi_id" id="instalasi_id" class="form-control select2" required></select>
                            </div> -->
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">TERIMA</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $('.data-table').DataTable({
        scrollX: true,
    });

    $("#data_table tbody").on("click","button.btn-mdl",function(e){
        e.preventDefault();
        var id = $(this).data().id;
        var requestid = $(this).data().requestid;
        var labid = $(this).data().labid;
        var instalasiid = $(this).data().instalasiid;
        var alat = $(this).data().alat;

        $("#id").val(id);
        $("#requestid").val(requestid);
        $("#labid").val(labid);
        $("#instalasiid").val(instalasiid);
        $("#alat").val(alat);

        var route = "{{ route('laboratory.getbyid', ':id') }}";
        route = route.replace(':id', labid);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#lab_nama").val(res.lab.nama_lab);
                $('#instalasi_id').empty();

                $.each(res.instalasi, function(i, iteminstalasi) {
                    if (iteminstalasi.id == instalasiid) {
                        $('#instalasi_nama').val(iteminstalasi.nama_instalasi);
                    }
                });
            });
        },"json");
    });

    $("#simpan").click(function(e) {
        e.preventDefault();

        var id = $('#id').val();
        var requestid = $('#requestid').val();
        var instalasiid = $('#instalasiid').val();
        
        //var route = "{{ route('receiveuttp.proses', ':id') }}";
        //route = route.replace(':id', id);
        //window.location = route;

        var form = $('#form_modal');
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{ route('receiveuut.proses') }}',form_data,function(response)
        {
            console.log(response.status)

            toastr["success"](response.messages,"Form Invalid Status : "+response.status);
            location.reload();
            // if(response.status===true)
            // {
            //     location.reload();
            // }
            // else
            // {
            //     // var msg = show_notice(response.messages);
            //     toastr["error"]("Mohon Periksan kembali","Form Invalid");
            // }
        });
        
        $("#prosesmodal").modal('hide');
    });

    $(".no_order").change(function() {
        var no_order = $(this).val();

        var form = $(this).closest("form");
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{ route('receiveuut.proses') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
    });

    // TABS
    $('#tabs a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });

    var hash = window.location.hash;
    $('#tabs a[href="' + hash + '"]').tab('show');
    // END TABS
});

</script>
@endsection