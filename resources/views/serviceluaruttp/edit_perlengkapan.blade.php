@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

@endsection

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(['url' => route('serviceluaruttp.simpaneditperlengkapan', ['id' => $order->id, 'idItem' => $item->id, 'idPerlengkapan' => $perlengkapan->id ]), 'id' => 'form_result'])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Alat Ukur</h4>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="label_sertifikat">Nama Pemilik</label>
                            {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Pemilik</label>
                            {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>  
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_uttp">Jenis UTTP</label>
                            {!! Form::text('jenis_uttp', 
                                $perlengkapan->uttp->type->uttp_type, ['class' => 'form-control','id' => 'jenis_uttp', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Merek</label>
                            {!! Form::text('tool_brand', $perlengkapan->uttp->tool_brand, ['class' => 'form-control','id' => 'tool_brand' ]) !!} 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Model/Tipe</label>
                            {!! Form::text('tool_model', $perlengkapan->uttp->tool_model, ['class' => 'form-control','id' => 'tool_model']) !!} 
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No Seri</label>
                            {!! Form::text('tool_serial_no', $perlengkapan->uttp->serial_no, ['class' => 'form-control','id' => 'tool_serial_no']) !!} 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Media Uji/Komoditas</label>
                            {!! Form::text('tool_media', $perlengkapan->uttp->tool_media, ['class' => 'form-control','id' => 'tool_media']) !!} 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Kapasitas Maksimum</label>
                            {!! Form::text('tool_capacity', $perlengkapan->uttp->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity' ]) !!} 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Kapasitas Minimum</label>
                            {!! Form::text('tool_capacity_min', $perlengkapan->uttp->tool_capacity_min, ['class' => 'form-control','id' => 'tool_capacity_min']) !!} 
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Satuan Kapasitas</label>
                            {!! Form::select('tool_capacity_unit', $units, $perlengkapan->uttp->tool_capacity_unit, ['class' => 'form-control','id' => 'tool_capacity_unit']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_made_in_id">Buatan</label>
                            {!! Form::select('tool_made_in_id', $negara, $item->uttp->tool_made_in_id, ['class' => 'form-control','id' => 'tool_made_in_id']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="factory">Nama Pabrikan</label>
                            {!! Form::text('tool_factory', 
                                $perlengkapan->uttp->tool_factory,
                                ['class' => 'form-control','id' => 'tool_factory']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="factory_addr">Alamat Pabrikan</label>
                            {!! Form::text('tool_factory_address', 
                                $perlengkapan->uttp->tool_factory_address,
                                ['class' => 'form-control','id' => 'tool_factory_address']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

       

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Lanjut</button> 
        </form>
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $('#tool_capacity_unit').select2({
            //placeholder: "- Pilih UML -",
            tags: true,
        });
        $('#tool_made_in_id').select2();
    });

</script>
@endsection