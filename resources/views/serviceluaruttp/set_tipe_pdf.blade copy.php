<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Sertifikat Evaluasi Tipe</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Arial Narrow", Arial, sans-serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-draft-uttp.png") : public_path("assets/images/logo/letterhead-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
    </style>
</head>

<body class="A4">

    <section class="sheet {{ $order->kabalai_date ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="padding-top: 35mm; padding-right: 25.4mm;">&nbsp;</div>

        <div class="text-center">
            <div class="title"><strong>SERTIFIKAT EVALUASI TIPE</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_surat_tipe }}</div>
        </div>

        <br/><br/>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 3mm;">1.</td>
                    <td colspan="3">Pemohon</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Nama Perusahaan</td>
                    <td style="width: 3mm;">:</td>
                    <td>{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Alamat Perusahaan</td>
                    <td>:</td>
                    <td>{{ $order->ServiceRequest->addr_sertifikat }}</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td colspan="3">Pabrikan</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Nama Pabrik</td>
                    <td>:</td>
                    <td>{{ 
                        $order->tool_factory != null ?
                        $order->tool_factory :
                        $order->ServiceRequestItem->uttp->tool_factory 
                    }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Negara Pembuat</td>
                    <td>:</td>
                    <td>{{ 
                        $order->tool_made_in != null ? 
                        $order->tool_made_in :
                        $order->ServiceRequestItem->uttp->tool_made_in 
                    }}</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td colspan="3">Identitas Alat Ukur, Alat Takar, Alat Timbang dan Alat Perlengkapan</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Jenis</td>
                    <td style="width: 3mm;">:</td>
                    <td>{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Merek</td>
                    <td>:</td>
                    <td>{{ 
                        $order->tool_brand != null ? 
                        $order->tool_brand :
                        $order->ServiceRequestItem->uttp->tool_brand 
                    }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Model/Tipe</td>
                    <td>:</td>
                    <td>{{ 
                        $order->tool_model != null ? 
                        $order->tool_model :
                        $order->ServiceRequestItem->uttp->tool_model 
                    }}</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td colspan="3">Deskripsi Teknis</td>
                </tr>
                <?php
                $teknis['tool_capacity'] = $order->tool_capacity . ' ' .$order->tool_capacity_unit;
                if($order->tool_capacity_min != null && $order->tool_capacity_min > 0) {
                    $teknis['Kapasitas Minimum'] = $order->tool_capacity_min . ' ' . $order->tool_capacity_unit;
                }
                if($order->kelas_keakurasian != null && $order->kelas_keakurasian != '') {
                    $teknis['Kelas Keakurasian'] = $order->kelas_keakurasian;
                }
                if($order->daya_baca != null && $order->daya_baca != '') {
                    $teknis['Daya Baca'] = $order->daya_baca;
                }
                if($order->interval_skala_verifikasi != null && $order->interval_skala_verifikasi != '') {
                    $teknis['Interval Skala Verifikasi'] = $order->interval_skala_verifikasi;
                }
                if($order->konstanta != null && $order->konstanta != '') {
                    $teknis['Konstanta'] = $order->konstanta;
                }
                if($order->kelas_single_axle_load != null && $order->kelas_single_axle_load != '') {
                    $teknis['Kelas Single Axle Load'] = $order->kelas_single_axle_load;
                }
                if($order->kelas_single_group_load != null && $order->kelas_single_group_load != '') {
                    $teknis['Kelas Single Group Load'] = $order->kelas_single_group_load;
                }
                if($order->metode_pengukuran != null && $order->metode_pengukuran != '') {
                    $teknis['Metode Pengukuran'] = $order->metode_pengukuran;
                }
                if($order->sistem_jaringan != null && $order->sistem_jaringan != '') {
                    $teknis['Sistem Jaringan'] = $order->sistem_jaringan;
                }
                if($order->kelas_temperatur != null && $order->kelas_temperatur != '') {
                    $teknis['Kelas Temperatur'] = $order->kelas_temperatur;
                }
                if($order->rasio_q != null && $order->rasio_q != '') {
                    $teknis['Rasio Q<sub>3</sub>/Q<sub>1</sub>'] = $order->rasio_q;
                }
                if($order->diameter_nominal != null && $order->diameter_nominal != '') {
                    $teknis['Diameter Nominal'] = $order->diameter_nominal;
                }
                ?>
                <tr>
                    <td></td>
                    <td>Kapasitas Maksimum</td>
                    <td>:</td>
                    <td>{{ 
                            $order->tool_capacity != null ?
                            $order->tool_capacity :
                            $order->ServiceRequestItem->uttp->tool_capacity 
                        }}&nbsp;
                        {{ 
                            $order->tool_capacity_unit != null ?
                            $order->tool_capacity_unit :
                            $order->ServiceRequestItem->uttp->tool_capacity_unit 
                        }}
                    </td>
                </tr>
                @if($order->tool_capacity_min != null && $order->tool_capacity_min > 0)
                <tr>
                    <td></td>
                    <td style="width: 40mm;">Kapasitas Minimum</td>
                    <td>:</td>
                    <td>{{ $order->tool_capacity_min }}
                        {{ $order->tool_capacity_unit }}
                    </td>
                </tr>
                @elseif($order->ServiceRequestItem->uttp->tool_capacity_min > 0)
                <tr>
                    <td></td>
                    <td>Kapasitas Minimum</td>
                    <td>:</td>
                    <td>{{ $order->ServiceRequestItem->uttp->tool_capacity_min 
                        }}
                        {{ $order->ServiceRequestItem->uttp->tool_capacity_unit 
                        }}
                    </td>
                </tr>
                @endif
                @if($order->kelas_keakurasian != null && $order->kelas_keakurasian != '')
                <tr>
                    <td></td>
                    <td>Kelas Keakurasian</td>
                    <td>:</td>
                    <td>{{ $order->kelas_keakurasian }}</td>
                </tr>
                @endif
                @if($order->daya_baca != null && $order->daya_baca != '')
                <tr>
                    <td></td>
                    <td>Daya Baca</td>
                    <td>:</td>
                    <td>{{ $order->daya_baca }}</td>
                </tr>
                @endif
                @if($order->interval_skala_verifikasi != null && $order->interval_skala_verifikasi != '')
                <tr>
                    <td></td>
                    <td>Interval Skala Verifikasi</td>
                    <td>:</td>
                    <td>{{ $order->interval_skala_verifikasi }}</td>
                </tr>
                @endif
                @if($order->kelas_single_axle_load != null && $order->kelas_single_axle_load != '')
                <tr>
                    <td></td>
                    <td>Kelas <i>Single Axle Load</i></td>
                    <td>:</td>
                    <td>{{ $order->kelas_single_axle_load }}</td>
                </tr>
                @endif
                @if($order->kelas_single_group_load != null && $order->kelas_single_group_load != '')
                <tr>
                    <td></td>
                    <td>Kelas <i>Single Group Load</i></td>
                    <td>:</td>
                    <td>{{ $order->kelas_single_group_load }}</td>
                </tr>
                @endif
                @if($order->metode_pengukuran != null && $order->metode_pengukuran != '')
                <tr>
                    <td></td>
                    <td>Metode Pengukuran</td>
                    <td>:</td>
                    <td>{{ $order->metode_pengukuran }}</td>
                </tr>
                @endif
                @if($order->sistem_jaringan != null && $order->sistem_jaringan != '')
                <tr>
                    <td></td>
                    <td>Sistem Jaringan</td>
                    <td>:</td>
                    <td>{{ $order->sistem_jaringan }}</td>
                </tr>
                @endif
                @if($order->konstanta != null && $order->konstanta != '')
                <tr>
                    <td></td>
                    <td>Konstanta</td>
                    <td>:</td>
                    <td>{{ $order->konstanta }}</td>
                </tr>
                @endif
                <tr>
                    <td>5.</td>
                    <td>Referensi Standar</td>
                    <td>:</td>
                    <td>{!! $order->ServiceRequestItem->uttp->type->syarat_teknis !!}</td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td style="max-width: 20mm;">a. Nomor Surat Keterangan Hasil Pemeriksaan Tipe</td>
                    <td>:</td>
                    <td>
                        <?php $idx = 0; ?>
                        @foreach($otherOrders as $other)
                        @if($other->is_skhpt)
                        {{ $other->no_sertifikat != null ? $other->no_sertifikat : '-- sedang diproses bersamaan --' }}{!! $idx < count($otherOrders) ? '<br/>' : '' !!}
                        <?php $idx++; ?>
                        @endif
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>b. Nomor Surat Keterangan Hasil Pengujian</td>
                    <td>:</td>
                    <td>
                        <?php $idx = 0; ?>
                        @foreach($otherOrders as $other)
                        @if(!$other->is_skhpt)
                        {{ $other->no_sertifikat != null ? $other->no_sertifikat : '-- sedang diproses bersamaan --' }}{!! $idx < count($otherOrders) ? '<br/>' : '' !!}
                        <?php $idx++; ?>
                        @endif
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Hasil</td>
                    <td>:</td>
                    <td><strong>{{ $order->ServiceRequestItem->uttp->type->uttp_type }}</strong> dengan Merek <strong>{{ $order->ServiceRequestItem->uttp->tool_brand }}</strong> tipe <strong>{{ $order->ServiceRequestItem->uttp->tool_model }}</strong> dinyatakan <strong>
                        {{ $order->hasil_uji_memenuhi ? 'Memenuhi' : 'Tidak Memenuhi'}} Syarat Teknis</strong></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3">
                    Surat Keterangan Hasil Pemeriksaan Tipe (SKHPT) dan/atau Surat Keterangan Hasil Pengujian (SKHP) tercantum dalam lampiran yang merupakan bagian tidak terpisahkan dari Sertifikat Evaluasi Tipe ini.
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tanggal diterbitkan</td>
                    <td>:</td>
                    <?php setlocale(LC_TIME, "id_ID"); ?>
                    <td>{{ $order->kabalai_date ? strftime("%e %B %Y", strtotime($order->kabalai_date)) : strftime("%e %B %Y") }}</td>
                </tr>
            </tbody>
        <table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 18.5mm;">
            <tbody>
                <tr>
                    <td>Kepala Balai Pengujian UTTP,</td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai' }}<br/>
                        NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
    </section>

</body>

</html>