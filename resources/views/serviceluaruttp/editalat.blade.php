@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        {!! Form::open(['url' => route('serviceluaruttp.saveeditalat', $serviceOrder->id), 'id' => 'form_result'])  !!}
        
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_uttp">Jenis UTTP</label>
                    {!! Form::text('jenis_uttp', 
                        $serviceOrder->uttp != null ? 
                        $serviceOrder->uttp->type->uttp_type :
                        $serviceOrder->ServiceRequestItem->uttp->type->uttp_type, ['class' => 'form-control','id' => 'jenis_uttp', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
                            
            <div class="col-md-3">
                <div class="form-group">
                    <label>Merek</label>
                    {!! Form::text('tool_brand', $serviceOrder->tool_brand, ['class' => 'form-control','id' => 'tool_brand']) !!} 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Model/Tipe</label>
                    {!! Form::text('tool_model', $serviceOrder->tool_model, ['class' => 'form-control','id' => 'tool_model']) !!} 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Nomor Seri</label>
                    {!! Form::text('tool_serial_no', $serviceOrder->tool_serial_no, ['class' => 'form-control','id' => 'tool_serial_no']) !!} 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Media Uji/Komoditas</label>
                    {!! Form::text('tool_media', $serviceOrder->tool_media, ['class' => 'form-control','id' => 'tool_media']) !!} 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Kapasitas Maksimum</label>
                    {!! Form::text('tool_capacity', $serviceOrder->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity' ]) !!} 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Kapasitas Minimum</label>
                    {!! Form::text('tool_capacity_min', $serviceOrder->tool_capacity_min, ['class' => 'form-control','id' => 'tool_capacity_min']) !!} 
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Satuan Kapasitas</label>
                    {!! Form::select('tool_capacity_unit', $units, $serviceOrder->tool_capacity_unit, ['class' => 'form-control','id' => 'tool_capacity_unit']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Buatan</label>
                    {!! Form::select('tool_made_in_id', $negara, $serviceOrder->tool_made_in_id, ['class' => 'form-control','id' => 'tool_made_in']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nama Pabrik Pembuat</label>
                    {!! Form::text('tool_factory', $serviceOrder->tool_factory, ['class' => 'form-control','id' => 'tool_factory']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Alamat Pabrik Pembuat</label>
                    {!! Form::textarea('tool_factory_address', $serviceOrder->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address']) !!} 
                </div>
            </div>
        </div>

        <button role="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan Perubahan Data Alat</button>
         {!! Form::close() !!}

    </div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function ()
    {
        $('#tool_capacity_unit').select2({
            //placeholder: "- Pilih UML -",
            tags: true,
        });
        $('#tool_made_in').select2();
        $("#form_result").validate({
            rules: {
                file_skhp: {
                    required: true,
                },
            },
            messages: {
                file_skhp: 'File lampiran harus diupload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    }); 
</script>
@endsection