@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    
    <div class="panel panel-filled table-area">
        <div class="panel-body">
            
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No SET</th>
                        <th>Jenis UTTP</th>
                        <th>Merek</th>
                        <th>Model/Tipe</th>
                        <th>Nama Pemilik</th>
                        <th>Tanggal Terbit SET</th>
                        <th>Disahkan Oleh</th>
                        <th>No SKHP</th>
                        <th>No SKHPT</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $row)
                    <tr id="{{ $row->id }}">
                        <td>{{ $row->no_surat_tipe }}</td>
                        <td>{{ $row->uttp_type_certificate }} /<br/>{{ $row->uttp_type }}</td>
                        <td>{{ $row->tool_brand }}</td>
                        <td>{{ $row->tool_model }}</td>
                        <td>{{ $row->label_sertifikat }}</td>
                        <td>{{ date("d-m-Y", strtotime($row->kabalai_date)) }}</td>
                        <td>{{ $row->kabalai }}</td>
                        <td>{{ $row->no_sertifikat }}</td>
                        <td>{{ $row->no_sertifikat_tipe}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    let table = $('#data_table').DataTable({
        scrollX: true,
    });

   
});

</script>
@endsection