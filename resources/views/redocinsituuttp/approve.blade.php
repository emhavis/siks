@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
            
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Booking</h4>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="label_sertifikat">Nama Pemilik</label>
                                    {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="addr_sertifikat">Alamat Pemilik</label>
                                    {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                                </div>
                            </div>  
                        </div>

                    </div>
                </div>

                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Pemohon</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_name">Nama Pemohon</label>
                                    {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_name">Jenis Tanda Pengenal</label>
                                    {!! Form::text('id_type_id', $requestor->kantor == "Perusahaan" ? "NIB" : "NPWP", ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                                    {!! Form::text('pic_id_no', $requestor->kantor == "Perusahaan" ? $requestor->nib : $requestor->npwp, ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_phone_no">Nomor Telepon</label>
                                    {!! Form::text('pic_phone_no', $requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_email">Alamat Email</label>
                                    {!! Form::text('pic_email', $requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_from">Usulan Jadwal Mulai</label>
                                    {!! Form::text('scheduled_test_date_from', date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_from', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_to">Usulan Jadwal Selesai</label>
                                    {!! Form::text('scheduled_test_date_to', date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_to', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis_layanan">Jenis Layanan</label>
                                    {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lokasi_pengujian">Lokasi Pengujian</label>
                                    {!! Form::text('lokasi_pengujian', 
                                        ($request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor') . ($request->lokasi_dl == 'dalam' ? ' (Dalam Negeri)' : ' (Luar Negeri)') , 
                                        ['class' => 'form-control','id' => 'lokasi_pengujian', 'readonly']) !!}
                                    
                                </div>
                                
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="no_surat_permohonan">No Surat Permohonan</label>
                                    {!! Form::text('no_surat_permohonan', $request->no_surat_permohonan, ['class' => 'form-control','id' => 'no_surat_permohonan', 'placeholder' => 'No Surat Permohonan', 'autocomplete' => 'off', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tgl_surat_permohonan">Tanggal Surat Permohonan</label>
                                    {!! Form::text('tgl_surat_permohonan', date("d-m-Y", strtotime(isset($request->tgl_surat_permohonan) ? $request->tgl_surat_permohonan : date("Y-m-d"))), ['class' => 'form-control','id' => 'tgl_surat_permohonan', 'placeholder' => 'Tanggal Surat Permohonan', 'autocomplete' => 'off', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="file_surat_permohonan">Surat Permohonan</label>
                                    @if(!empty($request->file_surat_permohonan))
                                    <div class="input-group">
                                        <a href="<?= config('app.siks_url') ?>/tracking/download_permohonan/{{ $request->id }}" class="btn btn-warning btn-sm">DOWNLOAD</a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Penugasan dan Penjadwalan</h4>
                    </div>
                    <div class="panel-body" id="panel_staff">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="doc_no">No Surat Tugas (Revisi)</label>
                                    {!! Form::text('doc_no', $doc->doc_no,
                                        ['class' => 'form-control','id' => 'doc_no', 'readonly']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            @if ($request->lokasi_dl == 'dalam')
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inspection_prov_id">Alamat Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                                    {!! Form::text('inspection_prov_id', 
                                        $request->inspectionKabkot->nama .', '. $request->inspectionProv->nama,
                                        ['class' => 'form-control','id' => 'inspection_prov_id', 'readonly']) !!}
                                </div>
                            </div>
                            @else 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inspection_prinspection_negara_idov_id">Alamat Lokasi Pengujian, Negara</label>
                                    {!! Form::text('inspection_negara_id', 
                                        $request->inspectionNegara->nama_negara,
                                        ['class' => 'form-control','id' => 'inspection_negara_id', 'readonly']) !!}
                                </div>
                            </div>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="spuh_price">Total Uang Harian</label>
                                    {!! Form::hidden('spuh_rate_id', $request->spuh_rate_id,
                                        ['class' => 'form-control','id' => 'spuh_rate_id', 'readonly']) !!}
                                    {!! Form::text('spuh_price', $request->spuh_price,
                                        ['class' => 'form-control','id' => 'spuh_price', 'readonly']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_from">Jadwal Waktu Pengujian, Mulai</label>
                                    {!! Form::text('date_from', 
                                        date("d-m-Y", strtotime(isset($doc->date_from) ? $doc->date_from : date("Y-m-d"))),
                                        ['class' => 'form-control','id' => 'date_from', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date_to">Jadwal Waktu Pengujian, Selesai</label>
                                    {!! Form::text('date_to', 
                                        date("d-m-Y", strtotime(isset($doc->date_to) ? $doc->date_to : date("Y-m-d"))),
                                        ['class' => 'form-control','id' => 'date_to', 'readonly']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="file_bukti_bayar">Catatan Revisi</label>
                                    <textarea name="keterangan_revisi" id="keterangan_revisi" class="form-control" disabled>{{ $doc->keterangan_revisi }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="table_data_penjadwalan">Penguji/Pemeriksa</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table_data_penjadwalan" class="table table-striped table-hover table-responsive-sm">
                                    <thead>
                                        <tr>
                                            <th>NIP</th>
                                            <th>Nama</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($staffs as $row)
                                        @if($row->scheduledStaff != null)
                                        <tr>
                                            <td>{{ $row->scheduledStaff->nip }}</td>
                                            <td>{{ $row->scheduledStaff->nama }}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                {!! Form::open(['url' => route('redocinsituuttp.approve', $request->id), 'id' => 'form_approve'])  !!}
        
                <input type="hidden" name="id" value="{{ $request->id }}" />
                
                <input type="hidden" name="submit_val" id="submit_val" value="0"/>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="is_approved">Persetujuan</label>
                            {!! Form::select('is_approved', ['ya'=>'Ya, Setuju','tidak'=>'Perbaiki'], null, 
                                ['class' => 'form-control select2','id' => 'is_approved', 'style' => 'width:100%']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="notes">Catatan</label>
                            {!! Form::textarea('approval_note', $doc->approval_note,
                                ['class' => 'form-control','id' => 'approval_note', 'readonly']) !!}       
                        </div>
                    </div>
                </div>
                
                <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan_submit">Setujui Surat Tugas</button>  

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/jquery-validation/additional-methods.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan, #data_table_check").DataTable();

        $('#is_approved').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        }).on('select2:select', function (e) {
            var data = e.params.data;
            var stat = $('#status').val();

            if (data.id == 'tidak') {
                $('#btn_simpan_submit').text('Surat Tugas Tidak Lengkap');
                $('#btn_simpan_submit').removeClass('btn-success');
                $('#btn_simpan_submit').addClass('btn-danger');

                $("#approval_note").prop("readonly", false); 
            } else {
                $('#btn_simpan_submit').text('Setujui Surat Tugas');
                $('#btn_simpan_submit').removeClass('btn-danger');
                $('#btn_simpan_submit').addClass('btn-success');

                $("#approval_note").val("");
                $("#approval_note").prop("readonly", true); 
            }
        });

        $("#form_approve").validate({
            rules: {
                notes: {
                    required: function(element) {
                        return ($("#is_approved").val() == 'tidak');
                    },
                },
                /*
                file_review: {
                    required: function(element) {
                        return ($("#is_approved").val() == 'tidak');
                    },
                },
                */
            },
            messages: {
                notes: 'Catatan perbaikan harus diisi',
                //file_review: 'File lampiran perbaikan harus diupload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    });
</script>
@endsection