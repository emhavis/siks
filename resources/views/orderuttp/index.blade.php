@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
<div class="row">
    <ul class="nav nav-tabs" role="tablist" id="tabs">
        @foreach($instalasiList as $instalasi)
        @if ($loop->first)
        <li role="presentation" class="active"><a href="#order_{{ $instalasi->id }}" aria-controls="order_{{ $instalasi->id }}" role="tab" data-toggle="tab">{{ $instalasi->nama_instalasi }}</a></li>
        @else
        <li role="presentation"><a href="#order_{{ $instalasi->id }}" aria-controls="order_{{ $instalasi->id }}" role="tab" data-toggle="tab">{{ $instalasi->nama_instalasi }}</a></li>
        @endif
        @endforeach
    </ul>
    <div class="tab-content" id="nav-tabContent">
        @foreach($instalasiList as $instalasi)
        <div role="tabpanel" class="tab-pane {{ ($loop->first) ? 'active' : '' }}" id="order_{{ $instalasi->id }}">
            <br/>

            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="qr_{{ $instalasi->id }}"  >
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="no_order">Terima Alat Berdasar QR Code</label>
                                            <input type="hidden" name="instalasi_id" value="{{ $instalasi->id }}" />
                                            {!! Form::text('no_order', null, ['class' => 'form-control no_order','id' => 'no_order_'.$instalasi->id, 'autofocus' => 'autofocus']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="error_notes" clsas=""></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br/>

                    
                    <table id="data_table_{{ $instalasi->id }}" class="table table-striped table-hover table-responsive-sm data_table_order">
                        <thead>
                            <tr>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Pengujian</th>
                                <th>Tgl Order</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows[$instalasi->id] as $row)
                            <tr>
                                <td>{{ $row->ServiceRequestItem->no_order }}</td>
                                <td>
                                    {{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : ''}})
                                    <br/>{{ $row->uttpType->uttp_type }}
                                </td>
                                <td>
                                    @foreach($row->ServiceRequestItem->inspections as $inspection)
                                    {{ $inspection->inspectionPrice->inspection_type }}
                                    <br/>
                                    @endforeach
                                </td>
                                <td>{{ date("d-m-Y", strtotime($row->ServiceRequestItem->order_at)) }}</td>
                                <td>
                                @if($loop->index == 0)
                                <button class="btn btn-warning btn-sm btn-mdl" 
                                    data-id="{{ $row->ServiceRequestItem->id }}" 
                                    data-requestid="{{ $row->ServiceRequest->id }}"
                                    data-labid="{{ $laboratory_id }}"
                                    data-instalasiid="{{ $row->instalasi_id }}">PROSES</button>
                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Proses Order</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">

                            <input type="hidden" name="id" id="id"/>
                            <input type="hidden" name="requestid" id="requestid"/>
                            <input type="hidden" name="labid" id="labid"/>
                            <input type="hidden" name="instalasi_id" id="instalasiid"/>
                            <div class="form-group">
                                <label>Laboratorium</label>
                                <input type="text" name="lab_nama" id="lab_nama" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Instalasi</label>
                                <input type="text" name="instalasi_nama" id="instalasi_nama" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pengujian/Pemeriksaan</label>
                                <textarea name="inspections" id="inspections" readonly
                                    class="form-control"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">PROSES</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>

<script type="text/javascript">
$(document).ready(function()
{
    $('.data_table_order').DataTable({
        scrollX: true,
        ordering: false,
    });

    $("button.btn-mdl").click(function(e){
        e.preventDefault();
        var id = $(this).data().id;
        var requestid = $(this).data().requestid;
        var labid = $(this).data().labid;
        var instalasiid = $(this).data().instalasiid;

        $("#id").val(id);
        $("#requestid").val(requestid);
        $("#labid").val(labid);
        $("#instalasiid").val(instalasiid);

        var route = "{{ route('laboratory.getbyid', ':id') }}";
        route = route.replace(':id', labid);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#lab_nama").val(res.lab.nama_lab);
                $('#instalasi_id').empty();

                $.each(res.instalasi, function(i, iteminstalasi) {
                    if (iteminstalasi.id == instalasiid) {
                        $('#instalasi_nama').val(iteminstalasi.nama_instalasi);
                    }
                    /*
                    $('#instalasi_id').append(`<option value="${iteminstalasi.id}">
                                       ${iteminstalasi.nama_instalasi}
                                  </option>`);
                    */
                });
            });
        },"json");

        var route = "{{ route('orderuttp.getinspections', ':id') }}";
        route = route.replace(':id', id);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#inspections").val(res.inspections.join("\n"));
            });
        },"json");
    });

    $("#simpan").click(function(e) {
        e.preventDefault();

        var id = $('#id').val();
        var requestid = $('#requestid').val();
        var instalasiid = $('#instalasiid').val();
        
        //var route = "{{ route('orderuttp.proses', ':id') }}";
        //route = route.replace(':id', id);

        //$("#prosesmodal").modal('hide');
        
        //window.location = route;

        var form = $('#form_modal');
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{ route('orderuttp.proses') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksa kembali","Form Invalid");
            }
        });
        
        $("#prosesmodal").modal('hide');
        
    });

    $(".no_order").change(function() {
        console.log('disinis');
        var no_order = $(this).val();

        var form = $(this).closest("form");
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        $.post('{{ route('orderuttp.proses') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
    });

    // TABS
    $('#tabs a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });

    var hash = window.location.hash;
    $('#tabs a[href="' + hash + '"]').tab('show');
    // END TABS
});

</script>
@endsection