@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>        
        <div class="panel-heading">
            <div class="panel-body">
                {!! Form::open(array('id' => 'form_create_standard', 'enctype' => "multipart/form-data")) !!}
                    <div class="form-group">
                        <label for="tool_code">QR Code Alat</label>

                        <span class="input-group">
                            {!! Form::text('tool_code', '',
                               ['class' => 'form-control',
                                'style' => 'font-size:16px;',
                                'id' => 'tool_code',
                                'placeholder' => '000.0.00.0000',
                                'required']) !!}
                            <span class="input-group-btn">
                                <button id="search_item" type="button" class="btn btn-accent">Cari</button>
                                <a class="btn btn-success" href="{{ route('service.create') }}">
                                    Buat Baru
                                </a>    
                            </span>
                        </span>                        
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="uml_id2" id="uml_id2" />
                        <label for="uml_id">Nama Unit Metrologi Legal</label>
                        {!! Form::text('uml_id', '',
                            ['class' => 'form-control',
                             'id' => 'uml_id',
                             'placeholder' => 'UML',
                             'readonly']) !!}

                    </div>
                    <div class="form-group">
                        <input type="hidden" name="uml_sub_id2" id="uml_sub_id2" />
                        <label for="uml_sub_id">Nama Sub Unit Metrologi Legal</label>
                        {!! Form::text('uml_sub_id', '',
                            ['class' => 'form-control',
                             'id' => 'uml_sub_id',
                             'placeholder' => 'Sub UML',
                             'readonly']) !!}

                    </div>
            <div class="panel panel-filled panel-c-accent">
                <div class="panel-body">
                    <div class="form-group">
                        <input type="hidden" name="standard_measurement_type_id2" id="standard_measurement_type_id2" />
                        <label for="standard_measurement_type_id">Jenis Besaran</label>
                        {!! Form::text('standard_measurement_type_id', '',
                            ['class' => 'form-control',
                             'id' => 'standard_measurement_type_id',
                             'placeholder' => 'Jenis Besaran',
                             'readonly']) !!}
                    </div>
                    <div class="form-group">
                        <label for="standard_tool_type_id">Jenis Alat</label>
                        {!! Form::select('standard_tool_type_id', array(), null,
                            ['class' => 'form-control',
                             'id' => 'standard_tool_type_id',
                             'placeholder' => '- Pilih Jenis Alat -',
                             'required']) !!}
                        <span id="progress_spin" style="float:right;display:none;">
                            <i class="fa fa-circle-o-notch fa-spin"></i>
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="standard_detail_type_id">Rincian Alat</label>
                        {!! Form::select('standard_detail_type_id', array(), null,
                            ['class' => 'form-control',
                             'id' => 'standard_detail_type_id',
                             'placeholder' => '- Pilih Rincian Alat -',
                             'required']) !!}
                        <span id="progress_spin" style="float:right;display:none;">
                            <i class="fa fa-circle-o-notch fa-spin"></i>
                        </span>
                    </div>
                </div>
            </div>
                    <div class="form-group">
                        <label for="sumber_id">Sumber</label>
                        {!! Form::select('sumber_id', $msumber, null,
                            ['class' => 'form-control',
                             'id' => 'sumber_id',
                             'placeholder' => '- Pilih Sumber -',
                             'required']) !!}

                    </div>
                    <div class="form-group">
                        <label for="brand">Merk</label>
                        {!! Form::text('brand', '',
                                       ['class' => 'form-control',
                                        'id' => 'brand',
                                        'placeholder' => 'Merk',
                                        'autocomplete' => 'off',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="made_in">Buatan</label>
                        {!! Form::select('made_in', $standardNegara, null,
                            ['class' => 'form-control',
                             'id' => 'made_in',
                             'placeholder' => '- Pilih Negara -',
                             'required']) !!}

                    </div>
                    <div class="form-group">
                        <label for="model">Model</label>
                        {!! Form::text('model', '',
                                       ['class' => 'form-control',
                                        'id' => 'model',
                                        'placeholder' => 'Model',
                                        'autocomplete' => 'off',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="tipe">Tipe</label>
                        {!! Form::text('tipe', '',
                                       ['class' => 'form-control',
                                        'id' => 'tipe',
                                        'placeholder' => 'Tipe Alat',
                                        'autocomplete' => 'off',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="no_seri">Nomor Seri</label>
                        {!! Form::text('no_seri', '',
                                       ['class' => 'form-control',
                                        'id' => 'no_seri',
                                        'placeholder' => 'Nomor Seri',
                                        'autocomplete' => 'off',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="no_identitas">Nomor Identitas</label>
                        {!! Form::text('no_identitas', '',
                                       ['class' => 'form-control',
                                        'id' => 'no_identitas',
                                        'placeholder' => 'Nomor Identitas',
                                        'autocomplete' => 'off',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="capacity">Kapasitas</label>
                        {!! Form::text('capacity', '',
                                       ['class' => 'form-control',
                                        'id' => 'capacity',
                                        'placeholder' => 'Kapasitas',
                                        'autocomplete' => 'off',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="daya_baca">Daya Baca</label>
                        {!! Form::text('daya_baca', '',
                                       ['class' => 'form-control',
                                        'id' => 'daya_baca',
                                        'placeholder' => 'Daya Baca',
                                        'autocomplete' => 'off',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="class">Kelas</label>
                        {!! Form::text('class', '',
                                       ['class' => 'form-control',
                                        'id' => 'class',
                                        'placeholder' => 'Kelas',
                                        'autocomplete' => 'off',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="jumlah_per_set">Jumlah Per Set</label>
                        {!! Form::text('jumlah_per_set', '',
                                       ['class' => 'form-control',
                                        'id' => 'jumlah_per_set',
                                        'required']) !!}

                    </div>
                    <button type="button" id="btn_cancel" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</button>
                    <button type="button" id="btn_submit" class="btn btn-warning"><i class="fa fa-save"></i> Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.min.js') }}"></script>
<script type='text/javascript'>

var el = new Object();

$(document).ready(function(){

    el.masking = "999.9.99.9999";

    $('#standard_tool_type_id, #standard_detail_type_id, #made_in, #sumber_id').select2({
        allowClear: true
    });

    $("#tool_code").inputmask(el.masking);

    $("#jumlah_per_set").inputmask({'alias': 'numeric', 'placeholder': '0'});

    $('#search_item').click(function()
    {

    });

    $('#search_item').click(function()
    {
        $.post('{{ route('qrcode.qrcodeinfoByUml') }}',{ tool_code: $('#tool_code').val(),uml_id:{{Auth::user()->uml_id}} },function(response)
        {
            res = response[0];
            console.log(response);
            $("#uml_id").val(res["uml_name"]);
            $("#uml_id2").val(res["uml_id"]);
            $("#uml_sub_id").val(res["uml_sub_name"]);
            $("#uml_sub_id2").val(res["uml_sub_id"]);
            $("#standard_measurement_type_id").val(res["standard_type"]);
            $("#standard_measurement_type_id2").val(res["standard_measurement_type_id"]);

            $('#standard_tool_type_id').empty();
            $('#standard_detail_type_id').empty();

            $.ajax({
                url: '{{ route('measurementtype.tool_type') }}',
                type: 'post',
                data: {
                    standardMeasurementTypeId: res["standard_measurement_type_id"],
                    _token: "{{ csrf_token() }}"
                },
                success: function(response){
                    var standard_drop_list = $('#standard_tool_type_id');

                    standard_drop_list.append("<option>- Pilih Jenis Alat -</option>");
                    $.each(response, function(index, element) {
                        standard_drop_list.append("<option value='"+ index +"'>" + element + "</option>");
                    });
                }
            });

        },"json");
    });

    $('#standard_tool_type_id').change(function(){
        $.ajax({
            url: '{{ route('measurementtype.detail_type') }}',
            type: 'post',
            data: {
                standardToolTypeId: this.value,
                _token: "{{ csrf_token() }}"                
            },
            beforeSend: function() {
                $(".progress-spin").show();
            },
            success: function(response){
                var standard_drop_list = $('#standard_detail_type_id');
                standard_drop_list.empty();

                standard_drop_list.append("<option>- Pilih Jenis Alat -</option>");
                $.each(response, function(index, element) {

                    standard_drop_list.append("<option value='"+ index +"'>" + element + "</option>");
                });
            },
            complete:function(data){
                $(".progress-spin").hide();
            }
        });
    });

    $("#btn_submit").click(function(e){
        e.preventDefault();
        $("#alert_board").empty();

        var form_object = $("#form_create_standard");
        var form_data = $("#form_create_standard").serialize();

        $("#panel_create").toggleClass("ld-loading");

        $.post('{{ route('umlstandard.store') }}',form_data,function(response){
            if(response[0]==false)
            {
                var msg = show_notice(response[1]);
                $("#alert_board").html(msg);
                $("#panel_create").toggleClass("ld-loading");
            }
            else
            {
                window.location = '{{ route('umlstandard') }}';
            }
        },"json");
    });

});

function show_notice(msg)
{
    var errmessage = {
        'tool_code':'QR Code Alat wajib diisi',
        'uml_id':'Nama Unit Metrologi Legal wajib diisi',
        'uml_sub_id':'Nama Sub Unit Metrologi Legal wajib diisi',
        'standard_measurement_type_id':'Jenis Besaran wajib diisi',
        'sumber_id':'Nama Sumber wajib diisi',
        'standard_tool_type_id':'Jenis Alat wajib diisi',
        'standard_detail_type_id':'Rincian Alat wajib diisi',
        'brand':'Merk wajib diisi',
        'made_in':'Buatan wajib diisi',
        'model':'Model wajib diisi',
        'tipe':'Tipe wajib diisi',
        'no_seri':'No Seri wajib diisi',
        'no_identitas':'No Identitas wajib diisi',
        'capacity':'No Kapasitas wajib diisi',
        'daya_baca':'Daya Baca wajib diisi',
        'class':'Kelas wajib diisi',
        'jumlah_per_set':'Jumlah Per Set wajib diisi'
    };

    $(document).find("small.wajib-isi").remove();
            var str = '<div class="panel panel-filled panel-c-danger">'+
        '<div class="panel-body">'+
            '<b>Terjadi kesalahan.</b>'+
            'Telah terjadi kesalahan, hubungi administrator untuk info lebih lanjut.'+
            '<ul>';
            $.each(msg,function(i,v)
            {
                if(errmessage[i])
                {
                    str += '<li>'+errmessage[i]+'</li>';
                    $(document).find("label[for='"+i+"']").append("<small class='text-warning wajib-isi'> wajib diisi</small>");
                }else{
                    str += '<li>'+v+'</li>';
                }
            });
    str += '</ul>'+
        '</div>'+
    '</div>';

    return str;
}

</script>

@endsection
