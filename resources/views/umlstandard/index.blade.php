@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    @if(Auth::user()->user_role=="7")
    <a href="{{ route('umlstandard.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Register Standard Baru</a>
    @endif
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>QR CODE</th>
                        <th>UML</th>
                        <th>SUB UML</th>
                        <th>Standard Type</th>
                        <th>Jumlah Per Set</th>
                        <th>Tanggal Register</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @if($registeredStandards->count())
                    @foreach($registeredStandards as $data)

                    <tr>
                        <td>{{ $data->tool_code }}</td>
                        <td>{{ $data->uml_name }}</td>
                        <td>{{ $data->uml_sub_name }}</td>
                        <td>{{ $data->standard_type }}</td>
                        <td>{{ $data->jumlah_per_set }}</td>
                        <td>{{ $data->register_date }}</td>
                        <td>
                        @if($data->stat_registered=="0")
                            @if(Auth::user()->user_role=="7")
                            NEW
                            @endif
                            @if(Auth::user()->user_role=="2")
                            <a href="{{ route('umlstandard.proses', $data->id) }}" class="btn btn-warning btn-sm">PROSES</a>
                            @endif
                        @endif
                        @if($data->stat_registered=="1")
                            @if(Auth::user()->user_role=="7")
                            SEDANG DI PROSES
                            @endif
                            @if(Auth::user()->user_role=="2")
                            SUDAH MASUK PROSES
                            @endif
                        @endif
                        </td>
                    </tr>
                    @endforeach

                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data").DataTable();
    });
</script>
@endsection