<html>
<head>
<title>SKHP</title>
<style type="text/css">

/* Styling report go here */

.page-header, .page-header-space {
  /* height: 100px; */
  height: 0;
}

.page-footer, .page-footer-space {
  height: 40px;
}

.page-footer {
  position: fixed;
  bottom: 0;
  width: 100%;
  border-top: 1px solid black; /* for demo */
  background: white;
  font-family: Arial;
  font-size:10pt;
  /* margin-left: 8mm; */
}

.page-header {
  position: fixed;
  top: 0mm;
  width: 100%;
  border-bottom: 1px solid black; /* for demo */
  background: white; /* for demo */
}

.page {
  page-break-after: always;
  /* font-family: Helvetica;
   */
  font-family: Arial;
  font-size:11pt;
  padding:8mm 8mm 0 8mm;
  /* border-bottom: 1px solid #999; */
  min-height: 26cm;
  margin-bottom: 2cm;
  /* border: #999 solid 1px; */
}

.title {
    font-family: Arial;
    font-size:14pt;
    text-decoration:underline;
}

.subtitle {
    font-family: Arial;
    font-size:9pt;
    font-style: italic;
}

/* .right {
  position: absolute;
  right: 0px;
  width: 300px;
} */

.right {
  position: absolute;
  right: 0px;
  width: 400px;
}

/* th, td {
  padding-top: 8px;
} */

.table {
  padding-top: 8px;
}
.table th {
  padding-top: 8px;
}
.table td{
 vertical-align: top;
 font-family: Arial;
 font-size:10pt;
  margin: 10px;
  padding-top:10px;
}
.table td .eng {
 vertical-align: top;
 font-family: Arial;
 font-size:8pt;
 font-style:italic;
}

.table1 {
  font-size: 9pt;;
  padding-top: 1px;
}
.table1 th{
  font-family: Arial;
  border: 1px solid #999;
  padding-top:8px;
}
.table1 td{
  border: 1px solid #999;
  font-family: Arial;
  /* border-collapse: collapse; */
  padding-top:1px;
}



.table-border {
    border: 1px solid #999;
    border-collapse: collapse;
}

.table-foot {
    font-family: Arial;
    font-size:11pt;
    padding-top: 6px;
    /* font-weight: bold; */
}

#watermark {
    position: fixed;
    font-size: 150pt;
    font-weight: bold;
    color: #ccc;
    top: 30%;
    width: 100%;
    text-align: center;
    opacity: .6;
    transform: rotate(-10deg);
    transform-origin: 50% 50%;
    z-index: -1000;
}

@media print {
   thead {display: table-header-group;}
   tfoot {display: table-footer-group;}

   button {display: none;}

   .page{border: #fff solid 0px;}

   body {margin: 0;}
}
 /* End of stiling page */
</style>
</head>
<body>
    @if($order->status_approval!="2")
    <div id="watermark">DRAFT</div>
    @endif

    <table border="0">
        <tr>
            <td style="width: 20mm;">
                <img src="{{ $view ? asset('assets/images/logo/logo_kemendag.png') : public_path('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
            </td>
            <td>
                <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN<br />TERTIB NIAGA DIREKTORAT METROLOGI</p>
                <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
            </td>
        </tr>
    </table>

    <table width="100%" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td>
            <!--*** CONTENT GOES HERE ***-->
            <div class="header" style="float: right; height: 30px; padding-right:200px; font-size:18pt;">
            <center>{{ $order->no_service }}</center>
            </div>
            <div class="page">
                <center>
                    <div class="title">SURAT KETERANGAN HASIL PENGUJIAN</div>
                    <div class="subtitle">Certificate of Testing Result</div>
                    Nomor: {{ $order->no_sertifikat }}</p>
                </center>
                <br/><br/>
                <table class="table" width="100%" cellpading="0" cellspacing="0">
                    <tr>
                        <td>Jenis UTTP<div class="eng">Measuring Instrument</div></td>
                        <td>:</td>
                        <td colspan="4">{{ $order->uttp_type_id != null ? $order->uttpType->uttp_type : $order->order->ServiceRequestItem->uttp->type->uttp_type }}</td>
                        <!--
                        <td>Kapasitas</td>
                        <td>:</td>
                        <td>{{ $order->order->ServiceRequestItem->uttp->tool_capacity }}</td>
                        -->
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">Merek<div class="eng">Trade mark</div></td>
                        <td>:</td>
                        <td colspan="4">{{ ($order->tool_brand != null ? $order->tool_brand : $order->order->ServiceRequestItem->uttp->tool_brand) }}</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">Model/Tipe<div class="eng">Model/Type</div></td>
                        <td>:</td>
                        <td colspan="4">{{ ($order->tool_model != null ? $order->tool_model : $order->order->ServiceRequestItem->uttp->tool_model) }}</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">No. Seri<div class="eng">Series Number</div></td>
                        <td>:</td>
                        <td colspan="4">{{ $order->serial_no != null ? $orer->serial_no : $order->order->ServiceRequestItem->uttp->serial_no }}</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">Kapasitas<div class="eng">Capacity</div></td>
                        <td>:</td>
                        <td colspan="4">{{ $order->tool_capacity != null ? $order->tool_capacity : $order->order->ServiceRequestItem->uttp->tool_capacity }} {{ $order->order->ServiceRequestItem->uttp->tool_capacity_unit }}</td>
                    </tr>
                    <!--
                    <tr>
                        <td>Buatan</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->order->ServiceRequestItem->uttp->tool_made_in }}</td>
                    </tr>
                    -->
                    <tr>
                        <td>Pemilik/Pemakai<div class="eng">Owner/User</div></td>
                        <td>:</td>
                        <td colspan="4">{{ $order->label_sertifikat ? $order->label_sertifikat : $order->request->label_sertifikat }}</td>
                    </tr>
                    <tr>
                        <td>Lokasi Alat<div class="eng">Location</div></td>
                        <td>:</td>
                        <td colspan="4">{{ $order->addr_sertifikat ? $order->addr_sertifikat : $order->request->addr_sertifikat  }}</td>
                    </tr>
                    
                    <tr>
                        <td style="vertical-align: top;">Hasil<div class="eng">Result</div></td>
                        <td style="vertical-align: top;">:</td>
                        <td colspan="4">
                        @if($order->order->hasil_uji_memenuhi == 'memenuhi')
                        Disahkan berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal.
                        @else
                        Batal berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal. 
                        @endif
                        </td>
                    </tr>
                </table>
                <br />
                <table class="table" width="100%" cellpading="0" cellspacing="0">
                    <tr>
                        <td colspan="2">SKHP ini terdiri dari 2 (dua) halaman<div class="eng">This certificate consists of 2 (two) pages</div></td>
                    </tr>
                    <tr>
                        <td colspan="2">Catatan:<div class="eng">Notes:</div></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 50px;">1.</td>
                        <td>UTTP ini wajib ditera ulang paling lambat tanggal {{ date('d M Y', strtotime($order->order->sertifikat_expired_at)) }}
                            <div class="eng">This measuring instrument mandatory to reverificate on {{ date('d M Y', strtotime($order->order->sertifikat_expired_at)) }}</div></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 50px;">2.</td>
                        <td>Apabila tanda tera rusak dan/atau kawat segel putus, UTTP ini wajib ditera ulang
                            <div class="eng">If the verification mark is damaged and/or the seal wire is broken, this measuring instrument must be re-verificated</div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 50px;">3.</td>
                        <td>Pemutusan tanda tera hanya dapat dilakukan dengan sepengetahuan Direktorat Metrologi
                            <div class="eng">Breaking of the verification mark can only be done with the agreement of the Directorate of Metrology</div>
                        </td>
                    </tr>
                </table>
                
                <br/>
                <br/>
                <div class="right">
                    <table cellpading="0" cellspacing="0">
                        <tr>
                            <td></td>
                            <td>Bandung, {{ $order->kabalai_date ? date("d M Y", strtotime($order->kabalai_date)) : date("d M Y") }}</td>
                        </tr>
                        <tr>
                            <td>a.n</td>
                            <td>Direktur Metrologi</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Kepala Balai Pengujian UTTP</td>
                        </tr>
                        <tr style="height: 50px;">
                            <td></td>
                            <td>
                                @if($order->KaBalai != null)
                                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(123)->margin(0.5)->generate($qrcode_generator)) !!} ">
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>{{ $order->KaBalai != null ? $order->KaBalai->full_name : '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div class="page">
                <div>DATA PENGUJIAN</div>
                <div class="eng">Testing Data</div>

                <br/><br/>
                <table class="table" width="100%" cellpading="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" style="padding-left: 30px;">Pegawai Berhak</td>
                        <td rowspan="2">:</td>
                        <td>1. {{ $order->order->test_by_1 != null ? $order->order->TestBy1->full_name : '' }}</td>
                        <td>NIP: {{ $order->order->test_by_1 != null ? $order->order->TestBy1->nip : '' }}</td>
                    </tr>
                    <tr>
                        <td>2. {{ $order->order->test_by_2 != null ? $order->order->TestBy2->full_name : '' }}</td>
                        <td>NIP: {{ $order->order->test_by_2 != null ? $order->order->TestBy2->nip : '' }}</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">Tanggal Pengujian</td>
                        <td>:</td>
                        <td colspan="4">
                        @if($order->stat_service_order == 4)
                        {{ date("d M Y", strtotime($order->order->staff_entry_datein)) . ' - ' . date("d M Y", strtotime($order->order->cancel_at)) }}
                        @else
                            {{ date("d M Y", strtotime($order->order->staff_entry_datein)) . ' - ' . date("d M Y", strtotime($order->order->staff_entry_dateout)) }}
                        @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">Lokasi</td>
                        <td>:</td>
                        <td colspan="4">
                        {{ $order->order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'Direktorat Metrologi' : 'Lokasi UTTP Terpasang'}}
                        </td>
                    </tr>
                    @if($order->order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
                    <tr>
                        <td style="padding-left: 30px;">Identitas Badan Hitung</td>
                        <td>:</td>
                        <td colspan="4">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 45px;">Tipe</td>
                        <td>:</td>
                        <td colspan="4">
                            <?php 
                                $tipe_array = $order->order->ttuBadanHitungs->pluck('type')->toArray(); 
                            ?>
                            {{ implode("; ", $tipe_array) }}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 45px;">No Seri</td>
                        <td>:</td>
                        <td colspan="4">
                            <?php 
                                $seri_array = $order->order->ttuBadanHitungs->pluck('serial_no')->toArray(); 
                            ?>
                            {{ implode("; ", $seri_array) }}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">Nomor Tangki</td>
                        <td>:</td>
                        <td colspan="4">
                        {{ $order->order->ttu->tank_no }}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">Nomor Tag</td>
                        <td>:</td>
                        <td colspan="4">
                        {{ $order->order->ttu->tag_no }}
                        </td>
                    </tr>
                    @elseif($order->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM')
                    <tr>
                        <td style="padding-left: 30px;">Identitas Badan Hitung</td>
                        <td>:</td>
                        <td colspan="4">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 45px;">Merek</td>
                        <td>:</td>
                        <td colspan="4">
                            <?php 
                                $merek_array = $order->order->ttuBadanHitungs->pluck('brand')->toArray(); 
                            ?>
                            {{ implode("; ", $merek_array) }}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 45px;">Tipe</td>
                        <td>:</td>
                        <td colspan="4">
                            <?php 
                                $tipe_array = $order->order->ttuBadanHitungs->pluck('type')->toArray(); 
                            ?>
                            {{ implode("; ", $tipe_array) }}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 45px;">No Seri</td>
                        <td>:</td>
                        <td colspan="4">
                            <?php 
                                $seri_array = $order->order->ttuBadanHitungs->pluck('serial_no')->toArray(); 
                            ?>
                            {{ implode("; ", $seri_array) }}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">Nomor Tangki</td>
                        <td>:</td>
                        <td colspan="4">
                        {{ $order->order->ttu->tank_no }}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">Nomor Tag</td>
                        <td>:</td>
                        <td colspan="4">
                        {{ $order->order->ttu->tag_no }}
                        </td>
                    </tr>
                    @elseif($order->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air')
                    <tr>
                        <td style="padding-left: 30px;">Totalisator</td>
                        <td>:</td>
                        <td colspan="4">
                        {{ $order->order->ttu->totalisator }}
                        </td>
                    </tr>
                    @else
                    <tr>
                        <td style="padding-left: 30px;">Totalisator</td>
                        <td>:</td>
                        <td colspan="4">
                        {{ $order->order->ttu->totalisator }}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px;">K Faktor</td>
                        <td>:</td>
                        <td colspan="4">
                        {{ $order->order->ttu->kfactor }}
                        </td>
                    </tr>
                    @endif
                </table>  

                <br/>
                <div>HASIL PENGUJIAN</div>
                <div class="eng">Testing Result</div>
                <br/>

                @if($order->order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
                <table class="table1" width="100%" cellpading="0" cellspacing="0">
                    <tr style="text-align: center; width:2px;">
                        <th rowspan="2">Input Level (mm)</th>
                        <th colspan="2">Kesalahan Penunjukan</th>
                        <th rowspan="2">Kesalahan Histerisis (mm)</th>
                    </tr>
                    <tr style="text-align: center; width:2px;">
                        <th>Kesalahan Naik (mm)</th>
                        <th>Kesalahan Turun (mm)</th>
                    </tr>
                    @foreach($order->order->ttuItems as $ttuItem)
                    <tr>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->input_level }}</td>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error_up }}</td>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error_down }}</td>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error_hysteresis }}</td>
                    </tr>
                    @endforeach
                </table>
                @elseif($order->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM')
                <table class="table1" width="100%" cellpading="0" cellspacing="0">
                    <tr style="text-align: center; width:2px;">
                        <th>Kecepatan Alir/<div class="eng">Flow Rate</div><br/>(L/menit)</th>
                        <th>Kesalahan/<div class="eng">Error</div><br/>(%)<br/>BKD: &#177;0.5%</th>
                        <th>Meter Factor<br/>BKD: 1&#177;0.5%</th>
                        <th>Kemampuan Ulang/<div class="eng">Repeatiblity</div><br/>(%)<br/>BKD: 0.1%</th>
                    </tr>
                    @foreach($order->order->ttuItems as $ttuItem)
                    <tr>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->flow_rate }}</td>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error }}</td>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->repeatability }}</td>
                    </tr>
                    @endforeach
                </table>
                @elseif($order->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air')
                <table class="table1" width="100%" cellpading="0" cellspacing="0">
                    <tr style="text-align: center; width:2px;">
                        <th>Kecepatan Alir/<div class="eng">Flow Rate</div><br/>(L/menit)</th>
                        <th>Kesalahan/<div class="eng">Error</div><br/>(%)<br/>BKD: &#177;0.5%</th>
                        <th>Kemampuan Ulang/<div class="eng">Repeatiblity</div><br/>(%)<br/>BKD: &#177;0.1%</th>
                    </tr>
                    @foreach($order->order->ttuItems as $ttuItem)
                    <tr>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->flow_rate }}</td>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error }}</td>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->repeatability }}</td>
                    </tr>
                    @endforeach
                </table>
                @elseif($order->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas')
                <table class="table1" width="100%" cellpading="0" cellspacing="0">
                    <tr style="text-align: center; width:2px;">
                        <th>Kecepatan Alir/<div class="eng">Flow Rate</div><br/>(m3/h)</th>
                        <th>Kesalahan/<div class="eng">Error</div><br/>(%)</th>
                        <th>Kemampuan Ulang/<div class="eng">Repeatiblity</div><br/>(%)</th>
                    </tr>
                    @foreach($order->order->ttuItems as $ttuItem)
                    <tr>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->flow_rate }}</td>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error }}</td>
                        <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->repeatability }}</td>
                    </tr>
                    @endforeach
                </table>
                @endif

                <br/>
                <div>PEMBUBUHAN TANDA TERA</div>
                <div class="eng">Affixing Verification Mark</div>
                <br/>

                <table class="table" width="100%" cellpading="0" cellspacing="0">
                    <tr>
                        <td style="padding-left: 50px;">1.</td>
                        <td>Pada lemping tanda tera dibubuhkan Tanda Daerah [Ukuran Tanda Daerah, contoh D8] (25), Tanda Pegawai Berhak H4 [list tanda pegawai berhak], contohnya (bc) dan Tanda Sah  ”21” </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 50px;">2.</td>
                        <td>1 (satu) buah buah Tanda Jaminan JP8 dipasang pada kawat yang digunakan untuk menggantungkan lemping tanda tera</td>
                    </tr>
                </table>

                @if($order->order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
                @else
                <br/>
                <div>REKOMENDASI PENGGUNAAN</div>
                <div class="eng">Affixing Verification Mark</div>
                <br/>
                <p>Penggunaan harus dilengkapi saringan dan pemisah udara.
                @endif

                <br/>
                <table class="table-foot" style="border: none;" cellpading="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">Bandung, {{ $order->kabalai_date ? date("d M Y", strtotime($order->kabalai_date)) : date("d M Y") }}</td>
                    </tr>
                    <tr>
                        <td width="60%">Penguji Tipe UTTP</td>
                        <td>Direviu Oleh</td>
                    </tr>
                    <tr>
                        <td>{{ $order->order->test_by_1 != null ? $order->order->TestBy1->full_name : '' }}</td>
                        <td>Sub Koordinator Pengujian Tipe</td>
                    </tr>
                    <tr>
                        <td>{{ $order->order->test_by_2 != null ? $order->order->TestBy2->full_name : '' }}</td>
                        <td>{{ $order->order->supervisor_staff != null ? $order->order->SupervisorStaff->full_name : '' }}</td>
                    </tr>
                    <tr>
                        <td>{!! $order->KaBalai != null ? QrCode::size(120)->margin(0.5)->generate($qrcode_generator) : '' !!}</td>
                    </tr>
                </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
