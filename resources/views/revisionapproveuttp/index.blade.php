@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <!-- <a href="{{ route('requestuttp.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Pendaftaran</a> -->
    <!-- <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a> -->

    <table id="table_data" class="table table-striped table-hover table-responsive-sm">
        <thead>
            <tr>
                <th>No Sertifikat</th>
                <th>Nama Pemilik</th>
                <th>Nama Pemohon</th>
                <th>Persetujuan</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>{{ $row->order ? $row->order->no_sertifikat : '' }}</td>   
                <td>{{ $row->request ? $row->request->label_sertifikat : '' }}</td>
                <td>{{ $row->requestor->full_name }}</td>
                <td>{{ $row->masterstatus->status }}</td>
                <td>
                    @if($row->status_approval == null)
                    <a href="{{ route('revisionapproveuttp.approvesubko', $row->id) }}" class="btn btn-success btn-sm">Persetujuan</a>
                    @elseif($row->status_approval == 1)
                    <a href="{{ route('revisionapproveuttp.approve', $row->id) }}" class="btn btn-success btn-sm">Persetujuan</a>
                    @endif
                </td>
                <td> 
                    @if (!$row->order->is_skhpt)
                    <a href="{{ route('revisiontechorderuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                    <a href="{{ route('revisiontechorderuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                    @endif
                    @if($row->request->service_type_id == 6 || $row->request->service_type_id == 7)
                    <a href="{{ route('revisiontechorderuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                    <a href="{{ route('revisiontechorderuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                    @endif
                    @if($row->order->ujitipe_completed==true || $row->order->has_set)
                    <a href="{{ route('revisiontechorderuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                    <a href="{{ route('revisiontechorderuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data,#table_data_process,#table_data_done").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

    });
</script>
@endsection