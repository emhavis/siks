@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <!-- <a href="{{ route('requestuut.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Pendaftaran</a> -->
    <!-- <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a> -->

    <table id="table_data" class="table table-striped table-hover table-responsive-sm">
        <thead>
            <tr>
                <th>No Sertifikat</th>
                <th>Nama Pemilik</th>
                <th>Nama Pemesan</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>{{ $row->order->no_sertifikat }}</td>
                <td>{{ $row->request->label_sertifikat }}</td>
                <td>{{ $row->requestor->full_name }}</td>
                <td>{{ $row->masterstatus->status }}</td>
                <td>
                    <a href="{{ route('revisionapproveuut.preview', $row->id) }}" class="btn btn-warning btn-sm">Lihat Revisi SKHP</a>
                    <a href="{{ route('revisionapproveuut.print', $row->id) }}" class="btn btn-warning btn-sm">Download Revisi SKHP</a>
                    @if($row->status_approval == null)
                    <a href="{{ route('revisionapproveuut.approvesubko', $row->id) }}" class="btn btn-warning btn-sm">Persetujuan</a>
                    @elseif($row->status_approval == 1)
                    <a href="{{ route('revisionapproveuut.approve', $row->id) }}" class="btn btn-warning btn-sm">Persetujuan</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data,#table_data_process,#table_data_done").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

    });
</script>
@endsection