@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>QR Code<br> <span class="c-white">List</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-check"></i>
            </div>
            <div class="header-title">
                <h3>QR Code</h3>
                <small>
                    Halaman manajemen QR Code.
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
    <?php
    $userid = 'SP2KPWEBAPP';
    // $sViewUrl = '$datarow[0]->view_url';
    $sViewUrl = '/views/2019_08_DB-Gudang-Rev2/DB-Sebaran';
    if ($userid) {
        //echo 'debug: Authenticated on tableau as ', $userid, '<br />';
        $loginparams = 'username='. $userid;
        //$urlparams = ':?iframeSizedToWindow=false&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no';
        $urlparams = ':?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no&:origin=false&:showShareOptions=false&:toolbar=no';
        $url = 'https://analitik.kemendag.go.id/trusted/';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$loginparams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        $ticket = curl_exec ($ch);
        curl_close ($ch);
        // echo 'Ticket:', $ticket,'<br/>';
        echo '<iframe src='.$url.$ticket.$sViewUrl.'?'.$urlparams.'"
                width="1000" height="800" frameborder="0">
        </iframe>';
    } else {
        echo 'Log on to see Tableau content';
    }

    ?>  
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function () {
    });
</script>
@endsection
