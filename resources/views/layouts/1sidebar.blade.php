<aside class="navigation">
    <nav>
        <ul class="nav luna-nav">
            <li class="nav-category">
                Main
            </li>
            <li class="{{ request()->is('home') ? 'active' : '' }}">
                <a href="#">Dashboard</a>
            </li>

            <li>
                <a href="#monitoring" data-toggle="collapse" aria-expanded="false">
                    Monitoring<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="monitoring" class="nav nav-second collapse">
                    <li><a href="#"> Jumlah Standar </a></li>
                    <li><a href="#"> Verifikasi </a></li>
                </ul>
            </li>

            <li class="nav-category">
                Master Data
            </li>
            <li>
                <a href="{{ route('user.index') }}">Pengguna</a>
            </li>
            <li>
                <a href="{{ route('role.index') }}">Grup</a>
            </li>
            <li>
            <a href="#extras" data-toggle="collapse" aria-expanded="false">
                    Standar <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="extras" class="nav nav-second collapse">
                    <li><a href="{{ route('standarddetailtype.index') }}">Rincian Alat</a></li>
                    <li><a href="{{ route('standard.index') }}">Master Standar</a></li>
                </ul>
            </li>
            <li>
                <a href="#extras" data-toggle="collapse" aria-expanded="false">
                    Lokasi <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="extras" class="nav nav-second collapse">
                    <li><a href="profile.html">Provinsi</a></li>
                    <li><a href="contacts.html">Kota/Kabupaten</a></li>
                </ul>
            </li>

            <li class="nav-category">
                Layanan
            </li>
            <li class="{{ request()->is('service/*') || request()->is('service') || request()->is('order/index') || request()->is('order') ? 'active' : '' }}">
                <a href="#verification" data-toggle="collapse" aria-expanded="{{ request()->is('service/*') || request()->is('service') || request()->is('order/index') || request()->is('order') ? 'true' : 'false' }}" class="{{ request()->is('service/*') || request()->is('service') || request()->is('order/index') || request()->is('order') ?  'collapsed' : '' }}">
                    Verifikasi<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="verification" class="nav nav-second collapse {{ request()->is('service/*') || request()->is('service') || request()->is('order/index') || request()->is('order') ? 'in' : '' }}">
                    <li class="{{ request()->is('service/index') || request()->is('service') ? 'active' : '' }}"><a href="{{ route('service.index') }}">Penelusuran</a></li>
                    <li class="{{ request()->is('service/create') || request()->is('service') ? 'active' : '' }}"><a href="{{ route('service.create') }}">Verifikasi Standar</a></li>
                </ul>
            </li>

            <li class="nav-info">
                <div style="margin-left:auto;text-align:center;margin-bottom:10px;">
                    <img id="imgLogo" src="{{ asset('assets/images/logo/logo_v1.0.png') }}" style="width:100px;"/>
                </div>
                <div class="m-t-xs" style="text-align:center;">
                    <span class="c-white">SIKS</span> 
                    <div> Sistem Informasi Ketertelusuran Standar. </div>
                    <div> Copyright © 2019 </div>
                    <div> Kementerian Perdagangan Republik Indonesia.</div>
        
                </div>
            </li>
        </ul>
    </nav>
</aside>