<section class="content">
    <div class="container-fluid">

    	@if($attribute["module"]!="home")
		<div class="row">
		    <div class="col-lg-12">
		        <div class="view-header">
		            <div class="header-icon">
		                <i class="pe page-header-icon pe-7s-users"></i>
		            </div>
		            <div class="header-title">
		                <h3>
		                @if($attribute["menu"]["active"]["submenu"]!=null)
		                {{ $attribute["menu"]["active"]["submenu"] == 'link' ? "Details" :'' }}
		                @else
		                {{ $attribute["menu"]["active"]["mainmenu"] }}
		                @endif
		            	</h3>
		                <small>
		                    Halaman Detail Manajemen Module.
		                </small>
		            </div>
		        </div>
		        <hr>
		    </div>
		</div>
		@endif

        @yield('content')

    </div>
</section>