<style type"text/css"> 
    .badge-notif {
            position:relative;
    }
    .badge-notif[data-badge]:after {
            content:attr(data-badge);
            position:absolute;
            margin-right:3px;
            top:+10px;
            right:0px;
            font-size:.7em;
            background:#f3a709;
            color:white;
            width:18px;
            height:18px;
            text-align:center;
            line-height:18px;
            border-radius: 30%;
    }
    .badge-success[data-badge]:after {
            content:attr(data-badge);
            position:absolute;
            margin-right:3px;
            top:+10px;
            right:0px;
            font-size:.7em;
            background:#3c763d;
            color:white;
            width:18px;
            height:18px;
            text-align:center;
            line-height:18px;
            border-radius: 30%;
    }
</style>
<aside class="navigation">
    <nav>
        <ul class="nav luna-nav">
            <li class="{{ request()->is('home') ? 'active' : '' }}">
                <a href="{{route('home')}}">Dashboard</a>
            </li>
            @foreach($attribute["menu"] as $label=>$route)
            @if($label!="active")
            @if(is_array($route) && isset($route['link']))
                <li class="">
                    <a href="{{ route($route['link']) }}" class="badge-notif" 
                    <?php if(isset($route['nilai']) && $route['nilai'] > 0) { ?> 
                        data-badge="{{is_array($route) ? $route['nilai'] : ''}}" 
                    <?php }else {} ?>
                    >{{ $label }}</a></li>
            @elseif(is_array($route))
                <li 
                {{ $attribute["menu"]["active"]["mainmenu"]==$label?"class=active":"" }}
                >
                    <a href="#{{ str_replace(' ', '', strtolower($label)) }}" data-toggle="collapse" aria-expanded="false">{{ $label }}<span class="sub-nav-icon"> <i class="stroke-arrow"></i></span> </a>
                    <ul id="{{ str_replace(' ', '', strtolower($label)) }}" class="nav nav-second collapse
                    {{ $attribute["menu"]["active"]["mainmenu"]==$label?"in":"" }}
                    ">
                    @foreach($route as $label2 => $route2)
                        @if($route2=="#")
                            <li><a href="#">{{ $label2 }}</a></li>
                        @else     
                            @if(!isset($route2['link']))
                                @if(isset($route2['sub']))
                                <div id="sb">
                                    <li class="menusub">
                                                <a id="sbcollapse" style="font-weight:bold; text-decoration:none">{{ $label2 }} <i class="stroke-arrow"></i></a> 
                                            </li>
                                            <div id="sub1">
                                                @foreach($route2['sub'] as $v)
                                                    <li style="font-weight:bold;padding-left: 10px;margin-top:5px !important; margin-bottom:5px !importannt;"><a href="{{ url('servicehistoryuut')}}/{{$v}}"> {{$v}}</a></li>
                                                @endforeach
                                            </div>
                                </div>
                                @else
                                    <li><a href="<?= url('/') ?>/{{$route2}}">{{ $label2 }}</a></li>
                                @endif
                            @else 
                                @if(is_array($route2) && isset($route2['link']))
                                <li
                                {{ $attribute["menu"]["active"]["submenu"]==$label2?"class=active":"" }}
                                ><a href="<?= url('/') ?>/{{is_array($route2) ? $route2['link'] : ''}}" 
                                class="{{ is_array($route2) && $route2['nilai'] > 0 ? 'badge-notif' : 'badge-success' }}" data-badge="
                                {{is_array($route2) ? $route2['nilai'] : ''}}
                                ">{{ $label2 }}</a></li>
                                @endif
                            @endif
                        @endif
                    @endforeach                    
                    </ul>
                </li>            
            @else
                @if(is_numeric($route))
                <li class=""></li>
                @else
                <li class=""><a href="{{ route($route) }}">{{ $label }}</a></li>
                @endif
            @endif
            @endif
            
            @endforeach
            <li>
                <a href="{{ route('logout') }}">Logout</a>
            </li>

            <li class="nav-info">
                <div style="margin-left:auto;text-align:center;margin-bottom:10px;">
                    <img id="imgLogo" src="{{ asset('assets/images/logo/logo_v1.0.png') }}" style="width:100px;"/>
                </div>
                <div class="m-t-xs" style="text-align:center;">
                    <span class="c-white">SIMPEL UPTP IV</span> 
                    <div> Copyright © 2021 </div>
                    <div> Kementerian Perdagangan Republik Indonesia.</div>
                </div>
            </li>
        </ul>
    </nav>
</aside>