<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <div id="mobile-menu">
                <div class="left-nav-toggle">
                    <a href="#">
                        <i class="stroke-hamburgermenu"></i>
                    </a>
                </div>
            </div>
            <a class="navbar-brand" href="{{route('home')}}">
                SIMPEL
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="left-nav-toggle">
                <a href="">
                    <i class="stroke-hamburgermenu"></i>
                </a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                        <span class="username username-hide-on-mobile">
                            {{ ucfirst(Auth::user()->full_name) }}
                        </span>
                        <i class="fa fa-angle-down"></i>
                        <img src="{{ asset('assets/images/profile.jpg') }}" class="img-circle" alt="">
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <!-- <li><a href="#">Ubah Password</a></li> -->
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>