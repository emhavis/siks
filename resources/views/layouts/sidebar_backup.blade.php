<aside class="navigation">
    <nav>
        <ul class="nav luna-nav">
            <li class="{{ request()->is('home') ? 'active' : '' }}">
                <a href="{{route('home')}}">Dashboard</a>
            </li>
            @if (Auth::user()->user_role==4)
            <li class="{{ request()->is('order')? 'active' : '' }}"><a href="{{ route('order') }}">List Order</a></li>
            <li class="{{ request()->is('service')? 'active' : '' }}"><a href="{{ route('service') }}">Proses Order</a></li>
            <li class="{{ request()->is('stafhistory')? 'active' : '' }}"><a href="{{ route('service.history') }}">History Order</a></li>
            @endif

            @if (Auth::user()->user_role==3)
            <li class="{{ request()->is('kaindex')? 'active' : '' }}"><a href="{{ route('tested') }}">Proses Data</a></li>
            <li class="{{ request()->is('kahistory')? 'active' : '' }}"><a href="{{ route('tested.history') }}">History Data</a></li>
            @endif

            @if (Auth::user()->user_role==6)
            <li class="{{ request()->is('kaditindex')? 'active' : '' }}"><a href="{{ route('finished') }}">Proses Data</a></li>
            <li class="{{ request()->is('kadithistory')? 'active' : '' }}"><a href="{{ route('finished.history') }}">History Data</a></li>
            @endif

            @if (Auth::user()->user_role==7)
            <li class="{{ request()->is('register_standard')? 'active' : '' }}"><a href="{{ route('umlstandard') }}">Register Standard</a></li>
            @endif

            @if (Auth::user()->user_role==8)
            <li class="{{ request()->is('tuindex')? 'active' : '' }}"><a href="{{ route('frontdesk') }}">Report Standard</a></li>
            @endif

            @if (Auth::user()->user_role==2)
            <li>
                <a href="#monitoring" data-toggle="collapse" aria-expanded="false">
                    Monitoring<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="monitoring" class="nav nav-second collapse">
                    <li><a href="#"> Jumlah Standar </a></li>
                    <li><a href="#"> Verifikasi </a></li>
                </ul>
            </li>
            <li class="nav nav-second collapse {{ request()->is('user') 
                    || request()->is('laboratory')
                    || request()->is('role')? 'active' : '' }}">
                <a href="#settinguser" data-toggle="collapse" aria-expanded="false">
                    Setting User<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="settinguser" class="nav nav-second collapse {{ request()->is('user') 
                    || request()->is('laboratory')
                    || request()->is('role')? 'in' : '' }}">
                    <li class="{{ request()->is('user')? 'active' : '' }}"><a href="{{ route('user') }}">Pengguna</a></li>
                    <li class="{{ request()->is('role')? 'active' : '' }}"><a href="{{ route('role') }}">Roles/Group</a></li>
                </ul>
            </li>
            <li class="nav nav-second collapse {{ request()->is('measurementtype')? 'active' : '' }}">
                <a href="#measurementtype" data-toggle="collapse" aria-expanded="false">
                    Master Data<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="measurementtype" class="nav nav-second collapse {{ request()->is('measurementtype')? 'in' : '' }}">
                    <li class="{{ request()->is('measurementtype')? 'active' : '' }}"><a href="{{ route('measurementtype') }}">Master Standard</a></li>
                    <li class="{{ request()->is('laboratory')? 'active' : '' }}"><a href="{{ route('laboratory') }}">Laboratorium</a></li>
                    <li class="{{ request()->is('measurementtype')? 'active' : '' }}"><a href="{{ route('templates') }}">Master Template Result</a></li>
                </ul>
            </li>
            <li class="{{ request()->is('detailtype') 
                || request()->is('umlstandard')
                || request()->is('qrcode')
                || request()->is('standard')? 'active' : '' }}">
                <a href="#extras" data-toggle="collapse" aria-expanded="false">
                    Standar <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="extras" class="nav nav-second collapse {{ request()->is('detailtype') 
                    || request()->is('umlstandard')
                    || request()->is('qrcode')
                    || request()->is('standard')? 'in' : '' }}">
                    <li class="{{ request()->is('detailtype')? 'active' : '' }}"><a href="{{ route('detailtype') }}">Rincian Alat</a></li>
                    <li class="{{ request()->is('qrcode')? 'active' : '' }}"><a href="{{ route('qrcode') }}">QR Codes</a></li>
                    <li class="{{ request()->is('standard')? 'active' : '' }}"><a href="{{ route('standard') }}">Master Standar</a></li>
                    <li class="{{ request()->is('umlstandard')? 'active' : '' }}"><a href="{{ route('umlstandard') }}">UML Register Standar</a></li>
                </ul>
            </li>

            <li class="{{ request()->is('service/index') || request()->is('service')? 'active' : '' }}">
                <a href="#verification" data-toggle="collapse" aria-expanded="false">
                    Verifikasi<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="verification" class="nav nav-second collapse {{ request()->is('service/index') || request()->is('service')? 'in' : '' }}">
                    <li class="{{ request()->is('service/index') || request()->is('service') ? 'active' : '' }}"><a href="{{ route('request') }}">Verifikasi Standar</a></li>
                </ul>
            </li>
            @endif
            <li>
                <a href="{{ route('home.logout') }}">Logout</a>
            </li>

            <li class="nav-info">
                <div style="margin-left:auto;text-align:center;margin-bottom:10px;">
                    <img id="imgLogo" src="{{ asset('assets/images/logo/logo_v1.0.png') }}" style="width:100px;"/>
                </div>
                <div class="m-t-xs" style="text-align:center;">
                    <span class="c-white">SIKS</span> 
                    <div> Sistem Informasi Ketertelusuran Standar. </div>
                    <div> Copyright © 2019 </div>
                    <div> Kementerian Perdagangan Republik Indonesia.</div>
                </div>
            </li>
        </ul>
    </nav>
</aside>