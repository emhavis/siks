<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

<!-- Page title -->
<title>SIMPEL UPTP IV</title>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">

<!-- Vendor styles -->
<link rel="stylesheet" href="{{ asset('assets/vendor/fontawesome/css/font-awesome.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/animate.css/animate.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/toastr/toastr.min.css') }}"/>

<!-- App styles -->
<link rel="stylesheet" href="{{ asset('assets/styles/pe-icons/pe-icon-7-stroke.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/styles/pe-icons/helper.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/styles/stroke-icons/style.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/styles/style.css') }}">


@yield('styles')