<!DOCTYPE html>
<html>
<head>
    @include('layouts.head')
    <script type="text/javascript">
        var base_url = "{{ url('/') }}";
    </script>
</head>
<body>

<!-- Wrapper-->
<div class="wrapper">

    <!-- Nav Header-->
    @include('layouts.navheader')
    <!-- End Nav header-->

    <!-- Navigation-->
    @include('layouts.sidebar')
    <!-- End navigation-->

    <!-- Main content-->
    @include('layouts.body')
    <!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
@include('layouts.footer')

</body>

</html>