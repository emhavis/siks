<script src="{{ asset('assets/vendor/pacejs/pace.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sparkline/index.js') }}"></script>
<script src="{{ asset('assets/vendor/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('assets/vendor/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('assets/vendor/flot/jquery.flot.spline.js') }}"></script>

<!-- App scripts -->
<script src="{{ asset('assets/scripts/luna.js') }}"></script>

<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{ asset('assets/vendor/functions.js') }}"></script>


@yield('scripts')