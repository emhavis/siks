@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">

            <table id="data_table" class="table table-striped table-hover table-responsive-sm data-table">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Nama Pemilik</th>
                        <th>Nama Pemohon</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th>Tgl Gudang</th>
                        <th>Tgl Serah Terima</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequestItem->no_order }}</td>
                        <td>{{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                        <td>{{ $row->ServiceRequest->label_sertifikat }}</td>
                        <td>{{ $row->ServiceRequest->requestor->full_name }}</td>
                        <td>{{ $row->staff_entry_datein }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>{{ $row->warehouse_in_at }}</td>
                        <td>{{ $row->warehouse_out_at }}</td>
                        <td>
                            <button class="btn btn-warning btn-sm btn-mdl" 
                                    data-id="{{ $row->id }}" 
                                    data-noorder="{{ $row->ServiceRequestItem->no_order }}"
                                    data-pemilik="{{ $row->ServiceRequest->label_sertifikat }}"
                                    data-pemohon="{{ $row->ServiceRequest->requestor->full_name }}"
                                    data-requestid="{{ $row->ServiceRequest->id }}"
                                    data-labid="{{ $row->laboratory_id }}"
                                    data-alat="{{ $row->tool_brand . '/' . $row->tool_model . '/' . $row->tool_type . ' (' . ($row->tool_serial_no ? $row->tool_serial_no : '') . ')' }}"
                                    data-nama="{{ $row->warehouse_out_nama }}"
                                    >Lihat</button>
                            <a target="_blank" href="{{ route('warehousehistoryuut.print', $row->id) }}" class="btn btn-warning btn-sm">Bukti Serah Terima</a>
                                    
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Serah Terima Alat ke Pemohon</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <input type="hidden" name="id" id="id"/>
                            <input type="hidden" name="requestid" id="requestid"/>
                            <input type="hidden" name="labid" id="labid"/>
                            <input type="hidden" name="instalasiid" id="instalasiid"/>

                            <div class="form-group">
                                <label>No Order</label>
                                <input type="text" name="order_no" id="order_no" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Alat</label>
                                <input type="text" name="alat" id="alat" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemilik</label>
                                <input type="text" name="pemilik" id="pemilik" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemohon</label>
                                <input type="text" name="pemohon" id="pemohon" class="form-control" readonly required />
                            </div>

                            <div class="form-group">
                                <label>Nama Penerima Alat</label>
                                <input type="text" name="warehouse_out_nama" id="warehouse_out_nama" class="form-control"  readonly required />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function (){
    $('.data-table').DataTable();

    $("button.btn-mdl").click(function(e){
        e.preventDefault();
        var id = $(this).data().id;
        var requestid = $(this).data().requestid;
        var labid = $(this).data().labid;
        var instalasiid = $(this).data().instalasiid;
        var alat = $(this).data().alat;
        var pemilik = $(this).data().pemilik;
        var pemohon = $(this).data().pemohon;
        var noorder = $(this).data().noorder;
        var nama = $(this).data().nama;

        $("#id").val(id);
        $("#requestid").val(requestid);
        $("#labid").val(labid);
        $("#instalasiid").val(instalasiid);
        $("#alat").val(alat);
        $("#pemilik").val(pemilik);
        $("#pemohon").val(pemohon);
        $("#order_no").val(noorder);
        $("#warehouse_out_nama").val(nama);

        var route = "{{ route('laboratory.getbyid', ':id') }}";
        route = route.replace(':id', labid);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#lab_nama").val(res.lab.nama_lab);
                
            });
        },"json");
    });

    var form =  $("#form_modal");


});

</script>
@endsection