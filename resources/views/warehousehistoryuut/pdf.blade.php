<html>
<head>
    <title>KUITANSI</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table th{text-align: center;}
        table th,td{padding: 5px 10px;}
        table.f9 td,tr.f9 td, .f9{font-size: 9pt;}
        table.f10 td,th{font-size: 10pt;}
        table.f11 td{font-size: 11pt;}
        table.f12 td{font-size: 12pt;}
        p.small{
            font-size: 7pt;
        }
    </style>
</head>
<body>
    <center>
        <p class="f9">Sistem Informasi Pelayanan UPTP IV</p>
    </center>

    <div style="page-break-after: always;">
        <table border="0">
            <tr>
                <td style="width: 20mm;">
                    <img src="{{ asset('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
                </td>
                <td>
                    <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN<br />TERTIB NIAGA DIREKTORAT METROLOGI</p>
                    <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
                </td>
            </tr>
        </table>
        <center>
            <h4><b>BUKTI SERAH TERIMA</b></h4>
        </center>
        <?php setlocale(LC_TIME, "id_ID"); ?>
        <table style="width: 100%;" class="f10">
            <tr>
                <td colspan="3" style="text-align: right;"><b>NO. PENDAFTARAN : {{$row->ServiceRequest->no_register}}</b></td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Nomor Order</b></td>
                <td style="width: 80mm;">: {{ $row->ServiceRequestItem->no_order }}</td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Sudah Terima Dari</b></td>
                <td style="width: 80mm;">: Balai Pengelolaan Standar Nasional Satuan Ukuran</td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Jenis Alat</b></td>
                <td style="width: 80mm;">: {{ $row->uut->stdtype->uut_type }}</td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Identitas Alat</b></td>
                <td style="width: 80mm;">: {{ $row->tool_brand }}/{{ $row->tool_model }} (Serial No: {{ $row->serial_no }})</td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Diterima Oleh</b></td>
                <td style="width: 80mm;">: {{ $row->warehouse_out_nama }}</td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Diterima Pada</b></td>
                <td style="width: 80mm;">: {{ $row->warehouse_out_at ? strftime("%e %B %Y %H:%M", strtotime($row->warehouse_out_at)) : '' }}</td>
            </tr>
        </table>
    </div>
</body>
</html>