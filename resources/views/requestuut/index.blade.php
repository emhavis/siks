@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

<style type"text/css"> 
    .badge-notif {
            position:relative;
    }
    .badge-notif[data-badge]:after {
            content:attr(data-badge);
            position:absolute;
            top:-10px;
            right:-10px;
            font-size:.7em;
            background:#f3a709;
            color:white;
            width:18px;
            height:18px;
            text-align:center;
            line-height:18px;
            border-radius: 50%;
    }
    </style>
@endsection

@section('content')
<div class="row">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <a href="{{ route('requestuut.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Pendaftaran</a> -->
    <!-- <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a> -->

    <ul class="nav nav-tabs" role="tablist" id="tabs">
        <li role="presentation" class="active">
            <a href="#booking" aria-controls="booking" role="tab" data-toggle="tab">Booking Hari Ini</a>
        </li>
        <li role="presentation">
            <a href="#booking_all" id="booking_all_tab" aria-controls="booking_all" role="tab" data-toggle="tab">Booking Dalam Kantor</a>
        </li>
        
        <li role="presentation">
            <a href="#frontdesk_pendaftaran" aria-controls="frontdesk_pendaftaran" role="tab" data-toggle="tab">Pendaftaran</a>
        </li>
        <li role="presentation"><a href="#frontdesk_penagihan" aria-controls="frontdesk_penagihan" role="tab" data-toggle="tab" >Invoice</a></li>
        <li role="presentation"><a href="#frontdesk_validasi" aria-controls="frontdesk_validasi" role="tab" data-toggle="tab" >Validasi</a></li>
        <li role="presentation"><a href="#frontdesk_kirim" aria-controls="frontdesk_kirim" role="tab" data-toggle="tab" >Kirim Alat</a></li>
        
        <li role="presentation"><a href="#processing" aria-controls="processing" role="tab" data-toggle="tab">Proses</a></li>
        <li role="presentation"><a href="#done" aria-controls="done" role="tab" data-toggle="tab">Selesai</a></li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="booking">
            <br/>
            <div class="panel panel-filled table-area">
                <div class="panel-heading">                    
                    <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Booking</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Biaya</th>
                                <th>Nama Pemohon</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bookings as $row)
                            <tr>
                                <td>{{ $row->booking_no }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->est_total_price,0,',','.') }}</td>
                                <td>{{ $row->Pic ? $row->Pic->full_name : '' }}</td>
                                <td>
                                    <form class="form_create_request">
                                    {!! Form::hidden('booking_id', $row->id) !!}
                                    <!-- {!! Form::hidden('received_date', $row->id) !!} -->
                                    {!! Form::button('Konfirmasi Booking', ['class' => 'btn btn-w-md btn-accent btn-simpan', 'name' => 'submitbutton', 'value' => 'save']) !!}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="booking_all">
        <br/>            
            <div class="panel panel-filled table-area">
                <div class="panel-heading">                  

                    <table id="table_data_all" class="table table-striped table-hover table-responsive-sm">
                    <thead>
                            <tr>
                                <th>id</th>
                                <th>No Booking</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Order</th>
                                <th>Nama Pemohon</th>
                                <th>Rencana Pengantaran</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- @foreach($bookings_all as $row)
                            <tr>
                                <td>{{ $row->booking_no }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->est_total_price,0,',','.') }}</td>
                                <td>{{ $row->Pic ? $row->Pic->full_name : ''}}</td>
                                <td>{{ date("d-m-Y", strtotime($row->est_arrival_date)) }}</td>
                            </tr>
                            @endforeach -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

       
        <div role="tabpanel" class="tab-pane" id="frontdesk_pendaftaran">
            <br/>
            <a href="{{ route('requestuut.createbooking') }}" class="btn btn-w-md btn-primary" id="btn_create">Konfirmasi Booking</a>

            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    

                    <table id="table_data_pendaftaran" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Biaya</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_pendaftaran as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>
                                @if($row->payment_code==null)
                                xx-xxxx-xx-xxx
                                @endif
                                @if($row->payment_code!=null)
                                {{ $row->no_order }}
                                @endif
                                </td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->total_price,0,',','.') }}</td>
                                <td>{{ $row->requestor ? $row->requestor->full_name : '' }}</td>
                                <td>{{ $row->status->status }}</td>
                                <td>
                                    <a href="{{ route('requestuut.editbooking', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>

                                    <!-- <a href="{{ route('requestuut.destroy', $row->id) }}" class="btn btn-warning btn-sm">Hapus</a> -->
                                    <button class="btn btn-warning btn-sm hapus" 
                                                data-id="{{ $row->id }}" data-noreg={{$row->no_register}}> Hapus </button>
                                        
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="frontdesk_penagihan">
            
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    
                    <table id="table_data_penagihan" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Biaya</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_penagihan as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>
                                @if($row->payment_code==null)
                                xx-xxxx-xx-xxx
                                @endif
                                @if($row->payment_code!=null)
                                {{ $row->no_order }}
                                @endif
                                </td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->total_price,0,',','.') }}</td>
                                <td>{{ $row->requestor ? $row->requestor->full_name : '' }}</td>
                                <td>{{ $row->status->status }}</td>
                                <td>
                                    <a target="_blank" href="{{ route('requestuut.pdf', $row->id) }}" class="btn btn-warning btn-sm">Cetak Invoive/Order</a>
                                    <a href="{{ route('requestuut.payment', $row->id) }}" class="btn btn-warning btn-sm">Kode Billing</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="frontdesk_validasi">            
            <div class="panel panel-filled table-area">
                <div class="panel-heading">

                    <table id="table_data_validasi" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Biaya</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_validasi as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->total_price,0,',','.') }}</td>
                                <td>{{ $row->requestor ? $row->requestor->full_name : '' }}</td>
                                <td>{{ $row->status->status }}</td>
                                <td>
                                    <a href="{{ route('requestuut.valid', $row->id) }}" class="btn btn-warning btn-sm">Validasi Pembayaran</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="frontdesk_kirim">            
            <div class="panel panel-filled table-area">
                <div class="panel-heading"> 
                    <div class="row">
                            <form id="kirimAlatQR"  >
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="no_order">Kirim Alat Berdasar QR Code</label>
                                    {!! Form::text('no_order', null, ['class' => 'form-control','id' => 'no_order']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="error_notes" clsas=""></div>
                            </div>
                            </form>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-danger pull-right" id="refresh_btn">Refresh</button>
                            </div>
                        </div>
                        <div class="row">
                            
                        </div>
                    <br/>                   

                    <table id="table_data_kirim" class="table table-striped table-hover table-responsive-sm">
                        
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>Tanggal Pendaftaran </th>
                                <th>Detail Alat </th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_kirim as $row)
                            <tr>
                            <td>{{ $row->ServiceRequest != null ? $row->ServiceRequest->no_register : '' }}</td>
                            <td>{{ date("d-m-Y",  strtotime($row->ServiceRequest->received_date)) }}</td>
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }}/{{ $row->tool_type}} ({{ $row->tool_serial_no ? $row->tool_serial_no : ''}})<br/>Kapasitas: {{ $row->tool_capacity }} {{ $row->tool_capacity_unit }}</td>
                                <td>{{ $row->ServiceRequestItem->no_order }}</td>
                                <td>{{ $row->ServiceRequest != null ? $row->ServiceRequest->label_sertifikat : ''}}</td>
                                <td>{{ $row->ServiceRequest != null && $row->ServiceRequest->requestor ? $row->ServiceRequest->requestor->full_name : '' }}</td>
                                <td>{{ $row->ServiceRequest != null ? $row->ServiceRequest->status->status : '' }}</td>
                                <td>
                                    {!! Form::button('Cetak Tag', ['class' => 'btn btn-warning btn-sm btn-cetak-tag', 'id' => 'cetak_tag','name' => 'cetak_tag', 'value' => 'save',
                                        'data-id' => $row->id ]) !!}
                                    @if($row->ServiceRequestItem->cetak_tag_created != null)
                                    {!! Form::button('Kirim ke Lab', ['class' => 'btn btn-sm btn-accent btn-kirim-alat', 'id'=>'btn_kirim_alat', 'name' => 'submitbutton', 'value' => 'save',
                                        'data-id' => $row->ServiceRequestItem->id]) !!}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="processing">
            <div class="panel panel-filled table-area">
                <br/>
                <div class="panel-heading">
                   
                    <table id="table_data_process" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>No Pendaftaran</th>
                                <th>Tanggal Pendaftaran </th>
                                <th>Detail Alat </th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <!-- <th></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <!-- @foreach($rows_process as $row)
                            <tr> 
                                <td>{{ $row->serviceRequest != null ? $row->serviceRequest->no_register : '' }}</td>
                                <td>{{ $row->ServiceRequest != null ? $row->ServiceRequest->received_date :'' }}</td>
                                <td>{{ $row->uuts->tool_brand }}/{{ $row->uuts->tool_model }}/{{ $row->uuts->tool_type}} ({{ $row->uuts->serial_no ? $row->uuts->serial_no : ''}})<br/>Kapasitas: {{ $row->uuts->tool_capacity }} {{ $row->uuts->tool_capacity_unit }}</td>
                                <td>{{ $row->no_order }}</td>
                                <td>{{ $row->serviceRequest != null ? $row->serviceRequest->label_sertifikat : ''}}</td>
                                <td>{{ $row->serviceRequest != null && $row->serviceRequest->requestor ? $row->serviceRequest->requestor->full_name : '' }}</td>
                                <td>{{ $row->status->status }}</td>
                                <td>
                                    @if($row->serviceRequest != null && $row->serviceRequest->file_skhp!==null)
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">SKHP</a>
                                    <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">Lampiran SKHP</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="done">
            <div class="panel panel-filled table-area">
                <br/>
                <div class="panel-heading">
                   
                    <table id="table_data_done" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th>Berkas Proses</th>
                                <th>Berkas Hasil</th>
                            </tr>
                        </thead>
                        <tbody>                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>   
</div>
<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Serah Terima Alat ke Pemohon</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <div class="form-group">
                                <input class ="form-control" type="hidden" name="id" id="id"/>
                                <input class="form-control" type="hidden" name="requestid" id="requestid"/>
                                <input class= "form-control" type="hidden" name="labid" id="labid"/>
                                <input type="hidden" name="instalasiid" id="instalasiid"/>
                            </div>
                            <div class="form-group">
                                <label>No Pendaftaran</label>
                                <input type="text" name="noregistrasi" id="noregistrasi" class="form-control" readonly required />
                            </div>

                            <div class="form-group">
                                <label>Alat</label>
                                <input type="text" name="alat" id="alat" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemilik</label>
                                <input type="text" name="pemilik" id="pemilik" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemohon</label>
                                <input type="text" name="pemohon" id="pemohon" class="form-control" readonly required />
                            </div>

                            <div class="form-group">
                                <label>Nama Penerima Alat</label>
                                <input type="text" name="warehouse_out_nama" id="warehouse_out_nama" class="form-control"  required />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">SERAH TERIMA</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Ingin menghapus data ini ?</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal-hapus">
                            <div class="form-group">
                                <input class ="form-control" type="hidden" name="id" id="id"/>
                                <input class="form-control" type="hidden" name="requestid" id="requestid"/>
                                <input class= "form-control" type="hidden" name="labid" id="labid"/>
                                <input type="hidden" name="instalasiid" id="instalasiid"/>
                            </div>
                            <div class="form-group">
                                <label>No Pendaftaran</label>
                                <input type="text" name="no_registrasi" id="no_registrasi" class="form-control" readonly required />
                            </div>

                            
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="hapus" class="btn btn-accent">Hapus</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_pendaftaran tbody").on("click","button.hapus",function(e){
            <?php $id; ?>
            console.log('ini di klick');
            e.preventDefault();
            var iddelete = $(this).data().id;
            var noreg = $(this).data().noreg;
            $('#noregistrasi').val(noreg);
            
            $("#deletemodal").modal().on('shown.bs.modal', function ()
            {
                $('#no_registrasi').val(noreg);

                var route = "{{ route('requestuut.destroy', ':id') }}";
                route = route.replace(':id', iddelete);

                $('#hapus').click(function(){
                    $.get(route,function(response)
                    {
                        
                        console.log(response.status)
                        console.log('{{ route('requestuut.destroy','id') }}');
                        if(response.status == true){
                            $("#deletemodal").modal().hide();
                            toastr["success"](response.messages,"Form Status : Ok");
                            location.reload();
                        }else{
                            toastr["error"](response.messages,"Form Invalid Status : "+response.status);
                        }
                    });
                });
                
            });
            
        });
        // end of code
        $('.btn-simpan').click(function(e){
            
            e.preventDefault();

             $(this).attr('disabled', true);
            toastr["warning"]("Konfirmasi booking sedang diproses","Form Valid");
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';
            
            $.post('{{ route('requestuut.simpanbooking') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuut') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('.btn-cetak-tag').click(function(e){
            e.preventDefault();

            var url = '{{ route("requestuut.tag", ":id") }}';
            url = url.replace(':id', $(this).data("id"));

            window.open(url, '_blank');
            window.focus();
            location.reload();
        });

        $('#frontdesk_kirim #table_data_kirim tbody').on("click",'.btn-kirim-alat', function(e){
            e.preventDefault();

            var url = '{{ route("requestuut.instalasi") }}';
            //url = url.replace(':id', $(this).data("id"));

            location.reload();

            var form_data = 'id=' +$(this).data("id") + '&_token={{ csrf_token() }}';

            $.post(url,form_data,function(response)
            {
                if(response.status===true)
                {
                    //location.reload();
                    //window.location = '{{ route('requestuut') }}'
                    toastr["success"]("Berhasil dikirim. Silakan refresh jika diperlukan.","Form Valid");
                }
                else
                {
                    var msg = show_notice(response.messages);
                    //window.location = '{{ route('requestuut') }}'
                    toastr["error"]("Mohon Periksa kembali","Form Invalid" + response.messages);;
                }
            });
        });

        $("#no_order").change(function(e) {
            
            e.preventDefault();
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuut.kirimqr') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    //location.reload();
                    toastr["success"]("Berhasil dikirim","Form Valid");
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('#refresh_btn').click(function(e) {
            e.preventDefault();
            location.reload();
            $(this).tab('show');
        });
        // TABS
        $('#tabs a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });

        var hash = window.location.hash;
        $('#tabs a[href="' + hash + '"]').tab('show');
        // END TABS

        $("#table_data,#table_data_pendaftaran,#table_data_penagihan,#table_data_validasi,#table_data_kirim,#table_data_process,#table_data_done").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });


        // $('.btn-simpan').click(function(e)
        // {
        //     e.preventDefault();

        //      $(this).attr('disabled', true);
        //     toastr["warning"]("Konfirmasi booking sedang diproses","Form Valid");
            
        //     var form = $(this).parents('form:first');
        //     var form_data = form.serialize();
        //     form_data += '&_token={{ csrf_token() }}';
            
        //     $.post('{{ route('requestuut.simpanbooking') }}',form_data,function(response)
        //     {
        //         if(response.status===true)
        //         {
        //             window.location = '{{ route('requestuut') }}' + '/editbooking/' + response.id;
        //         }
        //         else
        //         {
        //             var msg = show_notice(response.messages);
        //             toastr["error"]("Mohon Periksan kembali","Form Invalid");
        //         }
        //     });
        // });

        $("button.btn-mdl").click(function(e){
            e.preventDefault();
            console.log(e.preventDefault());
            var id = $(this).data().id;
            var requestid = $(this).data().requestid;
            var labid = $(this).data().labid;
            var instalasiid = $(this).data().instalasiid;
            var alat = $(this).data().alat;
            var pemilik = $(this).data().pemilik;
            var pemohon = $(this).data().pemohon;
            var reg_no = $(this).data().noregistrasi;

            $("#id").val(id);
            $("#requestid").val(requestid);
            $("#labid").val(labid);
            $("#instalasiid").val(instalasiid);
            $("#alat").val(alat);
            $("#pemilik").val(pemilik);
            $("#pemohon").val(pemohon);
            $("#noregistrasi").val(reg_no)

            var route = "{{ route('laboratory.getbyid', ':id') }}";
            route = route.replace(':id', labid);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#lab_nama").val(res.lab.nama_lab);
                
            });
        },"json");
    });

    var form =  $("#form_modal");
    form.validate({
        rules: {
            warehouse_out_nama: {
                required: true,
            },
        },
        messages: {
            warehouse_out_nama: 'Nama penerima wajib diisi',
        },
        errorClass: "help-block error",
        highlight: function(e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function(e) {
            $(e).closest(".form-group").removeClass("has-error")
        },
    });

    $("#simpan").click(function(e) {
        e.preventDefault();

        var id = $('#id').val();
        var requestid = $('#requestid').val();
        
        //var route = "{{ route('receiveuttp.proses', ':id') }}";
        //route = route.replace(':id', id);
        //window.location = route;

        // var form = $('#form_modal');
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        $.post('{{ route('warehouseuut.warehouse') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                // location.reload();
            }
            else
            {
                // var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksa kembali","Form Invalid");
            }
        });
        
        $("#prosesmodal").modal('hide');
    });

    /**
     * Perubahan ke serverside masingg - masing tab
     */
    
            
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (event) {
        var tabID = $(event.target).attr('href');
        if ( tabID === '#booking_all' ) {
            let table2 = $('#table_data_all').DataTable( {
                responsive: true,
                processing: true,
                serverSide: true,
                destroy:true,
                searching: {
                    return: true
                },
                ajax: {
                    url: '{{ route('request.booking.all') }}',
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: function(data) {
                        data.cSearch = $("#search").val();
                    }
                },
                order: ['1', 'DESC'],
                pageLength: 50,
                aoColumns: [
                    {
                        data: 'id',
                        "visible": false,
                        "searchable": false
                    },
                    {
                        data: 'booking_no',
                        "searchable": false
                    },
                    {
                        data: 'label_sertifikat',
                        "searchable": false
                    },
                    {
                        data: 'est_total_price',
                        "searchable": false
                    },
                    {
                        data: 'pic.full_name',
                        "searchable": false
                    },
                    {
                        data: 'est_arrival_date',
                        "searchable": false
                    },
                ]
            });
            table2.columns.adjust().responsive.recalc();
        }else if(tabID =='#done'){
           
            
            let table2 = $('#table_data_done').DataTable( {
                responsive: true,
                processing: true,
                serverSide: true,
                destroy:true,
                searching: {
                    return: true
                },
                ajax: {
                    url: '{{ route('request.booking.done') }}',
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: function(data) {
                        data.cSearch = $("#search").val();
                    }
                },
                order: ['1', 'DESC'],
                pageLength: 50,
                aoColumns: [
                    {
                        data: 'service_request.no_register',
                        "visible": true,
                        "searchable": true
                    },
                    {

                        render: function(data,type,row){ 
                            let ret ='';
                            if(row.service_request.payment_code ==null){
                                ret += 'xx-xxxx-xx-xxx'
                            }
                            if(row.service_request.payment_code !=null){
                                ret += row.service_request_item.no_order
                            }
                            
                            return ret;               
                        }
                    },
                    {
                        data: 'service_request.label_sertifikat'},
                    {
                        data: 'service_request.requestor.full_name',
                        "searchable": false
                    },
                    {
                        data: 'service_request.status.status',
                    },
                    {
                        data: 'id',
                        render: function(data, type, row, meta){
                            let link_invoice = '{{ route('requestuut.pdf', "::invid") }}' . replace('::invid', row.service_request_id);
                            let link_order = '{{ route('requestuut.buktiorder', "::odid") }}'. replace('::odid', row.service_request_id);
                            let link_kwitansi = '{{ route('requestuut.kuitansi', "::kkwid") }}'. replace('::kkwid', row.service_request_id);
                            
                            return  '<a target="_blank" href="'+link_invoice +'" class="btn btn-warning btn-sm">Invoice:</a> '+
                                    '<a target="_blank" href="'+link_order+'" class="btn btn-warning btn-sm">Order:</a>' +
                                    '<a target="_blank" href="'+link_kwitansi+'" class="btn btn-warning btn-sm">Kuitansi:</a>';
                        }
                    },
                    {
                        data: 'id',
                        render: function(data,type,row){
                            return  `<a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">SKHP</a>` +
                                    `<a href="{{ route('serviceuut.download', $row->id) }}" class="btn btn-warning btn-sm">Lampiran SKHP</a>`;
                        }
                    },
                ]
            });
            table2.columns.adjust().responsive.recalc();
        }else if(tabID =='#processing'){
            let table2 = $('#table_data_process').DataTable( {
                responsive: true,
                processing: true,
                serverSide: true,
                destroy:true,
                searching: {
                    return: true
                },
                ajax: {
                    url: '{{ route('request.booking.process') }}',
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: function(data) {
                        data.cSearch = $("#search").val();
                    }
                },
                order: ['1', 'DESC'],
                pageLength: 50,
                // searching: true,
                aoColumns: [
                    {
                        data: 'id',
                        "visible": false,
                        "searchable": false
                    },
                    {
                        data: 'service_request.no_register',
                        "searchable": true,
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        data: 'service_request.received_date',
                        "searchable": false,
                        "defaultContent": "<i>Not set</i>"
                    },
                    {
                        render: function(data,type, row){
                            return row.uuts.tool_brand + '/' + row.uuts.tool_model + '/' + row.uuts.tool_type + row.uuts.serial_no + '<br/> Kapasitas: ' + row.uuts.tool_capacity + row.uuts.tool_capacity_unit;
                        },
                        "searchable": true
                    },
                    {
                        data: 'no_order',
                        "searchable": false
                    },
                    {
                        data: 'service_request.label_sertifikat',
                        "searchable": true,
                        "defaultContent": "<i>Not set</i>",
                    },
                    {
                        data: 'service_request.requestor.full_name',
                        "searchable": true,
                        "defaultContent": "<i>Not set</i>",
                    },
                    {
                        data: 'status.status',
                        "searchable": false,
                        "defaultContent": "<i>Not set</i>",
                    },
                    // {
                    //     data:"id",
                    //     render: function(data, type, row, meta){
                    //         console.log(data)
                    //         console.log(row)
                    //         if(row.service_request.file_skhp!==null){
                    //             return `<a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">SKHP</a>` +
                    //                     `<a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">Lampiran SKHP</a>`;
                    //         }else{
                    //             return '-';
                    //         }
                    //         return row.service_request;
                    //     },
                    //     "defaultContent": "<i>Not set</i>"
                    // },
                ]
            });
            table2.columns.adjust().responsive.recalc();
        }
    });
    
    // end table

    });
</script>
@endsection