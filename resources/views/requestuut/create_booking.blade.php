@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        <form id="form_create_request">
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Booking</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="booking_id">No Booking</label>
                            {!! Form::select('booking_id', $bookings, '', ['class' => 'form-control','id' => 'booking_id', 'placeholder' => 'Nomor booking pendaftaran mandiri', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="for_sertifikat">Peruntukan Sertifikat</label>
                            {!! Form::text('for_sertifikat', '', ['class' => 'form-control','id' => 'for_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                    -->
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="label_sertifikat">Label Sertifikat</label>
                            {!! Form::text('label_sertifikat', '', ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Sertifikat</label>
                            {!! Form::text('addr_sertifikat', '', ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>  
                </div>
            </div>
        </div>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pendaftar</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Nama Pendaftar</label>
                            {!! Form::text('pic_name', '', ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Jenis Tanda Pengenal</label>
                            {!! Form::text('id_type_id', '', ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                            {!! Form::text('pic_id_no', '', ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_phone_no">Nomor Telepon</label>
                            {!! Form::text('pic_phone_no', '', ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_email">Alamat Email</label>
                            {!! Form::text('pic_email', '', ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <!--
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="received_date">Tanggal Masuk Alat</label>
                            {!! Form::text('received_date', '', ['class' => 'date form-control','id' => 'receipt_date', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'required']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="estimate_date">Tanggal Perkiraan Selesai</label>
                            {!! Form::text('estimate_date', '', ['class' => 'date form-control','id' => 'estimate_date', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'required']) !!}
                        </div>
                    </div>
                </div>
                -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', '', ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Lanjut</button> 
        </form>
        </div>
    </div>
</div>
</div>

<div id="table_standard_item" style="display: none;">
    <div class="panel panel-filled panel-c-danger">
        <div class="panel-body">
            <div class="row">
                <button class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_remove_standard"><i class="fa fa-minus faa-flash"></i></button>
            </div>
            <div class="row">
                <div id="standard_item">
                    <table id="standard_item_inspeksi" class="table table-responsive-sm input-table">
                        <thead>
                            <tr>
                                <th>Jenis Inspeksi</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Satuan Harga</th>
                                <th>Jumlah Harga</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3"></td>
                                <td>Subtotal: </td>
                                <td><input readonly type="text" name="subtotal[]" id="subtotal" class="form-control" /></td>
                                <td></td>
                            </tr>                            
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        $('#booking_id').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });

        $("#booking_id").change(function(){
            $.get('{{ route('servicebooking.getbyid') }}/'+this.value,function(response){
                $("#for_sertifikat").val(response.for_sertifikat == 'lain' ? "Instansi Lain" : "Sendiri");
                $("#label_sertifikat").val(response.label_sertifikat);
                $("#addr_sertifikat").val(response.addr_sertifikat);

                $("#pic_name").val(response.pic.full_name);
                if (response.pic.kantor == "Perusahaan") {
                    $("#id_type_id").val("NIB");
                    $("#pic_id_no").val(response.pic.nib);
                } else {
                    $("#id_type_id").val("NPWP");
                    $("#pic_id_no").val(response.pic.npwp);
                }
                $("#pic_phone_no").val(response.pic.phone);
                $("#pic_email").val(response.pic.email);
                $("#jenis_layanan").val(response.jenis_layanan);


            });
        });

        $('#btn_simpan').click(function(e){
            e.preventDefault();
            
            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

           
            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuut') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });
    });

</script>
@endsection