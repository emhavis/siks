<html>
<head>
    <title>KUITANSI</title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        /* Set A4 size */
        @media only screen and (min-width: 0px){
            .wrapper {
                width: 80% !important;
            }
        }
        @media only screen and (min-width: 992px){
            .wrapper {
                width: 50% !important;
            }
            /* hide it elsewhere */
        }

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
 
        /* @page {
            size: A4;
            margin: letter;
        } */
 
        /* Set content to fill the entire A4 page */
        /* html,
        body {
            width: 100%;
            height: auto;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
        } */

        @page { 
            margin: 20mm; /* Atur margin sesuai kebutuhan */
            size: A4;
        }
        body { 
            margin: 0;
            font-family: Arial;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
            /* page-break-after: always; */
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
                font-family: Arial, sans-serif;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            @page { 
                margin: 20mm; /* Atur margin sesuai kebutuhan */
                padding: 20mm;
            }
            body { 
                margin: 0; /* Ini bisa tetap 0 untuk body, jika kamu sudah mengatur margin di @page */
            }
            body.A4 { width: 210mm }
        }
 
        /* Style content with shaded background */
        .content {
            width: 100%;
            /* Adjust the width as needed */
            height: auto;
            /* Adjust the height as needed*/
            padding: 20px;
            box-sizing: border-box;
            /* font-family: Arial, sans-serif; */
            /* background-color:#e0e0e0; */
        }
        .list-disc{
            list-style: disc inside;
            margin-left:60px;
            ont-family:"Arial",sans-serif;
        }

        .id2{
            list-style: disc inside;
            margin-left:120px;
            ont-family:"Arial",sans-serif;
        }
        h1 {
            color: white;
            text-align: center;
        }

        p {
            font-family: verdana;
            /* font-size: 20px; */
        }
        .text-sub-title{
            text-indent:-20px;
            margin-left:75px; 
            background-color: lightblue;
        }
        .text-indent{
            text-indent:12px;
            margin-left:75px; 
            background-color: lightblue;
        }
        .wrap{
            margin:0 auto
        }
        .text-bullet-indent{
            text-indent:0px;margin-left:60px;
        }
        .table-indent{
            text-indent:0px;margin-left:70px; padding: 4px; border-collapse: collapse; width: auto;
        }
        .table-indent td, .table-indent th {
        border: 1px solid #ddd;
        padding: 8px;
        }

        .table-indent tr:nth-child(even){background-color: #f2f2f2;}

        .table-indent tr:hover {background-color: #ddd;}

        .table-indent th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #04AA6D;
            color: white;
        }
    </style>
</head>
<body>
   <div class="ml-5 mt-1 wrapper" style="width: 50% !important;" id="wrapper">
       <div style="justify-content: left" class="content">
                {!! $row->MasterLaboratory->kaji_ulang !!}
                <hr class="mt-1 mb-1"/>
                <p class="text-bullet-indet">
                    <label class="text-left" style="font-size:12pt;" for="">Catatan pelanggan: **</label>
                    <p class="table-indent" style="font-size:14pt;">{{$row->ServiceRequestItem->keterangan}}</p>
                </p>
                <hr class="mt-1 mb-1"/>
        </div>
   </div>
</body>
</html>