@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        <form id="form_create_request">
                <div class="panel panel-filled panel-c-danger">
                    <div class="panel-body">
                        <form id="form_item_{{ $request->id }}">
                            {!! Form::hidden('id', $request->id) !!}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis</label>
                                        {!! Form::text('serial_no', $request->id, ['class' => 'form-control','id' => 'serial_no', 'readonly']) !!} 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Nomor Seri</label>
                                        {!! Form::text('serial_no', $request->uttp->serial_no, ['class' => 'form-control','id' => 'serial_no']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Merek</label>
                                        {!! Form::text('tool_brand', $request->uttp->tool_brand, ['class' => 'form-control','id' => 'tool_brand']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Model</label>
                                        {!! Form::text('tool_model', $request->uttp->tool_model, ['class' => 'form-control','id' => 'tool_model']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tipe</label>
                                        {!! Form::text('tool_type', $request->uttp->tool_type, ['class' => 'form-control','id' => 'tool_type']) !!} 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Buatan</label>
                                        {!! Form::text('tool_made_in', $request->uttp->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Kapasitas</label>
                                        {!! Form::text('tool_capacity', $request->uttp->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity']) !!} 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pabrikan</label>
                                        {!! Form::text('tool_factory', $request->uttp->tool_factory, ['class' => 'form-control','id' => 'tool_factory']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Alamat Pabrikan</label>
                                        {!! Form::text('tool_factory_address', $request->uttp->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address']) !!} 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Jumlah Pengujian</label>
                                        {!! Form::text('quantity', $request->quantity, ['class' => 'form-control','id' => 'quantity', 'readonly']) !!} 
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row">
                            <button class="btn btn-warning btn-sm faa-parent animated-hover pull-right btn-save-edit" 
                                data-id="{{ $request->id }}" id="btn_edit_item">
                                <i class="fa fa-save faa-flash"></i> Simpan Alat
                            </button>
                            <a href="{{ route('requestuttp.createbookinginspection', $request->id) }}"
                                class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_add_inspeksi">
                                <i class="fa fa-plus faa-flash"></i> Tambah Uji</a>
                        </div>
                    </div>
            </div>
        </div>

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button> 
        <button type="button" class="btn btn-w-md btn-accent" id="btn_submit">Simpan dan Konfirmasi</button> 
        
        </div>
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        $('#booking_id').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });

        $('#btn_simpan').click(function(e){
            e.preventDefault();
            
            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuttp.simpaneditbooking', $request->id) }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}';
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('#btn_submit').click(function(e){
            e.preventDefault();
            
            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuttp.submitbooking', $request->id) }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}';
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('.btn-save-edit').click(function(e){
            e.preventDefault();
            var id = $(this).data().id;
            
            var form_data = $("#form_item_" + id).serialize();

            form_data += '&_token={{ csrf_token() }}';

            var postroute = "{{ route('requestuttp.edititem', ':id') }}";
            postroute = postroute.replace(':id', id);
            
            $.post(postroute,form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = "{{ route('requestuttp.editbooking', $request->id) }}";
                }
                else
                {
                    //var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });
    });

</script>
@endsection