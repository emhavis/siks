@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <a href="{{ route('inspricerange.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUser" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Inspection Type</th>
                        <th>Harga Terendah</th>
                        <th>Harga Tertinggi</th>
                        <th>Price</th>
                        <th>Units</th>
                        <th>Instalations</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($data as $row)
                    <tr>
                        <td>{{ $row->inspection_type }}</td>
                        <td>{{ $row->min_range }}</td>
                        <td>{{ $row->max_range }}</td>
                        <td>{{ $row->price }}</td>
                        <td>{{ $row->unit}}</td>
                        <td>{{ $row->instalasi_id}}</td>
                        <td>
                            <a title="ubah" href="" class="btn btn-accent btn-sm" style="margin-right:2px;">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a title="ubah" href="" class="btn btn-accent btn-sm" style="margin-right:2px;">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>        
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function () {
    
    $('#tUser').DataTable();

   

});
</script>
@endsection