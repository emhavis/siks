@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row col-md-6">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="inspection_type">Nama Inspeksi</label> 
                        {!!
                            Form::text("inspection_type",$row?$row->inspection_type:'',[
                            'class' => 'form-control',
                            'id' => 'inspection_type',
                            'placeholder' => 'Inspepction Type',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="price">Harga</label> 
                        {!!
                            Form::text("price",$row?$row->price:'',[
                            'class' => 'form-control',
                            'id' => 'price',
                            'placeholder' => 'Price',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_template_id">Nama Template</label> 
                        {!!
                            Form::select("inspection_template_id",
                            $templates,
                            $row?$row->inspection_template_id:'',[
                            'class' => 'form-control',
                            'id' => 'inspection_template_id',
                            'placeholder' => 'Template ',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="service_type_id">Nama Layanan</label> 
                        {!!
                            Form::select("service_type_id",
                            $services,
                            $row?$row->service_type_id:'',[
                            'class' => 'form-control',
                            'id' => 'service_type_id',
                            'placeholder' => 'Nama Layanan ',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="instalasi_id">Nama Instalasi</label> 
                        {!!
                            Form::select("instalasi_id",
                            $instalasi,
                            $row?$row->instalasi_id:'',[
                            'class' => 'form-control',
                            'id' => 'instalasi_id',
                            'placeholder' => 'Nama Instalasi ',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="unit">Satuan</label> 
                        {!!
                            Form::text("unit",$row?$row->unit:'',[
                            'class' => 'form-control',
                            'id' => 'unit',
                            'placeholder' => 'Satuan Unit',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-12">
                        <div class="row col-12">
                            <div class="col-6">
                                <label for="has_range">Range ?</label>    &nbsp; &nbsp; 
                                {!!
                                    Form::checkbox("has_range",$row?$row->has_range:'',[
                                    'class' => 'form-control',
                                    'id' => 'has_range',
                                    'placeholder' => 'Has A Range',
                                    'required',
                                    'checked' => 'false'
                                    ]);
                                !!}
                            </div>
                        </div>
                    </div>
                    <div class="regForm">

                    </div>
                    <div id="addForm">
                    <div class="form-group">
                            <label for="price">Harga Tertinggi</label> 
                            {!!
                                Form::text("max_price",$row?$row->max_price:'',[
                                'class' => 'form-control',
                                'id' => 'max_price',
                                'placeholder' => 'Higest Price',
                                'required'
                                ]);
                            !!}
                        </div>
                        <div class="form-group">
                            <label for="price">Harga Terendah</label> 
                            {!!
                                Form::text("min_price",$row?$row->min_price:'',[
                                'class' => 'form-control',
                                'id' => 'min_price',
                                'placeholder' => 'Lower Price',
                                'required'
                                ]);
                            !!}
                        </div>
                        <div class="form-group">
                            <label for="unit">Interval Satuan</label> 
                            {!!
                                Form::text("range_unit",$row?$row->range_unit:'',[
                                'class' => 'form-control',
                                'id' => 'range_unit',
                                'placeholder' => 'Interval Satuan Unit',
                                'required'
                                ]);
                            !!}
                        </div>
                    </div>
                    
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function ()
{
    document.getElementById('addForm').style.display= 'none';  
    var checkbox = document.querySelector("input[name=has_range]");
    checkbox.checked =''

    checkbox.addEventListener('change', function() {
        if (this.checked) {
            document.getElementById('addForm').style.display= 'block';; 
        } else {
            document.getElementById('addForm').style.display= 'none';  
        }
    });
    $('#template_id').select2({
    });
    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('insprice.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('insprice') }}';
            }
        });

    });
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection