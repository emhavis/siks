@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

@endsection

@section('content')
<div class="row">
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(['url' => route('edit.save')])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <div class="row">
                    <div class="col-md-6">
                        <h4>Input Harga Pengujian</h4>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <!--  -->
                <input type="hidden" name="id" value="{{$id}}"/>
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('price', 'Harga Pengujian', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::text("price",'',[
                                    'class' => 'form-control',
                                    'id' => 'price',
                                    'data-type' => 'currency',
                                    'placeholder' => 'Rp1,000,000.00',
                                    'required'
                                ])
                            }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Lanjut</button> 
        </form>
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        let data = [];
        let js_data = '<?php echo json_encode(array()); ?>';
        let js_obj_data = JSON.parse(js_data );
        
        $('#price').attr('value',<?=$price?>);
        formatCurrency($('#price'));
        $('.select2').select2({
            placeholder: "- Pilih Alat -",
            allowClear: true
        }).on('change', function(){
            $.ajax({
                url:"<?php echo URL::to('') ?>/" + "setting-price-find/" + $(this).val(),
                type:"POST",
                data:{
                    _token:'{{csrf_token()}}',
                },
                success: function(response){
                    js_data = response
                    js_obj_data = js_data;
                },
                error: function (xhr, error, code) {
                    console.log(xhr)
                }
            });
        });

        // Jquery Dependency

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() { 
                formatCurrency($(this), "blur");
            }
        });

        function formatNumber(n) {
            // format number 1000000 to 1,234,567
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        }


        function formatCurrency(input, blur) {
            // appends $ to value, validates decimal side
            // and puts cursor back in right position.
            
            // get input value
            var input_val = input.val();
            
            // don't validate empty input
            if (input_val === "") { return; }
            
            // original length
            var original_len = input_val.length;

            // initial caret position 
            var caret_pos = input.prop("selectionStart");
                
            // check for decimal
            if (input_val.indexOf(",") >= 0) {

                // get position of first decimal
                // this prevents multiple decimals from
                // being entered
                var decimal_pos = input_val.indexOf(",");

                // split number by decimal point
                var left_side = input_val.substring(0, decimal_pos);
                var right_side = input_val.substring(decimal_pos);

                // add commas to left side of number
                left_side = formatNumber(left_side);

                // validate right side
                right_side = formatNumber(right_side);
                
                // On blur make sure 2 numbers after decimal
                if (blur === "blur") {
                    right_side += "00";
                }
                
                // Limit decimal to only 2 digits
                right_side = right_side.substring(0, 2);

                // join number by .
                input_val = "Rp" + left_side + "," + right_side;

            } else {
                // no decimal entered
                // add commas to number
                // remove all non-digits
                input_val = formatNumber(input_val);
                input_val = "Rp" + input_val;
                
                // final formatting
                if (blur === "blur") {
                input_val += ",00";
                }
            }
        
            // send updated string to input
            input.val(input_val);

            // put caret back in the right position
            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }
    });

</script>
@endsection
