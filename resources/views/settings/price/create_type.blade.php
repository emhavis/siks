@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

@endsection

@section('content')
<div class="row">
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(['url' => route('type.save')])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <div class="row">
                    <div class="col-md-6">
                        <h4>Input Tipe Pengujian</h4>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <!--  -->
                <input type="hidden" name="id" value="1"/>
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('uut_type', 'Nama Alat', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::text("uut_type",'',[
                                    'class' => 'form-control',
                                    'id' => 'uut_type',
                                    'placeholder' => 'Nama Pengujian',
                                    'required'
                                ])
                            }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('jangka_waktu', 'Berlaku (Tahun)', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::text("jangka_waktu",'',[
                                    'class' => 'form-control',
                                    'id' => 'jangka_waktu',
                                    'type' => 'number',
                                    'placeholder' => 'jangka waktu',
                                    'required'
                                ])
                            }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('lab_id', 'Laboratory', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::select('lab_id',$lab,1, ['class' => 'form-control select2','id' => 'lab_id', ])
                            }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('unit_id', '', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::select('unit_id',$units,1, ['class' => 'form-control select2','id' => 'unit_id' ])
                            }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('is_active', 'Aktif / Off', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::checkbox('is_active', 1, null)
                            }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Lanjut</button> 
        </form>
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        let data = [];
        let js_data = '<?php echo json_encode(array()); ?>';
        let js_obj_data = JSON.parse(js_data );
        
        $('.select2').select2({
            placeholder: "- Pilih Alat -",
            allowClear: true
        }).on('change', function(){
            $.ajax({
                url:"<?php echo URL::to('') ?>/" + "setting-price-find/" + $(this).val(),
                type:"POST",
                data:{
                    _token:'{{csrf_token()}}',
                },
                success: function(response){
                    js_data = response
                    js_obj_data = js_data;
                    console.log(js_data)
                },
                error: function (xhr, error, code) {
                    console.log(xhr)
                }
            });
        });
    });

</script>
@endsection