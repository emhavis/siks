@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

@endsection

@section('content')
<div class="row">
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(['url' => route('price.save')])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <div class="row">
                    <div class="col-md-6">
                        <h4>Input Harga Pengujian</h4>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <!--  -->
                <input type="hidden" name="id" value="1"/>
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('inspection_type', 'Nama Pengujian', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::text("inspection_type",'',[
                                    'class' => 'form-control',
                                    'id' => 'inspection_type',
                                    'placeholder' => 'Nama Pengujian',
                                    'required'
                                ])
                            }}
                        </div>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('service_type_id', 'Tipe Pengujian', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::select('service_type_id',[1=>'Perifikasi', 2=>'Kalibrasi'],1, ['class' => 'form-control select2','id' => 'service_type_id' ])
                            }}
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('price', 'Harga Pengujian', array('class' => 'control-label', 'data-type' => 'currency')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::text("price",'',[
                                    'class' => 'form-control',
                                    'id' => 'price',
                                    'type' => 'number',
                                    'placeholder' => '00.00',
                                    'required'
                                ])
                            }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('unit', 'Ket. Penujian', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::text("unit",'',[
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'id' => 'unit',
                                    'placeholder' => 'Unit/ Per-Alat',
                                    'required'
                                ])
                            }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('lab_id', 'Laboratory', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::select('lab_id',$lab,1, ['class' => 'form-control select2','id' => 'lab_id', ])
                            }}
                        </div>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('user_type', 'Tipe Pengguna', array('class' => 'control-label')) }}
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            {{
                                Form::select('user_type',['null' => 'Reular', 'UML' => 'UML'],1, ['class' => 'form-control select2','id' => 'user_type', ])
                            }}
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Lanjut</button> 
        </form>
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        let data = [];
        let js_data = '<?php echo json_encode(array()); ?>';
        let js_obj_data = JSON.parse(js_data );
        
        $('.select2').select2({
            placeholder: "- Pilih Alat -",
            allowClear: true
        }).on('change', function(){
            $.ajax({
                url:"<?php echo URL::to('') ?>/" + "setting-price-find/" + $(this).val(),
                type:"POST",
                data:{
                    _token:'{{csrf_token()}}',
                },
                success: function(response){
                    js_data = response
                    js_obj_data = js_data;
                },
                error: function (xhr, error, code) {
                    console.log(xhr)
                }
            });
        });

        // Jquery Dependency

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() { 
                formatCurrency($(this), "blur");
            }
        });

        function formatNumber(n) {
            // format number 1000000 to 1,234,567
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        }


        function formatCurrency(input, blur) {
            var input_val = input.val();
            if (input_val === "") { return; }
            var original_len = input_val.length;
            var caret_pos = input.prop("selectionStart");
                
            // check for decimal
            if (input_val.indexOf(",") >= 0) {
                var decimal_pos = input_val.indexOf(",");
                var left_side = input_val.substring(0, decimal_pos);
                var right_side = input_val.substring(decimal_pos);
                left_side = formatNumber(left_side);
                right_side = formatNumber(right_side);
                if (blur === "blur") {
                    right_side += "00";
                }
                
                right_side = right_side.substring(0, 2);
                
                input_val = "Rp" + left_side + "," + right_side;

            } else {
                input_val = formatNumber(input_val);
                input_val = "Rp" + input_val;
                
                if (blur === "blur") {
                input_val += ",00";
                }
            }
        
            // send updated string to input
            input.val(input_val);

            // put caret back in the right position
            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }

    });

</script>
@endsection
