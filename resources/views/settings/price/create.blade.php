@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

@endsection

@section('content')
<div class="row">
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(['url' => route('save')])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <div class="row">
                    <div class="col-md-6">
                        <h4>Pengaturan Harga Penggujian Alat Ukur</h4>
                    </div>
                    <div class="col-md-6 text-right">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"
                                style="background:transparent">
                                    <span class="username username-hide-on-mobile">
                                       Opsi
                                    </span>
                                    <i class="fa fa-gear"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li class="single-a"><a href="{{route('type.create')}}" style="color:white !important; background:transparent !important">Tambah Pengujian</a></li>
                                    <li class="single-a"><a href="{{route('price.create')}}" style="color:white !important; background:transparent !important">Tambah Harga Penujian</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <!--  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id" value="1"/>
                <div class="row">
                    <div class="col-md-1" style="padding-top:5px;">
                        <div class="form-group">
                            <label for="jenis_uttp">Pilih Alat</label>
                        </div>
                    </div>
                    <div class="col-md-11">
                        <div class="form-group">
                            {!! Form::select('type',$standard,1, ['class' => 'form-control select2','id' => 'type', ]) !!}
                        </div>
                    </div>
                </div>
                <!--  -->
                <div class="row">
                    <div class="col-md-6 nonuml">
                        <!--  -->
                        <table id="table_nonuml" name="table_nonuml" class="table table-responsive-sm input-table">
                            <thead>
                                <tr><h5>Item Pengujian Non UML</h5></tr>
                                <tr>
                                    <th>Pilih</th>
                                    <th>Nama Pengujian</th>
                                    <th>Harga Satuan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <!--  -->
                    </div>
                    <div class="col-md-6">
                        <!--  -->
                        <table id="table_uml" class="table table-responsive-sm input-table uml">
                            <thead>
                                <tr><h5>Item Pengujian UML</h5></tr>
                                <tr>
                                    <th>Pilih</th>
                                    <th>Nama Pengujian</th>
                                    <th>Harga Satuan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <!--  -->
                    </div>
                </div>
                <!--  -->
            </div>
        </div>
        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Lanjut</button> 
        </form>
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        let data = [];
        let js_data = '<?php echo json_encode($tarif); ?>';
        let js_obj_data = JSON.parse(js_data );
        
        $('.select2').select2({
            placeholder: "- Pilih Alat -",
            allowClear: true
        }).on('change', function(){
            $.ajax({
                url:"<?php echo URL::to('') ?>/" + "setting-price-find/" + $(this).val(),
                type:"POST",
                data:{
                    _token:'{{csrf_token()}}',
                },
                success: function(response){
                    js_data = response
                    js_obj_data = js_data;
                    // nonuml
                    $('#table_nonuml').DataTable({
                        processing: true,
                        destroy:true,
                        serverSide: true,
                        dataType: 'JSON',
                        ajax :{
                            url:"<?php echo (route('price.data')) ?>",
                            type:"POST",
                            select: {
                                style: 'multi',
                                selector: 'td:first-child input[type="checkbox"]',
                                className: 'row-selected'
                            },
                            data:{
                                id:1,
                                _token: '{{csrf_token()}}'
                            },
                            success: function(response){
                                data =  JSON.parse(response.data)
                                
                                for(let i =0; i < data.length; i++) {
                                    let ch ='';
                                    const f = js_obj_data.some(item => item.id == data[i].id);
                                    
                                    if(f){
                                        ch ='checked';
                                    }
                                    var body = "<tr>";
                                        body    += "<td> <input type='checkbox' name='ids_nonuml[]' data-id="+ data[i].id +" value='"+ data[i].id +"'"+ch+"/></td>";
                                        body    += "<td>" + data[i].inspection_type + "</td>";
                                        body    += "<td> Rp." + formatNumber(data[i].price) + "</td>";
                                        body    += '<td>';
                                            body    += '<ul class="nav navbar-nav navbar-right" style="padding:0px;">';
                                                body    += '<li class="dropdown dropdown-user" style="padding:0px;">';
                                                    body    += '<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false" style="background:transparent">';
                                                        body    += '<i class="fa fa-pencil"></i>';
                                                    body    += '</a>';
                                                    body    += '<ul class="dropdown-menu dropdown-menu-default">';
                                                        body    += '<li><a href="<?php echo url('/')?>/setting-price-edit/'+data[i].id+'/'+data[i].price+'" style="color:white !important; background:transparent !important">Ubah Harga</a></li>';
                                                    body    += '</ul>';
                                                body    += '</li>';
                                            body    += '</ul>';
                                        body    += '</td>';
                                        body    += "</tr>";
                                        $('#table_nonuml').append(body);            
                                }
                                
                            },
                            error: function (xhr, error, code) {
                                // $('#table_nonuml').DataTable({});
                            }
                        },
                        columns:[
                            {   data: "service_type_id" },
                            {   data: "inspection_type" },
                        ],
                        columnDefs: [
                            // targets may be classes
                            {targets: [0], searchable: false}
                        ],
                    });

                    // tabel uml
                    $('#table_uml').DataTable({
                        processing: true,
                        destroy:true,
                        serverSide: true,
                        dataType: 'JSON',
                        ajax :{
                            url:"<?php echo (route('price.uml')) ?>",
                            type:"POST",
                            select: {
                                style: 'multi',
                                selector: 'td:first-child input[type="checkbox"]',
                                className: 'row-selected'
                            },
                            data:{
                                id:1,
                                _token: '{{csrf_token()}}'
                            },
                            success: function(response){
                                data =  JSON.parse(response.data)
                                
                                for(let i =0; i < data.length; i++) {
                                    let ch ='';
                                    const f = js_obj_data.some(item => item.id == data[i].id);
                                    
                                    if(f){
                                        ch ='checked';
                                    }
                                    var body = "<tr>";
                                        body    += "<td> <input type='checkbox' name='ids_uml[]' data-id="+ data[i].id +" value='"+ data[i].id +"'"+ch+"/></td>";
                                        body    += "<td>" + data[i].inspection_type + "</td>";
                                        body    += "<td> Rp." + formatNumber(data[i].price) + "</td>";
                                        body    += '<td>';
                                            body    += '<ul class="nav navbar-nav navbar-right" style="padding:0px;">';
                                                body    += '<li class="dropdown dropdown-user" style="padding:0px;">';
                                                    body    += '<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false" style="background:transparent">';
                                                        body    += '<i class="fa fa-pencil"></i>';
                                                    body    += '</a>';
                                                    body    += '<ul class="dropdown-menu dropdown-menu-default">';
                                                        body    += '<li><a href="<?php echo url('/')?>/setting-price-edit/'+data[i].id+'/'+data[i].price+'" style="color:white !important; background:transparent !important">Ubah Harga</a></li>';
                                                    body    += '</ul>';
                                                body    += '</li>';
                                            body    += '</ul>';
                                        body    += '</td>';
                                        body    += "</tr>";
                                        $('#table_uml').append(body);            
                                }
                                
                            },
                            error: function (xhr, error, code) {
                                // $('#table_nonuml').DataTable({});
                            }
                        },
                        columns:[
                            {   data: "service_type_id" },
                            {   data: "inspection_type" },
                        ],
                        columnDefs: [
                            // targets may be classes
                            {targets: [0], searchable: false}
                        ],
                    });
                },
                error: function (xhr, error, code) {
                    console.log(xhr)
                }
            });
        });

        function formatNumber(n) {
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        }

         // nonuml
         $('#table_nonuml').DataTable({
            processing: true,
            destroy:true,
            serverSide: true,
            dataType: 'JSON',
            ajax :{
                url:"<?php echo (route('price.data')) ?>",
                type:"POST",
                select: {
                    style: 'multi',
                    selector: 'td:first-child input[type="checkbox"]',
                    className: 'row-selected'
                },
                data:{
                    id:1,
                    _token: '{{csrf_token()}}'
                },
                success: function(response){
                    data =  JSON.parse(response.data)
                    
                    for(let i =0; i < data.length; i++) {
                        let ch ='';
                        const f = js_obj_data.some(item => item.id == data[i].id);
                        
                        if(f){
                            ch ='checked';
                        }
                        var body = "<tr>";
                            body    += "<td> <input type='checkbox' name='ids_nonuml[]' data-id="+ data[i].id +" value='"+ data[i].id +"'"+ch+"/></td>";
                            body    += "<td>" + data[i].inspection_type + "</td>";
                            body    += "<td> Rp." + formatNumber(data[i].price) + "</td>";
                            body    += '<td>';
                                body    += '<ul class="nav navbar-nav navbar-right" style="padding:0px;">';
                                    body    += '<li class="dropdown dropdown-user" style="padding:0px;">';
                                        body    += '<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false" style="background:transparent">';
                                            body    += '<i class="fa fa-pencil"></i>';
                                        body    += '</a>';
                                        body    += '<ul class="dropdown-menu dropdown-menu-default">';
                                            body    += '<li class="full"><a href="<?php echo url('/')?>/setting-price-edit/'+data[i].id+'/'+data[i].price+'" style="color:white !important; background:transparent !important">Ubah Harga</a></li>';
                                        body    += '</ul>';
                                    body    += '</li>';
                                body    += '</ul>';
                            body    += '</td>';
                            body    += "</tr>";
                            $('#table_nonuml').append(body);            
                    }
                    
                },
                error: function (xhr, error, code) {
                    // $('#table_nonuml').DataTable({});
                }
            },
            columns:[
                {   data: "service_type_id" },
                {   data: "inspection_type" },
            ],
            columnDefs: [
                // targets may be classes
                {targets: [0], searchable: false}
            ],
        });

        // tabel uml
        $('#table_uml').DataTable({
            processing: true,
            destroy:true,
            serverSide: true,
            dataType: 'JSON',
            ajax :{
                url:"<?php echo (route('price.uml')) ?>",
                type:"POST",
                select: {
                    style: 'multi',
                    selector: 'td:first-child input[type="checkbox"]',
                    className: 'row-selected'
                },
                data:{
                    id:1,
                    _token: '{{csrf_token()}}'
                },
                success: function(response){
                    data =  JSON.parse(response.data)
                    
                    for(let i =0; i < data.length; i++) {
                        let ch ='';
                        const f = js_obj_data.some(item => item.id == data[i].id);
                        
                        if(f){
                            ch ='checked';
                        }
                        var body = "<tr>";
                            body    += "<td> <input type='checkbox' name='ids_uml[]' data-id="+ data[i].id +" value='"+ data[i].id +"'"+ch+"/></td>";
                            body    += "<td>" + data[i].inspection_type + "</td>";
                            body    += "<td> Rp." + formatNumber(data[i].price) + "</td>";
                            body    += '<td>';
                                body    += '<ul class="nav navbar-nav navbar-right" style="padding:0px;">';
                                    body    += '<li class="dropdown dropdown-user" style="padding:0px;">';
                                        body    += '<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false" style="background:transparent">';
                                            body    += '<i class="fa fa-pencil"></i>';
                                        body    += '</a>';
                                        body    += '<ul class="dropdown-menu dropdown-menu-default">';
                                            body    += '<li class="full"><a href="<?php echo url('/')?>/setting-price-edit/'+data[i].id+'/'+data[i].price+'" style="color:white !important; background:transparent !important">Ubah Harga</a></li>';
                                        body    += '</ul>';
                                    body    += '</li>';
                                body    += '</ul>';
                            body    += '</td>';
                            body    += "</tr>";
                            $('#table_uml').append(body);            
                    }
                    
                },
                error: function (xhr, error, code) {
                    // $('#table_nonuml').DataTable({});
                }
            },
            columns:[
                {   data: "service_type_id" },
                {   data: "inspection_type" },
            ],
            columnDefs: [
                // targets may be classes
                {targets: [0], searchable: false}
            ],
        });

    });

</script>
<style>
    ul.dropdown-menu li a {
        text-decoration: none;
        position: relative;
    }
    ul.dropdown-menu li.full::before {
    /* ul.dropdown-menu li a::before { */
        content: '';
        /* background-color: hsla(196, 61%, 58%, .75) !important; */
        /* background: hsla(196, 61%, 58%, .75) !important; */
        background-color: hsla(45, 79%, 45%, 1);
        position: absolute;
        left: 0;
        bottom: 0px;
        width: 100%;
        height: 8px;
        z-index: -1;
    }
    ul.dropdown-menu li.full:hover::before {
    /* ul.dropdown-menu li a:hover::before { */
        bottom: 0;
        height: 100%;
    }
    ul.dropdown-menu li.full::before {
    /* ul.dropdown-menu li a::before { */
        content: '';
        /* background-color: hsla(196, 61%, 58%, .75) !important; */
        background-color: hsla(45, 79%, 45%, 1);
        position: absolute;
        left: 0;
        bottom: 3px;
        width: 100%;
        height: 8px;
        z-index: -1;
        transition: all .3s ease-in-out;
    }
    /* other one */
    ul.dropdown-menu li.single-a a::before {
        content: '';
        /* background-color: hsla(196, 61%, 58%, .75) !important; */
        /* background: hsla(196, 61%, 58%, .75) !important; */
        background-color: hsla(45, 79%, 45%, 1);
        position: absolute;
        left: 0;
        bottom: 0px;
        width: 100%;
        height: 8px;
        z-index: -1;
    }

    ul.dropdown-menu li.single-a a:hover::before {
        bottom: 0;
        height: 100%;
    }
    ul.dropdown-menu li.single-a a::before {
        content: '';
        /* background-color: hsla(196, 61%, 58%, .75) !important; */
        background-color: hsla(45, 79%, 45%, 1);
        position: absolute;
        left: 0;
        bottom: 3px;
        width: 100%;
        height: 8px;
        z-index: -1;
        transition: all .3s ease-in-out;
    }
    
    /* ul.dropdown-menu li a:hover{
		color: #fff;
        box-shadow: inset 200px 0 0 0 #54b3d6;
    } */
</style>
@endsection