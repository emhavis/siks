@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row col-md-6">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
            <div class="panel-body">
                {!! Form::model($row, ['method' => 'PATCH', 'route' => ['uttpowners.update', $row->id],
                    'id' => 'form_update_owner', 'enctype' => "multipart/form-data"]) !!}
                    <div class="form-group">
                        <label for="inspection_type">Nama</label> 
                        {!!
                            Form::text("nama",$row?$row->nama:'',[
                            'class' => 'form-control',
                            'id' => 'nama',
                            'placeholder' => 'Nama Owner',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_type">NPWP</label> 
                        {!!
                            Form::text("npwp",$row?$row->npwp:'',[
                            'class' => 'form-control',
                            'id' => 'npwp',
                            'placeholder' => 'NPWP Numbers',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_type">NIB</label> 
                        {!!
                            Form::text("nib",$row?$row->nib:'',[
                            'class' => 'form-control',
                            'id' => 'nib',
                            'placeholder' => 'NIB Numbers',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="price">Alamat</label> 
                        {!!
                            Form::text("alamat",$row?$row->alamat:'',[
                            'class' => 'form-control',
                            'id' => 'price',
                            'placeholder' => 'Price',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_template_id">Nama Kota</label> 
                        {!!
                            Form::select("kota_id",
                            $kabupatenkota,
                            $row?$row->kota_id:'',[
                            'class' => 'form-control',
                            'id' => 'kota_id',
                            'placeholder' => 'Kabupaten Kota ',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="price">Email</label> 
                        {!!
                            Form::email("email",$row?$row->email:'',[
                            'class' => 'form-control',
                            'id' => 'email',
                            'placeholder' => 'Email',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="price">Telepon</label> 
                        {!!
                            Form::text("telepon",$row?$row->telepon:'',[
                            'class' => 'form-control',
                            'id' => 'telepon',
                            'placeholder' => 'Telephone Number',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_type">Penanggung Jawab</label> 
                        {!!
                            Form::text("penanggung_jawab",$row?$row->penanggung_jawab:'',[
                            'class' => 'form-control',
                            'id' => 'penanggung_jawab',
                            'placeholder' => 'Penanggung Jawab',
                            'required'
                            ]);
                        !!}
                    </div>
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection