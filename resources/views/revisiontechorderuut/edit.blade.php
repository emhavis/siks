@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">

        {!! Form::open(['id' => 'form_create_request', 'route' => ['revisionorderuttp.simpanorder', $revision->id]]) !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Perbaikan Sertifikat</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_sertifikat">No Sertifikat</label>
                            {!! Form::text('no_sertifikat', $revision->order->no_sertifikat, ['class' => 'form-control','id' => 'no_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', $revision->request ? $revision->request->jenis_layanan : '', ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="uttp_type_id_old">Jenis Alat (Tertulis)</label>
                            {!! Form::text('uttp_type_id_old', $revision->order->ServiceRequestItem->uttp->type->uttp_type, ['class' => 'form-control','id' => 'uttp_type_id_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="uttp_type_id_new">Jenis Alat (Pembetulan)</label>
                            {!! Form::text('uttp_type_id_new', $revision->uttpType != null ? $revision->uttpType->uttp_type : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'uttp_type_id_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_brand_old">Merek (Tertulis)</label>
                            {!! Form::text('tool_brand_old', $revision->order->ServiceRequestItem->uttp->tool_brand, ['class' => 'form-control','id' => 'tool_brand_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_brand_new">Merek (Pembetulan)</label>
                            {!! Form::text('tool_brand_new', $revision->tool_brand != null ? $revision->tool_brand : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_brand_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_model_old">Model (Tertulis)</label>
                            {!! Form::text('tool_model_old', $revision->order->ServiceRequestItem->uttp->tool_model, ['class' => 'form-control','id' => 'tool_model_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_model_new">Model (Pembetulan)</label>
                            {!! Form::text('tool_model_new', $revision->tool_model != null ? $revision->tool_model : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_model_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_type_old">Tipe (Tertulis)</label>
                            {!! Form::text('tool_type_old', $revision->order->ServiceRequestItem->uttp->tool_type, ['class' => 'form-control','id' => 'tool_type_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_type_new">Tipe (Pembetulan)</label>
                            {!! Form::text('tool_type_new', $revision->tool_type != null ? $revision->tool_type : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_type_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_capacity_old">Kapasitas (Tertulis)</label>
                            {!! Form::text('tool_capacity_old', $revision->order->ServiceRequestItem->uttp->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_capacity_new">Kapasitas (Pembetulan)</label>
                            {!! Form::text('tool_capacity_new', $revision->tool_capacity != null ? $revision->tool_capacity : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_capacity_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_made_in_old">Buatan (Tertulis)</label>
                            {!! Form::text('tool_made_in_old', $revision->order->ServiceRequestItem->uttp->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_made_in_new">Buatan (Pembetulan)</label>
                            {!! Form::text('tool_made_in_new', $revision->tool_made_in != null ? $revision->tool_made_in : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_made_in_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_old">Pabrikan (Tertulis)</label>
                            {!! Form::text('tool_factory_old', $revision->order->ServiceRequestItem->uttp->tool_factory, ['class' => 'form-control','id' => 'tool_factory_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_new">Pabrikan (Pembetulan)</label>
                            {!! Form::text('tool_factory_new', $revision->tool_factory != null ? $revision->tool_factory : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_factory_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_address_old">Alamat Pabrikan (Tertulis)</label>
                            {!! Form::textarea('tool_factory_address_old', $revision->order->ServiceRequestItem->uttp->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_address_new">Alamat Pabrikan (Pembetulan)</label>
                            {!! Form::textarea('tool_factory_address_new', $revision->tool_factory_address != null ? $revision->tool_factory_address : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'tool_factory_address_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="label_sertifikat_old">Pemohon (Tertulis)</label>
                            {!! Form::text('label_sertifikat_old', $revision->request ? $revision->request->label_sertifikat : '', ['class' => 'form-control','id' => 'label_sertifikat_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="label_sertifikat_new">Pemohon (Pembetulan)</label>
                            {!! Form::text('label_sertifikat_new', $revision->label_sertifikat != null ? $revision->label_sertifikat : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'label_sertifikat_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="addr_sertifikat_old">Alamat Pemohon (Tertulis)</label>
                            {!! Form::textarea('addr_sertifikat_old', $revision->request ? $revision->request->addr_sertifikat : '', ['class' => 'form-control','id' => 'addr_sertifikat_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="addr_sertifikat_new">Alamat Pemohon (Pembetulan)</label>
                            {!! Form::textarea('addr_sertifikat_new', $revision->addr_sertifikat != null ? $revision->addr_sertifikat : 'Tetap (Tidak Berubah)', ['class' => 'form-control','id' => 'addr_sertifikat_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="others">Lainnya (Catatan untuk perbaikan selain pada halaman pertama SKHP)</label>
                            {!! Form::textarea('others', $revision->others, ['class' => 'form-control','id' => 'others', 'readonly']) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="file_skhp">Lampiran Evaluasi</label>
                            {!! Form::file('file_skhp', null,
                                ['class' => 'form-control','id' => 'file_skhp']) !!}
                        </div>
                    </div>
                </div>

                @if($revision->request)
        
                @if($revision->request->service_type_id == 4 || $revision->request->service_type_id == 5)
        
                @if($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
                <table id="data_table_ttu_alg_badanhitung" class="table table-striped table-hover table-responsive-sm">
                    <thead>
                        <tr colspan="3">Identitas Badan Hitung</tr>
                        <tr>
                            <th>Tipe</th>
                            <th>No Seri</th>
                            <th>
                                <button type="button" id="btn_add_badanhitung" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($badanHitungs as $ttuItem)
                        <tr>
                            <td><input type="text" class="form-control" name="type[]" value="{{ $ttuItem->type }}"/></td>
                            <td><input type="text" class="form-control" name="serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                            <td>
                                <input type="hidden" name="badanhitung_item_id[]" />
                                <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @elseif($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM')
                <table id="data_table_ttu_mabbm_badanhitung" class="table table-striped table-hover table-responsive-sm">
                    <thead>
                        <tr colspan="4">Identitas Badan Hitung</tr>
                        <tr>
                            <th>Merek</th>
                            <th>Tipe</th>
                            <th>No Seri</th>
                            <th>
                                <button type="button" id="btn_add_badanhitung" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($badanHitungs as $ttuItem)
                        <tr>
                            <td><input type="text" class="form-control" name="brand[]" value="{{ $ttuItem->brand }}"/></td>
                            <td><input type="text" class="form-control" name="type[]" value="{{ $ttuItem->type }}"/></td>
                            <td><input type="text" class="form-control" name="serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                            <td>
                                <input type="hidden" name="badanhitung_item_id[]" />
                                <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif

                @if($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tank_no">Nomor Tangki</label>
                            {!! Form::text('tank_no', 
                                $ttu ? $ttu->tank_no : '',
                                ['class' => 'form-control','id' => 'tank_no']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tag_no">Nomor Tag</label>
                            {!! Form::text('tag_no', 
                                $ttu ? $ttu->tag_no : '',
                                ['class' => 'form-control','id' => 'tag_no']) !!}
                        </div>
                    </div>
                </div>
                @elseif($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="totalisator">Totalisator</label>
                            {!! Form::text('totalisator', 
                                null,
                                ['class' => 'form-control','id' => 'totalisator']) !!}
                        </div>
                    </div>
                </div>
                @elseif($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' ||
                    $revision->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' ||
                    $revision->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)'
                )
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tank_no">Totalisator</label>
                            {!! Form::text('totalisator', 
                                null,
                                ['class' => 'form-control','id' => 'totalisator']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kfactor">K Faktor</label>
                            {!! Form::text('kfactor', 
                                null,
                                ['class' => 'form-control','id' => 'kfactor']) !!}
                        </div>
                    </div>
                </div>
                @endif

                @endif

                @endif
            </div>
        </div>

        

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_submit">Perbaiki</button> 
        
        {!! Form::close() !!}

        </div>
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">


</script>
@endsection