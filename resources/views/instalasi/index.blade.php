@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <a href="{{ route('instalasi.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUser" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Nama Instalasi</th>
                        <th>Nama Laboratorium</th>
                        <th>Kuota Online</th>
                        <th>Kuota Offline</th>
                        <th>SLA Tera/Tera Ulang (hari kerja)</th>
                        <th>SLA Pengujian Tipe (hari kerja)</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($instalasi->count())
                    @foreach($instalasi as $role)
                    <tr>
                        <td>{{ ucfirst($role->nama_instalasi) }}</td>
                        <td>{{ $role->lab->nama_lab }}</td>
                        <td>{{ ucfirst($role->quota_online) }}</td>
                        <td>{{ ucfirst($role->quota_offline) }}</td>
                        <td>{{ ucfirst($role->sla_day) }}</td>
                        <td>{{ ucfirst($role->sla_tipe_day) }}</td>
                        <td>
                            <a href="{{ route('instalasi.create', $role->id) }}" class="btn btn-info btn-sm">Edit</a>
                            <a href="#" id="action" data-id="{{ $role->id }}" data-action="delete" class="btn btn-warning btn-sm">Delete</a>
                            {{-- @if($role->is_active!="t")
                            <a href="#" id="action" data-id="{{ $role->id }}" data-action="aktif" class="btn btn-warning btn-sm">Aktif</a>
                            @endif
                            @if($role->is_active=="t")
                            <a href="#" id="action" data-id="{{ $role->id }}" data-action="nonaktif" class="btn btn-warning btn-sm">Non Aktif</a>
                            @endif --}}
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function () {
    
    $('#tUser').DataTable();

    $("a#action").click(function(e)
    {
        e.preventDefault();
        var data = $(this).data();
        data._token = "{{ csrf_token() }}";
        $.post("{{ route('laboratory.action') }}",data,function(response)
        {
            if(response.status==true)
            {
                toastr["info"]("Data berhasil dihapus", "Info");
                setTimeout(function()
                {
                    window.location.href = "{{ route("laboratory") }}";
                },3000);
            }
            else
            {
                toastr["error"](response["message"], "Error");
            }
        });
    });

});
</script>
@endsection