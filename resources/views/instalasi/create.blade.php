@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            
        </div>
        <div class="panel-body">
            <form id="createGroup">
                @if($id)
                <input type="hidden" name="id" value="{{ $id }}">
                @endif
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="nama_instalasi">Nama Instalasi</label> 
                        {!!
                            Form::text("nama_instalasi",$row?$row->nama_instalasi:'',[
                            'class' => 'form-control',
                            'id' => 'nama_instalasi',
                            'placeholder' => 'Nama Instalasi',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-6">
                        <label for="lab_id">Nama Laboratorium</label> 
                        {!! Form::select('lab_id', 
                            $laboratories,
                            $row?$row->lab_id:'', 
                            ['class' => 'form-control', 'id' => 'lab_id', 'placeholder' => '- Pilih Laboratorium -', 'required']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="quota_online">Kuota Online Harian</label> 
                        {!!
                            Form::number("quota_online",$row?$row->quota_online:'',[
                            'class' => 'form-control',
                            'id' => 'quota_online',
                            'placeholder' => 'Kuota Online Harian',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-3">
                        <label for="quota_offline">Kuota Offline Harian</label> 
                        {!!
                            Form::number("quota_offline",$row?$row->quota_offline:'',[
                            'class' => 'form-control',
                            'id' => 'quota_offline',
                            'placeholder' => 'Kuota Offline Harian',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-3">
                        <label for="sla_day">SLA Tera/Tera Ulang(dalam hari kerja)</label> 
                        {!!
                            Form::number("sla_day",$row?$row->sla_day:'',[
                            'class' => 'form-control',
                            'id' => 'sla_day',
                            'placeholder' => 'SLA (dalam hari kerja)',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group col-md-3">
                        <label for="sla_day">SLA Pengujian Tipe (dalam hari kerja)</label> 
                        {!!
                            Form::number("sla_tipe_day",$row?$row->sla_tipe_day:'',[
                            'class' => 'form-control',
                            'id' => 'sla_day',
                            'placeholder' => 'SLA (dalam hari kerja)',
                            'required'
                            ]);
                        !!}
                    </div>
                </div>
                <button type="submit" id="submit" class="btn btn-default">Submit</button>
            </form> 
        </div>
    </div>
    </div>
</div>

@foreach($kajiUlangs as $kajiUlang)
<div class="row">
    <div class="col-md-12">
    <div class="panel panel-filled" id="panel_kaji_ulang_{{ $kajiUlang->id }}">  
        <div class="loader">
            <div class="loader-bar"></div>
        </div>   
        <div class="panel-heading">
            <h4>Formulir Kaji Ulang</h4>
        </div>
        <div class="panel-body">
            <form id="kajiUlang" class="form-kaji-ulang">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $kajiUlang->id }}">
                <input type="hidden" name="instalasi_id" value="{{ $kajiUlang->instalasi_id }}">
                <input type="hidden" name="service_type_id" value="{{ $kajiUlang->service_type_id }}">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="service_type">Jenis Layanan</label> 
                        {!!
                            Form::text("service_type",$kajiUlang->service_type,[
                            'class' => 'form-control',
                            'id' => 'service_type',
                            'required', 'readonly',
                            ]);
                        !!}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="kaji_ulang">Kaji Ulang</label>   
                        {!!
                            Form::textarea('kaji_ulang', $kajiUlang->kaji_ulang,[
                            'class' => 'form-control ckeditor',
                            'id' => 'kaji_ulang_' . $kajiUlang->id,
                            'placeholder' => 'Isi Kaji Ulang',
                            'required'
                            ]);
                        !!}
                    </div>
                </div>

                <button type="submit" id="simpan-kaji-ulang" class="btn btn-default simpan-kaji-ulang">Simpan</button>
            </form>
        </div>
    </div>
    </div>
</div>
@endforeach
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}"></script>
<script>
$(document).ready(function ()
{

    $('#lab_id').select2({
        // placeholder: "- Pilih UML -",
        allowClear: true
    });

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('instalasi.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('instalasi') }}';
            }
        });

    });

    $(".simpan-kaji-ulang").click(function(e)
    {
        e.preventDefault();

        var form = $(this).parents('form:first');
        var id = form.find('input[name=id]').val();

        var editorText = CKEDITOR.instances['kaji_ulang_' + id].getData();        
        form.find('textarea[name=kaji_ulang]').html(editorText);

        console.log(editorText);

        var formData = form.serialize();

        $("#panel_kaji_ulang_" + id).toggleClass("ld-loading");
        $.post('{{ route('instalasi.saveKajiUlang') }}',formData,function(response)
        {
            $("#panel_kaji_ulang_" + id).toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('instalasi') }}';
            }
        });


    });
});

</script>
@endsection