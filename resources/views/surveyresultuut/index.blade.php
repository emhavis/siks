@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<form id="createGroup" action="{{ route('surveyresultuut')}}" method="GET" >
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="survey_id">Survey</label>
                {!! Form::select('survey_id', $surveys, $survey->id, ['class' => 'form-control','id' => 'survey_id']) !!}
            </div> 
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="start_date">Periode</label> 
                <div class="input-group input-daterange" id="dt_range">
                    <input type="text" class="form-control" id="start_date" name="start_date"
                    value="{{ $start != null ? date("d-m-Y", strtotime($start)) : '' }}">
                    <div class="input-group-addon">s/d</div>
                    <input type="text" class="form-control" id="end_date" name="end_date"
                    value="{{ $end != null ? date("d-m-Y", strtotime($end)) : '' }}">
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="surveylokasi_id">Lokasi</label>
                {!! Form::select('lokasi', ['dalam' => 'Dalam Kantor', 'luar' => 'Dinas Luar'], null, ['class' => 'form-control','id' => 'lokasi']) !!}
            </div> 
        </div>
        <div class="col-md-2">
            {!! Form::submit('Filter', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_filter']) !!}
        </div>
        <div class="col-md-2 text-right">
            <button class="btn btn-success" id="btn1">Export Data</button>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md-12">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Layanan</th>
                        <th>Lokasi Pengujian</th>
                        <th>No Order</th>
                        @foreach($questions as $q)
                        @if($q->question_type == 'file')
                        <th>Pertanyaan {{ $q->sequence }}: {{ $q->question }}</th>
                        @else
                        <th>Pertanyaan {{ $q->sequence }}: {{ $q->question }}</th>
                        @endif
                        @endforeach
                        <th>Petugas 1</th>
                        <th>Petugas 2</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row['created_at'] }}</td>
                        <td>{{ $row['jenis_layanan'] }}</td>
                        <td>{{ $row['lokasi_pengujian'] }}</td>
                        <td>{{ $row['no_order'] }}</td>
                        @foreach($questions as $q)
                        @if($q->question_type == 'file')
                        <td>
                            @if($row['input_value_' . $q->id] != null)
                        <a href="<?= config('app.siks_url') ?>/survey/download/{{ $row['input_id'] }}/{{ $q->id }}" >{{ $row['input_value_' . $q->id]}}</a>
                            @endif
                        </td>
                        @else
                        <td>{{ $row['input_value_' . $q->id]}}</td>
                        @endif
                        @endforeach
                        <td>{{ $row['petugas_1'] }}</td>
                        <td>{{ $row['petugas_2'] }}</td>
                    </tr>
                    @endforeach                
                </tbody>
            </table>
        </div>        
    </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>

<script>
$(document).ready(function()
{
    $('#survey_id').select2();
    $('#lokasi').select2();
    $('.input-daterange input').each(function() {
        $(this).datepicker({
            format:"dd-mm-yyyy",
        });
    });

    $('#data_table').DataTable({
        scrollX: true
    });

    $("#btn1").on('click',function(e) {
        // console.log('console');
        var _token = $('#_token').val();
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $('#createGroup');
        console.log(form.serialize());

        var route = "{{route('surveyresultuut.export')}}?" + form.serialize();
        location.assign(route);

    });
});
</script>
@endsection