
<table id="data_table" class="table table-striped table-hover table-responsive-sm">
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Layanan</th>
            <th>Lokasi</th>
            <th>No Order</th>
            @foreach($questions as $q)
            <th>Pertanyaan {{ $q->sequence }}</th>
            @endforeach
            <th>Petugas 1</th>
            <th>Petugas 2</th>
        </tr>
    </thead>
    <tbody>
        @foreach($rows as $row)
        <tr>
            <td>{{ $row['created_at'] }}</td>
            <td>{{ $row['jenis_layanan'] }}</td>
            <td>{{ $row['lokasi_pengujian'] }}</td>
            <td>{{ $row['no_order'] }}</td>
            @foreach($questions as $q)
            <td>{{ $row['input_value_' . $q->id]}}</td>
            @endforeach
            <td>{{ $row['petugas_1'] }}</td>
            <td>{{ $row['petugas_2'] }}</td>
        </tr>
        @endforeach                
    </tbody>
</table>