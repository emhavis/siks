@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
<style>
    input[type="checkbox"] {
        accent-color: #4CAF50;
        /* Green color */
    }
    input.largerCheckbox {
        width: 40px;
        height: 20px;
        accent-color: #4CAF50;
    }
    
    /* Hide the browser's default checkbox */
    .container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    }

    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
    background-color: #ccc;
    }

    /* When the checkbox is checked, add a blue background */
    .container input:checked ~ .checkmark {
    background-color: #2196F3;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }

    /* Show the checkmark when checked */
    .container input:checked ~ .checkmark:after {
    display: block;
    }

    /* Style the checkmark/indicator */
    .container .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
    }
</style>
@endsection

@section('content')
<!--
<dic class="row">
    <div class="col-6">
            <div class="input-group rounded">
                <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                aria-describedby="search-addon" />
                <span class="input-group-text border-0" id="search-addon">
                    <i class="fas fa-search"></i>
                </span>
            </div>
        </div>
</div>
-->
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-body">
            <form id="createGroup" action="{{ route('resumespuhuut')}}" method="GET" >
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="year">Tahun</label>
                            <input type="number" class="form-control" id="year" name="year" min="2024" max="{{ date('Y') }}"
                                value="{{ $year != null ? $year : date('Y') }}">
                        </div> 
                    </div>
                    
                    <div class="col-md-2">
                        {!! Form::submit('Filter', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_filter']) !!}
                    </div>
                    <div class="col-md-2 text-right">
                        <button class="btn btn-success" id="btn1">Export Data</button>
                    </div>

                </div>
                
                <div class="row">
                    
                </div>
            <!-- </form>    -->
            <br/> 
            <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th rowspan="2" style="vertical-align:top !important;">Select</th>
                        <th rowspan="2" style="vertical-align:top !important;">No Surat Tugas</th>
                        <th rowspan="2" style="vertical-align:top !important;">No Order</th>
                        <th rowspan="2" style="vertical-align:top !important;">No Pendaftaran</th>
                        <th rowspan="2" style="vertical-align:top !important;">Provinsi</th>
                        <th rowspan="2" style="vertical-align:top !important;">Nama Pemilik</th>
                        <th rowspan="2" style="vertical-align:top !important;">Nama Pemohon</th>
                        <th rowspan="2" style="vertical-align:top !important;">Penanggung Jawab</th>
                        <th rowspan="2" style="vertical-align:top !important;">Mulai Penugasan</th>
                        <th rowspan="2" style="vertical-align:top !important;">Selsai Penugasan</th>
                        <th rowspan="2" style="vertical-align:top !important;">Jumlah Hari</th>
                        <th rowspan="2" style="vertical-align:top !important;">Penguji/Pemeriksa 1</th>
                        <th rowspan="2" style="vertical-align:top !important;">Penguji/Pemeriksa 2</th>
                        <th rowspan="2" style="vertical-align:top !important;">Jumlah Penguji</th>
                        <th rowspan="2" style="vertical-align:top !important;">SBM SPUH</th>
                        <th rowspan="2" style="vertical-align:top !important;">Nominal SPUH</th>
                        <th colspan="3" class="text-center">Status Doc.</th>
                        <th rowspan="2" style="vertical-align:top !important;"></th>
                    </tr>
                    <tr>
                        <!-- <th></th> -->
                        <!-- <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th> -->
                        <th style="background-color:red !important;">SPUH</th>
                        <th style="background-color:green !important;">Valid</th>
                        <th style="background-color:blue !important;">TF</th>
                        <!-- <th></th> -->
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>
                            <input type="checkbox" name="id_selected[]" value="{{ $row->id }}"/>
                        </td>
                        <td>{{ $row->doc_no }}</td>
                        <td>{{ $row->no_order }}</td>
                        <td>{{ $row->no_register }}</td>
                        <td>{{ $row->provinsi }}</td>
                        <td>{{ $row->label_sertifikat }}</td>
                        <td>{{ $row->full_name }}</td>
                        <td>
                            {{ ($row->keuangan_perusahaan ?? ' ') . "-" . ($row->keuangan_pic ?? ' ') . "-" . ($row->keuangan_jabatan ?? ' ') ."-". ($row->keuangan_hp ?? ' ') }}
                        </td>
                        <td>{{ date("d-m-Y", strtotime($row->date_from)) }} </td> 
                        <td>{{ date("d-m-Y", strtotime($row->date_to)) }}</td>
                        <td>
                            <?php
                                $dt1 = new DateTime($row->date_from);
                                $dt2 = new DateTime($row->date_to);
                                $dt2 = $dt2->modify("+1 day");
                                $interval = $dt1->diff($dt2);
                            ?>
                            {{ $interval->format('%a') }}
                        </td>
                        <td>{{ $row->nama_petugas_1 }} ({{ $row->nip_petugas_1 ?? '-' }})</td>
                        <td>{{ $row->nama_petugas_2 }} ({{ $row->nip_petugas_2 ?? '-' }})</td>
                        <td>{{$row->spuh_staff}}</td>
                        <td>{{$row->spuh_rate}}</td>
                        <td>{{ number_format($row->price, 2, ',', '.') }}</td>
                        <td>
                            <input type="checkbox" id="p{{$row->id}}" name="p{{$row->id}}" class="largerCheckbox" {{$row->spuh_payment_date != null ? 'checked' : '' }} disabled
                            {{ $row->spuh_payment_date != null ? printf("class='text-success'") : ''}}>
                        </td>
                        <td>
                            <input type="checkbox" id="v{{$row->id}}" name="v{{$row->id}}" class="largerCheckbox" {{$row->valid != null ? 'checked' : '' }} disabled 
                            {{ $row->valid != null ? 'class="text-success"' : ''}}>
                        </td>
                        <td>
                            <input type="checkbox" id="{{$row->id}}" name="{{$row->id}}" class="largerCheckbox" {{$row->is_staff_paid == true ? 'checked' : '' }} disabled 
                            {{ $row->is_staff_paid == true ? 'class="text-success"' : ''}}>
                        </td>
                        <td>
                            @if($row->is_staff_paid != true)
                                <a id="btn_paid" name="btn_paid" class="btn btn-warning btn-sm btn-mdl"
                                    data-id="{{ $row->request_id }}" 
                                    data-state="true"
                                >Paid</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </form> 
        </div>
    </div>  
</div>
<div class="modal fade" id="modalpaid" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Informasi</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <input type="hidden" name="id" id="id"/>
                            <input type="hidden" name="state" id="state"/>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">Ya</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#btn1").on('click',function(e) {
            // console.log('console');
            var _token = $('#_token').val();
            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $('#createGroup');
            console.log(form.serialize());
            // form.append("_token", _token);
            /*
            $.ajax({
                type: "GET",
                url: "{{route('file-export')}}",
                data: form.serialize(), // serializes the form's elements.
                success: function(data)
                {
                    console.log(data); // show response from the php script.
                }
            });
            */
           var route = "{{route('_resume-spuh-export')}}?" + form.serialize();
           location.assign(route);

        });


        // data table will generated
        var table = $('#table_data').DataTable({
            scrollX : true,
        });

        $('#table_data tbody').on( 'click', 'a.btn-mdl', function (e) {
            e.preventDefault();
            var id = $(this).data().id;
            var dstate = $(this).data().state;
            $("#id").val(id);
            $("#state").val(dstate);
            console.log(id);

            var route = "{{ url('resumespuhuut/paid') }}/"+id+ '/' + dstate ;

            $.get(route, function(res){
                console.log(res);
                if(res[1]=='success')
                    {
                        toastr["success"]("Data berhasil diubah", "Success");
                        setTimeout(function()
                        {
                            window.location.href = "{{url('resumespuhuut')}}";
                        },1000);
                    }
                // $("#modalpaid").modal().on('shown.bs.modal', function ()
                // {
                //     $("#lab_nama").val(res.lab.nama_lab);
                //     $('#instalasi_id').empty();
                // });
            },"json");
            
        });
    });
</script>
@endsection