<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Keterangan Hasil Pengujian</title>
    
    <link rel="stylesheet" href="{{ $view ? asset('assets/vendor/papercss/paper.min.css') : public_path('assets/vendor/papercss/paper.min.css') }}">
    <link rel="stylesheet" href="{{ $view ? asset('assets/vendor/bootstrap4/css/bootstrap.min.css') : public_path('assets/vendor/bootstrap4/css/bootstrap.min.css') }}">
    
    <!--
    <link rel="stylesheet" href="{{ $view ? asset('assets/vendor/papercss/paper.min.css') : public_path('assets/vendor/papercss/paper.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
-->
    <style>
        
        body {
            font-family: Arial;
            font-size: 10pt;
            line-height: 1.5;
        }
        .margin-page {
            position: absolute;
            margin: 35mm 25.4mm 25.4mm 25.4mm;
            top: 0px;
        }
        .no-margin-page {
            position: absolute;
            margin: 25.4mm;
        }
        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }

        .letterhead {
            position: relative;
            z-index: -1000;
        }

        @page { 
            size: A4;
        }
    </style>
</head>

<body class="A4">

    <section class="sheet container container-fluid px-0 letterhead"> 
        <div class="has-bg-img" >
            <img src="{{ $view ? asset('assets/images/logo/letterhead-draft.png') : public_path('assets/images/logo/letterhead-draft.png') }}" 
                class="img-fluid" />
        </div>

        <div class="margin-page">
            <div class="float-right" style="font-size:18pt;">{{ $order->no_service }}</div><br/>

            <div class="text-center">
                <div class="title"><strong>SURAT KETERANGAN HASIL PENGUJIAN</strong></div>
                <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
            </div>

            <br/><br/>
            <div class="row">
                <div class="col-4 pr-0">Jenis UTTP</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</div>
            </div>
            <div class="row">
                <div class="col-4 pr-0">Merek/Tipe</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequestItem->uttp->tool_brand . '/' . $order->ServiceRequestItem->uttp->tool_model }}</div>
            </div>
            @if($order->ServiceRequestItem->uttp->tool_media != null && $order->ServiceRequestItem->uttp->tool_media != '')
            <div class="row">
                <div class="col-4 pr-0">Media Uji/Komoditas</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequestItem->uttp->tool_media }}</div>
            </div>
            @endif
            <div class="row">
                <div class="col-4 pr-0">Kapasitas Maksimum</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequestItem->uttp->tool_capacity }} {{ $order->ServiceRequestItem->uttp->tool_capacity_unit }}</div>
            </div>
            @if($order->ServiceRequestItem->uttp->tool_capacity_min > 0)
            <div class="row">
                <div class="col-4 pr-0">Kapasitas Minimum</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequestItem->uttp->tool_capacity_min }} {{ $order->ServiceRequestItem->uttp->tool_capacity_unit }}</div>
            </div>
            @endif
            @if($order->kelas_keakurasian != null && $order->kelas_keakurasian != '')
            <div class="row">
                <div class="col-4 pr-0">Kelas Keakurasian</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->kelas_keakurasian }}</div>
            </div>
            @endif
            @if($order->daya_baca != null && $order->daya_baca != '')
            <div class="row">
                <div class="col-4 pr-0">Daya Baca</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->daya_baca }}</div>
            </div>
            @endif
            @if($order->interval_skala_verifikasi != null && $order->interval_skala_verifikasi != '')
            <div class="row">
                <div class="col-4 pr-0">Interval Skala Verifikasi</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->interval_skala_verifikasi }}</div>
            </div>
            @endif
            @if($order->kelas_single_axle_load != null && $order->kelas_single_axle_load != '')
            <div class="row">
                <div class="col-4 pr-0">Kelas <i>Single Axle Load</i></div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->kelas_single_axle_load }}</div>
            </div>
            @endif
            @if($order->kelas_single_group_load != null && $order->kelas_single_group_load != '')
            <div class="row">
                <div class="col-4 pr-0">Kelas <i>Single Group Load</i></div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->kelas_single_group_load }}</div>
            </div>
            @endif
            @if($order->metode_pengukuran != null && $order->metode_pengukuran != '')
            <div class="row">
                <div class="col-4 pr-0">Metode Pengukuran</i></div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->metode_pengukuran }}</div>
            </div>
            @endif
            @if($order->sistem_jaringan != null && $order->sistem_jaringan != '')
            <div class="row">
                <div class="col-4 pr-0">Sistem Jaringan</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->sistem_jaringan }}</div>
            </div>
            @endif
            @if($order->konstanta != null && $order->konstanta != '')
            <div class="row">
                <div class="col-4 pr-0">Konstanta</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->konstanta }}</div>
            </div>
            @endif
            <div class="row">
                <div class="col-4 pr-0">Buatan</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequestItem->uttp->tool_made_in }}</div>
            </div>
            <div class="row">
                <div class="col-4 pr-0">Nama Pabrikan</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequestItem->uttp->tool_factory }}</div>
            </div>
            <div class="row">
                <div class="col-4 pr-0">Alamat Pabrikan</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequestItem->uttp->tool_factory_address }}</div>
            </div>
            <div class="row">
                <div class="col-4 pr-0">Pemohon</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequest->label_sertifikat }}</div>
            </div>
            <div class="row">
                <div class="col-4 pr-0">Alamat Pemohon</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequest->addr_sertifikat }}</div>
            </div>
            <div class="row">
                <div class="col-4 pr-0">Diuji Oleh</div>
                <div class="col-auto px-0">: </div>
                <div class="col">{{ $order->TestBy1 != null ? $order->TestBy1->full_name : '' }}</div>
                <div class="col-auto">NIP</div>
                <div class="col-auto px-0">:</div>
                <div class="col">123131313</div>   
            </div>
            <div class="row">
                <div class="col-4 pr-0">&nbsp;</div>
                <div class="col-auto px-0">&nbsp;</div>
                <div class="col">{{ $order->TestBy2 != null ? $order->TestBy2->full_name : '' }}</div>
                <div class="col-auto">NIP</div>
                <div class="col-auto px-0">:</div>
                <div class="col">123131313</div>   
            </div>
            <div class="row">
                <div class="col-4 pr-0">Waktu Pengujian</div>
                <div class="col-auto px-0">:</div>
                <div class="col">
                    @if($order->stat_service_order == 4)
                        {{ date("d F Y", strtotime($order->staff_entry_datein)) . ' - ' . date("d F Y", strtotime($order->cancel_at)) }}
                    @else
                        {{ date("d F Y", strtotime($order->staff_entry_datein)) . ' - ' . date("d F Y", strtotime($order->staff_entry_dateout)) }}
                    @endif  
                </div>
            </div>
            <div class="row">
                <div class="col-4 pr-0">Dasar Pengujian</div>
                <div class="col-auto px-0">:</div>
                <div class="col">Surat Permohonan dari: {{ $order->ServiceRequest->label_sertifikat }}
                    <br/>
                    Nomor: {{ $order->ServiceRequestItem->reference_no }}
                    <br/>
                    Tanggal: {{ date("d F Y", strtotime($order->ServiceRequestItem->reference_date)) }}
                </div>
            </div>
            <div class="row">
                <div class="col-4 pr-0">Persyaratan Teknis</div>
                <div class="col-auto px-0">:</div>
                <div class="col">{{ $order->ServiceRequestItem->uttp->type->syarat_teknis }}</div>
            </div>
            <div class="row">
                <div class="col-4 pr-0">Hasil</div>
                <div class="col-auto px-0">:</div>
                <div class="col">
                    @if($order->cancel_at != null)
                    Pengujian tidak dapat dilakukan atau dilanjutkan karena {{ $order->cancel_notes }}.
                    @else
                    <strong>{{ $order->ServiceRequestItem->uttp->type->uttp_type }}</strong> Merek <strong>{{ $order->ServiceRequestItem->uttp->tool_brand }}</strong> tipe <strong>{{ $order->ServiceRequestItem->uttp->tool_model }}</strong> dinyatakan <strong>{{ $order->hasil_uji_memenuhi ? 'Memenuhi' : 'Tidak Memenuhi'}} Syarat Teknis</strong>,
                    untuk jenis-jenis pengujian terlampir yang merupakan bagian tidak terpisahkan dari Surat Keterangan Hasil Pengujian ini. 
                    @endif
                </div>
            </div>
            <br/>
            <div class="row justify-content-end">
                <div class="col-5">
                    <div class="row">
                        <div class="col">Bandung, {{ $order->kabalai_date ? date("d F Y", strtotime($order->kabalai_date)) : date("d F Y") }}</div>
                    </div>
                    <div class="row">
                        <div class="col">Kepala Balai Pengujian UTTP,</div>
                    </div>
                    <div class="row">
                        <div class="col" style="height: 50px;">
                            @if($order->KaBalai != null)
                            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(123)->margin(0.5)->generate($qrcode_generator)) !!} ">
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">{{ $order->KaBalai != null ? $order->KaBalai->full_name : '' }}</div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    @if($order->cancel_at == null)
    <section class="sheet container-fluid px-0">

        <div class="no-margin-page">
            
            <div class="text-center">
                <div class="title"><strong>RESUME EVALUASI TIPE UTTP</strong></div>
                <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
            </div>

            <br/><br/>  

            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th rowspan="2" class="text-center">NO</th>
                        <th rowspan="2" class="text-center">PEMERIKSAAN &amp; PENGUJIAN</th>
                        <th colspan="3" class="text-center">PEMENUHAN SYARAT</th>
                        <th rowspan="2" class="text-center">KETERANGAN</th>
                    </tr>
                    <tr>
                        <th class="text-center">YA</th>
                        <th class="text-center">TIDAK</th>
                        <th class="text-center">N/A</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($order->inspections as $inspection)
                    <tr>
                        <td>
                            @if($inspection->inspectionItem->is_header)
                            {{ $inspection->inspectionItem->no }}
                            @endif
                        </td>
                        <td>
                            @if($inspection->inspectionItem->is_header)
                            {{ $inspection->inspectionItem->name }}
                            @else
                            {{ $inspection->inspectionItem->no }} {{ $inspection->inspectionItem->name }}
                            @endif
                        </td>
                        <td class="text-center">
                            @if($inspection->is_accepted && $inspection->inspectionItem->is_tested)
                            X
                            @endif
                        </td>
                        <td class="text-center">
                            @if($inspection->is_accepted != null && !$inspection->is_accepted && $inspection->inspectionItem->is_tested)
                            X
                            @endif
                        </td>
                        <td class="text-center">
                            @if($inspection->is_accepted == null && $inspection->inspectionItem->is_tested)
                            X
                            @endif
                        </td>
                        <td></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    
    </section>
    @endif

</body>

</html>