@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        @if($serviceOrder->subkoordinator_notes != null)
        <div class="alert alert-info" role="alert">
            {{ $serviceOrder->subkoordinator_notes }}
        </div>
        @endif

        {!! Form::open(['url' => route('serviceluaruut.resultupload', $serviceOrder->id), 'files' => true, 'id' => 'form_result'])  !!}
        
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_uttp">Jenis UTTP</label>
                    {!! Form::text('jenis_uttp', 
                        $serviceOrder->uuts != null ? 
                        $serviceOrder->uuts->stdtype->uut_type :
                        $serviceOrder->ServiceRequestItem->uuts->stdtype->uut_type, ['class' => 'form-control','id' => 'jenis_uut', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6 pull-right">
                <a href="{{ route('serviceluaruut.editalat', $serviceOrder->id) }}"
                                class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_edit_alat">
                                <i class="fa fa-edit faa-flash"></i> Edit Data Alat</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas Maksimum</label>
                    {!! Form::text('kapasitas', 
                        $serviceOrder->tool_capacity != null && $serviceOrder->tool_capacity_unit != null ?
                        $serviceOrder->tool_capacity . ' ' . $serviceOrder->tool_capacity_unit : 
                        $serviceOrder->ServiceRequestItem->uuts->tool_capacity . ' ' . $serviceOrder->ServiceRequestItem->uuts->tool_capacity_unit, ['class' => 'form-control','id' => 'kapasitas', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas Minimum</label>
                    {!! Form::text('kapasitas', 
                        $serviceOrder->tool_capacity_min != null && $serviceOrder->tool_capacity_unit != null ?
                        $serviceOrder->tool_capacity_min . ' ' . $serviceOrder->tool_capacity_unit : 
                        $serviceOrder->ServiceRequestItem->uuts->tool_capacity_min . ' ' . $serviceOrder->ServiceRequestItem->uuts->tool_capacity_unit, ['class' => 'form-control','id' => 'kapasitas', 'readonly']) !!}
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Merek/Model/Tipe/Nomor Seri</label>
                    {!! Form::text('merek', 
                        $serviceOrder->tool_brand != null && $serviceOrder->tool_model != null && $serviceOrder->tool_serial_no != null ?
                        $serviceOrder->tool_brand . '/' . 
                        $serviceOrder->tool_model . '/' .
                        $serviceOrder->tool_serial_no : 
                        $serviceOrder->ServiceRequestItem->uuts->tool_brand . '/' . 
                        $serviceOrder->ServiceRequestItem->uuts->tool_model . '/' .
                        $serviceOrder->ServiceRequestItem->uuts->serial_no,
                        ['class' => 'form-control','id' => 'merek', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->tool_made_in != null ? 
                        $serviceOrder->tool_made_in : 
                        $serviceOrder->ServiceRequestItem->uuts->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan', 'readonly']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Dasar Pengujian, Nomor</label>
                    {!! Form::text('reference_no', 
                        $serviceOrder->ServiceRequestItem->reference_no,
                        ['class' => 'form-control','id' => 'reference_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_date">Dasar Pengujian, Tanggal</label>
                    {!! Form::text('reference_date', 
                        date("d-m-Y", strtotime(isset($serviceOrder->ServiceRequestItem->reference_date) ? $serviceOrder->ServiceRequestItem->reference_date : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'reference_date', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_1">Diuji Oleh (1)</label>
                    {!! Form::text('test_by_1', 
                        $user->full_name,
                        ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_2">Diuji Oleh (2)</label>
                    {!! Form::select('test_by_2', $users, $serviceOrder->test_by_2, 
                        ['class' => 'form-control', 'id' => 'test_by_2']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_datein">Waktu Pengujian, Mulai</label>
                    {!! Form::text('staff_entry_datein', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_datein) ? $serviceOrder->staff_entry_datein : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'staff_entry_datein', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_dateout">Waktu Pengujian, Selesai</label>
                    {!! Form::text('staff_entry_dateout', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_dateout) ? $serviceOrder->staff_entry_dateout : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'staff_entry_dateout', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Item Pengujian/Pemeriksaan</label>
                    <?php
                        $inspections = [];
                        foreach($serviceOrder->ServiceRequestItem->inspections as $inspection) {
                            $inspections[] = $inspection->inspectionPrice->inspection_type;
                        }
                    ?>
                    <textarea name="inspections" id="inspections" readonly
                        class="form-control">{!! implode("\n", $inspections) !!}</textarea>
                </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
        
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseDataTipe" aria-expanded="false" aria-controls="collapseExample">
                    Data Hasil Uji &nbsp;&nbsp;<i class="fa fa-angle-down"></i>
                </button>
                <label>&nbsp;&nbsp;Diisi sesuai dengaan kebutuhan masing-masing Jenis UTTP</label>
            </div>
        </div>
        <br/>

        <div class="collapse" id="collapseDataTipe">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kelas Keakurasian</label>
                    {!! Form::text('kelas_keakurasian', $serviceOrder->kelas_keakurasian, ['class' => 'form-control','id' => 'kelas_keakurasian']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Daya Baca</label>
                    {!! Form::text('daya_baca', $serviceOrder->daya_baca, ['class' => 'form-control','id' => 'daya_baca']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Interval Skala Verifikasi</label>
                    {!! Form::text('interval_skala_verifikasi', $serviceOrder->interval_skala_verifikasi, ['class' => 'form-control','id' => 'interval_skala_verifikasi']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kecepatan</label>
                    {!! Form::text('kecepatan', $serviceOrder->kecepatan, ['class' => 'form-control','id' => 'kecepatan']) !!} 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kelas Single Axle Load</label>
                    {!! Form::text('kelas_single_axle_load', $serviceOrder->kelas_single_axle_load, ['class' => 'form-control','id' => 'kelas_single_axle_load']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kelas Single Group Load</label>
                    {!! Form::text('kelas_single_group_load', $serviceOrder->kelas_single_group_load, ['class' => 'form-control','id' => 'kelas_single_group_load']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Metode Pengukuran</label>
                    {!! Form::text('metode_pengukuran', $serviceOrder->metode_pengukuran, ['class' => 'form-control','id' => 'metode_pengukuran']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Sistem Jaringan</label>
                    {!! Form::text('sistem_jaringan', $serviceOrder->sistem_jaringan, ['class' => 'form-control','id' => 'sistem_jaringan']) !!} 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Konstanta</label>
                    {!! Form::text('konstanta', $serviceOrder->konstanta, ['class' => 'form-control','id' => 'konstanta']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kelas Temperatur</label>
                    {!! Form::text('kelas_temperatur', $serviceOrder->kelas_temperatur, ['class' => 'form-control','id' => 'kelas_temperatur']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Rasio Q<sub>3</sub>/Q<sub>1</sub></label>
                    {!! Form::text('rasio_q', $serviceOrder->rasio_q, ['class' => 'form-control','id' => 'rasio_q']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Diameter Nominal</label>
                    {!! Form::text('diameter_nominal', $serviceOrder->diameter_nominal, ['class' => 'form-control','id' => 'diameter_nominal']) !!} 
                </div>
            </div>
        </div>
        </div>

        @elseif($serviceOrder->ServiceRequest->service_type_id == 4 || $serviceOrder->ServiceRequest->service_type_id == 5)
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sertifikat_expired_at">Masa Berlaku</label>
                    {!! Form::text('sertifikat_expired_at', 
                        date("d-m-Y", strtotime(isset($serviceOrder->sertifikat_expired_at) ? $serviceOrder->sertifikat_expired_at : $sertifikat_expired_at)),
                        ['class' => 'form-control','id' => 'sertifikat_expired_at', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="hasil_uji_memenuhi">Hasil Uji</label>
                    {!! Form::select('hasil_uji_memenuhi', ['sah'=>'Sah','batal'=>'Batal'], $serviceOrder->hasil_uji_memenuhi ? 'sah' : 'batal', 
                        ['class' => 'form-control select2','id' => 'hasil_uji_memenuhi', 'style' => 'width:100%']) !!}
                    
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tanda_pegawai_berhak">Tanda Pegawai Berhak</label>
                    {!! Form::text('tanda_pegawai_berhak', $serviceOrder->tanda_pegawai_berhak,
                        ['class' => 'form-control','id' => 'tanda_pegawai_berhak']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tanda_daerah">Tanda Daerah</label>
                    {!! Form::text('tanda_daerah', $serviceOrder->tanda_daerah,
                        ['class' => 'form-control','id' => 'tanda_daerah']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tanda_sah">Tanda Sah</label>
                    {!! Form::text('tanda_sah', $serviceOrder->tanda_sah,
                        ['class' => 'form-control','id' => 'tanda_sah']) !!}
                </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->lokasi_pengujian == 'luar' && $serviceOrder->ServiceRequestItem->location != null)
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Lokasi Penempatan</label>
                    {!! Form::text('location', $serviceOrder->ServiceRequestItem->location, ['class' => 'form-control','id' => 'location', 'readonly']) !!} 
                </div>
            </div>
        </div>
        @if(count($serviceOrder->ttuPerlengkapans) > 0)
        <h5>Perlengkapan</h5>
        <div class="row">
            <div class="col-md-12">
                <table id="perlengkapan" class="table table-responsive-sm input-table">
                    <thead>
                        <tr>
                            <th>Jenis</th>
                            <th>Merek</th>
                            <th>Model/Tipe</th>
                            <th>Nomor Seri</th>
                            <th>Keterangan/Penyegelan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($serviceOrder->ttuPerlengkapans as $perlengkapan)
                        <tr>
                            <td>{{ $perlengkapan->uuts->type->uttp_type }}</td>
                            <td>{{ $perlengkapan->uuts->tool_brand }}</td>
                            <td>{{ $perlengkapan->uuts->tool_model }}</td>
                            <td>{{ $perlengkapan->uuts->serial_no }}</td>
                            <td>
                                <input type="hidden" name="perlengkapan_id[]" />
                                <input type="hidden" name="perlengkapan_uttp_id[]" value="{{ $perlengkapan->uttp_id }}"/>
                                <input type="text" class="form-control" name="perlangkapan_keterangan[]" 
                                    value="{{ $perlengkapan->keterangan }}"/>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        @endif
        
        @if($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Automatic Level Gauge' || 
            $serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Pelat Orifice'
        )
        <table id="data_table_ttu_alg_badanhitung" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr colspan="3">Identitas Badan Hitung</tr>
                <tr>
                    <th>Tipe</th>
                    <th>No Seri</th>
                    <th>
                        <button type="button" id="btn_add_badanhitung" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($badanHitungs as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="type[]" value="{{ $ttuItem->type }}"/></td>
                    <td><input type="text" class="form-control" name="serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="badanhitung_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Meter Arus BBM')
        <table id="data_table_ttu_mabbm_badanhitung" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr colspan="4">Identitas Badan Hitung</tr>
                <tr>
                    <th>Merek</th>
                    <th>Tipe</th>
                    <th>No Seri</th>
                    <th>
                        <button type="button" id="btn_add_badanhitung" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($badanHitungs as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="brand[]" value="{{ $ttuItem->brand }}"/></td>
                    <td><input type="text" class="form-control" name="type[]" value="{{ $ttuItem->type }}"/></td>
                    <td><input type="text" class="form-control" name="serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="badanhitung_item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endif

        @if($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Automatic Level Gauge' ||
            $serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Pelat Orifice')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tank_no">Nomor Tangki</label>
                    {!! Form::text('tank_no', 
                        $ttu ? $ttu->tank_no : '',
                        ['class' => 'form-control','id' => 'tank_no']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tag_no">Nomor Tag</label>
                    {!! Form::text('tag_no', 
                        $ttu ? $ttu->tag_no : '',
                        ['class' => 'form-control','id' => 'tag_no']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Meter Air' ||
            $serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Pompa Ukur BBM'
        )
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="totalisator">Totalisator</label>
                    {!! Form::text('totalisator', 
                        $ttu ? $ttu->totalisator : '',
                        ['class' => 'form-control','id' => 'totalisator']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Meter Arus BBM' ||
            $serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Meter Gas' ||
            $serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Meter Gas (USM WetCal)' ||
            $serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Pressure Transmitter' ||
            $serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Pompa Ukur BBG'
        )
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="totalisator">Totalisator</label>
                    {!! Form::text('totalisator', 
                        $ttu ? $ttu->totalisator : '',
                        ['class' => 'form-control','id' => 'totalisator']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kfactor">K Faktor</label>
                    {!! Form::text('kfactor', 
                        $ttu ? $ttu->kfactor : '',
                        ['class' => 'form-control','id' => 'kfactor']) !!}
                </div>
            </div>
        </div>
        @endif

        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="persyaratan_teknis">Persyaratan Teknis</label>
                    {!! Form::hidden('persyaratan_teknis_id', 
                        $serviceOrder->ServiceRequestItem->uuts->stdtype->oiml_id,
                        ['class' => 'form-control','id' => 'persyaratan_teknis_id', 'readonly']) !!}
                    <textarea name="persyaratan_teknis_id_str" id="persyaratan_teknis_id_str" readonly
                        class="form-control">{!! $serviceOrder->ServiceRequestItem->uuts->stdtype->syarat_teknis !!}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="file_skhp">Lampiran Evaluasi *</label>
                    {!! Form::file('file_skhp', 
                        ['class' => 'form-control','id' => 'file_skhp', 'accept'=> 'application/pdf']) !!}
                </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
        <table id="data_table" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Pemeriksaan/Pengujian</th>
                    <th>Pemenuhan Persyaratan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($inspectionItems as $item)
                <tr>
                    <td>{{ $item->inspectionItem->name }}</td>
                    <td>
                    @if($item->inspectionItem->is_tested)
                    {!! Form::select('is_accepted_'.$item->id, ['ya'=>'Ya','tidak'=>'Tidak', ''=>'N/A'], 
                        isset($item->is_accepted) ? ($item->is_accepted ? 'ya' : 'tidak') : '', 
                        ['class' => 'form-control select2','id' => 'is_accepted_'.$item->id, 'style' => 'width:100%']) !!}
                    @endif
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="has_set">Catatan Hasil Pemeriksaan (akan dicantumkan di dalam SKHPT)</label>
                    <textarea name="catatan_hasil_pemeriksaan" id="catatan_hasil_pemeriksaan"
                        class="form-control">{!! $serviceOrder->catatan_hasil_pemeriksaan !!}</textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="has_set">Memerlukan Sertifikat Evaluasi Tipe</label>
                    <input type="checkbox" name="has_set" value="has_set" id="has_set" {{ $serviceOrder->has_set ? 'checked' : '' }} />
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="set_memenuhi">Hasil Sertifikat Evaluasi Tipe</label>
                    {!! Form::select('set_memenuhi', ['memenuhi'=>'Memenuhi','tolak'=>'Tidak Memenuhi (Ditolak)'], $serviceOrder->set_memenuhi ? 'memenuhi' : 'tolak', 
                        ['class' => 'form-control select2','id' => 'set_memenuhi', 'style' => 'width:100%']) !!}
                    
                </div>
            </div>
        </div> 

        @elseif($serviceOrder->ServiceRequest->service_type_id == 4 || $serviceOrder->ServiceRequest->service_type_id == 5)
        
        @if($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Automatic Level Gauge')
        <table id="data_table_ttu_alg" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th rowspan="2">Input Level (mm)</th>
                    <th colspan="2">Kesalahan Penunjukan</th>
                    <th rowspan="2">Kesalahan Histerisis (mm)</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>Naik (mm)</th>
                    <th>Turun (mm)<th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="input_level[]" value="{{ $ttuItem->input_level }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error_up[]" value="{{ $ttuItem->error_up }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="error_down[]" value="{{ $ttuItem->error_down }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="error_hysteresis[]" value="{{ $ttuItem->error_hysteresis }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Meter Arus BBM')
        <table id="data_table_ttu_mabbm" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Kecepatan Alir (L/menit)</th>
                    <th>Kesalahan (%) BKD: &#177;0,5%</th>
                    <th>Meter Factor BKD: 1&#177;0,005</th>
                    <th>Kemampuan Ulang BKD: &#177;0,1%</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="meter_factor[]" value="{{ $ttuItem->meter_factor }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatability[]" value="{{ $ttuItem->repeatability }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Meter Gas' || 
        $serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Pompa Ukur BBG')
        <table id="data_table_ttu_mg" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Kecepatan Alir (m3/h)</th>
                    <th>Kesalahan (%)</th>
                    <th>Kemampuan Ulang %</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatibility[]" value="{{ $ttuItem->repeatibility }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Meter Air')
        <table id="data_table_ttu_ma" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Kecepatan Alir (L/menit)</th>
                    <th>Kesalahan (%) BKD: &plusmn;0,5%</th>
                    <th>Kemampuan Ulang BKD: &plusmn;0,1%</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatability[]" value="{{ $ttuItem->repeatability }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Meter Gas (USM WetCal)')
        <table id="data_table_ttu_mguwc" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th rowspan="2">Kecepatan Alir (m3/h)</th>
                    <th colspan="2">Kesalahan</th>
                    <th colspan="2">Kemampuan Ulang</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>Nilai (%)</th>
                    <th>BKD (%)</th>
                    <th>Nilai</th>
                    <th>BKD</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="error_bkd[]" value="{{ $ttuItem->error_bkd }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatability[]" value="{{ $ttuItem->repeatability }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="repeatability_bkd[]" value="{{ $ttuItem->repeatability_bkd }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Depth Tape')
        <table id="data_table_ttu_dt" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Rentang Ukur (m)</th>
                    <th>Suhu Dasar (&#8451;)</th>
                    <th>Panjang Sebenarnya (mm)</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="rentang_ukur[]" value="{{ $ttuItem->rentang_ukur }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="suhu_dasar[]" value="{{ $ttuItem->suhu_dasar }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="panjang_sebenarnya[]" value="{{ $ttuItem->panjang_sebenarnya }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'UTI Meter')
        <table id="data_table_ttu_uti" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Rentang Ukur (m)</th>
                    <th>Temperatur Dasar (&#8451;)</th>
                    <th>Panjang Sebenarnya (mm)</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="rentang_ukur[]" value="{{ $ttuItem->rentang_ukur }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="suhu_dasar[]" value="{{ $ttuItem->suhu_dasar }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="panjang_sebenarnya[]" value="{{ $ttuItem->panjang_sebenarnya }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <br/>
        
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Pelat Orifice')
        <table id="data_table_ttu_po" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="5">Ukuran</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>d (mm)</th>
                    <th>D (mm)</th>
                    <th>e (mm)</th>
                    <th>E (mm)</th>
                    <th>&alpha;</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="d_inner[]" value="{{ $ttuItem->d_inner }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="d_outer[]" value="{{ $ttuItem->d_outer }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="e_inner[]" value="{{ $ttuItem->e_inner }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="e_outer[]" value="{{ $ttuItem->e_outer }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="alpha[]" value="{{ $ttuItem->alpha }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Pompa Ukur BBM')
        <table id="data_table_ttu_pubbm" class="table table-striped table-hover table-responsive-sm">
            <thead>
                
                <tr>
                    <th>Merek</th>
                    <th>Tipe</th>
                    <th>Nomor Seri</th>
                    <th>Debit Maks (L/min)</th>
                    <th>Media</th>
                    <th>Jumlah Nozzle</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="text" step="any" class="form-control" name="tool_brand[]" value="{{ $ttuItem->tool_brand }}"/></td>
                    <td><input type="text" step="any" class="form-control" name="tool_type[]" value="{{ $ttuItem->tool_type }}"/></td> 
                    <td><input type="text" step="any" class="form-control" name="tool_serial_no[]" value="{{ $ttuItem->tool_serial_no }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="debit_max[]" value="{{ $ttuItem->debit_max }}"/></td>
                    <td><input type="text" step="any" class="form-control" name="tool_media[]" value="{{ $ttuItem->tool_media }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="nozzle_count[]" value="{{ $ttuItem->nozzle_count }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Pressure Transmitter' ||
        $serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Differential Pressure Transmitter' ||
        $serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Temperature Transmitter' 
        )
        <table id="data_table_ttu_pt" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="2">Input</th>
                    <th rowspan="2">Aktual (mA)</th>
                    <th colspan="2">Output (mA)</th>
                    <th colspan="2">Error (%)</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>%</th>
                    @if($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Pressure Transmitter')
                    <th>kg/cm<sup>2</sup></th>
                    @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Differential Pressure Transmitter')
                    <th>mmH<sub>2</sub>O</th>
                    @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'Temperature Transmitter')
                    <th>&#8457;</th>
                    @endif
                    <th>Up</th>
                    <th>Down</th>
                    <th>Up</th>
                    <th>Down</th>
                    
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="input_pct[]" value="{{ $ttuItem->input_pct }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="input_level[]" value="{{ $ttuItem->input_level }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="actual[]" value="{{ $ttuItem->actual }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="output_up[]" value="{{ $ttuItem->output_up }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="output_down[]" value="{{ $ttuItem->output_down }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error_up[]" value="{{ $ttuItem->error_up }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error_down[]" value="{{ $ttuItem->error_down }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uuts->type->kelompok == 'EVC')
        <table id="data_table_ttu_evc" class="table table-striped table-hover table-responsive-sm">
            <thead>
                
                <tr>
                    <th>Error Total (%)</th>
                    <th>BKD (%)</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error_bkd[]" value="{{ $ttuItem->error_bkd }}"/></td> 
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @endif
        
        @else
        @endif

        <input type="hidden" name="stat_sertifikat" id="stat_sertifikat" value="0"/>
        <button role="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan Hasil Uji</button>
        <button class="btn btn-w-md btn-success" id="btn_simpan_submit">Kirim Hasil Uji</button>
        {!! Form::close() !!}

    </div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function ()
    {
        $('.select2, #hasil_uji_memenuhi').select2({
            //placeholder: "- Pilih UML -",
            allowClear: true
        });
        $('#test_by_2').select2({
            placeholder: "- Pilih Pegawai -",
            allowClear: true
        });
        /*
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        */

        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_alg_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" step="any" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_mabbm_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" step="any" class="form-control" name="brand[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_alg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_alg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="input_level[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_up[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_down[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_hysteresis[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mabbm");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="meter_factor[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_ma').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_ma");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_ma').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mguwc').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mguwc");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_bkd[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="repeatability_bkd[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mguwc').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_dt').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_dt");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="rentang_ukur[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="suhu_dasar[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="panjang_sebenarnya[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_dt').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_uti').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_uti");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="rentang_ukur[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="suhu_dasar[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="panjang_sebenarnya[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_uti').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_uti_suhu').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_uti_suhu");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="penunjukan[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="penunjukan_standar[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="koreksi[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="suhu_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_uti_suhu').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_po').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_po");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="d_inner[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="d_outer[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="e_inner[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="e_outer[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="alpha[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_po').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_pubbm').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_pubbm");
            var insp_item = '<tr>' + 
                '   <td><input type="text" step="any" class="form-control" name="tool_brand[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="tool_type[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="tool_serial_no[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="debit_max[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="tool_media[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="nozzle_count[]" /></td>' +  
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_pubbm').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_pt').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_pt");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="input_pct[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="input_level[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="actual[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="output_up[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="output_down[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="error_up[]" /></td>' +  
                '   <td><input type="number" step="any" class="form-control" name="error_down[]" /></td>' +  
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_pt').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_evc').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_evc");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_bkd[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_evc').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            $("#stat_sertifikat").val(1);

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#form_result").validate({
            rules: {
                file_skhp: {
                    required: true,
                },
            },
            messages: {
                file_skhp: 'File lampiran harus diupload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    }); 
</script>
@endsection