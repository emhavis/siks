<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Keterangan Hasil Pengujian</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-draft-uttp.png") : public_path("assets/images/logo/letterhead-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .vertical-middle {
            vertical-align: middle !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
    </style>
</head>

<body class="A4">

    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order }}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN HASIL PENGUJIAN</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 40mm;">Jenis UTTP</td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="5">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td>Merek/Tipe</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_brand != null && $order->tool_model != null ?
                        $order->tool_brand . '/' . $order->tool_model :
                        $order->ServiceRequestItem->uttp->tool_brand . '/' . $order->ServiceRequestItem->uttp->tool_model 
                    }}</td>
                </tr>
                @if(
                    ($order->tool_media != null && $order->tool_media != '') ||
                    ($order->ServiceRequestItem->uttp->tool_media != null && $order->ServiceRequestItem->uttp->tool_media != '')
                    )
                <tr>
                    <td>Media Uji/Komoditas</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_media != null ? 
                        $order->tool_media :
                        $order->ServiceRequestItem->uttp->tool_media 
                    }}</td>
                </tr>
                @endif
                <tr>
                    <td style="width: 40mm;">Kapasitas Maksimum</td>
                    <td>:</td>
                    <td>{{ 
                            $order->tool_capacity != null ?
                            $order->tool_capacity :
                            $order->ServiceRequestItem->uttp->tool_capacity 
                        }}&nbsp;
                        {{ 
                            $order->tool_capacity_unit != null ?
                            $order->tool_capacity_unit :
                            $order->ServiceRequestItem->uttp->tool_capacity_unit 
                        }}
                    </td>
                    @if($order->tool_capacity_min != null && $order->tool_capacity_min != '')
                    <td style="width: 3mm"></td>
                    <td style="width: 35mm;">Kapasitas Minimum</td>
                    <td>:</td>
                    <td>{{ $order->tool_capacity_min }}
                        {{ $order->tool_capacity_unit }}
                    </td>
                    @elseif($order->ServiceRequestItem->uttp->tool_capacity_min != '')
                    <td style="width: 3mm"></td>
                    <td>Kapasitas Minimum</td>
                    <td>:</td>
                    <td>{{ $order->ServiceRequestItem->uttp->tool_capacity_min 
                        }}
                        {{ $order->ServiceRequestItem->uttp->tool_capacity_unit 
                        }}
                    </td>
                    @endif
                </tr>
                
                <?php
                $teknis = [];
                if($order->kelas_keakurasian != null && $order->kelas_keakurasian != '') {
                    $teknis['Kelas Keakurasian'] = $order->kelas_keakurasian;
                }
                if($order->daya_baca != null && $order->daya_baca != '') {
                    $teknis['Daya Baca'] = $order->daya_baca;
                }
                if($order->interval_skala_verifikasi != null && $order->interval_skala_verifikasi != '') {
                    $teknis['Interval Skala Verifikasi'] = $order->interval_skala_verifikasi;
                }
                if($order->kecepatan != null && $order->kecepatan != '') {
                    $teknis['Kecapatan'] = $order->kecepatan;
                }
                if($order->kelas_single_axle_load != null && $order->kelas_single_axle_load != '') {
                    $teknis['Kelas <i>Single Axle Load</i>'] = $order->kelas_single_axle_load;
                }
                if($order->kelas_single_group_load != null && $order->kelas_single_group_load != '') {
                    $teknis['Kelas <i>Single Group Load</i>'] = $order->kelas_single_group_load;
                }
                if($order->metode_pengukuran != null && $order->metode_pengukuran != '') {
                    $teknis['Metode Pengukuran'] = $order->metode_pengukuran;
                }
                if($order->sistem_jaringan != null && $order->sistem_jaringan != '') {
                    $teknis['Sistem Jaringan'] = $order->sistem_jaringan;
                }
                if($order->konstanta != null && $order->konstanta != '') {
                    $teknis['Konstanta'] = $order->konstanta;
                }
                if($order->kelas_temperatur != null && $order->kelas_temperatur != '') {
                    $teknis['Kelas Temperatur'] = $order->kelas_temperatur;
                }
                if($order->rasio_q != null && $order->rasio_q != '') {
                    $teknis['Rasio Q<sub>3</sub>/Q<sub>1</sub>'] = $order->rasio_q;
                }
                if($order->diameter_nominal != null && $order->diameter_nominal != '') {
                    $teknis['Diameter Nominal'] = $order->diameter_nominal;
                }
                ?>

                @foreach($teknis as $label => $value)
                
                @if($loop->iteration % 2 != 0)
                <tr>
                    <td style="width: 40mm;">{!! $label !!}</td>
                    <td>:</td>
                    @if($loop->last)
                    <td colspan="5">{!! $value !!}</td>
                </tr>
                    @else 
                    <td>{!! $value !!}</td>
                    @endif
                @else
                    <td style="width: 5mm"></td>
                    <td style="width: 35mm;">{!! $label !!}</td>
                    <td style="width: 3mm;">:</td>
                    <td>{!! $value !!}</td>
                </tr>
                @endif

                @endforeach

                
                <tr>
                    <td>Buatan</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_made_in != null ? 
                        $order->tool_made_in :
                        $order->ServiceRequestItem->uttp->tool_made_in 
                    }}</td>
                </tr>
                <tr>
                    <td>Nama Pabrikan</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_factory != null ?
                        $order->tool_factory :
                        $order->ServiceRequestItem->uttp->tool_factory 
                    }}</td>
                </tr>
                <tr>
                    <td>Alamat Pabrikan</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_factory_address != null ? 
                        $order->tool_factory_address : 
                        $order->ServiceRequestItem->uttp->tool_factory_address 
                    }}</td>
                </tr>
                <tr>
                    <td>Pemohon</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                <tr>
                    <td>Alamat Pemohon</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->ServiceRequest->addr_sertifikat }}</td>
                </tr>
                <tr>
                    <td>Diuji/Diverifikasi Oleh</td>
                    <td>:</td>
                    <td>{{ $order->TestBy1 != null && $order->TestBy1->PetugasUttp != null ? $order->TestBy1->PetugasUttp->nama : '' }}</td>
                    <td style="width: 3mm"></td>
                    <td colspan="3">NIP : {{ $order->TestBy1 != null && $order->TestBy1->PetugasUttp != null ? $order->TestBy1->PetugasUttp->nip : '' }}</td>
                </tr>
                @if($order->TestBy2)
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>{{ $order->TestBy2 != null && $order->TestBy2->PetugasUttp != null ? $order->TestBy2->PetugasUttp->nama : '' }}</td>
                    <td style="width: 3mm"></td>
                    <td colspan="3">NIP : {{ $order->TestBy2 != null && $order->TestBy2->PetugasUttp != null ? $order->TestBy2->PetugasUttp->nip : '' }}</td>
                </tr>
                @endif
                <tr>
                    <td>Waktu Pengujian/Verifikasi</td>
                    <td>:</td>
                    <td colspan="5">
                    @if($order->stat_service_order == 4)
                        {{ format_long_date($order->staff_entry_datein) . ' - ' . format_long_date($order->cancel_at) }}
                    @else
                        {{ format_long_date($order->staff_entry_datein) . ' - ' . format_long_date($order->staff_entry_dateout) }}
                    @endif  
                    </td>
                </tr>
                <tr>
                    <td>Lokasi Pengujian/Verifikasi</td>
                    <td>:</td>
                    <td colspan="5">
                        {{ $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'Direktorat Metrologi' : 'Lokasi UTTP Terpasang'}}
                    </td>
                </tr>
                <tr>
                    <td>Dasar Pengujian/Verifikasi</td>
                    <td>:</td>
                    <td colspan="5">
                        Surat Permohonan dari: {{ $order->ServiceRequest->label_sertifikat }}
                        <br/>
                        Nomor: {{ $order->ServiceRequestItem->reference_no }}, Tanggal: {{ format_long_date($order->ServiceRequestItem->reference_date) }} 
                    </td>
                </tr>
                
                <tr>
                    <td>Persyaratan Teknis</td>
                    <td>:</td>
                    <td colspan="5">{!! $order->ServiceRequestItem->uttp->type->syarat_teknis !!}</td>
                </tr>
                <tr>
                    <td>Hasil</td>
                    <td>:</td>
                    <td colspan="5">
                        @if($order->cancel_at != null)
                        Pengujian tidak dapat dilakukan atau dilanjutkan karena {{ $order->cancel_notes }}.
                        @else
                        <strong>{{ $order->ServiceRequestItem->uttp->type->uttp_type }}</strong> Merek <strong>
                            {{ $order->tool_brand != null ? $order->tool_brand : $order->ServiceRequestItem->uttp->tool_brand }}
                            </strong> tipe <strong>{{ $order->tool_model != null ? $order->tool_model : $order->ServiceRequestItem->uttp->tool_model }}</strong> 
                            dinyatakan <strong>{{ $order->hasil_uji_memenuhi ? 'Memenuhi' : 'Tidak Memenuhi'}} Syarat Teknis</strong>,
                            untuk jenis-jenis pengujian terlampir yang merupakan bagian tidak terpisahkan dari Surat Keterangan Hasil Pengujian ini. 
                        @endif
                    </td>
                </tr>
            </tbody>
        <table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td>Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date("d-m-Y") }}
                        <br/>Kepala Balai Pengujian UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}<br/>
                        NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>

    @if($order->cancel_at == null)
    <div class="page-break"></div>

    <section class="sheet watermark">
        <div class="text-right" style="padding-top: 25.4mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <div class="text-center" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="title"><strong>RESUME EVALUASI TIPE UTTP</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>  

        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <thead>
                <tr>
                    <th rowspan="2" class="text-center vertical-middle">NO</th>
                    <th rowspan="2" class="text-center vertical-middle">PEMERIKSAAN &amp; PENGUJIAN</th>
                    <th colspan="3" class="text-center vertical-middle">PEMENUHAN SYARAT</th>
                    <th rowspan="2" class="text-center vertical-middle">KETERANGAN</th>
                </tr>
                <tr>
                    <th class="text-center">YA</th>
                    <th class="text-center">TIDAK</th>
                    <th class="text-center">N/A</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->inspections as $inspection)
                <tr>
                    <td>
                        @if($inspection->inspectionItem->is_header)
                        {{ $inspection->inspectionItem->no }}
                        @endif
                    </td>
                    <td>
                        @if($inspection->inspectionItem->is_header)
                        {{ $inspection->inspectionItem->name }}
                        @else
                        {{ $inspection->inspectionItem->no }} {{ $inspection->inspectionItem->name }}
                        @endif
                    </td>
                    <td class="text-center">
                        @if($inspection->is_accepted && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td class="text-center">
                        @if(isset($inspection->is_accepted) && $inspection->is_accepted == false && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td class="text-center">
                        @if(!isset($inspection->is_accepted) && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td>&nbsp;</td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <br/>
          <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Persetujuan Tipe,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Persetujuan Tipe' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
    
    </section>
    @endif


</body>

</html>