@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        @if($serviceOrder->kalab_notes != null)
        <div class="alert alert-info" role="alert">
            {{ $serviceOrder->kalab_notes }}
        </div>
        @endif

        {!! Form::open(['url' => route('serviceluaruut.resultupload', $serviceOrder->id), 'files' => true, 'id' => 'form_result'])  !!}
        
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_uttp">Jenis UUT</label>
                    {!! Form::text('jenis_uttp', $serviceOrder->ServiceRequestItem->uuts->stdtype->uut_type, ['class' => 'form-control','id' => 'jenis_uut', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas</label>
                    {!! Form::text('kapasitas', $serviceOrder->ServiceRequestItem->uuts->tool_capacity . ' ' . $serviceOrder->ServiceRequestItem->uuts->tool_capacity_unit, ['class' => 'form-control','id' => 'kapasitas', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Merek/Model/Tipe/Nomor Seri</label>
                    {!! Form::text('merek', 
                        $serviceOrder->ServiceRequestItem->uuts->tool_brand . '/' . 
                        $serviceOrder->ServiceRequestItem->uuts->tool_model . '/' .
                        $serviceOrder->ServiceRequestItem->uuts->tool_type . '/' .
                        $serviceOrder->ServiceRequestItem->uuts->serial_no,
                        ['class' => 'form-control','id' => 'merek', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->ServiceRequestItem->uuts->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan', 'readonly']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>

        <!-- 
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory">Nama Pabrikan</label>
                    {!! Form::text('factory', 
                        $serviceOrder->ServiceRequestItem->uuts->tool_factory,
                        ['class' => 'form-control','id' => 'factory', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory_addr">Alamat Pabrikan</label>
                    {!! Form::text('factory_addr', 
                        $serviceOrder->ServiceRequestItem->uuts->tool_factory_address,
                        ['class' => 'form-control','id' => 'factory_addr', 'readonly']) !!}
                </div>
            </div>
        </div>
        -->

        <!--
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Dasar Pengujian, Nomor</label>
                    {!! Form::text('reference_no', 
                        $serviceOrder->ServiceRequestItem->reference_no,
                        ['class' => 'form-control','id' => 'reference_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_date">Dasar Pengujian, Tanggal</label>
                    {!! Form::text('reference_date', 
                        date("d-m-Y", strtotime(isset($serviceOrder->ServiceRequestItem->reference_date) ? $serviceOrder->ServiceRequestItem->reference_date : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'reference_date', 'readonly']) !!}
                </div>
            </div>
        </div>
        -->

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_1">Diuji Oleh (1)</label>
                    {!! Form::text('test_by_1', 
                        $user->full_name,
                        ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_2">Diuji Oleh (2)</label>
                    {!! Form::select('test_by_2', $users, null, 
                        ['class' => 'form-control select2', 'id' => 'test_by_2']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_datein">Waktu Pengujian, Mulai</label>
                    {!! Form::text('staff_entry_datein', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_datein) ? $serviceOrder->staff_entry_datein : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'staff_entry_datein', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="staff_entry_dateout">Waktu Pengujian, Selesai</label> {{$serviceOrder->ServiceRequest->service_type_id}}
                    {!! Form::text('staff_entry_dateout', 
                        date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_dateout) ? $serviceOrder->staff_entry_dateout : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'staff_entry_dateout', 'readonly']) !!}
                </div>
            </div>
        </div>
        @if($serviceOrder->ServiceRequest->service_type_id == 1):
        <div class="row">
            <div class="col-md-6">
                    <div class="form-group">
                        <label for="hasil_uji_memenuhi">Memenuhi Syarat</label>
                        {!! Form::select('hasil_uji_memenuhi', [True=>'Ya', False=>'Tidak'], null, 
                            ['class' => 'form-control select2', 'id' => 'hasil_uji_memenuhi']) !!}
                    </div>
            </div>
            <div class="col-md-6">
                <div class="form-group berlaku">
                    <label for="sertifikat_expired_at">Berlaku Sampai (dd-mm-yyyy)</label>
                    {!! Form::text('sertifikat_expired_at', null,
                    ['class' => 'form-control','id' => 'sertifikat_expired_at']) !!}
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="file_skhp">Lampiran Cerapan<strong style="color:red;">&nbsp;*</strong></label>
                    {!! Form::file('file_skhp', null,
                        ['class' => 'form-control','id' => 'file_skhp']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="file_lampiran">Lampiran Sertifikat <strong style="color:red;">&nbsp;*</strong></label>
                    {!! Form::file('file_lampiran', null,
                        ['class' => 'form-control','id' => 'file_lampiran']) !!}
                </div>
            </div>
        </div>


        <input type="hidden" name="stat_sertifikat" id="stat_sertifikat" value="0"/>
        <button role="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan Hasil Uji</button>
        <button class="btn btn-w-md btn-success" id="btn_simpan_submit">Kirim Hasil Uji</button>
        {!! Form::close() !!}
        
        
        
       
    </div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function ()
    {
        $(".berlaku").hide();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });
        /*
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        */

        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_alg_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_mabbm_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="brand[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_alg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_alg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="input_level[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_up[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_down[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_hysteresis[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mabbm");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="meter_factor[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_ma').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_ma");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_ma').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mguwc').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mguwc");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_bkd[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td><input type="number" class="form-control" name="repeatability_bkd[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mguwc').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            $("#stat_sertifikat").val(1);

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#hasil_uji_memenuhi").on("change", function(){
            if($(this).val() ==false){
                $(".berlaku").show();
            }else{
                $(".berlaku").hide();
            }
        });
    }); 
</script>
@endsection