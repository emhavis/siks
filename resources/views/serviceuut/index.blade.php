@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-12">
                {!! Form::open(['url' => route('serviceuut.savetestqr'), 'id' => 'qr'])  !!}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="no_order">Selesai Uji Berdasar QR Code</label>
                                    {!! Form::text('no_order', null, ['class' => 'form-control','id' => 'no_order', 'autofocus' => 'autofocus']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="error_notes" clsas=""></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/>
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Kapasitas/ Dayabaca</th>
                        <th>Pengujian</th>
                        <th>Diterima Oleh</th>
                        <th>Tgl Order</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th>Status Alat</th>
                        <th>Berkas</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequestItem ? $row->ServiceRequestItem->no_order :'' }}</td>
                        <td>{{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                        <td>{{ $row && isset($row->tool_capacity_min) ? $row->tool_capacity_min : ''}}{{ $row ? $row->tool_capacity_min_unit : ''}} - {{$row ? $row->tool_capacity : ''}}{{ $row ? $row->tool_capacity_unit : ''}} / {{ $row ? $row->tool_dayabaca : ''}}{{ $row ? $row->tool_dayabaca_unit : ''}}</td>
                        <td>
                            @if($row->ServiceRequestItem)
                                @foreach($row->ServiceRequestItem->inspections as $inspection)
                                {{ $inspection->inspectionPrice->inspection_type }}
                                <br/>
                                @endforeach
                            @endif
                        </td>
                        <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name :''}}</td>
                        <!-- <td> $row->test1 ? $row->test1->full_name : ""  $row->test2 ? ' & '.$row->test2->full_name : "" </td> -->
                        <td>{{ date("d-m-Y", strtotime($row->ServiceRequestItem->order_at)) }}</td>
                        <td>{{ date("d-m-Y", strtotime($row->staff_entry_datein)) }}</td>
                        <td>{{ $row->staff_entry_dateout != null ? date("d-m-Y", strtotime($row->staff_entry_dateout)) : '' }}</td>
                        <td>
                            @if($row->ServiceRequestItem)
                            {{$row->ServiceRequestItem->status_id == 16 ? "Pengujian alat dibatalkan" :"" }} (
                            {{ $row->stat_warehouse == 2 ? 'Sudah Diserahterimakan dengan Pemohon' : 
                                ($row->stat_warehouse == 1 ? 'Berada di Gudang' : 'Berada di Instalasi')}} )
                            @endif
                        </td>
                        <td>
                            <?php if($row->ServiceRequest): ?>
                                <?php if ($row->ServiceRequest->service_type_id == 1 || $row->ServiceRequest->service_type_id == 2): ?>
                                    @if($row->ServiceRequestItem->keterangan != '' || $row->ServiceRequestItem->keterangan != null)
                                        <a id="btn_permintaan" data-permintaan="{{$row->ServiceRequestItem->keterangan}}"data-target="#kup" data-toggle="modal" class="btn btn-info btn-sm">KU Permintaan</a>
                                    @endif
                                    <?php if($row->ServiceRequestItem->file_manual_book !=''): ?>
                                        <a href="<?= config('app.siks_url') ?>/tracking/download_uut/manual_book/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Buku Manual</a>
                                    <?php endif ?>

                                    <?php if($row->ServiceRequest->booking->file_certificate !=null): ?>
                                        <a href="<?= config('app.siks_url') ?>/booking/download_permohonan/certificate/{{ $row->ServiceRequest->booking->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                                    <?php endif ?>
                                <?php elseif ($row->ServiceRequest->service_type_id == 5): ?>
                                <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Sertifikat Sebelumnya</a>
                                <?php elseif ($row->ServiceRequest->service_type_id == 1): ?>
                                <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                                <?php endif ?>
                            <?php endif ?>
                        </td>
                        <!-- <td>
                                <button class="btn btn-warning btn-sm btn-mdl" 
                                                data-id="{{ $row->ServiceRequestItem->id }}" 
                                                data-file="{{ asset($row->path_skhp) }}">TERIMA ALAT</button>
                        </td> -->
                        <td>
                        @if($row->stat_service_order=="0")
                            @if($row->pending_status=="0" || $row->pending_status==null)
                            <a href="{{ route('serviceuut.test', $row->id) }}" class="btn btn-warning btn-sm">SELESAI UJI</a>
                            <a href="{{ route('serviceuut.pending', $row->id) }}" class="btn btn-danger btn-sm">TUNDA</a>
                            @else
                            <a href="{{ route('serviceuut.continue', $row->id) }}" class="btn btn-info btn-sm">LANJUT UJI</a>
                            @endif
                            <a href="{{ route('serviceuut.cancel', $row->id) }}" class="btn btn-danger btn-sm">BATAL UJI</a>
                        @elseif($row->stat_service_order=="1")
                            @if($row->stat_sertifikat==0)
                            <a href="{{ route('serviceuut.result', $row->id) }}" class="btn btn-warning btn-sm">HASIL UJI</a>
                            @endif
                            @if($row->file_skhp!==null)
                            <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                            @if($row->kabalai_id !=null)
                            <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                            @endif
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                            <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                            @endif
                            <a href="{{ route('serviceuut.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD CERAPAN</a>
                                @if($row->file_lampiran!==null)
                                <a href="{{ route('serviceuut.downloadLampiran', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                @endif
                            @endif
                            <!-- @if($row->stat_warehouse ==null || $row->stat_warehouse ==0)
                                <a href="{{ route('serviceuut.warehouse', $row->id) }}" class="btn btn-warning btn-sm">GUDANG ALAT SELESAI</a>
                            @endif -->
                        @elseif($row->stat_service_order=="2")
                            <a href="{{ route('serviceuut.result', $row->id) }}" class="btn btn-warning btn-sm">HASIL UJI</a>
                            @if($row->file_skhp!==null)
                                <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                @if($row->kabalai_id !=null)
                                <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                @endif
                                @if($row->ujitipe_completed==true)
                                    <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                @endif
                                <a href="{{ route('file.show', [$row->id,'cerapan']) }}" class="btn btn-warning btn-sm">LIHAT CERAPAN</a>
                                @if($row->file_lampiran!==null)
                                <a href="{{ route('file.show', [$row->id,'lampiran']) }}" class="btn btn-warning btn-sm">LIHAT LAMPIRAN</a>
                                @endif
                            @endif

                            <!-- @if($row->stat_warehouse =="0" || $row->stat_warehouse ==null)
                                <a href="{{ route('serviceuut.warehouse', $row->id) }}" class="btn btn-warning btn-sm">GUDANG ALAT SELESAI</a>
                            @endif -->
                        @elseif($row->stat_service_order=="4")
                            <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">SURAT PEMBATALAN</a>
                            @if($row->kabalai_id !=null)
                            <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                            @endif
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                            <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                            @endif
                        @elseif($row->is_finish=="0")
                            @if($row->file_skhp!==null)
                                <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                @if($row->kabalai_id !=null)
                                <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT</a>
                                @endif
                                @if($row->ujitipe_completed==true)
                                    <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                @endif
                                <a href="{{ route('file.show', [$row->id,'cerapan']) }}" class="btn btn-warning btn-sm">LIHAT CERAPAN</a>
                                @if($row->file_lampiran!==null)
                                <a href="{{ route('file.show', [$row->id,'lampiran']) }}" class="btn btn-warning btn-sm">LIHAT LAMPIRAN</a>
                                @endif
                            @endif
                            <!-- @if($row->stat_warehouse =="0" || $row->stat_warehouse ==null)
                                <a href="{{ route('serviceuut.warehouse', $row->id) }}" class="btn btn-warning btn-sm">GUDANG ALAT SELESAI</a>
                            @endif -->
                        @elseif($row->is_finish=="1")
                        SELESAI
                        @endif
                        <a href="{{ route('document.download', $row->id) }}" class="btn btn-warning btn-sm">Kaji Ulang</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- <div class="modal fade" id="viewer" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">PDF Viewer</h4>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src=""></iframe>
                        </div>
                        <object type="application/pdf" data="" width="100%" height="500" style="height: 85vh;">No Support</object>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">TERIMA</button>
            </div>
        </div>
    </div>
</div> -->

<!-- Modal -->
    <!-- <div id="myModal1" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                 Modal content
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">

                        <embed src="" frameborder="0" width="100%" height="400px">

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> -->

    <!-- Kaji Ulang Permintaan -->
    <div class="modal fade" id="kup" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="m-t-none">Kaji Ulang Permintaan</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="embed-responsive embed-responsive-4by3">
                                <textarea name="txt_permintaan" id="txt_permintaan" style="width:100%;" rows="30">{{print_r('PErmintaan')}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ok</button>
                </div>
            </div>
        </div>
    </div>
    <!--  -->
  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">

                        <embed src="" id="embed1"name="embed1"
                               frameborder="0" width="100%" height="400px">

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){

    $("#data_table tbody").on("click","button.btn-mdl",function(e){
        e.preventDefault();
        var fileForView = $(this).data().file;
        var isi =$(".embed-responsive-item").src =fileForView;
        var data = $("#embed1").src =fileForView;

        $("#myModal").modal().on('shown.bs.modal', function ()
            {
                console.log($("#embed1").src)
            });
    });

    $('#data_table').DataTable({
        scrollX: true,
    });

    $("#no_order").change(function() {
        var no_order = $(this).val();

        var form = $('#qr');
        form[0].submit();

        /*
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{ route('orderuttp.proses') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
        */
    });

    $('#kup').on('show.bs.modal', function(e) {
        var $modal = $(this);
        var $button = $(e.relatedTarget);
        var $notifications = $button.siblings('div.hidden');
        let s = $button.data('permintaan');
        let area = $('#txt_permintaan').text(s);
    });
});

</script>
@endsection