<html>
<head>
<title>SKHP</title>
<style type="text/css">

/* Styling report go here */

.page-header, .page-header-space {
  /* height: 100px; */
  height: 0;
}

.page-footer, .page-footer-space {
  height: 40px;
}

.page-footer {
  position: fixed;
  bottom: 0;
  width: 100%;
  border-top: 1px solid black; /* for demo */
  background: white;
  font-family: Arial;
  font-size:10pt;
  /* margin-left: 8mm; */
}

.page-header {
  position: fixed;
  top: 0mm;
  width: 100%;
  border-bottom: 1px solid black; /* for demo */
  background: white; /* for demo */
}

.page {
  page-break-after: always;
  /* font-family: Helvetica;
   */
  font-family: Arial;
  font-size:11pt;
  padding:8mm 8mm 0 8mm;
  /* border-bottom: 1px solid #999; */
  min-height: 26cm;
  margin-bottom: 2cm;
  /* border: #999 solid 1px; */
}

.title {
    font-family: Arial;
    font-size:14pt;
    text-decoration:underline;
}

/* .right {
  position: absolute;
  right: 0px;
  width: 300px;
} */

.right {
  position: absolute;
  right: 0px;
  width: 400px;
}

/* th, td {
  padding-top: 8px;
} */

.table {
  padding-top: 8px;
}
.table th {
  padding-top: 8px;
}
.table td{
 vertical-align: top;
 font-family: Arial;
 font-size:10pt;
  margin: 14px;
  padding-top:14px;
}

.table1 {
  font-size: 9pt;;
  padding-top: 1px;
}
.table1 th{
  font-family: Arial;
  border: 1px solid #999;
  padding-top:8px;
}
.table1 td{
  border: 1px solid #999;
  font-family: Arial;
  /* border-collapse: collapse; */
  padding-top:1px;
}


.table-border {
    border: 1px solid #999;
    border-collapse: collapse;
}

.table-foot {
    font-family: Arial;
    font-size:11pt;
    padding-top: 6px;
    /* font-weight: bold; */
}

#watermark {
    position: fixed;
    font-size: 150pt;
    font-weight: bold;
    color: #ccc;
    top: 30%;
    width: 100%;
    text-align: center;
    opacity: .6;
    transform: rotate(-10deg);
    transform-origin: 50% 50%;
    z-index: -1000;
}

@media print {
   thead {display: table-header-group;}
   tfoot {display: table-footer-group;}

   button {display: none;}

   .page{border: #fff solid 0px;}

   body {margin: 0;}
}
 /* End of stiling page */
</style>
</head>
<body>
    @if($order->stat_service_order!="3")
    <div id="watermark">DRAFT</div>
    @endif

    <table border="0">
        <tr>
            <td style="width: 20mm;">
                <img src="{{ $view ? asset('assets/images/logo/logo_kemendag.png') : public_path('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
            </td>
            <td>
                <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN<br />TERTIB NIAGA DIREKTORAT METROLOGI</p>
                <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
            </td>
        </tr>
    </table>

    <table width="100%" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td>
            <!--*** CONTENT GOES HERE ***-->
            <div class="header" style="float: right; height: 30px; padding-right:200px; font-size:18pt;">
            <center>{{ $order->no_service }}</center>
            </div>
            <div class="page">
                <center>
                    <div class="title">SURAT KETERANGAN HASIL PENGUJIAN</div>
                    Nomor: {{ $order->no_sertifikat }}</p>
                </center>
                <br/><br/>
                <table class="table" width="100%" cellpading="0" cellspacing="0">
                    <tr>
                        <td>Jenis UTTP</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                    </tr>

                    @if ($order->ServiceRequestItem->uttp->tool_capacity_min > 0)
                    <tr>
                        <td>Kapasitas Minimum</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequestItem->uttp->tool_capacity_min }} {{ $order->ServiceRequestItem->uttp->tool_capacity_unit }}</td>
                    </tr>
                    @endif
                    <tr>
                        <td>Kapasitas Maksimum</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequestItem->uttp->tool_capacity }} {{ $order->ServiceRequestItem->uttp->tool_capacity_unit }}</td>
                    </tr>
                    
                    <tr>
                        <td>Merek/Model/Tipe</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequestItem->uttp->tool_brand . '/' . 
                        $order->ServiceRequestItem->uttp->tool_model }}</td>
                    </tr>
                    <tr>
                        <td>Buatan</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequestItem->uttp->tool_made_in }}</td>
                    </tr>
                    <tr>
                        <td>Pemohon</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequest->label_sertifikat }}</td>
                    </tr>
                    <tr>
                        <td>Alamat Pemohon</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequest->addr_sertifikat }}</td>
                    </tr>
                    <!--
                    <tr>
                        <td>Nama Pabrikan</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequestItem->uttp->tool_factory }}</td>
                    </tr>
                    <tr>
                        <td>Alamat Pabrikan</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequestItem->uttp->tool_factory_address }}</td>
                    </tr>
                    -->
                    @if($order->cancel_at == null)
                    <tr>
                        <td rowspan="2" style="vertical-align: top;">Diuji Oleh</td>
                        <td rowspan="2" style="vertical-align: top;">:</td>
                        <td>{{ $order->TestBy1 != null ? $order->TestBy1->full_name : '' }}</td>
                        <td>NIP</td>
                        <td>:</td>
                        <td>{{ $order->TestBy1 != null ? $order->TestBy1->nip : '' }}</td>
                    </tr>
                    <tr>
                        <td>{{ $order->TestBy2 != null ? $order->TestBy2->full_name : '' }}</td>
                        <td>NIP</td>
                        <td>:</td>
                        <td>{{ $order->TestBy2 != null ? $order->TestBy2->nip : '' }}</td>
                    </tr>
                    <tr>
                        <td>Waktu Pengujian</td>
                        <td>:</td>
                        <td colspan="4">
                        @if($order->stat_service_order == 4)
                        {{ date("d M Y", strtotime($order->staff_entry_datein)) . ' - ' . date("d M Y", strtotime($order->cancel_at)) }}
                        @else
                            {{ date("d M Y", strtotime($order->staff_entry_datein)) . ' - ' . date("d M Y", strtotime($order->staff_entry_dateout)) }}
                        @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">Dasar Pengujian</td>
                        <td style="vertical-align: top;">:</td>
                        <td colspan="4">Surat Permohonan dari {{ $order->ServiceRequest->label_sertifikat }}
                            <br/>
                            Nomor: {{ $order->ServiceRequestItem->reference_no }}
                            <br/>
                            Tanggal: {{ date("d M Y", strtotime($order->ServiceRequestItem->reference_date)) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Persyaratan Teknis</td>
                        <td>:</td>
                        <td colspan="4">{!! $order->ServiceRequestItem->uttp->type->syarat_teknis !!}</td>
                    </tr>
                    @endif
                    <tr>
                        <td style="vertical-align: top;">Hasil</td>
                        <td style="vertical-align: top;">:</td>
                        <td colspan="4">
                        @if($order->cancel_at != null)
                        Pengujian tidak dapat dilakukan atau dilanjutkan karena {{ $order->cancel_notes }}.
                        @else
                        <strong>{{ $order->ServiceRequestItem->uttp->type->uttp_type }}</strong> dengan Merek <strong>{{ $order->ServiceRequestItem->uttp->tool_brand }}</strong> tipe <strong>{{ $order->ServiceRequestItem->uttp->tool_model }}</strong> dinyatakan <strong>{{ $order->hasil_uji_memenuhi ? 'Memenuhi' : 'Tidak Memenuhi'}} Syarat Teknis</strong>,
                        untuk jenis-jenis pengujian terlampir pada Resume Evaluasi Tipe UTTP  yang merupakan bagian tidak terpisahkan dari Surat Keterangan Hasil Pengujian ini. 
                        @endif
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <div class="right">
                    <table cellpading="0" cellspacing="0">
                        <tr>
                            <td></td>
                            <td>Bandung, {{ $order->kabalai_date ? date("d M Y", strtotime($order->kabalai_date)) : date("d M Y") }}</td>
                        </tr>
                        <tr>
                            <td>a.n</td>
                            <td>Direktur Metrologi</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Kepala Balai Pengujian UTTP</td>
                        </tr>
                        <tr style="height: 50px;">
                            <td></td>
                            <td>
                                @if($order->KaBalai != null)
                                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(123)->margin(0.5)->generate($qrcode_generator)) !!} ">
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>{{ $order->KaBalai != null ? $order->KaBalai->full_name : '' }}</td>
                            <!--
                            <td>Usman, S.Si., M.Si.</td>
                            -->
                        </tr>
                    </table>
                </div>
            </div>
            @if($order->cancel_at == null)
            <div class="page">
                <center>
                    <div class="title">RESUME EVALUASI TIPE UTTP</div>
                    Nomor: {{ $order->no_sertifikat }}</p>
                </center>

                <br/><br/>
                <table class="table1" width="100%" cellpading="0" cellspacing="0">
                    <tr style="text-align: center; width:2px;">
                        <th rowspan="2">NO</th>
                        <th rowspan="2">PEMERIKSAAN &amp; PENGUJIAN</th>
                        <th colspan="3">PEMENUHAN SYARAT</th>
                        <th rowspan="2">KETERANGAN</th>
                    </tr>
                    <tr style="text-align: center; width:2px;">
                        <th>YA</th>
                        <th>TIDAK</th>
                        <th>N/A</th>
                    </tr>

                    @foreach($order->inspections as $inspection)
                    <tr>
                        <td style="text-align: center; width:2px;">
                            @if($inspection->inspectionItem->is_header)
                            {{ $inspection->inspectionItem->no }}
                            @endif
                        </td>
                        <td>
                            @if($inspection->inspectionItem->is_header)
                            {{ $inspection->inspectionItem->name }}
                            @else
                            {{ $inspection->inspectionItem->no }} {{ $inspection->inspectionItem->name }}
                            @endif
                        </td>
                        <td style="text-align: center;">
                            @if($inspection->is_accepted && $inspection->inspectionItem->is_tested)
                            X
                            @endif
                        </td>
                        <td style="text-align: center;">
                            @if($inspection->is_accepted != null && !$inspection->is_accepted && $inspection->inspectionItem->is_tested)
                            X
                            @endif
                        </td>
                        <td style="text-align: center;">
                            @if($inspection->is_accepted == null && $inspection->inspectionItem->is_tested)
                            X
                            @endif
                        </td>
                        <td></td>
                    </tr>
                    @endforeach
                </table>

                <br/>
                <table class="table-foot" style="border: none;" cellpading="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">Bandung, {{ $order->kabalai_date ? date("d M Y", strtotime($order->kabalai_date)) : date("d M Y") }}</td>
                    </tr>
                    <tr>
                        <td width="60%">Penguji Tipe UTTP</td>
                        <td>Direviu Oleh</td>
                    </tr>
                    <tr>
                        <td>1. {{ $order->TestBy1->full_name }}</td>
                        <td>Sub Koordinator Pengujian Tipe</td>
                    </tr>
                    <tr>
                        <td>2. {{ $order->TestBy2->full_name }}</td>
                        <td>{{ $order->SupervisorStaff ? $order->SupervisorStaff->full_name : '' }}</td>
                    </tr>
                    <tr>
                        <td>
                            @if($order->KaBalai != null)
                            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(123)->margin(0.5)->generate($qrcode_generator)) !!} ">
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
            @endif
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
