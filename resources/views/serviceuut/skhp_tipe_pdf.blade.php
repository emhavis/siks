<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Sertifikat {{ $order->ServiceRequestItem->ServiceRequest->svc ? $order->ServiceRequestItem->ServiceRequest->svc->service_type :'' }}</title>

    <style>
        
        /* paper css */
        .ohm {
            content: "\2126";
            }
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: Arial;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
            /* border :2px solid;
            color :red; */
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        /* @media print {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }

            button {display: none;}

            .page{border: #fff solid 0px;}

            body {margin: 0;}
        } */
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-nodraft-snsu.png") : public_path("assets/images/logo/letterhead-nodraft-snsu.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }

        .letterhead_kan {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-nodraft-snsu-kan.png") : public_path("assets/images/logo/letterhead-nodraft-snsu-kan.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;

        }

        .letterhead-draft_kan {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-draft-snsu-kan.png") : public_path("assets/images/logo/letterhead-draft-snsu-kan.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;

        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-draft-snsu.png") : public_path("assets/images/logo/letterhead-draft-snsu.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            opacity: 0.6;
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
        .table td .eng {
            font-size:8pt;
            font-style:italic;
        }

        .eng {
            font-size:10pt;
            font-style:italic;
        }

        #underline-gap {
            text-decoration: underline;
            text-underline-position: under;
        }
    </style>
</head>

<body class="A4">
    @if(($order->cancel_at == null && $order->cancel_notes ==null && $order->hasil_uji_memenuhi ==null) || $order->hasil_uji_memenuhi ==true)
        @if($order->is_certified == true || $order->is_certified == 1)
        <section class="sheet {{ $order->kabalai_date ? 'letterhead_kan' : 'letterhead-draft_kan' }}"> 
        @else
        <section class="sheet {{ $order->kabalai_date ? 'letterhead' : 'letterhead-draft' }}"> 
        @endif

            <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm; margin-bottom:5mm; ">{{ $order->ServiceRequestItem->no_order != null ? $order->ServiceRequestItem->no_order : 'X-XXX-XXX'}}</div>

            <div class="text-center">
                <div class="title" style="border:0px solid; margin-left: -20;">
                    <strong>SERTIFIKAT {{ strtoupper($order->ServiceRequestItem->ServiceRequest->svc ? $order->ServiceRequestItem->ServiceRequest->svc->service_type :'') }}</strong>
                </div>
                <div class="subtitle" style="border:0px solid; margin-left: -20;">
                    <i>{{ strtoupper($order->ServiceRequestItem->ServiceRequest->svc->service_type) =='VERIFIKASI' ? 'Verification' :'Calibration'}} certificate </i>
                </div>
            </div>
            </br>
            <div class="text-center" style="border:0px solid; margin-left: -20;">
                <div class="subtitle"><strong>Nomor : {{ $order->no_sertifikat !='' ? $order->no_sertifikat : ' 0000/00/00/00/00 '}}</strong></div>
            </div>

            <br/><br/>
        
            <table class="table table-borderless" width="100%" cellpadding="2px" cellspacing="0"s
            style="padding-left: 25.4mm; padding-right: 25.4mm; ">
                <tbody style="padding-bottom: 25.4mm; margin-bottom:40px">
                    <tr style="border-spacing: 40px;">
                        <td class="title" style="min-width: 40mm;"><strong> Nama Alat </strong><div class="eng">Measuring Instrument</div></td>
                        <td style="width: 3mm;">:</td>
                        <td colspan="4" class="title"><strong>{{ $order->tool_name }}</strong></td>
                    </tr>
                    <tr style="height: 10px;">
                        <td></td>
                        <td></td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px;">Merk / Buatan<div class="eng">Trade Mark / Manufactured by</div></td>
                        <td>:</td>
                        <td colspan="4">{{ $order->tool_brand }} / {{$order->tool_made_in ? $order->tool_made_in : '-'}}</td>
                    </tr>
                    
                    <tr>
                        <td style="padding-left: 15px;">Model/Tipe<div class="eng">Model/Type</div></td>
                        <td>:</td>
                        <td colspan="4">{{ $order->tool_model }}</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px;">No. Seri/No.Identitas<div class="eng">Series Number</div></td>
                        <td>:</td>
                        <td colspan="4">{{ $order->tool_serial_no }}</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px;">Kapasitas / Daya Baca<div class="eng">Maximum Capacity</div></td>
                        <td>:</td>
                        <td colspan="4">
                            @if(($order->tool_capacity_min || $order->tool_dayabaca != null) && $order->ServiceRequestItem->uuts->stdtype->id == 210)
                            <?php
                                $source = strlen($order->tool_capacity) > 5 ? $order->tool_capacity : $order->ServiceRequestItem->uuts->tool_capacity;
                                $strArray = explode(';', trim($source));

                                $source2 = strlen($order->tool_dayabaca) > 5 ? $order->tool_dayabaca : $order->ServiceRequestItem->uuts->tool_dayabaca;
                                $strArray2 = explode(';', trim($source2));

                                $arrNew = Array();

                                $ksH ='';
                                $ksT ='';
                                $dcH = '';
                                $dcT = '';
                                foreach($strArray  as $key=>$val){
                                    if((str_contains($val,'T:' ) || str_contains($val,'t:')) && $key == 0 ){
                                        $ksT = $ksT.$val;
                                    }else if((!str_contains($val,'T:' ) || !str_contains($val,'H:')) && $key == 0){
                                        $ksT = $ksT.'T:'.$val;
                                    }else
                                    if((str_contains($val,'H:' ) || str_contains($val,'h:')) && $key > 0){
                                        $ksH =  $ksH.$val;
                                    }else if((!str_contains($val,'H:' ) || !str_contains($val,'T:')) && $key == 1){
                                        $ksH = $ksH.'H:'.$val;
                                    }
                                }

                                foreach($strArray2  as $key=>$val){
                                    if((str_contains($val,'T:' ) || str_contains($val,'t:')) && $key == 0 ){
                                        $ksT = $ksT.' / '.$val.';';
                                    }else if((!str_contains($val,'T:' ) || !str_contains($val,'H:')) && $key == 0){
                                        $ksT = $ksT.' / T:'.$val.';';
                                    }else
                                    if((str_contains($val,'H:' ) || str_contains($val,'h:')) && $key > 0){
                                        $ksH =  $ksH.' / '.$val.';';
                                    }else if((!str_contains($val,'H:' ) || !str_contains($val,'T:')) && $key == 1){
                                        $ksH = $ksH.' / H:'.$val.';';
                                    }
                                }

                                $kapasitas = $ksT.$ksH;//$order->tool_capacity_min ? $order->tool_capacity_min : "-" ;
                                $kapasitas = str_replace(';',"</br>",$kapasitas);
                                $dayabaca = $order->tool_dayabaca;
                                $databaca = str_replace(';',"</br>",$dayabaca);
                                echo($kapasitas);
                                // echo("</br> Daya baca : </br>");
                                // echo($dayabaca);
                            ?>
                            @else
                                @if($order->tool_capacity_min !=null || $order->tool_capacity_min >0) {{ $order->tool_capacity_min }} {{ $order->tool_capacity_min_unit }} - @endif
                                {{ $order->tool_capacity }} {{ $order->tool_capacity_unit }} / {{ $order->tool_dayabaca }} {{ $order->tool_dayabaca_unit }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px;">Kelas / Jumlah<div class="eng">Class </div></td>
                        <td>:</td>
                        <td colspan="4">{{ $order->tool_class ? $order->tool_class :'-' }} @if($order->ServiceRequest->service_type_id == 1 && $order->hasil_uji_memenuhi == false) (Spesifikasi Pabrik) @endif / {{$order->tool_jumlah}}</td>
                    </tr>
                    <tr style="height: 30mm;">
                        <td></td>
                        <td></td>
                        <td colspan="4"></td>
                    </tr>
                    <!-- <tr>
                        <td class="title"><strong>Pemilik/Pemakai</strong><div class="eng">Owner/User</div></td>
                        <td>:</td>
                        <td class ="title" colspan="4"><strong>{{ $order->ServiceRequest->label_sertifikat }}</strong></td>
                    </tr> -->
                    <tr>
                            <td width="20%" style="font-size: 16px !important; font-weight:regular;"><strong>Pemilik</strong><div class="eng">Owner/User</div></td>
                            <td width="2%" style="font-size: 16px !important; font-weight:bold;">:</td>
                            <td style="font-size: 16px !important; font-weight:bold;" colspan="4">
                                {{ strtoupper($order->ServiceRequest->label_sertifikat) }}<br>
                                {{ $order->ServiceRequest->addr_sertifikat }}
                            </td>
                    </tr>
                    
                    <tr colspan="4" style="height: 30mm;">
                        <td><br/></td>
                        <td></td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            @if($order->ServiceRequestItem->ServiceRequest->svc->id !=2)
                                <strong>
                                    Sertifikat SUML ini berlaku sampai dengan {{ ($order->sertifikat_expired_at) ? into_tanggal($order->sertifikat_expired_at) :  into_tanggal(date('d F Y'), strtotime('1 Years')) }}
                                </strong>
                                <div class="eng">This certificate is valid until {{ ($order->sertifikat_expired_at) ? date('F d, Y', strtotime($order->sertifikat_expired_at)) :  date('F d, Y', strtotime('1 Years')) }}</div>
                            @endif
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr style="height: 30mm;">
                        <td></td>
                        <td></td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td colspan="4"><strong>Sertifikat ini terdiri dari {{$order->page_num ? $order->page_num:2}} ( {{to_words_idn($order->page_num ? $order->page_num:2)}} ) halaman</strong> <div class="eng">This certificate consist of {{$order->page_num ? $order->page_num:2}} ( {{to_words($order->page_num ? $order->page_num:2)}} ) pages</div></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr style="height: 30mm;">
                        <td></td>
                        <td></td>
                        <td colspan="4"></td>
                    </tr>

                    <!-- <tr>
                        <td>Hasil<div class="eng">Result</div></td>
                        <td>:</td>
                        <td colspan="4">
                            @if($order->hasil_uji_memenuhi == true)
                            Disahkan berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal.
                            @else
                            Batal berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal. 
                            @endif  
                        </td>
                    </tr> -->
                </tbody>
            <table>

            <br/>
            <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
                <tbody>
                    <tr>
                        <td class="text-left">Bandung, {{ $order->kabalai_date ? into_tanggal($order->kabalai_date) : into_tanggal(date("d F Y")) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Kepala Balai Pengelolaan </td>
                    </tr>
                    <tr>
                        <td class="text-left">Standar Ukuran Metrologi Legal,</td>
                    </tr>
                    <tr>
                        <td class="text-left" style="height: 30mm; padding: top 4px;">
                            @if($order->KaBalai != null)
                            <img src='data:image/png;base64, {!! 
                                base64_encode(QrCode::format("png")
                                    ->size(100)
                                    ->margin(0.5)
                                    ->errorCorrection("H")
                                    ->generate($qrcode_generator)) !!} '>
                            @endif
                        </td>
                    </tr>
                    <tr style="padding-top: -10px !important;">
                        <td class="text-left" style="padding-top: -19px !important;">
                            {{ $order->KaBalai != null && $order->KaBalai->PetugasUut ? $order->KaBalai->PetugasUut->nama : 'Kepala Balai' }}</br>
                            <!-- NIP:  $order->KaBalai != null && $order->KaBalai->PetugasUut ? $order->KaBalai->PetugasUut->nip : ''  -->
                        </td>
                    </tr>
                </tbody>
            </table>
        </section>
    @endif
    @if($order->cancel_at != null && $order->cancel_notes != null)
    <section class="sheet {{ $order->kabalai_date ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">
        {{ $order->ServiceRequestItem->no_order != null ? $order->ServiceRequestItem->no_order : 'X-XXX-XXX'}}
        </div>

        <div class="text-center">
            <div class="title">
                <strong><u>SURAT KETERANGAN</u></strong>
            </div>
            <!-- <div class="subtitle">
                <i>{{ strtoupper($order->ServiceRequestItem->ServiceRequest->svc->service_type) =='VERIFIKASI' ? 'Verification' :'Calibration'}} certificate </i>
            </div> -->
        </div>
        <div class="text-center">
            <div class="subtitle"><strong>Nomor : /PKTN.4.7 /{{ $order->cancel_at !='' ? date("m",strtotime($order->cancel_at)).'/'.date("Y",strtotime($order->cancel_at)) : ' 0000/00/00/00/00 '}}</strong></div>
        </div>

        <br/><br/>
    
        <table class="table table-borderless" width="100%" cellpadding="2px" cellspacing="0"s
         style="padding-left: 25.4mm; padding-right: 25.4mm; ">
            <tbody style="padding-bottom: 25.4mm; margin-bottom:40px">
                <tr style="border-spacing: 40px;">
                    <td class="title" style="min-width: 40mm;"><strong> Nama Alat </strong><div class="eng">Measuring Instrument</div></td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="4" class="title"><strong>{{ $order->tool_name }}</strong></td>
                </tr>
                <tr style="height: 10px;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Merk / Buatan<div class="eng">Trade Mark / Manufactured by</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_brand }} / {{$order->tool_made_in ? $order->tool_made_in : '-'}}</td>
                </tr>
                
                <tr>
                    <td style="padding-left: 15px;">Model/Tipe<div class="eng">Model/Type</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_model }} / {{ $order->tool_type ? $order->tool_type : '-' }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">No. Seri/No.Identitas <div class="eng">Series Number</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_serial_no }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Kapasitas / Daya Baca<div class="eng">Maximum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">
                        @if($order->tool_capacity_min !=null || $order->tool_capacity_min >0) {{ $order->tool_capacity_min }} {{ $order->tool_capacity_min_unit }} - @endif
                        {{ $order->tool_capacity }} {{ $order->tool_capacity_unit }} / {{ $order->tool_dayabaca }} {{ $order->tool_dayabaca_unit }}
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Kelas / Jumlah<div class="eng">Class </div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_class ? $order->tool_class :'-' }} @if($order->ServiceRequest->service_type_id == 1 && $order->hasil_uji_memenuhi == false) (Spesifikasi Pabrik) @endif / {{$order->tool_jumlah}}</td>
                </tr>
                <tr style="height: 30mm;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td class="title">
                        <!-- <strong>Pemilik/Pemakai</strong><div class="eng">Owner/User</div> -->
                    </td>
                    <td>:</td>
                    <td class ="title" colspan="4">
                        <!-- <strong>{{ $order->ServiceRequest->label_sertifikat }}</strong> -->
                    </td>
                </tr>
                <tr>
                        <td width="20%" style="font-size: 16px !important; font-weight:regular;"><strong>Pemilik</strong><div class="eng">Owner/User</div></td>
                        <td width="2%" style="font-size: 16px !important; font-weight:bold;">:</td>
                        <td style="font-size: 16px !important; font-weight:bold;" colspan="12">
                            {{ strtoupper($order->ServiceRequest->label_sertifikat) }}<br>
                            {{ $order->ServiceRequest->addr_sertifikat }}
                        </td>
                </tr>
                
                <tr colspan="4" style="height: 30mm;">
                    <td><br/></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="12">
                        <!-- if($order->ServiceRequestItem->ServiceRequest->svc->id !=2) -->
                        <p>
                            Alat tersebut tidak dapat diterbitkan sertifikat karena {{$order->cancel_notes}}
                            <!-- tidak memenuhi persyaratan kemetrologian pada SK Dirjen PKTN 
                            No 22 Tahun 2021 ttg ST SUML Besaran Panjang (hasil terlampir)  -->
                        </p>
                        </br>
                            Demikian Surat Keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.
                            <!-- <div class="eng">This certificate is valid until {{ ($order->sertifikat_expired_at) ? date('F d, Y', strtotime($order->sertifikat_expired_at)) :  date('F d, Y', strtotime('1 Years')) }}</div> -->
                        <!-- endif -->
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height: 30mm;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <!-- <strong>Sertifikat ini terdiri dari 2 (dua) halaman</strong> <div class="eng">This certificate consist of 2 (two) pages</div> -->
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height: 30mm;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
            </tbody>
        <table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td class="text-left">Bandung, {{ $order->kabalai_date ? into_tanggal($order->kabalai_date) : into_tanggal(date("d F Y")) }}</td>
                </tr>
                <tr>
                    <td class="text-left">Kepala Balai Pengelolaan Standar</td>
                </tr>
                <tr>
                    <td class="text-left">Nasional Satuan Ukuran,</td>
                </tr>
                <tr>
                    <td class="text-left" style="height: 30mm; padding: top 4px;">
                        @if($order->KaBalai != null)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr style="padding-top: -10px !important;">
                    <td class="text-left" style="padding-top: -19px !important;">
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUut ? $order->KaBalai->PetugasUut->nama : 'Kepala Balai' }}</br>
                        <!-- NIP:  $order->KaBalai != null && $order->KaBalai->PetugasUut ? $order->KaBalai->PetugasUut->nip : ''  -->
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
    @endif

    @if($order->cancel_notes !=null && $order->cancel_at == null)
    <section class="sheet {{ $order->kabalai_date ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">
        {{ $order->ServiceRequestItem->no_order != null ? $order->ServiceRequestItem->no_order : 'X-XXX-XXX'}}
        </div>

        <div class="text-center">
            <div class="title">
                <strong><u>SURAT KETERANGAN</u></strong>
            </div>
            <!-- <div class="subtitle">
                <i>{{ strtoupper($order->ServiceRequestItem->ServiceRequest->svc->service_type) =='VERIFIKASI' ? 'Verification' :'Calibration'}} certificate </i>
            </div> -->
        </div>
        <div class="text-center">
            <div class="subtitle"><strong>Nomor : /PKTN.4.7 /{{ $order->cancel_at !='' ? date("m",strtotime($order->cancel_at)).'/'.date("Y",strtotime($order->cancel_at)) : ' 0000/00/00/00/00 '}}</strong></div>
        </div>

        <br/><br/>
    
        <table class="table table-borderless" width="100%" cellpadding="2px" cellspacing="0"s
         style="padding-left: 25.4mm; padding-right: 25.4mm; ">
            <tbody style="padding-bottom: 25.4mm; margin-bottom:40px">
                <tr style="border-spacing: 40px;">
                    <td class="title" style="min-width: 40mm;"><strong> Nama Alat </strong><div class="eng">Measuring Instrument</div></td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="4" class="title"><strong>{{ $order->ServiceRequestItem->uuts->tool_name }}</strong></td>
                </tr>
                <tr style="height: 10px;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Merk / Buatan<div class="eng">Trade Mark / Manufactured by</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_brand }} / {{$order->ServiceRequestItem->uuts->tool_made_in ? $order->ServiceRequestItem->uuts->tool_made_in : '-'}}</td>
                </tr>
                
                <tr>
                    <td style="padding-left: 15px;">Model/Tipe<div class="eng">Model/Type</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_model }} / {{ $order->ServiceRequestItem->uuts->tool_type ? $order->ServiceRequestItem->uuts->tool_type : '-' }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">No. Seri/No.Identitas<div class="eng">Series Number</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->serial_no }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Kapasitas / Daya Baca<div class="eng">Maximum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">
                            @if($order->ServiceRequestItem->uuts->tool_capacity_min !=null || $order->ServiceRequestItem->uuts->tool_capacity_min >0) {{ $order->ServiceRequestItem->uuts->tool_capacity_min }} {{ $order->ServiceRequestItem->uuts->tool_capacity_min_unit }} - @endif
                            {{ $order->ServiceRequestItem->uuts->tool_capacity }} {{ $order->ServiceRequestItem->uuts->tool_capacity_unit }} / {{ $order->ServiceRequestItem->uuts->tool_dayabaca }} {{ $order->ServiceRequestItem->uuts->tool_dayabaca_unit }}
                        <!-- endif -->
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Kelas / Jumlah<div class="eng">Class </div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uuts->class ? $order->ServiceRequestItem->uuts->class :'-' }} @if($order->ServiceRequest->service_type_id == 1 && $order->hasil_uji_memenuhi == false) (Spesifikasi Pabrik) @endif / {{$order->tool_jumlah}}</td>
                </tr>
                <tr style="height: 30mm;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td class="title">
                        <!-- <strong>Pemilik/Pemakai</strong><div class="eng">Owner/User</div> -->
                    </td>
                    <td>:</td>
                    <td class ="title" colspan="4">
                        <!-- <strong>{{ $order->ServiceRequest->label_sertifikat }}</strong> -->
                    </td>
                </tr>
                <tr>
                        <td width="20%" style="font-size: 16px !important; font-weight:regular;"><strong>Pemilik</strong><div class="eng">Owner/User</div></td>
                        <td width="2%" style="font-size: 16px !important; font-weight:bold;">:</td>
                        <td style="font-size: 16px !important; font-weight:bold;" colspan="12">
                            {{ strtoupper($order->ServiceRequest->label_sertifikat) }}<br>
                            {{ $order->ServiceRequest->addr_sertifikat }}
                        </td>
                </tr>
                
                <tr colspan="4" style="height: 30mm;">
                    <td><br/></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="12">
                        <!-- if($order->ServiceRequestItem->ServiceRequest->svc->id !=2) -->
                        <p>
                            Alat tersebut tidak dapat diterbitkan sertifikat karena {{$order->cancel_notes}}
                            <!-- tidak memenuhi persyaratan kemetrologian pada SK Dirjen PKTN 
                            No 22 Tahun 2021 ttg ST SUML Besaran Panjang (hasil terlampir)  -->
                        </p>
                        </br>
                            Demikian Surat Keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.
                            <!-- <div class="eng">This certificate is valid until {{ ($order->sertifikat_expired_at) ? date('F d, Y', strtotime($order->sertifikat_expired_at)) :  date('F d, Y', strtotime('1 Years')) }}</div> -->
                        <!-- endif -->
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height: 30mm;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <!-- <strong>Sertifikat ini terdiri dari 2 (dua) halaman</strong> <div class="eng">This certificate consist of 2 (two) pages</div> -->
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height: 30mm;">
                    <td></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
            </tbody>
        <table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td class="text-left">Bandung, {{ $order->kabalai_date ? into_tanggal($order->kabalai_date) : into_tanggal(date("d F Y")) }}</td>
                </tr>
                <tr>
                    <td class="text-left">Kepala Balai Pengelolaan Standar</td>
                </tr>
                <tr>
                    <td class="text-left">Nasional Satuan Ukuran,</td>
                </tr>
                <tr>
                    <td class="text-left" style="height: 30mm; padding: top 4px;">
                        @if($order->KaBalai != null)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr style="padding-top: -10px !important;">
                    <td class="text-left" style="padding-top: -19px !important;">
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUut ? $order->KaBalai->PetugasUut->nama : 'Kepala Balai' }}</br>
                        <!-- NIP:  $order->KaBalai != null && $order->KaBalai->PetugasUut ? $order->KaBalai->PetugasUut->nip : ''  -->
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
    @endif

</body>

</html>