<html>
<head>
<title>SKHP</title>
<style type="text/css">

/* Styling report go here */

.page-header, .page-header-space {
  /* height: 100px; */
  height: 0;
}

.page-footer, .page-footer-space {
  height: 40px;
}

.page-footer {
  position: fixed;
  bottom: 0;
  width: 100%;
  border-top: 1px solid black; /* for demo */
  background: white;
  font-family: Arial;
  font-size:10pt;
  /* margin-left: 8mm; */
}

.page-header {
  position: fixed;
  top: 0mm;
  width: 100%;
  border-bottom: 1px solid black; /* for demo */
  background: white; /* for demo */
}

.page {
  page-break-after: always;
  /* font-family: Helvetica;
   */
  font-family: Arial;
  font-size:11pt;
  padding:8mm 8mm 0 8mm;
  /* border-bottom: 1px solid #999; */
  min-height: 26cm;
  margin-bottom: 2cm;
  /* border: #999 solid 1px; */
}

.title {
    font-family: Arial;
    font-size:14pt;
    text-decoration:underline;
}

/* .right {
  position: absolute;
  right: 0px;
  width: 300px;
} */

.right {
  position: absolute;
  right: 0px;
  width: 400px;
}

/* th, td {
  padding-top: 8px;
} */

.table {
  padding-top: 8px;
}
.table th {
  padding-top: 8px;
}
.table td{
 vertical-align: top;
 font-family: Arial;
 font-size:10pt;
  margin: 14px;
  padding-top:14px;
}

.tablenew {
  padding-top: 8px;
}
.tablenew th {
  padding-top: 8px;
}

.tablenew td {
    font-family: Verdana;
    font-size:12pt;
    padding-left: 30px;
    margin: 14px;
    padding-top:14px;
}

.table1 {
  font-size: 9pt;;
  padding-top: 1px;
}
.table1 th{
  font-family: Arial;
  border: 1px solid #999;
  padding-top:8px;
}
.table1 td{
  border: 1px solid #999;
  font-family: Arial;
  /* border-collapse: collapse; */
  padding-top:1px;
}


.table-border {
    border: 1px solid #999;
    border-collapse: collapse;
}

.table-foot {
    font-family: Arial;
    font-size:11pt;
    padding-top: 6px;
    /* font-weight: bold; */
}

#watermark {
    position: fixed;
    font-size: 150pt;
    font-weight: bold;
    color: #ccc;
    top: 30%;
    width: 100%;
    text-align: center;
    opacity: .6;
    transform: rotate(-10deg);
    transform-origin: 50% 50%;
    z-index: -1000;
}

@media print {
   thead {display: table-header-group;}
   tfoot {display: table-footer-group;}

   button {display: none;}

   .page{border: #fff solid 0px;}

   body {margin: 0;}
}
 /* End of stiling page */
</style>
</head>
<body>
    @if($order->stat_service_order!="3")
    <div id="watermark">DRAFT</div>
    @endif

    <table border="0">
        <tr>
            <td style="width: 20mm;">
                <img src="{{ $view ? asset('assets/images/logo/logo_kemendag.png') : public_path('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
            </td>
            <td>
                <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN<br />TERTIB NIAGA DIREKTORAT METROLOGI</p>
                <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
            </td>
        </tr>
    </table>

    <table width="100%" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td>
            <!--*** CONTENT GOES HERE ***-->
            <div class="page">
                <center>
                    <div class="title">SERTIFIKAT VERIFIKASI</div>
                    Nomor: {{ $order->no_sertifikat }}</p>
                </center>
                <br/><br/>
                <table border="1" style="float: right;">
                    <tr>
                        <td><center>{{ $order->no_service }}</center></td>
                    </tr>
                    <tr>
                        <td><center>{{ date('d F Y') }}</center></td>
                    </tr>
                </table>
                <div>&nbsp;</div>
                <table class="table" width="70%" cellpading="0" cellspacing="0">
                    <tr>
                        <td width="20%" style="font-size: 18px !important; font-weight:bold;"><strong>Nama Alat</strong></td>
                        <td width="2%" style="font-size: 18px !important; font-weight:bold;">:</td>
                        <td style="font-size: 18px !important; font-weight:bold;">{{ ($order->ServiceRequestItem->standard!=NULL) ? strtoupper($order->ServiceRequestItem->standard->deskripsi_alat) : '' }}</td>
                    </tr>
                </table>
                    
                <table class="tablenew" width="100%" cellpading="0" cellspacing="0">                    
                    <tr>
                        <td>Merek / Buatan</td>
                        <td>:</td>
                        <td>{{ $order->ServiceRequestItem->uuts->tool_brand .' / '.$order->ServiceRequestItem->uuts->tool_made_in }}</td>
                        
                    </tr>
                    <tr>
                        <td>Model / Tipe</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_model . '/' . 
                        $order->ServiceRequestItem->uuts->tool_type }}</td>
                    </tr>
                    <tr>
                        <td>No Seri / Identitas</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequestItem->uuts->serial_no.' / '. (($order->ServiceRequestItem->standard!=NULL) ? $order->ServiceRequestItem->standard->no_identitas : '') }}</td>
                    </tr>
                    <tr>
                        <td>Kapasitas / Skala Utama</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequestItem->uuts->tool_capacity. ' / '. (($order->ServiceRequestItem->standard!=NULL) ? $order->ServiceRequestItem->standard->daya_baca : '') }}</td>
                    </tr>
                    <tr>
                        <td>Kelas / Suhu Dasar</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequest->label_sertifikat }}</td>
                    </tr>
                    <tr>
                        <td>Koefisien Muai Kubik</td>
                        <td>:</td>
                        <td colspan="4">{{ $order->ServiceRequest->addr_sertifikat }}</td>
                    </tr>
                </table>
                <br />
                <table class="table" width="70%" cellpading="0" cellspacing="0">
                    <tr>
                        <td width="20%" style="font-size: 16px !important; font-weight:bold;"><strong>Pemilik</strong></td>
                        <td width="2%" style="font-size: 16px !important; font-weight:bold;">:</td>
                        <td style="font-size: 16px !important; font-weight:bold;">
                            {{ $order->ServiceRequest->label_sertifikat }}<br>
                            {{ $order->ServiceRequest->addr_sertifikat }}
                        </td>
                    </tr>
                </table>
                <br />
                <p>
                    <strong>Sertifikat SUML ini berlaku sampai dengan {{ ($order->sertifikat_expired_at) ? date('d F Y', strtotime($order->sertifikat_expired_at)) :  date('d F Y', strtotime('1 Years')) }}</strong>
                </p>
                <p>
                    Sertifikat ini terdiri dari 3 (tiga) halaman
                </p>
                <br />
                <div class="right">
                    <table cellpading="0" cellspacing="0">
                        <tr>
                            <td></td>
                            <td>Bandung, {{ $order->kabalai_date ? date("d M Y", strtotime($order->kabalai_date)) : date("d M Y") }}</td>
                        </tr>
                        <tr>
                            <td>a.n</td>
                            <td>Direktur Metrologi</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Koordinator Pelayanan Verifikasi<br />Standar Ukuran dan Kalibrasi Alat Ukur</td>
                        </tr>
                        <tr style="height: 50px;">
                            <td></td>
                            <td>
                                @if($order->KaBalai != null)
                                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(123)->margin(0.5)->generate($qrcode_generator)) !!} ">
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>{{ $order->KaBalai != null ? $order->KaBalai->full_name : '' }}</td>
                            <!--
                            <td>Usman, S.Si., M.Si.</td>
                            -->
                        </tr>
                    </table>
                </div>
            </div>
            <div class="page">
                <h4>METODE, STANDAR DAN TELUSURAN</h4>
                <table class="tablenew" width="100%" cellpading="0" cellspacing="0">                    
                    <tr>
                        <td style="vertical-align: top">- Metode</td>
                        <td style="vertical-align: top">:</td>
                        <td style="vertical-align: top">
                            <ul>
                                <li>Peraturan  Menteri Perdagangan Republik Indonesia Nomor 52 Tahun 2019 Tentang Standar
                                    Ukuran Metrologi Legal</li>
                                <li>Keputusan Direktur Jenderal Perlindugan Konsumen dan Tertib Niaga Nomor 124 Tahun 2020
                                    Tentang Syarat Teknis Standar Ukuran Metrologi Legal Besaran Volume</li>
                                <li>EURAMET cg-21 version 2.0 (05/2020) “Guidelines on the calibration of standard in 
                                    Capacity measures using the volumetric methode</li>                                        
                                <li>OIML R-120 (1996) “Standard capacity measures for testing measuring systems for liquids 
                                    other  than water”</li>
                                <li>IK-SNSU-509 Instruksi Kerja Kalibrasi Bejana Ukur Metode Volumetrik</li>                                  
                            </ul>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="vertical-align: top">- Standar</td>
                        <td style="vertical-align: top">:</td>
                        <td style="vertical-align: top">
                            <ul>
                                <li>Bejana Ukur Standar (NS: ATM 00432, 006/16)</li>
                                <li>Thermometer (NS: 38470371)</li>
                                <li>Stopwatch</li>                                  
                            </ul>
                        </td>
                        
                    </tr>
                    <tr>
                        <td colspan="3" style="vertical-align: top">- Tertelusur ke Satuan Pengukuran SI melalui Direktorat Metrologi</td>
                                                
                    </tr>
                </table>
                @if($order->cancel_at == null)
                <h4>DATA VERIFIKASI</h4>
                <table class="table" width="100%" cellpading="0" cellspacing="0">                    
                    <tr>
                        <td>No/Tanggal Order</td>
                        <td>:</td>
                        <td>{{ $order->ServiceRequest->no_order .' / '.$order->ServiceRequestItem->uuts->tool_made_in }}</td>
                    </tr>
                    <tr>
                        <td>Penguji</td>
                        <td>:</td>
                        <td>{{ $order->TestBy1 != null ? $order->TestBy1->full_name : '' }}</td>
                    </tr>
                    <tr>
                        <td>Disaksikan Oleh</td>
                        <td>:</td>
                        <td>{{ '-' }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Pengujian</td>
                        <td>:</td>
                        <td>
                            @if($order->stat_service_order == 4)
                            {{ date("d M Y", strtotime($order->staff_entry_datein)) . ' - ' . date("d M Y", strtotime($order->cancel_at)) }}
                            @else
                                {{ date("d M Y", strtotime($order->staff_entry_datein)) . ' - ' . date("d M Y", strtotime($order->staff_entry_dateout)) }}
                            @endif    
                        </td>
                    </tr>
                    <tr>
                        <td>Lokasi</td>
                        <td>:</td>
                        <td>{{ $order->ServiceRequest->lokasi_pengujian }}</td>
                    </tr>
                    <tr>
                        <td>Kondisi Ruangan</td>
                        <td>:</td>
                        <td>
                            Suhu: <br />
                            Kelembapan: 
                        </td>
                    </tr>
                </table>
                @endif
            </div>
            @if($order->cancel_at == null)
            <div class="page">
                <center>
                    <div class="title">RESUME HASIL VERIFIKASI</div>
                    Nomor: {{ $order->no_sertifikat }}</p>
                </center>

                <table class="table1" width="100%" cellpading="0" cellspacing="0">
                    <tr style="text-align: center; width:2px;">
                        <th rowspan="2">NO</th>
                        <th rowspan="2">PEMERIKSAAN &amp; PENGUJIAN</th>
                        <th colspan="3">PEMENUHAN SYARAT</th>
                        <th rowspan="2">KETERANGAN</th>
                    </tr>
                    <tr style="text-align: center; width:2px;">
                        <th>YA</th>
                        <th>TIDAK</th>
                        <th>N/A</th>
                    </tr>

                    @foreach($order->inspections as $inspection)
                    <tr>
                        <td style="text-align: center; width:2px;">
                            @if($inspection->inspectionItem->is_header)
                            {{ $inspection->inspectionItem->no }}
                            @endif
                        </td>
                        <td>
                            @if($inspection->inspectionItem->is_header)
                            {{ $inspection->inspectionItem->name }}
                            @else
                            {{ $inspection->inspectionItem->no }} {{ $inspection->inspectionItem->name }}
                            @endif
                        </td>
                        <td style="text-align: center;">
                            @if($inspection->is_accepted && $inspection->inspectionItem->is_tested)
                            X
                            @endif
                        </td>
                        <td style="text-align: center;">
                            @if($inspection->is_accepted != null && !$inspection->is_accepted && $inspection->inspectionItem->is_tested)
                            X
                            @endif
                        </td>
                        <td style="text-align: center;">
                            @if($inspection->is_accepted == null && $inspection->inspectionItem->is_tested)
                            X
                            @endif
                        </td>
                        <td></td>
                    </tr>
                    @endforeach
                </table>

                <p style="font-size: 9pt;width:100%">
                    <i>
                    Catatan :<br />
                    -	Hasil yang dinyatakan dalam sertifikat hanya terkait dengan barang yang diverifikasi.<br />
                    -	Ketidakpastian pengukuran dinyatakan pada tingkat kepercayaan sekitar 95% dengan faktor cakupan k=2,00.<br />
                    -	Dalam hal memenuhi persyaratan di SK Dirjen PKTN Nomor 124 Tahun 2020 maka pemilik diwajibkan memperbaiki bejana ukur sehingga memenuhi persyaratan administrasi dan persyaratan kemetrologian saat jadwal verifikasi ulang berikutnya.<br />
                    </i>
                </p>
                <div class="right">
                    <table class="table-foot" cellpading="0" cellspacing="0">
                        <tr>
                            <td></td>
                            <td>Subkoordinator Pelayanan<br />Verifikasi Standar Ukuran</td>
                        </tr>
                        <tr style="height: 50px;">
                            <td></td>
                            <td>
                                @if($order->KaBalai != null)
                                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(123)->margin(0.5)->generate($qrcode_generator)) !!} ">
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>{{ $order->KaBalai != null ? $order->KaBalai->full_name : '' }}</td>
                            <!--
                            <td>Usman, S.Si., M.Si.</td>
                            -->
                        </tr>
                    </table>
                </div>
            </div>
            @endif
            <div class="page-break"></div>    
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
