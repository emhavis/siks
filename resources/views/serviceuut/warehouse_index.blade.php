@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-12">
                {!! Form::open(['url' => route('serviceuut.savetestqr'), 'id' => 'qr'])  !!}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="no_order">Cari Order Berdasar QR Code</label>
                                    {!! Form::text('no_order', null, ['class' => 'form-control','id' => 'no_order', 'autofocus' => 'autofocus']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="error_notes" clsas=""></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/>
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Diterima Oleh</th>
                        <th>Diuji Oleh</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th>Berkas</th>
                        <th></th>
                        <th>Lampiran</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequestItem->no_order }}</td>
                        <td>{{ $row->ServiceRequestItem->uut->tool_brand }}/{{ $row->ServiceRequestItem->uut->tool_model }}/{{ $row->ServiceRequestItem->uut->tool_type }} ({{ $row->ServiceRequestItem->uut->serial_no ? $row->ServiceRequestItem->uut->serial_no : '-' }})</td> 
                        <td>{{ $row->MasterUsers->full_name }}</td>
                        <td>{{ $row->TestBy1 ? $row->TestBy1->full_name : "" }} {{ $row->TestBy2 ? ' & '.$row->TestBy2->full_name : "" }}</td>
                        <td>{{ $row->staff_entry_datein }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>
                            <?php if ($row->ServiceRequest->service_type_id == 4): ?>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/type_approval_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Persetujuan Tipe</a>
                            <?php elseif ($row->ServiceRequest->service_type_id == 5): ?>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Sertifikat Sebelumnya</a>
                            <?php elseif ($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7): ?>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                            <!--<a href="<?= config('app.siks_url') ?>/tracking/download/calibration_manual/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Manual Kalibrasi</a>-->
                            <a href="<?= config('app.siks_url') ?>/tracking/download/manual_book/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Buku Manual</a>
                            <?php endif ?>
                        </td>
                        <td>
                        <td>
                            @if($row->file_skhp!==null)
                            <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuut.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                            <a href="{{ route('serviceuut.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                            @endif
                            <a href="{{ route('serviceuut.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                            @endif
                        </td>
                        <td>
                        @if($row->is_finish=="0")    
                            @if($row->stat_warehouse=="0" || $row->stat_warehouse == null)
                            <a href="{{ route('serviceuut.warehouse', $row->id) }}" class="btn btn-primary btn-sm">GUDANG ALAT SELESAI</a>
                            @endif
                        @elseif($row->is_finish=="1")
                            SELESAI
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('#data_table').DataTable();

    $("#no_order").change(function() {
        var no_order = $(this).val();

        var form = $('#qr');
        form[0].submit();

        /*
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{-- route('orderuut.proses') --}}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
        */
    });
});

</script>
@endsection