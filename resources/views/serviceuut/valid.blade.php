<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta name="author" content="Direktorat Metrologi" />    
	<meta name="description" content="SIMPEL UTTP IV">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Document title -->
    <title>SIMPEL UTTP IV</title>
    <!-- Stylesheets & Fonts -->
    <link href="{{ asset('assets/alt/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alt/style.css') }}" rel="stylesheet">
    <style>
        .footer-sticky {
            position: absolute !important;
            bottom: 0;
            width: 100%;
        }
    </style>
</head>

<body>


    <!-- Body Inner -->
    <div class="body-inner">

    <!-- Header -->
        <header id="header" data-transparent="true" class="header-logo-center">
            <div class="header-inner m-t-40">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <span class="logo-default m-t-40">
                            <img id="imgLogo" src="{{ asset('assets/images/logo/logo_v1.0.png') }}" style="width:100px;"/>
                        </span>
                    </div>
                    <!--End: Logo-->
                    
                </div>
            </div>
        </header>
        <!-- end: Header -->

        @if($order != null)
        <section>
            <div class="container">
                <div class="heading-text heading-section text-center m-b-40" data-animate="fadeInUp">
                    <span class="lead">SKHP tervalidasi untuk rincian sebagai berikut:</span>
                </div>

                <div class="row">
                    <div class="content col-lg-12">
                        
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td width="20%">Nomor SKHP</td>
                                        <td>{{ $order->no_sertifikat }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Tanggal Terbit</td>
                                        <td>{{ date("d M Y", strtotime($order->kabalai_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Disahkan Oleh</td>
                                        <td>{{ $order->KaBalai->full_name }}, Kepala Balai Pengujian SNSU, a.n. Direktur Metrologi</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Jenis UTTP</td>
                                        <td>{{ $order->ServiceRequestItem->uuts->stdtype->uut_type }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Merek</td>
                                        <td>{{ $order->ServiceRequestItem->uuts->tool_brand }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Model/Tipe</td>
                                        <td>{{ $order->ServiceRequestItem->uuts->tool_model }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">No Seri</td>
                                        <td>{{ $order->ServiceRequestItem->uuts->serial_no }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Nama Pemilik</td>
                                        <td>{{ $order->ServiceRequest->label_sertifikat }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        @else 
        <section>
            <div class="container">
                <div class="heading-text heading-section text-center m-b-40" data-animate="fadeInUp">
                    <span class="lead">SKHP tidak valid</span>
                </div>
            </div>
        </section>
        @endif
        <!-- END WHAT WE DO -->


        <!-- Footer -->
        <footer id="footer" class="<?= $order == null ? 'footer-sticky' : '' ?>">
            <div class="copyright-content">
                <div class="container">
                    <div class="copyright-text text-center">&copy; 2021 SIMPEL UTTP IV </div>
                </div>
            </div>
        </footer>
        <!-- end: Footer -->

    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>
    <!--Plugins-->
    <script src="{{ asset('assets/alt/jquery.js') }}"></script>
    <script src="{{ asset('assets/alt/plugins.js') }}"></script>

    <!--Template functions-->
    <script src="{{ asset('assets/alt/functions.js') }}"></script>




</body>

</html>