@extends('layouts.app')
<style type="text/css">
    .error{
        color:yellow
    }
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        {!! Form::open(['url' => route('serviceuut.confirmcancel', $serviceOrder->id),'files'=>true,'name'=>'cancel_form'])  !!}
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis">Jenis</label>
                    {!! Form::text('jenis', $serviceOrder->ServiceRequestItem->uuts->stdtype->uut_type, ['class' => 'form-control','id' => 'jenis', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group input-group" role="group">
                    <span class="input-group-text">Kapasitas dan Satuan </span>
                    {!! Form::text('kapasitas', $serviceOrder->tool_capacity, ['class' => 'form-control','aria-label' => 'kapasitas','id' => 'kapasitas', 'placeholder' => 'Kapasitas']) !!}
                    {!! Form::text('capacity_unit', $serviceOrder->tool_capacity_unit, ['class' => 'form-control','aria-label' => 'capacity_unit','id' => 'capacity_unit','placeholder' => 'Unit']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Merek/Model/Tipe/Nomor Seri</label>
                    {!! Form::text('merek', 
                        $serviceOrder->tool_brand . '/' . 
                        $serviceOrder->tool_model . '/' .
                        $serviceOrder->tool_type . '/' .
                        $serviceOrder->serial_no,
                        ['class' => 'form-control','id' => 'merek', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Dasar Pengujian, Nomor</label>
                    {!! Form::text('reference_no', 
                        $serviceOrder->ServiceRequestItem->reference_no,
                        ['class' => 'form-control','id' => 'reference_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_date">Dasar Pengujian, Tanggal</label>
                    {!! Form::text('reference_date', 
                        $serviceOrder->ServiceRequestItem->reference_date,
                        ['class' => 'form-control','id' => 'reference_date', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pending_notes">Alasan Pembatalan Pemeriksaan/Pengujian</label>
                    {!! Form::textArea('cancel_notes', 
                        $serviceOrder->cancel_notes,
                        ['class' => 'form-control','id' => 'cancel_notes','rows'=>'5']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="cancelation_file" style="padding-bottom:20px">Lampiran Pembatalan</label>
                    {!! Form::file('cancelation_file', null,
                        ['class' => ['form-control','form-control-lg'],'id' => 'cancelation_file']) !!}
                </div>
                <a class="btn btn-danger" href="{{ route('file.show', [$serviceOrder->id,'batal']) }}">Lihat</a>
                <!-- <label><a href="{{route('serviceuut.cancelation_download',$serviceOrder->id)}}">{{$serviceOrder->cancelation_file}}</a></label> -->
            </div>
        </div>
        <button role="submit" class="btn btn-w-md btn-danger" id="btn_simpan">Batal Uji</button>
        {!! Form::close() !!}
        
    </div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function ()
    {
        $('#cancelation_file').attr('size',60);
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
    });
$(function (){
    $("form[name='cancel_form']").validate({
    rules:{
        cancel_notes:'required'
    },
    messages:{
        cancel_notes:"Masukkan alasan pembatalan.!"
    }
});

});

</script>
@endsection