@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
            
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Booking</h4>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="label_sertifikat">Nama Pemilik</label>
                                    {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="addr_sertifikat">Alamat Pemilik</label>
                                    {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                                </div>
                            </div>  
                        </div>

                    </div>
                </div>

                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Pemohon</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_name">Nama Pemohon</label>
                                    {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_name">Jenis Tanda Pengenal</label>
                                    {!! Form::text('id_type_id', $requestor->kantor == "Perusahaan" ? "NIB" : "NPWP", ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                                    {!! Form::text('pic_id_no', $requestor->kantor == "Perusahaan" ? $requestor->nib : $requestor->npwp, ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_phone_no">Nomor Telepon</label>
                                    {!! Form::text('pic_phone_no', $requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_email">Alamat Email</label>
                                    {!! Form::text('pic_email', $requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_from">Usulan Jadwal Mulai</label>
                                    {!! Form::text('scheduled_test_date_from', date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_from', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_to">Usulan Jadwal Selesai</label>
                                    {!! Form::text('scheduled_test_date_to', date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_to', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis_layanan">Jenis Layanan</label>
                                    {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lokasi_pengujian">Lokasi Pengujian</label>
                                    {!! Form::text('lokasi_pengujian', $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' , ['class' => 'form-control','id' => 'lokasi_pengujian', 'readonly']) !!}
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Penugasan dan Penjadwalan</h4>
                    </div>
                    <div class="panel-body" id="panel_staff">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inspection_prov_id">Alamat Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                                    {!! Form::text('inspection_prov_id', 
                                        $request->inspectionKabkot->nama .', '. $request->inspectionProv->nama,
                                        ['class' => 'form-control','id' => 'inspection_prov_id', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="spuh_rate">Uang Harian</label>
                                    {!! Form::hidden('spuh_rate_id', $request->spuh_rate_id,
                                        ['class' => 'form-control','id' => 'spuh_rate_id', 'readonly']) !!}
                                    {!! Form::text('spuh_rate', $request->spuh_rate,
                                        ['class' => 'form-control','id' => 'spuh_rate', 'readonly']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_from">Jadwal Waktu Pengujian, Mulai</label>
                                    {!! Form::text('scheduled_test_date_from', 
                                        date("d-m-Y", strtotime(isset($doc->date_from) ? $doc->date_from : date("Y-m-d"))),
                                        ['class' => 'form-control','id' => 'scheduled_test_date_from', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_to">Jadwal Waktu Pengujian, Selesai</label>
                                    {!! Form::text('scheduled_test_date_to', 
                                        date("d-m-Y", strtotime(isset($doc->date_to) ? $doc->date_to : date("Y-m-d"))),
                                        ['class' => 'form-control','id' => 'scheduled_test_date_to', 'readonly']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="table_data_penjadwalan">Penguji/Pemeriksa</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table_data_penjadwalan" class="table table-striped table-hover table-responsive-sm">
                                    <thead>
                                        <tr>
                                            <th>NIK</th>
                                            <th>Nama</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($staffs as $row)
                                        @if( $row->scheduledStaff != null )
                                        <tr>
                                            <td>{{ $row->scheduledStaff->nip }}</td>
                                            <td>{{ $row->scheduledStaff->nama }}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Alat</h4>
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped table-hover table-responsive-sm" id="t-roll">
                            <thead style="font-weight:bold; color:white;">
                                <tr>
                                    <th>No</th>
                                    <th>Jenis Alat Ukur</th>
                                    <th>Merek</th>
                                    <th>Model/Tipe</th>
                                    <th>No. Seri</th>
                                    <th>Media Uji</th>
                                    <th>Kapasitas Maks.</th>
                                    <th>Kapasitas Min.</th>
                                    <th>Satuan</th>
                                    <th>Buatan</th>
                                    <th>Pabrikan</th>
                                    <th>Alamat Pabrik</th>
                                    @if ($request->lokasi_pengujian == 'luar')
                                    <th style="width: 200px;">Alamat Lokasi Pengujian</th>
                                    @else 
                                    
                                    @endif
                                    <th>Kelengkapan Persyaratan (Break Down) </th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php $num=0 ?>
                                @foreach($request->items as $item)
                                    <?php $num +=1 ?>
                                    <tr>
                                        <form id="form_item_{{ $item->id }}">
                                            {!! Form::hidden('id', $item->id) !!}
                                            {!! Form::hidden('uut_id', $item->uut_id) !!}
                                            {!! Form::hidden('standard_type_id', $item->uuts->type_id) !!}
                                            <td>{{ $num}}</td>
                                            <td>{{ $item->uuts->stdtype->uut_type}}</td>
                                            <td>{{ $item->uuts->tool_brand}}</td>
                                            <td>{{ $item->uuts->tool_model}}</td>
                                            <td>{{ $item->uuts->serial_no}}</td>
                                            <td>{{ $item->uuts->tool_media}}</td>
                                            <td>{{ $item->uuts->tool_capacity}}
                                            <td>{{ $item->uuts->tool_capacity_min}}</td>
                                            <td>{{ $item->uuts->tool_capacity_unit }}</td>
                                            <td>{{ $item->uuts->tool_made_in }}</td>
                                            <td>{{ $item->uuts->tool_factory }}</td>
                                            <td>{{ $item->uuts->tool_factory_address }}</td>
                                            @if ($request->lokasi_pengujian == 'luar')
                                            <td>
                                                {{ $item->location }}, {{ ($item->kabkot ? $item->kabkot->nama : '') . ', ' . ($item->provinsi ? $item->provinsi->nama : '') }}
                                                <br/>
                                                ({{ $item->location_lat }}, {{ $item->location_long }})
                                            </td>
                                            @else 
                                            
                                            @endif
                                            <td> 
                                                
                                                    @if($request->path_surat_permohonan != null)
                                                        <a href="<?= config('app.siks_url') ?>/tracking/download_permohonan_uut/{{ $request->id }}"
                                                            class="btn btn-default btn-sm faa-parent animated-hover" >
                                                            <i class="fa fa-download faa-flash"></i> Surat Permohonan
                                                        </a>
                                                    @endif
                                                    @if($item->path_last_certificate != null)
                                                        <a href="<?= config('app.siks_url') ?>/tracking/download_uut/last_certificate/{{ $item->id }}"
                                                            class="btn btn-default btn-sm faa-parent animated-hover" >
                                                            <i class="fa fa-download faa-flash"></i> Sertifikat Sebelumnya
                                                        </a>
                                                    @endif
                                                
                                            </td>
                                            
                                        </form>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            
                </div>

                {!! Form::open(['url' => route('firstcheckluaruut.savecheck', $request->id), 'files' => true, 'id' => 'form_result'])  !!}
        
                <input type="hidden" name="id" value="{{ $request->id }}" />
                
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Pemeriksaan Awal</h4>
                    </div>
                    <div class="panel-body" id="panel_check">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="is_accepted">Kesiapan Petugas?</label>
                                    {!! Form::select('is_siap_petugas', ['ya'=>'Siap','tidak'=>'Tidak Siap'], 
                                                isset($doc->is_siap_petugas) ? ($doc->is_siap_petugas ? 'ya' : 'tidak') : null, 
                                                ['class' => 'form-control is_siap_petugas','id' => 'is_siap_petugas', 'style' => 'width:100%']) !!}
                                            </td>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="keterangan_tidak_siap">Keterangan</label>
                                    {!! Form::textarea('keterangan_tidak_siap', null,
                                        ['class' => 'form-control','id' => 'keterangan_tidak_siap', 'readonly']) !!}       
                                </div>
                            </div>
                        </div>

                        <div class="row" id="kelengkapan">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="is_accepted">Kelengkapan Data?</label>
                                    {!! Form::select('is_accepted', ['ya'=>'Lengkap','tidak'=>'Tidak Lengkap'], 
                                                isset($doc->is_accepted) ? ($doc->is_accepted ? 'ya' : 'tidak') : null, 
                                                ['class' => 'form-control is_accepted','id' => 'is_accepted', 'style' => 'width:100%']) !!}
                                            </td>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="notes">Catatan</label>
                                    {!! Form::textarea('keterangan_tidak_lengkap', $doc->keterangan_tidak_lengkap,
                                        ['class' => 'form-control','id' => 'keterangan_tidak_lengkap', 'readonly']) !!}       
                                </div>
                            </div>
                        </div>

                        <div class="row" id="bukti">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="file_bukti_pemeriksaan_awal"> Bukti Pemeriksaan Awal (<a href="https://kemend.ag/PemeriksaanAwalDL" target="_blank">Formulir download disini</a>) *</label>
                                    {!! Form::file('file_bukti_pemeriksaan_awal', 
                                        ['class' => 'form-control','id' => 'file_bukti_pemeriksaan_awal', 'accept'=> 'application/pdf,image/png, image/jpeg']) !!}
                                </div>
                            </div>
                            <div class="cal-md-6">
                                @if($doc->file_bukti_pemeriksaan_awal !=null)
                                <div class="input-group">
                                    <label style="padding-top:30px"></label>
                                    <a href="{{ route('firstcheckluaruut.downloadbukti', $doc->id) }}" class="btn btn-warning btn-sm">DOWNLOAD BUKTI</a>
                                    <label>&nbsp; Nama File:  {{$doc->file_bukti_pemeriksaan_awal}}</label>
                                </div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
                <input type="hidden" name="submit_val" id="submit_val" value="0"/>
                
                </form>

                
                <button type="button" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button>  
                <button type="button" class="btn btn-w-md btn-accent" id="btn_simpan_submit">Simpan dan Konfirmasi</button>  

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Pakta Integritas</h4>
                <h4 class="m-t-none">Layanan In-Situ Balai Pengelolaan SUML</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                
                <hr/>

                <div class="row">
                    <div class="col-12">
                        <p>Saya: <br/>
                        @foreach($staffs as $row)
                            @if( $row->scheduledStaff != null )
                            {{ $row->scheduledStaff->nama }} ({{ $row->scheduledStaff->nip }})
                            @endif
                            @if(!$loop->last)
                            <br/>
                            @endif
                        @endforeach
                        <br/>dalam menjalankan tugas ini, menyatakan sebagai berikut:</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                    <ol>
                        <li>Berperan secara pro aktif dalam upaya pencegahan  dan pemberantasan Korupsi, Kolusi, Nepotisme serta tidak melibatkan diri dalam perbuatan tercela;</li>
                        <li>Tidak meminta atau menerima pemberian secara langsung atau tidak langsung berupa suap, hadiah, bantuan, atau bentuk lainnya yang tidak sesuai dengan ketentuan yang berlaku;</li>
                        <li>Bersikap transparan, jujur, obyektif, dan akuntabel dalam melaksanakan tugas;</li>
                        <li>Menghindari pertentangan kepentingan (Conflict of Interest) dalam pelaksanaan tugas;</li>
                        <li>Memberi contoh dalam kepatuhan terhadap peraturan perundang-undangan dalam melaksanakan tugas, terutama kepada sesama pegawai di lingkungan kerja saya secara konsisten;</li>
                        <li>Akan menyampaikan informasi penyimpangan integritas di Direktorat Metrologi, Direktorat Jenderal  Perlindungan Konsumen dan Tertib Niaga, Kementerian Perdagangan serta turut menjaga kerahasiaan saksi atas pelanggaran peraturan yang dilaporkannya;</li>
                        <li>Bila saya melanggar hal-hal tersebut di atas, saya siap menghadapi konsekuensinya.</li>
                    </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table>
                            <tr>
                                <td><input type="checkbox" name="is_integritas" value="is_integritas" id="is_integritas" /></td>
                                <td><label class="checkbox-inline" for="is_integritas">Dengan ini saya menyatakan pakta integritas ini dibuat dengan sebenar-benarnya dan akan saya pertanggungjawabkan sebagaimana mestinya.</label></td>
                            </tr>
                        </table>
                        <div class="form-group">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="approve" class="btn btn-success" disabled>Setuju dan Kirim</button>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/jquery-validation/additional-methods.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan, #data_table_check").DataTable();
        $('#t-roll').DataTable({
            "scrollX": true
        });

        $('#kelengkapan, #bukti').show();

        $('.is_siap_petugas').select2({
            //placeholder: "- Pilih UML -",
            allowClear: true
        }).on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data.id);
            
            if (data.id == 'tidak') {
                $("#keterangan_tidak_siap").prop("readonly", false); 
                $('#kelengkapan, #bukti').hide();
            } else {
                $("#keterangan_tidak_siap").val("");
                $("#keterangan_tidak_siap").prop("readonly", true); 
                $('#kelengkapan, #bukti').show();
            }
        });

        $('.is_accepted').select2({
            //placeholder: "- Pilih UML -",
            allowClear: true
        }).on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data.id);
            
            if (data.id == 'tidak') {
                $("#keterangan_tidak_lengkap").prop("readonly", false); 
            } else {
                $("#keterangan_tidak_lengkap").val("");
                $("#keterangan_tidak_lengkap").prop("readonly", true); 
            }
        });
        
        $("#btn_simpan").click(function(e){
            e.preventDefault();

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            var file = $('#file_bukti_pemeriksaan_awal')[0].files.length;
            var lengkap = $('#is_accepted').val();
            var siap_petugas = $('#is_siap_petugas').val();

            if (siap_petugas == 'ya' && lengkap == 'ya') {
                $("#confirmModal").modal().on('shown.bs.modal', function ()
                {
                    $('#is_integritas').change(function(){
                        if(this.checked) {
                            $('#approve').prop('disabled', false);
                        } else {
                            $('#approve').prop('disabled', true);
                        }
                    });
                    $('#approve').click(function(){
                        $("#submit_val").val(1);

                        var form = $("#form_result");
                        if (form.valid()) {
                            toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                            form[0].submit();
                        }
                    });
                });
            } else {
                $("#submit_val").val(1);

                var form = $("#form_result");
                if (form.valid()) {
                    toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                    form[0].submit();
                }
            }
        });
    });
</script>
@endsection