@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <div class="tab-content" id="nav-tabContent">
        <div>
            <br/>
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
 
                    <table id="data_table" class="table table-striped table-hover table-responsive-sm data-table">
                        <thead>
                            <tr>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Status Alat</th>
                                <th>Berkas</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows as $row)
                            <tr>
                                <td>{{ $row->ServiceRequestItem->no_order }}</td>
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : "" }}</td>
                                <td>{{ $row->test1 ? $row->test1->full_name : "" }} {{ $row->test2 ? ' & '.$row->test2->full_name : "" }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->staff_entry_datein)) }}</td>
                                <td>{{ $row->staff_entry_dateout != null ? date("d-m-Y", strtotime($row->staff_entry_dateout)) : '' }}</td>
                                <td>
                                    @if($row->ServiceRequestItem->status_id ==16)
                                        {{$row->ServiceRequestItem->status->status}}
                                    @elseif($row->ServiceRequestItem->status_id ==16)
                                        {{$row->ServiceRequestItem->status->status}}
                                    @else
                                        @if($row->stat_warehouse == 2) Sudah Diserahterimakan dengan Pemohon
                                        @elseif($row->stat_sertifikat == 3 && $row->stat_service_order ==3) <span>Di setujui KABALAI</span>
                                        @elseif ($row->stat_sertifikat == 1 && $row->stat_service_order == 2 ) Menunggu Persetujuan KABALAI
                                        @elseif ($row->stat_sertifikat == 2 && $row->stat_warehouse == 1 ) Dikirim ke KABALAI
                                        @elseif (($row->stat_sertifikat == 1 || $row->stat_sertifikat == 1) && $row->stat_warehouse == 0 && ($row->subkoordinator_notes =='' || $row->subkoordinator_notes ==null))  Dikirim ke SUBKO
                                        @elseif($row->stat_sertifikat == 1 && $row->stat_service_order ==2 && ($row->subkoordinator_notes !='' || $row->subkoordinator_notes !=null)) <span>Dikembalikan oleh SUBKO</span>
                                        @elseif ($row->stat_warehouse == 1)  Berada di Gudang

                                        @endif
                                            <!-- ($row->stat_sertifikat == 2 && $row->stat_warehouse == 1) ? 'Dikirim ke KABALAI':
                                            (($row->stat_sertifikat == 1 || $row->stat_sertifikat == 0) && $row->stat_warehouse == 1) ? 'Dikirim ke SUBKO':
                                            ($row->stat_warehouse == 1) ? 'Berada di Gudang' :'' -->
                                    
                                    @endif
                                </td>
                                <td>
                                    @if ($row->ServiceRequest->service_type_id == 5)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Sertifikat Sebelumnya</a>
                                    @elseif ($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                                        @if ($row->ServiceRequestItem->path_calibration_manual != null)
                                        <a href="<?= config('app.siks_url') ?>/tracking/download/calibration_manual/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Manual Kalibrasi</a>
                                        @endif
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/manual_book/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Buku Manual</a>
                                    @elseif($row->ServiceRequest->service_type_id == 1 || $row->ServiceRequest->service_type_id ==2)
                                        @if($row->ServiceRequestItem->file_manual_book !='')
                                            <a href="<?= config('app.siks_url') ?>/tracking/download_uut/manual_book/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Buku Manual</a>
                                        @endif 
                                        @if($row->ServiceRequest->booking->file_certificate !=null)
                                        <a href="<?= config('app.siks_url') ?>/booking/download_permohonan/certificate/{{ $row->ServiceRequest->booking->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($row->ServiceRequestItem->status_id ==16)
                                        @if (!$row->is_skhpt)
                                            <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">SURAT PEMBATALAN</a>
                                        @endif
                                    @else
                                        @if($row->file_skhp!==null)
                                            @if (!$row->is_skhpt)
                                                <a href="{{ route('serviceuut.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT</a>
                                                @if($row->kabalai_id)
                                                    <a href="{{ route('serviceuut.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIIFIKAT</a>
                                                @endif
                                            @endif
                                            <a href="{{ route('file.show', [$row->id,'cerapan']) }}" class="btn btn-warning btn-sm">LIHAT CERAPAN</a>
                                            @if($row->file_lampiran!==null)
                                            <a href="{{ route('file.show', [$row->id,'lampiran']) }}" class="btn btn-warning btn-sm">LIHAT LAMPIRAN</a>
                                            @endif
                                            @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                            <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                            <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                            @endif
                                            @if($row->ujitipe_completed==true || $row->has_set)
                                            <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                            <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                            @endif
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Kirim Alat ke Gudang</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <input type="hidden" name="id" id="id"/>
                            <input type="hidden" name="requestid" id="requestid"/>
                            <input type="hidden" name="labid" id="labid"/>
                            <input type="hidden" name="instalasiid" id="instalasiid"/>

                            <div class="form-group">
                                <label>Alat</label>
                                <input type="text" name="alat" id="alat" class="form-control" readonly required />
                            </div>

                            <div class="form-group">
                                <label>Laboratorium</label>
                                <input type="text" name="lab_nama" id="lab_nama" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Instalasi</label>
                                <input type="text" name="instalasi_nama" id="instalasi_nama" class="form-control" readonly required />
                                <!--
                                <select name="instalasi_id" id="instalasi_id" class="form-control select2" required></select>
                                -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">KIRIM</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('.data-table').DataTable({
        scrollX: true,
    });

    $("button.btn-mdl").click(function(e){
        e.preventDefault();
        var id = $(this).data().id;
        var requestid = $(this).data().requestid;
        var labid = $(this).data().labid;
        var instalasiid = $(this).data().instalasiid;
        var alat = $(this).data().alat;

        $("#id").val(id);
        $("#requestid").val(requestid);
        $("#labid").val(labid);
        $("#instalasiid").val(instalasiid);
        $("#alat").val(alat);

        var route = "{{ route('laboratory.getbyid', ':id') }}";
        route = route.replace(':id', labid);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#lab_nama").val(res.lab.nama_lab);
                $('#instalasi_id').empty();

                $.each(res.instalasi, function(i, iteminstalasi) {
                    if (iteminstalasi.id == instalasiid) {
                        $('#instalasi_nama').val(iteminstalasi.nama_instalasi);
                    }
                });
            });
        },"json");
    });

    $("#simpan").click(function(e) {
        e.preventDefault();

        var id = $('#id').val();
        var requestid = $('#requestid').val();
        var instalasiid = $('#instalasiid').val();
        
        //var route = "{{ route('receiveuttp.proses', ':id') }}";
        //route = route.replace(':id', id);
        //window.location = route;

        var form = $('#form_modal');
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{ route('servicehistoryuttp.warehouse') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
        
        $("#prosesmodal").modal('hide');
    });
});

</script>
@endsection