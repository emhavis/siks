@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled col-12" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading" >
            <h4>Pendelegasian Tugas</h4>
        </div>
        @if (\Session::has('response'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('response') !!}</li>
                </ul>
            </div>
        @endif
        <div class="panel-body">
            <form id="createGroup" action="{{ route('delegationuttp.store')}}" method="POST" >
            
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="user_delegator_id", value="{{ $delegator_user->id }}"/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="user_delegatee_id">Didelegasikan Kepada</label> 
                            {!! Form::select('user_delegatee_id', $delegatee_users, null, 
                                    ['class' => 'form-control', 'id' => 'user_delegatee_id']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="berlaku_mulai">Berlaku Mulai</label>
                            {!! Form::text('berlaku_mulai', 
                                date("d-m-Y"),
                                ['class' => 'form-control','id' => 'berlaku_mulai', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="berlaku_sampai">Berlaku Sampai</label>
                            {!! Form::text('berlaku_sampai', 
                                date("d-m-Y"),
                                ['class' => 'form-control','id' => 'berlaku_sampai', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                
                
                <button type="submit" id="submit" class="btn btn-default">Simpan</button>
            </form> 
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script>
$(document).ready(function ()
{
    $('#user_delegatee_id').select2({
        placeholder: "- Pilih Pegawai -",
        allowClear: true
    });

    var date = new Date();
    $('#berlaku_mulai, #berlaku_sampai').datepicker({
        format:"dd-mm-yyyy",
        autoclose: true,
        startDate: '-d',
    });
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection