@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-body">
            <form id="createGroup" action="{{ route('delegationuttp')}}" method="GET" >
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Filter:</label>
                        {!!
                            Form::select("filter",
                            ['active' => 'Berlaku', 'history' => 'Riwayat', 'all' => 'Semua'],
                            $filter?$filter:'active',[
                            'class' => 'form-control',
                            'id' => 'filter',
                            ]);
                        !!}
                    </div>
                </div>
                <div class="col-md-2">
                    {!! Form::submit('Filter', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_filter']) !!}
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-2 text-right">
                    <a href="{{ route('delegationuttp.create') }}" class="btn btn-w-md btn-primary">Delegasi Baru</a>
                </div>
            </div>
            </form>
            <br/>
            <table id="table_data" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Didelegasikan Kepada</th>
                    <th>Berlaku Mulai</th>
                    <th>Berlaku Sampai</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($delegations as $delegation)
                <tr>
                    <td>{{ $delegation->delegatee->full_name }}</td>
                    <td>{{ date("d-m-Y", strtotime($delegation->berlaku_mulai)) }}</td>
                    <td>{{ date("d-m-Y", strtotime($delegation->berlaku_sampai)) }}</td>
                    <td>
                        {{ $delegation->status == 1 ? 'Aktif' : 'Tidak Berlaku' }}
                    </td>
                    <td>
                        <a href="{{ route('delegationuttp.edit', $delegation->id) }}" class="btn btn-sm btn-warning">Edit</a>
                        <a href="#" data-id="{{$delegation->id}}" 
                                class="btn btn-danger btn-sm delete btn-hapus" 
                                data-toggle="modal" 
                                data-target="#deleteModal">Hapus
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Delete Warning Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('delegationuttp.destroy', 'id') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="modal-body">
                    
                    <h5 class="text-center">Apakah data benar akan dihapus?</h5></br>
                    <input type="hidden" id="id" name="id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-danger btn-sm">Yes, Dihapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Delete Modal --> 
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function ()
    {
        $('.btn-hapus').click(function(){
            let id = $(this).attr('data-id');
            $('#id').val($(this).attr('data-id'));
            $('#confirmModal').modal('show');
        });

        $('#filter').select2();
        $("#btn1").on('click',function(e) {
            // console.log('console');
            var _token = $('#_token').val();
            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $('#createGroup');
            console.log(form.serialize());
            // form.append("_token", _token);
            /*
            $.ajax({
                type: "GET",
                url: "{{route('file-export')}}",
                data: form.serialize(), // serializes the form's elements.
                success: function(data)
                {
                    console.log(data); // show response from the php script.
                }
            });
            */
           var route = "{{route('file-export')}}?" + form.serialize();
           location.assign(route);

        });

        // data table will generated
        var table = $('#table_data').DataTable();
    });
</script>
@endsection