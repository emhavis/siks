@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">

    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            
            <table id="table_data_penjadwalan" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Pendaftaran</th>
                        <th>Nama Pemilik</th>
                        <th>Nama Pemohon</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->no_register }}</td>
                        <td>{{ $row->label_sertifikat }}</td>
                        <td>{{ $row->requestor->full_name }}</td>
                        <td>{{ $row->status->status }}</td>
                        <td>
                            <a href="{{ route('docinsituuut.approval', $row->id) }}" class="btn btn-warning btn-sm">Persetujuan</a>
                            @if($row->spuh_doc_id != null)
                            <a href="{{ route('schedulinguut.surattugas', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Draft Surat Tugas</a>
                            @endif
                            @if($row->is_integritas == true)
                            <a target="_blank" href="{{ route('docinsituuut.integritas', $row->id) }}" class="btn btn-warning btn-sm">Pakta Integritas</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>

        </div>
    </div>

</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan").DataTable();
        
    });
</script>
@endsection