<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Pakta Integritas Layanan In-Situ Balai Pengelolaan SUML</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            text-align: justify;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
    </style>
</head>

<body class="A4">


    <section class="sheet letterhead"> 

        <div class="text-right" style="padding-top: 27mm; padding-right: 25.4mm;">&nbsp;</div>

        <div class="text-center">
            <div class="title"><strong>PAKTA INTEGRITAS</strong></div>
            <div class="subtitle">Layanan In-Situ Balai Pengelolaan SUML</div>
        </div>

        <br/>
     
        <p style="padding-left: 25.4mm; padding-right: 25.4mm;">Saya: </p>
        <p style="padding-left: 30.4mm; padding-right: 25.4mm;">
        @foreach($staffs as $row)
            @if( $row->scheduledStaff != null )
            {{ $row->scheduledStaff->nama }} ({{ $row->scheduledStaff->nip }})
            @endif
            @if(!$loop->last)
            <br/>
            @endif
        @endforeach
        </p>
        <p style="padding-left: 25.4mm; padding-right: 25.4mm;">dalam menjalankan tugas ini, menyatakan sebagai berikut:</p>
        
        <ol style="padding-left: 30.4mm; padding-right: 25.4mm;">
            <li>Berperan secara pro aktif dalam upaya pencegahan  dan pemberantasan Korupsi, Kolusi, Nepotisme serta tidak melibatkan diri dalam perbuatan tercela;</li>
            <li>Tidak meminta atau menerima pemberian secara langsung atau tidak langsung berupa suap, hadiah, bantuan, atau bentuk lainnya yang tidak sesuai dengan ketentuan yang berlaku;</li>
            <li>Bersikap transparan, jujur, obyektif, dan akuntabel dalam melaksanakan tugas;</li>
            <li>Menghindari pertentangan kepentingan (Conflict of Interest) dalam pelaksanaan tugas;</li>
            <li>Memberi contoh dalam kepatuhan terhadap peraturan perundang-undangan dalam melaksanakan tugas, terutama kepada sesama pegawai di lingkungan kerja saya secara konsisten;</li>
            <li>Akan menyampaikan informasi penyimpangan integritas di Direktorat Metrologi, Direktorat Jenderal  Perlindungan Konsumen dan Tertib Niaga, Kementerian Perdagangan serta turut menjaga kerahasiaan saksi atas pelanggaran peraturan yang dilaporkannya;</li>
            <li>Bila saya melanggar hal-hal tersebut di atas, saya siap menghadapi konsekuensinya.</li>
        </ol>

        <p style="padding-left: 25.4mm; padding-right: 25.4mm;">
            Pakta Integritas ini dibuat dan disetujui melalui aplikasi SIMPEL pada tanggal {{  format_long_date($request->integritas_at) }}.
        </p>
    </section>

    
</body>

</html>