@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#processing" aria-controls="processing" role="tab" data-toggle="tab">Order</a></li>
        <li role="presentation"><a href="#done" aria-controls="done" role="tab" data-toggle="tab">Proses</a></li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="processing">
            
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    

                    <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Sertifikat</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_processing as $row)
                            <tr>
                                <td>{{ $row->order ? $row->order->no_sertifikat : '' }}</td>
                                <td>{{ $row->request ? $row->request->label_sertifikat : '' }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->masterstatus->status }}</td>
                                <td>
                                    <a href="{{ route('revisionorderuttp.order', $row->id) }}" class="btn btn-warning btn-sm">Terima Order</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="done">
            <div class="panel panel-filled table-area">
                <br/>
                <div class="panel-heading">
                <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Sertifikat</th>
                                <th>No Sertifikat Perbaikan</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_processed as $row)
                            <tr>
                                <td>{{ $row->order ? $row->order->no_sertifikat : '' }}</td>    
                                <td>{{ $row->no_sertifikat }}</td>
                                <td>{{ $row->request ? $row->request->label_sertifikat : '' }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->masterstatus->status }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
    
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data,#table_data_process,#table_data_done").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $('.btn-simpan').click(function(e){
            e.preventDefault();
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });
    });
</script>
@endsection