@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">

        {!! Form::open(['id' => 'form_create_request', 'route' => ['revisionorderuttp.simpanorder', $revision->id]]) !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Perbaikan Sertifikat</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_sertifikat">No Sertifikat</label>
                            {!! Form::text('no_sertifikat', $revision->order->no_sertifikat, ['class' => 'form-control','id' => 'no_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', $revision->request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                
                @if($revision->uttp_type_id != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="uttp_type_id_old">Jenis Alat (Tertulis)</label>
                            {!! Form::text('uttp_type_id_old', $revision->order->ServiceRequestItem->uttp->type->uttp_type, ['class' => 'form-control','id' => 'uttp_type_id_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="uttp_type_id_new">Jenis Alat (Pembetulan)</label>
                            {!! Form::text('uttp_type_id_new', $revision->uttpType->uttp_type, ['class' => 'form-control','id' => 'uttp_type_id_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif

                @if($revision->tool_brand != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_brand_old">Merek (Tertulis)</label>
                            {!! Form::text('tool_brand_old', $revision->order->ServiceRequestItem->uttp->tool_brand, ['class' => 'form-control','id' => 'tool_brand_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_brand_new">Merek (Pembetulan)</label>
                            {!! Form::text('tool_brand_new', $revision->tool_brand, ['class' => 'form-control','id' => 'tool_brand_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_model != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_model_old">Model (Tertulis)</label>
                            {!! Form::text('tool_model_old', $revision->order->ServiceRequestItem->uttp->tool_model, ['class' => 'form-control','id' => 'tool_model_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_model_new">Model (Pembetulan)</label>
                            {!! Form::text('tool_model_new', $revision->tool_model, ['class' => 'form-control','id' => 'tool_model_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_type != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_type_old">Tipe (Tertulis)</label>
                            {!! Form::text('tool_type_old', $revision->order->ServiceRequestItem->uttp->tool_type, ['class' => 'form-control','id' => 'tool_type_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_type_new">Tipe (Pembetulan)</label>
                            {!! Form::text('tool_type_new', $revision->tool_type, ['class' => 'form-control','id' => 'tool_type_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_capacity != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_capacity_old">Kapasitas (Tertulis)</label>
                            {!! Form::text('tool_capacity_old', $revision->order->ServiceRequestItem->uttp->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_capacity_new">Kapasitas (Pembetulan)</label>
                            {!! Form::text('tool_capacity_new', $revision->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_made_in != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_made_in_old">Buatan (Tertulis)</label>
                            {!! Form::text('tool_made_in_old', $revision->order->ServiceRequestItem->uttp->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_made_in_new">Buatan (Pembetulan)</label>
                            {!! Form::text('tool_made_in_new', $revision->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif

                @if($revision->tool_factory != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_old">Pabrikan (Tertulis)</label>
                            {!! Form::text('tool_factory_old', $revision->order->ServiceRequestItem->uttp->tool_factory, ['class' => 'form-control','id' => 'tool_factory_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_new">Pabrikan (Pembetulan)</label>
                            {!! Form::text('tool_factory_new', $revision->tool_factory_new, ['class' => 'form-control','id' => 'tool_factory_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->tool_factory_address != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_address_old">Alamat Pabrikan (Tertulis)</label>
                            {!! Form::textarea('tool_factory_address_old', $revision->order->ServiceRequestItem->uttp->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_factory_address_new">Alamat Pabrikan (Pembetulan)</label>
                            {!! Form::textarea('tool_factory_address_new', $revision->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif

                @if($revision->label_sertifikat != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="label_sertifikat_old">Pemohon (Tertulis)</label>
                            {!! Form::text('label_sertifikat_old', $revision->request->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="label_sertifikat_new">Pemohon (Pembetulan)</label>
                            {!! Form::text('label_sertifikat_new', $revision->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->addr_sertifikat != null)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="addr_sertifikat_old">Alamat Pemohon (Tertulis)</label>
                            {!! Form::textarea('addr_sertifikat_old', $revision->request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat_old', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="addr_sertifikat_new">Alamat Pemohon (Pembetulan)</label>
                            {!! Form::textarea('addr_sertifikat_new', $revision->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat_new', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                @if($revision->others != null)
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="others">Lainnya (akan diperbaiki oleh petugas di instalasi terkait)</label>
                            {!! Form::textarea('others', $revision->others, ['class' => 'form-control','id' => 'others', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>

        

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_submit">Terima Order Perbaikan</button> 
        
        {!! Form::close() !!}

        </div>
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">


</script>
@endsection