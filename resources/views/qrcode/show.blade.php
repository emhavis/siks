@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>QR Code<br> <span class="c-white">List</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-add-user"></i>
            </div>
            <div class="header-title">
                <h3>QR Code</h3>
                <small>
                    Halaman Penambahan QR Code
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_edit">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>        
        <div class="panel-heading">
            <div class="panel-body">
                {!! Form::open(array('id' => 'form_create_qrcode')) !!}
                    <div class="form-group">
                        <label for="uml_name">Nama Unit Metrologi Legal</label>
                        {!! Form::select('uml_id', $umls, '',
                            ['class' => 'form-control',
                             'id' => 'uml_id',
                             'placeholder' => '- Pilih UML -',
                             'required']) !!}

                    </div>
                    <div class="form-group">
                        <label for="uml_sub_id">Nama Sub Unit Metrologi Legal</label>
                        {!! Form::select('uml_sub_id', array(), '',
                            ['class' => 'form-control',
                             'id' => 'uml_sub_id',
                             'placeholder' => '- Pilih Sub UML -',
                             'required']) !!}

                    </div>
                    <div class="form-group">
                        <label for="standard_measurement_type_id">Jenis Besaran</label>
                        {!! Form::select('standard_measurement_type_id', $standardMeasurementTypes, null,
                            ['class' => 'form-control',
                             'id' => 'standard_measurement_type_id',
                             'placeholder' => '- Pilih Jenis Besaran -',
                             'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="class">Dari QR Code</label>
                        {!! Form::select('from_qrcode', array(), null,
                            ['class' => 'form-control',
                             'id' => 'from_qrcode',
                             'placeholder' => '- Dari QR Code -',
                             'required']) !!}

                    </div>

                    <div class="form-group">
                        <label for="class">Sampai dengan QR Code</label>
                        {!! Form::select('to_qrcode', array(), null,
                            ['class' => 'form-control',
                             'id' => 'to_qrcode',
                             'placeholder' => '- Sampai dengan QR Code -',
                             'required']) !!}

                    </div>
            </div>
            <a type="button" href="{{ route('qrcode') }}" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</a>
            {!! Form::close() !!}
            <button type="submit" id="submit" class="btn btn-warning"><i class="fa fa-save"></i> Submit</button>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/scripts/JSPrintManager.js') }}"></script>
<script src="{{ asset('assets/scripts/zip.js') }}"></script>
<script src="{{ asset('assets/scripts/zip-ext.js') }}"></script>
<script src="{{ asset('assets/scripts/deflate.js') }}"></script>

<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.min.js') }}"></script>
<script type='text/javascript'>

var el = new Object();

$(document).ready(function(){

    $('#uml_id, #uml_sub_id, #standard_measurement_type_id, #from_qrcode, #to_qrcode').select2({ allowClear: true });

    $('#uml_id').change(function()
    {
        el.uml_id = this.value;

        $.post('{{ route('umlsub.dropdown') }}',{id: this.value,_token:"{{ csrf_token() }}"},function(response)
        {
            var standard_drop_list = $('#uml_sub_id');
            standard_drop_list.empty();

            standard_drop_list.append("<option>- Pilih Sub UML -</option>");
            $.each(response, function(index, element) {
                standard_drop_list.append("<option value='"+ element.id +"'>" + element.uml_sub_name + "</option>");
            });
        },"json");
    });

    $("#submit").click(function(e){
        e.preventDefault();
        var form_data = $("#form_create_qrcode").serialize();
        $.post('{{ route('qrcode.prints') }}',form_data,function(response)
        {
            if(response.length>0)
            {
                var cmds = '\x0AN';
                var x = 1;
                $.each(response,function(i,code)
                {
                    var ab = code.replace(/\./g, "");
                    var a = ab.slice(0,3);
                    var b = ab.slice(3,6);
                    var c = ab.slice(6,10);

////////////////////////////////////////////////////////////////////////////
                    // OPSI DUA KOLOM LABEL //
////////////////////////////////////////////////////////////////////////////
                    // if(x==1)
                    // {
                    //     cmds += '\x0Ab162,15,Q,s5,"'+code+'"';
                    //     cmds += '\x0AA162,125,0,3,1,1,N,"metrologilegal"';
                    //     cmds += '\x0AA307,15,0,4,1,1,N,"'+a+'"';
                    //     cmds += '\x0AA307,45,0,4,1,1,N,"'+b+'"';
                    //     cmds += '\x0AA292,75,0,4,1,1,N,"'+c+'"';
                    // }
                    // if(x==2)
                    // {
                    //     cmds += '\x0Ab497,15,Q,s5,"'+code+'"';
                    //     cmds += '\x0AA497,125,0,3,1,1,N,"metrologilegal"';
                    //     cmds += '\x0AA635,15,0,4,1,1,N,"'+a+'"';
                    //     cmds += '\x0AA635,45,0,4,1,1,N,"'+b+'"';
                    //     cmds += '\x0AA620,75,0,4,1,1,N,"'+c+'"';
                    // } 

                    // if(x==2)
                    // {
                    //     cmds += '\x0AP1';
                    //     cmds += '\x0AN';
                    //     x=0;
                    // }

////////////////////////////////////////////////////////////////////////////
                    // OPSI TIGA KOLOM LABEL //
////////////////////////////////////////////////////////////////////////////

                    if(x==1)
                    {
                        cmds += '\x0Ab62,13,Q,s5,"'+code+'"';
                        cmds += '\x0AA62,123,0,3,1,1,N,"metrologilegal"';
                        cmds += '\x0AA210,15,0,4,1,1,N,"'+a+'"';
                        cmds += '\x0AA210,45,0,4,1,1,N,"'+b+'"';
                        cmds += '\x0AA195,75,0,4,1,1,N,"'+c+'"';
                    }
                    if(x==2)
                    {
                        cmds += '\x0Ab337,13,Q,s5,"'+code+'"';
                        cmds += '\x0AA337,123,0,3,1,1,N,"metrologilegal"';
                        cmds += '\x0AA482,15,0,4,1,1,N,"'+a+'"';
                        cmds += '\x0AA482,45,0,4,1,1,N,"'+b+'"';
                        cmds += '\x0AA467,75,0,4,1,1,N,"'+c+'"';
                    } 
                    if(x==3)
                    {
                        cmds += '\x0Ab607,13,Q,s5,"'+code+'"';
                        cmds += '\x0AA607,123,0,3,1,1,N,"metrologilegal"';
                        cmds += '\x0AA752,15,0,4,1,1,N,"'+a+'"';
                        cmds += '\x0AA752,45,0,4,1,1,N,"'+b+'"';
                        cmds += '\x0AA737,75,0,4,1,1,N,"'+c+'"';
                    }

                    if(x==3)
                    {
                        cmds += '\x0AP1';
                        cmds += '\x0AN';
                        x=0;
                    }

// ///////////////////////////////////////////////////////////////////////////////////////////////

                    x++;
                });
                cmds += '\x0AP1\x0A';

                JSPM.JSPrintManager.start()
                .then(_ => {
                    var cpj = new JSPM.ClientPrintJob();
                    
                    cpj.clientPrinter = new JSPM.InstalledPrinter('Honeywell PC42t (203 dpi) - ESim');
                    // cpj.clientPrinter = new JSPM.InstalledPrinter('Honeywell PC42t-203-ESim');
                    
                    // cpj.clientPrinter = new JSPM.DefaultPrinter(); // Optional. Jika diatas tdk terdeteksi.

                    cpj.printerCommands = cmds;
                    cpj.sendToClient();

                    window.location = '{{ route('qrcode.show') }}';
                })
                .catch((e) => {
                  alert(e);
                });

            }

        },"json");
    })

    $('#standard_measurement_type_id, #uml_sub_id').change(function()
    {
        var from_qrcode = $('#from_qrcode');
        var to_qrcode = $('#to_qrcode');
        var uml_id = $("#uml_id").val();
        var uml_sub_id = $("#uml_sub_id").val();
        var standard_measurement_type_id = $("#standard_measurement_type_id").val();

        if(parseInt(uml_sub_id)>0 && parseInt(standard_measurement_type_id)>0 && parseInt(uml_id)>0)
        {
            get_qrcode();
        }
        else
        {
            from_qrcode.empty().select2();
            to_qrcode.empty().select2();
        }
    });


});

function get_qrcode(uml_sub_id,standard_measurement_type_id)
{
    var from_qrcode = $('#from_qrcode');
    var to_qrcode = $('#to_qrcode');
    var form_data = $('#form_create_qrcode').serialize();
    $.post('{{ route('qrcode.dropdown') }}',form_data,function(response)
    {
        console.log(response);
        from_qrcode.empty();
        to_qrcode.empty();
        
        var a = new Array();
        $.each(response[1], function(index, element)
        {
            a.push({"id":index,"text":element});
        });

        from_qrcode.select2({data:a});
        to_qrcode.select2({data:a});
    },"json");    
}
</script>

@endsection
