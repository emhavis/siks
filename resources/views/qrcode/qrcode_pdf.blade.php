<html>
<head>
    <title>QR Code List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table tr td,
        table tr th{
            font-size: 9pt;
        }
        p.small{
            font-size: 7pt;
        }
    @page
    {
        size: 10.5cm 20cm portrait;
        margin: 0;
        padding: 0;
    }
    body
    {
        margin: 0px; 
    }

    </style>
</head>
<body>
    <table style="margin-left: 41mm;padding: 0;" cellpadding="0" cellspacing="0">
        <tr style="margin: 0;padding: 0;">
            <?php $i = 0; ?>
            @foreach($qrcode_list as $qrcode)
                <td style="text-align: center; margin: 0;padding-top: 2mm;padding-bottom: 2mm; padding-right: 18mm;">
                    <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('assets/images/logo/logo_kemendag.png', 0.4, true)->margin(0)->size(50)->errorCorrection('H')->generate($qrcode)) !!} ">
                    <p class="small" style="margin: 0;padding: 0;">{{$qrcode}}</p>
                </td>
                @php
                $i++;
                if(($i%3)==0){ echo '</tr><tr>'; }
                @endphp
            @endforeach
        </tr>
    </table>
</body>
</html>