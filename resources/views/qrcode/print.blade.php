@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>QR Code<br> <span class="c-white">List</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-add-user"></i>
            </div>
            <div class="header-title">
                <h3>QR Code</h3>
                <small>
                    Halaman Penambahan QR Code
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.min.js') }}"></script>
<script type='text/javascript'>
$(document).ready(function(){

    $('#uml_id, #standard_measurement_type_id').select2({ allowClear: true });

    $("#qrcode_list").inputmask({'alias': 'numeric', 'placeholder': '0'});

});

</script>

@endsection
