@extends('layouts.app')

@section('content')
<div class="row">
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
                <form id="form_create_qrcode">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="uml_id">Nama Unit Metrologi Legal</label>
                            {!! Form::select('uml_id', $umls, '',
                                ['class' => 'form-control',
                                 'id' => 'uml_id',
                                 'placeholder' => '- Pilih UML -',
                                 'required']) !!}

                        </div>
                        <div class="form-group">
                            <label for="uml_sub_id">Nama Sub Unit Metrologi Legal</label>
                            {!! Form::select('uml_sub_id', array(), '',
                                ['class' => 'form-control',
                                 'id' => 'uml_sub_id',
                                 'placeholder' => '- Pilih Sub UML -',
                                 'required']) !!}

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-filled panel-c-warning">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="standard_measurement_type_id">Jenis Besaran</label>
                                    {!! Form::select('standard_measurement_type_id', $standardMeasurementTypes, null,
                                        ['class' => 'form-control',
                                         'id' => 'standard_measurement_type_id',
                                         'placeholder' => '- Pilih Jenis Besaran -',
                                         'required']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="qrcode_list">Jumlah QR Code</label>
                                    {!! Form::text('qrcode_list', '',
                                                   ['class' => 'form-control',
                                                    'id' => 'qrcode_list',
                                                    'required']) !!}

                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <a type="button" href="{{ route('qrcode') }}" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</a>
            <button type="submit" id="submit" class="btn btn-default">Submit</button>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type='text/javascript'>

var el = new Object();

$(document).ready(function(){

    $('#uml_id, #uml_sub_id, #standard_measurement_type_id').select2();

    $("#qrcode_list").inputmask({'alias': 'numeric', 'placeholder': '0'});

    $('#uml_id').change(function()
    {
        el.uml_id = this.value;

        $.post('{{ route('umlsub.dropdown') }}',
        {
            id: this.value,
            _token:"{{ csrf_token() }}"
        },
        function(response)
        {
            var standard_drop_list = $('#uml_sub_id');
            standard_drop_list.empty();

            standard_drop_list.append("<option>- Pilih Sub UML -</option>");
            $.each(response, function(index, element) {
                standard_drop_list.append("<option value='"+ element.id +"'>" + element.uml_sub_name + "</option>");
            });
        },"json");
    });

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#form_create_qrcode").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('qrcode.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('qrcode') }}';
            }
        });

    });
});

</script>

@endsection
