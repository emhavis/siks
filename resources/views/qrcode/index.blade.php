@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <a href="{{ route('qrcode.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Buat Baru</a>
    <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a>

    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tqrcode" class="table table-striped table-hover table-responsive-sm">
                <thead>
                        <th>UML</th>
                        <th>Sub UML</th>
                        <th>QR Code Alat</th>
                        <th>Besaran</th>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#tqrcode').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ route('qrcode.getdata') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
           "columns": [
                { "data": "uml_name" },
                { "data": "uml_sub_name" },
                { "data": "tool_code" },
                { "data": "standard_type" }
            ]

        });
    });
</script>
@endsection
