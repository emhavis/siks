@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <!-- <div id="alert_board"></div> -->
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>        
        <div class="panel-heading">
            <div class="panel-body">
            {!! Form::open(array('route' => 'measurementtype.store','id' => 'form_create_standard', 'enctype' => "multipart/form-data")) !!}
            <div class="col-md-6">
            <div class="panel panel-filled">
                <div class="panel-body">                    
                    <div class="form-group">
                        <label for="standard_measurement_type_id">Jenis Besaran</label>
                        <div class="input-group"> 
                        {!! Form::select('standard_measurement_type_id', $standardMeasurementTypes, null,
                            ['class' => 'form-control',
                             'id' => 'standard_measurement_type_id',
                             'placeholder' => '- Pilih Jenis Besaran -',
                             'required']) !!}
                        <div class="btn btn-group pull-right">
                        <button id="pop" data-action="add" data-type="standard_measurement_type_id" type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                        <button id="pop" data-action="edit" data-type="standard_measurement_type_id" type="button" class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                        <button id="pop" data-action="del" data-type="standard_measurement_type_id" type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                        </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="standard_tool_type_id">Jenis Alat</label>
                        <div class="input-group"> 
                        {!! Form::select('standard_tool_type_id', array(), null,
                            ['class' => 'form-control',
                             'id' => 'standard_tool_type_id',
                             'placeholder' => '- Pilih Jenis Alat -',
                             'required']) !!}
                        <div class="btn btn-group pull-right">
                        <button id="pop" data-action="add" data-type="standard_tool_type_id" type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                        <button id="pop" data-action="edit" data-type="standard_tool_type_id" type="button" class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                        <button id="pop" data-action="del" data-type="standard_tool_type_id" type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="standard_detail_type_id">Rincian Alat</label>
                        <div class="input-group"> 
                        {!! Form::select('standard_detail_type_id', array(), null,
                            ['class' => 'form-control',
                             'id' => 'standard_detail_type_id',
                             'placeholder' => '- Pilih Rincian Alat -',
                             'required']) !!}
                        <div class="btn btn-group pull-right">
                        <button id="pop" data-action="add" data-type="standard_detail_type_id" type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                        <button id="pop" data-action="edit" data-type="standard_detail_type_id" type="button" class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                        <button id="pop" data-action="del" data-type="standard_detail_type_id" type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="panel panel-filled panel-c-info">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="inspection_price_id">Inspeksi Alat(Inspection Price)</label>
                        <div class="input-group"> 
                        {!! Form::select('inspection_price_id', array(), null,
                            ['class' => 'form-control',
                             'id' => 'inspection_price_id',
                             'placeholder' => '- Pilih Inspeksi Alat -',
                             'required']) !!}
                        <div class="btn btn-group pull-right">
                        <button id="pop" data-action="add" data-type="inspection_price_id" type="button" class="btn btn-success ml-5"><i class="fa fa-plus"></i></button>
                        <button id="pop" data-action="edit" data-type="inspection_price_id" type="button" class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                        <button id="pop" data-action="del" data-type="inspection_price_id" type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="apgmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Add/Edit Data Master</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_apgmodal">
                            @csrf
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" name="standard_name" id="standard_name" class="form-control" required />
                            </div>
                            <div class="form-group" style="display: none;" id="price_box">
                                <label>Price</label>
                                <input type="text" name="price" id="price" class="form-control" required />
                            </div>
                            <div class="form-group" style="display: none;" id="unit_box">
                                <label>Unit</label>
                                {!! Form::select('unit', $units, "0", ['class' => 'form-control select2','id' => 'unit', 'required']) !!}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="simpan" class="btn btn-accent">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.min.js') }}"></script>
<script type='text/javascript'>

var el = new Object();

$(document).ready(function(){

    el.standard_measurement_type_id = 0;
    el.inspection_price_id = 0;
    el.standard_tool_type_id = 0;
    el.standard_detail_type_id = 0;

    $('#standard_measurement_type_id, #inspection_price_id, #standard_tool_type_id, #standard_detail_type_id').select2();

    $('#standard_measurement_type_id').change(function()
    {
        el.standard_measurement_type_id = $(this).val();
        set_standard_tool_type();
    });

    $('#standard_tool_type_id').change(function()
    {
        el.standard_tool_type_id = $(this).val();
        set_standard_detail_type();
    });

    $('#standard_detail_type_id').change(function()
    {
        el.standard_detail_type_id = $(this).val();
        set_inspection_price();
    });

    $('#inspection_price_id').change(function()
    {
        el.inspection_price_id = $(this).val();
    });


    $("button#pop").click(function(e){
        e.preventDefault();
        el.data = $(this).data();

        switch(el.data.action)
        {
            case "add":
                $("#apgmodal").modal().on('shown.bs.modal', function ()
                {
                    // $("#form_apgmodal")[0].reset();
                    $("#standard_name","#form_apgmodal").val('');
                    if(el.data.type=="inspection_price_id")
                    {
                        $("#price_box","#form_apgmodal").show();
                        $("#unit_box","#form_apgmodal").show();
                        $("#price","#form_apgmodal").val('');
                    }
                    else
                    {
                        $("#price_box","#form_apgmodal").hide();
                        $("#unit_box","#form_apgmodal").hide();
                    }
                });
                break;
            case "edit":
                var id = $('#'+el.data.type).select2('data')[0].id;
                var text = $('#'+el.data.type).select2('data')[0].text;

                if(parseInt(id)>0)
                {
                    $("#apgmodal").modal().on('shown.bs.modal', function ()
                    {
                        $("#standard_name","#form_apgmodal").val(text);
                        if(el.data.type=="inspection_price_id")
                        {
                            var a = text.split(" - ");
                            $("#price_box","#form_apgmodal").show();
                            $("#unit_box","#form_apgmodal").show();
                            $("#standard_name","#form_apgmodal").val(a[0]);
                            $("#price","#form_apgmodal").val(a[1]);
                            $("#unit","#form_apgmodal").val(a[2]);
                        }
                        else
                        {
                            $("#price_box","#form_apgmodal").hide();
                            $("#unit_box","#form_apgmodal").hide();
                        }
                    });
                }
                break;
            case "del":
                var form_data = '&oper='+el.data.action+'&type='+el.data.type;
                form_data += '&standard_measurement_type_id='+el.standard_measurement_type_id;
                form_data += '&inspection_price_id='+el.inspection_price_id;
                form_data += '&standard_tool_type_id='+el.standard_tool_type_id;
                form_data += '&standard_detail_type_id='+el.standard_detail_type_id;
                form_data += '&_token={{ csrf_token() }}';
                $.post('{{ route('measurementtype.store') }}',form_data,function(res){
                    if(res[0]==true)
                    {
                        switch(el.data.type)
                        {
                            case 'standard_measurement_type_id': set_standard_measurement_type(); break;
                            case 'standard_tool_type_id': set_standard_tool_type(); break;
                            case 'standard_detail_type_id': set_standard_detail_type(); break;
                            case 'inspection_price_id': set_inspection_price(); break;
                        }                        
                    }
                    else
                    {
                        toastr["info"](res[1], "Pemberitahuan")
                    }
                },"json");
                break;
        }


    });

    $("button#simpan").click(function(e)
    {
        e.preventDefault();
        var form_data = $("#form_apgmodal").serialize();
        form_data += '&oper='+el.data.action+'&type='+el.data.type;
        form_data += '&standard_measurement_type_id='+el.standard_measurement_type_id;
        form_data += '&inspection_price_id='+el.inspection_price_id;
        form_data += '&standard_tool_type_id='+el.standard_tool_type_id;
        form_data += '&standard_detail_type_id='+el.standard_detail_type_id;

        $.post('{{ route('measurementtype.store') }}',form_data,function(res){
            if(res[0]==true)
            {
                $('#apgmodal').modal('hide');
                switch(el.data.type)
                {
                    case 'standard_measurement_type_id': set_standard_measurement_type(); break;
                    case 'standard_tool_type_id': set_standard_tool_type(); break;
                    case 'standard_detail_type_id': set_standard_detail_type(); break;
                    case 'inspection_price_id': set_inspection_price(); break;
                }
            }
            else
            {
                toastr["error"](res[1], "Pemberitahuan")
                // $("#alert_board",'#apgmodal').text(res[1]).show('slideDown');
            }
        },"json")

    });

});

function set_standard_measurement_type()
{
    $.get('{{ route('measurementtypedropdown') }}',function(response)
    {
        var standard_drop_list = $('#standard_measurement_type_id');
        standard_drop_list.empty();

        standard_drop_list.append("<option>- Pilih Jenis Alat -</option>");
        $.each(response, function(index, element) {
            standard_drop_list.append("<option value='"+ index +"'>" + element + "</option>");
        });
    });

    el.standard_measurement_type_id = 0;
    el.inspection_price_id = 0;
    el.standard_tool_type_id = 0;
    el.standard_detail_type_id = 0;

    $('#inspection_price_id, #standard_tool_type_id, #standard_detail_type_id').empty().select2();
}

function set_standard_tool_type()
{
    $.get('{{ route('tooltypedropdown') }}/'+el.standard_measurement_type_id,
    function(response)
    {
            var standard_drop_list = $('#standard_tool_type_id');
            standard_drop_list.empty();

            standard_drop_list.append("<option>- Pilih Jenis Alat -</option>");
            $.each(response, function(index, element) {

            standard_drop_list.append("<option value='"+ index +"'>" + element + "</option>");
        });
    });

    el.inspection_price_id = 0;
    el.standard_tool_type_id = 0;
    el.standard_detail_type_id = 0;

    $('#inspection_price_id, #standard_detail_type_id').empty().select2();
}

function set_standard_detail_type()
{
    $.get('{{ route('detailtypedropdown') }}/'+el.standard_tool_type_id,
    function(response)
    {
        var standard_drop_list = $('#standard_detail_type_id');
        standard_drop_list.empty();

        standard_drop_list.append("<option>- Pilih Rincian Alat -</option>");
        $.each(response, function(index, element) {
            standard_drop_list.append("<option value='"+ index +"'>" + element + "</option>");
        });
    },"json");

    el.inspection_price_id = 0;
    el.standard_detail_type_id = 0;

    $('#inspection_price_id').empty().select2();
}

function set_inspection_price()
{
    $.get('{{ route('ipricedropdown') }}/'+el.standard_detail_type_id,
    function(response)
    {
        var inspection_price_list = $('#inspection_price_id');
        inspection_price_list.empty();

        inspection_price_list.append("<option>- Pilih Inspeksi Alat -</option>");
        $.each(response, function(index, element) {
            inspection_price_list.append("<option value='"+ element.id +"'>" + element.inspection_type+" - "+ element.price + " - "+element.unit+"</option>");
        });
    });

    el.inspection_price_id = 0;
}
</script>

@endsection
