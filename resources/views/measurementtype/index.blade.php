@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <a href="{{ route('measurementtype.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tStandardDetail" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Measurement Type</th>
                        <th>Tool Type</th>
                        <th>Detail Type</th>
                    </tr>
                </thead>
                <tbody>
                    @if($row->count())
                    @foreach($row as $standardtype)
                    <tr>
                        <td>{{ $standardtype->id }}</td>
                        <td>{{ $standardtype->standardtooltype->standardmeasurementtype->standard_type }}</td>
                        <td>{{ $standardtype->standardtooltype->attribute_name }}</td>
                        <td>{{ $standardtype->standard_detail_type_name }}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function () {
        
        $('#tStandardDetail').DataTable();

    });
</script>
@endsection