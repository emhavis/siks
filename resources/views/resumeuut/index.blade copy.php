@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
 
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            

            <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Nama Pemilik</th>
                        <th>Nama Pemesan</th>
                        <th>Jumlah Alat</th>
                        <th>Rincian Alat</th>
                        <th>Obyek Pengujian</th>
                        <th>Tarif PNBP</th>
                        <th>Total PNBP</th>
                        <th>Billing Code</th>
                        <th>NTPN</th>
                        <th>No. Kuitansi</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai</th>
                        <th>Status SLA</th>
                        <th>Instalasi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequest->no_order }}</td>
                        <td>{{ $row->ServiceRequest->label_sertifikat }}</td>
                        <td>{{ $row->ServiceRequest->requestor->full_name }}</td>
                        <td>{{ count($row->ServiceRequest->items) }}</td>
                        <td>{{ $row->ServiceRequestItem->uttp->type->uttp_type }}</td>
                        <td>{{ $row->ServiceRequestItemInspection->inspectionPrice->inspection_type }}</td>
                        <td>{{ $row->ServiceRequestItemInspection->price * $row->ServiceRequestItemInspection->quantity }}</td>
                        <td>{{ $row->ServiceRequest->total_price }}</td>
                        <td>{{ $row->ServiceRequest->billing_code }}</td>
                        <td>{{ $row->ServiceRequest->payment_code }}</td>
                        <td>&nbsp;
                        </td>
                        <td>{{ $row->ServiceRequest->received_date }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>&nbsp;</td>
                        <td>{{ $row->instalasi->nama_instalasi }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>  
    
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data").DataTable({
            scrollX: true,
        });
        
    });
</script>
@endsection