@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <!-- <a href="{{ route('requestuttp.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Pendaftaran</a> -->
    <!-- <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a> -->

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#frontdesk" aria-controls="frontdesk" role="tab" data-toggle="tab">Pendaftaran</a></li>
        <li role="presentation"><a href="#processing" aria-controls="processing" role="tab" data-toggle="tab">Proses</a></li>
        <li role="presentation"><a href="#done" aria-controls="done" role="tab" data-toggle="tab">Selesai</a></li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="frontdesk">
            <br/>
            <a href="{{ route('requestuttp.createbooking') }}" class="btn btn-w-md btn-primary" id="btn_create">Konfirmasi Booking</a>

            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <!--
                    {!! Form::open(['url' => route('requestuttp'),'method' => 'get'])  !!}
                    <div class="row mb-2">
                        <div class="col-md-9">
                            <div class="form-group">
                                {!! Form::select('status_id', $statuses, $id_arr, 
                                    ['class' => 'form-control select2','id' => 'status_id','multiple'=>true]) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                        <button role="submit" class="btn btn-w-md btn-accent" id="btn_filter">Filter</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    -->

                    <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Order</th>
                                <th>Nama Pemesan</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>
                                @if($row->payment_code==null)
                                xx-xxxx-xx-xxx
                                @endif
                                @if($row->payment_code!=null)
                                {{ $row->no_order }}
                                @endif
                                </td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->total_price,0,',','.') }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->status->status }}</td>
                                <td>
                                    <a target="_blank" href="{{ route('requestuttp.pdf', $row->id) }}" class="btn btn-warning btn-sm">Cetak Invoive/Order</a>
                                    @if($row->payment_code===null && $row->status_id === 3)
                                    <a href="{{ route('requestuttp.payment', $row->id) }}" class="btn btn-warning btn-sm">Bayar</a>
                                    @else
                                        @if($row->stat_service_request===1)
                                        <a href="{{ route('requestuttp.paymentcancel', $row->id) }}" class="btn btn-warning btn-sm">Batal</a>
                                        @endif
                                    @endif
                                    @if($row->status_id === 1)
                                    <a href="{{ route('requestuttp.editbooking', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                    <a href="{{ route('request.destroy', $row->id) }}" class="btn btn-warning btn-sm">Hapus</a>
                                    @elseif($row->status_id === 4)
                                    {!! Form::open(['url' => route('requestuttp.instalasi', $row->id)])  !!}
                                    {!! Form::submit('Kirim ke Instalasi', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_simpan', 'name' => 'submitbutton', 'value' => 'save']) !!}
                                    {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane active" id="processing">
            <div class="panel panel-filled table-area">
                <br/>
                <div class="panel-heading">
                   
                    <table id="table_data_process" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Order</th>
                                <th>Nama Pemesan</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_process as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>
                                @if($row->payment_code==null)
                                xx-xxxx-xx-xxx
                                @endif
                                @if($row->payment_code!=null)
                                {{ $row->no_order }}
                                @endif
                                </td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->total_price,0,',','.') }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->status->status }}</td>
                                <td>
                                    <a target="_blank" href="{{ route('requestuttp.pdf', $row->id) }}" class="btn btn-warning btn-sm">Cetak Invoive/Order</a>
                                    @if($row->payment_code===null && $row->status_id === 3)
                                    <a href="{{ route('requestuttp.payment', $row->id) }}" class="btn btn-warning btn-sm">Bayar</a>
                                    @else
                                        @if($row->stat_service_request===1)
                                        <a href="{{ route('requestuttp.paymentcancel', $row->id) }}" class="btn btn-warning btn-sm">Batal</a>
                                        @endif
                                    @endif
                                    @if($row->status_id === 1)
                                    <a href="{{ route('requestuttp.editbooking', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                    <a href="{{ route('request.destroy', $row->id) }}" class="btn btn-warning btn-sm">Hapus</a>
                                    @elseif($row->status_id === 4)
                                    {!! Form::open(['url' => route('requestuttp.instalasi', $row->id)])  !!}
                                    {!! Form::submit('Kirim ke Instalasi', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_simpan', 'name' => 'submitbutton', 'value' => 'save']) !!}
                                    {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane active" id="done">
            <div class="panel panel-filled table-area">
                <br/>
                <div class="panel-heading">
                   
                    <table id="table_data_done" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Order</th>
                                <th>Nama Pemesan</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_done as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>
                                @if($row->payment_code==null)
                                xx-xxxx-xx-xxx
                                @endif
                                @if($row->payment_code!=null)
                                {{ $row->no_order }}
                                @endif
                                </td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->total_price,0,',','.') }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->status->status }}</td>
                                <td>
                                    <a target="_blank" href="{{ route('requestuttp.pdf', $row->id) }}" class="btn btn-warning btn-sm">Cetak Invoive/Order</a>
                                    @if($row->payment_code===null && $row->status_id === 3)
                                    <a href="{{ route('requestuttp.payment', $row->id) }}" class="btn btn-warning btn-sm">Bayar</a>
                                    @else
                                        @if($row->stat_service_request===1)
                                        <a href="{{ route('requestuttp.paymentcancel', $row->id) }}" class="btn btn-warning btn-sm">Batal</a>
                                        @endif
                                    @endif
                                    @if($row->status_id === 1)
                                    <a href="{{ route('requestuttp.editbooking', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                    <a href="{{ route('request.destroy', $row->id) }}" class="btn btn-warning btn-sm">Hapus</a>
                                    @elseif($row->status_id === 4)
                                    {!! Form::open(['url' => route('requestuttp.instalasi', $row->id)])  !!}
                                    {!! Form::submit('Kirim ke Instalasi', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_simpan', 'name' => 'submitbutton', 'value' => 'save']) !!}
                                    {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data,#table_data_process,#table_data_done").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });
    });
</script>
@endsection