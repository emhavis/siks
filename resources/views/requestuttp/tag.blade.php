<html>
<head>
    <title>CETAK TAG</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table th{text-align: center;}
        table.f9 td,tr.f9 td, .f9{font-size: 8pt; padding: 0px;}
        table.f10 td,th{font-size: 10pt;}
        table.f11 td{font-size: 11pt;}
        table.f12 td{font-size: 12pt;}
        p.small{
            font-size: 7pt;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 8pt;
            line-height: 1.3; 
        }
        @page { margin: 0px; }

    </style>
</head>
<body>
    <table style="width: 70mm; padding: 4mm 0px; border: none;">

        <tr>
            <td style="vertical-align:middle;">
                <table style="padding: 1mm 0mm 1mm 4mm;">
                    <tr>
                        <td><img src='data:image/png;base64, {!! 
                                base64_encode(QrCode::format("png")
                                    ->size(70)
                                    ->errorCorrection("H")
                                    ->generate($row->ServiceRequestItem->no_order)) !!} '></td>
                    </tr>
                </table>
            </td>
            <td>
                <table style="padding: 0px;">
                    <tr>
                        <td style="vertical-align:top; width:6mm;">NO</td>
                        <td style="vertical-align:top; max-width:3mm;">:</td>
                        <td style="vertical-align:top;"><strong>{{ $row->ServiceRequestItem->no_order }} / {{ count($row->ServiceRequest->items) }}</strong></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">TGL</td>
                        <td style="vertical-align:top;">:</td>
                        <td style="vertical-align:top;">{{ date("d M Y", strtotime($row->ServiceRequestItem->order_at )) }} </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">ID</td>
                        <td style="vertical-align:top;">:</td>
                        <td style="vertical-align:top; max-width:30mm">
                            {{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no }})
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">INST</td>
                        <td style="vertical-align:top;">:</td>
                        <td style="vertical-align:top;">{{ $row->instalasi->nama_instalasi }} </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>