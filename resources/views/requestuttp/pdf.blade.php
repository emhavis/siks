<html>
<head>
    <title>{{ $row->payment_code?'ORDER':'INVOICE' }}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table th{text-align: center;}
        table th,td{padding: 5px 10px;}
        table.f9 td,tr.f9 td, .f9{font-size: 9pt;}
        table.f10 td,th{font-size: 10pt;}
        table.f11 td{font-size: 11pt;}
        table.f12 td{font-size: 12pt;}
        p.small{
            font-size: 7pt;
        }
    </style>
</head>
<body>
    <center>
        <p class="f9">Sistem Informasi Pelayanan UPTP IV</p>
    </center>

    @if ($row->total_price > 0)
    
    
        <table border="0">
            <tr>
                <td style="width: 22mm;">
                    <img src="{{ asset('assets/images/logo/logo_kemendag.png') }}" style="width: 22mm;">
                </td>
                <td>
                    <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN TERTIB NIAGA
                    <br/>DIREKTORAT METROLOGI</p>
                    <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
                </td>
            </tr>
        </table>
        <center>
            <h4><b>INVOICE</b></h4>
        </center>
        <table style="width: 100%;" class="f10">
            <tr>
                <td colspan="3" style="text-align: right;"><b>NO. PENDAFTARAN : {{$row->no_register}}</b></td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Perusahaan</b></td>
                <td style="width: 80mm;">: {{ $row->requestor->full_name }}</td>
                <td rowspan="3" style="text-align: right;vertical-align: top;"><b>NO. ORDER : xxxx-xx-xxx</b></td>
            </tr>
            <tr>
                <td><b>Alamat</b></td>
                <td>: {{ $row->requestor->alamat }}</td>
            </tr>
            <tr>
                <td><b>Jenis Pesanan</b></td>
                <td>: {{ $row->jenis_layanan }}</td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;" border="1" class="f10">
            <tr>
                <th>No.</th>
                <th>Nama Alat</th>
                <th>Jumlah Alat Per Unit/Set</th>
                <th>Objek Pengujian</th>
                <th>Banyak Set/Unit</th>
                <th>Biaya Set/Unit</th>
                <th>TOTAL BIAYA</th>
            </tr>
            @foreach($row->items as $idx => $item)
            <tr>
                <td style="vertical-align:top;">{{ $idx + 1 }}</td>
                <td style="vertical-align:top;">
                    {{ $item->uttp->type->uttp_type }}<br/>
                    {{ $item->uttp->tool_brand }}/{{ $item->uttp->tool_model }}/{{ $item->uttp->tool_type }} (Serial No: {{ $item->uttp->serial_no }})
                </td>
                <td style="vertical-align:top;">1 item per 1 set</td>
                <td style="vertical-align:top;">
                    <ul style="margin: 0;list-style: none; padding: 0;">
                    @foreach($item->inspections as $inspection)
                        <li style="margin: 0;">{{ $inspection->inspectionPrice->inspection_type }}</li>
                    @endforeach
                    </ul>
                </td>
                <td style="vertical-align:top; text-align:right;">
                    <ul style="margin: 0;list-style: none; padding: 0;">
                        @foreach($item->inspections as $inspection)
                            <li>{{ $inspection->quantity }}</li>
                        @endforeach
                    </ul>
                </td>
                <td style="vertical-align:top;text-align: right;">
                    <ul style="margin: 0;list-style: none; padding: 0;">
                        @foreach($item->inspections as $inspection)
                            <li>Rp. {{ number_format($inspection->price,2,',','.') }}</li>
                        @endforeach
                    </ul>
                </td>
                <td style="vertical-align:top;text-align: right;">
                    <ul style="margin: 0;list-style: none;text-align: right; padding: 0;">
                        @foreach($item->inspections as $inspection)
                            <li>Rp. {{ number_format($inspection->price*$inspection->quantity,2,',','.') }}</li>
                        @endforeach
                    </ul>
                </td>
            </tr>
            @endforeach
            @if($row->denda_inv_price != null && $row->denda_inv_price > 0)
            <tr>
                <td colspan="6">Denda Keterlambatan</td>
                <td style="text-align: right;">Rp. {{number_format($row->denda_inv_price - $row->total_price ,2,',','.')}}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: right;">Total Keseluruhan</td>
                <td style="text-align: right;">Rp. {{number_format($row->denda_inv_price,2,',','.')}}</td>
            </tr>
            @else
            <tr>
                <td colspan="6" style="text-align: right;">Total Keseluruhan</td>
                <td style="text-align: right;">Rp. {{number_format(isset($row->origin_price) ? $row->origin_price : 0 ,2,',','.')}}</td>
            </tr>
            @if($row->persen_potongan > 0)
            <tr>
                <td colspan="6" style="text-align: right;">Total Setelah Potongan {{$row->persen_potongan }} %</td>
                <td style="text-align: right;">Rp. {{number_format(($row->total_price),2,',','.')}}</td>
            </tr>
            @endif
            @endif
        </table>

        <br />
        <table style="width: 100%;" class="f10" border="1">
            <tr>
                <th colspan="3">Tanggal</th>
                <th rowspan="2">Penerima</th>
                <th rowspan="2">Pemohon</th>
                <th rowspan="2">Keterangan</th>
            </tr>
            <tr>
                <th>Terima</th>
                <th>Proses</th>
                <th>Selesai</th>
            </tr>
            <tr class="text-center">
                <td rowspan="2" style="height: 20mm;">{{date("d M Y",strtotime($row->received_date))}}</td>
                <td rowspan="2"></td>
                <td rowspan="2"></td>
                <td style="height: 20mm;"></td>
                <td></td>
                <td rowspan="2"></td>
            </tr>
            <tr>            
                <td style="text-align: center;">{{$row->MasterUsers->full_name}}</td>
                <td style="text-align: center;">{{$row->requestor->full_name}}</td>
            </tr>
            <tr>
                <td colspan="6">
                    Berdasarkan Undang-Undang Nomor 9 Tahun 2018 tentang Penerimaan Negara Bukan Pajak, apabila
                    Wajib Bayar tidak melakukan pembayaran PNBP Terutang sampai dengan jatuh tempo, maka akan dikenakan
                    sanksi administratif berupa <b>Denda 2% (dua persen) per bulan</b> dari jumlah PNBP Terutang dan 
                    bagian dari bulan dihitung satu bulan penuh.
                </td>
            </tr>
        </table>

        <br />
        <p style="font-size: 10pt">Pembayaran dapat dilakukan melalui SIMPONI dengan <strong>Kode Billing: {{ $row->billing_code }}</strong> dengan batas waktu terkahir tanggal <strong>{{date("d M Y",strtotime($row->billing_to_date))}}</strong></p>

        
    @endif

    
</body>
</html>