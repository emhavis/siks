<html>
<head>
    <title>{{ $row->payment_code?'ORDER':'INVOICE' }}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table th{text-align: center;}
        table th,td{padding: 5px 10px;}
        table.f9 td,tr.f9 td, .f9{font-size: 9pt;}
        table.f10 td,th{font-size: 10pt;}
        table.f11 td{font-size: 11pt;}
        table.f12 td{font-size: 12pt;}
        p.small{
            font-size: 7pt;
        }
    </style>
</head>
<body>
    <center>
        <p class="f9">Sistem Informasi Pelayanan UPTP IV</p>
    </center>
    <table border="0">
        <tr>
            <td style="width: 20mm;">
                <img src="{{ asset('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
            </td>
            <td>
                <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN<br />TERTIB NIAGA DIREKTORAT METROLOGI</p>
                <p class="f9">Jl. Parteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
            </td>
        </tr>
    </table>
    <center>
        <h4><b>BUKTI PENERIMAAN BARANG</b></h4>
    </center>
    <table style="width: 100%;" class="f10">
        <tr>
            <td colspan="3" style="text-align: right;"><b>NO. PENDAFTARAN : {{$row->no_register}}</b></td>
        </tr>
        <tr>
            <td style="width: 30mm;"><b>Perusahaan</b></td>
            <td style="width: 80mm;">: {{ $row->requestor->full_name }}</td>
            <td rowspan="3" style="text-align: right;vertical-align: top;"><b>NO. ORDER : 
                <?php
                    $no_orders = "";
                    foreach($row->items as $item) {
                        if ($no_orders != "") {
                            $no_orders .= ",";
                        }
                        $no_orders .= $item->no_order;
                    }
                ?>
            {{ $no_orders }}
            </b></td>
        </tr>
        <tr>
            <td><b>Alamat</b></td>
            <td>: {{ $row->requestor->alamat }}</td>
        </tr>
        <tr>
            <td><b>Jenis Pesanan</b></td>
            <td>: {{ $row->jenis_layanan }}</td>
        </tr>
    </table>
    <br />
    <table style="width: 100%;" border="1" class="f10">
        <tr>
            <th rowspan="2">No.</th>
            <th rowspan="2">Nama Alat</th>
            <th rowspan="2">Kapasitas/Kelas</th>
            <th rowspan="2">Objek Pengujian</th>
            <th rowspan="2">Jumlah</th>
            <th colspan="2">TARIF BERDASARKAN PP NO. 31/2017</th>
        </tr>
        <tr>
            <th>Tarif</th>
            <th>Total</th>
        </tr>
        @foreach($row->items as $idx => $item)
        <tr>
            <td style="vertical-align:top;">{{ $idx + 1 }}</td>
            <td style="vertical-align:top;">
                {{ $item->uttp->type->uttp_type }}<br/>
                {{ $item->uttp->tool_brand }}/{{ $item->uttp->tool_model }}/{{ $item->uttp->tool_type }} (Serial No: {{ $item->uttp->serial_no }})
            </td>
            <td style="vertical-align:top;">{{ $item->uttp->tool_capacity }} {{ $item->uttp->tool_capacity_unit }} </td>
            <td style="vertical-align:top;">
                <ul style="margin: 0;list-style: none;">
                @foreach($item->inspections as $inspection)
                    <li style="margin: 0;">{{ $inspection->inspectionPrice->inspection_type }}</li>
                @endforeach
                </ul>
            </td>
            <td style="vertical-align:top; text-align:right;">
                <ul style="margin: 0;list-style: none;">
                    @foreach($item->inspections as $inspection)
                        <li>{{ $inspection->quantity }}</li>
                    @endforeach
                </ul>
            </td>
            <td style="vertical-align:top;text-align: right;">
                <ul style="margin: 0;list-style: none;">
                    @foreach($item->inspections as $inspection)
                        <li>Rp. {{ number_format($inspection->price,2,',','.') }}</li>
                    @endforeach
                </ul>
            </td>
            <td style="vertical-align:top;text-align: right;">
                <ul style="margin: 0;list-style: none;text-align: right;">
                    @foreach($item->inspections as $inspection)
                        <li>Rp. {{ number_format($inspection->price*$inspection->quantity,2,',','.') }}</li>
                    @endforeach
                </ul>
            </td>
        </tr>
        @endforeach
        <tr>
            <td colspan="6" style="text-align: right;">Total Keseluruhan</td>
            <td style="text-align: right;">Rp. {{number_format($row->total_price,2,',','.')}}</td>
        </tr>
    </table>

    <br />
    <table style="width: 100%;" class="f10" border="1">
        <tr>
            <th colspan="3">Tanggal</th>
            <th rowspan="2">Penerima</th>
            <th rowspan="2">Pemohon</th>
            <th rowspan="2">Keterangan</th>
        </tr>
        <tr>
            <th>Terima</th>
            <th>Proses</th>
            <th>Selesai</th>
        </tr>
        <tr class="text-center">
            <td rowspan="2" style="height: 20mm;">{{date("d M Y",strtotime($row->received_date))}}</td>
            <td rowspan="2" style="height: 20mm;">{{date("d M Y",strtotime($row->received_date))}}</td>
            <td rowspan="2" style="height: 20mm;">{{date("d M Y",strtotime($row->estimated_date))}}</td>
            <td style="height: 20mm;"></td>
            <td></td>
            <td rowspan="2"></td>
        </tr>
        <tr>            
            <td style="text-align: center;">{{$row->MasterUsers->full_name}}</td>
            <td style="text-align: center;">{{$row->requestor->full_name}}</td>
        </tr>
    </table>
</body>
</html>