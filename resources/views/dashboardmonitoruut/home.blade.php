@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
@endsection

@section('content')
<form id="createGroup" action="{{ route('dashboardmonitoruut')}}" method="GET" >
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="start_date">Periode</label> 
                <div class="input-group input-daterange" id="dt_range">
                    <input type="text" class="form-control" id="start_date" name="start_date"
                    value="{{ $start != null ? date("d-m-Y", strtotime($start)) : '' }}">
                    <div class="input-group-addon">s/d</div>
                    <input type="text" class="form-control" id="end_date" name="end_date"
                    value="{{ $end != null ? date("d-m-Y", strtotime($end)) : '' }}">
                </div>
            </div>
        </div>
          <div class="col-md-4">
            {!! Form::submit('Filter', ['class' => 'btn btn-w-md btn-accent', 'id'=>'btn_filter']) !!}
        </div>
    </div>
</form>
<div class="row">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">Keseluruhan</a></li>
        
        <li role="presentation"><a href="#verifikasi" aria-controls="verifikasi" role="tab" data-toggle="tab">Verifikasi</a></li>
        <li role="presentation"><a href="#kalibrasi" aria-controls="kalibrasi" role="tab" data-toggle="tab">Kalibrasi</a></li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="all">
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <canvas id="chartAll"></canvas>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Laboratorium</th>
                                    <th>Jumlah UUT</th>
                                    <th>UUT Dalam Proses</th>
                                    <th>UUT Selesai Sesuai SLA</th>
                                    <th>UUT Selesai Melebihi SLA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summaries as $row)
                                <tr>
                                    <td>{{ $row->nama_lab }}</td>
                                    <td>
                                        <a href="#" class="details" 
                                            data-instalasi="{{ $row->lab_id }}" 
                                            data-type="all" 
                                            data-sla="semua">{{ $row->jumlah }}</a>
                                    </td>
                                    <td>
                                        <a href="#" class="details" 
                                            data-lab="{{ $row->lab_id }}" 
                                            data-type="all" 
                                            data-sla="proses">{{ $row->jumlah_proses }}</a>
                                    </td>
                                    <td>
                                        <a href="#" class="details" 
                                            data-lab="{{ $row->lab_id }}" 
                                            data-type="all" 
                                            data-sla="sesuai">{{ $row->jumlah_sesuai_sla }}</a>
                                    </td>
                                    <td>
                                        <a href="#" class="details" 
                                            data-lab="{{ $row->lab_id }}" 
                                            data-type="all" 
                                            data-sla="lebih">{{ $row->jumlah_lebih_sla }}</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> -->
        </div>
        <!-- <div role="tabpanel" class="tab-pane" id="verifikasi">
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <canvas id="chartVerifikasi"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <table id="data_table_verifikasi" class="table table-striped table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Laboratorium</th>
                                    <th>Jumlah UUT</th>
                                    <th>UUT Dalam Proses</th>
                                    <th>UUT Selesai Sesuai SLA</th>
                                    <th>UUT Selesai Melebihi SLA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summaries_v as $row)
                                <tr>
                                <td>{{ $row->nama_lab }}</td>
                                    <td><a href="#" class="details" 
                                        data-lab="{{ $row->lab_id }}" 
                                        data-type="verifikasi" 
                                        data-sla="semua">{{ $row->jumlah }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-lab="" 
                                        data-type="verifikasi" 
                                        data-sla="proses">{{ $row->jumlah_proses }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-lab="{{ $row->lab_id }}" 
                                        data-type="ttu" 
                                        data-sla="sesuai">{{ $row->jumlah_sesuai_sla }}</a></td>
                                    <td><a href="#" class="details" 
                                        data-lab="{{ $row->lab_id }}" 
                                        data-type="verifikasi" 
                                        data-sla="lebih">{{ $row->jumlah_lebih_sla }}</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- <div role="tabpanel" class="tab-pane" id="kalibrasi">
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <canvas id="chartKalibrasi"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-filled table-area">
                    <div class="panel-heading">
                        <table id="data_table_kalibrasi" class="table table-striped table-hover table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Laboratorium</th>
                                    <th>Jumlah UUT</th>
                                    <th>UUT Dalam Proses</th>
                                    <th>UUT Selesai Sesuai SLA</th>
                                    <th>UUT Selesai Melebihi SLA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summaries_k as $row)
                                <tr>
                                    <td>{{ $row->nama_lab }}</td>
                                    <td>{{ $row->jumlah }}</td>
                                    <td>{{ $row->jumlah_proses }}</td>
                                    <td>{{ $row->jumlah_sesuai_sla }}</td>
                                    <td>{{ $row->jumlah_lebih_sla }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>

<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="data_table_details" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>No Register</th>
                        <th>Layanan</th>
                        <th>Instalasi</th>
                        <th>Jenis Alat</th>
                        <th>Terima</th>
                        <th>Selesai</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/chart.js/dist/Chart.min.js') }}"></script>
<script>
$(document).ready(function()
{
    $('#data_table, #data_table_verifikasi, #data_table_kalibrasi').DataTable();
    var tbl_details = $('#data_table_details').DataTable();
    $('.input-daterange input').each(function() {
        $(this).datepicker({
            format:"dd-mm-yyyy",
        });
    });
    
    var ctx = $("#chartAll");
    const labels = @json($lab);
    const data = @json($summaries);
    const config = {
        type: 'bar',
        options: {
            onClick: function(e) {
                barOnClick('all', e);
            },
        },
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Jumlah Total',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(54, 162, 235)'
                }, 
                {
                    label: 'Proses',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah_proses',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(255, 159, 64)'
                },{
                    label: 'Selesai',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah_selesai',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(75, 192, 192)'
                }
                /*, 
                {
                    label: 'UUT Selesai Sesuai SLA',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah_sesuai_sla',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(75, 192, 192)'
                }, 
                {
                    label: 'UUT Selesai Melebihi SLA',
                    data: data,
                    parsing: {
                        yAxisKey: 'jumlah_lebih_sla',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(255, 99, 132)'
                }*/
            ]
        }
    };
    var chart = new Chart(ctx, config);

    var ctx_verifikasi = $("#chartVerifikasi");
    const labels_verifikasi = @json($lab_v);
    const data_verifikasi = @json($summaries_v);
    const config_verifikasi = {
        type: 'bar',
        data: {
            labels: labels_verifikasi,
            datasets: [
                {
                    label: 'Jumlah UUT',
                    data: data_verifikasi,
                    parsing: {
                        yAxisKey: 'jumlah',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(54, 162, 235)'
                }, 
                {
                    label: 'UUT dalam Proses',
                    data: data_verifikasi,
                    parsing: {
                        yAxisKey: 'jumlah_proses',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(255, 159, 64)'
                }, 
                {
                    label: 'UUT Selesai Sesuai SLA',
                    data: data_verifikasi,
                    parsing: {
                        yAxisKey: 'jumlah_sesuai_sla',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(75, 192, 192)'
                }, 
                {
                    label: 'UUT Selesai Melebihi SLA',
                    data: data_verifikasi,
                    parsing: {
                        yAxisKey: 'jumlah_lebih_sla',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(255, 99, 132)'
                }
            ]
        }
    };
    var chart_verifikasi = new Chart(ctx_verifikasi, config_verifikasi);

    var ctx_kalibrasi = $("#chartKalibrasi");
    const labels_kalibrasi = @json($lab_k);
    const data_kalibrasi = @json($summaries_k);
    const config_kalibrasi = {
        type: 'bar',
        data: {
            labels: labels_kalibrasi,
            datasets: [
                {
                    label: 'Jumlah UUT',
                    data: data_kalibrasi,
                    parsing: {
                        yAxisKey: 'jumlah',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(54, 162, 235)'
                }, 
                {
                    label: 'UUT dalam Proses',
                    data: data_kalibrasi,
                    parsing: {
                        yAxisKey: 'jumlah_proses',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(255, 159, 64)'
                }, 
                {
                    label: 'UUT Selesai Sesuai SLA',
                    data: data_kalibrasi,
                    parsing: {
                        yAxisKey: 'jumlah_sesuai_sla',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(75, 192, 192)'
                }, 
                {
                    label: 'UUT Selesai Melebihi SLA',
                    data: data_kalibrasi,
                    parsing: {
                        yAxisKey: 'jumlah_lebih_sla',
                        xAxisKey: 'nama_lab'
                    },
                    backgroundColor: 'rgb(255, 99, 132)'
                }
            ]
        }
    };
    var chart_kalibrasi = new Chart(ctx_kalibrasi, config_kalibrasi);

    $(document).on('click','#data_table tbody tr td a.details', function(e) {
        e.preventDefault();
       
        var data = { 
                start_date: $('#start_date').val(),
                end_date: $('#end_date').val(),
                lab: $(this).data('lab'),
                type: $(this).data('type'),
                sla: $(this).data('sla'),
            };
        
        getAndRedraw(data);
    });

    $('#nav').on('click', function(e) {
        tbl_details.clear().draw();
    });

    function barOnClick(type, e) {
        var cht = e.chart;
        
        const points = cht.getElementsAtEventForMode(e, 'nearest', { intersect: true }, true);

        if (points.length) {
            const firstPoint = points[0];
            const label = cht.data.labels[firstPoint.index];
            const value = cht.data.datasets[firstPoint.datasetIndex].data[firstPoint.index];
            const y = cht.data.datasets[firstPoint.datasetIndex].parsing.yAxisKey;

            switch(y) {
                case 'jumlah':
                    var sla = 'semua';
                    break;
                case 'jumlah_proses':
                    var sla = 'proses';
                    break;
                case 'jumlah_sesuai_sla':
                    var sla = 'sesuai';
                    break;
                case 'jumlah_lebih_sla':
                    var sla = 'lebih';
                    break;
                default:
                    var sla = 'semua';
            }

            var data = { 
                start_date: $('#start_date').val(),
                end_date: $('#end_date').val(),
                lab: value.lab_id,
                type: type,
                sla: sla,
            };
        
            getAndRedraw(data);
        }
    }

    function getAndRedraw(data) {

        $.get( "{{ route('dashboardmonitoruut.details') }}", data)
            .done(function(result) {
                tbl_details.clear();
                $.each(result, function(key, item) {
                    tbl_details.row.add([
                        item.no_order,
                        item.no_register,
                        item.jenis_layanan,
                        item.nama_lab,
                        item.jenis,
                        item.received_date,
                        item.staff_entry_dateout
                    ]);
                });
                tbl_details.draw();
            });
    }
});
</script>
@endsection