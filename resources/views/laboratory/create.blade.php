@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="nama_lab">Nama Laboratorium</label> 
                        {!!
                            Form::text("nama_lab",$row?$row->nama_lab:'',[
                            'class' => 'form-control',
                            'id' => 'nama_lab',
                            'placeholder' => 'Nama Laboratorium',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="tipe">Jenis Laboratorium</label> 
                        {!! Form::select('tipe', 
                            array("standard" => "Standard", "uttp" => "UTTP"),
                            $row?$row->tipe:'', 
                            ['class' => 'form-control', 'id' => 'tipe', 'placeholder' => '- Pilih Jenis -', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="quota_online">Kuota Online Harian</label> 
                        {!!
                            Form::number("quota_online",$row?$row->quota_online:'',[
                            'class' => 'form-control',
                            'id' => 'quota_online',
                            'placeholder' => 'Kuota Online Harian',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="quota_offline">Kuota Offline Harian</label> 
                        {!!
                            Form::number("quota_offline",$row?$row->quota_offline:'',[
                            'class' => 'form-control',
                            'id' => 'quota_offline',
                            'placeholder' => 'Kuota Offline Harian',
                            'required'
                            ]);
                        !!}
                    </div>
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function ()
{
    $('#tipe').select2({
        // placeholder: "- Pilih UML -",
        allowClear: true
    });

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('laboratory.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('laboratory') }}';
            }
        });

    });
});

</script>
@endsection