@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">

    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            
            <table id="table_data_penjadwalan" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Pendaftaran</th>
                        <th>Nama Pemilik</th>
                        <th>Nama Pemohon</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->no_register }}</td>
                        
                        <td>{{ $row->label_sertifikat }}</td>
                        <td>{{ $row->requestor->full_name }}</td>
                        <td>{{ $row->status->status }} 
                            @if($row->spuh_doc_id != null)
                            @if($row->spuhDoc->keterangan_tidak_siap != null)
                            <div class="alert alert-warning" role="alert">
                                Petugas Tidak Siap: {{ $row->spuhDoc->keterangan_tidak_siap }}
                            </div>
                            @endif
                            @if($row->spuhDoc->keterangan_tidak_lengkap != null)
                            <div class="alert alert-danger" role="alert">
                                Tidak Lengkap: {{ $row->spuhDoc->keterangan_tidak_lengkap }}
                            </div>
                            @endif
                            @if($row->spuhDoc->approval_note != null)
                            <div class="alert alert-danger" role="alert">
                                Tidak Lengkap: {{ $row->spuhDoc->approval_note }}
                            </div>
                            @endif
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('schedulinguut.schedule', $row->id) }}" class="btn btn-warning btn-sm">Penjadwalan</a>
                            @if($row->spuh_doc_id != null)
                            <a href="{{ route('schedulinguut.surattugas', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">Draft Surat Tugas</a>
                            @if($row->spuhDoc->keterangan_tidak_lengkap != null)
                            <a href="{{ route('schedulinguut.cancel', ['id' => $row->id]) }}" class="btn btn-warning btn-sm">Batal</a>
                            @endif
                            @endif
                            <button class="btn btn-danger btn-sm hapus" data-id="{{$row->id}}">Hapus</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
       
    
</div>
<div class="modal fade" id="mdelete" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="m-t-none">Konfirmasi</h4>
                    <hr>
                    <div class="row">
                        <form id="fdelete"action="" method="POST">
                            <div class="form-group">
                                <input type="hidden" id='order_id' name= "order_id" class="form-control">
                            </div>
                        </form>
                        <div class="col-md-12">
                            <p/> apakah yakin untuk menghapus data ini?
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger yakin" >hapus</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $('#table_data_penjadwalan tbody').on('click','.hapus', function(e){
            var id = $(this).data('id');
            $('#mdelete').modal('show');
            $('#mdelete').on('shown.bs.modal', function(){
                $('#order_id').val(id)
                console.log("id " + id)
            });
            $('#mdelete').on("click",".yakin",function(e){
                var form = $("#fdelete");
                var form_data = form.serialize();
                form_data += '&_token={{ csrf_token() }}';
                $.get('{{ route('schedulinguut.delete')}}', form_data, function(response){
                    // $('#mdelete').modal('hide');
                    console.log(response)
                    if(response==='ok')
                    {
                        $('.modal').each(function(){
                            $(this).modal('hide');
                        })
                        toastr['success']("Terimkasih","Data berhasil dihapus");
                        window.location.reload();
                    }
                });
            });
        });
        $('.btn-simpan').click(function(e){
            e.preventDefault();
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });
    });
</script>
@endsection