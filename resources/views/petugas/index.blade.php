@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <a href="{{ route('petugas.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUser" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($petugas->count())
                    @foreach($petugas as $p)
                    <tr>
                        <td>{{ $p->nip }}</td>
                        <td>{{ $p->nama }}</td>
                        <td>{{ $p->is_active == 't' ? 'Aktif' : 'Non Aktif' }}</td>
                        <td>
                            <a href="{{ route('petugas.create', $p->id) }}" class="btn btn-info btn-sm">Edit</a>
                            @if($p->is_active!="t")
                            <a href="#" id="action" data-id="{{ $p->id }}" data-action="delete" class="btn btn-warning btn-sm">Delete</a>
                            <a href="#" id="action" data-id="{{ $p->id }}" data-action="aktif" class="btn btn-warning btn-sm">Aktif</a>
                            @endif
                            @if($p->is_active=="t")
                            <a href="#" id="action" data-id="{{ $p->id }}" data-action="nonaktif" class="btn btn-warning btn-sm">Non Aktif</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function () {
    
    $('#tUser').DataTable();

    $("a#action").click(function(e)
    {
        e.preventDefault();
        var data = $(this).data();
        data._token = "{{ csrf_token() }}";
        $.post("{{ route('petugas.action') }}",data,function(response)
        {
            if(response.status==true)
            {
                toastr["info"]("Data berhasil dihapus", "Info");
                setTimeout(function()
                {
                    window.location.href = "{{ route("petugas") }}";
                },3000);
            }
            else
            {
                toastr["error"](response["message"], "Error");
            }
        });
    });

});
</script>
@endsection