@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">

@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="nama">Nama</label> 
                        {!!
                            Form::text("nama",$row?$row->nama:'',[
                            'class' => 'form-control',
                            'id' => 'nama',
                            'placeholder' => 'Nama',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="nip">NIP</label> 
                        {!!
                            Form::text("nip",$row?$row->nip:'',[
                            'class' => 'form-control',
                            'id' => 'nip',
                            'placeholder' => 'NIP',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label> 
                        {!!
                            Form::email("email",$row?$row->email:'',[
                            'class' => 'form-control',
                            'id' => 'email',
                            'placeholder' => 'Email',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="jabatan">Jabatan</label> 
                        {!!
                            Form::text("jabatan",$row?$row->jabatan:'',[
                            'class' => 'form-control',
                            'id' => 'jabatan',
                            'placeholder' => 'Jabatan',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="pangkat">Pangkat</label> 
                        {!! Form::select('pangkat', 
                            $pangkats,
                            $row?$row->pangkat:'', 
                            ['class' => 'form-control', 'id' => 'pangkat', 'placeholder' => '- Pilih Pangkat/Golongan -', 'required']) !!}
                    </div>
                    <div class="form-group ">
                        <label for="flag_unit">UPT</label> 
                        {!! Form::select('flag_unit', 
                            [
                                'uttp' => 'Balai UTTP',
                                'snsu' => 'Balai SNSU',
                                'non'  => 'Non UTTP/SNSU'    
                            ],
                            $row?$row->flag_unit:'', 
                            ['class' => 'form-control', 'id' => 'flag_unit', 'placeholder' => '- Pilih UPT -', 'required']) !!}
                    </div>
                    
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>

<script>
$(document).ready(function ()
{

    $('#flag_unit, #pangkat').select2();

    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('petugas.store') }}',formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = '{{ route('petugas') }}';
            }
        });

    });
});

</script>
@endsection