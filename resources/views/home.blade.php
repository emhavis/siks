@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="header-title">
                        <h4 class="m-b-xs">Dashboard</h4>
                        <h2 class="m-b-xs">
                            @if(Auth::user()->user_role=="3" || Auth::user()->user_role=="4")
                            {{ "Laboratorium ".ucfirst(strtolower($attribute["nama_lab"])) }}
                            @elseif(Auth::user()->user_role=="7")
                            {{ ucwords(strtolower($attribute["uml_name"])) }}
                            @else
                            {{ ucwords(strtolower($attribute["role_name"])) }}
                            @endif
                        </h2>
                        <small>
                            Berisikan fitur system untuk memudahkan akses informasi
                        </small>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
var myform;
var myhot;

$(document).ready(function () {
    // Run toastr notification with Welcome message
    setTimeout(function(){
        toastr.options = {
            "positionClass": "toast-top-right",
            "closeButton": true,
            "progressBar": true,
            "showEasing": "swing",
            "timeOut": "3000"
        };
        //toastr.warning('<strong>You entered SIKS</strong> <br/><small>Sistem Informasi Ketertelusuran Standar. </small>');
        toastr.warning('<strong>You entered SIMPEL UTTP IV</strong>');
    },1600)

});
</script>
@endsection