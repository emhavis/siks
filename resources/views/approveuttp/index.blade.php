@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<style type"text/css"> 
    .badge-notif-tab {
            position:relative;
    }
    .badge-notif-tab[data-badge]:after {
            content:attr(data-badge);
            position:absolute;
            top:-10px;
            right:-5px;
            font-size:.7em;
            background:#f3a709;
            color:white;
            width:18px;
            height:18px;
            text-align:center;
            line-height:18px;
            border-radius: 50%;
    }
</style>
@endsection

@section('content') 
<div class="row">

    <ul class="nav nav-tabs" role="tablist" id="tabs">
        <li role="presentation" class="active">
            <a href="#kn" aria-controls="kn" role="tab" data-toggle="tab" 
                class="badge-notif-tab" 
                <?php if ((count($rows_kn)) > 0 ){ ?> 
                    data-badge="{{count($rows_kn)}}" 
                <?php }else{ } ?>
            >Dalam Kantor</a>
        </li>
        <li role="presentation">
            <a href="#dl" aria-controls="dl" role="tab" data-toggle="tab" 
                class="badge-notif-tab" 
                <?php if ((count($rows_dl)) > 0 ){ ?> 
                    data-badge="{{count($rows_dl)}}" 
                <?php }else{ } ?>
            >Dinas Luar</a>
        </li>
    </ul>

    <div class="tab-content" id="nav-tabContent">
        <div role="tabpanel" class="tab-pane active" id="kn">
            <br/>            

            <div class="panel panel-filled table-area">
                <div class="panel-body">

                    <button class="btn btn-w-md btn-success" id="btn_simpan_kn" disabled >Setujui Terpilih</button>
                    
                    <div class="row"><div class="col">&nbsp;</div></div>
                    
                    <table id="data_table_kn" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th data-orderable="false">{!! Form::checkbox('select_all_kn', 'select_all_kn', false, ['class' => 'form-check-input', 'id' => 'select_all_kn' ]) !!}</th>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Pengujian</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Catatan Revisi</th>
                                <th>Status</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_kn as $row)
                            <tr id="{{ $row->id }}">
                                <td>{!! Form::checkbox('row_selected', 'row_selected', false, ['class' => 'form-control', ]) !!}</td>
                                <td>{{ $row->ServiceRequestItem->no_order }} / {{ count($row->ServiceRequest->items) }}<br/>
                                    {{ $row->ServiceRequest->lokasi_pengujian == 'luar' ? '(DL)' : '(KN)' }}
                                </td>
                                        
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>
                                    @foreach($row->ServiceRequestItem->inspections as $inspection)
                                    {{ $inspection->inspectionPrice->inspection_type }}
                                    <br/>
                                    @endforeach
                                </td>
                                <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : '' }}</td>
                                <td>{{ $row->TestBy1 ? $row->TestBy1->full_name : "" }} {{ $row->TestBy2 ? ' & '.$row->TestBy2->full_name : "" }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->staff_entry_datein)) }}</td>
                                <td>
                                    @if($row->cancel_at == null)
                                    {{ $row->staff_entry_dateout != null ? date("d-m-Y", strtotime($row->staff_entry_dateout)) : '' }}
                                    @else
                                    BATAL: {{ date('d-m-Y', strtotime($row->cancel_at)) }}
                                    @endif
                                </td>
                                <td>
                                    @if($row->subkoordinator_notes != null)
                                    <div class="alert alert-warning" role="alert">
                                        Catatan Subkoordinator: {{ $row->subkoordinator_notes }}
                                    </div>
                                    @endif
                                    @if($row->kabalai_notes != null)
                                    <div class="alert alert-danger" role="alert">
                                        Catatan Kabalai: {{ $row->kabalai_notes }}
                                    </div>
                                    @endif
                                </td>
                                <td>
                                    {{ $row->ServiceRequestItem->status->status }}<br/>
                                    Draft: {{ $row->stat_sertifikat == 3 ? 'Sudah Disetujui Ka Balai' :
                                        ($row->stat_sertifikat == 2 ? 'Sudah Disetujui Ketua Tim' : 'Penyusunan Draft' )}}<br/>
                                    Alat: {{ $row->stat_warehouse == 2 ? 'Sudah Diserahterimakan dengan Pemohon' : 
                                                ($row->stat_warehouse == 1 ? 'Berada di Gudang' : 'Berada di Instalasi')}}
                                </td>
                                <td>
                                    @if($row->stat_sertifikat==null)
                                        @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                        <a href="{{ route('approveuttp.approvekalab', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                        @elseif($row->ServiceRequest->service_type_id == 4 || $row->ServiceRequest->service_type_id == 5)
                                        <a href="{{ route('approveuttp.approvesubko', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                        @endif
                                    @elseif($row->stat_sertifikat==1)
                                    <a href="{{ route('approveuttp.approvesubko', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                    @elseif($row->stat_sertifikat==2)
                                    <a href="{{ route('approveuttp.approve', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                    @endif
                                    @if($approval_before_delegasi != null && $row->stat_sertifikat == $approval_before_delegasi && $delegator != null)
                                    Delegasi dari {{ $delegator != null ? $delegator : '' }}
                                    @endif
                                </td>
                                <td>
                                
                                    @if($row->cancel_at != null)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->file_skhp!==null)
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                    <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="dl">
            <br/>            

            <div class="panel panel-filled table-area">
                <div class="panel-body">

                    <button class="btn btn-w-md btn-success" id="btn_simpan_dl" disabled >Setujui Terpilih</button>
                    
                    <div class="row"><div class="col">&nbsp;</div></div>
    
                    <table id="data_table_dl" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th data-orderable="false">{!! Form::checkbox('select_all_dl', 'select_all_dl', false, ['class' => 'form-check-input', 'id' => 'select_all_dl' ]) !!}</th>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Pengujian</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Catatan Revisi</th>
                                <th>Status</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_dl as $row)
                            <tr id="{{ $row->id }}">
                                <td>{!! Form::checkbox('row_selected', 'row_selected', false, ['class' => 'form-control', ]) !!}</td>
                                <td>{{ $row->ServiceRequestItem->no_order }} / {{ count($row->ServiceRequest->items) }}<br/>
                                    {{ $row->ServiceRequest->lokasi_pengujian == 'luar' ? '(DL)' : '(KN)' }}
                                </td>

                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>
                                    @foreach($row->ServiceRequestItem->inspections as $inspection)
                                    {{ $inspection->inspectionPrice->inspection_type }}
                                    <br/>
                                    @endforeach
                                </td>
                                <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : '' }}</td>
                                <td>{{ $row->TestBy1 ? $row->TestBy1->full_name : "" }} {{ $row->TestBy2 ? ' & '.$row->TestBy2->full_name : "" }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->staff_entry_datein)) }}</td>
                                <td>
                                    @if($row->cancel_at == null)
                                    {{ $row->staff_entry_dateout != null ? date("d-m-Y", strtotime($row->staff_entry_dateout)) : '' }}
                                    @else
                                    BATAL: {{ date('d-m-Y', strtotime($row->cancel_at)) }}
                                    @endif
                                </td>
                                <td>
                                    @if($row->subkoordinator_notes != null)
                                    <div class="alert alert-warning" role="alert">
                                        Catatan Subkoordinator: {{ $row->subkoordinator_notes }}
                                    </div>
                                    @endif
                                    @if($row->kabalai_notes != null)
                                    <div class="alert alert-danger" role="alert">
                                        Catatan Kabalai: {{ $row->kabalai_notes }}
                                    </div>
                                    @endif
                                </td>
                                <td>
                                    {{ $row->ServiceRequestItem->status->status }}<br/>
                                    Draft: {{ $row->stat_sertifikat == 3 ? 'Sudah Disetujui Ka Balai' :
                                        ($row->stat_sertifikat == 2 ? 'Sudah Disetujui Ketua Tim' : 'Penyusunan Draft' )}}<br/>
                                    Alat: {{ $row->stat_warehouse == 2 ? 'Sudah Diserahterimakan dengan Pemohon' : 
                                                ($row->stat_warehouse == 1 ? 'Berada di Gudang' : 'Berada di Instalasi')}}
                                </td>
                                <td>
                                    @if($row->stat_sertifikat==null)
                                        @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                        <a href="{{ route('approveuttp.approvekalab', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                        @elseif($row->ServiceRequest->service_type_id == 4 || $row->ServiceRequest->service_type_id == 5)
                                        <a href="{{ route('approveuttp.approvesubko', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                        @endif
                                    @elseif($row->stat_sertifikat==1)
                                    <a href="{{ route('approveuttp.approvesubko', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                    @elseif($row->stat_sertifikat==2)
                                    <a href="{{ route('approveuttp.approve', $row->id) }}" class="btn btn-success btn-sm">PERSETUJUAN</a>
                                    @endif
                                    @if($approval_before_delegasi != null && $row->stat_sertifikat == $approval_before_delegasi && $delegator != null)
                                    Delegasi dari {{ $delegator != null ? $delegator : '' }}
                                    @endif
                                </td>
                                <td>
                                
                                    @if($row->cancel_at != null)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->file_skhp!==null)
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                    <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-body">
                <h4 class="m-t-none">Konfirmasi Persetujuan</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <p>Apakah benar akan menyetujui semua data yang dipilih?</p>
            </div>
            <div class="modal-footer">
            {!! Form::open(['url' => route('approveuttp.approvesubmitall'), 'files' => true])  !!}
                <input type="hidden" name="ids" id="ids" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" id="simpan" class="btn btn-accent">SETUJU</button>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    let table_kn = $('#data_table_kn').DataTable({
        "order": [[ 1, "asc" ]],
        columnDefs: [ {
            targets: 0,
            orderable: false,
            searchable: false,
        } ],
        select: {
            style: 'multi',
            selector: 'td:first-child',
        },
        scrollX: true,
    });
    let table_dl = $('#data_table_dl').DataTable({
        "order": [[ 1, "asc" ]],
        columnDefs: [ {
            targets: 0,
            orderable: false,
            searchable: false,
        } ],
        select: {
            style: 'multi',
            selector: 'td:first-child',
        },
        scrollX: true,
    });

    $('#data_table_kn tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');

        //console.log(table.rows('.selected').data());
        //console.log(table.rows('.selected').data().length +' row(s) selected');
        var cb = $(this).find("> td:first-child > input:checkbox");
        
        //console.log($(this).hasClass('selected'));
        cb.prop('checked', $(this).hasClass('selected'));

        var rows = table_kn.rows('.selected').ids();
        $('#ids').val(rows.join(","));
        $('#btn_simpan_kn').prop("disabled", false);
    } );

    $("#btn_simpan_kn").click(function(e){
        e.preventDefault();
        $("#prosesmodal").modal('show');
    });

    $("#select_all_kn").change(function() {
        //console.log(table.rows());
        //console.log(table.rows('.selected').data().length +' row(s) selected');

        if ($(this).is( ":checked" )) {
            table_kn.rows().nodes().to$().addClass('selected');     
            table_kn.rows().nodes().to$().find("> td:first-child > input:checkbox").prop('checked', true);
        } else {
            table_kn.rows().nodes().to$().removeClass('selected');
            table_kn.rows().nodes().to$().find("> td:first-child > input:checkbox").prop('checked', false);
        }

        var rows = table_kn.rows('.selected').ids();
        $('#ids').val(rows.join(","));
        $('#btn_simpan_kn').prop("disabled", false);
    });


    $('#data_table_dl tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');

        //console.log(table.rows('.selected').data());
        //console.log(table.rows('.selected').data().length +' row(s) selected');
        var cb = $(this).find("> td:first-child > input:checkbox");
        
        //console.log($(this).hasClass('selected'));
        cb.prop('checked', $(this).hasClass('selected'));

        var rows = table_dl.rows('.selected').ids();
        $('#ids').val(rows.join(","));
        $('#btn_simpan_dl').prop("disabled", false);
    } );

    $("#btn_simpan_dl").click(function(e){
        e.preventDefault();
        $("#prosesmodal").modal('show');
    });

    $("#select_all_dl").change(function() {
        //console.log(table.rows());
        //console.log(table.rows('.selected').data().length +' row(s) selected');

        if ($(this).is( ":checked" )) {
            table_dl.rows().nodes().to$().addClass('selected');     
            table_dl.rows().nodes().to$().find("> td:first-child > input:checkbox").prop('checked', true);
        } else {
            table_dl.rows().nodes().to$().removeClass('selected');
            table_dl.rows().nodes().to$().find("> td:first-child > input:checkbox").prop('checked', false);
        }

        var rows = table_dl.rows('.selected').ids();
        $('#ids').val(rows.join(","));
        $('#btn_simpan_dl').prop("disabled", false);
    });
});

</script>
@endsection