<html>
<head>
    <title>{{ $row->payment_code?'ORDER':'INVOICE' }}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table th{text-align: center;}
        table th,td{padding: 5px 10px;}
        table.f9 td,tr.f9 td, .f9{font-size: 8pt;}
        table.f10 td,th{font-size: 10pt;}
        table.f11 td{font-size: 11pt;}
        table.f12 td{font-size: 12pt;}
        p.small{
            font-size: 7pt;
        }
        table {
            border: 1px solid black;
        }
    </style>
</head>
<body>
    @foreach($row->items as $idx => $item)
    @foreach($item->inspections as $idxinsp => $inspection)
    <table style="width: 100mm; " class="f9">
        
        <tr>
            <td rowspan="3">{{ QrCode::size(100)->margin(0.1)->generate($row->no_order . '#' . $item->id . '#' . $inspection->id) }}</td>
            <td style="vertical-align:top;">NO ORDER</td>
            <td style="vertical-align:top;">: {{ $row->no_order }}</td>
        </tr>
        <tr>
            <td style="vertical-align:top;">NAMA ALAT</td>
            <td style="vertical-align:top;">:
                {{ $item->uuts->tool_brand }}<br/>
                {{ $item->uuts->tool_brand }}/{{ $item->uuts->tool_model }} ({{ $item->uuts->serial_no }})
            </td>
        </tr>
        <!-- <tr>
            <td style="vertical-align:top;">NAMA LAB</td>
            <td style="vertical-align:top;">:
                {{ $inspection->StandardInspectionPrice->lab }} 
            </td>
        </tr> -->
        <tr>
            <td style="vertical-align:top;">PENGUJIAN:</td>
            <td style="vertical-align:top;">
                {{ $inspection->inspectionPrice->inspection_type }} 
            </td>
        </tr>
        <!--
        <tr>
            <td style="vertical-align:top;">PENGUJIAN:</td>
            <td style="vertical-align:top;">
                <ul style="margin: 0;list-style: none;">
                @foreach($item->inspections as $inspection)
                    <li style="margin: 0;">{{ $inspection->StandardInspectionPrice->inspection_type }}</li>
                @endforeach
                </ul>
            </td>
        </tr>
        -->
    </table>
    @endforeach
    @endforeach
</body>
</html>