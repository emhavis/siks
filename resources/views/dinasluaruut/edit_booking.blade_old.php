@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        <form id="form_create_request">
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Booking</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="booking_id">No Booking</label>
                            {!! Form::text('booking_no', $booking->booking_no, ['class' => 'form-control','id' => 'booking_no', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="for_sertifikat">Peruntukan Sertifikat</label>
                            {!! Form::text('for_sertifikat', $request->for_sertifikat, ['class' => 'form-control','id' => 'for_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                    -->
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="label_sertifikat">Nama Pemilik</label>
                            {!! Form::hidden('uut_owner_id', $request->uut_owner_id, ['class' => 'form-control','id' => 'uut_owner_id', 'readonly']) !!}
                            {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                   

                    <!-- <div class="col-md-12">
                        <div class="form-group">
                            <label for="uttp_owner_id">Label Sertifikat</label>
                            <select class="form-control" name="uut_owner_id" id="uut_owner_id">
                                <option value="{{ $request->uut_owner_id }}" selected>{{ $request->label_sertifikat }}</option>
                            </select>   
                        </div>
                    </div> -->
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Pemilik</label>
                            {!! Form::text('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>  
                </div>
            </div>
        </div>

        <?php if($booking->file_certificate !=''): ?>
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Surat permohonan pengujian.</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                                <a href="<?= config('app.siks_url') ?>/tracking/permohonan/certificate/{{$booking->id}}" class="btn btn-info btn-sm">Surat permohonan.</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p>
                        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                            Opsi
                        </a>
                        </p>
                        <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                            <div class="form-group">
                                <a href="{{ route('requestuut.destroy', $request->id) }}" class="btn btn-danger btn-sm">Tolak permohonan.</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif ?>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pendaftar</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Nama Pendaftar</label>
                            {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Jenis Tanda Pengenal</label>
                            {!! Form::text('id_type_id', $requestor->kantor == "Perusahaan" ? "NIB" : ($requestor->kantor == "Mahasiswa" ? "KTP" : "NPWP"), ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                            {!! Form::text('pic_id_no', $requestor->kantor == "Perusahaan" ? $requestor->nib : ( $requestor->kantor == "Mahasiswa" ?   $requestor->ktp : $requestor->npwp), ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_phone_no">Nomor Telepon</label>
                            {!! Form::text('pic_phone_no', $requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_email">Alamat Email</label>
                            {!! Form::text('pic_email', $requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @if ($request->lokasi_pengujian == 'dalam')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="received_date">Tanggal Masuk Alat</label>
                            {!! Form::text('received_date', date("d-m-Y", strtotime(isset($request->received_date) ? $request->received_date : date("Y-m-d"))), ['class' => 'form-control','id' => 'receipt_date', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="estimate_date">Tanggal Perkiraan Selesai</label>
                            {!! Form::text('estimated_date', date("d-m-Y", strtotime(isset($request->estimated_date) ? $request->estimated_date : date("Y-m-d"))), ['class' => 'form-control','id' => 'estimated_date', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_from">Usulan Jadwal Mulai</label>
                            {!! Form::text('scheduled_test_date_from', date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_from', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_to">Usulan Jadwal Selesai</label>
                            {!! Form::text('scheduled_test_date_to', date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_to', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lokasi_pengujian">Lokasi Pengujian</label>
                            {!! Form::text('jenis_layanan', $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor', ['class' => 'form-control','id' => 'lokasi_pengujian_view', 'readonly']) !!}
<!--                             
                            <select class="form-control" name="lokasi_pengujian" id="lokasi_pengujian">
                                <option value="dalam" <?= $request->lokasi_pengujian == 'dalam' ? 'selected' : '' ?> >Dalam Kantor</option>
                                <option value="luar" <?= $request->lokasi_pengujian == 'luar' ? 'selected' : '' ?>>Luar Kantor</option>  
                            </select>
                            -->
                        </div>
                        
                    </div>
                </div>
                @if ($request->lokasi_pengujian == 'dalam')
                @else
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inspection_loc">Alamat Lokasi Pengujian</label>
                            {!! Form::text('inspection_loc', $request->inspectionKabKot->nama.' - ' .$request->inspectionProv->nama, ['class' => 'form-control','id' => 'inspection_loc', 'readonly']) !!}
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="inspection_prov_id">Alamat Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                            {!! Form::text('inspection_prov_id', null,null, ['class' => 'form-control','id' => 'inspection_prov_id', 'readonly']) !!}
                        </div>
                    </div> -->
                </div>
                @endif
            </div>
        </div>
        </form>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Item Pengujian x</h4>
            </div>

            <div class="panel-body">
                <table class ="table table-responsive-sm input-table" id ="t-datas">
                    <thead>
                        <th>No</th>
                        <th style="width: 200px;">Jenis Alat</th>
                        <th style="width: 200px;">Nama Alat</th>
                        <th>LAB</th>
                        <th>SLA</th>
                        <th>Merek</th>
                        <th>Buatan</th>
                        <th>Model/Tipe</th>
                        <th>No. Seri</th>
                        <th>Media Uji</th>
                        <th>Kapasitas</th>
                        <th>Satuan</th>
                        <th>Jumlah</th>
                        @if ($request->lokasi_pengujian == 'luar')
                            <th style="width: 200px;">Alamat Lokasi Pengujian</th>
                        @endif
                        <th>Kelengkapan Persyaratan(Break Down) </th>
                        <th style="width: 200px;">Permintaan kaji ulang</th>
                        <th style="width: 200px;">Item Pengujian</th>
                        <th>Total Biaya</th>
                        <th>Confirmed ?</th>
                        <th></th>

                    </thead>
                    <tbody>
                        <?php $num = 0;?>
                        @foreach($request->items as $item)
                            <?php $num +1;?>
                            {!! Form::hidden('id', $item->id) !!}
                            {!! Form::hidden('uut_id', $item->uut_id) !!}
                            {!! Form::hidden('uut_type_id', $item->uuts->type_id) !!}
                            <td>{{$num}}</td>
                            <td>{{$item->uuts ? $item->uuts->stdtype->uut_type :''}}</td>
                            <td>{{$item->uuts ? $item->uuts->tool_name : ''}}</td>
                            <td>{{$item->uuts->stdtype->lab->nama_lab}}</td>
                            <td>{{$item->sla_overide ? $item->sla_overide : $item->uuts->stdtype->lab->sla_day}}</td>
                            <td>{{$item->uuts ? $item->uuts->tool_brand : ''}}</td>
                            <td>{{$item->uuts->tool_made_in}}</td>
                            <td>{{$item->uuts ? $item->uuts->tool_model : ''}}</td>
                            <td>{{$item->uuts ? $item->uuts->serial_no : ''}}</td>
                            <td>{{$item->uuts ? $item->uuts->tool_media : ''}}</td>
                            <td>{{$item->uuts->tool_capacity}}</td>
                            <td>{{$item->uuts->tool_capacity_unit}}</td>
                            <td>{{$item->uuts->jumlah}}</td>
                            <!-- untuk informasi lain -->
                            @if ($request->lokasi_pengujian == 'luar')
                                <td>{{ $item->location }}, {{ ($item->kabkot ? $item->kabkot->nama : '') . ', ' . ($item->provinsi ? $item->provinsi->nama : '') }}</td>
                            @else 
                                @if ($request->service_type_id == 4 || $request->service_type_id == 5)
                                <td>{{ $item->location }}</td>
                                @endif
                            @endif
                            <!-- Biaya -->
                            <td> 
                                @if ($request->service_type_id == 1)
                                    @if($item->path_application_letter != null)
                                        <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $item->id }}"
                                            class="btn btn-default btn-sm faa-parent animated-hover" >
                                            <i class="fa fa-download faa-flash"></i> Surat Permohonan
                                        </a>
                                    @endif
                                @elseif ($request->service_type_id == 2)
                                    @if($item->path_last_certificate != null)
                                        <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $item->id }}"
                                            class="btn btn-default btn-sm faa-parent animated-hover" >
                                            <i class="fa fa-download faa-flash"></i> Sertifikat Sebelumnya
                                        </a>
                                    @endif
                                @endif
                            </td>
                            <!-- Item uji -->
                            <td>
                                @foreach($item->inspections as $inspection)
                                    {{ $inspection->inspectionPrice->inspection_type }}
                                @endforeach
                            </td>
                            <td>{{ $item->keterangan }}</td>
                            <td>{{ number_format($item->subtotal, 2, ',', '.') }}</td>
                            <td>{{ $item->status_id === 1 ? 'No' : 'Yes'}}</td>
                            <td> <a href="{{ route('requestuut.createbookinginspection', $item->id) }}"
                                class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_add_inspeksi">
                                <i class="fa fa-edit faa-flash"></i> Item Uji</a> 
                            </td>
                        @endforeach
                    </tbody>
                </table>

                @foreach($request->items as $item)
                <div class="panel panel-filled panel-c-danger">
                    <div class="panel-body">
                        <form id="form_item_{{ $item->id }}">
                            {!! Form::hidden('id', $item->id) !!}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis Alat</label>
                                        {!! Form::text('tool_type', $item->uuts ? $item->uuts->stdtype->uut_type :'', ['class' => 'form-control','id' => 'serial_no', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Alat</label>
                                        {!! Form::text('tool_name', $item->uuts ? $item->uuts->tool_name : ''.'/'.($item->uuts ? $item->uuts->tool_name : ''), ['class' => 'form-control','id' => 'tool_name', 'readonly']) !!} 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label name="laboratory" >Lab</label>
                                        {!! Form::text('laboratory', $item->uuts->stdtype->lab->nama_lab, ['class' => 'form-control','id' => 'laboratory', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label name="sla_overide" >SLA</label>
                                        {!! Form::text('sla_overide', $item->sla_overide ? $item->sla_overide : $item->uuts->stdtype->lab->sla_day, ['class' => 'form-control','id' => 'sla_overide']) !!} 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Merek</label>
                                        {!! Form::text('tool_brand', $item->uuts ? $item->uuts->tool_brand : '', ['class' => 'form-control','id' => 'tool_brand', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Model/Tipe</label>
                                        {!! Form::text('tool_model', $item->uuts ? $item->uuts->tool_model : '', ['class' => 'form-control','id' => 'tool_model', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Nomor Seri/Identitas</label>
                                        {!! Form::text('serial_no', $item->uuts ? $item->uuts->serial_no : '', ['class' => 'form-control','id' => 'serial_no', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Media Uji/Komoditas</label>
                                        {!! Form::text('tool_media', $item->uuts ? $item->uuts->tool_media : '', ['class' => 'form-control','id' => 'tool_media', 'readonly']) !!} 
                                    </div>
                                </div>
                                
                                <!-- <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tipe</label>
                                        {!! Form::text('tool_type', $item->uuts->tool_type, ['class' => 'form-control','id' => 'tool_type', 'readonly']) !!} 
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Standar</label>
                                        {!! Form::select('standard_id', $standards,$item->standard_id != null ? $item->standard_id : 0, ['class' => 'form-control','id' => 'standard_id', 'placeholder' => 'Jenis Standard']) !!} 
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tool Code</label>
                                        {!! Form::text('tool_code', '', ['class' => 'form-control','id' => 'tool_code', 'readonly']) !!} 
                                    </div>
                                </div> -->
                            
                            </div>
                            <div class="row">
                                <!-- <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Buatan</label>
                                        {!! Form::text('tool_made_in', $item->uuts->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in', 'readonly']) !!} 
                                    </div>
                                </div> -->
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Kapasitas</label>
                                        {!! Form::text('tool_capacity', $item->uuts->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Satuan Kapasitas</label>
                                        {!! Form::text('tool_capacity_unit', $item->uuts->tool_capacity_unit, ['class' => 'form-control','id' => 'tool_capacity_unit', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Daya Baca</label>
                                        {!! Form::text('tool_dayabaca', $item->uuts->tool_dayabaca, ['class' => 'form-control','id' => 'tool_dayabaca', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Satuan Daya Baca</label>
                                        {!! Form::text('tool_dayabaca_unit', $item->uuts->tool_dayabaca_unit, ['class' => 'form-control','id' => 'tool_dayabaca_unit', 'readonly']) !!} 
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Kelas</label>
                                        {!! Form::text('class', $item->uuts->class, ['class' => 'form-control','id' => 'class', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Jumlah</label>
                                        {!! Form::text('jumlah', $item->uuts->jumlah, ['class' => 'form-control','id' => 'jumlah', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Buatan</label>
                                        {!! Form::text('tool_made_in', $item->uuts->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in', 'readonly']) !!} 
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Pabrik Tempat Produksi</label>
                                        {!! Form::text('tool_factory', $item->uuts->tool_factory, ['class' => 'form-control','id' => 'tool_factory', 'readonly']) !!} 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Alamat Pabrik Tempat Produksi</label>
                                        {!! Form::text('tool_factory_address', $item->uuts->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address', 'readonly']) !!} 
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Jumlah Pengujian</label>
                                        {!! Form::text('quantity', $item->quantity, ['class' => 'form-control','id' => 'quantity', 'readonly']) !!} 
                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="row">
                                <button class="btn btn-warning btn-sm faa-parent animated-hover pull-right btn-item-edit" 
                                    data-id="{{ $item->id }}" id="edit_item_btn">
                                    <i class="fa fa-edit faa-flash"></i> Simpan Item
                                </button>
                            </div>
                            -->
                        </form>

                        @if(count($perlengkapans) > 0)
                        <h5>Perlengkapan</h5>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="perlengkapan" class="table table-responsive-sm input-table">
                                    <thead>
                                        <tr>
                                            <th>Merek</th>
                                            <th>Model/Tipe</th>
                                            <th>Nomor Seri</th>
                                            <th>Media Uji/Komoditas</th>
                                            <th>Buatan</th>
                                            <th>Kapasitas Maksimal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($item->perlengkapans as $perlengkapan)
                                        <tr>
                                            <td>{{ $perlengkapan->uttp->tool_brand }}</td>
                                            <td>{{ $perlengkapan->uttp->tool_model }}</td>
                                            <td>{{ $perlengkapan->uttp->serial_no }}</td>
                                            <td>{{ $perlengkapan->uttp->tool_media }}</td>
                                            <td>{{ $perlengkapan->uttp->tool_made_in }}</td>
                                            <td>{{ $perlengkapan->uttp->tool_capacity . ' ' . $perlengkapan->uttp->tool_capacity_unit }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif

                        <h5>Pengujian:</h5>
                        <div class="row">
                            <button class="btn btn-default btn-sm faa-parent animated-hover pull-right btn-history" 
                                data-id="{{ $item->uut_id }}" id="btn_hist_item">
                                <i class="fa fa-history faa-flash"></i> Riwayat Pengujian
                            </button>
                            
                            <button class="btn btn-warning btn-sm faa-parent animated-hover pull-right btn-save-edit" 
                                data-id="{{ $item->id }}" id="btn_edit_item">
                                <i class="fa fa-save faa-flash"></i> Simpan Alat
                            </button>
                           
                            <a href="{{ route('requestuut.createbookinginspection', $item->id) }}"
                                class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_add_inspeksi">
                                <i class="fa fa-plus faa-flash"></i> Tambah Uji
                            </a>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="standard_item_inspeksi" class="table table-responsive-sm input-table">
                                    <thead>
                                        <tr>
                                            <th>Jenis Pengujian</th>
                                            <th>Jumlah</th>
                                            <th>Satuan</th>
                                            <th>Harga Satuan</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($item->inspections as $inspection)
                                        <tr>
                                            <td>{{$inspection->inspectionPrice->inspection_type}}</td>
                                            <td>{{ $inspection->quantity }}</td>
                                            <td>{{ $inspection->inspectionPrice->unit }}</td>
                                            <td>{{ number_format($inspection->price, 2, ',', '.') }}</td>
                                            <td>{{ number_format($inspection->quantity * $inspection->price, 2, ',', '.') }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td>Subtotal: </td>
                                            <td><input readonly type="text" name="subtotal[]" id="subtotal" class="form-control" value="{{ number_format($item->subtotal, 2, ',', '.') }}"/></td>
                                        </tr>                            
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>    
                @endforeach

            </div>
        </div>

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button> 
        <button type="button" class="btn btn-w-md btn-accent" id="btn_submit">Simpan dan Konfirmasi</button> 
        
        </div>
    </div>
</div>
</div>

<div class="modal fade" tabindex="-1" id="historyModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="m-t-none">Riwayat Pengujian/Pemeriksaan</h5>
                <br/>
                <table id="inspection_history" class="table table-responsive-sm input-table">
                    <thead>
                        <tr>
                            <th>Jenis Pengujian</th>
                            <th>No SKHP</th>
                            <th>Tanggal, Mulai</th>
                            <th>Tanggal, Selesai</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    var index = 1;


    var tc = $('#standard_id').val() !=null ? $('#standard_id').val()  :0;
    console.log(tc)

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        if(tc !='' > 0){
            var url = "{{ route('requestuut.getStandard', ':id') }}";

            var id =  $('#standard_id').val();
            var token = $(this).data('token');

            url = url.replace(':id',id)
            
            console.log('ada :'+url)
            $.ajax({
                type: 'GET',
                url: url,
                data:{
                    _token:'{{ csrf_token() }}',
                    id: tc,
                },
                success: function(d){
                    console.log(d)
                }
            });
        }
        $('#booking_id, #lokasi_pengujian,#standard_id').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });
        // $('#uut_owner_id').select2({
        //     // placeholder: "- Pilih UML -",
        //     allowClear: true,
        //     ajax: {
        //         url: '{{ route('requestuut.getowners') }}',
        //         data: function (params) {
        //             var query = {
        //                 q: params.term,
        //             }
                    
        //             return query;
        //         },
        //         processResults: function (data) {
        //             console.log(data);
        //             return {
        //                 results: $.map(data, function(obj, index) {
        //                     return { id: obj.id, text: obj.nama };
        //                 })
        //             };
        //         },
        //     }
        // }).on("select2:select", function (e) { 
        //     var route = "{{ route('requestuut.getowner', ':id') }}";
        //     route = route.replace(':id', e.params.data.id);
        //     $.get(route,function(res)
        //     {
        //         $('#addr_sertifikat').val(res.alamat);
        //     });
        // });

        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });

        $('#btn_simpan').click(function(e){
            e.preventDefault();

            $('#btn_submit').attr('disabled', true);
            $('#btn_simpan').attr('disabled', true);

            toastr["warning"]("Sedang menyimpan. Harap tunggu.");

            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuut.simpaneditbooking', $request->id) }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuut') }}';
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('#btn_submit').click(function(e){
            e.preventDefault();
            $('#btn_submit').attr('disabled', true);
            $('#btn_simpan').attr('disabled', true);

            toastr["warning"]("Sedang menyimpan. Harap tunggu.");

            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuut.submitbooking', $request->id) }}',form_data,function(response)
            {
                $(this).attr('disabled', false);
                if(response.status===true)
                {
                    window.location = '{{ route('requestuut') }}';
                }
                else
                {
                   
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('.btn-save-edit').click(function(e){
            e.preventDefault();
            var id = $(this).data().id;
            
            var form_data = $("#form_item_" + id).serialize();

            form_data += '&_token={{ csrf_token() }}';

            var postroute = "{{ route('requestuut.edititem', ':id') }}";
            postroute = postroute.replace(':id', id);
            
            $.post(postroute,form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = "{{ route('requestuut.editbooking', $request->id) }}";
                    toastr['success']("Terimkasih","Data berhasil disimpan");
                }
                // else
                // {
                //     //var msg = show_notice(response.messages);
                //     toastr["error"]("Mohon Periksan kembali","Form Invalid");
                // }
            });
        });

        $('.btn-item-edit').click(function(e){
            e.preventDefault();
            var id = $(this).data().id;
            
            var form_data = $("#form_item_" + id).serialize();
            form_data += 'method=PATCH';

            form_data += '&_token={{ csrf_token() }}';

            var postroute = "{{ route('requestuut.updateStandard', ':id') }}";
            postroute = postroute.replace(':id', id);
            $.post(postroute,form_data,function(response)
            {
                // if(response.status === true){
                //     if(response.tool_code !=''){
                //         $('#tool_code').val(response.tool_code)
                //     }
                // }
            });
        });

        $('.btn-history').click(function(e){
            e.preventDefault();
            var id = $(this).data().id;
            
            var route = "{{ route('requestuut.gethistory', ':id') }}";
            route = route.replace(':id', id);
            //$.get(route,function(res)
            //{
                
            //});
            $("#inspection_history").DataTable().destroy();
            $("#inspection_history").DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: route,
                    type: 'GET'
                },
                "ajax": function(data, callback, settings) {
                    
                    $.ajax({
                        url: route,
                        type: "GET",
                        success: function(res) {
                            console.log(res); 
                            callback({
                                recordsTotal: res.length,
                                recordsFiltered: res.length,
                                data: res
                            });
                        }
                    });

                },
                "columns": [
                    { data: 'service_request_item_inspection.inspection_price.inspection_type', },
                    { data: 'no_sertifikat', },
                    { data: 'staff_entry_datein', },
                    { data: 'staff_entry_dateout' },
                ],
            });

            $('#historyModal').modal('show');

        });

        $('#standard_id').on('change', function(){
            var id = $('#id').val;
            // console.log(id);
            var route = "{{ route('requestuut.updateStandard', ':id') }}";

            var standard_id =  $('#standard_id').val();
            var token = $(this).data('token');

            route = route.replace(':id', id);
            $.ajax({
                url: route,
                type: "PATCH",
                cache: false,
                data:{
                    _token:'{{ csrf_token() }}',
                    type: 3,
                    standard_id: $('#standard_id').val(),
                },
                success: function(dataResult,response){
                    dataResult = JSON.parse(dataResult);
                    var msg = show_notice(response.messages);
                    toastr["error"](msg);
                    
                }
            });
        });

        $('#t-datas').DataTable({
            "scrollX": true
        });

        // $('#standard_id').on('change',function(){
        //     var standard_id =  $('#standard_id').val();
        //     var token = $(this).data('token');
        //     var base_url =  "{{ route('requestuut.updateStandard', ':id') }}";
        //     base_url = postroute.replace(':id', id);
        //     $.ajax({
        //         url:base_url,
        //         type: 'PATCH',
        //         data: { _token :token,city_id:city_id },
        //         success:function(msg){
        //         alert("success");
        //         }
        // });});
    });
</script>
@endsection