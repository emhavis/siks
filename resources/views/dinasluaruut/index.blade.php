@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

<style type"text/css"> 
    .badge-notif-tab {
            position:relative;
    }
    .badge-notif-tab[data-badge]:after {
            content:attr(data-badge);
            position:absolute;
            top:-10px;
            right:-10px;
            font-size:.7em;
            background:#f3a709;
            color:white;
            width:18px;
            height:18px;
            text-align:center;
            line-height:18px;
            border-radius: 50%;
    }
    </style>
@endsection

@section('content')
<div class="row">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <a href="{{ route('requestuut.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Pendaftaran</a> -->
    <!-- <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a> -->

    <ul class="nav nav-tabs" role="tablist" id="tabs">

        <li role="presentation" class="active">
            <a href="#booking_luar" aria-controls="booking_luar" role="tab" data-toggle="tab"
            class="badge-notif-tab" 
            <?php if ((count($bookings_luar)) > 0 ){ ?> 
                data-badge="{{count($bookings_luar)}}" 
            <?php }else{ } ?>
        >Booking Dinas Luar</a></li>
        
        <li role="presentation">
            <a href="#frontdesk_pendaftaran" aria-controls="frontdesk_pendaftaran" role="tab" data-toggle="tab"
            class="badge-notif-tab" 
            <?php if ((count($rows_pendaftaran_luar)) > 0 ){ ?> 
                data-badge="{{count($rows_pendaftaran_luar)}}" 
            <?php }else{ } ?>        
        >Pendaftaran Dinas Luar</a>
        </li>
    </ul>
    <div class="tab-content" id="nav-tabContent">


        <div role="tabpanel" class="tab-pane active" id="booking_luar">
        <br/>            
            <div class="panel panel-filled table-area">
                <div class="panel-heading">                  

                    <table id="table_data" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Booking</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Biaya</th>
                                <th>Nama Pemohon</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bookings_luar as $row)
                            <tr>
                                <td>{{ $row->booking_no }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->est_total_price,0,',','.') }}</td>
                                <td>{{ $row->Pic ? $row->Pic->full_name : '' }}</td>
                                <td>
                                    <form class="form_create_request">
                                    {!! Form::hidden('booking_id', $row->id) !!}
                                    {!! Form::button('Konfirmasi Booking', ['class' => 'btn btn-w-md btn-accent btn-simpan', 'name' => 'submitbutton', 'value' => 'save']) !!}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="frontdesk_pendaftaran">
            <br/>
            <a href="{{ route('requestuut.createbooking') }}" class="btn btn-w-md btn-primary" id="btn_create">Konfirmasi Booking</a>

            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    

                    <table id="table_data_pendaftaran" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Biaya</th>
                                <th>Nama Pemohon</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_pendaftaran_luar as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>
                                @if($row->payment_code==null)
                                xx-xxxx-xx-xxx
                                @endif
                                @if($row->payment_code!=null)
                                {{ $row->no_order }}
                                @endif
                                </td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->total_price,0,',','.') }}</td>
                                <td>{{ $row->requestor ? $row->requestor->full_name : '' }}</td>
                                <td>{{ $row->status->status }}</td>
                                <td>
                                    <a href="{{ route('requestuut.editbooking', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>

                                    <!-- <a href="{{ route('requestuut.destroy', $row->id) }}" class="btn btn-warning btn-sm">Hapus</a> -->
                                    <button class="btn btn-warning btn-sm hapus" 
                                                data-id="{{ $row->id }}" data-noreg={{$row->no_register}}> Hapus </button>
                                        
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>   
</div>
<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Serah Terima Alat ke Pemohon</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <div class="form-group">
                                <input class ="form-control" type="hidden" name="id" id="id"/>
                                <input class="form-control" type="hidden" name="requestid" id="requestid"/>
                                <input class= "form-control" type="hidden" name="labid" id="labid"/>
                                <input type="hidden" name="instalasiid" id="instalasiid"/>
                            </div>
                            <div class="form-group">
                                <label>No Pendaftaran</label>
                                <input type="text" name="noregistrasi" id="noregistrasi" class="form-control" readonly required />
                            </div>

                            <div class="form-group">
                                <label>Alat</label>
                                <input type="text" name="alat" id="alat" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemilik</label>
                                <input type="text" name="pemilik" id="pemilik" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemohon</label>
                                <input type="text" name="pemohon" id="pemohon" class="form-control" readonly required />
                            </div>

                            <div class="form-group">
                                <label>Nama Penerima Alat</label>
                                <input type="text" name="warehouse_out_nama" id="warehouse_out_nama" class="form-control"  required />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">SERAH TERIMA</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Ingin menghapus data ini ?</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal-hapus">
                            <div class="form-group">
                                <input class ="form-control" type="hidden" name="id" id="id"/>
                                <input class="form-control" type="hidden" name="requestid" id="requestid"/>
                                <input class= "form-control" type="hidden" name="labid" id="labid"/>
                                <input type="hidden" name="instalasiid" id="instalasiid"/>
                            </div>
                            <div class="form-group">
                                <label>No Pendaftaran</label>
                                <input type="text" name="no_registrasi" id="no_registrasi" class="form-control" readonly required />
                            </div>

                            
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="hapus" class="btn btn-accent">Hapus</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_pendaftaran tbody").on("click","button.hapus",function(e){
            <?php $id; ?>
            console.log('ini di klick');
            e.preventDefault();
            var iddelete = $(this).data().id;
            var noreg = $(this).data().noreg;
            $('#noregistrasi').val(noreg);
            
            $("#deletemodal").modal().on('shown.bs.modal', function ()
            {
                $('#no_registrasi').val(noreg);

                var route = "{{ route('requestuut.destroy', ':id') }}";
                route = route.replace(':id', iddelete);

                $('#hapus').click(function(){
                    $.get(route,function(response)
                    {
                        
                        console.log(response.status)
                        console.log('{{ route('requestuut.destroy','id') }}');
                        if(response.status == true){
                            $("#deletemodal").modal().hide();
                            toastr["success"](response.messages,"Form Status : Ok");
                            location.reload();
                        }else{
                            toastr["error"](response.messages,"Form Invalid Status : "+response.status);
                        }
                    });
                });
                
            });
            
        });
        // end of code
        $('.btn-simpan').click(function(e){
            
            e.preventDefault();

             $(this).attr('disabled', true);
            toastr["warning"]("Konfirmasi booking sedang diproses","Form Valid");
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';
            
            $.post('{{ route('requestuut.simpanbooking') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuut') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('.btn-cetak-tag').click(function(e){
            e.preventDefault();

            var url = '{{ route("requestuut.tag", ":id") }}';
            url = url.replace(':id', $(this).data("id"));

            window.open(url, '_blank');
            window.focus();
            location.reload();
        });

        $('#frontdesk_kirim #table_data_kirim tbody').on("click",'.btn-kirim-alat', function(e){
            e.preventDefault();

            var url = '{{ route("requestuut.instalasi") }}';
            //url = url.replace(':id', $(this).data("id"));

            location.reload();

            var form_data = 'id=' +$(this).data("id") + '&_token={{ csrf_token() }}';

            $.post(url,form_data,function(response)
            {
                if(response.status===true)
                {
                    //location.reload();
                    //window.location = '{{ route('requestuut') }}'
                    toastr["success"]("Berhasil dikirim. Silakan refresh jika diperlukan.","Form Valid");
                }
                else
                {
                    var msg = show_notice(response.messages);
                    //window.location = '{{ route('requestuut') }}'
                    toastr["error"]("Mohon Periksa kembali","Form Invalid" + response.messages);;
                }
            });
        });

        $("#no_order").change(function(e) {
            
            e.preventDefault();
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuut.kirimqr') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    //location.reload();
                    toastr["success"]("Berhasil dikirim","Form Valid");
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('#refresh_btn').click(function(e) {
            e.preventDefault();
            location.reload();
            $(this).tab('show');
        });
        // TABS
        $('#tabs a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });

        var hash = window.location.hash;
        $('#tabs a[href="' + hash + '"]').tab('show');
        // END TABS

        $("#table_data,#table_data_pendaftaran,#table_data_penagihan,#table_data_validasi,#table_data_kirim,#table_data_process,#table_data_done").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });


        // $('.btn-simpan').click(function(e)
        // {
        //     e.preventDefault();

        //      $(this).attr('disabled', true);
        //     toastr["warning"]("Konfirmasi booking sedang diproses","Form Valid");
            
        //     var form = $(this).parents('form:first');
        //     var form_data = form.serialize();
        //     form_data += '&_token={{ csrf_token() }}';
            
        //     $.post('{{ route('requestuut.simpanbooking') }}',form_data,function(response)
        //     {
        //         if(response.status===true)
        //         {
        //             window.location = '{{ route('requestuut') }}' + '/editbooking/' + response.id;
        //         }
        //         else
        //         {
        //             var msg = show_notice(response.messages);
        //             toastr["error"]("Mohon Periksan kembali","Form Invalid");
        //         }
        //     });
        // });

        $("button.btn-mdl").click(function(e){
            e.preventDefault();
            console.log(e.preventDefault());
            var id = $(this).data().id;
            var requestid = $(this).data().requestid;
            var labid = $(this).data().labid;
            var instalasiid = $(this).data().instalasiid;
            var alat = $(this).data().alat;
            var pemilik = $(this).data().pemilik;
            var pemohon = $(this).data().pemohon;
            var reg_no = $(this).data().noregistrasi;

            $("#id").val(id);
            $("#requestid").val(requestid);
            $("#labid").val(labid);
            $("#instalasiid").val(instalasiid);
            $("#alat").val(alat);
            $("#pemilik").val(pemilik);
            $("#pemohon").val(pemohon);
            $("#noregistrasi").val(reg_no)

            var route = "{{ route('laboratory.getbyid', ':id') }}";
            route = route.replace(':id', labid);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#lab_nama").val(res.lab.nama_lab);
                
            });
        },"json");
    });

    var form =  $("#form_modal");
    form.validate({
        rules: {
            warehouse_out_nama: {
                required: true,
            },
        },
        messages: {
            warehouse_out_nama: 'Nama penerima wajib diisi',
        },
        errorClass: "help-block error",
        highlight: function(e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function(e) {
            $(e).closest(".form-group").removeClass("has-error")
        },
    });

    $("#simpan").click(function(e) {
        e.preventDefault();

        var id = $('#id').val();
        var requestid = $('#requestid').val();
        
        //var route = "{{ route('receiveuttp.proses', ':id') }}";
        //route = route.replace(':id', id);
        //window.location = route;

        // var form = $('#form_modal');
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        $.post('{{ route('warehouseuut.warehouse') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                // location.reload();
            }
            else
            {
                // var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksa kembali","Form Invalid");
            }
        });
        
        $("#prosesmodal").modal('hide');
    });

    /**
     * Perubahan ke serverside masingg - masing tab
     */
    
    
    
    // end table

    });
</script>
@endsection