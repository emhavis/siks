<html>
<head>
    <title>CETAK TAG</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table th{text-align: center;}
        table.f9 td,tr.f9 td, .f9{font-size: 8pt; padding: 0px;}
        table.f10 td,th{font-size: 10pt;}
        table.f11 td{font-size: 11pt;}
        table.f12 td{font-size: 12pt;}
        p.small{
            font-size: 7pt;
        }
        body { margin: 0px;  }
        @page { margin: 0px; }

    </style>
</head>
<body style="text-align:center;">
    <table style="width: 70mm; padding: 12 0px 0px 0px; border: 0px solid black;">

        <tr style="text-center">
            <td style="vertical-align:middle;">
                <table style="padding: 2px;">
                    <tr>
                        <td><img src='data:image/png;base64, {!! 
                                base64_encode(QrCode::format("png")
                                    ->size(70)
                                    ->errorCorrection("H")
                                    ->generate($row->ServiceRequestItem->no_order)) !!} '></td>
                    </tr>
                </table>
            </td>
            <td styel="horizontal-align:middle;">
                <table style="padding: 0px; font-size: 8pt;" >
                    <tr>
                        <td style="vertical-align:top; width:4mm;">NO</td>
                        <td style="vertical-align:top; max-width:3mm;">:</td>
                        <td style="vertical-align:top;"><strong>{{ $row->ServiceRequestItem->no_order }}</strong></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">TGL</td>
                        <td style="vertical-align:top;">:</td>
                        <td style="vertical-align:top;">{{ date("d M Y", strtotime($row->ServiceRequestItem->order_at )) }} </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">ID</td>
                        <td style="vertical-align:top;">:</td>
                        <td style="vertical-align:top; max-width:30mm">
                            {{ $row->ServiceRequestItem->uuts->tool_brand }}/{{ $row->ServiceRequestItem->uuts->tool_model }} ({{ $row->ServiceRequestItem->uuts->serial_no }}) /{{ $row->tool_capacity }} {{ $row->tool_capacity_unit }}
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">LAB</td>
                        <td style="vertical-align:top;">:</td>
                        <td style="vertical-align:top;">{{ $row->ServiceRequestItem->uuts->stdType->lab->nama_lab }} </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>