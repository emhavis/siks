@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        <form id="form_create_request">
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pendaftaran</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="for_sertifikat">Peruntukan Sertifikat</label>
                            {!! Form::text('for_sertifikat', $request->for_sertifikat, ['class' => 'form-control','id' => 'for_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="label_sertifikat">Label Sertifikat</label>
                            {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat','readonly' ]) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Sertifikat</label>
                            {!! Form::text('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', ]) !!}
                        </div>
                    </div>  
                </div>
            </div>
        </div>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pendaftar</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Nama Pendaftar</label>
                            {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Jenis Tanda Pengenal</label>
                            {!! Form::text('id_type_id', $requestor->kantor == "Perusahaan" ? "NIB" : "NPWP", ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                            {!! Form::text('pic_id_no', $requestor->kantor == "Perusahaan" ? $requestor->nib : $requestor->npwp, ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_phone_no">Nomor Telepon</label>
                            {!! Form::text('pic_phone_no', $requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_email">Alamat Email</label>
                            {!! Form::text('pic_email', $requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="received_date">Tanggal Masuk Alat</label>
                            {!! Form::text('received_date', date("d-m-Y", strtotime(isset($request->received_date) ? $request->received_date : date("Y-m-d"))), ['class' => 'date form-control','id' => 'receipt_date', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'required']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="estimate_date">Tanggal Perkiraan Selesai</label>
                            {!! Form::text('estimated_date', date("d-m-Y", strtotime(isset($request->estimated_date) ? $request->estimated_date : date("Y-m-d"))), ['class' => 'date form-control','id' => 'estimated_date', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Item Pengujian</h4>
            </div>

            <div class="panel-body">

                @foreach($request->items as $item)
                <div class="panel panel-filled panel-c-danger">
                    <div class="panel-body">
                        <form id="form_item_{{ $item->id }}">
                        {!! Form::hidden('id', $item->id) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jenis</label>
                                    {!! Form::text('serial_no', $item->uttp->type->uttp_type, ['class' => 'form-control','id' => 'serial_no', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nomor Seri</label>
                                    {!! Form::text('serial_no', $item->uttp->serial_no, ['class' => 'form-control','id' => 'serial_no']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Merek</label>
                                    {!! Form::text('tool_brand', $item->uttp->tool_brand, ['class' => 'form-control','id' => 'tool_brand']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Model</label>
                                    {!! Form::text('tool_model', $item->uttp->tool_model, ['class' => 'form-control','id' => 'tool_model']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Tipe</label>
                                    {!! Form::text('tool_type', $item->uttp->tool_type, ['class' => 'form-control','id' => 'tool_type']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Buatan</label>
                                    {!! Form::text('tool_made_in', $item->uttp->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Kapasitas</label>
                                    {!! Form::text('tool_capacity', $item->uttp->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pabrikan</label>
                                    {!! Form::text('tool_factory', $item->uttp->tool_factory, ['class' => 'form-control','id' => 'tool_factory']) !!} 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Alamat Pabrikan</label>
                                    {!! Form::text('tool_factory_address', $item->uttp->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Jumlah Pengujian</label>
                                    {!! Form::text('quantity', $item->quantity, ['class' => 'form-control','id' => 'quantity', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        </form>

                        <div class="row">
                            <button class="btn btn-warning btn-sm faa-parent animated-hover pull-right btn-save-edit" 
                                data-id="{{ $item->id }}" id="btn_edit_item">
                                <i class="fa fa-save faa-flash"></i> Simpan Alat
                            </button>
                            <a href="{{ route('requestuttp.createbookinginspection', $item->id) }}"
                                class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_add_inspeksi">
                                <i class="fa fa-plus faa-flash"></i> Tambah Uji</a>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <table id="standard_item_inspeksi" class="table table-responsive-sm input-table">
                                    <thead>
                                        <tr>
                                            <th>Jenis Pengujian</th>
                                            <th>Jumlah</th>
                                            <th>Satuan</th>
                                            <th>Harga Satuan</th>
                                            <th>Subtotal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($item->inspections as $inspection)
                                        <tr>
                                            <td>{{ $inspection->inspectionPrice->inspection_type }}</td>
                                            <td>{{ $inspection->quantity }}</td>
                                            <td>{{ $inspection->inspectionPrice->unit }}</td>
                                            <td>{{ number_format($inspection->price, 2, ',', '.') }}</td>
                                            <td>{{ number_format($inspection->quantity * $inspection->price, 2, ',', '.') }}</td>
                                            <td>
                                                {!! Form::open(['url' => route('requestuttp.hapusbookinginspection', $inspection->id)])  !!} 
                                                <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i> Hapus</button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td>Subtotal: </td>
                                            <td><input readonly type="text" name="subtotal[]" id="subtotal" class="form-control" value="{{ number_format($item->subtotal, 2, ',', '.') }}"/></td>
                                            <td></td>
                                        </tr>                            
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>    
                @endforeach

            </div>
        </div>

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button> 
        <button type="button" class="btn btn-w-md btn-accent" id="btn_submit">Simpan dan Konfirmasi</button> 
        
        </div>
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        $('#booking_id').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });

        $('#btn_simpan').click(function(e){
            e.preventDefault();
            
            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuttp.simpaneditbooking', $request->id) }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}';
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('#btn_submit').click(function(e){
            e.preventDefault();
            
            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuttp.submitbooking', $request->id) }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}';
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('.btn-save-edit').click(function(e){
            e.preventDefault();
            var id = $(this).data().id;
            
            var form_data = $("#form_item_" + id).serialize();

            form_data += '&_token={{ csrf_token() }}';

            var postroute = "{{ route('requestuttp.edititem', ':id') }}";
            postroute = postroute.replace(':id', id);
            
            $.post(postroute,form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = "{{ route('requestuttp.editbooking', $request->id) }}";
                }
                else
                {
                    //var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });
    });

</script>
@endsection