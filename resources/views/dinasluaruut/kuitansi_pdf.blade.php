<html>
<head>
    <title>KUITANSI</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table th{text-align: center;}
        table th,td{padding: 5px 10px;}
        table.f9 td,tr.f9 td, .f9{font-size: 9pt;}
        table.f10 td,th{font-size: 10pt;}
        table.f11 td{font-size: 11pt;}
        table.f12 td{font-size: 12pt;}
        p.small{
            font-size: 7pt;
        }
    </style>
</head>
<body>
    <center>
        <p class="f9">Sistem Informasi Pelayanan UPTP IV</p>
    </center>

    @if ($row->total_price > 0 && $row->payment_date != null)
    <div style="page-break-after: always;">
        <table border="0">
            <tr>
                <td style="width: 20mm;">
                    <img src="{{ asset('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
                </td>
                <td>
                    <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN<br />TERTIB NIAGA DIREKTORAT METROLOGI</p>
                    <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
                </td>
            </tr>
        </table>
        <center>
            <h4><b>KUITANSI</b></h4>
        </center>
        <table style="width: 100%;" class="f10">
            <tr>
                <td colspan="3" style="text-align: right;"><b>NO. PENDAFTARAN : {{$row->no_register}}</b></td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Nomor</b></td>
                <td style="width: 80mm;">: {{ $row->no_register }}</td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Sudah Terima Dari</b></td>
                <td style="width: 80mm;">: {{ $row->requestor->full_name }}</td>
            </tr>
            <tr>
                <td><b>Banyaknya Uang</b></td>
                <td>: {{ ucwords($terbilang) }} Rupiah</td>
            </tr>
            <tr>
                <td><b>Untuk Pembayaran</b></td>
                <td>: Biaya Pengujian dalam rangka {{ $row->jenis_layanan }} dengan Nomor Order {{ $row->no_order }} berdasarkan PMK RI Nomor 140 Tahun 2023</td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;" class="f10" border="0">
            <tr>
                <td width="75%">&nbsp;</td>
                <td style="width: 80mm;">BANDUNG, {{ $row->payment_date }}
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Jumlah  Rp. {{number_format($row->total_price,2,',','.')}}</b></td>
                <td>KASIR</td>
            </tr>
            
        </table>
    </div>
    @endif

    @if ($row->lokasi_pengujian == 'luar')
    @foreach($docs as $doc)
    @if($doc->payment_date != null)
    <div style="page-break-after: always;">
        <table border="0">
            <tr>
                <td style="width: 20mm;">
                    <img src="{{ asset('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
                </td>
                <td>
                    <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN<br />TERTIB NIAGA DIREKTORAT METROLOGI</p>
                    <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
                </td>
            </tr>
        </table>
        <center>
            <h4><b>KUITANSI</b></h4>
        </center>
        <table style="width: 100%;" class="f10">
            <tr>
                <td colspan="3" style="text-align: right;"><b>NO. PENDAFTARAN : {{$row->no_register}}</b></td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Nomor</b></td>
                <td style="width: 80mm;">: {{ $row->no_register }}</td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Sudah Terima Dari</b></td>
                <td style="width: 80mm;">: {{ $row->requestor->full_name }}</td>
            </tr>
            <tr>
                <td><b>Banyaknya Uang</b></td>
                <td>: {{ ucwords($terbilangSPUH[$doc->id]) }} Rupiah</td>
            </tr>
            <tr>
                <td><b>Untuk Pembayaran</b></td>
                <td>: Uang Harian dalam rangka {{ $row->jenis_layanan }} di {{ $row->inspectionProv->nama }} berdasarkan PMK Nomor 119/PMK.02/2020 tentang Standar Biaya Masukan Tahun Anggaran 2021</td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;" class="f10" border="0">
            <tr>
                <td width="75%">&nbsp;</td>
                <td style="width: 80mm;">BANDUNG, {{ $doc->payment_date }}
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Jumlah  Rp. {{number_format($doc->invoiced_price,2,',','.')}}</b></td>
                <td>KASIR</td>
            </tr>
            
        </table>
    </div>
    @endif
    @if($doc->act_price != null && $doc->act_price > $doc->price && $doc->act_payment_date != null)
    <div style="page-break-after: always;">
        <table border="0">
            <tr>
                <td style="width: 20mm;">
                    <img src="{{ asset('assets/images/logo/logo_kemendag.png') }}" style="width: 20mm;">
                </td>
                <td>
                    <p style="font-size: 13pt;font-weight: bold;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN<br />TERTIB NIAGA DIREKTORAT METROLOGI</p>
                    <p class="f9">Jl. Pasteur No. 27 Bandung 40171 Telp. 022-4202773 Fax. 022-4207035</p>
                </td>
            </tr>
        </table>
        <center>
            <h4><b>KUITANSI</b></h4>
        </center>
        <table style="width: 100%;" class="f10">
            <tr>
                <td colspan="3" style="text-align: right;"><b>NO. PENDAFTARAN : {{$row->no_register}}</b></td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Nomor</b></td>
                <td style="width: 80mm;">: {{ $row->no_register }}</td>
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Sudah Terima Dari</b></td>
                <td style="width: 80mm;">: {{ $row->requestor->full_name }}</td>
            </tr>
            <tr>
                <td><b>Banyaknya Uang</b></td>
                <td>: {{ ucwords($terbilangActSPUH[$doc->id]) }} Rupiah</td>
            </tr>
            <tr>
                <td><b>Untuk Pembayaran</b></td>
                <td>: Uang Harian dalam rangka {{ $row->jenis_layanan }} di {{ $row->inspectionProv->nama }} berdasarkan PMK Nomor 119/PMK.02/2020 tentang Standar Biaya Masukan Tahun Anggaran 2021</td>
            </tr>
        </table>
        <br />
        <table style="width: 100%;" class="f10" border="0">
            <tr>
                <td width="75%">&nbsp;</td>
                <td style="width: 80mm;">BANDUNG, {{ $doc->act_payment_date }}
            </tr>
            <tr>
                <td style="width: 30mm;"><b>Jumlah  Rp. {{number_format($doc->act_price - $doc->price,2,',','.')}}</b></td>
                <td>KASIR</td>
            </tr>
            
        </table>
    </div>
    @endif
    @endforeach
    @endif

</body>
</html>