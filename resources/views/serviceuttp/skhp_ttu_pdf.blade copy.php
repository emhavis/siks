<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Keterangan Hasil Pengujian</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-draft-uttp.png") : public_path("assets/images/logo/letterhead-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
        .table td .eng {
            font-size:8pt;
            font-style:italic;
        }
    </style>
</head>

<body class="A4">

    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order }}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN HASIL PENGUJIAN</strong></div>
            <div class="subtitle"><i>Certificate of Testing Result</i></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="min-width: 40mm;">Jenis UTTP<div class="eng">Measuring Instrument</div></td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Merek<div class="eng">Trade Mark</div></td>
                    <td>:</td>
                    <td colspan="4">{{ 
                        $order->tool_brand != null ? 
                        $order->tool_brand :
                        $order->ServiceRequestItem->uttp->tool_brand 
                    }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Model/Tipe<div class="eng">Model/Type</div></td>
                    <td>:</td>
                    <td colspan="4">{{ 
                        $order->tool_model != null ? 
                        $order->tool_model :
                        $order->ServiceRequestItem->uttp->tool_model 
                    }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">No. Seri<div class="eng">Serial Number</div></td>
                    <td>:</td>
                    <td colspan="4">{{ 
                        $order->tool_serial_no != null ? 
                        $order->tool_serial_no :
                        $order->ServiceRequestItem->uttp->serial_no 
                    }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Maksimum<div class="eng">Maximum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">
                        {{ 
                            $order->tool_capacity != null ?
                            $order->tool_capacity :
                            $order->ServiceRequestItem->uttp->tool_capacity 
                        }}&nbsp;
                        {{ 
                            $order->tool_capacity_unit != null ?
                            $order->tool_capacity_unit :
                            $order->ServiceRequestItem->uttp->tool_capacity_unit 
                        }}
                    </td>
                </tr>
                @if($order->tool_capacity_min != null && $order->tool_capacity_min > 0)
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Minimum<div class="eng">Minimum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_capacity_min }}&nbsp;
                        {{ $order->tool_capacity_unit }}
                    </td>
                </tr>
                @elseif($order->ServiceRequestItem->uttp->tool_capacity_min > 0)
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Minimum<div class="eng">Minimum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uttp->tool_capacity_min 
                        }}&nbsp;
                        {{ $order->ServiceRequestItem->uttp->tool_capacity_unit 
                        }}
                    </td>
                </tr>
                @endif
                <tr>
                    <td>Pemilik/Pemakai<div class="eng">Owner/User</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                <tr>
                    <td>Lokasi Alat<div class="eng">Location</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequest->addr_sertifikat }}</td>
                </tr>

                <tr>
                    <td>Hasil<div class="eng">Result</div></td>
                    <td>:</td>
                    <td colspan="4">
                        @if($order->hasil_uji_memenuhi == true)
                        Disahkan berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal.
                        @else
                        Batal berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal. 
                        @endif  
                    </td>
                </tr>
            </tbody>
        <table>

        <br />
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td colspan="2">SKHP ini terdiri dari {{ count($order->ttuPerlengkapans) > 0 ? '3 (tiga)' : '2 (dua)' }} halaman
                    <div class="eng">This certificate consists of {{ count($order->ttuPerlengkapans) > 0 ? '3 (three)' : '2 (two)' }} pages</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">Catatan:<div class="eng">Notes:</div></td>
            </tr>
            <tr>
                <td style="padding-left: 10px; max-width: 3mm;">1.</td>
                <td>UTTP ini wajib ditera ulang paling lambat tanggal {{ format_long_date($order->sertifikat_expired_at) }}
                    <?php setlocale(LC_TIME, "en_EN"); ?>
                    <div class="eng">This measuring instrument mandatory to reverificate on {{ strftime('%e %B %Y', strtotime($order->sertifikat_expired_at)) }}</div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px; max-width: 3mm;">2.</td>
                <td>Apabila tanda tera rusak dan/atau kawat segel putus, UTTP ini wajib ditera ulang
                    <div class="eng">If the verification mark is damaged and/or the seal wire is broken, this measuring instrument must be re-verificated</div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px; max-width: 3mm;">3.</td>
                <td>Pemutusan tanda tera hanya dapat dilakukan dengan sepengetahuan Direktorat Metrologi
                    <div class="eng">Breaking of the verification mark can only be done with the agreement of the Directorate of Metrology</div>
                </td>
            </tr>
        </table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 40mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>a.n Direktur Metrologi
                        <br/>Kepala Balai Pengujian UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 30mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(123)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}
                        <br/>NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>

    @if($order->cancel_at == null)
    <div class="page-break"></div>
    <section class="sheet watermark">
        <div class="text-right" style="padding-top: 25.4mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>DATA PENGUJIAN</strong></div>
            <div class="subtitle"><i>Testing Data</i></div>
        </div>

        <br/>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                @if($order->TestBy2 != null)
                <td rowspan="2">Pegawai Berhak</td>
                <td rowspan="2">:</td>
                @else
                <td>Pegawai Berhak</td>
                <td>:</td>
                @endif
                <td>1. {{ $order->TestBy1 != null && $order->TestBy1->PetugasUttp != null ? $order->TestBy1->PetugasUttp->nama : '' }}</td>
                <td>NIP: {{ $order->TestBy1 != null && $order->TestBy1->PetugasUttp != null ? $order->TestBy1->PetugasUttp->nip : '' }}</td>
            </tr>
            @if($order->TestBy2 != null)
            <tr>
                <td>2. {{ $order->TestBy2 != null && $order->TestBy2->PetugasUttp != null ? $order->TestBy2->PetugasUttp->nama : '' }}</td>
                <td>NIP: {{ $order->TestBy2 != null && $order->TestBy2->PetugasUttp != null ? $order->TestBy2->PetugasUttp->nip : '' }}</td>
            </tr>
            @endif
            <tr>
                <td>Tanggal Pengujian</td>
                <td>:</td>
                <td colspan="4">
                @if($order->stat_service_order == 4)
                    {{ format_long_date($order->staff_entry_datein) . ' - ' . format_long_date($order->cancel_at) }}
                @else
                    {{ format_long_date($order->staff_entry_datein) . ' - ' . format_long_date($order->staff_entry_dateout) }}
                @endif
                </td>
            </tr>
            <tr>
                <td>Lokasi</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'Direktorat Metrologi' : 'Lokasi UTTP Terpasang'}}
                </td>
            </tr>
            @if($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge' || 
                $order->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice'
            )
            <tr>
                <td>Identitas Badan Hitung</td>
                <td>:</td>
                <td colspan="4">
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">Tipe</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $tipe_array = $order->ttuBadanHitungs->pluck('type')->toArray(); 
                    ?>
                    {{ implode("; ", $tipe_array) }}
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">No Seri</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $seri_array = $order->ttuBadanHitungs->pluck('serial_no')->toArray(); 
                    ?>
                    {{ implode("; ", $seri_array) }}
                </td>
            </tr>
            <tr>
                <td>Nomor Tangki</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->tank_no }}
                </td>
            </tr>
            <tr>
                <td>Nomor Tag</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->tag_no }}
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM')
            <tr>
                <td>Identitas Badan Hitung</td>
                <td>:</td>
                <td colspan="4">
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">Merek</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $merek_array = $order->ttuBadanHitungs->pluck('brand')->toArray(); 
                    ?>
                    {{ implode("; ", $merek_array) }}
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">Tipe</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $tipe_array = $order->ttuBadanHitungs->pluck('type')->toArray(); 
                    ?>
                    {{ implode("; ", $tipe_array) }}
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">No Seri</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $seri_array = $order->ttuBadanHitungs->pluck('serial_no')->toArray(); 
                    ?>
                    {{ implode("; ", $seri_array) }}
                </td>
            </tr>
            <tr>
                <td>Totalisator</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->totalisator }}
                </td>
            </tr>
            <tr>
                <td>K Faktor</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->kfactor }}
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air')
            <tr>
                <td>Totalisator</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->totalisator }}
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' || 
                $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG'
                )
            <tr>
                <td>Totalisator</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->totalisator }}
                </td>
            </tr>
            <tr>
                <td>K Faktor</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->kfactor }}
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBM')
            <tr>
                <td>Totalisator</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->totalisator }}
                </td>
            </tr>
            @endif
        </table>  

        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>HASIL PENGUJIAN</strong></div>
            <div class="subtitle"><i>Testing Result</i></div>
        </div>
        
        <br/>
        @if($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th rowspan="2">Input Level (mm)</th>
                <th colspan="2">Kesalahan Penunjukan</th>
                <th rowspan="2">Kesalahan Histerisis (mm)</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>Kesalahan Naik (mm)</th>
                <th>Kesalahan Turun (mm)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->input_level }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error_up }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error_down }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error_hysteresis }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Kecepatan Alir/<div class="eng">Flow Rate</div><br/>(L/menit)</th>
                <th>Kesalahan/<div class="eng">Error</div><br/>(%)<br/>BKD: &#177;0.5%</th>
                <th>Meter Faktor<br/>BKD: 1&#177;0.5%</th>
                <th>Kemampuan Ulang/<div class="eng">Repeatablity</div><br/>(%)<br/>BKD: 0.1%</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->flow_rate }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->meter_factor }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->repeatability_bkd }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Kecepatan Alir/<div class="eng">Flow Rate</div><br/>(m<sup>3</sup>/h)</th>
                <th>Kesalahan/<div class="eng">Error</div><br/>(%)</th>
                <th>Kemampuan Ulang/<div class="eng">Repeatiblity</div><br/>(%)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->flow_rate }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->repeatability }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Kecepatan Alir/<div class="eng">Flow Rate</div><br/>(m3/h)</th>
                <th>Kesalahan/<div class="eng">Error</div><br/>(%)<br/>BKD: &plusmn;0,5%</th>
                <th>Kemampuan Ulang/<div class="eng">Repeatiblity</div><br/>(%)<br/>BKD: &plusmn;0,1%</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->flow_rate }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->repeatability }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Kecepatan Alir (m3/h)</th>
                <th>Kesalahan</th>
                <th>Kemampuan Ulang</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->flow_rate }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->repeatability }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Depth Tape')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Rentang Ukur (m)</th>
                <th>Suhu Dasar (&#8451;)</th>
                <th>Panjang Sebenarnya (mm)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->rentang_ukur }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->suhu_dasar }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->panjang_sebenarnya }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'UTI Meter')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Rentang Ukur (m)</th>
                <th>Temperatur Dasar (&#8451;)</th>
                <th>Panjang Sebenarnya (mm)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->rentang_ukur }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->suhu_dasar }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->panjang_sebenarnya }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th colspan="5">Ukuran</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>d (mm)</th>
                <th>D (mm)</th>
                <th>e (mm)</th>
                <th>E (mm)</th>
                <th>&alpha;</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->d_inner }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->d_outer }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->e_inner }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->e_outer }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->alpha }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBM')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Merek</th>
                <th>Tipe</th>
                <th>Nomor Seri</th>
                <th>Debit Maks (L/min)</th>
                <th>Media</th>
                <th>Jumlah Nozzle</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: left; padding-left: 10px;">{{ $ttuItem->tool_brand }}</td>
                <td style="text-align: left; padding-left: 10px;">{{ $ttuItem->tool_type }}</td>
                <td style="text-align: left; padding-left: 10px;">{{ $ttuItem->tool_serial_no }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->debit_max }}</td>
                <td style="text-align: left; padding-left: 10px;">{{ $ttuItem->tool_media }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->nozzle_count }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter' 
        )
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th colspan="2">Input</th>
                <th rowspan="2">Aktual (mA)</th>
                <th colspan="2">Output (mA)</th>
                <th colspan="2">Error (%)</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>%</th>
                @if($order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter')
                <th>kg/cm<sup>2</sup></th>
                @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter')
                <th>mmH<sub>2</sub>O</th>
                @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter')
                <th>&#8457;</th>
                @endif
                <th>Up</th>
                <th>Down</th>
                <th>Up</th>
                <th>Down</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->input_pct }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->input_level }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->actual }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->output_up }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->output_down }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error_up }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error_down }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'EVC')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Error (%)</th>
                <th>BKD (%)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error }}</td>
                <td style="text-align: right; padding-right: 10px;">{{ $ttuItem->error_bkd }}</td>
            </tr>
            @endforeach
        </table>
        @endif

        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>PEMBUBUHAN TANDA TERA</strong></div>
            <div class="subtitle"><i>Affixing Verification Mark</i></div>
        </div>

        <br/>
        @if($order->ServiceRequestItem->uttp->type->kelompok == 'Depth Tape' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'UTI Meter')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    Pada lemping tanda tera dibubuhkan Tanda Daerah <b>D8</b> ({{ $order->tanda_daerah }}), Tanda Pegawai Berhak <b>H4</b> ({{ $order->tanda_pegawai_berhak }}), dan Tanda Sah <b>SL6</b> ”{{ $order->tanda_sah }}” 
                    dicapkan pada pemberat dan sampak dekat gantungan pemberat
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>1 (satu) buah buah Tanda Jaminan <b>JP8</b> dipasang pada kawat yang digunakan untuk menggantungkan lemping tanda tera</td>
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>1 (satu) buah buah Tanda Jaminan <b>JP8</b> dipasang pada penutup transmitter</td>
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'EVC')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>1 (satu) buah buah Tanda Jaminan <b>JP8</b> dipasang pada baut penutup alat dengan baut sensor</td>
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    Pada lemping tanda tera dibubuhkan Tanda Daerah <b>D8</b> ({{ $order->tanda_daerah }}), Tanda Pegawai Berhak <b>H4</b> ({{ $order->tanda_pegawai_berhak }}), dan Tanda Sah <b>SL6</b> ”{{ $order->tanda_sah }}”
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>1 (satu) buah buah Tanda Jaminan <b>JP8</b> dipasang pada kawat yang digunakan untuk menggantungkan lemping tanda tera</td>
            </tr>
            <tr>
                <td>3.</td>
                <td>1 (satu) buah Tanda Sah <b>SP6</b> dan 1 (satu) buah Tanda Pegawai yang Berhak <b>HP6</b> dibubuhkan secara bertolak belakang pada alat justir</td>
            </tr>
            <tr>
                <td>4.</td>
                <td>1 (satu) Tanda Sah Plombir SP6 dibubuhkan pada tempat yang khusus untuk penyegelan dari badan hitung sedemikian rupa, sehingga mudah serta jelas terlihat dari luar</td>
            </tr>
            <tr>
                <td>5.</td>
                <td>Pada baut-baut pengikat tutup badan hitung dibubuhkan Tanda Jaminan JP8</td>
            </tr>
            <tr>
                <td>6.</td>
                <td>Badan hitung, peralatan penghantar dan badan ukur diikat menjadi satu dengan kawat segel yang dijamin dengan Tanda Jaminan JP8</td>
            </tr>
            <tr>
                <td>7.</td>
                <td>Setiap bagian dari Pompa Ukur BBG yang memungkinkan dapat dilakukan perubahan kebenaran pengukuran, harus disegel dengan Tanda Jaminan Plombir (JP) ukuran 8 mm atau tanda jaminan yang sesuai</td>
            </tr>
        </table>
        @else
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    Pada lemping tanda tera dibubuhkan Tanda Daerah <b>D8</b> ({{ $order->tanda_daerah }}), Tanda Pegawai Berhak <b>H4</b> ({{ $order->tanda_pegawai_berhak }}), dan Tanda Sah <b>SL6</b> ”{{ $order->tanda_sah }}”
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>1 (satu) buah buah Tanda Jaminan <b>JP8</b> dipasang pada kawat yang digunakan untuk menggantungkan lemping tanda tera</td>
            </tr>
        </table>
        @endif

        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>REKOMENDASI PENGGUNAAN</strong></div>
            <div class="subtitle"><i>Recommended Instalation</i></div>
        </div>
        <br/>
        @if($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter'
        )
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'EVC')   
        <p style="padding-left: 25.4mm; padding-right: 25.4mm;">Dapat digunakan sebagai alat konversi perhitungan volume gas pada kondisi dasar.
        @else
        <p style="padding-left: 25.4mm; padding-right: 25.4mm;">Penggunaan harus dilengkapi saringan dan pemisah udara.
        @endif

        <br/>
        <br/>

        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Tera/Tera Ulang UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Persetujuan Tipe' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>

    </section>
    @endif

    @if(count($order->ttuPerlengkapans) > 0)
    <div class="page-break"></div>
    <section class="sheet watermark">
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th width="20px">No.</th>
                <th>Nama Perlengkapan</th>
                <th>Merk; Tipe; Nomor Seri; Daerah Ukur</th>
                <th>Keterangan/Penyegelan</th>
            </tr>
            @foreach($order->ttuPerlengkapans as $perlengkapan)
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $loop->index + 1 }}</td>
                <td style="padding-right: 10px;"></td>
                <td style="padding-right: 10px;">{{ $perlengkapan->uttp->tool_brand }}; {{ $perlengkapan->uttp->tool_model }}; {{ $perlengkapan->uttp->serial_no }}</td>
                <td style="padding-right: 10px;">{{ $perlengkapan->keterangan }}</td>
            </tr>
            @endforeach
        </table>
    </section>
    @endif

</body>

</html>