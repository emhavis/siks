<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Keterangan Hasil Pengujian</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-sk-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-sk-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-sk-draft-uttp.png") : public_path("assets/images/logo/letterhead-sk-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
        .table td .eng {
            font-size:8pt;
            font-style:italic;
        }
    </style>
</head>

<body class="A4">

    @if($order->cancel_at == null)
    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order }}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN HASIL PENGUJIAN</strong></div>
            <div class="subtitle"><i>Certificate of Testing Result</i></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>
        
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                @if($order->ServiceRequestItem->uttp->type->kelompok != 'Tangki Ukur Kapal')
                <tr>
                    <td style="min-width: 40mm;">Jenis UTTP<div class="eng">Measuring Instrument</div></td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                @else
                <tr>
                    <td style="min-width: 40mm;">Jenis UTTP<div class="eng">Measuring Instrument</div></td>
                    <td style="width: 3mm;">:</td>
                    <td style="width: 60mm;">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                    <td style="width: 25mm;">Volume Bersih<div class="eng">Nett Volume</div></td>
                    <td style="width: 3mm;">:</td>
                    <td style="width: 25mm;">{{ $order->ttu->volume_bersih }}</td>
                </tr>
                @endif
                @if($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Orifice')
                <tr>
                    <td style="padding-left: 15px;">Orifice Fitting</div></td>
                    <td></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td style="padding-left: 18px;">Merek<div class="eng">Trade Mark</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_brand }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 18px;">Model/Tipe<div class="eng">Model/Type</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_model }}</td>
                </tr>                
                <tr>
                    <td style="padding-left: 18px;">No. Seri<div class="eng">Serial Number</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_serial_no }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 18px;">Line Bore Size<div class="eng">Line Bore Size</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->line_bore_size }}</td>
                </tr>
                @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Kapal')
                <tr>
                    <td>Nama Kapal<div class="eng">Vessel’s Name</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->kapal }}</td>
                </tr>
                <tr>
                    <td>Ukuran<div class="eng">Dimensions</div></td>
                    <td>:</td>
                    <td colspan="4">
                        Panjang <span style="font-size:8pt; font-style:italic;">(LOA)</span> = {{ $order->ttu->panjang }};  
                        Lebar <span style="font-size:8pt; font-style:italic;">(Breadth)</span> = {{ $order->ttu->lebar }};  
                        Kedalaman <span style="font-size:8pt; font-style:italic;">(Depth)</span> = {{ $order->ttu->kedalaman }} 
                    </td>
                </tr>
                <tr>
                    <td>Pemilik / Pemakai<div class="eng">Owner / User</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequest->label_sertifikat }} / {{ $order->ttu->user }}</td>
                </tr>
                <tr>
                    <td>Operator<div class="eng">Operator</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->operator }}</td>
                </tr>
                <tr>
                    <td>Jumlah Kompartemen<div class="eng">Number of Compartments</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->no_compartments }}</td>
                </tr>
                <tr>
                    <td>No. Tabel Volume Tangki<div class="eng">Tank Volume Table No.</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->tank_vol_table_no }} tanggal {{ format_long_date($order->ttu->tank_vol_table_date) }}</td>
                </tr>
                <tr>
                    <td>Pembuat Tabel<div class="eng">Maker of Tank Volume Table </div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->pembuat_tabel }}</td>
                </tr>
                <tr>
                    <td>Diverifikasi Oleh<div class="eng">Inspected by</div></td>
                    <td>:</td>
                    <td colspan="4">Direktorat Metrologi</td>
                </tr>
                @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Tetap Silinder Tegak')
                <tr>
                    <td style="padding-left: 15px;">Merek<div class="eng">Trade Mark</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_brand }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Model/Tipe<div class="eng">Model/Type</div></td>
                    <td>:</td>
                    <td style="width: 55mm;">{{ $order->tool_model }}</td>
                    <td style="width: 20mm;">No. Seri<div class="eng">Serial Number</div></td>
                    <td style="width: 3mm;">:</td>
                    <td style="min-width: 25mm;">{{ $order->tool_serial_no }}</td>
                </tr>
                @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh')
                <tr>
                    <td style="padding-left: 15px;">Merek<div class="eng">Trade Mark</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_brand }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Model/Tipe<div class="eng">Model/Type</div></td>
                    <td>:</td>
                    <td style="width: 55mm;">{{ $order->tool_model }}</td>
                    <td style="width: 20mm;">No. Seri<div class="eng">Serial Number</div></td>
                    <td style="width: 3mm;">:</td>
                    <td style="min-width: 25mm;">{{ $order->tool_serial_no }}</td>
                </tr>
                @else
                <tr>
                    <td style="padding-left: 15px;">Merek<div class="eng">Trade Mark</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_brand }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Model/Tipe<div class="eng">Model/Type</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_model }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">No. Seri<div class="eng">Serial Number</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_serial_no }}</td>
                </tr>
                @endif
                @if($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Ultrasonik')
                <tr>
                    <td style="padding-left: 18px;">Line Bore Size<div class="eng">Line Bore Size</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->line_bore_size }}</td>
                </tr>
                @endif
                @if($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Tetap Silinder Tegak')
                <tr>
                    <td style="padding-left: 15px;">Tinggi Tangki<div class="eng">Tank Height</div></td>
                    <td style="width: 3mm;">:</td>
                    <td style="width: 55mm;">{{ $order->ttu->tinggi_tangki }}</td>
                    <td style="width: 20mm;">Diameter<div class="eng">Diamater</div></td>
                    <td style="width: 3mm;">:</td>
                    <td style="min-width: 25mm;">{{ $order->ttu->diameter }}</td>
                </tr>
                @endif
                @if($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh')
                <tr>
                    <td style="padding-left: 15px;">Jaringan Listrik<div class="eng">Power Grid</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->jaringan_listrik }}</td>
                </tr>
                @endif
                @if($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Kapal')
                @else
                @if($order->tool_capacity != null && $order->tool_capacity != '' && $order->tool_capacity > 0)
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Maksimum<div class="eng">Maximum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">
                        {{ $order->tool_capacity }}&nbsp;{!! $order->tool_capacity_unit !!}
                    </td>
                </tr>
                @endif
                @if($order->tool_capacity_min != null && $order->tool_capacity_min != '' && $order->tool_capacity_min > 0)
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Minimum<div class="eng">Minimum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_capacity_min }}&nbsp;{!! $order->tool_capacity_unit !!}
                    </td>
                </tr>
                @elseif($order->tool_capacity_min != null 
                    && $order->tool_capacity_min != '')
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Minimum<div class="eng">Minimum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_capacity_min }}&nbsp;{!! $order->tool_capacity_unit !!}
                    </td>
                </tr>
                @endif
                @if($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh')
                <tr>
                    <td style="padding-left: 15px;">Konstanta<div class="eng">Constant</div></td>
                    <td>:</td>
                    <td>{{ $order->ttu->konstanta }}</td>
                    <td style="width: 20mm;">Kelas<div class="eng">Class</div></td>
                    <td style="width: 3mm;">:</td>
                    <td style="min-width: 25mm;">{{ $order->ttu->kelas }}</td>
                </tr>
                @endif
                <tr>
                    <td>Pemilik/Pemakai<div class="eng">Owner/User</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                @endif
                <tr>
                    <td>Lokasi Alat<div class="eng">Location</div></td>
                    <td>:</td>
                    <td colspan="4">
                        @if ($order->ServiceRequest->lokasi_pengujian == 'luar')
                        {{ $order->location_alat }}
                        @else
                        {{ $order->location }}
                        @endif
                    </td>
                    <!--
                    <td colspan="4">{{ $order->ServiceRequest->addr_sertifikat }}</td>
                    -->
                </tr>
                @if($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Tetap Silinder Tegak')
                <tr>
                    @if($order->TestBy2 != null && 
                        (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                            || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
                    <td rowspan="2">Pegawai Berhak</td>
                    <td rowspan="2">:</td>
                    @else
                    <td>Pegawai Berhak</td>
                    <td>:</td>
                    @endif
                    @if ($order->TestBy1 != null)
                        @if ($order->TestBy1->PetugasUttp != null)
                    <td style="min-width: 35mm;">1. {{ $order->TestBy1->PetugasUttp->nama }}</td>
                    <td colspan="3">NIP: {{ $order->TestBy1->PetugasUttp->nip }}</td>
                        @elseif ($order->TestBy1->PetugasUut != null)
                    <td style="min-width: 35mm">1. {{ $order->TestBy1->PetugasUut->nama }}</td>
                    <td colspan="3">NIP: {{ $order->TestBy1->PetugasUut->nip }}</td>
                        @else
                    <td style="min-width: 35mm">1. </td>
                    <td colspan="3">NIP: </td>
                        @endif
                    @else 
                    <td style="min-width: 35mm"></td>
                    <td colspan="3"></td>
                    @endif
                </tr>
                @if($order->TestBy2 != null && 
                        (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                            || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
                <tr>
                    @if ($order->TestBy2 != null)
                        @if ($order->TestBy2->PetugasUttp != null)
                    <td>2. {{ $order->TestBy2->PetugasUttp->nama }}</td>
                    <td colspan="3">NIP: {{ $order->TestBy2->PetugasUttp->nip }}</td>
                        @elseif ($order->TestBy2->PetugasUut != null)
                    <td>2. {{ $order->TestBy2->PetugasUut->nama }}</td>
                    <td colspan="3">NIP: {{ $order->TestBy2->PetugasUut->nip }}</td>
                        @else
                    <td>2. </td>
                    <td colspan="3">NIP: </td>
                        @endif
                    @else 
                    <td></td>
                    <td colspan="3"></td>
                    @endif
                </tr>
                @endif
                <tr>
                    <td>Tanggal Pengujian</td>
                    <td>:</td>
                    <td colspan="4">
                    <?php
                        $start = new DateTime($order->mulai_uji);
                        $end = new DateTime($order->stat_service_order == 4 ? $order->cancel_at : $order->selesai_uji );
                        $diff = $end->diff($start);
                    ?>
                    @if($diff->d == 0)
                        {{ format_long_date($order->mulai_uji) }}
                    @else
                    @if($order->stat_service_order == 4)
                        {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->cancel_at) }}
                    @else
                        {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->selesai_uji) }}
                    @endif
                    @endif
                    </td>
                </tr>
                @endif

                <tr>
                    <td>Hasil<div class="eng">Result</div></td>
                    <td>:</td>
                    <td colspan="4">
                        @if($order->hasil_uji_memenuhi == true)
                        Disahkan berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal.
                        @else
                        Batal berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal. 
                        @endif  
                    </td>
                </tr>

                
            </tbody>
        <table>

        @if($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Tetap Silinder Tegak')
        <br />
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td colspan="2">Catatan:<div class="eng">Notes:</div></td>
            </tr>
            @if($order->hasil_uji_memenuhi == true)
            <tr>
                <td>1.</td>
                <td>UTTP ini wajib ditera ulang paling lambat tanggal <b>{{ format_long_date($order->sertifikat_expired_at) }}</b>
                    <?php setlocale(LC_TIME, "en_EN"); ?>
                    <div class="eng">This measuring instrument mandatory to reverificate on {{ strftime('%e %B %Y', strtotime($order->sertifikat_expired_at)) }}</div>
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>
                    Plat nominal dibubuhkan Tanda Daerah <b>D8</b> ({{ $order->tanda_daerah }}), Tanda Pegawai Berhak <b>H4</b> ({{ $order->tanda_pegawai_berhak }}), dan Tanda Sah <b>SL6</b> ({{ $order->tanda_sah }}) 
                    <div class="eng">On the nominal plate are affixed the Region Mark <b>D8</b> ({{ $order->tanda_daerah }}), the Authorized Officer Mark <b>H4</b> ({{ $order->tanda_pegawai_berhak }}), and the Validity Mark <b>SL6</b> ({{ $order->tanda_sah }})</div>
                </td>
            </tr>
            <tr>
                <td>3.</td>
                <td>
                    Tabel tangki terdapat pada lampiran yang merupakan bagian tidak terpisahkan dari SKHP ini.
                    <div class="eng">The tank table is included in the appendix which is an integral part of this SKHP.</div>
                </td>
            </tr>
            @endif
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Kapal')
        <br />
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td colspan="2">Catatan:<div class="eng">Notes:</div></td>
            </tr>
            @if($order->hasil_uji_memenuhi == true)
            <tr>
                <td>1.</td>
                <td>Apabila tidak terjadi perubahan atau kerusakan pada UTTP, dan masa Dry Dock belum jatuh tempo, maka SKHP ini habis masa berlaku pada tanggal <b>{{ format_long_date($order->sertifikat_expired_at) }}.</b>
                <div class="eng">If there are no changes or damages on the Measuring Instruments, and the Dry Dock is not yet due, this Certificate is valid until  {{ strftime('%e %B %Y', strtotime($order->sertifikat_expired_at)) }}</div>
                </td>
            </tr>
            @if($order->catatan_hasil_pemeriksaan != null)
            <tr>
                <td>2.</td>
                <td>{{ $order->catatan_hasil_pemeriksaan }}</td>
            </tr>
            @endif
            @endif
        </table>
        @else
        <br />
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td colspan="2">SKHP ini terdiri dari {{ count($order->ttuPerlengkapans) > 0 ? '3 (tiga)' : '2 (dua)' }} halaman
                    <div class="eng">This certificate consists of {{ count($order->ttuPerlengkapans) > 0 ? '3 (three)' : '2 (two)' }} pages</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">Catatan:<div class="eng">Notes:</div></td>
            </tr>
            @if($order->hasil_uji_memenuhi == true)
            @if($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Ultrasonik')
            <tr>
                <td style="padding-left: 10px; width: 3mm;">1.</td>
                <td>Sistem UTTP ini wajib ditera ulang paling lambat tanggal <b>{{ format_long_date($order->sertifikat_expired_at) }}</b>
                    <?php setlocale(LC_TIME, "en_EN"); ?>
                    <div class="eng">This measuring instrument mandatory to reverificate on {{ strftime('%e %B %Y', strtotime($order->sertifikat_expired_at)) }}</div>
                </td>
            </tr>
            @else
            <tr>
                <td style="padding-left: 10px; width: 3mm;">1.</td>
                <td>UTTP ini wajib ditera ulang paling lambat tanggal <b>{{ format_long_date($order->sertifikat_expired_at) }}</b>
                    <?php setlocale(LC_TIME, "en_EN"); ?>
                    <div class="eng">This measuring instrument mandatory to reverificate on {{ strftime('%e %B %Y', strtotime($order->sertifikat_expired_at)) }}</div>
                </td>
            </tr>
            @endif
            @if($order->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice')
            <tr>
                <td style="padding-left: 10px; width: 3mm;">2.</td>
                <td>Data ukuran Pelat Orifis ditampilkan pada suhu referensi <b>{{ number_format((float)$order->ttu->suhu_pengujian, 1, ',', '') }} &deg;C</b>
                    <div class="eng">This Orifice Plate is calibrated at reference temperature <b>{{ number_format((float)$order->ttu->suhu_pengujian, 1, '.', '') }} &deg;C</b></div>
                </td>
            </tr>
            @if($order->catatan_hasil_pemeriksaan != null && strlen($order->catatan_hasil_pemeriksaan))
            <tr>
                <td style="padding-left: 10px; width: 3mm;">3.</td>
                <td>{!! $order->catatan_hasil_pemeriksaan !!}</td>
            </tr>
            @endif
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Ultrasonik')
            <tr>
                <td style="padding-left: 10px; width: 3mm;">2.</td>
                <td>Wet Callibration dilaksanakan pada tanggal {{ format_long_date($order->ttu->tanggal_wet_cal) }}, 
                dan Wet Callibration selanjutnya wajib dilaksanakan selambat-lambatnya pada tanggal {{ format_long_date($order->ttu->expired_wet_cal) }}
                    <div class="eng">Wet Callibration conducted on {{ format_long_date($order->ttu->tanggal_wet_cal) }}, 
                        and the next Wet Callibration must be conducted no later than {{ format_long_date($order->ttu->expired_wet_cal) }}</div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px; width: 3mm;">3.</td>
                <td>Apabila tanda tera rusak dan/atau kawat segel putus, UTTP ini wajib ditera ulang
                    <div class="eng">If the verification mark is damaged and/or the seal wire is broken, this measuring instrument must be re-verificated</div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px; width: 3mm;">4.</td>
                <td>Pemutusan tanda tera hanya dapat dilakukan dengan sepengetahuan Direktorat Metrologi
                    <div class="eng">Breaking of the verification mark can only be done with the agreement of the Directorate of Metrology</div>
                </td>
            </tr>
            @else
            <tr>
                <td style="padding-left: 10px; width: 3mm;">2.</td>
                <td>Apabila tanda tera rusak dan/atau kawat segel putus, UTTP ini wajib ditera ulang
                    <div class="eng">If the verification mark is damaged and/or the seal wire is broken, this measuring instrument must be re-verificated</div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px; width: 3mm;">3.</td>
                <td>Pemutusan tanda tera hanya dapat dilakukan dengan sepengetahuan Direktorat Metrologi
                    <div class="eng">Breaking of the verification mark can only be done with the agreement of the Directorate of Metrology</div>
                </td>
            </tr>
            @if($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
            <tr>
                <td style="padding-left: 10px; width: 3mm;">4.</td>
                <td>Harus dilakukan verifikasi dan setting level saat ALG dipasang di tangki dengan sepengetahuan Direktorat Metrologi serta dilakukan lock paramter/penguncian setelah setting level
                    <div class="eng">It is mandatory to perform verification and setting level on the ALG upon its installation on the tank with the consent of Directorate of Metrology, and lock the parameters afterward</div>
                </td>
            </tr>
            @elseif($order->catatan_hasil_pemeriksaan != null && strlen($order->catatan_hasil_pemeriksaan))
            <tr>
                <td style="padding-left: 10px; width: 3mm;">4.</td>
                <td>{!! $order->catatan_hasil_pemeriksaan !!}</td>
            </tr>
            @endif
            @endif
            @else
            <tr>
                <td style="padding-left: 10px; width: 3mm;">&nbsp;</td>
                <td>{!! $order->catatan_hasil_pemeriksaan !!}</td>
            </tr>
            @endif
        </table>
        @endif

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Kepala Balai Pengujian
                        <br/>Alat Ukur, Alat Takar, Alat Timbang
                        <br/>dan Alat Perlengkapan
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} ' 
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}
                        <br/>NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>

    @if($order->ServiceRequestItem->uttp->type->kelompok != 'Tangki Ukur Tetap Silinder Tegak')
    <div class="page-break"></div>
    <section class="sheet watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <div style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>DATA PENGUJIAN</strong></div>
            <div class="subtitle"><i>Testing Data</i></div>
        </div>

        <br/>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            @if($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Kadar Air')
            <tr>
                <td>Metode Pengukuran</td>
                <td>:</td>
                <td colspan="4">{{ $order->tool_media_pengukuran }}</td>
            </tr>
            @endif
            <tr>
                @if($order->TestBy2 != null && 
                    (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                        || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
                <td rowspan="2">Pegawai Berhak</td>
                <td rowspan="2">:</td>
                @else
                <td>Pegawai Berhak</td>
                <td>:</td>
                @endif
                @if ($order->TestBy1 != null)
                    @if ($order->TestBy1->PetugasUttp != null)
                <td style="min-width: 35mm">1. {{ $order->TestBy1->PetugasUttp->nama }}</td>
                <td>NIP: {{ $order->TestBy1->PetugasUttp->nip }}</td>
                    @elseif ($order->TestBy1->PetugasUut != null)
                <td style="min-width: 35mm">1. {{ $order->TestBy1->PetugasUut->nama }}</td>
                <td>NIP: {{ $order->TestBy1->PetugasUut->nip }}</td>
                    @else
                <td style="min-width: 35mm">1. </td>
                <td>NIP: </td>
                    @endif
                @else 
                <td style="min-width: 35mm"></td>
                <td></td>
                @endif
            </tr>
            @if($order->TestBy2 != null && 
                    (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                        || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
            <tr>
                @if ($order->TestBy2 != null)
                    @if ($order->TestBy2->PetugasUttp != null)
                <td>2. {{ $order->TestBy2->PetugasUttp->nama }}</td>
                <td>NIP: {{ $order->TestBy2->PetugasUttp->nip }}</td>
                    @elseif ($order->TestBy2->PetugasUut != null)
                <td>2. {{ $order->TestBy2->PetugasUut->nama }}</td>
                <td>NIP: {{ $order->TestBy2->PetugasUut->nip }}</td>
                    @else
                <td>2. </td>
                <td>NIP: </td>
                    @endif
                @else 
                <td></td>
                <td></td>
                @endif
            </tr>
            @endif
            <tr>
                <td>Tanggal Pengujian</td>
                <td>:</td>
                <td colspan="4">
                <?php
                    $start = new DateTime($order->mulai_uji);
                    $end = new DateTime($order->stat_service_order == 4 ? $order->cancel_at : $order->selesai_uji );
                    $diff = $end->diff($start);
                ?>
                @if($diff->d == 0)
                    {{ format_long_date($order->mulai_uji) }}
                @else
                @if($order->stat_service_order == 4)
                    {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->cancel_at) }}
                @else
                    {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->selesai_uji) }}
                @endif
                @endif
                </td>
            </tr>
            <tr>
                <td>Lokasi</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'Direktorat Metrologi' : $order->location }}
                </td>
            </tr>
            @if($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge' || 
                $order->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice'
            )
            @if($order->ttuBadanHitungs != null && count($order->ttuBadanHitungs) > 0)
            <tr>
                <td>Identitas Badan Hitung</td>
                <td>:</td>
                <td colspan="4">
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">Tipe</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $tipe_array = $order->ttuBadanHitungs->pluck('type')->toArray(); 
                    ?>
                    {{ implode("; ", $tipe_array) }}
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">No Seri</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $seri_array = $order->ttuBadanHitungs->pluck('serial_no')->toArray(); 
                    ?>
                    {{ implode("; ", $seri_array) }}
                </td>
            </tr>
            @endif
            @if($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
            <tr>
                <td>Nomor Tangki</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->tank_no }}
                </td>
            </tr>
            <tr>
                <td>Nomor Tag</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->tag_no }}
                </td>
            </tr>
            @endif
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus Media Air')
            @if($order->ttuBadanHitungs != null && count($order->ttuBadanHitungs) > 0)
            <tr>
                <td>Identitas Badan Hitung</td>
                <td>:</td>
                <td colspan="4">
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">Merek</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $merek_array = $order->ttuBadanHitungs->pluck('brand')->toArray(); 
                    ?>
                    {{ implode("; ", $merek_array) }}
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">Tipe</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $tipe_array = $order->ttuBadanHitungs->pluck('type')->toArray(); 
                    ?>
                    {{ implode("; ", $tipe_array) }}
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">No Seri</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $seri_array = $order->ttuBadanHitungs->pluck('serial_no')->toArray(); 
                    ?>
                    {{ implode("; ", $seri_array) }}
                </td>
            </tr>
            @endif
            <tr>
                <td>Totalisator</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->totalisator }}
                </td>
            </tr>
            <tr>
                <td>K Faktor</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->kfactor }}
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air')
            <tr>
                <td>Totalisator</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->totalisator }}
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' || 
                $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus Media Air' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Arus BBM')
            @if($order->ttuBadanHitungs != null && count($order->ttuBadanHitungs) > 0)
            <tr>
                <td>Identitas Badan Hitung</td>
                <td>:</td>
                <td colspan="4">
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">Merek</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $merek_array = $order->ttuBadanHitungs->pluck('brand')->toArray(); 
                    ?>
                    {{ implode("; ", $merek_array) }}
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">Tipe</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $tipe_array = $order->ttuBadanHitungs->pluck('type')->toArray(); 
                    ?>
                    {{ implode("; ", $tipe_array) }}
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10mm;">No Seri</td>
                <td>:</td>
                <td colspan="4">
                    <?php 
                        $seri_array = $order->ttuBadanHitungs->pluck('serial_no')->toArray(); 
                    ?>
                    {{ implode("; ", $seri_array) }}
                </td>
            </tr>
            @endif
            @if($order->ttu->suhu_pengujian != null)
            <tr>
                <td>Suhu Pengujian</td>
                <td>:</td>
                <td colspan="4">
                {{ number_format((float)$order->ttu->suhu_pengujian, 3, ',', '') }} &deg;C
                </td>
            </tr>
            @endif
            <tr>
                <td>K Faktor</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->kfactor }}
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG')
            <tr>
                <td>Totalisator</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->totalisator }}
                </td>
            </tr>
            <tr>
                <td>K Faktor</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->kfactor }}
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBM')
            <tr>
                <td>Totalisator</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->totalisator }}
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Ultrasonik')
            <tr>
                <td>K Faktor</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->kfactor }}
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh')
            <tr>
                <td>Kondisi Ruangan</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->kondisi_ruangan }}
                </td>
            </tr>
            <tr>
                <td>Trapo Ukur (CT; PT)</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->trapo_ukur }}
                </td>
            </tr>
            @endif
        </table>  

        @if($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Ultrasonik' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Orifice' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'Pens Recorder')
        @else
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>HASIL PENGUJIAN</strong></div>
            <div class="subtitle"><i>Testing Result</i></div>
        </div>
        @endif
        
        <br/>
        @if($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th rowspan="2">Input Level (mm)</th>
                <th colspan="2">Kesalahan Penunjukan</th>
                <th rowspan="2">Kesalahan Histerisis (mm)</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>Kesalahan Naik (mm)</th>
                <th>Kesalahan Turun (mm)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->input_level, 1, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error_up, 1, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error_down, 1, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error_hysteresis, 1, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus Media Air' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Arus BBM')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Kecepatan Alir/<div style="font-style:italic;font-szie:8px;">Flow Rate</div>(L/min)</th>
                <th>Kesalahan/<div style="font-style:italic;font-szie:8px;">Error</div>(%)</th>
                <th>Meter Faktor</th>
                <th>Kemampuan Ulang/<div style="font-style:italic;font-szie:8px;">Repeatability</div>(%)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->flow_rate, 0, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->meter_factor, 5, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->repeatability, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Kecepatan Alir/<div style="font-style:italic;font-szie:8px;">Flow Rate</div>(m<sup>3</sup>/h)</th>
                <th>Kesalahan/<div style="font-style:italic;font-szie:8px;">Error</div>(%)</th>
                <th>Kemampuan Ulang/<div style="font-style:italic;font-szie:8px;">Repeatiblity</div>(%)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->flow_rate, 2, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->repeatability, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Kecepatan Alir/<div style="font-style:italic;font-szie:8px;">Flow Rate</div>(m3/h)</th>
                <th>Kesalahan/<div style="font-style:italic;font-szie:8px;">Error</div>(%)<br/>BKD: &plusmn;0,5%</th>
                <th>Kemampuan Ulang/<div style="font-style:italic;font-szie:8px;">Repeatiblity</div>(%)<br/>BKD: &plusmn;0,1%</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->flow_rate, 2, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->repeatability, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Kadar Air')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Komoditas</div></th>
                <th>Penunjukan Alat Rata-Rata (%)</th>
                <th>Kesalahan (%)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->commodity }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->penunjukan, 2, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error, 2, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Kecepatan Alir (m3/h)</th>
                <th>Kesalahan</th>
                <th>Kemampuan Ulang</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->flow_rate, 2, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->repeatability, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Depth Tape')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Rentang Ukur (mm)</th>
                <th>Suhu Dasar (&deg;C)</th>
                <th>Panjang Sebenarnya (mm)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->rentang_ukur, 0, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->suhu_dasar, 0, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->panjang_sebenarnya, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'UTI Meter')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Rentang Ukur (mm)</th>
                <th>Temperatur Dasar (&deg;C)</th>
                <th>Panjang Sebenarnya (mm)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->rentang_ukur, 0, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->suhu_dasar, 0, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->panjang_sebenarnya, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        <br/>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <th colspan="3">Temperatur</th>
            </tr>
            <tr>
                <th>Penunjukan UTTP (&deg;C)</th>
                <th>Penunjukan Standar (&deg;C)</th>
                <th>Koreksi (&deg;C)</th>
            </tr>
            @foreach($order->ttuSuhus as $suhu)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$suhu->penunjukan, 1, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$suhu->penunjukan_standar, 1, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$suhu->koreksi, 1, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice')
        <div style="text-align: center;">
        <img style="padding-left: 25.4mm; padding-right: 25.4mm;" width="450"
            src="{{ $view ? asset('assets/images/logo/skhp_po.png') : public_path('assets/images/logo/skhp_po.png') }}" alt="Gambar PO">
        </img>
        </div>
        <br/><br/>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th colspan="9">Ukuran</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>d (mm)</th>
                <th>D (mm)</th>
                <th>e (mm)</th>
                <th>E (mm)</th>
                <th>Sudut Bevel</th>
                <th>Roundness (mm)</th>
                <th>Flatness (mm)</th>
                <th>Roughness (&#181;m)</th>
                <th>Eksentrisitas (mm)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->d_inner != null ? number_format((float)$ttuItem->d_inner, 3, ',', '') : '-' }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->d_outer != null ? number_format((float)$ttuItem->d_outer, 3, ',', '') : '-' }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->e_inner != null ? number_format((float)$ttuItem->e_inner, 3, ',', '') : '-' }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->e_outer != null ? number_format((float)$ttuItem->e_outer, 3, ',', '') : '-' }}</td>
                <td style="text-align: center; padding-right: 10px;">{!! $ttuItem->alpha !!}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->roundness != null ? number_format((float)$ttuItem->roundness, 3, ',', '') : '-' }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->flatness != null ? number_format((float)$ttuItem->flatness, 3, ',', '') : '-' }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->roughness != null ? number_format((float)$ttuItem->roughness, 3, ',', '') : '-' }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->eksentrisitas != null ? number_format((float)$ttuItem->eksentrisitas, 3, ',', '') : '-' }}</td>
            </tr>
            @endforeach
        </table>
        <br/><br/>
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>
                    <u>Catatan:</u><br/>
                    Ketidakpastian U<sub>95%</sub> = {{ number_format((float)$order->ttu->ketidakpastian, 3, ',', '') }} mm 
                    (k = {{ $order->ttu->kfactor }})
                </td>
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBM')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Merek</th>
                <th>Tipe</th>
                <th>Nomor Seri</th>
                <th>Debit Maks (L/min)</th>
                <th>Media</th>
                <th>Jumlah Nozzle</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: left; padding-left: 10px;">{{ $ttuItem->tool_brand }}</td>
                <td style="text-align: left; padding-left: 10px;">{{ $ttuItem->tool_type }}</td>
                <td style="text-align: left; padding-left: 10px;">{{ $ttuItem->tool_serial_no }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->debit_max, 2, ',', '') }}</td>
                <td style="text-align: left; padding-left: 10px;">{{ $ttuItem->tool_media }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->nozzle_count }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter' 
        )
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th colspan="2">Input</th>
                <th rowspan="2">Aktual({!! $order->satuan_output !!})</th>
                <th colspan="2">Output ({!! $order->satuan_output !!})</th>
                <th colspan="2">Error ({!! $order->satuan_error !!})</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>%</th>
                @if($order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter')
                <th>kg/cm<sup>2</sup></th>
                @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter')
                <th>mmH<sub>2</sub>O</th>
                @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter')
                <th>{!! $order->satuan_suhu !!}</th>
                @endif
                <th>Up</th>
                <th>Down</th>
                <th>Up</th>
                <th>Down</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->input_pct, 2, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->input_level, 2, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->actual, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->output_up, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->output_down, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error_up, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error_down, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'EVC')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Error (%)</th>
                <th>BKD (%)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error_bkd, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <th colspan="6">Kesalahan/Error Meter kWh</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>Tegangan (V)</th>
                <th>Frekuensi (Hz)</th>
                <th>Arus (A)</th>
                <th>Faktor Daya</th>
                <th>Kesalahan (%)</th>
                <th>Ketidaktetapan (%)</th>
            </tr>
            @foreach($order->ttuItems->filter(function ($value, $key) {
                        return $value->jenis_item == 'kWh';
                    }) as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->tegangan, 1, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->frekuensi, 0, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->arus, 2, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->faktor_daya }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->ketidaktetapan, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        <br/>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <th colspan="6">Kesalahan/Error Meter kVARh</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>Tegangan (V)</th>
                <th>Frekuensi (Hz)</th>
                <th>Arus (A)</th>
                <th>Faktor Daya</th>
                <th>Kesalahan (%)</th>
                <th>Ketidaktetapan (%)</th>
            </tr>
            @foreach($order->ttuItems->filter(function ($value, $key) {
                        return $value->jenis_item == 'kVARh';
                    }) as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->tegangan, 1, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->frekuensi, 0, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->arus, 2, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->faktor_daya }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->ketidaktetapan, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Kapal')
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Tangki</th>
                <th>Sounding (m)</th>
                <th>Volume (m3)</th>
            </tr>
            @foreach($order->ttuItems as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->tank_no }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->sounding, 4, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->volume, 2, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        @endif

        @if($order->ServiceRequestItem->uttp->type->kelompok != 'Tangki Ukur Kapal')

        @if($order->hasil_uji_memenuhi == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>PEMBUBUHAN TANDA TERA</strong></div>
            <div class="subtitle"><i>Affixing Verification Mark</i></div>
        </div>

        <br/>
        @if($order->ServiceRequestItem->uttp->type->kelompok == 'UTI Meter')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    Pada lemping tanda tera dibubuhkan Tanda Daerah <b>D8</b> ({{ $order->tanda_daerah }}), Tanda Pegawai Berhak <b>H4</b> ({{ $order->tanda_pegawai_berhak }}), dan Tanda Sah <b>SL6</b> ”{{ $order->tanda_sah }}” 
                    dicapkan pada pemberat dan sampak dekat gantungan pemberat
                </td>
            </tr>
            <tr>
                <td>2.</td>
                @if($order->ServiceRequest->service_type_id == 4)
                <td>1 (satu) buah Tanda Jaminan <b>JP8</b> dipasang pada kawat yang digunakan untuk menggantungkan lemping tanda tera</td>
                @elseif($order->ServiceRequest->service_type_id == 5)
                <td>1 (satu) buah Tanda Sah <b>SP6</b> "{{ $order->selesai_uji ? date('y', strtotime($order->selesai_uji)) : date('y') }}" dipasang pada kawat yang digunakan untuk menggantungkan lemping tanda tera</td>
                @endif 
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pens Recorder')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                1 (satu) buah Tanda Jaminan <b>JP8</b> dan 1 (satu) buah Tanda Sah “{{ $order->selesai_uji ? date('y', strtotime($order->selesai_uji)) : date('y') }}” pada penutup Three Pen Recorder
                </td>
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Depth Tape')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    Tanda Daerah <b>D8</b> ({{ $order->tanda_daerah }}) dan Tanda Pegawai Berhak <b>H4</b> ({{ $order->tanda_pegawai_berhak }})  
                    dicapkan pada engkol dan pemberat, Tanda Sah <b>SL6</b> ”{{ $order->tanda_sah }}” dicapkan pada sampak dekat gantungan pemberat
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>Tanda Sah <b>SL4</b> "{{ $order->selesai_uji ? date('y', strtotime($order->selesai_uji)) : date('y') }}" dicapkan pada pemberat</td>
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>1 (satu) buah Tanda Jaminan <b>JP8</b> dipasang pada penutup transmitter</td>
                <!--
                @if($order->ServiceRequest->service_type_id == 4)
                <td>1 (satu) buah Tanda Jaminan <b>JP8</b> dipasang pada penutup transmitter</td>
                @elseif($order->ServiceRequest->service_type_id == 5)
                <td>1 (satu) buah Tanda Sah <b>SP6</b> "{{ $order->kabalai_date ? date('y', strtotime($order->kabalai_date)) : date('y') }}" dipasang pada penutup transmitter</td>
                @endif
                --> 
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'EVC')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                @if($order->ServiceRequest->service_type_id == 4)
                <td>1 (satu) buah Tanda Jaminan <b>JP8</b> dipasang pada baut penutup alat dengan baut sensor</td>
                @elseif($order->ServiceRequest->service_type_id == 5)
                <td>1 (satu) buah Tanda Jaminan <b>JP8</b> dipasang pada baut penutup alat dengan baut sensor</td>
                @endif 
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    Pada lemping tanda tera dibubuhkan Tanda Daerah <b>D8</b> ({{ $order->tanda_daerah }}), Tanda Pegawai Berhak <b>H4</b> ({{ $order->tanda_pegawai_berhak }}), dan Tanda Sah <b>SL6</b> ”{{ $order->tanda_sah }}”
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>1 (satu) buah Tanda Jaminan <b>JP8</b> dipasang pada kawat yang digunakan untuk menggantungkan lemping tanda tera</td>
            </tr>
            <tr>
                <td>3.</td>
                <td>1 (satu) buah Tanda Sah <b>SP6</b> dan 1 (satu) buah Tanda Pegawai yang Berhak <b>HP6</b> dibubuhkan secara bertolak belakang pada alat justir</td>
            </tr>
            <tr>
                <td>4.</td>
                <td>1 (satu) Tanda Sah Plombir SP6 dibubuhkan pada tempat yang khusus untuk penyegelan dari badan hitung sedemikian rupa, sehingga mudah serta jelas terlihat dari luar</td>
            </tr>
            <tr>
                <td>5.</td>
                <td>Pada baut-baut pengikat tutup badan hitung dibubuhkan Tanda Jaminan JP8</td>
            </tr>
            <tr>
                <td>6.</td>
                <td>Badan hitung, peralatan penghantar dan badan ukur diikat menjadi satu dengan kawat segel yang dijamin dengan Tanda Jaminan JP8</td>
            </tr>
            <tr>
                <td>7.</td>
                <td>Setiap bagian dari Pompa Ukur BBG yang memungkinkan dapat dilakukan perubahan kebenaran pengukuran, harus disegel dengan Tanda Jaminan Plombir (JP) ukuran 8 mm atau tanda jaminan yang sesuai</td>
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus Media Air')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    Pada lemping tanda tera dibubuhkan Tanda Daerah <b>D8</b> ({{ $order->tanda_daerah }}), Tanda Pegawai Berhak <b>H4</b> ({{ $order->tanda_pegawai_berhak }}), dan Tanda Sah <b>SL6</b> ”{{ $order->tanda_sah }}”
                </td>
            </tr>
            <tr>
                <td>2.</td>
                @if($order->ServiceRequest->service_type_id == 4)
                <td>1 (satu) buah Tanda Jaminan <b>JP8</b> untuk dibubuhkan pada kawat yang menghubungkan lemping tanda tera serta pada bagian-bagian meter yang tidak boleh dilakukan perubahan dan 1 (satu) buah Tanda Jaminan JP8 pada Alat justir.</td>
                @elseif($order->ServiceRequest->service_type_id == 5)
                <td>1 (satu) buah Tanda Sah <b>SP6</b> "{{ $order->selesai_uji ? date('y', strtotime($order->selesai_uji)) : date('y') }}"  pada alat justir serta 1 (satu) buah Tanda Jaminan JP8 dibubuhkan pada kawat yang menghubungkan lemping tera dan bagian-bagian meter yang tidak boleh dilakukan perubahan.</td>
                @endif 
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Kadar Air')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    Cap Tanda Tera pada Lemping: Tanda Daerah D4 ({{ $order->tanda_daerah }}), Tanda Pegawai Berhak H4 ({{ $order->tanda_pegawai_berhak }}) dan Tanda Sah SL4 “{{ $order->tanda_sah }}”.
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>Pembubuhan Tanda Tera: Tanda Tera Jaminan JL4 dan Tanda Tera Sah SL6 “{{ $order->selesai_uji ? date('y', strtotime($order->selesai_uji)) : date('y') }}” dicap pada lak yang dilekatkan pada bagian badan meter kadar air.</td>
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'ATG')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td colspan="2">
                Pembubuhan Tanda Tera: 1 (satu) buah Tanda Jaminan <b>JP8</b> pada baut penutup alat penyetel nol.
                </td>
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh')
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    1 (satu) buah Tanda Pegawai Berhak <b>HP4</b> "{{ $order->tanda_pegawai_berhak }}"" dan Tanda Sah SP6 "{{ $order->tanda_sah }}" dipasang secara bolak-balik pada baut penutup Meter kWh
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>1 (satu) buah Tanda Jaminan <b>JP8</b> dibubuhkan pada tombol reset Meter kWh</td>
            </tr>
        </table>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice')
        <!--
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td style="width: 5mm;">1.</td>
                <td>
                    Pada lemping tanda tera dibubuhkan Tanda Daerah <b>D8</b> ({{ $order->tanda_daerah }}), Tanda Pegawai Berhak <b>H4</b> ({{ $order->tanda_pegawai_berhak }}), dan Tanda Sah <b>SL6</b> ”{{ $order->tanda_sah }}”
                </td>
            </tr>
        </table>
        -->
        @else
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    Pada lemping tanda tera dibubuhkan Tanda Daerah <b>D8</b> ({{ $order->tanda_daerah }}), Tanda Pegawai Berhak <b>H4</b> ({{ $order->tanda_pegawai_berhak }}), dan Tanda Sah <b>SL6</b> ”{{ $order->tanda_sah }}”
                </td>
            </tr>
            <tr>
                <td>2.</td>
                @if($order->ServiceRequest->service_type_id == 4)
                <td>1 (satu) buah Tanda Jaminan <b>JP8</b> dipasang pada kawat yang digunakan untuk menggantungkan lemping tanda tera</td>
                @elseif($order->ServiceRequest->service_type_id == 5)
                <td>1 (satu) buah Tanda Sah <b>SP6</b> "{{ $order->selesai_uji ? date('y', strtotime($order->selesai_uji)) : date('y') }}" dipasang pada kawat yang digunakan untuk menggantungkan lemping tanda tera</td>
                @endif 
            </tr>
        </table>
        @endif

        @if($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Arus BBM' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Orifice' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Turbin' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Ultrasonik' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'ATG' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Non Automatic Weighing Instruments (NAWI)'
        )
        @else
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>REKOMENDASI PENGGUNAAN</strong></div>
            <div class="subtitle"><i>Recommended Instalation</i></div>
        </div>
        @if($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice' || 
        $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Depth Tape' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'UTI Meter' ||
        $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Kadar Air'
        )
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'EVC')   
        <p style="padding-left: 25.4mm; padding-right: 25.4mm;">Dapat digunakan sebagai alat konversi perhitungan volume gas pada kondisi dasar.</p>
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Pens Recorder')
        @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh')
        @else
        <p style="padding-left: 25.4mm; padding-right: 25.4mm;">Penggunaan harus dilengkapi saringan dan pemisah udara.</p>
        @endif
        @endif

        @endif

        @endif

        <br/>
        <br/>

        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Tera/Tera Ulang UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Tera/Tera Ulang UTTP' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>

    </section>
    @endif

    @if(count($order->ttuPerlengkapans) > 0)
    <div class="page-break"></div>
    <section class="sheet watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <table class="table table-bordered" width="100%" style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th width="20px">No.</th>
                <th>Nama Perlengkapan</th>
                <th>Merk; Tipe; Nomor Seri; <br/>Daerah Ukur</th>
                <th>Keterangan/Penyegelan</th>
            </tr>
            @foreach($order->ttuPerlengkapans as $perlengkapan)
            <?php
                $kapasitas = '';
                if (strlen($perlengkapan->uttp->tool_capacity_min) > 0) {
                    if ($perlengkapan->uttp->tool_capacity_min != null 
                        && strlen($perlengkapan->uttp->tool_capacity_min) > 0)
                    {
                        $kapasitas = $perlengkapan->uttp->tool_capacity_min . ' - ';
                    } 
                    $kapasitas .= $perlengkapan->uttp->tool_capacity . ' ' . $perlengkapan->uttp->tool_capacity_unit;
                }
            ?>
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $loop->index + 1 }}</td>
                <td style="padding-right: 10px;">
                    {{ $perlengkapan->uttp->type->uttp_type }}
                    @if($perlengkapan->tag != null && strlen($perlengkapan->tag) > 0)
                    <br/>(Tag No. {{ $perlengkapan->tag }})
                    @endif
                </td>
                <td style="padding-right: 10px;">{{ $perlengkapan->uttp->tool_brand }}; {{ $perlengkapan->uttp->tool_model }}; {{ $perlengkapan->uttp->serial_no }}; <br/>{{ $kapasitas }} </td>
                <td style="padding-right: 10px;">{{ $perlengkapan->keterangan }}</td>
            </tr>
            @endforeach
        </table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Tera/Tera Ulang UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Tera/Tera Ulang UTTP' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
    @endif

    @else

    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order }}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>

        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        Berdasarkan permohonan pengujian dengan No. Order {{ $order->ServiceRequestItem->no_order }} tanggal 
        {{ format_long_date($order->ServiceRequestItem->order_at) }}, untuk 
        </p>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 30mm;">Alat Ukur</td>
                    <td style="width: 2mm;">:</td>
                    <td colspan="5">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td>Merek</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_brand != null ?
                        $order->tool_brand  :
                        $order->ServiceRequestItem->uttp->tool_brand 
                    }}</td>
                </tr>
                <tr>
                    <td>Tipe</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_model != null ?
                        $order->tool_model :
                        $order->ServiceRequestItem->uttp->tool_model 
                    }}</td>
                </tr>
                <tr>
                    <td>Nomor Seri</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_serial_no != null  ?
                        $order->tool_serial_no :
                        $order->ServiceRequestItem->uttp->serial_no 
                    }}</td>
                </tr>
                
                <tr>
                    <td>Pemilik</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                
            </tbody>
        <table>

        <br/>
        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        <strong>batal diuji</strong> di Instalasi Uji {{ $order->instalasi->nama_instalasi }} Direktorat Metrologi karena {{ $order->cancel_notes }}.
        </p>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td>Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date("d-m-Y") }}
                        <br/>Kepala Balai Pengujian UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}<br/>
                        NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>

    @endif



</body>

</html>