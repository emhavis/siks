@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <ul class="nav nav-tabs" role="tablist" id="tabs">
        @foreach($instalasiList as $instalasi)
        @if ($loop->first)
        <li role="presentation" class="active"><a href="#order_{{ $instalasi->id }}" aria-controls="order_{{ $instalasi->id }}" role="tab" data-toggle="tab">{{ $instalasi->nama_instalasi }}</a></li>
        @else
        <li role="presentation"><a href="#order_{{ $instalasi->id }}" aria-controls="order_{{ $instalasi->id }}" role="tab" data-toggle="tab">{{ $instalasi->nama_instalasi }}</a></li>
        @endif
        @endforeach
    </ul>
    <div class="tab-content" id="nav-tabContent">
        @foreach($instalasiList as $instalasi)
        <div role="tabpanel" class="tab-pane {{ ($loop->first) ? 'active' : '' }}" id="order_{{ $instalasi->id }}">
            <br/>
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                        {!! Form::open(['url' => route('serviceuttp.savetestqr'), 'id' => 'qr'])  !!}
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="no_order">Selesai Uji Berdasar QR Code</label>
                                            <input type="hidden" name="instalasi_id" value="{{ $instalasi->id }}" />
                                            {!! Form::text('no_order', null, ['class' => 'form-control no_order','id' => 'no_order', 'autofocus' => 'autofocus']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="error_notes" clsas=""></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br/>
                    <table id="data_table" class="table table-striped table-hover table-responsive-sm data-table">
                        <thead>
                            <tr>
                                <th>No Order</th>
                                <th>Alat</th>
                                <th>Pengujian</th>
                                <th>Diterima Oleh</th>
                                <th>Diuji Oleh</th>
                                <th>Tgl Terima</th>
                                <th>Tgl Selesai Uji</th>
                                <th>Status Alat</th>
                                <th>Berkas</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows[$instalasi->id] as $row)
                            <tr>
                                <td>{{ $row->ServiceRequestItem->no_order }} / {{ count($row->ServiceRequest->items) }}</td>
                                <td>{{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : '-' }})</td> 
                                <td>
                                    @foreach($row->ServiceRequestItem->inspections as $inspection)
                                    {{ $inspection->inspectionPrice->inspection_type }}
                                    <br/>
                                    @endforeach
                                </td>
                                <td>{{ $row->MasterUsers ? $row->MasterUsers->full_name : "" }}</td>
                                <td>{{ $row->TestBy1 ? $row->TestBy1->full_name : "" }} {{ $row->TestBy2 ? ' & '.$row->TestBy2->full_name : "" }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->staff_entry_datein)) }}</td>
                                <td>{{ $row->staff_entry_dateout != null ? date("d-m-Y", strtotime($row->staff_entry_dateout)) : '' }}</td>
                                <td>
                                    {{ $row->stat_warehouse == 2 ? 'Sudah Diserahterimakan dengan Pemohon' : 
                                        ($row->stat_warehouse == 1 ? 'Berada di Gudang' : 'Berada di Instalasi')}}
                                </td>
                                <td>
                                    @if ($row->ServiceRequest->service_type_id == 4)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/type_approval_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Persetujuan Tipe</a>
                                    @elseif ($row->ServiceRequest->service_type_id == 5)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Sertifikat Sebelumnya</a>
                                    @elseif ($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                                        @if ($row->ServiceRequestItem->path_calibration_manual != null)
                                        <a href="<?= config('app.siks_url') ?>/tracking/download/calibration_manual/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Manual Kalibrasi</a>
                                        @endif
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/manual_book/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Buku Manual</a>
                                    @endif
                                </td>
                                
                                <td>
                                @if($row->stat_service_order=="0")
                                    @if($row->pending_status=="0" || $row->pending_status==null)
                                    <a href="{{ route('serviceuttp.test', $row->id) }}" class="btn btn-warning btn-sm">SELESAI UJI</a>
                                    <a href="{{ route('serviceuttp.pending', $row->id) }}" class="btn btn-danger btn-sm">TUNDA</a>
                                    @else
                                    <a href="{{ route('serviceuttp.continue', $row->id) }}" class="btn btn-info btn-sm">LANJUT UJI</a>
                                    @endif
                                    <a href="{{ route('serviceuttp.cancel', $row->id) }}" class="btn btn-danger btn-sm">BATAL UJI</a>
                                @elseif($row->stat_service_order=="1")
                                    @if($row->stat_sertifikat==0)
                                    <a href="{{ route('serviceuttp.result', $row->id) }}" class="btn btn-warning btn-sm">HASIL UJI</a>
                                    @endif
                                    @if($row->file_skhp!==null)
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                    <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                    @endif
                                @elseif($row->stat_service_order=="2")
                                    <a href="{{ route('serviceuttp.result', $row->id) }}" class="btn btn-warning btn-sm">HASIL UJI</a>
                                    @if($row->file_skhp!==null)
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                    <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                                    @endif
                                @elseif($row->stat_service_order=="4")
                                    @if (!$row->is_skhpt)
                                    <a href="{{ route('serviceuttp.print', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                                    <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                                    @endif
                                    @if($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7)
                                    <a href="{{ route('serviceuttp.printTipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SKHPT</a>
                                    <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHPT</a>
                                    @endif
                                    @if($row->ujitipe_completed==true || $row->has_set)
                                    <a href="{{ route('serviceuttp.printSETipe', ['id' => $row->id, 'stream' => 1]) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                                    <a href="{{ route('serviceuttp.printSETipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                                    @endif
                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('.data-table').DataTable({
        scrollX: true,
    });

    $(".no_order").change(function() {
        var no_order = $(this).val();

        var form = $(this).closest("form");
        form[0].submit();

        /*
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{ route('orderuttp.proses') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
        */
    });

    // TABS
    $('#tabs a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });

    var hash = window.location.hash;
    $('#tabs a[href="' + hash + '"]').tab('show');
    // END TABS
});

</script>
@endsection