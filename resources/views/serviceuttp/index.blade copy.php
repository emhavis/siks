@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-12">
                    <form id="qr"  >
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="no_order">Selesai Uji Berdasar QR Code</label>
                                    {!! Form::text('no_order', null, ['class' => 'form-control','id' => 'no_order', 'autofocus' => 'autofocus']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="error_notes" clsas=""></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/>
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Diterima Oleh</th>
                        <th>Diuji Oleh</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th>Berkas</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequestItem->no_order }}</td>
                        <td><a onclick="showToolCodeInfo('{{ $row->ServiceRequestItem->uttp->id }}')" href="#">
                            {{ $row->ServiceRequestItem->uttp->tool_brand }}/{{ $row->ServiceRequestItem->uttp->tool_model }}/{{ $row->ServiceRequestItem->uttp->tool_type }} ({{ $row->ServiceRequestItem->uttp->serial_no ? $row->ServiceRequestItem->uttp->serial_no : '-' }})
                        </a></td> 
                        <td>{{ $row->MasterUsers->full_name }}</td>
                        <td>{{ $row->TestBy1 ? $row->TestBy1->full_name : "" }} {{ $row->TestBy2 ? ' & '.$row->TestBy2->full_name : "" }}</td>
                        <td>{{ $row->staff_entry_datein }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>
                            <?php if ($row->ServiceRequest->service_type_id == 4): ?>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/type_approval_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Persetujuan Tipe</a>
                            <?php elseif ($row->ServiceRequest->service_type_id == 5): ?>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Sertifikat Sebelumnya</a>
                            <?php elseif ($row->ServiceRequest->service_type_id == 6 || $row->ServiceRequest->service_type_id == 7): ?>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Surat Permohonan</a>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/calibration_manual/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Manual Kalibrasi</a>
                            <a href="<?= config('app.siks_url') ?>/tracking/download/manual_book/{{ $row->ServiceRequestItem->id }}" class="btn btn-info btn-sm">Buku Manual</a>
                            <?php endif ?>
                        </td>
                        <td>
                        @if($row->stat_service_order=="0")
                            @if($row->pending_status=="0" || $row->pending_status==null)
                            <a href="{{ route('serviceuttp.test', $row->id) }}" class="btn btn-warning btn-sm">SELESAI UJI</a>
                            <a href="{{ route('serviceuttp.pending', $row->id) }}" class="btn btn-danger btn-sm">TUNDA</a>
                            @else
                            <a href="{{ route('serviceuttp.continue', $row->id) }}" class="btn btn-info btn-sm">LANJUT UJI</a>
                            @endif
                            <a href="{{ route('serviceuttp.cancel', $row->id) }}" class="btn btn-danger btn-sm">BATAL UJI</a>
                        @elseif($row->stat_service_order=="1")
                            @if($row->stat_sertifikat==0)
                            <a href="{{ route('serviceuttp.result', $row->id) }}" class="btn btn-warning btn-sm">HASIL UJI</a>
                            @endif
                            @if($row->file_skhp!==null)
                            <a href="{{ route('serviceuttp.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuttp.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                            <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                            @endif
                            <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                            @endif
                            @if($row->stat_warehouse=="0" || $row->stat_warehouse == null)
                            <a href="{{ route('serviceuttp.warehouse', $row->id) }}" class="btn btn-warning btn-sm">GUDANG ALAT SELESAI</a>
                            @endif
                        @elseif($row->stat_service_order=="2")
                            <a href="{{ route('serviceuttp.result', $row->id) }}" class="btn btn-warning btn-sm">HASIL UJI</a>
                            @if($row->file_skhp!==null)
                            <a href="{{ route('serviceuttp.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuttp.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                            <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                            @endif
                            <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                            @endif
                            @if($row->stat_warehouse=="0" || $row->stat_warehouse == null)
                            <a href="{{ route('serviceuttp.warehouse', $row->id) }}" class="btn btn-warning btn-sm">GUDANG ALAT SELESAI</a>
                            @endif
                        @elseif($row->stat_service_order=="4")
                            <a href="{{ route('serviceuttp.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuttp.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                            <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                            @endif
                        @elseif($row->is_finish=="0")
                            @if($row->file_skhp!==null)
                            <a href="{{ route('serviceuttp.preview', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SKHP</a>
                            <a href="{{ route('serviceuttp.print', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SKHP</a>
                            @if($row->ujitipe_completed==true)
                            <a href="{{ route('serviceuttp.previewTipe', $row->id) }}" class="btn btn-warning btn-sm">LIHAT SERTIFIKAT EVALUASI TIPE</a>
                            <a href="{{ route('serviceuttp.printTipe', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD SERTIFIKAT EVALUASI TIPE</a>
                            @endif
                            <a href="{{ route('serviceuttp.download', $row->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                            @endif
                            @if($row->stat_warehouse=="0" || $row->stat_warehouse == null)
                            <a href="{{ route('serviceuttp.warehouse', $row->id) }}" class="btn btn-warning btn-sm">GUDANG ALAT SELESAI</a>
                            @endif
                        @elseif($row->is_finish=="1")
                        SELESAI
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <small class="stat-label">Jenis Besaran</small>
                        <h4 class="m-t-xs standard_measurement_type_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Jenis Alat</small>
                        <h4 class="m-t-xs standard_tool_type_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Rincian Alat</small>
                        <h4 class="m-t-xs standard_detail_type_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Merk / Buatan</small>
                        <h4 class="m-t-xs brand-made_in"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Model / Tipe</small>
                        <h4 class="m-t-xs model-tipe"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">No. Seri / No. Identitas</small>
                        <h4 class="m-t-xs no_seri-no_identitas"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Kapasitas / Daya Baca</small>
                        <h4 class="m-t-xs capacity-daya_baca"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Kelas</small>
                        <h4 class="m-t-xs kelas"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Sumber</small>
                        <h4 class="m-t-xs sumber_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Jumlah Per Set</small>
                        <h4 class="m-t-xs jumlah_per_set"></h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('#data_table').DataTable();

    $("#no_order").change(function() {
        var no_order = $(this).val();

        var form = $('#qr');
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{ route('orderuttp.proses') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
    });
});

function showToolCodeInfo(toolCode)
{
    $.post("{{ route('standard.tool_code') }}",{"tool_code":toolCode},function(response){
    
    var res = response[0];
    $("#myModal h4.modal-title").text(res.tool_code);
    $("#myModal h4.standard_measurement_type_id").text(res.standard_type);
    $("#myModal h4.standard_tool_type_id").text(res.attribute_name);
    $("#myModal h4.standard_detail_type_id").text(res.standard_detail_type_name);
    $("#myModal h4.brand-made_in").text(res.brand+" / "+res.nama_negara);
    $("#myModal h4.model-tipe").text(res.model+" / "+res.tipe);
    $("#myModal h4.no_seri-no_identitas").text(res.no_seri+" / "+res.no_identitas);
    $("#myModal h4.capacity-daya_baca").text(res.capacity+" / "+res.daya_baca);
    $("#myModal h4.kelas").text(res.class);
    $("#myModal h4.sumber_id").text(res.nama_sumber);
    $("#myModal h4.jumlah_per_set").text(res.jumlah_per_set);
    $("#myModal").modal();

    },"json");
}
</script>
@endsection