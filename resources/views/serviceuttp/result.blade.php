@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        @if($serviceOrder->subkoordinator_notes != null)
        <div class="alert alert-info" role="alert">
            {{ $serviceOrder->subkoordinator_notes }}
        </div>
        @endif

        {!! Form::open(['url' => route('serviceuttp.resultupload', $serviceOrder->id), 'files' => true, 'id' => 'form_result'])  !!}
        
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_uttp">Jenis UTTP</label>
                    {!! Form::text('jenis_uttp', 
                        $serviceOrder->uttp != null ? 
                        $serviceOrder->uttp->type->uttp_type :
                        $serviceOrder->ServiceRequestItem->uttp->type->uttp_type, ['class' => 'form-control','id' => 'jenis_uttp', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas Maksimum</label>
                    {!! Form::text('tool_capacity', $serviceOrder->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity' ]) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas Minimum</label>
                    {!! Form::text('tool_capacity_min', $serviceOrder->tool_capacity_min, ['class' => 'form-control','id' => 'tool_capacity_min']) !!} 
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Satuan Kapasitas</label>
                    {!! Form::select('tool_capacity_unit', $units, $serviceOrder->tool_capacity_unit, ['class' => 'form-control','id' => 'tool_capacity_unit']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Merek</label>
                    {!! Form::text('tool_brand', $serviceOrder->tool_brand, ['class' => 'form-control','id' => 'tool_brand' ]) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Model/Tipe</label>
                    {!! Form::text('tool_model', $serviceOrder->tool_model, ['class' => 'form-control','id' => 'tool_model']) !!} 
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>No Seri</label>
                    {!! Form::text('tool_serial_no', $serviceOrder->tool_serial_no, ['class' => 'form-control','id' => 'tool_serial_no']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Media Uji/Komoditas</label>
                    {!! Form::text('tool_media', $serviceOrder->tool_media, ['class' => 'form-control','id' => 'tool_media']) !!} 
                </div>
            </div>
        </div>
        @if($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Kadar Air')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Metode Pengukuran</label>
                    {!! Form::select('tool_media_pengukuran', ['Kapasitansi'=>'Kapasitansi','Resistansi'=>'Resistansi'], 
                        $serviceOrder->tool_media_pengukuran, 
                        ['class' => 'form-control select2','id' => 'tool_media_pengukuran', 'style' => 'width:100%']) !!}
                </div>
            </div>
        </div>
        @endif
        <!--
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Merek/Model/Tipe/Nomor Seri</label>
                    {!! Form::text('merek', 
                        $serviceOrder->tool_brand != null && $serviceOrder->tool_model != null && $serviceOrder->tool_serial_no != null ?
                        $serviceOrder->tool_brand . '/' . 
                        $serviceOrder->tool_model . '/' .
                        $serviceOrder->tool_serial_no : 
                        $serviceOrder->ServiceRequestItem->uttp->tool_brand . '/' . 
                        $serviceOrder->ServiceRequestItem->uttp->tool_model . '/' .
                        $serviceOrder->ServiceRequestItem->uttp->serial_no,
                        ['class' => 'form-control','id' => 'merek', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->tool_made_in != null ? 
                        $serviceOrder->tool_made_in : 
                        $serviceOrder->ServiceRequestItem->uttp->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan', 'readonly']) !!}
                </div>
            </div>
        </div>
        -->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tool_made_in_id">Buatan</label>
                    {!! Form::select('tool_made_in_id', $negara, $serviceOrder->tool_made_in_id, ['class' => 'form-control','id' => 'tool_made_in_id']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory">Nama Pabrikan</label>
                    {!! Form::text('tool_factory', 
                        $serviceOrder->tool_factory,
                        ['class' => 'form-control','id' => 'tool_factory']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory_addr">Alamat Pabrikan</label>
                    {!! Form::text('tool_factory_address', 
                        $serviceOrder->tool_factory_address,
                        ['class' => 'form-control','id' => 'tool_factory_address']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Dasar Pemeriksaan/Pengujian/Verifikasi, Nomor</label>
                    {!! Form::text('reference_no', 
                        $serviceOrder->ServiceRequestItem->reference_no,
                        ['class' => 'form-control','id' => 'reference_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_date">Dasar Pemeriksaan/Pengujian/Verifikasi, Tanggal</label>
                    {!! Form::text('reference_date', 
                        date("d-m-Y", strtotime(isset($serviceOrder->ServiceRequestItem->reference_date) ? $serviceOrder->ServiceRequestItem->reference_date : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'reference_date', 'readonly']) !!}
                </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
        <div class="row">
            <div class="col-md-12">
                <label for="dasar_pemeriksaan">Dasar Pemeriksaan/Pengujian/Verifikasi Lainnya (jika ada)</label>
                <textarea name="dasar_pemeriksaan" id="dasar_pemeriksaan"
                        class="form-control">{!! $serviceOrder->dasar_pemeriksaan !!}</textarea>
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_1">Diuji Oleh (1)</label>
                    {!! Form::text('test_by_1', 
                        $user->full_name,
                        ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="test_by_2">Diuji Oleh (2)</label>
                    {!! Form::select('test_by_2', $users, $serviceOrder->test_by_2, 
                        ['class' => 'form-control', 'id' => 'test_by_2']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="mulai_uji">Waktu Pengujian, Mulai
                        @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
                        (klik untuk mengubah tanggal)
                        @endif
                    </label>
                    {!! Form::text('mulai_uji', 
                        date("d-m-Y", strtotime(isset($serviceOrder->mulai_uji) ? $serviceOrder->mulai_uji : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'mulai_uji', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="selesai_uji">Waktu Pengujian, Selesai</label>
                    {!! Form::text('selesai_uji', 
                        date("d-m-Y", strtotime(isset($serviceOrder->selesai_uji) ? $serviceOrder->selesai_uji : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'selesai_uji', 'readonly']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Item Pengujian/Pemeriksaan</label>
                    <?php
                        $inspections = [];
                        foreach($serviceOrder->ServiceRequestItem->inspections as $inspection) {
                            $inspections[] = $inspection->inspectionPrice->inspection_type;
                        }
                    ?>
                    <textarea name="inspections" id="inspections" readonly
                        class="form-control">{!! implode("\n", $inspections) !!}</textarea>
                </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
        
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseDataTipe" aria-expanded="false" aria-controls="collapseExample">
                    Data Hasil Uji &nbsp;&nbsp;<i class="fa fa-angle-down"></i>
                </button>
                <label>&nbsp;&nbsp;Diisi sesuai dengaan kebutuhan masing-masing Jenis UTTP</label>
            </div>
        </div>
        <br/>

        <div id="collapseDataTipe">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kelas Keakurasian</label>
                    {!! Form::text('kelas_keakurasian', $serviceOrder->kelas_keakurasian, 
                        ['class' => 'form-control data-tipe','id' => 'kelas_keakurasian']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Interval Skala Verifikasi</label>
                    {!! Form::text('interval_skala_verifikasi', $serviceOrder->interval_skala_verifikasi, 
                        ['class' => 'form-control data-tipe','id' => 'interval_skala_verifikasi']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Interval Skala Terkecil</label>
                    {!! Form::text('daya_baca', $serviceOrder->daya_baca, 
                        ['class' => 'form-control data-tipe','id' => 'daya_baca']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kelas Massa Kendaraan</label>
                    {!! Form::text('kelas_massa_kendaraan', $serviceOrder->kelas_massa_kendaraan, 
                        ['class' => 'form-control data-tipe','id' => 'kelas_massa_kendaraan']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kecepatan</label>
                    {!! Form::text('kecepatan', $serviceOrder->kecepatan, 
                        ['class' => 'form-control data-tipe','id' => 'kecepatan']) !!} 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kelas Muatan Sumbu Tunggal</label>
                    {!! Form::text('kelas_single_axle_load', $serviceOrder->kelas_single_axle_load, 
                        ['class' => 'form-control data-tipe','id' => 'kelas_single_axle_load']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kelas Muatan Kelompok Sumbu</label>
                    {!! Form::text('kelas_single_group_load', $serviceOrder->kelas_single_group_load, 
                        ['class' => 'form-control data-tipe','id' => 'kelas_single_group_load']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Metode Pengukuran</label>
                    {!! Form::text('metode_pengukuran', $serviceOrder->metode_pengukuran, 
                        ['class' => 'form-control data-tipe','id' => 'metode_pengukuran']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Sistem Jaringan</label>
                    {!! Form::text('sistem_jaringan', $serviceOrder->sistem_jaringan, 
                        ['class' => 'form-control data-tipe','id' => 'sistem_jaringan']) !!} 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Konstanta</label>
                    {!! Form::text('konstanta', $serviceOrder->konstanta, 
                        ['class' => 'form-control data-tipe','id' => 'konstanta']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kelas/Rentang Temperatur</label>
                    {!! Form::text('kelas_temperatur', $serviceOrder->kelas_temperatur, 
                        ['class' => 'form-control data-tipe','id' => 'kelas_temperatur']) !!} 
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Rasio Q<sub>3</sub>/Q<sub>1</sub></label>
                    {!! Form::text('rasio_q', $serviceOrder->rasio_q, 
                        ['class' => 'form-control data-tipe','id' => 'rasio_q']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Diameter Nominal/Standar Flange</label>
                    {!! Form::text('diameter_nominal', $serviceOrder->diameter_nominal, 
                        ['class' => 'form-control data-tipe','id' => 'diameter_nominal']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jumlah Nozzle</label>
                    {!! Form::text('jumlah_nozzle', $serviceOrder->jumlah_nozzle, 
                        ['class' => 'form-control data-tipe','id' => 'jumlah_nozzle']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jenis Pompa</label>
                    {!! Form::text('jenis_pompa', $serviceOrder->jenis_pompa, 
                        ['class' => 'form-control data-tipe','id' => 'jenis_pompa']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Volume Bersih</label>
                    {!! Form::text('volume_bersih', $serviceOrder->volume_bersih, 
                        ['class' => 'form-control data-tipe','id' => 'volume_bersih']) !!} 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Diameter Tangki</label>
                    {!! Form::text('diameter_tangki', $serviceOrder->diameter_tangki, 
                        ['class' => 'form-control data-tipe','id' => 'diameter_tangki']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tinggi Tangki</label>
                    {!! Form::text('tinggi_tangki', $serviceOrder->tinggi_tangki, 
                        ['class' => 'form-control data-tipe','id' => 'tinggi_tangki']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Daya Baca</label>
                    {!! Form::text('meter_daya_baca', $serviceOrder->meter_daya_baca, 
                        ['class' => 'form-control data-tipe','id' => 'meter_daya_baca']) !!} 
                </div>
            </div>
        </div>
        </div>

        @elseif($serviceOrder->ServiceRequest->service_type_id == 4 || $serviceOrder->ServiceRequest->service_type_id == 5)
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sertifikat_expired_at">Masa Berlaku (klik untuk mengubah)</label>
                    {!! Form::text('sertifikat_expired_at', 
                        date("d-m-Y", strtotime(isset($serviceOrder->sertifikat_expired_at) ? $serviceOrder->sertifikat_expired_at : $sertifikat_expired_at)),
                        ['class' => 'form-control','id' => 'sertifikat_expired_at', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="hasil_uji_memenuhi">Hasil Uji *</label>
                    {!! Form::select('hasil_uji_memenuhi', ['' => '', 'sah'=>'Sah','batal'=>'Batal'], 
                        ($serviceOrder->hasil_uji_memenuhi != null ? ($serviceOrder->hasil_uji_memenuhi ? 'sah' : 'batal') : ''), 
                        ['class' => 'form-control select2','id' => 'hasil_uji_memenuhi', 'style' => 'width:100%']) !!}
                    
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tanda_pegawai_berhak">Tanda Pegawai Berhak</label>
                    {!! Form::text('tanda_pegawai_berhak', $serviceOrder->tanda_pegawai_berhak,
                        ['class' => 'form-control','id' => 'tanda_pegawai_berhak']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tanda_daerah">Tanda Daerah</label>
                    {!! Form::text('tanda_daerah', $serviceOrder->tanda_daerah,
                        ['class' => 'form-control','id' => 'tanda_daerah']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tanda_sah">Tanda Sah</label>
                    {!! Form::text('tanda_sah', $serviceOrder->tanda_sah,
                        ['class' => 'form-control','id' => 'tanda_sah']) !!}
                </div>
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->lokasi_pengujian == 'dalam')
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Lokasi Alat</label>
                    {!! Form::text('location', $serviceOrder->location, ['class' => 'form-control','id' => 'location']) !!} 
                </div>
            </div>
        </div>
        @endif

        @if($serviceOrder->ServiceRequest->lokasi_pengujian == 'luar' && $serviceOrder->ServiceRequestItem->location != null)
        <!--
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Lokasi Penempatan</label>
                    {!! Form::text('location', $serviceOrder->ServiceRequestItem->location, ['class' => 'form-control','id' => 'location', 'readonly']) !!} 
                </div>
            </div>
        </div>
        -->
        @if(count($serviceOrder->ttuPerlengkapans) > 0)
        <!--
        <h5>Perlengkapan</h5>
        <div class="row">
            <div class="col-md-12">
                <table id="perlengkapan" class="table table-responsive-sm input-table">
                    <thead>
                        <tr>
                            <th>Jenis</th>
                            <th>Merek</th>
                            <th>Model/Tipe</th>
                            <th>Nomor Seri</th>
                            <th>Keterangan/Penyegelan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($serviceOrder->ttuPerlengkapans as $perlengkapan)
                        <tr>
                            <td>{{ $perlengkapan->uttp->type->uttp_type }}</td>
                            <td>{{ $perlengkapan->uttp->tool_brand }}</td>
                            <td>{{ $perlengkapan->uttp->tool_model }}</td>
                            <td>{{ $perlengkapan->uttp->serial_no }}</td>
                            <td>
                                <input type="hidden" name="perlengkapan_id[]" />
                                <input type="hidden" name="perlengkapan_uttp_id[]" value="{{ $perlengkapan->uttp_id }}"/>
                                <input type="text" class="form-control" name="perlangkapan_keterangan[]" 
                                    value="{{ $perlengkapan->keterangan }}"/>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        -->
        @endif
        @endif
        
        @if($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge' || 
            $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice'
        )
        <table id="data_table_ttu_alg_badanhitung" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr colspan="3">Identitas Badan Hitung</tr>
                <tr>
                    <th>Tipe</th>
                    <th>No Seri</th>
                    <th>
                        <button type="button" id="btn_add_badanhitung" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($badanHitungs as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="type[]" value="{{ $ttuItem->type }}"/></td>
                    <td><input type="text" class="form-control" name="serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="badanhitung_item_id[]" />
                        <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' ||
            $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus Media Air')
        <table id="data_table_ttu_mabbm_badanhitung" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr colspan="4">Identitas Badan Hitung</tr>
                <tr>
                    <th>Merek</th>
                    <th>Tipe</th>
                    <th>No Seri</th>
                    <th>
                        <button type="button" id="btn_add_badanhitung" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($badanHitungs as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="brand[]" value="{{ $ttuItem->brand }}"/></td>
                    <td><input type="text" class="form-control" name="type[]" value="{{ $ttuItem->type }}"/></td>
                    <td><input type="text" class="form-control" name="serial_no[]" value="{{ $ttuItem->serial_no }}"/></td> 
                    <td>
                        <input type="hidden" name="badanhitung_item_id[]" />
                        <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endif

        @if($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tank_no">Nomor Tangki</label>
                    {!! Form::text('tank_no', 
                        $ttu ? $ttu->tank_no : '',
                        ['class' => 'form-control','id' => 'tank_no']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tag_no">Nomor Tag</label>
                    {!! Form::text('tag_no', 
                        $ttu ? $ttu->tag_no : '',
                        ['class' => 'form-control','id' => 'tag_no']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Air' ||
            $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBM'
        )
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="totalisator">Totalisator</label>
                    {!! Form::text('totalisator', 
                        $ttu ? $ttu->totalisator : '',
                        ['class' => 'form-control','id' => 'totalisator']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice') 
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="suhu_pengujian">Suhu Pengujian (&#8451;)</label>
                    {!! Form::number('suhu_pengujian', 
                        $ttu ? $ttu->suhu_pengujian : '',
                        ['class' => 'form-control','id' => 'suhu_pengujian']) !!}
                </div>
            </div>
        </div> 
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="ketidakpastian">Ketidakpastian U<sub>95%</sub> (mm)</label>
                    {!! Form::number('ketidakpastian', 
                        $ttu ? $ttu->ketidakpastian : '',
                        ['class' => 'form-control','id' => 'ketidakpastian']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kfactor">Nilai K</label>
                    {!! Form::text('kfactor', 
                        $ttu ? $ttu->kfactor : '',
                        ['class' => 'form-control','id' => 'kfactor']) !!}
                </div>
            </div>
        </div> 
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' || 
                $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus Media Air')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="suhu_pengujian">Suhu Pengujian (&#8451;)</label>
                    {!! Form::number('suhu_pengujian', 
                        $ttu ? $ttu->suhu_pengujian : '',
                        ['class' => 'form-control','id' => 'suhu_pengujian']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kfactor">K Faktor</label>
                    {!! Form::text('kfactor', 
                        $ttu ? $ttu->kfactor : '',
                        ['class' => 'form-control','id' => 'kfactor']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' ||
            $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)' ||
            $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' ||
            $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG'
        )
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="totalisator">Totalisator</label>
                    {!! Form::text('totalisator', 
                        $ttu ? $ttu->totalisator : '',
                        ['class' => 'form-control','id' => 'totalisator']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kfactor">K Faktor</label>
                    {!! Form::text('kfactor', 
                        $ttu ? $ttu->kfactor : '',
                        ['class' => 'form-control','id' => 'kfactor']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Orifice') 
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="line_bore_size">Line Bore Size</label>
                    {!! Form::text('line_bore_size', 
                        $ttu ? $ttu->line_bore_size : '',
                        ['class' => 'form-control','id' => 'line_bore_size']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter' ||
        $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Ultrasonik') 
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="line_bore_size">Line Bore Size</label>
                    {!! Form::text('line_bore_size', 
                        $ttu ? $ttu->line_bore_size : '',
                        ['class' => 'form-control','id' => 'line_bore_size']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kfactor">Nilai K</label>
                    {!! Form::text('kfactor', 
                        $ttu ? $ttu->kfactor : '',
                        ['class' => 'form-control','id' => 'kfactor']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Tetap Silinder Tegak') 
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tinggi_tangki">Tinggi Tangki</label>
                    {!! Form::text('tinggi_tangki', 
                        $ttu ? $ttu->tinggi_tangki : '',
                        ['class' => 'form-control','id' => 'tinggi_tangki']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="diameter">Diameter</label>
                    {!! Form::text('diameter', 
                        $ttu ? $ttu->diameter : '',
                        ['class' => 'form-control','id' => 'diameter']) !!}
                </div>
            </div>
        </div>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jaringan_listrik">Jaringan Listrik</label>
                    {!! Form::text('jaringan_listrik', 
                        $ttu ? $ttu->jaringan_listrik : '',
                        ['class' => 'form-control','id' => 'jaringan_listrik']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="konstanta">Konstanta</label>
                    {!! Form::text('konstanta', 
                        $ttu ? $ttu->konstanta : '',
                        ['class' => 'form-control','id' => 'konstanta']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kelas">Kelas</label>
                    {!! Form::text('kelas', 
                        $ttu ? $ttu->kelas : '',
                        ['class' => 'form-control','id' => 'kelas']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kondisi_ruangan">Kondisi Ruangan</label>
                    {!! Form::text('kondisi_ruangan', 
                        $ttu ? $ttu->kondisi_ruangan : '',
                        ['class' => 'form-control','id' => 'kondisi_ruangan']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="trapo_ukur">Trapo Ukur (CT; PT)</label>
                    {!! Form::text('trapo_ukur', 
                        $ttu ? $ttu->trapo_ukur : '',
                        ['class' => 'form-control','id' => 'trapo_ukur']) !!}
                </div>
            </div>
        </div>
        @endif

        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="persyaratan_teknis">Persyaratan Teknis</label>
                    {!! Form::hidden('persyaratan_teknis_id', 
                        $serviceOrder->ServiceRequestItem->uttp->type->oiml_id,
                        ['class' => 'form-control','id' => 'persyaratan_teknis_id', 'readonly']) !!}
                    <textarea name="persyaratan_teknis_id_str" id="persyaratan_teknis_id_str" readonly
                        class="form-control">{!! $serviceOrder->ServiceRequestItem->uttp->type->syarat_teknis !!}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="file_skhp">Lampiran Evaluasi *</label>
                    {!! Form::file('file_skhp', 
                        ['class' => 'form-control','id' => 'file_skhp', 'accept'=> 'application/pdf,image/png, image/jpeg']) !!}
                </div>
            </div>
            <div class="cal-md-6">
                @if($serviceOrder->path_skhp !=null)
                <div class="input-group">
                    <label style="padding-top:30px"></label>
                    <a href="{{ route('serviceuttp.download', $serviceOrder->id) }}" class="btn btn-warning btn-sm">DOWNLOAD LAMPIRAN</a>
                    <label>&nbsp; Nama File:  {{$serviceOrder->file_skhp}}</label>
                </div>
                @endif
            </div>
        </div>

        @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
        <table id="data_table" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Pemeriksaan/Pengujian</th>
                    <th>Pemenuhan Persyaratan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($inspectionItems as $item)
                @if($item->is_accepted == 'ya')
                <tr style="background-color: #449d44; color: white;">
                @elseif($item->is_accepted == 'ya')
                <tr style="background-color: #db524b; color: white;">
                @else
                <tr>
                @endif
                    <td>{{ $item->inspectionItem->name }}</td>
                    <td>
                    @if($item->inspectionItem->is_tested)
                    {!! Form::select('is_accepted_'.$item->id, ['ya'=>'Ya','tidak'=>'Tidak', ''=>'N/A'], 
                        isset($item->is_accepted) ? ($item->is_accepted ? 'ya' : 'tidak') : '', 
                        ['class' => 'form-control is_accepted_select','id' => 'is_accepted_'.$item->id, 'style' => 'width:100%']) !!}
                    @endif
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="catatan_hasil_pemeriksaan">Catatan Hasil Pemeriksaan (akan dicantumkan di dalam SKHPT)</label>
                    <textarea name="catatan_hasil_pemeriksaan" id="catatan_hasil_pemeriksaan"
                        class="form-control">{!! $serviceOrder->catatan_hasil_pemeriksaan !!}</textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="has_set">Memerlukan Sertifikat Evaluasi Tipe</label>
                    <input type="checkbox" name="has_set" value="has_set" id="has_set" {{ $serviceOrder->has_set ? 'checked' : '' }} />
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="set_memenuhi">Hasil Sertifikat Evaluasi Tipe</label>
                    {!! Form::select('set_memenuhi', ['memenuhi'=>'Memenuhi','tolak'=>'Tidak Memenuhi (Ditolak)'], $serviceOrder->set_memenuhi ? 'memenuhi' : 'tolak', 
                        ['class' => 'form-control select2','id' => 'set_memenuhi', 'style' => 'width:100%']) !!}
                    
                </div>
            </div>
        </div> 

        @elseif($serviceOrder->ServiceRequest->service_type_id == 4 || $serviceOrder->ServiceRequest->service_type_id == 5)

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="catatan_hasil_pemeriksaan">Catatan (diisi jika batal atau jika terdapat catatan atas hasil pemeriksaan)</label>
                    <textarea name="catatan_hasil_pemeriksaan" id="catatan_hasil_pemeriksaan"
                        class="form-control">{!! $serviceOrder->catatan_hasil_pemeriksaan !!}</textarea>
                </div>
            </div>
        </div>
        
        @if($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge')
        <table id="data_table_ttu_alg" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th rowspan="2">Input Level (mm)</th>
                    <th colspan="2">Kesalahan Penunjukan</th>
                    <th rowspan="2">Kesalahan Histerisis (mm)</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>Naik (mm)</th>
                    <th>Turun (mm)<th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="input_level[]" value="{{ $ttuItem->input_level }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error_up[]" value="{{ $ttuItem->error_up }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="error_down[]" value="{{ $ttuItem->error_down }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="error_hysteresis[]" value="{{ $ttuItem->error_hysteresis }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' ||
            $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus Media Air' ||
            $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter')
        <table id="data_table_ttu_mabbm" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Kecepatan Alir (L/menit)</th>
                    <th>Kesalahan (%) BKD: &#177;0,5%</th>
                    <th>Meter Factor BKD: 1&#177;0,005</th>
                    <th>Kemampuan Ulang BKD: &#177;0,1%</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="meter_factor[]" value="{{ $ttuItem->meter_factor }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatability[]" value="{{ $ttuItem->repeatability }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' || 
        $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG')
        <table id="data_table_ttu_mg" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Kecepatan Alir (m3/h)</th>
                    <th>Kesalahan (%)</th>
                    <th>Kemampuan Ulang %</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatability[]" value="{{ $ttuItem->repeatability }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Air')
        <table id="data_table_ttu_ma" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Kecepatan Alir (L/menit)</th>
                    <th>Kesalahan (%) BKD: &plusmn;0,5%</th>
                    <th>Kemampuan Ulang BKD: &plusmn;0,1%</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatability[]" value="{{ $ttuItem->repeatability }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Kadar Air')
        <table id="data_table_ttu_mka" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Komoditas</th>
                    <th>Penunjukan Alat Rata-Rata (%)</th>
                    <th>Kesalahan (%)</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="text" class="form-control" name="commodity[]" value="{{ $ttuItem->commodity }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="penunjukan[]" value="{{ $ttuItem->penunjukan }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)')
        <table id="data_table_ttu_mguwc" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th rowspan="2">Kecepatan Alir (m3/h)</th>
                    <th colspan="2">Kesalahan</th>
                    <th colspan="2">Kemampuan Ulang</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>Nilai (%)</th>
                    <th>BKD (%)</th>
                    <th>Nilai</th>
                    <th>BKD</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="flow_rate[]" value="{{ $ttuItem->flow_rate }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="error_bkd[]" value="{{ $ttuItem->error_bkd }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="repeatability[]" value="{{ $ttuItem->repeatability }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="repeatability_bkd[]" value="{{ $ttuItem->repeatability_bkd }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Depth Tape')
        <table id="data_table_ttu_dt" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Rentang Ukur (mm)</th>
                    <th>Suhu Dasar (&#8451;)</th>
                    <th>Panjang Sebenarnya (mm)</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="rentang_ukur[]" value="{{ $ttuItem->rentang_ukur }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="suhu_dasar[]" value="{{ $ttuItem->suhu_dasar }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="panjang_sebenarnya[]" value="{{ $ttuItem->panjang_sebenarnya }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'UTI Meter')
        <table id="data_table_ttu_uti" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th>Rentang Ukur (mm)</th>
                    <th>Temperatur Dasar (&#8451;)</th>
                    <th>Panjang Sebenarnya (mm)</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="rentang_ukur[]" value="{{ $ttuItem->rentang_ukur }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="suhu_dasar[]" value="{{ $ttuItem->suhu_dasar }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="panjang_sebenarnya[]" value="{{ $ttuItem->panjang_sebenarnya }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <br/>
        <table id="data_table_ttu_uti_suhu" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="3">Temperatur</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>Penunjukan UTTP (&#8451;)</th>
                    <th>Penunjukan Standar (&#8451;)</th>
                    <th>Koreksi (&#8451;)</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuSuhu as $suhu)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="penunjukan[]" value="{{ $suhu->penunjukan }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="penunjukan_standar[]" value="{{ $suhu->penunjukan_standar }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="koreksi[]" value="{{ $suhu->koreksi }}"/></td>
                    <td>
                        <input type="hidden" name="suhu_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice')
        <table id="data_table_ttu_po" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="9">Ukuran</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>d (mm)</th>
                    <th>D (mm)</th>
                    <th>e (mm)</th>
                    <th>E (mm)</th>
                    <th>&alpha;</th>
                    <th>Roundness (mm)</th>
                    <th>Flatness (mm)</th>
                    <th>Roughness (&#181;m)</th>
                    <th>Eksentrisitas (mm)</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="d_inner[]" value="{{ $ttuItem->d_inner }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="d_outer[]" value="{{ $ttuItem->d_outer }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="e_inner[]" value="{{ $ttuItem->e_inner }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="e_outer[]" value="{{ $ttuItem->e_outer }}"/></td>
                    <td><input type="text" class="form-control" name="alpha[]" value="{{ $ttuItem->alpha }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="roundness[]" value="{{ $ttuItem->roundness }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="flatness[]" value="{{ $ttuItem->flatness }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="roughness[]" value="{{ $ttuItem->roughness }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="eksentrisitas[]" value="{{ $ttuItem->eksentrisitas }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBM')
        <table id="data_table_ttu_pubbm" class="table table-striped table-hover table-responsive-sm">
            <thead>
                
                <tr>
                    <th>Merek</th>
                    <th>Tipe</th>
                    <th>Nomor Seri</th>
                    <th>Debit Maks (L/min)</th>
                    <th>Media</th>
                    <th>Jumlah Nozzle</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="text" step="any" class="form-control" name="tool_brand[]" value="{{ $ttuItem->tool_brand }}"/></td>
                    <td><input type="text" step="any" class="form-control" name="tool_type[]" value="{{ $ttuItem->tool_type }}"/></td> 
                    <td><input type="text" step="any" class="form-control" name="tool_serial_no[]" value="{{ $ttuItem->tool_serial_no }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="debit_max[]" value="{{ $ttuItem->debit_max }}"/></td>
                    <td><input type="text" step="any" class="form-control" name="tool_media[]" value="{{ $ttuItem->tool_media }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="nozzle_count[]" value="{{ $ttuItem->nozzle_count }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' ||
        $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter' ||
        $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter' 
        )
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="satuan_output">Satuan Output/Aktual</label>
                    {!! Form::select('satuan_output', [
                            'mA' => 'mA',
                            'ohm' => 'ohm',
                            '&deg;C'=>'&deg;C',
                            '&deg;F'=>'&deg;F', 
                            'K'=>'K',
                            'kg/cm2' => 'kg/cm2',
                            'mmH2O' => 'mmH2O',
                            'inH2O' => 'inH2O',
                            'bar' => 'bar'
                            ], 
                        $serviceOrder->satuan_output, 
                        ['class' => 'form-control', 'id' => 'satuan_output']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="satuan_error">Satuan Error</label>
                    {!! Form::select('satuan_error', [
                            '%' => '%',
                            'ohm' => 'ohm',
                            '&deg;C'=>'&deg;C',
                            '&deg;F'=>'&deg;F'], 
                        $serviceOrder->satuan_output, 
                        ['class' => 'form-control', 'id' => 'satuan_error']) !!}
                </div>  
            </div>
        </div>
        <table id="data_table_ttu_pt" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="2">Input</th>
                    <th rowspan="2">Aktual</th>
                    <th colspan="2">Output</th>
                    <th colspan="2">Error</th>
                    <th rowspan="2">
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
                <tr>
                    <th>%</th>
                    @if($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter')
                    <th>kg/cm<sup>2</sup></th>
                    @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter')
                    <th>mmH<sub>2</sub>O</th>
                    @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter')
                    <th>
                        {!! Form::select('satuan_suhu', [
                            'ohm' => 'ohm',
                            '&deg;C'=>'&deg;C',
                            '&deg;F'=>'&deg;F', 
                            'K'=>'K'], 
                        $serviceOrder->satuan_suhu, 
                        ['class' => 'form-control', 'id' => 'satuan_suhu']) !!}
                    </th>
                    @endif
                    <th>Up</th>
                    <th>Down</th>
                    <th>Up</th>
                    <th>Down</th>
                    
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="input_pct[]" value="{{ $ttuItem->input_pct }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="input_level[]" value="{{ $ttuItem->input_level }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="actual[]" value="{{ $ttuItem->actual }}"/></td> 
                    <td><input type="number" step="any" class="form-control" name="output_up[]" value="{{ $ttuItem->output_up }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="output_down[]" value="{{ $ttuItem->output_down }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error_up[]" value="{{ $ttuItem->error_up }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error_down[]" value="{{ $ttuItem->error_down }}"/></td>
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'EVC')
        <table id="data_table_ttu_evc" class="table table-striped table-hover table-responsive-sm">
            <thead>
                
                <tr>
                    <th>Error Total (%)</th>
                    <th>BKD (%)</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems as $ttuItem)
                <tr>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error_bkd[]" value="{{ $ttuItem->error_bkd }}"/></td> 
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @elseif($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh')
        <table id="data_table_ttu_kwh" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="7">Kesalahan/Error Meter kWh</th>
                </tr>
                <tr>
                    <th>Tegangan (V)</th>
                    <th>Frekuensi (Hz)</th>
                    <th>Arus (A)</th>
                    <th>Faktor Daya</th>
                    <th>Kesalahan (%)</th>
                    <th>Ketidaktetapan (%)</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems->filter(function ($value, $key) {
                        return $value->jenis_item == 'kWh';
                    }) as $ttuItem)
                <tr>
                    <input type="hidden" name="jenis_item[]" value="kWh"/>
                    <td><input type="number" step="any" class="form-control" name="tegangan[]" value="{{ $ttuItem->tegangan }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="frekuensi[]" value="{{ $ttuItem->frekuensi }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="arus[]" value="{{ $ttuItem->arus }}"/></td>
                    <td><input type="text" class="form-control" name="faktor_daya[]" value="{{ $ttuItem->faktor_daya }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="ketidaktetapan[]" value="{{ $ttuItem->ketidaktetapan }}"/></td> 
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        <table id="data_table_ttu_kvarh" class="table table-striped table-hover table-responsive-sm">
            <thead>
                <tr>
                    <th colspan="7">Kesalahan/Error Meter kVARh</th>
                </tr>
                <tr>
                    <th>Tegangan (V)</th>
                    <th>Frekuensi (Hz)</th>
                    <th>Arus (A)</th>
                    <th>Faktor Daya</th>
                    <th>Kesalahan (%)</th>
                    <th>Ketidaktetapan (%)</th>
                    <th>
                        <button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($ttuItems->filter(function ($value, $key) {
                        return $value->jenis_item == 'kVARh';
                    }) as $ttuItem)
                <tr>
                    <input type="hidden" name="jenis_item[]" value="kVARh"/>
                    <td><input type="number" step="any" class="form-control" name="tegangan[]" value="{{ $ttuItem->tegangan }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="frekuensi[]" value="{{ $ttuItem->frekuensi }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="arus[]" value="{{ $ttuItem->arus }}"/></td>
                    <td><input type="text" class="form-control" name="faktor_daya[]" value="{{ $ttuItem->faktor_daya }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="error[]" value="{{ $ttuItem->error }}"/></td>
                    <td><input type="number" step="any" class="form-control" name="ketidaktetapan[]" value="{{ $ttuItem->ketidaktetapan }}"/></td> 
                    <td>
                        <input type="hidden" name="item_id[]" />
                        <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>
                    </td>
                </tr>
                @endforeach
            <tbody>
        </table>
        @endif
        
        @else
        @endif

        <input type="hidden" name="stat_sertifikat" id="stat_sertifikat" value="0"/>
        <button class="btn btn-w-md btn-accent" id="btn_simpan">Simpan Hasil Uji</button>

        @if($serviceOrder->preview_count > 0)
        <button class="btn btn-w-md btn-success" id="btn_simpan_submit">Kirim Hasil Uji</button>
        @else
        <br/>
        <span>Pastikan untuk me-review draft sebelum mengirim hasil uji</span>
        @endif

        {!! Form::close() !!}

    </div>
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/jquery-validation/additional-methods.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function ()
    {
        $('.select2, #hasil_uji_memenuhi').select2({
            //placeholder: "- Pilih UML -",
            allowClear: true
        });
        $('.is_accepted_select').select2({
            //placeholder: "- Pilih UML -",
            allowClear: true
        }).on("change", function(e) {
            console.log(e);
            console.log(this.value);
            if (this.value == 'tidak') {
                $(this).closest("tr").css("background-color", "#db524b").css("color", "white");
            }
            else if (this.value == 'ya') {
                $(this).closest("tr").css("background-color", "#449d44").css("color", "white");
            }
            else {
                $(this).closest("tr").css("background-color", "").css("color", "");
            }
        });


        $('#test_by_2').select2({
            placeholder: "- Pilih Pegawai -",
            allowClear: true
        });
        $('#tool_capacity_unit').select2({
            //placeholder: "- Pilih UML -",
            tags: true,
        });
        $('#tool_made_in_id').select2();
        /*
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        */

        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_alg_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" step="any" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_mabbm_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" step="any" class="form-control" name="brand[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_alg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_alg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="input_level[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_up[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_down[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_hysteresis[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mabbm");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="meter_factor[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_ma').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_ma");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_ma').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mka').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mka");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="commodity[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="penunjukan[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mka').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mguwc').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mguwc");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_bkd[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="repeatability[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="repeatability_bkd[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mguwc').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_dt').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_dt");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="rentang_ukur[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="suhu_dasar[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="panjang_sebenarnya[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_dt').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_uti').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_uti");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="rentang_ukur[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="suhu_dasar[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="panjang_sebenarnya[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_uti').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_uti_suhu').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_uti_suhu");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="penunjukan[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="penunjukan_standar[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="koreksi[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="suhu_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_uti_suhu').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_po').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_po");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="d_inner[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="d_outer[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="e_inner[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="e_outer[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="alpha[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="roundness[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="flatness[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="roughness[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="eksentrisitas[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_po').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_pubbm').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_pubbm");
            var insp_item = '<tr>' + 
                '   <td><input type="text" step="any" class="form-control" name="tool_brand[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="tool_type[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="tool_serial_no[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="debit_max[]" /></td>' + 
                '   <td><input type="text" step="any" class="form-control" name="tool_media[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="nozzle_count[]" /></td>' +  
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_pubbm').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $("#satuan_suhu, #satuan_output, #satuan_error").select2();
        $('#data_table_ttu_pt').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_pt");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="input_pct[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="input_level[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="actual[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="output_up[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="output_down[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="error_up[]" /></td>' +  
                '   <td><input type="number" step="any" class="form-control" name="error_down[]" /></td>' +  
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_pt').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_evc').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_evc");
            var insp_item = '<tr>' + 
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="error_bkd[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_evc').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_kwh').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_kwh");
            var insp_item = '<tr>' + 
                '<input type="hidden" name="jenis_item[]" value="kWh"/>' +
                '   <td><input type="number" step="any" class="form-control" name="tegangan[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="frekuensi[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="arus[]" /></td>' +
                '   <td><input type="text" step="any" class="form-control" name="faktor_daya[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="ketidaktetapan[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_kwh').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_kvarh').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_kvarh");
            var insp_item = '<tr>' + 
                '<input type="hidden" name="jenis_item[]" value="kVARh"/>' +
                '   <td><input type="number" step="any" class="form-control" name="tegangan[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="frekuensi[]" /></td>' + 
                '   <td><input type="number" step="any" class="form-control" name="arus[]" /></td>' +
                '   <td><input type="text" step="any" class="form-control" name="faktor_daya[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="error[]" /></td>' +
                '   <td><input type="number" step="any" class="form-control" name="ketidaktetapan[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_kvarh').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $("#btn_simpan").click(function(e){
            e.preventDefault();

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            $("#stat_sertifikat").val(1);

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        jQuery.validator.addMethod("require_from_group", function (value, element, options) {
            var numberRequired = options[0];
            var selector = options[1];
            var fields = $(selector, element.form);
            var filled_fields = fields.filter(function () {
                // it's more clear to compare with empty string
                return $(this).val() != "";
            });
            var empty_fields = fields.not(filled_fields);
            // we will mark only first empty field as invalid
            if (filled_fields.length < numberRequired && empty_fields[0] == element) {
                return false;
            }
            return true;
            // {0} below is the 0th item in the options field
        }, "Salah satu dari data ini harus diisi");

        @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
        $("#form_result").validate({
            rules: {
                @if($serviceOrder->path_skhp == null)
                file_skhp: {
                    required: true,
                    accept: "application/pdf,image/png, image/jpeg"
                },
                @else
                file_skhp: {
                    accept: "application/pdf,image/png, image/jpeg"
                },
                @endif

                kelas_keakurasian: {
                    require_from_group: [1, ".data-tipe"]
                },
                interval_skala_verifikasi: {
                    require_from_group: [1, ".data-tipe"]
                },
                daya_baca: {
                    require_from_group: [1, ".data-tipe"]
                },

                kelas_massa_kendaraan: {
                    require_from_group: [1, ".data-tipe"]
                },
                kecepatan: {
                    require_from_group: [1, ".data-tipe"]
                },
                kelas_single_axle_load: {
                    require_from_group: [1, ".data-tipe"]
                },
                kelas_single_group_load: {
                    require_from_group: [1, ".data-tipe"]
                },

                metode_pengukuran: {
                    require_from_group: [1, ".data-tipe"]
                },
                sistem_jaringan: {
                    require_from_group: [1, ".data-tipe"]
                },

                konstanta: {
                    require_from_group: [1, ".data-tipe"]
                },
                kelas_temperatur: {
                    require_from_group: [1, ".data-tipe"]
                },

                rasio_q: {
                    require_from_group: [1, ".data-tipe"]
                },
                diameter_nominal: {
                    require_from_group: [1, ".data-tipe"]
                },

                jumlah_nozzle: {
                    require_from_group: [1, ".data-tipe"]
                },
                jenis_pompa: {
                    require_from_group: [1, ".data-tipe"]
                },

                volume_bersih: {
                    require_from_group: [1, ".data-tipe"]
                },
                diameter_tangki: {
                    require_from_group: [1, ".data-tipe"]
                },

                tinggi_tangki: {
                    require_from_group: [1, ".data-tipe"]
                },

                meter_daya_baca: {
                    require_from_group: [1, ".data-tipe"]
                },
            },
            messages: {
                file_skhp: 'File lampiran harus diupload dalam format pdf, png atau jpeg',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
        @else
        $("#form_result").validate({
            rules: {
                @if($serviceOrder->path_skhp == null)
                file_skhp: {
                    required: true,
                    accept: "application/pdf,image/png, image/jpeg"
                },
                @else
                file_skhp: {
                    accept: "application/pdf,image/png, image/jpeg"
                },
                @endif
                hasil_uji_memenuhi: {
                    required: true,
                    minlength: 1,
                },
            },
            messages: {
                file_skhp: 'File lampiran harus diupload dalam format pdf, png atau jpeg',
                hasil_uji_memenuhi: 'Hasil uji harus dipilih',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
        @endif
    }); 

    @if($serviceOrder->ServiceRequest->service_type_id == 6 || $serviceOrder->ServiceRequest->service_type_id == 7)
    $('input#mulai_uji').datepicker({
      format:"dd-mm-yyyy",
      autoclose:true,
      startDate: new Date("{{ $serviceOrder->staff_entry_datein }}"),
    });
    @endif

    $('input#tanggal_wet_cal').datepicker({
      format:"dd-mm-yyyy",
      autoclose:true,
    });

    @if($serviceOrder->ServiceRequest->service_type_id == 4 || $serviceOrder->ServiceRequest->service_type_id == 5)
    $('input#sertifikat_expired_at').datepicker({
      format:"dd-mm-yyyy",
      autoclose:true,
      startDate: new Date("{{ $serviceOrder->staff_entry_datein }}"),
    });
    @endif
</script>
@endsection