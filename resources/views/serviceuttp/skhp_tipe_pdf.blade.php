<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Keterangan Hasil Pengujian</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-sk-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-sk-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-sk-draft-uttp.png") : public_path("assets/images/logo/letterhead-sk-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .vertical-middle {
            vertical-align: middle !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            text-align: justify;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
    </style>
</head>

<body class="A4">

    @if($order->cancel_at == null)
    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order }}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN HASIL PENGUJIAN</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 40mm;">Jenis UTTP</td>
                    <td style="width: 2mm;">:</td>
                    <td colspan="5">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td>Merek/Tipe</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_brand != null && $order->tool_model != null ?
                        $order->tool_brand . '/' . $order->tool_model :
                        $order->ServiceRequestItem->uttp->tool_brand . '/' . $order->ServiceRequestItem->uttp->tool_model 
                    }}</td>
                </tr>
                <tr>
                    <td>Nomor Seri</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_serial_no != null  ?
                        $order->tool_serial_no :
                        $order->ServiceRequestItem->uttp->serial_no 
                    }}</td>
                </tr>
                @if(
                    ($order->tool_media != null && $order->tool_media != '') 
                    )
                <tr>
                    <td>Media Uji/Komoditas</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->tool_media }}</td>
                </tr>
                @endif
                @if($order->tool_capacity != null && $order->tool_capacity != '')
                <tr>
                    @if($order->tool_capacity != null && $order->tool_capacity != '')
                    <td style="width: 40mm;">Kapasitas Maksimum</td>
                    <td>:</td>
                    <td style="width: 36mm;">{{ $order->tool_capacity }}&nbsp;{{ $order->tool_capacity_unit }}
                    </td>
                    @endif
                    @if($order->tool_capacity_min != null && $order->tool_capacity_min != '')
                    <td style="width: 3mm;"></td>
                    <td style="width: 35mm;">Kapasitas Minimum</td>
                    <td style="width: 2mm;">:</td>
                    <td style="width: 36mm;">{{ $order->tool_capacity_min }}&nbsp;{{ $order->tool_capacity_unit }}
                    </td>
                    @else
                    <td colspan="4" style="width: 76mm"></td>
                    @endif
                </tr>
                @endif
                
                <?php
                $teknis = [];
                if($order->kelas_keakurasian != null && $order->kelas_keakurasian != '') {
                    $teknis['Kelas Keakurasian'] = $order->kelas_keakurasian;
                }
                if($order->daya_baca != null && $order->daya_baca != '') {
                    $teknis['Interval Skala Terkecil'] = $order->daya_baca . " " . $order->tool_capacity_unit;
                }
                if($order->interval_skala_verifikasi != null && $order->interval_skala_verifikasi != '') {
                    $teknis['Interval Skala Verifikasi'] = $order->interval_skala_verifikasi . " " . $order->tool_capacity_unit;
                }
                if($order->kelas_single_axle_load != null && $order->kelas_single_axle_load != '') {
                    $teknis['Kelas Muatan Sumbu Tunggal'] = $order->kelas_single_axle_load;
                }
                if($order->kelas_single_group_load != null && $order->kelas_single_group_load != '') {
                    $teknis['Kelas Muatan Kelompok Sumbu'] = $order->kelas_single_group_load;
                }
                if($order->kelas_massa_kendaraan != null && $order->kelas_massa_kendaraan != '') {
                    $teknis['Kelas Massa Kendaraan'] = $order->kelas_massa_kendaraan;
                }
                if($order->kecepatan != null && $order->kecepatan != '') {
                    $teknis['Kecepatan'] = $order->kecepatan;
                }
                if($order->metode_pengukuran != null && $order->metode_pengukuran != '') {
                    $teknis['Metode Pengukuran'] = $order->metode_pengukuran;
                }
                if($order->sistem_jaringan != null && $order->sistem_jaringan != '') {
                    $teknis['Sistem Jaringan'] = $order->sistem_jaringan;
                }
                if($order->konstanta != null && $order->konstanta != '') {
                    $teknis['Konstanta'] = $order->konstanta;
                }
                if($order->kelas_temperatur != null && $order->kelas_temperatur != '') {
                    $teknis['Kelas/Rentang Temperatur'] = $order->kelas_temperatur;
                }
                if($order->rasio_q != null && $order->rasio_q != '') {
                    $teknis['Rasio Q<sub>3</sub>/Q<sub>1</sub>'] = $order->rasio_q;
                }
                if($order->diameter_nominal != null && $order->diameter_nominal != '') {
                    $teknis['Diameter Nominal/Standar Flange'] = $order->diameter_nominal;
                }
                if($order->jumlah_nozzle != null && $order->jumlah_nozzle != '') {
                    $teknis['Jumlah Nozzle'] = $order->jumlah_nozzle;
                }
                if($order->jenis_pompa != null && $order->jenis_pompa != '') {
                    $teknis['Jenis Pompa'] = $order->jenis_pompa;
                }
                if($order->volume_bersih != null && $order->volume_bersih != '') {
                    $teknis['Volume Bersih'] = $order->volume_bersih;
                }
                if($order->diameter_tangki != null && $order->diameter_tangki != '') {
                    $teknis['Diameter Tangki'] = $order->diameter_tangki;
                }
                if($order->tinggi_tangki != null && $order->tinggi_tangki != '') {
                    $teknis['Tinggi Tangki'] = $order->tinggi_tangki;
                }
                if($order->meter_daya_baca != null && $order->meter_daya_baca != '') {
                    $teknis['Daya Baca'] = $order->meter_daya_baca;
                }
                ?>

                @foreach($teknis as $label => $value)
                
                @if($loop->iteration % 2 != 0)
                <tr>
                    <td style="width: 40mm;">{!! $label !!}</td>
                    <td style="width: 3mm;">:</td>
                    @if($loop->last)
                    <td colspan="5">{!! $value !!}</td>
                </tr>
                    @else 
                    <td style="width: 36mm;">{!! $value !!}</td>
                    @endif
                @else
                    <td style="width: 3mm;"></td>
                    <td style="width: 35mm;">{!! $label !!}</td>
                    <td style="width: 2mm;">:</td>
                    <td style="width: 36mm;">{!! $value !!}</td>
                </tr>
                @endif

                @endforeach

                
                <tr>
                    <td>Buatan</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->tool_made_in }}</td>
                </tr>
                <tr>
                    <td>Nama Pabrikan</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->tool_factory }}</td>
                </tr>
                <tr>
                    <td>Alamat Pabrikan</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->tool_factory_address }}</td>
                </tr>
                <tr>
                    <td>Pemohon</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                <tr>
                    <td>Alamat Pemohon</td>
                    <td>:</td>
                    <td colspan="5">
                        {{ $order->ServiceRequest->addr_sertifikat }}
                    </td>
                </tr>
                <tr>
                    <td>Diuji/Diverifikasi Oleh</td>
                    <td>:</td>
                    <td colspan="5" style="width: 100%;">
                        @if($order->TestBy1 != null)
                            @if ($order->TestBy1->PetugasUttp != null)
                        <span style="display:inline-block; width:60mm; padding-top: 5px;" >{{ $order->TestBy1->PetugasUttp->nama }}</span>
                        <span style="display:inline-block; width:2mm; padding-top: 5px;">&nbsp;</span>
                        <span style="display:inline-block; padding-top: 5px;">NIP : {{ $order->TestBy1->PetugasUttp->nip }}</span>
                            @elseif ($order->TestBy1->PetugasUut != null)
                        <span style="display:inline-block; width:60mm; padding-top: 5px;" >{{ $order->TestBy1->PetugasUut->nama }}</span>
                        <span style="display:inline-block; width:2mm; padding-top: 5px;">&nbsp;</span>
                        <span style="display:inline-block; padding-top: 5px;">NIP : {{ $order->TestBy1->PetugasUut->nip }}</span>
                            @else 
                            @endif
                        @endif
                        @if($order->TestBy2 != null && 
                            (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                                || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
                            @if ($order->TestBy2->PetugasUttp != null)
                        <br/>
                        <span style="display:inline-block; width:60mm;">{{ $order->TestBy2->PetugasUttp->nama }}</span>
                        <span style="display:inline-block; width:2mm;">&nbsp;</span>
                        <span style="display:inline-block;">NIP : {{ $order->TestBy2->PetugasUttp->nip }}</span>
                            @elseif ($order->TestBy2->PetugasUut != null)
                        <br/>
                        <span style="display:inline-block; width:60mm;">{{ $order->TestBy2->PetugasUut->nama }}</span>
                        <span style="display:inline-block; width:2mm;">&nbsp;</span>
                        <span style="display:inline-block;">NIP : {{ $order->TestBy2->PetugasUut->nip }}</span>
                            @else 
                            @endif
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Waktu Pengujian/Verifikasi</td>
                    <td>:</td>
                    <td colspan="5">
                    @if($order->stat_service_order == 4)
                        {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->cancel_at) }}
                    @else
                        {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->selesai_uji) }}
                    @endif  
                    </td>
                </tr>
                <tr>
                    <td>Lokasi Pengujian/Verifikasi</td>
                    <td>:</td>
                    <td colspan="5">
                        {{ $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'Direktorat Metrologi' : $order->location }}
                    </td>
                </tr>
                <tr>
                    <td>Dasar Pengujian/Verifikasi</td>
                    <td>:</td>
                    <td colspan="5" style="width: 154mm;">
                        Surat Permohonan dari: {{ $order->ServiceRequest->label_sertifikat }}
                        <br/>
                        Nomor: {{ $order->ServiceRequestItem->reference_no }}, Tanggal: {{ format_long_date($order->ServiceRequestItem->reference_date) }} 
                        @if($order->dasar_pemeriksaan != null && $order->dasar_pemeriksaan != '')
                        <br/>
                        {{ $order->dasar_pemeriksaan }}
                        @endif
                    </td>
                </tr>
                
                <tr>
                    <td>Persyaratan Teknis</td>
                    <td>:</td>
                    <td colspan="5" style="width: 154mm;">{!! $order->ServiceRequestItem->uttp->type->syarat_teknis !!}</td>
                </tr>
                <tr>
                    <td>Hasil</td>
                    <td>:</td>
                    <td colspan="5" style="width: 154mm;">
                        @if($order->cancel_at != null)
                        Pengujian tidak dapat dilakukan atau dilanjutkan karena {{ $order->cancel_notes }}.
                        @else
                        <strong>{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</strong> Merek <strong>
                            {{ $order->tool_brand != null ? $order->tool_brand : $order->ServiceRequestItem->uttp->tool_brand }}
                            </strong> tipe <strong>{{ $order->tool_model != null ? $order->tool_model : $order->ServiceRequestItem->uttp->tool_model }}</strong> 
                            dinyatakan <strong>{{ $order->hasil_uji_memenuhi ? 'Memenuhi' : 'Tidak Memenuhi'}} Syarat Teknis</strong>,
                            untuk jenis-jenis pengujian terlampir yang merupakan bagian tidak terpisahkan dari Surat Keterangan Hasil Pengujian ini. 
                        @endif
                    </td>
                </tr>
            </tbody>
        <table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date("d-m-Y")) }}
                        <br/>Kepala Balai Pengujian
                        <br/>Alat Ukur, Alat Takar, Alat Timbang
                        <br/>dan Alat Perlengkapan
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}<br/>
                        NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>

    @if($order->ServiceRequestItem->uttp->type->kelompok != 'Tangki Ukur Tetap Silinder Tegak')
    <div class="page-break"></div>

    <section class="sheet watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <div class="text-center" style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="title"><strong>RESUME PENGUJIAN TIPE UTTP</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>  

        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <thead>
                <tr>
                    <th rowspan="2" class="text-center vertical-middle">NO</th>
                    <th rowspan="2" class="text-center vertical-middle">PEMERIKSAAN &amp; PENGUJIAN</th>
                    <th colspan="3" class="text-center vertical-middle">PEMENUHAN SYARAT</th>
                    <th rowspan="2" class="text-center vertical-middle">KETERANGAN</th>
                </tr>
                <tr>
                    <th class="text-center">YA</th>
                    <th class="text-center">TIDAK</th>
                    <th class="text-center">N/A</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->inspections as $inspection)
                @if($loop->index < 30)
                <tr>
                    <td>
                        @if($inspection->inspectionItem->is_header)
                        {{ $inspection->inspectionItem->no }}
                        @endif
                    </td>
                    <td>
                        @if($inspection->inspectionItem->is_header)
                        {{ $inspection->inspectionItem->name }}
                        @else
                        {{ $inspection->inspectionItem->no }} {{ $inspection->inspectionItem->name }}
                        @endif
                    </td>
                    <td class="text-center">
                        @if($inspection->is_accepted && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td class="text-center">
                        @if(isset($inspection->is_accepted) && $inspection->is_accepted == false && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td class="text-center">
                        @if(!isset($inspection->is_accepted) && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td>&nbsp;</td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>

        @if(count($order->inspections) <= 30)
        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Persetujuan Tipe,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Persetujuan Tipe' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        @endif
    
    </section>
    @endif

    @if(count($order->inspections) > 30 && count($order->inspections) <= 60)
    <div class="page-break"></div>

    <section class="sheet watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <div class="text-center" style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="title"><strong>RESUME EVALUASI TIPE UTTP</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>  

        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <thead>
                <tr>
                    <th rowspan="2" class="text-center vertical-middle">NO</th>
                    <th rowspan="2" class="text-center vertical-middle">PEMERIKSAAN &amp; PENGUJIAN</th>
                    <th colspan="3" class="text-center vertical-middle">PEMENUHAN SYARAT</th>
                    <th rowspan="2" class="text-center vertical-middle">KETERANGAN</th>
                </tr>
                <tr>
                    <th class="text-center">YA</th>
                    <th class="text-center">TIDAK</th>
                    <th class="text-center">N/A</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->inspections as $inspection)
                @if($loop->index >= 30 && $loop->index < 60)
                <tr>
                    <td>
                        @if($inspection->inspectionItem->is_header)
                        {{ $inspection->inspectionItem->no }}
                        @endif
                    </td>
                    <td>
                        @if($inspection->inspectionItem->is_header)
                        {{ $inspection->inspectionItem->name }}
                        @else
                        {{ $inspection->inspectionItem->no }} {{ $inspection->inspectionItem->name }}
                        @endif
                    </td>
                    <td class="text-center">
                        @if($inspection->is_accepted && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td class="text-center">
                        @if(isset($inspection->is_accepted) && $inspection->is_accepted == false && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td class="text-center">
                        @if(!isset($inspection->is_accepted) && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td>&nbsp;</td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>

        @if(count($order->inspections) <= 60)
        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Persetujuan Tipe,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Persetujuan Tipe' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        @endif
    
    </section>
    @endif

    @if(count($order->inspections) > 60)
    <div class="page-break"></div>

    <section class="sheet watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <div class="text-center" style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="title"><strong>RESUME EVALUASI TIPE UTTP</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>  

        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <thead>
                <tr>
                    <th rowspan="2" class="text-center vertical-middle">NO</th>
                    <th rowspan="2" class="text-center vertical-middle">PEMERIKSAAN &amp; PENGUJIAN</th>
                    <th colspan="3" class="text-center vertical-middle">PEMENUHAN SYARAT</th>
                    <th rowspan="2" class="text-center vertical-middle">KETERANGAN</th>
                </tr>
                <tr>
                    <th class="text-center">YA</th>
                    <th class="text-center">TIDAK</th>
                    <th class="text-center">N/A</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->inspections as $inspection)
                @if($loop->index >= 60)
                <tr>
                    <td>
                        @if($inspection->inspectionItem->is_header)
                        {{ $inspection->inspectionItem->no }}
                        @endif
                    </td>
                    <td>
                        @if($inspection->inspectionItem->is_header)
                        {{ $inspection->inspectionItem->name }}
                        @else
                        {{ $inspection->inspectionItem->no }} {{ $inspection->inspectionItem->name }}
                        @endif
                    </td>
                    <td class="text-center">
                        @if($inspection->is_accepted && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td class="text-center">
                        @if(isset($inspection->is_accepted) && $inspection->is_accepted == false && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td class="text-center">
                        @if(!isset($inspection->is_accepted) && $inspection->inspectionItem->is_tested)
                        X
                        @endif
                    </td>
                    <td>&nbsp;</td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Persetujuan Tipe,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Persetujuan Tipe' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
    
    </section>
    @endif

    @else

    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order }}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>

        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        Berdasarkan permohonan pengujian dengan No. Order {{ $order->ServiceRequestItem->no_order }} tanggal 
        {{ format_long_date($order->ServiceRequestItem->order_at) }}, untuk 
        </p>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 30mm;">Alat Ukur</td>
                    <td style="width: 2mm;">:</td>
                    <td colspan="5">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td>Merek</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_brand != null ?
                        $order->tool_brand  :
                        $order->ServiceRequestItem->uttp->tool_brand 
                    }}</td>
                </tr>
                <tr>
                    <td>Tipe</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_model != null ?
                        $order->tool_model :
                        $order->ServiceRequestItem->uttp->tool_model 
                    }}</td>
                </tr>
                <tr>
                    <td>Nomor Seri</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_serial_no != null  ?
                        $order->tool_serial_no :
                        $order->ServiceRequestItem->uttp->serial_no 
                    }}</td>
                </tr>
                
                <tr>
                    <td>Pemilik</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                
            </tbody>
        <table>

        <br/>
        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        <strong>batal diuji</strong> di Instalasi Uji {{ $order->instalasi->nama_instalasi }} Direktorat Metrologi karena {{ $order->cancel_notes }}.
        </p>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td>Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date("d-m-Y") }}
                        <br/>Kepala Balai Pengujian UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}<br/>
                        NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>

    @endif


</body>

</html>