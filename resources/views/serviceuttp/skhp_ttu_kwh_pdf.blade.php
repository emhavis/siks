<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Keterangan Hasil Pengujian</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-sk-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-sk-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-sk-draft-uttp.png") : public_path("assets/images/logo/letterhead-sk-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
        .table td .eng {
            font-size:8pt;
            font-style:italic;
        }
    </style>
</head>

<body class="A4">

    @if($order->cancel_at == null)
    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order }}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN HASIL PENGUJIAN</strong></div>
            <div class="subtitle"><i>Certificate of Testing Result</i></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>
        
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="min-width: 40mm;">Jenis UTTP<div class="eng">Measuring Instrument</div></td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Merek<div class="eng">Trade Mark</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_brand }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Model/Tipe<div class="eng">Model/Type</div></td>
                    <td>:</td>
                    <td style="width: 55mm;">{{ $order->tool_model }}</td>
                    <td style="width: 20mm;">No. Seri<div class="eng">Serial Number</div></td>
                    <td style="width: 3mm;">:</td>
                    <td style="min-width: 25mm;">{{ $order->tool_serial_no }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 15px;">Jaringan Listrik<div class="eng">Power Grid</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->jaringan_listrik }}</td>
                </tr>
                @if($order->tool_capacity != null && $order->tool_capacity != '' && $order->tool_capacity > 0)
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Maksimum<div class="eng">Maximum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">
                        {{ $order->tool_capacity }}&nbsp;{!! $order->tool_capacity_unit !!}
                    </td>
                </tr>
                @endif
                @if($order->tool_capacity_min != null && $order->tool_capacity_min != '' && $order->tool_capacity_min > 0)
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Minimum<div class="eng">Minimum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_capacity_min }}&nbsp;{!! $order->tool_capacity_unit !!}
                    </td>
                </tr>
                @elseif($order->tool_capacity_min != null 
                    && $order->tool_capacity_min != '')
                <tr>
                    <td style="padding-left: 15px;">Kapasitas Minimum<div class="eng">Minimum Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->tool_capacity_min }}&nbsp;{!! $order->tool_capacity_unit !!}
                    </td>
                </tr>
                @endif
                <tr>
                    <td style="padding-left: 15px;">Konstanta<div class="eng">Constant</div></td>
                    <td>:</td>
                    <td>{{ $order->ttu->konstanta }}</td>
                    <td style="width: 20mm;">Kelas<div class="eng">Class</div></td>
                    <td style="width: 3mm;">:</td>
                    <td style="min-width: 25mm;">{{ $order->ttu->kelas }}</td>
                </tr>
                <tr>
                    <td>Pemilik/Pemakai<div class="eng">Owner/User</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                <tr>
                    <td>Lokasi Alat<div class="eng">Location</div></td>
                    <td>:</td>
                    <td colspan="4">
                        @if ($order->ServiceRequest->lokasi_pengujian == 'luar')
                        {{ $order->location_alat }}
                        @else
                        {{ $order->location }}
                        @endif
                    </td>
                    <!--
                    <td colspan="4">{{ $order->ServiceRequest->addr_sertifikat }}</td>
                    -->
                </tr>
                
                <tr>
                    <td>Hasil<div class="eng">Result</div></td>
                    <td>:</td>
                    <td colspan="4">
                        @if($order->hasil_uji_memenuhi == true)
                        Disahkan berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal.
                        @else
                        Batal berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal. 
                        @endif  
                    </td>
                </tr>

                
            </tbody>
        <table>

        <br />
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td colspan="2">SKHP ini terdiri dari {{ count($order->ttuPerlengkapans) > 0 ? '4 (empat)' : '3 (tiga)' }} halaman
                    <div class="eng">This certificate consists of {{ count($order->ttuPerlengkapans) > 0 ? '4 (four)' : '3 (three)' }} pages</div>
                </td>
            </tr>
            <tr>
                <td colspan="2">Catatan:<div class="eng">Notes:</div></td>
            </tr>
            @if($order->hasil_uji_memenuhi == true)
    
            <tr>
                <td style="padding-left: 10px; width: 3mm;">1.</td>
                <td>UTTP ini wajib ditera ulang paling lambat tanggal <b>{{ format_long_date($order->sertifikat_expired_at) }}</b>
                    <?php setlocale(LC_TIME, "en_EN"); ?>
                    <div class="eng">This measuring instrument mandatory to reverificate on {{ strftime('%e %B %Y', strtotime($order->sertifikat_expired_at)) }}</div>
                </td>
            </tr>
            
            <tr>
                <td style="padding-left: 10px; width: 3mm;">2.</td>
                <td>Apabila tanda tera rusak dan/atau kawat segel putus, UTTP ini wajib ditera ulang
                    <div class="eng">If the verification mark is damaged and/or the seal wire is broken, this measuring instrument must be re-verificated</div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px; width: 3mm;">3.</td>
                <td>Pemutusan tanda tera hanya dapat dilakukan dengan sepengetahuan Direktorat Metrologi
                    <div class="eng">Breaking of the verification mark can only be done with the agreement of the Directorate of Metrology</div>
                </td>
            </tr>
            
            @else
            <tr>
                <td style="padding-left: 10px; width: 3mm;">&nbsp;</td>
                <td>{!! $order->catatan_hasil_pemeriksaan !!}</td>
            </tr>
            @endif
        </table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Kepala Balai Pengujian
                        <br/>Alat Ukur, Alat Takar, Alat Timbang
                        <br/>dan Alat Perlengkapan
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} ' 
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}
                        <br/>NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>

    <div class="page-break"></div>
    <section class="sheet watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <div style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>DATA PENGUJIAN</strong></div>
            <div class="subtitle"><i>Testing Data</i></div>
        </div>

        <br/>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">

            <tr>
                @if($order->TestBy2 != null && 
                    (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                        || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
                <td rowspan="2">Pegawai Berhak</td>
                <td rowspan="2">:</td>
                @else
                <td>Pegawai Berhak</td>
                <td>:</td>
                @endif
                @if ($order->TestBy1 != null)
                    @if ($order->TestBy1->PetugasUttp != null)
                <td style="min-width: 35mm">1. {{ $order->TestBy1->PetugasUttp->nama }}</td>
                <td>NIP: {{ $order->TestBy1->PetugasUttp->nip }}</td>
                    @elseif ($order->TestBy1->PetugasUut != null)
                <td style="min-width: 35mm">1. {{ $order->TestBy1->PetugasUut->nama }}</td>
                <td>NIP: {{ $order->TestBy1->PetugasUut->nip }}</td>
                    @else
                <td style="min-width: 35mm">1. </td>
                <td>NIP: </td>
                    @endif
                @else 
                <td style="min-width: 35mm"></td>
                <td></td>
                @endif
            </tr>
            @if($order->TestBy2 != null && 
                    (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                        || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
            <tr>
                @if ($order->TestBy2 != null)
                    @if ($order->TestBy2->PetugasUttp != null)
                <td>2. {{ $order->TestBy2->PetugasUttp->nama }}</td>
                <td>NIP: {{ $order->TestBy2->PetugasUttp->nip }}</td>
                    @elseif ($order->TestBy2->PetugasUut != null)
                <td>2. {{ $order->TestBy2->PetugasUut->nama }}</td>
                <td>NIP: {{ $order->TestBy2->PetugasUut->nip }}</td>
                    @else
                <td>2. </td>
                <td>NIP: </td>
                    @endif
                @else 
                <td></td>
                <td></td>
                @endif
            </tr>
            @endif
            <tr>
                <td>Tanggal Pengujian</td>
                <td>:</td>
                <td colspan="4">
                <?php
                    $start = new DateTime($order->mulai_uji);
                    $end = new DateTime($order->stat_service_order == 4 ? $order->cancel_at : $order->selesai_uji );
                    $diff = $end->diff($start);
                ?>
                @if($diff->d == 0)
                    {{ format_long_date($order->mulai_uji) }}
                @else
                @if($order->stat_service_order == 4)
                    {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->cancel_at) }}
                @else
                    {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->selesai_uji) }}
                @endif
                @endif
                </td>
            </tr>
            <tr>
                <td>Lokasi</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'Direktorat Metrologi' : $order->location }}
                </td>
            </tr>
            
            <tr>
                <td>Kondisi Ruangan</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->kondisi_ruangan }}
                </td>
            </tr>
            <tr>
                <td>Trapo Ukur (CT; PT)</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->trapo_ukur }}
                </td>
            </tr>
        </table>  

        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>HASIL PENGUJIAN</strong></div>
            <div class="subtitle"><i>Testing Result</i></div>
        </div>
        
        <br/>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <th colspan="6">Kesalahan/Error Meter kWh</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>Tegangan (V)</th>
                <th>Frekuensi (Hz)</th>
                <th>Arus (A)</th>
                <th>Faktor Daya</th>
                <th>Kesalahan (%)</th>
                <th>Ketidaktetapan (%)</th>
            </tr>
            @foreach($order->ttuItems->filter(function ($value, $key) {
                        return $value->jenis_item == 'kWh';
                    }) as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->tegangan, 1, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->frekuensi, 0, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->arus, 2, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->faktor_daya }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->ketidaktetapan, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>

        <br/>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <th colspan="6">Kesalahan/Error Meter kVARh</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>Tegangan (V)</th>
                <th>Frekuensi (Hz)</th>
                <th>Arus (A)</th>
                <th>Faktor Daya</th>
                <th>Kesalahan (%)</th>
                <th>Ketidaktetapan (%)</th>
            </tr>
            @foreach($order->ttuItems->filter(function ($value, $key) {
                        return $value->jenis_item == 'kVARh';
                    }) as $ttuItem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->tegangan, 1, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->frekuensi, 0, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->arus, 2, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $ttuItem->faktor_daya }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->error, 3, ',', '') }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ number_format((float)$ttuItem->ketidaktetapan, 3, ',', '') }}</td>
            </tr>
            @endforeach
        </table>
        
    </section>

    <section class="sheet watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>
   
        <div style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
        </div>

        @if($order->hasil_uji_memenuhi == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>PEMBUBUHAN TANDA TERA</strong></div>
            <div class="subtitle"><i>Affixing Verification Mark</i></div>
        </div>

        <br/>
        <table class="table table-borderless" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>1.</td>
                <td>
                    1 (satu) buah Tanda Pegawai Berhak <b>HP4</b> "{{ $order->tanda_pegawai_berhak }}"" dan Tanda Sah SP6 "{{ $order->tanda_sah }}" dipasang secara bolak-balik pada baut penutup Meter kWh
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>1 (satu) buah Tanda Jaminan <b>JP8</b> dibubuhkan pada tombol reset Meter kWh</td>
            </tr>
        </table>

        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>REKOMENDASI PENGGUNAAN</strong></div>
            <div class="subtitle"><i>Recommended Instalation</i></div>
        </div>
        

        @endif

        <br/>
        <br/>

        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Tera/Tera Ulang UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Tera/Tera Ulang UTTP' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>

    </section>


    @if(count($order->ttuPerlengkapans) > 0)
    <div class="page-break"></div>
    <section class="sheet watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <table class="table table-bordered" width="100%" style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th width="20px">No.</th>
                <th>Nama Perlengkapan</th>
                <th>Merk; Tipe; Nomor Seri; <br/>Daerah Ukur</th>
                <th>Keterangan/Penyegelan</th>
            </tr>
            @foreach($order->ttuPerlengkapans as $perlengkapan)
            <?php
                $kapasitas = '';
                if (strlen($perlengkapan->uttp->tool_capacity_min) > 0) {
                    if ($perlengkapan->uttp->tool_capacity_min != null 
                        && strlen($perlengkapan->uttp->tool_capacity_min) > 0)
                    {
                        $kapasitas = $perlengkapan->uttp->tool_capacity_min . ' - ';
                    } 
                    $kapasitas .= $perlengkapan->uttp->tool_capacity . ' ' . $perlengkapan->uttp->tool_capacity_unit;
                }
            ?>
            <tr>
                <td style="text-align: right; padding-right: 10px;">{{ $loop->index + 1 }}</td>
                <td style="padding-right: 10px;">
                    {{ $perlengkapan->uttp->type->uttp_type }}
                    @if($perlengkapan->tag != null && strlen($perlengkapan->tag) > 0)
                    <br/>(Tag No. {{ $perlengkapan->tag }})
                    @endif
                </td>
                <td style="padding-right: 10px;">{{ $perlengkapan->uttp->tool_brand }}; {{ $perlengkapan->uttp->tool_model }}; {{ $perlengkapan->uttp->serial_no }}; <br/>{{ $kapasitas }} </td>
                <td style="padding-right: 10px;">{{ $perlengkapan->keterangan }}</td>
            </tr>
            @endforeach
        </table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Tera/Tera Ulang UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Tera/Tera Ulang UTTP' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
    @endif

    @else

    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order }}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>

        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        Berdasarkan permohonan pengujian dengan No. Order {{ $order->ServiceRequestItem->no_order }} tanggal 
        {{ format_long_date($order->ServiceRequestItem->order_at) }}, untuk 
        </p>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 30mm;">Alat Ukur</td>
                    <td style="width: 2mm;">:</td>
                    <td colspan="5">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td>Merek</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_brand != null ?
                        $order->tool_brand  :
                        $order->ServiceRequestItem->uttp->tool_brand 
                    }}</td>
                </tr>
                <tr>
                    <td>Tipe</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_model != null ?
                        $order->tool_model :
                        $order->ServiceRequestItem->uttp->tool_model 
                    }}</td>
                </tr>
                <tr>
                    <td>Nomor Seri</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_serial_no != null  ?
                        $order->tool_serial_no :
                        $order->ServiceRequestItem->uttp->serial_no 
                    }}</td>
                </tr>
                
                <tr>
                    <td>Pemilik</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                
            </tbody>
        <table>

        <br/>
        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        <strong>batal diuji</strong> di Instalasi Uji {{ $order->instalasi->nama_instalasi }} Direktorat Metrologi karena {{ $order->cancel_notes }}.
        </p>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td>Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date("d-m-Y") }}
                        <br/>Kepala Balai Pengujian UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}<br/>
                        NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>

    @endif



</body>

</html>