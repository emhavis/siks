@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_uttp">Jenis UTTP</label>
                    {!! Form::text('jenis_uttp', 
                        $serviceOrder->uttp != null ? 
                        $serviceOrder->uttp->type->uttp_type :
                        $serviceOrder->ServiceRequestItem->uttp->type->uttp_type, ['class' => 'form-control','id' => 'jenis_uttp', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas Minimum</label>
                    {!! Form::text('kapasitas', 
                        $serviceOrder->tool_capacity_min != null && $serviceOrder->tool_capacity_unit != null ?
                        $serviceOrder->tool_capacity_min . ' ' . $serviceOrder->tool_capacity_unit : 
                        $serviceOrder->ServiceRequestItem->uttp->tool_capacity_min . ' ' . $serviceOrder->ServiceRequestItem->uttp->tool_capacity_unit, ['class' => 'form-control','id' => 'kapasitas', 'readonly']) !!}
                </div>  
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="kapasitas">Kapasitas Maksimum</label>
                    {!! Form::text('kapasitas', 
                        $serviceOrder->tool_capacity != null && $serviceOrder->tool_capacity_unit != null ?
                        $serviceOrder->tool_capacity . ' ' . $serviceOrder->tool_capacity_unit : 
                        $serviceOrder->ServiceRequestItem->uttp->tool_capacity . ' ' . $serviceOrder->ServiceRequestItem->uttp->tool_capacity_unit, ['class' => 'form-control','id' => 'kapasitas', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Merek/Model/Tipe/Nomor Seri</label>
                    {!! Form::text('merek', 
                        $serviceOrder->tool_brand != null && $serviceOrder->tool_model != null && $serviceOrder->tool_serial_no != null ?
                        $serviceOrder->tool_brand . '/' . 
                        $serviceOrder->tool_model . '/' .
                        $serviceOrder->tool_serial_no : 
                        $serviceOrder->ServiceRequestItem->uttp->tool_brand . '/' . 
                        $serviceOrder->ServiceRequestItem->uttp->tool_model . '/' .
                        $serviceOrder->ServiceRequestItem->uttp->serial_no,
                        ['class' => 'form-control','id' => 'merek', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->tool_made_in != null ? 
                        $serviceOrder->tool_made_in : 
                        $serviceOrder->ServiceRequestItem->uttp->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan', 'readonly']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                </div>
            </div>
        </div>

        <!--
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory">Nama Pabrikan</label>
                    {!! Form::text('factory', 
                        $serviceOrder->ServiceRequestItem->uttp->tool_factory,
                        ['class' => 'form-control','id' => 'factory', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="factory_addr">Alamat Pabrikan</label>
                    {!! Form::text('factory_addr', 
                        $serviceOrder->ServiceRequestItem->uttp->tool_factory_address,
                        ['class' => 'form-control','id' => 'factory_addr', 'readonly']) !!}
                </div>
            </div>
        </div>
        -->

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Dasar Pengujian, Nomor</label>
                    {!! Form::text('reference_no', 
                        $serviceOrder->ServiceRequestItem->reference_no,
                        ['class' => 'form-control','id' => 'reference_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_date">Dasar Pengujian, Tanggal</label>
                    {!! Form::text('reference_date', 
                        date("d-m-Y", strtotime($serviceOrder->ServiceRequestItem->reference_date)),
                        ['class' => 'form-control','id' => 'reference_date', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Item Pengujian/Pemeriksaan</label>
                    <?php
                        $inspections = [];
                        foreach($serviceOrder->ServiceRequestItem->inspections as $inspection) {
                            $inspections[] = $inspection->inspectionPrice->inspection_type;
                        }
                    ?>
                    <textarea name="inspections" id="inspections" readonly
                        class="form-control">{!! implode("\n", $inspections) !!}</textarea>
                </div>
            </div>
        </div>

        {!! Form::open(['url' => route('serviceuttp.savetest', $serviceOrder->id)])  !!}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Tanggal Selesai Uji</label>
                    {!! Form::text('staff_entry_dateout', 
                        date("d-m-Y"),
                        ['class' => 'date form-control','id' => 'staff_entry_dateout', 'readonly']) !!}
                </div>
            </div>
        </div>
        <button role="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Selesai Uji</button>
        {!! Form::close() !!}
        
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function ()
    {
       
    });

</script>
@endsection