<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Sertifikat Evaluasi Tipe</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-draft-uttp.png") : public_path("assets/images/logo/letterhead-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
    </style>
</head>

<body class="A4">

    @if($order->set_memenuhi)

    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="padding-top: 35mm; padding-right: 25.4mm;">&nbsp;</div>

        <div class="text-center">
            <div class="title"><strong>SERTIFIKAT EVALUASI TIPE</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_surat_tipe }}</div>
        </div>

        <br/><br/>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 3mm;">1.</td>
                    <td colspan="7">Pemohon</td>
                </tr>
                <tr>
                    <td></td>
                    <td style="width: 40mm;">Nama Perusahaan</td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="5">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Alamat Perusahaan</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->ServiceRequest->addr_sertifikat }}</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td colspan="7">Pabrikan</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Nama Pabrik</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_factory != null ?
                        $order->tool_factory :
                        $order->ServiceRequestItem->uttp->tool_factory 
                    }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Negara Pembuat</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_made_in != null ? 
                        $order->tool_made_in :
                        $order->ServiceRequestItem->uttp->tool_made_in 
                    }}</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td colspan="7">Identitas Alat Ukur, Alat Takar, Alat Timbang dan Alat Perlengkapan</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Jenis</td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="5">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Merek</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_brand != null ? 
                        $order->tool_brand :
                        $order->ServiceRequestItem->uttp->tool_brand 
                    }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Model/Tipe</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_model != null ? 
                        $order->tool_model :
                        $order->ServiceRequestItem->uttp->tool_model 
                    }}</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td colspan="7">Deskripsi Teknis</td>
                </tr>
                <?php
                $teknis = [];
                if($order->kelas_keakurasian != null && $order->kelas_keakurasian != '') {
                    $teknis['Kelas Keakurasian'] = $order->kelas_keakurasian;
                }
                if($order->daya_baca != null && $order->daya_baca != '') {
                    $teknis['Interval Skala Terkecil'] = $order->daya_baca;
                }
                if($order->interval_skala_verifikasi != null && $order->interval_skala_verifikasi != '') {
                    $teknis['Interval Skala Verifikasi'] = $order->interval_skala_verifikasi;
                }
                if($order->kelas_single_axle_load != null && $order->kelas_single_axle_load != '') {
                    $teknis['Kelas Muatan Sumbu Tunggal'] = $order->kelas_single_axle_load;
                }
                if($order->kelas_single_group_load != null && $order->kelas_single_group_load != '') {
                    $teknis['Kelas Muatan Kelompok Sumbu'] = $order->kelas_single_group_load;
                }
                if($order->kelas_massa_kendaraan != null && $order->kelas_massa_kendaraan != '') {
                    $teknis['Kelas Massa Kendaraan'] = $order->kelas_massa_kendaraan;
                }
                if($order->kecepatan != null && $order->kecepatan != '') {
                    $teknis['Kecepatan'] = $order->kecepatan;
                }
                if($order->metode_pengukuran != null && $order->metode_pengukuran != '') {
                    $teknis['Metode Pengukuran'] = $order->metode_pengukuran;
                }
                if($order->sistem_jaringan != null && $order->sistem_jaringan != '') {
                    $teknis['Sistem Jaringan'] = $order->sistem_jaringan;
                }
                if($order->konstanta != null && $order->konstanta != '') {
                    $teknis['Konstanta'] = $order->konstanta;
                }
                if($order->kelas_temperatur != null && $order->kelas_temperatur != '') {
                    $teknis['Kelas/Rentang Temperatur'] = $order->kelas_temperatur;
                }
                if($order->rasio_q != null && $order->rasio_q != '') {
                    $teknis['Rasio Q<sub>3</sub>/Q<sub>1</sub>'] = $order->rasio_q;
                }
                if($order->diameter_nominal != null && $order->diameter_nominal != '') {
                    $teknis['Diameter Nominal/Standar Flange'] = $order->diameter_nominal;
                }
                if($order->jumlah_nozzle != null && $order->jumlah_nozzle != '') {
                    $teknis['Jumlah Nozzle'] = $order->jumlah_nozzle;
                }
                if($order->jenis_pompa != null && $order->jenis_pompa != '') {
                    $teknis['Jenis Pompa'] = $order->jenis_pompa;
                }
                if($order->volume_bersih != null && $order->volume_bersih != '') {
                    $teknis['Volume Bersih'] = $order->volume_bersih;
                }
                if($order->diameter_tangki != null && $order->diameter_tangki != '') {
                    $teknis['Diameter Tangki'] = $order->diameter_tangki;
                }
                if($order->tinggi_tangki != null && $order->tinggi_tangki != '') {
                    $teknis['Tinggi Tangki'] = $order->tinggi_tangki;
                }
                ?>
                <tr>
                    <td></td>
                    <td style="width: 40mm;">Kapasitas Maksimum</td>
                    <td>:</td>
                    <td>{{ 
                            $order->tool_capacity != null ?
                            $order->tool_capacity :
                            $order->ServiceRequestItem->uttp->tool_capacity 
                        }}&nbsp;
                        {{ 
                            $order->tool_capacity_unit != null ?
                            $order->tool_capacity_unit :
                            $order->ServiceRequestItem->uttp->tool_capacity_unit 
                        }}
                    </td>
                    @if($order->tool_capacity_min != null && $order->tool_capacity_min != '')
                    <td style="width: 5mm"></td>
                    <td style="width: 40mm;">Kapasitas Minimum</td>
                    <td>:</td>
                    <td>{{ $order->tool_capacity_min }}
                        {{ $order->tool_capacity_unit }}
                    </td>
                    @elseif($order->ServiceRequestItem->uttp->tool_capacity_min != '')
                    <td style="width: 5mm"></td>
                    <td style="width: 40mm;">Kapasitas Minimum</td>
                    <td>:</td>
                    <td>{{ $order->ServiceRequestItem->uttp->tool_capacity_min 
                        }}
                        {{ $order->ServiceRequestItem->uttp->tool_capacity_unit 
                        }}
                    </td>
                    @endif
                </tr>

                @foreach($teknis as $label => $value)
                
                @if($loop->iteration % 2 != 0)
                <tr>
                    <td></td>
                    <td style="width: 40mm;">{!! $label !!}</td>
                    <td>:</td>
                    @if($loop->last)
                    <td colspan="5">{!! $value !!}</td>
                </tr>
                    @else 
                    <td>{!! $value !!}</td>
                    @endif
                @else
                    <td style="width: 5mm"></td>
                    <td style="width: 40mm;">{!! $label !!}</td>
                    <td style="width: 3mm;">:</td>
                    <td>{!! $value !!}</td>
                </tr>
                @endif

                @endforeach

                <tr>
                    <td>5.</td>
                    <td>Persyaratan Teknis</td>
                    <td colspan="6">:</td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="7">{!! $order->ServiceRequestItem->uttp->type->syarat_teknis !!}</td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td style="max-width: 20mm;">a. Nomor Surat Keterangan Hasil Pemeriksaan Tipe</td>
                    <td>:</td>
                    <td colspan="5">
                        <?php $idx = 0; ?>
                        @foreach($otherOrders as $other)
                        {{ $other->no_sertifikat_tipe != null ? $other->no_sertifikat_tipe : '-- sedang diproses bersamaan --' }}{!! $idx < count($otherOrders) ? '<br/>' : '' !!}
                        <?php $idx++; ?>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>b. Nomor Surat Keterangan Hasil Pengujian</td>
                    <td>:</td>
                    <td colspan="5">
                        <?php $idx = 0; ?>
                        @foreach($otherOrders as $other)
                        {{ $other->no_sertifikat != null ? $other->no_sertifikat : '-- sedang diproses bersamaan --' }}{!! $idx < count($otherOrders) ? '<br/>' : '' !!}
                        <?php $idx++; ?>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Hasil</td>
                    <td>:</td>
                    <td colspan="5"><strong>{{ $order->ServiceRequestItem->uttp->type->uttp_type }}</strong> dengan Merek <strong>{{ $order->ServiceRequestItem->uttp->tool_brand }}</strong> tipe <strong>{{ $order->ServiceRequestItem->uttp->tool_model }}</strong> dinyatakan <strong>
                        {{ $order->hasil_uji_memenuhi ? 'Memenuhi' : 'Tidak Memenuhi'}} Syarat Teknis</strong></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="7">
                    Surat Keterangan Hasil Pemeriksaan Tipe (SKHPT) dan/atau Surat Keterangan Hasil Pengujian (SKHP) tercantum dalam lampiran yang merupakan bagian tidak terpisahkan dari Sertifikat Evaluasi Tipe ini.
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tanggal diterbitkan</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date("d-m-Y")) }}</td>
                </tr>
            </tbody>
        <table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td>Kepala Balai Pengujian UTTP,</td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai' }}<br/>
                        NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
    </section>

    @else

    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="padding-top: 35mm; padding-right: 25.4mm;">&nbsp;</div>

        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm; width: 100%;">
            <tbody>
                <tr>
                    <td style="width: 15mm;">Nomor</td>
                    <td style="width: 3mm;">:</td>
                    <td>{{ $order->no_surat_tipe }}</td>
                    <td style="text-align: right;" class=" push-right">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date("d-m-Y")) }}
                    </td>
                </tr>
                <tr>
                    <td>Lampiran</td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="2">1 berkas</td>
                </tr>
                <tr>
                    <td>Perihal</td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="2">Penolakan Permohonan Evaluasi Tipe</td>
                </tr>
            </tbody>
        </table>

        <br/>

        <p style="padding-left: 25.4mm; padding-right: 25.4mm;">
            Yth. {{ $order->ServiceRequest->requestor->full_name }}<br/>
            di <br/>
            &nbsp; &nbsp; &nbsp;Tempat 
        </p>

        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        Memperhatikan hasil pengujian tipe terhadap purwarupa Alat Ukur, Alat Takar, Alat Timbang dan Alat Perlengkapan 
        berupa:</p>
        <table class="table table-borderless" style="padding-left: 30mm;">
            <tr>
                <td>Jenis Alat</td>
                <td sytle="width: 2mm;">:</td>
                <td>{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
            </tr>
            <tr>
                <td>Merek</td>
                <td sytle="width: 2mm;">:</td>
                <td>{{ $order->tool_brand != null ? $order->tool_brand : $order->ServiceRequestItem->uttp->tool_brand }}</td>
            </tr>
            <tr>
                <td>Model/Tipe</td>
                <td sytle="width: 2mm;">:</td>
                <td>{{ $order->tool_model != null ? $order->tool_model : $order->ServiceRequestItem->uttp->tool_model }}</td>
            </tr>
            <tr>
                <td>Pemilik</td>
                <td sytle="width: 2mm;">:</td>
                <td>{{ $order->ServiceRequest->label_sertifikat }}</td>
            </tr>
        </table>
        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        sebagaimana tercantum dalam 
        Surat Keterangan Hasil Pemeriksaan Tipe (SKHPT) 
        nomor {{ $order->no_sertifikat_tipe != null ? $order->no_sertifikat_tipe : '-- sedang diproses bersamaan --' }}
        tanggal {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date("d-m-Y")) }} 
        @if(!$order->is_skhpt)
        dan/atau 
        Surat Keterangan Hasil Pengujian (SKHP) 
        nomor {{ $order->no_sertifikat != null ? $order->no_sertifikat : '-- sedang diproses bersamaan --' }}
        tanggal {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date("d-m-Y")) }}
        @endif
        , dengan ini permohonan Saudara 
        Nomor {{ $order->ServiceRequestItem->reference_no }}, 
        Tanggal {{ format_long_date($order->ServiceRequestItem->reference_date) }} 
        <b>tidak dapat kami kabulkan</b>. 
        </p>

        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td>Kepala Balai Pengujian UTTP,</td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator)) !!} '>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai' }}<br/>
                        NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
    </section>

    @endif
</body>

</html>