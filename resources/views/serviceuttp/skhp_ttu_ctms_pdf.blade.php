<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Keterangan Hasil Pengujian</title>

    <style>
        
        /* paper css */
        @page { 
            margin: 0;
            size: A4;
        }
        body { 
            margin: 0;
            font-family: "Times New Roman", Times, serif;
            font-size: 10pt;
            line-height: 1.3; 
        }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }
        .page-break {
            page-break-after: always;
        }
        body.A4 .sheet { width: 210mm; height: 296mm }
        body.A4 .sheet-paged { 
            width: 210mm; 
            height: 296mm;
            page-break-inside: always;
        }
        @media screen {
            body { 
                background: #e0e0e0;
            }
            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
                margin: 5mm auto;
            }
        }
        @media print {
            body.A4 { width: 210mm }
        }

        /* custom */
        .letterhead {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-sk-nodraft-uttp.png") : public_path("assets/images/logo/letterhead-sk-nodraft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .letterhead-draft {
            background-image: url('{{ $view ? asset("assets/images/logo/letterhead-sk-draft-uttp.png") : public_path("assets/images/logo/letterhead-sk-draft-uttp.png") }}');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .watermark {
            background-image: url('{{ $view ? asset("assets/images/logo/logo-watermark.png") : public_path("assets/images/logo/logo-watermark.png") }}');
            background-repeat: no-repeat;
            background-position: center; 
        }

        .title {
            font-size: 12pt;
        }
        .subtitle {
            font-size: 10pt;
        }
        .push-right {
            position: absolute;
            right: 0px;
            width: 300px;
        }

        /* bootstrap 4.6.1 */
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .float-left {
            float: left !important;
        }
        .float-right {
            float: right !important;
        }
        .table {
            color: #000;
        }
        .table th,
        .table td {
            vertical-align: top;
            
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #000;
        }
        .table tbody + tbody {
            border-top: 2px solid #000;
        }
        .table-bordered {
            
            border-spacing: 0;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000;
        }
        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }
        .table-borderless th,
        .table-borderless td,
        .table-borderless thead th,
        .table-borderless tbody + tbody {
            border: 0;
        }
        
        .table td .eng {
            font-size:8pt;
            font-style:italic;
        }
    </style>
</head>

<body class="A4">

    @if($order->cancel_at == null)
    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order }}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN HASIL PENGUJIAN</strong></div>
            <div class="subtitle"><i>Certificate of Testing Result</i></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>
        
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="min-width: 40mm;">Jenis UTTP<div class="eng">Measuring Instrument</div></td>
                    <td style="width: 3mm;">:</td>
                    <td colspan="4">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td>Nama Kapal<div class="eng">Vessel’s Name</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->kapal }}</td>
                </tr>
                <tr>
                    <td>Ukuran<div class="eng">Dimensions</div></td>
                    <td>:</td>
                    <td colspan="4">
                        Panjang <span style="font-size:8pt; font-style:italic;">(LOA)</span> = {{ $order->ttu->panjang }};  
                        Lebar <span style="font-size:8pt; font-style:italic;">(Breadth)</span> = {{ $order->ttu->lebar }};  
                        Kedalaman <span style="font-size:8pt; font-style:italic;">(Depth)</span> = {{ $order->ttu->kedalaman }} 
                    </td>
                </tr>
                <tr>
                    <td>Kapasitas <div class="eng">Capacity</div></td>
                    <td>:</td>
                    <td colspan="4">
                        {{ $order->tool_capacity }}&nbsp;{!! $order->tool_capacity_unit !!} ({!! $order->ttu->catatan_kapasitas !!})
                    </td>
                </tr>
                <tr>
                    <td>Pemilik<div class="eng">Owner</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                <tr>
                    <td>Pemakai<div class="eng">User</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->user }}</td>
                </tr>
                <tr>
                    <td>Operator<div class="eng">Operator</div></td>
                    <td>:</td>
                    <td colspan="4">{{ $order->ttu->operator }}</td>
                </tr>
                <tr>
                    <td>Lokasi Alat<div class="eng">Location</div></td>
                    <td>:</td>
                    <td colspan="4">
                        @if ($order->ServiceRequest->lokasi_pengujian == 'luar')
                        {{ $order->location_alat }}
                        @else
                        {{ $order->location }}
                        @endif
                    </td>
                </tr>

                <tr>
                    <td>Hasil<div class="eng">Result</div></td>
                    <td>:</td>
                    <td colspan="4">
                        @if($order->hasil_uji_memenuhi == true)
                        Disahkan berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal.
                        @else
                        Batal berdasarkan Undang-Undang Republik Indonesia Nomor 2 Tahun 1981 tentang Metrologi Legal. 
                        @endif  
                    </td>
                </tr>

                
            </tbody>
        <table>

        <br />
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td colspan="2">Catatan:<div class="eng">Notes:</div></td>
            </tr>
            @if($order->hasil_uji_memenuhi == true)
            <tr>
                <td>1.</td>
                <td>Apabila tidak terjadi perubahan atau kerusakan pada UTTP, dan masa Dry Dock belum jatuh tempo, maka SKHP ini habis masa berlaku pada tanggal <b>{{ format_long_date($order->sertifikat_expired_at) }}.</b>
                <div class="eng">If there are no changes or damages on the Measuring Instruments, and the Dry Dock is not yet due, this Certificate is valid until  {{ strftime('%e %B %Y', strtotime($order->sertifikat_expired_at)) }}</div>
                </td>
            </tr>
            @if($order->ServiceRequestItem->uttp->type->kelompok == 'CTMS Gas')
            <tr>
                <td>2.</td>
                <td>Surat Keterangan Hasil Pengujian (SKHP) ini terdiri dari 3 (tiga) halaman</b>
                <div class="eng">This certificate consists of 3 (three) pages</div>
                </td>
            </tr>
            @elseif($order->ServiceRequestItem->uttp->type->kelompok == 'CTMS BBM')
            <tr>
                <td>2.</td>
                <td>Surat Keterangan Hasil Pengujian (SKHP) ini terdiri dari 2 (dua) halaman</b>
                <div class="eng">This certificate consists of 3 (two) pages</div>
                </td>
            </tr>
            @endif
            @if($order->catatan_hasil_pemeriksaan != null)
            <tr>
                <td>3.</td>
                <td>{{ $order->catatan_hasil_pemeriksaan }}</td>
            </tr>
            @endif
            @endif
        </table>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Kepala Balai Pengujian
                        <br/>Alat Ukur, Alat Takar, Alat Timbang
                        <br/>dan Alat Perlengkapan
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} ' 
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}
                        <br/>NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>

    
    @if($order->ServiceRequestItem->uttp->type->kelompok == 'CTMS Gas')
    <div class="page-break"></div>
    <section class="sheet-paged watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <div style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>DATA PENGUJIAN</strong></div>
            <div class="subtitle"><i>Testing Data</i></div>
        </div>

        <br/>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                @if($order->TestBy2 != null && 
                    (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                        || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
                <td rowspan="2">Pegawai Berhak</td>
                <td rowspan="2">:</td>
                @else
                <td>Pegawai Berhak</td>
                <td>:</td>
                @endif
                @if ($order->TestBy1 != null)
                    @if ($order->TestBy1->PetugasUttp != null)
                <td style="min-width: 35mm">1. {{ $order->TestBy1->PetugasUttp->nama }}</td>
                <td>NIP: {{ $order->TestBy1->PetugasUttp->nip }}</td>
                    @elseif ($order->TestBy1->PetugasUut != null)
                <td style="min-width: 35mm">1. {{ $order->TestBy1->PetugasUut->nama }}</td>
                <td>NIP: {{ $order->TestBy1->PetugasUut->nip }}</td>
                    @else
                <td style="min-width: 35mm">1. </td>
                <td>NIP: </td>
                    @endif
                @else 
                <td style="min-width: 35mm"></td>
                <td></td>
                @endif
            </tr>
            @if($order->TestBy2 != null && 
                    (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                        || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
            <tr>
                @if ($order->TestBy2 != null)
                    @if ($order->TestBy2->PetugasUttp != null)
                <td>2. {{ $order->TestBy2->PetugasUttp->nama }}</td>
                <td>NIP: {{ $order->TestBy2->PetugasUttp->nip }}</td>
                    @elseif ($order->TestBy2->PetugasUut != null)
                <td>2. {{ $order->TestBy2->PetugasUut->nama }}</td>
                <td>NIP: {{ $order->TestBy2->PetugasUut->nip }}</td>
                    @else
                <td>2. </td>
                <td>NIP: </td>
                    @endif
                @else 
                <td></td>
                <td></td>
                @endif
            </tr>
            @endif
            <tr>
                <td>Tanggal Pengujian</td>
                <td>:</td>
                <td colspan="4">
                <?php
                    $start = new DateTime($order->mulai_uji);
                    $end = new DateTime($order->stat_service_order == 4 ? $order->cancel_at : $order->selesai_uji );
                    $diff = $end->diff($start);
                ?>
                @if($diff->d == 0)
                    {{ format_long_date($order->mulai_uji) }}
                @else
                @if($order->stat_service_order == 4)
                    {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->cancel_at) }}
                @else
                    {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->selesai_uji) }}
                @endif
                @endif
                </td>
            </tr>
            <tr>
                <td>Lokasi</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'Direktorat Metrologi' : $order->location }}
                </td>
            </tr>
            
        </table>  

        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Data CTMS Kapal</strong></div>
            <div class="subtitle"><i>Vessel's CTMS Data</i></div>
        </div>
        
        <br/>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>Jumlah Tangki</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->jumlah_tangki }}
                </td>
            </tr>
        </table>  

        @if($order->ctms->tank_volume_table == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Tank Volume Table</strong></div>
        </div>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th></th>
                <th>Instansi</th>
                <th>Nomor Sertifikat</th>
                <th>Tanggal</th>
            </tr>
            @foreach($order->ctmsTank as $tank)
            <tr>
                <td style="text-align: left; padding-right: 10px;">
                    @switch($tank->jenis)
                        @case('Pembuat')
                            Pembuat
                            @break
                        @case('Inspeksi')
                            Diinspeksi oleh
                            @break
                        @case('Verifikasi')
                            Diverifikasi oleh
                            @break
                        @default
                    @endswitch
                </td>
                <td style="text-align: center; padding-right: 10px;">{{ $tank->instansi }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $tank->no_sertifikat }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $tank->tanggal != null ? date('d-m-Y', strtotime($tank->tanggal)) : '' }}</td>
            </tr>
            @endforeach
        </table>
        @endif

        @if($order->ctms->radar_level_gauge == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Radar Level Gauge</strong></div>
        </div>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;" >
            <tr>
                <td>Nomor Sertifikat</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Radar Level Gauge')->no_sertifikat }}</td>
            </tr>
            <tr>
                <td>Merek/Tipe</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Radar Level Gauge')->merek }} / {{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Radar Level Gauge')->tipe }}</td>
            </tr>
        </table> 

        @if($order->ctmsGauge->where('jenis', 'Radar Level Gauge (Backup)')->count() > 0)
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Radar (Main)</strong></div>
        </div>
        @endif
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Tank</th>
                <th>Nomor Seri</th>
            </tr>
            @foreach($order->ctmsGauge->whereIn('jenis', ['Radar Level Gauge', 'Radar Level Gauge (Main)']) as $gauge)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->tank }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->serial_no }}</td>
            </tr>
            @endforeach
        </table>
        @if($order->ctmsGauge->where('jenis', 'Radar Level Gauge (Backup)')->count() > 0)
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Radar (Backup)</strong></div>
        </div>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Tank</th>
                <th>Nomor Seri</th>
            </tr>
            @foreach($order->ctmsGauge->where('jenis', 'Radar Level Gauge (Backup)') as $gauge)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->tank }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->serial_no }}</td>
            </tr>
            @endforeach
        </table>
        @endif
        @endif

        @if($order->ctms->capacitance_level_gauge == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Capacitance Level Gauge</strong></div>
        </div>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>Nomor Sertifikat</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Capacitance Level Gauge')->no_sertifikat }}</td>
            </tr>
            <tr>
                <td>Merek/Tipe</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Capacitance Level Gauge')->merek }} / {{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Capacitance Level Gauge')->tipe }}</td>
            </tr>
        </table> 
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Electrode (Main)</strong></div>
        </div>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Tank</th>
                <th>Nomor Seri (Segmen)</th>
            </tr>
            @foreach($order->ctmsGauge->where('jenis', 'Capacitance Level Gauge (Main)') as $gauge)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->tank }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->serial_no }}</td>
            </tr>
            @endforeach
        </table>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Electrode (Backup)</strong></div>
        </div>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Tank</th>
                <th>Nomor Seri (Segmen)</th>
            </tr>
            @foreach($order->ctmsGauge->where('jenis', 'Capacitance Level Gauge (Backup)') as $gauge)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->tank }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->serial_no }}</td>
            </tr>
            @endforeach
        </table>
        @endif

        @if($order->ctms->float_level_gauge == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Float Level Gauge</strong></div>
        </div>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>Nomor Sertifikat</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Float Level Gauge')->no_sertifikat }}</td>
            </tr>
            <tr>
                <td>Merek/Tipe</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Float Level Gauge')->merek }} / {{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Float Level Gauge')->tipe }}</td>
            </tr>
        </table> 
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Tank</th>
                <th>Nomor Seri</th>
            </tr>
            @foreach($order->ctmsGauge->where('jenis', 'Float Level Gauge') as $gauge)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->tank }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->serial_no }}</td>
            </tr>
            @endforeach
        </table>
        @endif

    </section>
    @endif

    @if($order->ServiceRequestItem->uttp->type->kelompok == 'CTMS BBM')
    <div class="page-break"></div>
    <section class="sheet-paged watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <div style="padding-top: 25.4mm; padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>DATA PENGUJIAN</strong></div>
            <div class="subtitle"><i>Testing Data</i></div>
        </div>

        <br/>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                @if($order->TestBy2 != null && 
                    (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                        || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
                <td rowspan="2">Pegawai Berhak</td>
                <td rowspan="2">:</td>
                @else
                <td>Pegawai Berhak</td>
                <td>:</td>
                @endif
                @if ($order->TestBy1 != null)
                    @if ($order->TestBy1->PetugasUttp != null)
                <td style="min-width: 35mm">1. {{ $order->TestBy1->PetugasUttp->nama }}</td>
                <td>NIP: {{ $order->TestBy1->PetugasUttp->nip }}</td>
                    @elseif ($order->TestBy1->PetugasUut != null)
                <td style="min-width: 35mm">1. {{ $order->TestBy1->PetugasUut->nama }}</td>
                <td>NIP: {{ $order->TestBy1->PetugasUut->nip }}</td>
                    @else
                <td style="min-width: 35mm">1. </td>
                <td>NIP: </td>
                    @endif
                @else 
                <td style="min-width: 35mm"></td>
                <td></td>
                @endif
            </tr>
            @if($order->TestBy2 != null && 
                    (($order->test_by_2_sertifikat == true && $order->ServiceRequest->lokasi_pengujian == 'luar')
                        || $order->ServiceRequest->lokasi_pengujian == 'dalam'))
            <tr>
                @if ($order->TestBy2 != null)
                    @if ($order->TestBy2->PetugasUttp != null)
                <td>2. {{ $order->TestBy2->PetugasUttp->nama }}</td>
                <td>NIP: {{ $order->TestBy2->PetugasUttp->nip }}</td>
                    @elseif ($order->TestBy2->PetugasUut != null)
                <td>2. {{ $order->TestBy2->PetugasUut->nama }}</td>
                <td>NIP: {{ $order->TestBy2->PetugasUut->nip }}</td>
                    @else
                <td>2. </td>
                <td>NIP: </td>
                    @endif
                @else 
                <td></td>
                <td></td>
                @endif
            </tr>
            @endif
            <tr>
                <td>Tanggal Pengujian</td>
                <td>:</td>
                <td colspan="4">
                <?php
                    $start = new DateTime($order->mulai_uji);
                    $end = new DateTime($order->stat_service_order == 4 ? $order->cancel_at : $order->selesai_uji );
                    $diff = $end->diff($start);
                ?>
                @if($diff->d == 0)
                    {{ format_long_date($order->mulai_uji) }}
                @else
                @if($order->stat_service_order == 4)
                    {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->cancel_at) }}
                @else
                    {{ format_long_date($order->mulai_uji) . ' - ' . format_long_date($order->selesai_uji) }}
                @endif
                @endif
                </td>
            </tr>
            <tr>
                <td>Lokasi</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'Direktorat Metrologi' : $order->location }}
                </td>
            </tr>
            
        </table>  

        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Data CTMS Kapal</strong></div>
            <div class="subtitle"><i>Vessel's CTMS Data</i></div>
        </div>
        
        <br/>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>Jumlah Tangki</td>
                <td>:</td>
                <td colspan="4">
                {{ $order->ttu->jumlah_tangki }}
                </td>
            </tr>
        </table>  

        @if($order->ctms->tank_volume_table == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Tank Volume Table</strong></div>
        </div>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th></th>
                <th>Instansi</th>
                <th>Nomor Sertifikat</th>
                <th>Tanggal</th>
            </tr>
            @foreach($order->ctmsTank as $tank)
            <tr>
                <td style="text-align: left; padding-right: 10px;">
                    @switch($tank->jenis)
                        @case('Pembuat')
                            Pembuat
                            @break
                        @case('Inspeksi')
                            Diinspeksi oleh
                            @break
                        @case('Verifikasi')
                            Diverifikasi oleh
                            @break
                        @default
                    @endswitch
                </td>
                <td style="text-align: center; padding-right: 10px;">{{ $tank->instansi }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $tank->no_sertifikat }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $tank->tanggal != null ? date('d-m-Y', strtotime($tank->tanggal)) : '' }}</td>
            </tr>
            @endforeach
        </table>
        @endif

        @if($order->ctms->uti_meter == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Ullage Temperature Interface (UTI) Meter</strong></div>
        </div>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;"> 
            <tr style="text-align: center; width:2px;">
                <th>Nomor Sertifikat</th>
                <th>Merek</th>
                <th>Tipe</th>
                <th>Nomor Seri</th>
            </tr>
            @foreach($order->ctmsPerlengkapan->where('jenis', 'Ullage Temperature Interface (UTI) Meter') as $gauge)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->no_sertifikat }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->merek }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->tipe }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->serial_no }}</td>
            </tr>
            @endforeach
        </table>
        @endif

        @if($order->ctms->depth_tape == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Depth Tape</strong></div>
        </div>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Nomor Sertifikat</th>
                <th>Merek</th>
                <th>Tipe</th>
                <th>Nomor Seri</th>
            </tr>
            @foreach($order->ctmsPerlengkapan->where('jenis', 'Depth Tape') as $gauge)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->no_sertifikat }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->merek }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->tipe }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->serial_no }}</td>
            </tr>
            @endforeach
        </table>
        @endif

        @if($order->ctms->sistem_meter_arus_kerja == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Sistem Meter Arus Kerja</strong></div>
        </div>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th rowspan="2">No. Sertifikat</th>
                <th colspan="4">Nomor Seri</th>
            </tr>
            <tr style="text-align: center; width:2px;">
                <th>Meter Arus</th>
                <th>Pressure Transmitter</th>
                <th>Temperature Transmitter</th>
                <th>Flow Computer</th>
            </tr>
            @foreach($order->ctmsSistem as $sistem)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $sistem->no_sertifikat }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $sistem->serial_no_ma }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $sistem->serial_no_pt }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $sistem->serial_no_tt }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $sistem->serial_no_fc }}</td>
            </tr>
            @endforeach
        </table>
        @endif

        <br/>
        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Tera/Tera Ulang UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Tera/Tera Ulang UTTP' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
    @endif

    @if($order->ServiceRequestItem->uttp->type->kelompok == 'CTMS Gas')
    <div class="page-break"></div>
    <section class="sheet-paged watermark">
        <div class="text-right" style="float: right; position: absolute; padding-top: 10mm; padding-right: 25.4mm;">Lampiran {{ $order->no_sertifikat }}</div>

        <div style="padding-top: 25.4mm; ">
        @if($order->ctms->pressure_measuring_systems == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Pressure Measuring Systems</strong></div>
        </div>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>Nomor Sertifikat</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Pressure Measuring Systems')->no_sertifikat }}</td>
            </tr>
            <tr>
                <td>Merek/Tipe</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Pressure Measuring Systems')->merek }} / {{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Pressure Measuring Systems')->tipe }}</td>
            </tr>
        </table> 
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Tank</th>
                <th>Nomor Seri</th>
            </tr>
            @foreach($order->ctmsGauge->where('jenis', 'Pressure Measuring Systems') as $gauge)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->tank }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->serial_no }}</td>
            </tr>
            @endforeach
        </table>
        @endif

        @if($order->ctms->temperature_measuring_systems == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Temperature Measuring Systems</strong></div>
        </div>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>Nomor Sertifikat</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Temperature Measuring Systems')->no_sertifikat }}</td>
            </tr>
            <tr>
                <td>Merek/Tipe</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Temperature Measuring Systems')->merek }} / {{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Temperature Measuring Systems')->tipe }}</td>
            </tr>
        </table> 
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Temperature Sensor (Main)</strong></div>
        </div>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Tank</th>
                <th>Nomor Seri (Section)</th>
            </tr>
            @foreach($order->ctmsGauge->where('jenis', 'Temperature Measuring Systems (Main)') as $gauge)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->tank }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->serial_no }}</td>
            </tr>
            @endforeach
        </table>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Temperature Sensor (Backup)</strong></div>
        </div>
        <table class="table table-bordered" width="100%" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr style="text-align: center; width:2px;">
                <th>Tank</th>
                <th>Nomor Seri (Section)</th>
            </tr>
            @foreach($order->ctmsGauge->where('jenis', 'Temperature Measuring Systems (Backup)') as $gauge)
            <tr>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->tank }}</td>
                <td style="text-align: center; padding-right: 10px;">{{ $gauge->serial_no }}</td>
            </tr>
            @endforeach
        </table>
        @endif

        @if($order->ctms->trim_list_inclinometer == true)
        <br/>
        <div style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <div class="subtitle"><strong>Trim/List Inclinometer</strong></div>
        </div>
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tr>
                <td>Nomor Sertifikat</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Trim/List Inclinometer')->no_sertifikat }}</td>
            </tr>
            <tr>
                <td>Merek/Tipe</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Trim/List Inclinometer')->merek }} / {{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Trim/List Inclinometer')->tipe }}</td>
            </tr>
            <tr>
                <td>Nomor Seri</td>
                <td>:</td>
                <td>{{ $order->ctmsPerlengkapan->firstWhere('jenis', 'Trim/List Inclinometer')->serial_no }}</td>
            </tr>
        </table> 
        @endif
        </div>

        <br/>
        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 60mm;">Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y')) }}
                        <br/>Manajer Pelayanan Tera/Tera Ulang UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nama : 'Manajer Pelayanan Tera/Tera Ulang UTTP' }}<br/>
                        NIP: {{ $order->SubKoordinator != null && $order->SubKoordinator->PetugasUttp ? $order->SubKoordinator->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
    @endif

    @else

    <section class="sheet {{ $order->kabalai_date && $order->stat_service_order == 3 ? 'letterhead' : 'letterhead-draft' }}"> 

        <div class="text-right" style="font-size:18pt; padding-top: 35mm; padding-right: 25.4mm;">{{ $order->ServiceRequestItem->no_order }}</div>

        <div class="text-center">
            <div class="title"><strong>SURAT KETERANGAN</strong></div>
            <div class="subtitle">Nomor: {{ $order->no_sertifikat }}</div>
        </div>

        <br/><br/>

        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        Berdasarkan permohonan pengujian dengan No. Order {{ $order->ServiceRequestItem->no_order }} tanggal 
        {{ format_long_date($order->ServiceRequestItem->order_at) }}, untuk 
        </p>
    
        <table class="table table-borderless" style="padding-left: 25.4mm; padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td style="width: 30mm;">Alat Ukur</td>
                    <td style="width: 2mm;">:</td>
                    <td colspan="5">{{ $order->ServiceRequestItem->uttp->type->uttp_type_certificate }}</td>
                </tr>
                <tr>
                    <td>Merek</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_brand != null ?
                        $order->tool_brand  :
                        $order->ServiceRequestItem->uttp->tool_brand 
                    }}</td>
                </tr>
                <tr>
                    <td>Tipe</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_model != null ?
                        $order->tool_model :
                        $order->ServiceRequestItem->uttp->tool_model 
                    }}</td>
                </tr>
                <tr>
                    <td>Nomor Seri</td>
                    <td>:</td>
                    <td colspan="5">{{ 
                        $order->tool_serial_no != null  ?
                        $order->tool_serial_no :
                        $order->ServiceRequestItem->uttp->serial_no 
                    }}</td>
                </tr>
                
                <tr>
                    <td>Pemilik</td>
                    <td>:</td>
                    <td colspan="5">{{ $order->ServiceRequest->label_sertifikat }}</td>
                </tr>
                
            </tbody>
        <table>

        <br/>
        <p style="padding-left: 25.4mm; padding-right: 25.4mm; text-align: justify;">
        <strong>batal diuji</strong> di Instalasi Uji {{ $order->instalasi->nama_instalasi }} Direktorat Metrologi karena {{ $order->cancel_notes }}.
        </p>

        <br/>
        <table class="table table-borderless push-right" style="padding-right: 25.4mm;">
            <tbody>
                <tr>
                    <td>Bandung, {{ $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date("d-m-Y") }}
                        <br/>Kepala Balai Pengujian UTTP,
                    </td>
                </tr>
                <tr>
                    <td style="height: 20mm;">
                        @if($order->KaBalai != null && $order->stat_service_order == 3)
                        <img src='data:image/png;base64, {!! 
                            base64_encode(QrCode::format("png")
                                ->size(300)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator)) !!} '
                                style="width:100px;height:100px;">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP' }}<br/>
                        NIP: {{ $order->KaBalai != null && $order->KaBalai->PetugasUttp ? $order->KaBalai->PetugasUttp->nip : '' }}
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </section>

    @endif



</body>

</html>