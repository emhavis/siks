@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
<style type="text/css">
    table.input-table td{padding: 1px !important;}
</style>
@section('styles')

@endsection

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(array('route' => 'service.store','id' => 'form_create_request', 'enctype' => "multipart/form-data")) !!}
        <div class="panel panel-filled panel-c-success">
            <div class="panel-heading" >
                <h4>Informasi Unit Metrologi Legal</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="uml_id">Nama Unit Metrologi Legal</label>
                        {!! Form::select('uml_id', $umls, '', ['class' => 'form-control','id' => 'uml_id', 'placeholder' => 'Nama Unit Metrologi Legal', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="phone_no">No. Telepon / Fax</label>
                        {!! Form::text('phone_no', '', ['class' => 'form-control','id' => 'phone_no', 'placeholder' => 'No. Telepon / Fax', 'disabled']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="address">Alamat</label>
                        {!! Form::textarea('address', '', ['class' => 'form-control','id' => 'address', 'placeholder' => 'Alamat', 'size' => '10x5', 'disabled']) !!}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="for_sertifikat">Peruntukan Sertifikat</label>
                        {!! Form::select('for_sertifikat', array("Sendiri","Instansi Lain"), 0, ['class' => 'form-control', 'id' => 'for_sertifikat', 'placeholder' => '- Pilih Peruntukan -', 'required']) !!}
                    </div>
                </div>                
                <div class="col-md-12" id="label_sertifikat_div">
                    <div class="form-group">
                        <label for="label_sertifikat">Label Sertifikat</label>
                        {!! Form::text('label_sertifikat', '', ['class' => 'form-control','id' => 'label_sertifikat', 'placeholder' => 'Label Sertifikat']) !!}
                    </div>
                </div>
                <div class="col-md-12" id="addr_sertifikat_div">
                    <div class="form-group">
                        <label for="addr_sertifikat">Alamat Sertifikat</label>
                        {!! Form::text('addr_sertifikat', '', ['class' => 'form-control','id' => 'addr_sertifikat', 'placeholder' => 'Alamat Sertifikat']) !!}
                    </div>
                </div>
            </div>
<!--             <div class="text-right margin-b-15 margin-r-27">
                <a id="change_uml" title="ubah" href="{{ route('uml.edit', 1) }}" class="btn btn-success">Ubah Informasi UML</a>
            </div> -->
        </div>
        <div class="panel panel-filled panel-c-info">
            <div class="panel-heading" >
                <h4>Informasi Pendaftar</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="pic_name">Nama Pendaftar</label>
                        {!! Form::text('pic_name', '', ['class' => 'form-control','id' => 'pic_name', 'placeholder' => 'Nama Pendaftar', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="id_type_id">Jenis Tanda Pengenal</label>
                        {!! Form::select('id_type_id', $idTypes, null, ['class' => 'form-control', 'id' => 'id_type_id', 'placeholder' => '- Pilih Tanda Pengenal -', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                        {!! Form::text('pic_id_no', '', ['class' => 'form-control','id' => 'pic_id_no', 'placeholder' => 'Nomor ID Tanda Pengenal', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="pic_phone_no">Nomor Telepon</label>
                        {!! Form::text('pic_phone_no', '', ['class' => 'form-control','id' => 'pic_phone_no', 'placeholder' => 'Nomor Telepon', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="pic_email">Alamat Email</label>
                        {!! Form::text('pic_email', '', ['class' => 'form-control','id' => 'pic_email', 'placeholder' => 'Alamat Email', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="receipt_date">Tanggal Masuk Alat</label>
                        {!! Form::text('receipt_date', '', ['class' => 'date form-control','id' => 'receipt_date', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="estimate_date">Tanggal Perkiraan Selesai</label>
                        {!! Form::text('estimate_date', '', ['class' => 'date form-control','id' => 'estimate_date', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-filled panel-c-success">
        <div class="panel-body" id="standard_items_body">
            <div class="form-group">
                <label for="standard_code">Cari Standard Alat</label>
                <span class="input-group">
                    {!! Form::text('standard_code', '', ['id' => 'standard_code', 'class' => 'form-control', 'placeholder' => 'Nomor ID', 'autocomplete' => 'off']) !!}
                    <span class="input-group-btn">
                        <button id="search_item" type="button" class="btn btn-accent">Cari</button>
                        <a class="btn btn-success" href="{{ route('service.create') }}">
                            Buat Baru
                        </a>    
                    </span>
                </span>
            </div>
        </div>
        </div>

        <div id="standard_items"></div>
    
        <div id="total_table" style="display: none;">
            <div class="form-group">
                <label class="pull-right">TOTAL</label>
                <input readonly type="text" name="total" id="total" class="form-control" />
            </div>
        </div>

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Proses</button> 
        {!! Form::close() !!}
        </div>
    </div>
</div>
</div>

<div id="table_standard_item" style="display: none;">
    <div class="panel panel-filled panel-c-danger">
        <div class="panel-body">
            <div class="row">
                <button class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_remove_standard"><i class="fa fa-minus faa-flash"></i></button>
            </div>
            <div class="row">
                <div id="standard_item">
                    <table id="standard_item_inspeksi" class="table table-responsive-sm input-table">
                        <thead>
                            <tr>
                                <th>Jenis Inspeksi</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Satuan Harga</th>
                                <th>Jumlah Harga</th>
                                <th><button type="button" id="btn_add" class="btn btn-default btn-sm faa-parent animated-hover"><i class="fa fa-plus faa-flash"></i></button></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><select type="text" name="inspection_price_id[]" id="inspection_price_id" class="form-control"></select></td>
                                <td><input type="text" name="jumlah[]" id="jumlah" class="form-control" /></td>
                                <td><input readonly type="text" name="satuan[]" id="satuan" class="form-control" /></td>
                                <td><input readonly type="text" name="harga_satuan[]" id="harga_satuan" class="form-control" /></td>
                                <td><input readonly type="text" name="total_harga[]" id="total_harga" class="form-control" /></td>
                                <td><button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3"></td>
                                <td>Subtotal: </td>
                                <td><input readonly type="text" name="subtotal[]" id="subtotal" class="form-control" /></td>
                                <td></td>
                            </tr>                            
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.extensions.js') }}"></script>

<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        
        $('#uml_id,#for_sertifikat,#id_type_id').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $("#label_sertifikat_div,#addr_sertifikat_div").hide();

        $("#for_sertifikat").change(function(){
            if($(this).val()=="1")
            {
                $("#label_sertifikat_div,#addr_sertifikat_div").show();
            }
            else
            {
                $("#label_sertifikat_div,#addr_sertifikat_div").hide();
            }
        });

        // $('#id_type_id').select2({
        //     placeholder: "- Pilih Tanda Pengenal -",
        //     allowClear: true
        // });

        // $('.date').datepicker({
        //     format: 'dd-mm-yyyy',
        //     autoclose: true
        // });

        $('#btn_simpan').click(function(e){
            e.preventDefault();
            
            $("#alert_board").empty();

            // $("#panel_create").toggleClass("ld-loading");

            var form_data = $("#form_create_request").serialize();

            console.log(dt.inspection_price_ids);

            $.each(dt.inspection_price_ids,function(i,v)
            {
                if(v)
                {
                    form_data += '&inspection_price_ids['+i+']='+v;
                    form_data += '&inspection_price_jumlahs['+v+']='+dt.inspection_price_jumlahs[v];
                }
            });


            $.post('{{ route('service.simpan') }}',form_data,function(response){
                if(response[0]==false)
                {
                    var msg = show_notice(response[1]);
                    $("#alert_board").html(msg);
                }
                else
                {
                    // toastr["success"]("Data Berhasil di simpan", "Info");
                    window.location = '{{ route('service.index') }}';
                }

                // $("#panel_create").toggleClass("ld-loading");

            },"json");


        });

        $('#standard_items').on("click","button#btn_remove_standard",function()
        {
            var data = $(this).closest(".panel").find("#standard_item_inspeksi").data();
            dt.inspection_price_ids.splice(data.id);
            console.log(dt.inspection_price_ids);

            $(this).closest(".panel").remove();

        });

        $('#standard_items').on("click","button#btn_add",function()
        {
            var data = $(this).data();
            var tr = dt.inspection_prices[data.id];
            $(this).closest("#standard_item_inspeksi").find("tbody").append(tr.row);
        });

        $('#uml_id').change(function()
        {
            $.ajax({
                url: '{{ route('uml.getumlinfo') }}',
                type: 'post',
                data: {
                    id: this.value
                },
                beforeSend: function() {
                    $("#progress_spin").show();
                },
                success: function(response){
                    var json_response = JSON.parse(response);
                    $('#phone_no').val(json_response.phone_no);
                    $('#address').val(json_response.address);
                    $('#change_uml').attr('href','../uml/' + json_response.id + '/edit');
                },
                complete:function(data){
                    $("#progress_spin").hide();
                }
            });
        });
    
        $('#search_item').click(function(){
            $.post('{{ route('standard.umlstandardinfo') }}',{ tool_code: $('#standard_code').val() },function(response)
            {
                res = response[0];

                if(!res)
                {
                    toastr["warning"]("QR Code tidak ditemukan", "Info");
                }
                else if($("#standard_item_"+res.id,"#standard_items").length==0)
                {
                    if($("#total_table").is(":visible")==false)
                    {
                        $("#total_table").show();
                    }

                    dt.standard_items[res.id] = res;
                    var table = $("#table_standard_item").html();
                    var standard_item = '<div class="row">'
                    +'<div class="col-md-3">QR Code alat: <b class="text-warning">'+res.tool_code+'</b></div>'
                    +'<div class="col-md-3">Besaran alat: <b class="text-warning">'+res.standard_type+'</b></div>'
                    +'</div><div class="row">'
                    +'<div class="col-md-3">Jumlah per set: <b class="text-warning">'+res.jumlah_per_set+'</b></div>'
                    +'<div class="col-md-3">Jenis alat: <b class="text-warning">'+res.attribute_name+'</b></div>'
                    +'</div><div class="row">'
                    +'<div class="col-md-3"></div>'
                    +'<div class="col-md-3">Rincian alat: <b class="text-warning">'+res.standard_detail_type_name+'</b></div>'
                    +'</div>';

                    $('#standard_items').append(table);

                    $("#standard_item:last","#standard_items").prepend(standard_item);

                    $("#standard_item:last","#standard_items").attr("id","standard_item_"+res.id);

                    var table_inspection_prices = $("#standard_item_"+res.id,"#standard_items");

                    table_inspection_prices.find("#btn_add").attr("data-id",res.id);

                    table_inspection_prices.find("#standard_item_inspeksi").attr("data-id",res.id);


                    $.post('{{ route('standard.inspectionpriceinfo')}}',{"standard_detail_type_id":res.standard_detail_type_id},function(response2)
                    {
                        if(response2.length==0)
                        {
                            toastr["warning"]("Item Inspeksi tidak ditemukan", "Info");
                        }
                        else
                        {
                            var standard_drop_list = $('#inspection_price_id',table_inspection_prices);
                            standard_drop_list.empty();

                            standard_drop_list.append("<option>- Pilih Inspeksi -</option>");
                            $.each(response2, function(index, element)
                            {
                                dt.inspection_prices[element.id] = element;
                                standard_drop_list.append("<option value='"+ element.id +"'>" + element.inspection_type + "</option>");
                            });

                            dt.inspection_prices[res.id]= {"row":table_inspection_prices.find("tbody").html()};
                            console.log(dt.inspection_prices);
                        }

                    },"json");

                }

            },"json");
        });

        // $("#inspection_price_id","#standard_items").on("change",function(){
            
        //     console.log(this.value);

        // });

        $('#standard_items').on("click","button#btn_remove",function()
        {
            var data = $(this).closest("#standard_item_inspeksi").data();
            var id = $(this).closest("#standard_item_inspeksi").find("select#inspection_price_id").val();
            dt.inspection_price_ids[data.id].splice(id);
            console.log(dt.inspection_price_ids[data.id]);
            $(this).parent().parent().remove();
            ctotal(data.id);
        });

        $('#standard_items').on("change","table#standard_item_inspeksi select#inspection_price_id",function()
        {
            var id = this.value;
            var tr = $(this).closest("tr");
            // var table = $(this).parent().parent().parent().parent();
            var table = $(this).closest("#standard_item_inspeksi");
            var standard_id = table.data("id");

            var total_harga = parseInt(dt.standard_items[standard_id].jumlah_per_set)*parseInt(dt.inspection_prices[id].price);

            if(parseInt(id)>0)
            {
                tr.find("#jumlah").val(dt.standard_items[standard_id].jumlah_per_set);
                tr.find("#satuan").val(dt.inspection_prices[id].measurement_unit);
                tr.find("#harga_satuan").val(dt.inspection_prices[id].price).decimal();
                tr.find("#total_harga").val(total_harga).decimal();
            }

            ctotal(standard_id);
        });

        $('#standard_items').on("keyup","table#standard_item_inspeksi input#jumlah",function()
        {
            console.log(this.value);

            var table = $(this).closest("#standard_item_inspeksi");
            var standard_id = table.data("id");

            var tr = $(this).closest("tr");
            var id = tr.find("select#inspection_price_id").val();


            if(parseInt(this.value)>0)
            {
                var total_harga = parseInt(this.value)*parseInt(dt.inspection_prices[id].price);
                // tr.find("#jumlah").val(dt.standard_items[standard_id].jumlah_per_set);
                // tr.find("#satuan").val(dt.inspection_prices[id].measurement_unit);
                // tr.find("#harga_satuan").val(dt.inspection_prices[id].price).decimal();
                tr.find("#total_harga").val(total_harga).decimal();
            }

            ctotal(standard_id);
        });

        $('#standard_code').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                $('#search_item').click();
                return false;
            }
        });
    });

function ctotal(id)
{
    var total = 0;
    dt.inspection_price_ids[id] = new Array();

    $("input#total_harga","#standard_item_"+id).each(function(i,input)
    {
        if($(input).getInt()>0)
        {
            total = total + parseInt($(input).getInt());

            var inspection_price_id = $(this).parent().parent().find("#inspection_price_id").val();

            dt.inspection_price_ids[id].push(inspection_price_id);
            // dt.inspection_price_jumlahs[id].push(jumlah);
        }
    });

    $("input#jumlah","#standard_item_"+id).each(function(i,input)
    {
        if($(input).getInt()>0)
        {
            var inspection_price_id = $(this).parent().parent().find("#inspection_price_id").val();

            dt.inspection_price_jumlahs[inspection_price_id] = this.value;
        }
    });

    console.log(dt.inspection_price_ids);

    $("input#subtotal","#standard_item_"+id).val(total).decimal();

    stotal();
}

function stotal()
{
    var total = 0;

    $("input#subtotal","#form_create_request").each(function(i,input)
    {
        if($(input).getInt()>0)
        {
            total = total + parseInt($(input).getInt());
        }
    });

    $("input#total","#form_create_request").val(total).decimal();
}

function show_notice(msg)
{
    var errmessage = {
        'uml_id':'Nama Unit Metrologi Legal wajib diisi',
        'receipt_date':'Tanggal Masuk Alat wajib diisi',
        'estimate_date':'Tanggal Perkiraan Selesai wajib diisi',
        'label_sertifikat':'Label Sertifikat wajib diisi',
        'addr_sertifikat':'Label Sertifikat wajib diisi',
        'pic_name':'Nama Pendaftar wajib diisi',
        'pic_phone_no':'Nomor Telepon wajib diisi',
        'pic_email':'Alamat Email wajib diisi',
        'id_type_id':'Jenis Tanda Pengenal wajib diisi',
        'pic_id_no':'Nomor ID Tanda Pengenal wajib diisi',
        'standard_code':'Minimal Satu Standard Tool wajib diisi'
    };

    $(document).find("small.wajib-isi").remove();

    var str = '<div class="panel panel-filled panel-c-danger">'+
        '<div class="panel-body">'+
            '<b>Terjadi kesalahan.</b>'+
            'Telah terjadi kesalahan, hubungi administrator untuk info lebih lanjut.'+
            '<ul>';
            $.each(msg,function(i,v){
                str += '<li>'+errmessage[i]+'</li>';
                $(document).find("label[for='"+i+"']").append("<small class='text-warning wajib-isi'> wajib diisi</small>");
            });
    str += '</ul>'+
        '</div>'+
    '</div>';

    return str;
}

$.fn.extend({
    decimal: function()
    {
        this.inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'});

    },
    getInt: function()
    {
        var a = $(this).val();
        return parseFloat(a.replace(/,/g,""));
    }
});
</script>
@endsection