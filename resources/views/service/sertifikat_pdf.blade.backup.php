@php
function into_tanggal($tanggal)
{
    $bulan = array (1=>'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

@endphp

<script type="text/php">
    var_dump($pdf);
    if ( isset($pdf) ) {
        $font = Font_Metrics::get_font("helvetica", "bold");
        $pdf->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 6, array(0,0,0));
    }
</script>

<html>
<head>
<title>Sertifikat</title>
<style type="text/css">

    @page
    {
        size: 21cm 29.7cm portrait;
        margin: 1cm 1cm 1cm 2cm;
    }

    header {
        position: fixed;
        top: -10px;
        left: 0px;
        right: 0px;
        height: 50px;

        /** Extra personal styles **/
        text-align: right;
        font-size: 10pt;
    }

    footer {
        position: fixed; 
        bottom: -30px; 
        left: 0px; 
        right: 0px;
        height: 50px; 

        /** Extra personal styles **/
        text-align: center;
        font-size: 10pt;
    }

    footer .page-number:after {
        content: "Halaman " counter(page) " dari " counter(pageTotal);
    }

    table.page-break{
      page-break-before:auto;
      page-break-after: always;
    }

    table tr td,
    table tr th{
        font-size: 9pt;
        vertical-align: top;
    }

    body,table,p,h1,h2,h3,h4,h5
    {
        margin: 0px;
        font-family: Arial, Helvetica, sans-serif;
    }

    .h-lighter h3,h4
    {
        font-weight: lighter;
    }
    table#hasil_uji td
    {
        height: 5mm;
    }
    #watermark {
    position: fixed;
    font-size: 100pt;
    font-weight: bold;
    color: #ccc;
    top: 45%;
    width: 100%;
    text-align: center;
    opacity: .6;
    transform: rotate(-10deg);
    transform-origin: 50% 50%;
    z-index: -1000;
    }
</style>
</head>
<body>

<div style="page-break-after: always;">

@if(Auth::user()->user_role=="3" || Auth::user()->user_role=="8")
<div id="watermark">DRAFT</div>
@endif

    <table  width="100%">
        <tr>
            <td style="width: 3cm; border-right: #000 solid 3px; padding-right: 3px; margin-right:3px;vertical-align: bottom;" rowspan="3">
                <img src="{{ asset('assets/images/logo/logo_kemendag_sertifikat.png') }}" width="90%" />
            </td>
            <td style="height: 2cm; text-align: right;" colspan="2">
                @if($data->no_sertifikat)
                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->margin(0)->size(70)->errorCorrection('H')->generate($data->no_sertifikat)) !!} ">
                @endif
            </td>
        </tr>
        <tr>
            <td style="height: 1cm;" colspan="2">
                <h3>DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN TERTIB NIAGA</h3>
                <h3>DIREKTORAT METROLOGI</h3>
            </td>
        </tr>
        <tr>
            <td style="">
                <p>Jl. Pasteur No. 27 Bandung 40171</p>
                <p>Telp. (022) 4203597 Hunting, Fax(022) 4207035</p>
            </td>
            <td style="width: 3cm;">
                <img src="{{ asset('assets/images/logo/logo-kan.jpg') }}" width="80%" />
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="height: 0.5cm;text-align: center;" colspan="3">
                <h1><u>SERTIFIKAT KALIBRASI</u></h1>
                <h4><i>Calibration Certificate</i></h4>
            </td>
        </tr>
        <tr>
            <td style="height: 0.8cm;text-align: center; vertical-align: bottom;" colspan="3"><h2>Nomor : {{ $data->no_sertifikat }}</h2></td>
        </tr>
    </table>
    <br>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 65%;">
                <h3>Nama Alat :  {{ $data->attribute_name }}</h3>
                <h4><i>Calibration Certificate</i></h4>
            </td>
            <td style="text-align: center;vertical-align: middle;border: #000 solid 1px;">
                <h3>No Order :</h3>
            </td>
            <td style="text-align: center;border: #000 solid 1px;">
                <h3><u>{{ $data->no_order }}</u></h3>
                <h3>{{ $data->receipt_date }}</h3>
            </td>
        </tr>
    </table>
    <br>
    <table style="margin-left: 20px;" width="100%" cellpadding="0" cellspacing="0" class="h-lighter">
        <tr>
            <td style="width: 30%;">
                <h3>Merk / Buatan</h3>
                <h4><i>Trade Mark / Manufactured By</i></h4>                
            </td>
            <td><h3>: {{ $data->brand }}</h3></td>
        </tr>
        <tr>
            <td>
                <h3>Model / Tipe</h3>
                <h4><i>Model / Type</i></h4>                
            </td>
            <td><h3>: {{ $data->model }}</h3></td>
        </tr>
        <tr>
            <td>
                <h3>Nomor Seri / Identitas</h3>
                <h4><i>Serial Number / Identity</i></h4>                
            </td>
            <td><h3>: {{ $data->no_identitas }}</h3></td>
        </tr>
        <tr>
            <td>
                <h3>Kapasitas / Daya Baca</h3>
                <h4><i>Capacity / Readability</i></h4>                
            </td>
            <td><h3>: {{ $data->capacity }}</h3></td>
        </tr>
        <tr>
            <td>
                <h3>Kelas</h3>
                <h4><i>Class</i></h4>                
            </td>
            <td><h3>: {{ $data->class }}</h3></td>
        </tr>
    </table>
    <br>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3>Pemilik</h3>
                <h4><i>User</i></h4>
            </td>
            <td style="width: 1%;">:</td>
            <td style="width: 85%;">
                @if($data->for_sertifikat=="1")
                <h3>{{ $data->label_sertifikat }}</h3>
                <h3>{{ $data->addr_sertifikat }}</h3>
                @else
                <h3>{{ $data->nama_uml }}</h3>
                <h3>{{ $data->alamat }}</h3>
                @endif
            </td>
        </tr>
    </table>
    <br>
    <table class="h-lighter" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3>Sertifikat ini terdiri dari 2 (dua) halaman</h3>
                <h4><i>This certificate consists of 2 (two) pages</i></h4>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table style="margin-left: 300px;" class="h-lighter" width="80%" cellpadding="0" cellspacing="0">
        <tr>
            <td></td>
            <td>
                <h3>Bandung, {{ into_tanggal($data->ditmet_entry_date) }}</h3>
                <br>
            </td>
        </tr>
        <tr>        
            <td style="vertical-align: top;width: 1%;">a.n.</td>
            <td>
                <h3>Direktur Metrologi</h3>
                <h3>Kepala Balai Pengelolaan</h3>
                <h3>Standar Nasional Satuan Ukuran,</h3>
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="height: 2cm; vertical-align: bottom;"><h3>Aen Jueni, S.Si</h3></td>
        </tr>
    </table>
    <footer>
        <p>Dilarang menggandakan sebagian dari isi sertifikat ini tanpa izin tertulis dari Direktorat Metrologi Bandung</p><p>FO.BSNSU.SERT.01.00</p>
    </footer>
</div>

<!-- <div style="page-break-befo:after"> -->
@if(Auth::user()->user_role=="3" || Auth::user()->user_role=="8")
<div id="watermark">DRAFT</div>
@endif
    <header>
        Lampiran Sertifikat No. {{ $data->no_sertifikat }}
    </header>

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3>METODE, STANDAR DAN TELUSURAN</h3>
                <h4><i>Method, Standard and Traceability</i></h4>
            </td>
        </tr>
    </table>
    <br>
    <table style="margin-left: 20px;" width="100%" cellpadding="0" cellspacing="0" class="h-lighter">
        <tr>
            <td style="width: 20%;">
                <h3>- Metode</h3>
            </td>
            <td>
                @php
                foreach($metodeUji as $metode)
                {
                    echo "<h3>: ".$metode->nama_metode."</h3>";
                }
                @endphp
            </td>
        </tr>
        <tr>
            <td>
                <h3>- Standar</h3>
            </td>
            <td>
                @php
                foreach($standarUkuran as $standar)
                {
                    echo "<h3>: ".$standar->nama_standar_ukuran."</h3>";
                }
                @endphp
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>Tertelusur ke Satuan Pengukuran SI melalui {{ $data->tertelusur }}</h3>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3>DATA KALIBRASI</h3>
                <h4><i>Calibration Data</i></h4>
            </td>
        </tr>
    </table>
    <br>
    <table style="margin-left: 20px;" width="100%" cellpadding="0" cellspacing="0" class="h-lighter">
        <tr>
            <td style="width: 20%;">
                <h3>- Referensi</h3>
            </td>
            <td><h3>: PT. Pertamina Gas, tanggal 4 Januari 2019</h3></td>
        </tr>
        <tr>
            <td>
                <h3>- Tanggal Kalibrasi</h3>
            </td>
            <td><h3>: {{ into_tanggal($data->staff_entry_datein) }}</h3></td>
        </tr>
        <tr>
            <td>
                <h3>- Dikalibrasikan oleh</h3>
            </td>
            <td><h3>: M. Kholid Mawardi, S.T., M.T.</h3></td>
        </tr>
        <tr>
            <td>
                <h3>- Lokasi</h3>
            </td>
            <td><h3>: Lab. {{ $data->nama_lab }} Direktorat Metrologi</h3></td>
        </tr>
        <tr>
            <td>
                <h3>- Kondisi Ruangan</h3>
            </td>
            <td><h3>: Suhu : {{ $data->suhu_ruang }}</h3></td>
        </tr>
        <tr>
            <td></td>
            <td><h3>  Kelembapan : {{ $data->lembab_ruang }}</h3></td>
        </tr>
    </table>
    <br>
    <table width="100%" cellpadding="0" cellspacing="0" id="hasil_uji">
        <tr>
            <td>
                <h3>HASIL</h3>
            </td>
        </tr>
    </table>

        <?php
        $hasil_uji = json_decode($data->hasil_uji,true);
        $merges = json_decode($data->report_setting,true);
        $count_report = explode(",",$data->tmp_report_id);
        $y = 0;
        foreach($mtmp_report as $mtmp)
        {
            echo '<table width="100%" style="page-break-after:auto;margin: 30px 0;" cellpadding="5" cellspacing="0" border="1">';

            $column_setting = json_decode($mtmp->column_tmp,true);
            $data_uji = $hasil_uji["hot_".$mtmp->id];
            $data_map = array();
            $data_merge = array();
            $data_restrict = array();
            if(isset($merges["merge_".$mtmp->id]))
            {
                $data_merge = $merges["merge_".$mtmp->id];
            }

            foreach($data_merge as $row)
            {
                for($x=0;$x<$row["rowspan"];$x++)
                {
                    for($i=0;$i<$row["colspan"];$i++)
                    {
                        $rowx = $x+intval($row["row"]);
                        $rowi = $i+intval($row["col"]);
                        $data_restrict[] = $rowx.",".$rowi;
                    }
                }
                $data_map[$row["row"]][$row["col"]]["rowspan"] = $row["rowspan"];
                $data_map[$row["row"]][$row["col"]]["colspan"] = $row["colspan"];
            }

            for($baris=0; $baris < count($data_uji); $baris++)
            {
                $td_count = array();
                $td_string = '';
                // echo '<tr>';
                for($kolom=0; $kolom < count($data_uji[$baris]); $kolom++)
                {
                    if($baris<2)
                    {
                        $td = "th";
                        $style = 'style="text-align:center; background-color: #ccc;"';
                    }
                    else
                    {
                        $td = "td";
                        $style = "";
                    }

                    $bariskolom = $baris.",".$kolom;
                    $td_count[] = strlen($data_uji[$baris][$kolom]);
                    if(isset($data_map[$baris][$kolom]))
                    {
                        $rowspan = intval($data_map[$baris][$kolom]["rowspan"]);
                        $colspan = intval($data_map[$baris][$kolom]["colspan"]);
                        $td_string .= '<'.$td.' '.$style.' colspan="'.$colspan.'" rowspan="'.$rowspan.'">'.$data_uji[$baris][$kolom].'</'.$td.'>';
                    }
                    else if(!in_array($bariskolom, $data_restrict))
                    {
                        // $td_string .= '<'.$td.' '.$style.'>'.array_sum($td_count).'</'.$td.'>';
                        $td_string .= '<'.$td.' '.$style.'>'.$data_uji[$baris][$kolom].'</'.$td.'>';
                    }

                }
                if(array_sum($td_count)>0) echo '<tr>'.$td_string.'</tr>';
            }
            echo '</table>';
        }

        ?>
    <br>

    <table class="h-lighter" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3><i>Catatan :</i></h3>
                <h3><i>Ketidakpastian pengukuran dinyatakan pada tingkat</i></h3>
                <h3><i>kepercayaan sekitar 95% dengan faktor cakupan k=2</i></h3>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table style="margin-left: 300px; text-align: center;" class="h-lighter" width="80%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3>Kepala Pelayanan Teknis,</h3>
            </td>
        </tr>
        <tr>
            <td style="height: 2cm;vertical-align: bottom;"><h3>Aen Jueni, S.Si</h3></td>
        </tr>
    </table>
    <footer>
        <p>Dilarang menggandakan sebagian dari isi sertifikat ini tanpa izin tertulis dari Direktorat Metrologi Bandung</p>
        <p class="page-number"></p>
    </footer>
<!-- </div> -->

</body>
</html>
