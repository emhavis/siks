@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
<style type="text/css">
    table.input-table td{padding: 1px !important;}
</style>
@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>Layanan<br><span class="c-white">Verifikasi Standar</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-check"></i>
            </div>
            <div class="header-title">
                <h3>Laboratory Out</h3>
                <small>
                    Halaman pendaftaran layanan verifikasi standar.
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(array('route' => 'service.labin_simpan','id' => 'form_create_labout', 'enctype' => "multipart/form-data")) !!}
        <div class="panel panel-filled panel-c-success">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="laboratory_id">Laboratory</label>
                        {!! Form::select('laboratory_id', $mlaboratory, '', ['class' => 'form-control','id' => 'laboratory_id', 'placeholder' => 'Nama Laboratorium', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="service_order_id">QR Code</label>
                        {!! Form::select('service_order_id', array(), '', ['class' => 'form-control','id' => 'service_order_id', 'placeholder' => 'QR CODE', 'required']) !!}
                    </div>
                </div>
        </div>

    </div>
    <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Proses</button> 
    {!! Form::close() !!}
    </div>
</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.extensions.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#laboratory_id, #service_order_id").select2();

        $('#laboratory_id').change(function(){
            $.ajax({
                url: '{{ route('service.get_service_order_outs') }}',
                type: 'post',
                dataType:"JSON",
                data: {
                    laboratory_id: this.value
                },
                success: function(response)
                {
                    console.log(response);

                    var service_order_id = $('#service_order_id');
                    service_order_id.empty();

                    service_order_id.append("<option value=''>QR CODE</option>");
                    $.each(response, function(index, element)
                    {
                        console.log(index);
                        console.log(element);

                        service_order_id.append("<option value='"+ element.id +"'>" + element.tool_code + "</option>");
                    });
                }
            });
        });

        $('#btn_simpan').click(function(e)
        {
            e.preventDefault();
            
            var form_data = $("#form_create_labout").serialize();

            $.post('{{ route('service.labhead_simpan') }}',form_data,function(response)
            {
                if(response[0]==false)
                {
                    var msg = show_notice(response[1]);
                    $("#alert_board").html(msg);
                }
                else
                {
                    window.location = '{{ route('service.order_approve') }}';
                }

            },"json");


        });

    });

function show_notice(msg)
{
    var errmessage = {
        'laboratory_id':'Laboratorium wajib diisi',
        'service_order_id':'No. Order wajib diisi'
    };

    $(document).find("small.wajib-isi").remove();
            var str = '<div class="panel panel-filled panel-c-danger">'+
        '<div class="panel-body">'+
            '<b>Terjadi kesalahan.</b>'+
            'Telah terjadi kesalahan, hubungi administrator untuk info lebih lanjut.'+
            '<ul>';
            $.each(msg,function(i,v)
            {
                if(errmessage[i])
                {
                    str += '<li>'+errmessage[i]+'</li>';
                    $(document).find("label[for='"+i+"']").append("<small class='text-warning wajib-isi'> wajib diisi</small>");
                }else{
                    str += '<li>'+v+'</li>';
                }
            });
    str += '</ul>'+
        '</div>'+
    '</div>';

    return str;
}

</script>
@endsection