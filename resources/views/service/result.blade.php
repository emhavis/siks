@extends('layouts.app')
<link href="{{ asset('assets/handsontable/handsontable.full.min.css') }}" rel="stylesheet" media="screen">
<style type="text/css">

td, th, textarea {
    color:#000;
    border-color: #000;
    text-align: left;
}

</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        {!! Form::open(array('id' => 'form_create_labin', 'enctype' => "multipart/form-data")) !!}
        <input type="hidden" name="id" value="{{ $serviceOrders->id }}" />
        <div class="col-md-6">
            <div class="form-group">
                <label for="tmp_report_id">Pilih Template</label>
                {!! Form::select('tmp_report_id', $template_format_item, $serviceOrders->tmp_report_id, ['class' => 'form-control select2','id' => 'tmp_report_id', 'required','onchange'=>'generateHOT(this)']) !!}
            </div>
            <button role="button" class="btn btn-w-md btn-accent" id="btn_recount">Re-Count</button>
            <button role="button" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
</div>
<div id="hot_container"></div>
<div id="hot_container_custom"></div>
<div style="display:none">
    <div id="select_metodeuji_id">
        <div class="form-group">
            <label for="metodeuji_id">Metode Uji</label>
            {!! Form::select('metodeuji_id', $mMetodeUji, "", ['class' => 'form-control multiselect2','id' => 'metodeuji_id', 'required', 'multiple'=>'multiple']) !!}
        </div>
    </div>
    <div id="select_masscomp">
        <div class="form-group">
            <label for="masscomp">MassComp</label>
            {!! Form::select('masscomp', $masscomp, "0", ['class' => 'form-control select2','id' => 'masscomp', 'required',"onchange='getData(this)'"]) !!}
        </div>
    </div>
    <div id="select_massajenis">
        <div class="form-group">
            <label for="massajenis">Class</label>
            {!! Form::select('massajenis', $massajenis, "0", ['class' => 'form-control select2','id' => 'massajenis', 'required', "onchange='getData(this)'"]) !!}
        </div>
    </div>
    <div id="select_reference">
        <div class="form-group">
            <label for="reference">Reference Standard Class</label>
            {!! Form::select('reference', $serialnumber, "0", ['class' => 'form-control select2','id' => 'reference', 'required', 'required', "onchange='getData(this)'"]) !!}
        </div>
    </div>
    <div id="select_tertelusur">
        <div class="form-group">
            <label for="tertelusur">Tertelusur melalui</label>
            {!! Form::select('tertelusur', ["0"=>"-- Pilih --","SNSU"=>"SNSU","Direktorat Metrologi"=>"Direktorat Metrologi"], "0", ['class' => 'form-control select2','id' => 'tertelusur', 'required',"onchange='getData(this)'"]) !!}
        </div>
    </div>
    <div id="select_technician">
        <div class="form-group">
            <label for="technician">Technician Ass.</label>
            {!! Form::select('technician', ["0"=>"-- Pilih --","Oki Sri Swastini"=>"Oki Sri Swastini","Larisa Deviyani"=>"Larisa Deviyani","Ruseno"=>"Ruseno"], "0", ['class' => 'form-control select2','id' => 'technician', 'required',"onchange='getData(this)'"]) !!}
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form id="form_modal">
            <div class="modal-body" id="modal_body"></div>
            <div class="modal-footer">
                <button type="button" id="pilih" class="btn btn-accent">Pilih</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/handsontable/handsontable.full.min.js') }}"></script>
<script src="{{ asset('assets/vendor/generate.js') }}"></script>
<script type="text/javascript">

myhot   = new MyHot();
myform  = new MyForm({id:"form_create_labin",is_edit:"false",urlsubmit:'{{ route('service.simpan',$serviceOrders->id) }}',urlsuccess:'{{ route('service') }}'});
let hots,rowIndex,colIndex,id_template;


var template_setting1 =
{
    "12":
    {
        "startCols": 5,
        "color":"#000",
        "className": "htCenter htMiddle",
        "mergeCells": [
            {"row": 0,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 0,"col": 3,"rowspan": 1,"colspan": 8},
            {"row": 0,"col": 11,"rowspan": 1,"colspan": 4},

            {"row": 1,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 1,"col": 3,"rowspan": 1,"colspan": 8},
            {"row": 1,"col": 11,"rowspan": 2,"colspan": 4},

            {"row": 2,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 2,"col": 3,"rowspan": 1,"colspan": 8},

            {"row": 3,"col": 0,"rowspan": 1,"colspan": 15},

            {"row": 4,"col": 0,"rowspan": 1,"colspan": 6},
            {"row": 4,"col": 6,"rowspan": 1,"colspan": 9},
            {"row": 4,"col": 15,"rowspan": 1,"colspan": 11},

            {"row": 5,"col": 1,"rowspan": 1,"colspan": 2},
            {"row": 5,"col": 4,"rowspan": 1,"colspan": 2},
            {"row": 5,"col": 6,"rowspan": 1,"colspan": 3},
            {"row": 5,"col": 9,"rowspan": 1,"colspan": 6},
            {"row": 5,"col": 15,"rowspan": 1,"colspan": 4},
            {"row": 5,"col": 19,"rowspan": 1,"colspan": 6},

            {"row": 6,"col": 1,"rowspan": 1,"colspan": 5},
            {"row": 6,"col": 6,"rowspan": 1,"colspan": 3},
            {"row": 6,"col": 9,"rowspan": 1,"colspan": 6},
            {"row": 6,"col": 15,"rowspan": 1,"colspan": 4},
            {"row": 6,"col": 19,"rowspan": 1,"colspan": 2},
            {"row": 6,"col": 22,"rowspan": 2,"colspan": 1},
            {"row": 6,"col": 23,"rowspan": 2,"colspan": 2},
            {"row": 6,"col": 25,"rowspan": 2,"colspan": 1},

            {"row": 7,"col": 1,"rowspan": 1,"colspan": 5},
            {"row": 7,"col": 6,"rowspan": 1,"colspan": 3},
            {"row": 7,"col": 15,"rowspan": 1,"colspan": 4},
            {"row": 7,"col": 19,"rowspan": 1,"colspan": 2},

            {"row": 8,"col": 6,"rowspan": 1,"colspan": 3},
            {"row": 8,"col": 15,"rowspan": 1,"colspan": 4},
            {"row": 8,"col": 19,"rowspan": 1,"colspan": 2},
            {"row": 8,"col": 23,"rowspan": 1,"colspan": 2},

            {"row": 9,"col": 1,"rowspan": 1,"colspan": 5},
            {"row": 9,"col": 6,"rowspan": 1,"colspan": 3},
            {"row": 9,"col": 15,"rowspan": 1,"colspan": 4},
            {"row": 9,"col": 19,"rowspan": 1,"colspan": 2},
            {"row": 9,"col": 23,"rowspan": 1,"colspan": 2},

            {"row": 10,"col": 0,"rowspan": 1,"colspan": 15},

            {"row": 11,"col": 1,"rowspan": 1,"colspan": 5},
            {"row": 11,"col": 6,"rowspan": 1,"colspan": 3},
            {"row": 11,"col": 9,"rowspan": 1,"colspan": 6},

            {"row": 12,"col": 0,"rowspan": 1,"colspan": 15},
            {"row": 13,"col": 0,"rowspan": 1,"colspan": 15},

            {"row": 14,"col": 1,"rowspan": 1,"colspan": 4},
            {"row": 14,"col": 6,"rowspan": 1,"colspan": 3},
            {"row": 14,"col": 9,"rowspan": 1,"colspan": 2},
            {"row": 14,"col": 12,"rowspan": 1,"colspan": 2},

            {"row": 15,"col": 1,"rowspan": 1,"colspan": 4},
            {"row": 15,"col": 6,"rowspan": 1,"colspan": 3},
            {"row": 15,"col": 9,"rowspan": 1,"colspan": 2},
            {"row": 15,"col": 12,"rowspan": 1,"colspan": 2},

            {"row": 16,"col": 1,"rowspan": 1,"colspan": 4},
            {"row": 16,"col": 6,"rowspan": 1,"colspan": 3},
            {"row": 16,"col": 9,"rowspan": 1,"colspan": 2},
            {"row": 16,"col": 12,"rowspan": 1,"colspan": 2},

            {"row": 17,"col": 0,"rowspan": 1,"colspan": 15},
            {"row": 18,"col": 0,"rowspan": 1,"colspan": 15},

            {"row": 19,"col": 1,"rowspan": 1,"colspan": 2},
            {"row": 19,"col": 3,"rowspan": 1,"colspan": 2},
            {"row": 19,"col": 5,"rowspan": 1,"colspan": 2},
            {"row": 19,"col": 7,"rowspan": 1,"colspan": 2},
            {"row": 19,"col": 9,"rowspan": 1,"colspan": 2},
            {"row": 19,"col": 11,"rowspan": 1,"colspan": 4},

            {"row": 20,"col": 1,"rowspan": 1,"colspan": 2},
            {"row": 20,"col": 3,"rowspan": 1,"colspan": 2},
            {"row": 20,"col": 5,"rowspan": 1,"colspan": 2},
            {"row": 20,"col": 7,"rowspan": 1,"colspan": 2},
            {"row": 20,"col": 9,"rowspan": 1,"colspan": 2},
            {"row": 20,"col": 11,"rowspan": 1,"colspan": 4},

            {"row": 21,"col": 1,"rowspan": 1,"colspan": 2},
            {"row": 21,"col": 3,"rowspan": 1,"colspan": 2},
            {"row": 21,"col": 5,"rowspan": 1,"colspan": 2},
            {"row": 21,"col": 7,"rowspan": 1,"colspan": 2},
            {"row": 21,"col": 9,"rowspan": 1,"colspan": 2},
            {"row": 21,"col": 11,"rowspan": 1,"colspan": 4},

            {"row": 22,"col": 1,"rowspan": 1,"colspan": 8},
            {"row": 22,"col": 9,"rowspan": 1,"colspan": 2},
            {"row": 22,"col": 11,"rowspan": 1,"colspan": 4},

            {"row": 23,"col": 1,"rowspan": 1,"colspan": 8},
            {"row": 23,"col": 9,"rowspan": 1,"colspan": 2},
            {"row": 23,"col": 11,"rowspan": 1,"colspan": 4},

            {"row": 24,"col": 0,"rowspan": 1,"colspan": 15},
            {"row": 25,"col": 0,"rowspan": 1,"colspan": 15},

            {"row": 26,"col": 1,"rowspan": 1,"colspan": 4},
            {"row": 26,"col": 5,"rowspan": 1,"colspan": 4},
            {"row": 26,"col": 9,"rowspan": 1,"colspan": 4},
            {"row": 26,"col": 13,"rowspan": 1,"colspan": 2},

            {"row": 27,"col": 1,"rowspan": 1,"colspan": 2},{"row": 27,"col": 3,"rowspan": 1,"colspan": 2},{"row": 27,"col": 5,"rowspan": 1,"colspan": 2},{"row": 27,"col": 7,"rowspan": 1,"colspan": 2},{"row": 27,"col": 9,"rowspan": 1,"colspan": 2},{"row": 27,"col": 11,"rowspan": 1,"colspan": 2},{"row": 27,"col": 13,"rowspan": 1,"colspan": 2},
            {"row": 28,"col": 1,"rowspan": 1,"colspan": 2},{"row": 28,"col": 3,"rowspan": 1,"colspan": 2},{"row": 28,"col": 5,"rowspan": 1,"colspan": 2},{"row": 28,"col": 7,"rowspan": 1,"colspan": 2},{"row": 28,"col": 9,"rowspan": 1,"colspan": 2},{"row": 28,"col": 11,"rowspan": 1,"colspan": 2},{"row": 28,"col": 13,"rowspan": 1,"colspan": 2},
            {"row": 29,"col": 1,"rowspan": 1,"colspan": 2},{"row": 29,"col": 3,"rowspan": 1,"colspan": 2},{"row": 29,"col": 5,"rowspan": 1,"colspan": 2},{"row": 29,"col": 7,"rowspan": 1,"colspan": 2},{"row": 29,"col": 9,"rowspan": 1,"colspan": 2},{"row": 29,"col": 11,"rowspan": 1,"colspan": 2},{"row": 29,"col": 13,"rowspan": 1,"colspan": 2},
            {"row": 30,"col": 1,"rowspan": 1,"colspan": 2},{"row": 30,"col": 3,"rowspan": 1,"colspan": 2},{"row": 30,"col": 5,"rowspan": 1,"colspan": 2},{"row": 30,"col": 7,"rowspan": 1,"colspan": 2},{"row": 30,"col": 9,"rowspan": 1,"colspan": 2},{"row": 30,"col": 11,"rowspan": 1,"colspan": 2},{"row": 30,"col": 13,"rowspan": 1,"colspan": 2},
            {"row": 31,"col": 1,"rowspan": 1,"colspan": 2},{"row": 31,"col": 3,"rowspan": 1,"colspan": 2},{"row": 31,"col": 5,"rowspan": 1,"colspan": 2},{"row": 31,"col": 7,"rowspan": 1,"colspan": 2},{"row": 31,"col": 9,"rowspan": 1,"colspan": 2},{"row": 31,"col": 11,"rowspan": 1,"colspan": 2},{"row": 31,"col": 13,"rowspan": 1,"colspan": 2},
            {"row": 32,"col": 1,"rowspan": 1,"colspan": 2},{"row": 32,"col": 3,"rowspan": 1,"colspan": 2},{"row": 32,"col": 5,"rowspan": 1,"colspan": 2},{"row": 32,"col": 7,"rowspan": 1,"colspan": 2},{"row": 32,"col": 9,"rowspan": 1,"colspan": 2},{"row": 32,"col": 11,"rowspan": 1,"colspan": 2},{"row": 32,"col": 13,"rowspan": 1,"colspan": 2},
            {"row": 33,"col": 1,"rowspan": 1,"colspan": 2},{"row": 33,"col": 3,"rowspan": 1,"colspan": 2},{"row": 33,"col": 5,"rowspan": 1,"colspan": 2},{"row": 33,"col": 7,"rowspan": 1,"colspan": 2},{"row": 33,"col": 9,"rowspan": 1,"colspan": 2},{"row": 33,"col": 11,"rowspan": 1,"colspan": 2},{"row": 33,"col": 13,"rowspan": 1,"colspan": 2},
            {"row": 34,"col": 1,"rowspan": 1,"colspan": 2},{"row": 34,"col": 3,"rowspan": 1,"colspan": 2},{"row": 34,"col": 5,"rowspan": 1,"colspan": 2},{"row": 34,"col": 7,"rowspan": 1,"colspan": 2},{"row": 34,"col": 9,"rowspan": 1,"colspan": 2},{"row": 34,"col": 11,"rowspan": 1,"colspan": 2},{"row": 34,"col": 13,"rowspan": 1,"colspan": 2},

            {"row": 35,"col": 0,"rowspan": 1,"colspan": 15},

            {"row": 36,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 36,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 36,"col": 7,"rowspan": 1,"colspan": 4},
            {"row": 36,"col": 11,"rowspan": 1,"colspan": 4},

            {"row": 37,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 37,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 37,"col": 7,"rowspan": 1,"colspan": 4},
            {"row": 37,"col": 11,"rowspan": 1,"colspan": 4},

            {"row": 38,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 38,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 38,"col": 7,"rowspan": 1,"colspan": 4},
            {"row": 38,"col": 11,"rowspan": 1,"colspan": 4},

            {"row": 39,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 39,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 39,"col": 7,"rowspan": 1,"colspan": 4},
            {"row": 39,"col": 11,"rowspan": 1,"colspan": 4},

            {"row": 40,"col": 0,"rowspan": 1,"colspan": 15},
            {"row": 41,"col": 0,"rowspan": 1,"colspan": 9},

            {"row": 42,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 42,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 42,"col": 7,"rowspan": 1,"colspan": 2},

            {"row": 43,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 43,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 43,"col": 7,"rowspan": 1,"colspan": 2},

            {"row": 44,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 44,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 44,"col": 7,"rowspan": 1,"colspan": 2},

            {"row": 45,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 45,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 45,"col": 7,"rowspan": 1,"colspan": 2},

            {"row": 46,"col": 0,"rowspan": 1,"colspan": 15},
            {"row": 47,"col": 0,"rowspan": 1,"colspan": 15},

            {"row": 48,"col": 1,"rowspan": 1,"colspan": 14},

            {"row": 49,"col": 1,"rowspan": 1,"colspan": 4},
            {"row": 49,"col": 5,"rowspan": 1,"colspan": 3},
            {"row": 49,"col": 8,"rowspan": 1,"colspan": 4},
            {"row": 49,"col": 12,"rowspan": 1,"colspan": 3},

            {"row": 50,"col": 1,"rowspan": 1,"colspan": 4},
            {"row": 50,"col": 5,"rowspan": 1,"colspan": 3},
            {"row": 50,"col": 8,"rowspan": 1,"colspan": 4},
            {"row": 50,"col": 12,"rowspan": 1,"colspan": 3},

            {"row": 51,"col": 0,"rowspan": 1,"colspan": 8},
            {"row": 51,"col": 8,"rowspan": 1,"colspan": 7},

            {"row": 52,"col": 0,"rowspan": 1,"colspan": 15},

            {"row": 53,"col": 0,"rowspan": 1,"colspan": 9},
            {"row": 53,"col": 9,"rowspan": 1,"colspan": 6},

            {"row": 54,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 54,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 54,"col": 7,"rowspan": 1,"colspan": 2},
            {"row": 54,"col": 9,"rowspan": 4,"colspan": 6},

            {"row": 55,"col": 0,"rowspan": 2,"colspan": 3},
            {"row": 55,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 55,"col": 7,"rowspan": 1,"colspan": 2},

            {"row": 56,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 56,"col": 7,"rowspan": 1,"colspan": 2},

            {"row": 57,"col": 0,"rowspan": 1,"colspan": 3},
            {"row": 57,"col": 3,"rowspan": 1,"colspan": 4},
            {"row": 57,"col": 7,"rowspan": 1,"colspan": 2},

            {"row": 58,"col": 0,"rowspan": 1,"colspan": 15},

        ],
        "colWidths": [150,60,60,60,60,60,60,60,60,60,60,60,60,60,60]
    }
};

myhot.setTemplateSetting(@json(json_decode($template_setting,true)));

var template_header = @json(json_decode($template_header,true));

var hasil_uji = @json(json_decode($hasil_uji,true));
// if(hasil_uji)
// {
//     alert("11");
// }
// else
// {
//     alert("aa");
// }

// if(tmpReportId)
// {
//     alert("11");
// }
// else
// {
//     alert("aa");
// }

// if(hasil_uji && tmpReportId)
// {
    // SEMENTARA
    template_header[{{ $tmp_report_id }}] = hasil_uji;
// }

// CARA SIMPAN AMBIL NILAI
// console.log(template_header3[8][3][9]);
var template_header31 =
{"12":
    [
        [
        "CALIBRATION FORM","","",
        "LABORATORIUM KALIBRASI MASSA","","","","","","","",
        "Order No.","","",""
        ],
        [
        "BIDUR","","",
        "DIREKTORAT METROLOGI","","","","","","","",
        "V003-16-342","","",""
        ],
        [
        "Code : C-SNSU.201.04","","",
        "Jl. Pasteur No. 27 Bandung Telp. 022-4202773 Fax. 022-4266557","","","","","","",""
        ],
        [""],
        [
            "UNITT UNDER TEST (UUT) SPECIFICATION", "", "", "", "", "",
            "TIME, LOCATION & CONDITION", "", "", "", "", "", "","",""
        ],
        [
            "Merck / Origin","METTLER TOLEDO","","/","SWITZERLAND","",
            "Date","","","22/10/2019","","","","",""
        ],
        [
            "Type","-","","","","",
            "Location","","","Mass lab. Dir. Of Metrology","","","","",""
        ],
        [
            "Serial No","15935","","","","",
            "T, HR, P Start","","","19,1","oC","55,4","%","932,6","hPa"
        ],
        [
            "Nominal Range","1","mg","~","50","g",
            "T, HR, P End","","","19,1","oC","55,4","%","932,6","hPa"
        ],
        [
            "Class","M1","","","","",
            "T,HR,P rata2 act","","","19,1","oC","55,4","%","932,6","hPa"
        ],
        [""],
        [
            "Pemilik","BALAI PENGAMANAN FASILITAS KESEHATAN SURABAYA","","","","",
            "Alamat","","","Jl. Karangmeojangan No. 22","","","","",""
        ],
        [""],
        [
            "REFERENCE WEIGHT (REF), MASS COMPARATOR & DENSITY"
        ],
        [
            "Conventional Mass Ref (Mcr)","19999,59600","","","","g",
            "Air Density, ρa","","","1,06566724","","+","0,0334","","kg/m3"
        ],
        [
            "Uncertainty (Usert)","0,00780","","","","g",
            "Ref Density, ρr","","","8000","","+","30","","kg/m3"
        ],
        [
            "MC Resolution (d)","0,0050","","","","g",
            "UUT Density, ρt","","","7950","","+","140","","kg/m3"
        ],
        [""],
        ["CALIBRATION DATA"],
        [
            "Series",
            "Ir1","",
            "It1","",
            "It2","",
            "Ir2","",
            "∆I","",
            "Mct"
        ],
        [
            "",
            "g","",
            "g","",
            "g","",
            "g","",
            "g","",
            "g"
        ],
        [
            "1",
            "0,0000","",
            "0,0019","",
            "0,0019","",
            "0,0000","",
            "0,0019","",
            "19999,5979"
        ],
        [
            "",
            "","",
            "","",
            "","",
            "","",
            "Average","",
            "19999,5979"
        ],
        [
            "",
            "","",
            "","",
            "","",
            "","",
            "Correction","",
            "-0,402100"
        ],
        [""],
        ["UNCERTAINTY"],
        [
            "Component",
            "U standard","","","",
            "Sensitivity Coef.","","","",
            "Degree of freedom","","","",
            "CiUi",""
        ],
        [
            "1. Repeatability",
            "","","0,000000","",
            "cw  = 1","","1","",
            "υw = n - 1","","1","",
            "0,0000",""
        ],
        [
            "1. Repeatability",
            "","","0,000000","",
            "cw  = 1","","1","",
            "υw = n - 1","","1","",
            "0,0000",""
        ],
        [
            "1. Repeatability",
            "","","0,000000","",
            "cw  = 1","","1","",
            "υw = n - 1","","1","",
            "0,0000",""
        ],
        [
            "1. Repeatability",
            "","","0,000000","",
            "cw  = 1","","1","",
            "υw = n - 1","","1","",
            "0,0000",""
        ],
        [
            "1. Repeatability",
            "","","0,000000","",
            "cw  = 1","","1","",
            "υw = n - 1","","1","",
            "0,0000",""
        ],
        [
            "1. Repeatability",
            "","","0,000000","",
            "cw  = 1","","1","",
            "υw = n - 1","","1","",
            "0,0000",""
        ],
        [
            "1. Repeatability",
            "","","0,000000","",
            "cw  = 1","","1","",
            "υw = n - 1","","1","",
            "0,0000",""
        ],
        [
            "1. Repeatability",
            "","","0,000000","",
            "cw  = 1","","1","",
            "υw = n - 1","","1","",
            "0,0000",""
        ],
        [""],
        [
            "Combined Standard Uncertainty","","",
            "Effective Degree of Freedom","","","",
            "Coverage factor","","","",
            "Expanded Uncertainty, U","","",""
        ],
        [
            "uc","","",
            "ueff","","","",
            "k","","","",
            "g","","",""
        ],
        [
            "","","",
            "","","","",
            "","","","",
            "","","",""
        ],
        [
            "0,0524815","","",
            "51,173","","","",
            "2,008","","","",
            "0,10536","","",""
        ],
        [""],
        ["RESUME"],
        [
            "Conventional Mass of UUT","","",
            "19999,59790 g","","","",
            "STATUS",""
        ],
        [
            "Conventional Mass of UUT","","",
            "19999,59790 g","","","",
            "STATUS",""
        ],
        [
            "Conventional Mass of UUT","","",
            "19999,59790 g","","","",
            "STATUS",""
        ],
        [
            "Conventional Mass of UUT","","",
            "19999,59790 g","","","",
            "STATUS",""
        ],
        [""],
        ["METHOD, REFERENCE & TRACEABILITY"],
        [
            "Method",
            "- OIML R-111 (2004) - IK-SNSU.201 - Keputusan Dirjen PKTN No. 123 Tahun 2020"
        ],
        [
            "Reference",
            "Reference weight class","","","",
            ": M1","","",
            "Serial No.","","","",
            ": 1","",""            
        ],
        [
            "Masscomp",
            "Tipe","","","",
            ":   KA-50/A","","",
            "Readability","","","",
            ":   5,0000 mg","",""            
        ],
        [
            "The result of reported measurement traceable to SI through",
            "","","","",
            "","","",
            "Direktorat Metrologi","","","",
            "","",""            
        ],
        [""],
        [
            "PERSON ON DUTY (POD)","","","","","","","","",
            "NOTE","","","","",""
        ],
        [
            "","","",
            "NAME","","","",
            "SIGN","",
            ""
        ],
        [
            "TECHNICIAN","","",
            "FIRMAN","","","",
            "",""
        ],
        [
            "KURNIAWAN","","","",
            "",""
        ],
        [
            "MASS LAB HEAD","","",
            "ic","","","",
            "",""
        ],
        [""]
    ]
};

myhot.setTemplateHeaderHot(template_header);

function generateHOT(obj)
{
    var template_format = $(obj).select2('data');
    myhot.generateMyHot(template_format);
}

$(document).ready(function()
{
    myform.parsingFormElement();
    myform.setFormFormat();

    generateHOT($("#tmp_report_id"));

    // $("#tertelusur, #masscomp").select2();
    // $("#metodeuji_id, #tertelusur, #masscomp").select2().on('change', function() {
    //   var data = $(".select2 option:selected").text();
    //   alert(data);
    // //   $("#test").val(data);
    // });
    $("select#metodeuji_id").select2({tokenSeparators: [';', ' ']});

    $('#btn_simpan').click(function(e)
    {
        e.preventDefault();
        var form_data = "";
        id_template = $("#tmp_report_id").val();
        var form = $("#form_create_labin");

        var formData = form.serialize();
        var hotdata = JSON.stringify(template_header[id_template]);
        formData += "&hasil_uji="+ hotdata;
        
        $("#panel_create").toggleClass("ld-loading");
        $.post("{{ route('service.simpan') }}",formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            myform.setMessageErrors(response);
            myform.showFormError();
        });

    });

    hots = myhot.getHot();

    $("#btn_recount").click(function(e)
    {
        e.preventDefault();
        
        id_template = $("#tmp_report_id").val();
        var hotdata = JSON.stringify(template_header[id_template]);

        $("#panel_create").toggleClass("ld-loading");
        $.post("{{ route('service.recount') }}",{
            _token:"{{ csrf_token() }}",
            id:"{{ $serviceOrders->id }}",
            hotdata
        },function(res)
        {
            $("#panel_create").toggleClass("ld-loading");
            template_header[id_template] = res;
            hots[id_template].loadData(res);
        },"JSON");
    });

});

MyBeforeKeyDown = function (e)
{
    console.log(e,e.keyCode);
    id_template = $("#tmp_report_id").val();

    let changes = hots[id_template].getSelected();
    console.log(changes);
    
    
    if (e.keyCode === 46)
    {
        e.stopImmediatePropagation();
        rowIndex = changes[0][0];
        colIndex = changes[0][1];
        let ctn = null;

        if((colIndex===1 || colIndex===2) && ((rowIndex>=15 && rowIndex<=33)||(rowIndex>=36 && rowIndex<=47)))
        {
            let myData = template_header[id_template];
            myData[rowIndex][colIndex] = "";
            template_header[id_template] = myData;
            hots[id_template].loadData(myData);
        }
    }
    
    if (e.keyCode === 113)
    {
        e.stopImmediatePropagation();
        rowIndex = changes[0][0];
        colIndex = changes[0][1];
        let ctn = null;
        // Metode Uji
        if(rowIndex===7 && colIndex===1)
        {
            ctn = $("#select_metodeuji_id").html();
        }
        // Tracebility
        if(rowIndex===9 && colIndex===1)
        {
            ctn = $("#select_tertelusur").html();
        }
        //Technician Staf
        if(rowIndex===7 && colIndex===17)
        {
            ctn = $("#select_technician").html();
        }
        // Massa Jenis
        if(colIndex===16 && (rowIndex===4 || rowIndex===5))
        {
            ctn = $("#select_massajenis").html();
        }

        if(colIndex===1 && ((rowIndex>=15 && rowIndex<=33)||(rowIndex>=36 && rowIndex<=47)))
        {
            ctn = $("#select_reference").html();
        }

        if(colIndex===2 && ((rowIndex>=15 && rowIndex<=33)||(rowIndex>=36 && rowIndex<=47)))
        {
            ctn = $("#select_masscomp").html();
        }

        if(ctn!==null)
        {
            $("#myModal #modal_body").html(ctn);
            $("#metodeuji_id").select2();
            $("#myModal").modal();
            // $("#myModal").modal().on('shown.bs.modal', function(){
            //     alert("I want this to appear after the modal has opened!");
            // });
        }

        // $("#pilih").off("click").on("click",function()
        // {            
        //     let formdata = $("#form_modal").serialize();
        //     let formdatas = formdata.split("=");
        //     // console.log(rowIndex,colIndex);
        //     // console.log(formdata.split("="));

        //     let myData = template_header[id_template];
        //     myData[rowIndex][colIndex] = formdatas[1];
        //     template_header[id_template] = myData;
        //     hots[id_template].loadData(myData);

        //     $("#myModal").modal("hide");
        // });
    }
}

function getData(obj)
{
    let data = $(obj).find(":selected").text();
    let myData = template_header[id_template];
    myData[rowIndex][colIndex] = data;
    template_header[id_template] = myData;
    hots[id_template].loadData(myData);

    $("#myModal").modal("hide");
}

// MyAfterChange = function (changes, source)
// {
//     if (source === "loadData") return;

//     var rowIndex = changes[0][0];
//     var colIndexName = changes[0][1];
//     var cellOldValue = changes[0][2];
//     var cellNewValue = changes[0][3];

//     var id_template = $("#tmp_report_id").val();
//     var myData = template_header[id_template];

//     if(colIndexName===1 && (rowIndex===14 || rowIndex==15))
//     {
//         myData[16][1] = (parseFloat(myData[14][1])+parseFloat(myData[15][1]))/2;
//         template_header[id_template] = myData;
//         hots[id_template].loadData(myData);
//     }
// };

</script>
@endsection