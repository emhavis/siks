@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>Layanan<br><span class="c-white">Verifikasi Standar</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-check"></i>
            </div>
            <div class="header-title">
                <h3>Verifikasi Standar</h3>
                <small>
                    Layanan verifikasi standar Direktorat Metrologi.
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>
<div class="row">
    <div class="panel panel-filled">
        <div class="panel-body">
            {!! Form::open(array('route' => 'service.search','id' => 'form_search', 'enctype' => "multipart/form-data", 'class' => 'form-group table-area')) !!}
                <div class="form-group row">
                    <label for="serial_number" class="col-sm-2 col-form-label">Nomor Seri</label>
                    <div class="col-sm-8">
                        {!! Form::text('serial_number', '', ['class' => 'form-control','id' => 'serial_number', 'placeholder' => 'Nomor Seri', 'autocomplete' => 'off', 'required']) !!}
                        {!! Form::hidden('request_id', $id, ['id' => 'request_id']) !!}
                    </div>
                    <div class="col-sm-4">
                        <button type="submit" class="btn btn-accent">Cari</button>
                        <a class="btn btn-success" href="{{ route('service.create') }}">
                            Buat Baru
                        </a>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

    @if (\Session::has('NotFound'))
    <div class="panel panel-filled">
        <div class="panel-body">
            <h4>Data tidak ditemukan.</h4>
            <div style="margin-top:5px;">
                {!! \Session::get('NotFound') !!}
            </div>
        </div>
    </div>
    @endif

    @if ($umlStandard != null)
    {!! Form::open(array('route' => 'service.additem','id' => 'form_request_additem', 'enctype' => "multipart/form-data", 'class' => 'form-group table-area')) !!}
        <div class="panel panel-filled">
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="standar_name">Nama Standar</label> 
                            {!! Form::text('standard_name', $umlStandard->standard->standard_name, ['class' => 'form-control','id' => 'standard_name', 'placeholder' => 'Nama Standar', 'disabled']) !!}
                            {!! Form::hidden('uml_standard_id', $umlStandard->id, ['id' => 'uml_standard_id']) !!}
                            {!! Form::hidden('service_request_id', $id, ['id' => 'service_request_id']) !!}
                        </div>
                        <div class="form-group">
                            <label for="serial_number">Nomor Seri</label> 
                            {!! Form::text('serial_number', $umlStandard->serial_number, ['class' => 'form-control','id' => 'serial_number', 'placeholder' => 'Nomor Seri', 'disabled']) !!}
                        </div>
                        <div class="form-group margin-t-19">
                            <label for="standard_type">Jenis Standar</label>
                            {!! Form::text('standard_type', $umlStandard->standard->standard_type->standard_type, ['class' => 'form-control','id' => 'standard_type', 'placeholder' => 'Jenis Standar', 'disabled']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right">
            {!! Form::submit('Simpan dan Tambah Kembali', ['class' => 'btn btn-w-md btn-primary', 'name' => 'save_button', 'value' => 'saveback']) !!}
            {!! Form::submit('Simpan', ['class' => 'btn btn-w-md btn-accent', 'name' => 'save_button', 'value' => 'save']) !!}
        </div>
    {!! Form::close() !!}
    @endif
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#serial_number').focus();
    });
</script>
@endsection
