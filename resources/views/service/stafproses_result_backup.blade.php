@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<!-- <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}"> -->
<!-- <link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/> -->
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
<link href="{{ asset('assets/handsontable/handsontable.full.min.css') }}" rel="stylesheet" media="screen">

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/handsontable@latest/dist/handsontable.full.min.css">
<link rel="stylesheet" type="text/css" href="https://handsontable.com/static/css/main.css"> -->

<style type="text/css">
    table.input-table td{padding: 1px !important;}
</style>
@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>Layanan<br><span class="c-white">Verifikasi Standar</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-check"></i>
            </div>
            <div class="header-title">
                <h3>Test Result</h3>
                <small>
                    Halaman entry hasil laboratorium
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(array('id' => 'form_create_labin', 'enctype' => "multipart/form-data")) !!}
        <div class="panel panel-filled panel-c-success">
                <div class="col-md-12" id="form_content">
                    <div class="form-group">
                        <label for="metodeuji_id">Metode Uji</label>
                        {!! Form::select('metodeuji_id', $mMetodeUji, explode(",",$serviceOrders[0]->metodeuji_id), ['class' => 'form-control','id' => 'metodeuji_id', 'required', 'multiple'=>'multiple']) !!}
                    </div>
                    <div class="form-group">
                        <label for="standar_ukuran_id">Standard Ukuran</label>
                        {!! Form::select('standar_ukuran_id', $mStandardUkuran, explode(",",$serviceOrders[0]->standar_ukuran_id), ['class' => 'form-control','id' => 'standar_ukuran_id', 'required', 'multiple'=>'multiple']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="tertelusur">Tertelusur melalui</label>
                        {!! Form::text('tertelusur', $serviceOrders[0]->tertelusur, ['class' => 'form-control','id' => 'tertelusur', 'placeholder' => 'Tertelusur ke Satuan Pengukuran SI melalui', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="referensi">Referensi</label>
                        {!! Form::text('referensi', $serviceOrders[0]->referensi, ['class' => 'form-control','id' => 'referensi', 'placeholder' => 'Referensi', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="calibrator">Di Kalibrasi Oleh</label>
                        {!! Form::text('calibrator', $serviceOrders[0]->calibrator, ['class' => 'form-control','id' => 'calibrator', 'placeholder' => 'Di Kalibrasi oleh', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="suhu_ruang">Kondisi Suhu Ruangan</label>
                        {!! Form::text('suhu_ruang', $serviceOrders[0]->suhu_ruang, ['class' => 'form-control','id' => 'suhu_ruang', 'placeholder' => 'Kondisi Suhu Ruangan', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="lembab_ruang">Kondisi Kelembaban Ruangan</label>
                        {!! Form::text('lembab_ruang', $serviceOrders[0]->lembab_ruang, ['class' => 'form-control','id' => 'lembab_ruang', 'placeholder' => 'Kondisi Kelembaban Ruangan', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="cakupan">Faktor Cakupan</label>
                        {!! Form::text('cakupan', $serviceOrders[0]->cakupan, ['class' => 'form-control','id' => 'cakupan', 'placeholder' => 'Faktor Cakupan', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="remark_sertifikat">Keterangan</label>
                        {!! Form::textarea('remark_sertifikat', '', ['class' => 'form-control','id' => 'remark_sertifikat', 'placeholder' => 'Keterangan', 'size' => '10x5',]) !!}
                    </div>
                    <div class="form-group">
                        <label for="tipe_table_id"></label>
                        {!! Form::select('template_format', array(
                        "Pilih"=>"Pilih",
                        "DC CURRENT MEASUREMENT"=>"DC CURRENT MEASUREMENT",
                        "DC CURRENT SOURCE"=>"DC CURRENT SOURCE",
                        "DC VOLTAGE MEASUREMENT"=>"DC VOLTAGE MEASUREMENT",
                        "DC VOLTAGE SOURCE"=>"DC VOLTAGE SOURCE",
                        "RESISTANCE MEASUREMENT"=>"RESISTANCE MEASUREMENT",
                        "RESISTANCE SOURCE"=>"RESISTANCE SOURCE",
                        "3 WIRE RTD PT 100 (385) MEASUREMENT"=>"3 WIRE RTD PT 100 (385) MEASUREMENT",
                        "4 WIRE RTD PT 100 (385) MEASUREMENT"=>"4 WIRE RTD PT 100 (385) MEASUREMENT",
                        "RTD PT 100 (385) SOURCE"=>"RTD PT 100 (385) SOURCE",
                        "RTD PT 100 (385) OUTPUT"=>"RTD PT 100 (385) OUTPUT",
                        "AC VOLTAGE MEASURE @ 55 Hz"=>"AC VOLTAGE MEASURE @ 55 Hz",
                        "DC VOLTMETER"=>"DC VOLTMETER",
                        "AC VOLTMETER @ 55 Hz"=>"AC VOLTMETER @ 55 Hz",
                        "DC AMPEREMETER"=>"DC AMPEREMETER",
                        "OHM METER"=>"OHM METER",
                        "INSULATION TESTER @ 250 VOLT"=>"INSULATION TESTER @ 250 VOLT",
                        "INSULATION TESTER @ 500 VOLT"=>"INSULATION TESTER @ 500 VOLT",
                        "TEMPLATE MASSA 1"=>"TEMPLATE MASSA 1",
                        "TEMPLATE MASSA 2"=>"TEMPLATE MASSA 2",
                        "TEMPLATE MASSA 3"=>"TEMPLATE MASSA 3",
                        "TEMPLATE MASSA 4"=>"TEMPLATE MASSA 4",
                        "TEMPLATE MASSA 5"=>"TEMPLATE MASSA 5",
                        "TEMPLATE MASSA 6"=>"TEMPLATE MASSA 6",
                        "TEMPLATE PANJANG 1"=>"TEMPLATE PANJANG 1",
                        "TEMPLATE PANJANG 2"=>"TEMPLATE PANJANG 2",
                        "TEMPLATE PANJANG 3"=>"TEMPLATE PANJANG 3",
                        "TEMPLATE PANJANG 4"=>"TEMPLATE PANJANG 4",
                        "TEMPLATE PANJANG 5"=>"TEMPLATE PANJANG 5",
                        "TEMPLATE PANJANG 6"=>"TEMPLATE PANJANG 6",
                        "TEMPLATE PANJANG 7"=>"TEMPLATE PANJANG 7",
                        "TEMPLATE PANJANG 8"=>"TEMPLATE PANJANG 8",
                        "TEMPLATE PANJANG 9"=>"TEMPLATE PANJANG 9",
                        "TEMPLATE PANJANG 10"=>"TEMPLATE PANJANG 10",
                        "TEMPLATE PANJANG 11"=>"TEMPLATE PANJANG 11",
                        "TEMPLATE PANJANG 12"=>"TEMPLATE PANJANG 12",
                        "TEMPLATE PANJANG 13"=>"TEMPLATE PANJANG 13",
                        "TEMPLATE VOLUME 1"=>"TEMPLATE VOLUME 1",
                        "TEMPLATE VOLUME 2"=>"TEMPLATE VOLUME 2",
                        "TEMPLATE VOLUME 3"=>"TEMPLATE VOLUME 3",
                        "TEMPLATE GT 1"=>"TEMPLATE GT 1",
                        "TEMPLATE GT 2"=>"TEMPLATE GT 2",
                        "TEMPLATE GT 3"=>"TEMPLATE GT 3",
                        "TEMPLATE GT 4"=>"TEMPLATE GT 4",
                        "TEMPLATE GT 5"=>"TEMPLATE GT 5",
                        "TEMPLATE GT 6"=>"TEMPLATE GT 6",
                        "TEMPLATE GT 7"=>"TEMPLATE GT 7",
                        "TEMPLATE GT 8"=>"TEMPLATE GT 8",
                        "TEMPLATE GT 9"=>"TEMPLATE GT 9",
                        "TEMPLATE GT 10"=>"TEMPLATE GT 10",
                        "TEMPLATE GT 11"=>"TEMPLATE GT 11"
                        ), '', ['class' => 'form-control','id' => 'tipe_table_id', '']) !!}
                    </div>

                </div>
        </div>

    </div>
    <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Proses</button> 
    {!! Form::close() !!}
    </div>
</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ asset('assets/handsontable/handsontable.full.min.js') }}"></script>
<!-- <script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.js') }}"></script> -->
<!-- <script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.numeric.extensions.js') }}"></script> -->
<!-- <script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.extensions.js') }}"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/handsontable@latest/dist/handsontable.full.min.js"></script> -->
<script type="text/javascript">

var data_uji = '{{ $serviceOrders[0]->hasil_uji }}';

if(data_uji.length>0)
{
    data_uji = data_uji.replace(/(&quot\;)/g,"\"");
    var hasil_uji = JSON.parse(data_uji);    
}
else
{
    var hasil_uji = null;
}

var dc_current_measurement = {
    startRows: 10,
    startCols: 4,
    rowHeights: 30,
    className: "htRight htMiddle",
    cell: [
    {row: 0, col: 0, className: "htCenter htMiddle"}
    ],
    mergeCells: true,
    contextMenu: true,
    colWidths: [100, 200, 100, 200],
    licenseKey: 'non-commercial-and-evaluation'
};

var stopwatch = {
    data: [
    ["RANGE(s)","PENUNJUKAN ALAT","","","KOREKSI(s)","KETIDAKPASTIAN(s)"],
    ['','Jam', 'Menit', 'Sekon'],
    ['','', '', '','',''],
    ['','', '', '','',''],
    ['','', '', '','',''],
    ['','', '', '','',''],
    ['','', '', '','',''],
    ['','', '', '','','']
    ],
    startRows: 10,
    startCols: 10,
    rowHeights: 30,
    className: "htRight htMiddle",
    cell: [
    {row: 0, col: 0, className: "htCenter htMiddle"},
    {row: 0, col: 1, className: "htCenter htMiddle"},
    {row: 1, col: 1, className: "htCenter htMiddle"},
    {row: 1, col: 2, className: "htCenter htMiddle"},
    {row: 1, col: 3, className: "htCenter htMiddle"},
    {row: 0, col: 4, className: "htCenter htMiddle"},
    {row: 0, col: 5, className: "htCenter htMiddle"}
    ],
    mergeCells: true,
    contextMenu: true,
    colWidths: [100, 100, 100, 100, 150, 200],
    mergeCells: [
    {row: 0, col: 0, rowspan: 2, colspan: 1},
    {row: 0, col: 1, rowspan: 1, colspan: 3},
    {row: 0, col: 4, rowspan: 2, colspan: 1},
    {row: 0, col: 5, rowspan: 2, colspan: 1}
    ],
    licenseKey: 'non-commercial-and-evaluation'
};

$(document).ready(function ()
{
    $("#metodeuji_id,#standar_ukuran_id").select2({tokenSeparators: [',', ' ']});

    $("#tipe_table_id").select2();

    $('#btn_simpan').click(function(e)
    {
        e.preventDefault();

        var form_data = $("#form_create_labin").serialize();
        var metodeuji_id = $("#metodeuji_id").val();
        var standar_ukuran_id = $("#standar_ukuran_id").val();
        
        form_data += '&metodeuji_id='+metodeuji_id;
        form_data += '&standar_ukuran_id='+standar_ukuran_id;

        if(hot01) form_data += '&hot01='+JSON.stringify(hot01.getData());
        if(hot02) form_data += '&hot02='+JSON.stringify(hot02.getData());
        if(hot03) form_data += '&hot03='+JSON.stringify(hot03.getData());
        if(hot04) form_data += '&hot04='+JSON.stringify(hot04.getData());
        if(hot05) form_data += '&hot05='+JSON.stringify(hot05.getData());
        if(hot06) form_data += '&hot06='+JSON.stringify(hot06.getData());
        if(hot07) form_data += '&hot07='+JSON.stringify(hot07.getData());
        if(hot08) form_data += '&hot08='+JSON.stringify(hot08.getData());
        if(hot09) form_data += '&hot09='+JSON.stringify(hot09.getData());
        // if(hot10) form_data += '&hot10='+JSON.stringify(hot10.getData());
        
        // console.log(hot01.getSettings().nestedHeaders);

        $.post('{{ route('service.stafsimpan_result',$serviceOrders[0]->id) }}',form_data,function(response)
        {
            if(response[0]==false)
            {
                var msg = show_notice(response[1]);
                $("#alert_board").html(msg);
            }
            else
            {
                window.location = '{{ route('service.stafproses') }}';
            }

        },"json");
    });

    // var tipe = '{{ $template }}';
    // console.log(tipe);

    switch('{{ $template }}')
    {
        case "loop_calibrator": 
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            $("#form_content").append('<div class="hot-table" id="hot2"></div>');
            var hot01 = new Handsontable(document.getElementById("hot"),dc_current_measurement);
            hot01.updateSettings({nestedHeaders: [[{label: 'DC CURRENT MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            if(hasil_uji && hasil_uji[0] && hasil_uji[0].length>0) hot01.loadData(hasil_uji[0]);

            var hot02 = new Handsontable(document.getElementById("hot2"),dc_current_measurement);
            hot02.updateSettings({nestedHeaders: [[{label: 'DC CURRENT SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            if(hasil_uji && hasil_uji[1] && hasil_uji[1].length>0) hot02.loadData(hasil_uji[1]);
        break;
        case "true_rms_multimeter":
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            $("#form_content").append('<div class="hot-table" id="hot2"></div>');
            $("#form_content").append('<div class="hot-table" id="hot3"></div>');
            
            var hot01 = new Handsontable(document.getElementById("hot"),dc_current_measurement);
            hot01.updateSettings({nestedHeaders: [[{label: 'DC CURRENT MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            if(hasil_uji && hasil_uji[0] && hasil_uji[0].length>0) hot01.loadData(hasil_uji[0]);
            
            var hot02 = new Handsontable(document.getElementById("hot2"),dc_current_measurement);
            hot02.updateSettings({nestedHeaders: [[{label: 'DC CURRENT SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            if(hasil_uji && hasil_uji[1] && hasil_uji[1].length>0) hot02.loadData(hasil_uji[1]);

            var hot03 = new Handsontable(document.getElementById("hot3"),dc_current_measurement);
            hot03.updateSettings({nestedHeaders: [[{label: 'RTD PT 100(385) SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            if(hasil_uji && hasil_uji[2] && hasil_uji[2].length>0) hot03.loadData(hasil_uji[2]);
        break;

        case "decade_resistance_box": 
        case "digital_earth_tester": 
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            var hot01 = new Handsontable(document.getElementById("hot"),dc_current_measurement);
            hot01.updateSettings({nestedHeaders: [['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            if(hasil_uji && hasil_uji[0] && hasil_uji[0].length>0) hot01.loadData(hasil_uji[0]);
        break;
        case "compact_calibrator": 
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            var hot01 = new Handsontable(document.getElementById("hot"),dc_current_measurement);
            hot01.updateSettings({nestedHeaders: [['Range', 'RTD PT 100 (385) SOURCE', 'Koreksi', 'Ketidakpastian']]});
        break;
        case "insulation_tester": 
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            $("#form_content").append('<div class="hot-table" id="hot2"></div>');
            var hot01 = new Handsontable(document.getElementById("hot"),dc_current_measurement);
            hot01.updateSettings({nestedHeaders: [[{label: 'INSULATION TESTER @ 250 VOLT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot02 = new Handsontable(document.getElementById("hot2"),dc_current_measurement);
            hot02.updateSettings({nestedHeaders: [[{label: 'INSULATION TESTER @ 500 VOLT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
        break;
        case "stopwatch": 
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            var hot01 = new Handsontable(document.getElementById("hot"),stopwatch);
        break;
        case "process_meter": 
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            for(var i=2;i<=4;i++) $("#form_content").append('<div class="hot-table" id="hot'+i+'"></div>');

            var hot01 = new Handsontable(document.getElementById("hot"),dc_current_measurement);
            hot01.updateSettings({nestedHeaders: [[{label: 'DC VOLTMETER', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot02 = new Handsontable(document.getElementById("hot2"),dc_current_measurement);
            hot02.updateSettings({nestedHeaders: [[{label: 'AC VOLTMETER @55Hz', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot03 = new Handsontable(document.getElementById("hot3"),dc_current_measurement);
            hot03.updateSettings({nestedHeaders: [[{label: 'DC AMPEREMETER', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot04 = new Handsontable(document.getElementById("hot4"),dc_current_measurement);
            hot04.updateSettings({nestedHeaders: [[{label: 'OHM METER', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
        break;
        case "multifunction_calibrator":
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            for(var i=2;i<=6;i++) $("#form_content").append('<div class="hot-table" id="hot'+i+'"></div>');

            var hot01 = new Handsontable(document.getElementById("hot"),dc_current_measurement);
            hot01.updateSettings({nestedHeaders: [[{label: 'DC VOLTAGE MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot02 = new Handsontable(document.getElementById("hot2"),dc_current_measurement);
            hot02.updateSettings({nestedHeaders: [[{label: 'DC VOLTAGE SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot03 = new Handsontable(document.getElementById("hot3"),dc_current_measurement);
            hot03.updateSettings({nestedHeaders: [[{label: 'DC CURRENT MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot04 = new Handsontable(document.getElementById("hot4"),dc_current_measurement);
            hot04.updateSettings({nestedHeaders: [[{label: 'DC CURRENT SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot05 = new Handsontable(document.getElementById("hot5"),dc_current_measurement);
            hot05.updateSettings({nestedHeaders: [[{label: 'RESISTANCE SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot06 = new Handsontable(document.getElementById("hot6"),dc_current_measurement);
            hot06.updateSettings({nestedHeaders: [[{label: 'RTD PT 100 (385) SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
        break;
        case "handy_calibrator":
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            for(var i=2;i<=8;i++) $("#form_content").append('<div class="hot-table" id="hot'+i+'"></div>');

            var hot01 = new Handsontable(document.getElementById("hot"),dc_current_measurement);
            hot01.updateSettings({nestedHeaders: [[{label: 'DC VOLTAGE MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot02 = new Handsontable(document.getElementById("hot2"),dc_current_measurement);
            hot02.updateSettings({nestedHeaders: [[{label: 'DC VOLTAGE SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot03 = new Handsontable(document.getElementById("hot3"),dc_current_measurement);
            hot03.updateSettings({nestedHeaders: [[{label: 'DC CURRENT MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot04 = new Handsontable(document.getElementById("hot4"),dc_current_measurement);
            hot04.updateSettings({nestedHeaders: [[{label: 'DC CURRENT SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot05 = new Handsontable(document.getElementById("hot5"),dc_current_measurement);
            hot05.updateSettings({nestedHeaders: [[{label: 'RESISTANCE MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot06 = new Handsontable(document.getElementById("hot6"),dc_current_measurement);
            hot06.updateSettings({nestedHeaders: [[{label: 'RESISTANCE SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot07 = new Handsontable(document.getElementById("hot7"),dc_current_measurement);
            hot07.updateSettings({nestedHeaders: [[{label: '3 WIRE RTD PT 100 (385) SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot08 = new Handsontable(document.getElementById("hot8"),dc_current_measurement);
            hot08.updateSettings({nestedHeaders: [[{label: 'RTD PT 100 (385) SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
        break;
        case "documenting_process_calibrator":
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            for(var i=2;i<=9;i++) $("#form_content").append('<div class="hot-table" id="hot'+i+'"></div>');

            var hot01 = new Handsontable(document.getElementById("hot"),dc_current_measurement);
            hot01.updateSettings({nestedHeaders: [[{label: 'DC VOLTAGE MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot02 = new Handsontable(document.getElementById("hot2"),dc_current_measurement);
            hot02.updateSettings({nestedHeaders: [[{label: 'DC VOLTAGE SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot03 = new Handsontable(document.getElementById("hot3"),dc_current_measurement);
            hot03.updateSettings({nestedHeaders: [[{label: 'DC CURRENT MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot04 = new Handsontable(document.getElementById("hot4"),dc_current_measurement);
            hot04.updateSettings({nestedHeaders: [[{label: 'DC CURRENT SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot05 = new Handsontable(document.getElementById("hot5"),dc_current_measurement);
            hot05.updateSettings({nestedHeaders: [[{label: 'RESISTANCE MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot06 = new Handsontable(document.getElementById("hot6"),dc_current_measurement);
            hot06.updateSettings({nestedHeaders: [[{label: 'RESISTANCE SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot07 = new Handsontable(document.getElementById("hot7"),dc_current_measurement);
            hot07.updateSettings({nestedHeaders: [[{label: '* 4 WIRE RTD PT 100(385) MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot08 = new Handsontable(document.getElementById("hot8"),dc_current_measurement);
            hot08.updateSettings({nestedHeaders: [[{label: '* RTD PT 100(385) SOURCE', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot09 = new Handsontable(document.getElementById("hot9"),dc_current_measurement);
            hot09.updateSettings({nestedHeaders: [[{label: 'AC VOLTAGE MEASURE @ 55 Hz', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
        break;
        case "rtd_calibrator":
            $("#form_content").append('<div class="hot-table" id="hot"></div>');
            $("#form_content").append('<div class="hot-table" id="hot2"></div>');
            var hot01 = new Handsontable(document.getElementById("hot"),dc_current_measurement);
            hot01.updateSettings({nestedHeaders: [[{label: '4 WIRE RTD PT 100 (385) MEASUREMENT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
            var hot02 = new Handsontable(document.getElementById("hot2"),dc_current_measurement);
            hot02.updateSettings({nestedHeaders: [[{label: 'RTD PT 100 (385) OUTPUT', colspan: 4}],['Range', 'Penunjukan Alat', 'Koreksi', 'Ketidakpastian']]});
        break;
    }

});

function show_notice(msg)
{
    var errmessage = {
        'metodeuji_id':'Metode Uji wajib diisi',
        'standar_ukuran_id':'Standard Ukuran wajib diisi',
        'referensi':'Referensi wajib diisi',
        'calibrator':'Di Kalibrasi Oleh wajib diisi',
        'suhu_ruang':'Suhu Ruangan wajib diisi',
        'lembab_ruang':'Kelembaban Ruangan wajib diisi'
    };

    $(document).find("small.wajib-isi").remove();
            var str = '<div class="panel panel-filled panel-c-danger">'+
        '<div class="panel-body">'+
            '<b>Terjadi kesalahan.</b>'+
            'Telah terjadi kesalahan, hubungi administrator untuk info lebih lanjut.'+
            '<ul>';
            $.each(msg,function(i,v)
            {
                if(errmessage[i])
                {
                    str += '<li>'+errmessage[i]+'</li>';
                    $(document).find("label[for='"+i+"']").append("<small class='text-warning wajib-isi'> wajib diisi</small>");
                }else{
                    str += '<li>'+v+'</li>';
                }
            });
    str += '</ul>'+
        '</div>'+
    '</div>';

    return str;
}

</script>
@endsection