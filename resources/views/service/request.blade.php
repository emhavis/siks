@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>Layanan<br><span class="c-white">Verifikasi Standar</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-check"></i>
            </div>
            <div class="header-title">
                <h3>Verifikasi Standar</h3>
                <small>
                    Halaman pendaftaran layanan verifikasi standar.
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
    @if ($errors->any())
    <div class="panel panel-filled panel-c-danger">
        <div class="panel-body">
            <b>Terjadi kesalahan.</b>
            Telah terjadi kesalahan, hubungi administrator untuk info lebih lanjut.
        </div>
    </div>
    @endif
    {!! Form::open(array('route' => 'service.store','id' => 'form_create_request', 'enctype' => "multipart/form-data")) !!}
        <div class="panel panel-filled panel-c-success">
            <div class="panel-heading" >
                <h4>Informasi Unit Metrologi Legal</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="uml_name">Nama Unit Metrologi Legal</label> 
                        {!! Form::text('uml_name', $umlStandard->uml->uml_name, ['class' => 'form-control','id' => 'uml_name', 'placeholder' => 'Nama Unit Metrologi Legal', 'disabled']) !!}
                        {!! Form::hidden('uml_standards_id', $umlStandard->id, ['id' => 'uml_standards_id']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="phone_no">No. Telepon / Fax</label>
                        {!! Form::text('phone_no', $umlStandard->uml->phone_no, ['class' => 'form-control','id' => 'phone_no', 'placeholder' => 'No. Telepon / Fax', 'disabled']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="address">Alamat</label>
                        {!! Form::textarea('address', $umlStandard->uml->address, ['class' => 'form-control','id' => 'address', 'placeholder' => 'Alamat', 'size' => '10x5', 'disabled']) !!}
                    </div>
                </div>
            </div>
            <div class="text-right margin-b-15 margin-r-27">
                <a title="ubah" href="{{ route('uml.edit', $umlStandard->uml->id) }}" class="btn btn-success">Ubah</a>
            </div>
        </div>
        <div class="panel panel-filled panel-c-primary">
            <div class="panel-heading" >
                <h4>Informasi Standar</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="standar_name">Nama Standar</label> 
                        {!! Form::text('standard_name', $umlStandard->standard->standard_name, ['class' => 'form-control','id' => 'standard_name', 'placeholder' => 'Nama Standar', 'disabled']) !!}
                    </div>
                    <div class="form-group">
                        <label for="serial_number">Nomor Seri</label> 
                        {!! Form::text('serial_number', $umlStandard->serial_number, ['class' => 'form-control','id' => 'serial_number', 'placeholder' => 'Nomor Seri', 'disabled']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="standard_type">Jenis Standar</label>
                        {!! Form::text('standard_type', $umlStandard->standard->standard_type->standard_type, ['class' => 'form-control','id' => 'standard_type', 'placeholder' => 'Jenis Standar', 'disabled']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-filled panel-c-info">
            <div class="panel-heading" >
                <h4>Informasi Penyerahan Barang</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="entry_pic_name">Nama Penyerah Barang</label>
                        {!! Form::text('entry_pic_name', '', ['class' => 'form-control','id' => 'entry_pic_name', 'placeholder' => 'Nama Penyerah Barang', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="receipt_date">Jenis Tanda Pengenal</label>
                        {!! Form::select('pic_id_type', $idTypes, null, ['class' => 'form-control', 'id' => 'pic_id_type', 'placeholder' => '- Pilih Tanda Pengenal -', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="pic_phone_no">Nomor ID Tanda Pengenal</label>
                        {!! Form::text('pic_id', '', ['class' => 'form-control','id' => 'pic_id', 'placeholder' => 'Nomor ID Tanda Pengenal', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="pic_phone_no">Nomor Telepon Penyerah Barang</label>
                        {!! Form::text('pic_phone_no', '', ['class' => 'form-control','id' => 'pic_phone_no', 'placeholder' => 'Nomor Telepon Penyerah Barang', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="receipt_date">Tanggal Masuk Alat</label>
                        {!! Form::text('receipt_date', '', ['class' => 'date form-control','id' => 'receipt_date', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-w-md btn-accent">Simpan</button> 
        </div>
    {!! Form::close() !!}
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script type="text/javascript">
    $('.date').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });

    $('#pic_id_type').select2({
        placeholder: "- Pilih Tanda Pengenal -",
        allowClear: true
    });
</script>
@endsection