@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>Layanan<br><span class="c-white">Verifikasi Standar</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-check"></i>
            </div>
            <div class="header-title">
                <h3>Verifikasi Standar</h3>
                <small>
                    Halaman pendaftaran layanan verifikasi standar.
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>
<div class="row">
    @if ($errors->any())
    <div class="panel panel-filled panel-c-danger">
        <div class="panel-body">
            <b>Terjadi kesalahan.</b>
            Telah terjadi kesalahan, hubungi administrator untuk info lebih lanjut.
        </div>
    </div>
    @endif
    {!! Form::model($uml, ['method' => 'PATCH', 'route' => ['service.update', $serviceRequest->id],'id' => 'form_update_service', 'enctype' => "multipart/form-data"]) !!}
        <div class="panel panel-filled panel-c-success">
            <div class="panel-heading" >
                <h4>Informasi Unit Metrologi Legal</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="uml_name">Nama Unit Metrologi Legal</label>
                        {!! Form::select('uml_id', $umls, $serviceRequest->uml_id, ['class' => 'form-control','id' => 'uml_id', 'placeholder' => 'Nama Unit Metrologi Legal', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="phone_no">No. Telepon / Fax</label>
                        {!! Form::text('phone_no', $serviceRequest->uml->phone_no, ['class' => 'form-control','id' => 'phone_no', 'placeholder' => 'No. Telepon / Fax', 'disabled']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="address">Alamat</label>
                        {!! Form::textarea('address', $serviceRequest->uml->address, ['class' => 'form-control','id' => 'address', 'placeholder' => 'Alamat', 'size' => '10x5', 'disabled']) !!}
                    </div>
                </div>
            </div>
            <div class="text-right margin-b-15 margin-r-27">
                <a id="change_uml" title="ubah" href="{{ route('uml.edit', 1) }}" class="btn btn-success">Ubah Informasi UML</a>
            </div>
        </div>
        <div class="panel panel-filled panel-c-info">
            <div class="panel-heading" >
                <h4>Informasi Pendaftar</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="pic_name">Nama Pendaftar</label>
                        {!! Form::text('pic_name', $serviceRequest->pic_name, ['class' => 'form-control','id' => 'pic_name', 'placeholder' => 'Nama Pendaftar', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="id_type_id">Jenis Tanda Pengenal</label>
                        {!! Form::select('id_type_id', $idTypes, $serviceRequest->id_type_id, ['class' => 'form-control', 'id' => 'id_type_id', 'placeholder' => '- Pilih Tanda Pengenal -', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                        {!! Form::text('pic_id_no', $serviceRequest->pic_id_no, ['class' => 'form-control','id' => 'pic_id_no', 'placeholder' => 'Nomor ID Tanda Pengenal', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="pic_phone_no">Nomor Telepon</label>
                        {!! Form::text('pic_phone_no', $serviceRequest->pic_phone_no, ['class' => 'form-control','id' => 'pic_phone_no', 'placeholder' => 'Nomor Telepon', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="pic_email">Alamat Email</label>
                        {!! Form::text('pic_email', $serviceRequest->pic_email, ['class' => 'form-control','id' => 'pic_email', 'placeholder' => 'Alamat Email', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="receipt_date">Tanggal Masuk Alat</label>
                        {!! Form::text('receipt_date', $serviceRequest->receipt_date, ['class' => 'date form-control','id' => 'receipt_date', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'required']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-filled panel-c-success">
            <div class="panel-heading" >
                <h4>Daftar Standar</h4>
            </div>
            <div class="panel-body">
                <div class="add-section" style="display:none;">
                    <div class="form-group">
                        <span class="input-group">
                            {!! Form::text('standard_code', '', ['id' => 'standard_code', 'class' => 'form-control', 'placeholder' => 'Nomor ID', 'autocomplete' => 'off']) !!}
                            <span class="input-group-btn">
                                <button id="search_item" type="button" class="btn btn-accent">Cari</button>
                                <a class="btn btn-success" href="{{ route('service.create') }}">
                                    Buat Baru
                                </a>    
                            </span>
                        </span>
                    </div>
                </div>
                <button id="add_button" class="btn btn-w-md btn-success">Tambah Standar</button>
                <span id="progress_spin" style="float:right;display:none;">
                    <i class="fa fa-circle-o-notch fa-spin"></i>
                </span>
                <input id="item_id" name="item_id" type="hidden" value="" />
                <div class="table-area">
                    <table id="tStandard" class="table table-responsive-sm" style="font-size:14px !important;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kode Standar</th>
                                <th style="text-align:right;">Jumlah</th>
                                <th style="text-align:right;">Total Harga</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-w-md btn-accent">Proses</button> 
        </div>
    {!! Form::close() !!}
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script type="text/javascript">
    var index = 1;

    $('.date').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });
    
    $('#uml_id').select2({
        placeholder: "- Pilih UML -",
        allowClear: true
    });

    $('#id_type_id').select2({
        placeholder: "- Pilih Tanda Pengenal -",
        allowClear: true
    });

    $('.date').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });

    $('#add_button').click(function(){
        $('.add-section').slideToggle("slow");
        $('#standard_code').focus();
        if($('#add_button').text() == 'Batal'){
            $('#add_button').text('Tambah Standar');
            $('#add_button').removeClass('btn-danger');
            $('#add_button').addClass('btn-success');
        }
        else{
            $('#add_button').text('Batal');
            $('#add_button').removeClass('btn-success');
            $('#add_button').addClass('btn-danger');
        }
        return false;
    });

    $('#uml_id').change(function(){
        $.ajax({
            url: '{{ route('uml.getumlinfo') }}',
            type: 'post',
            data: {
                id: this.value
            },
            beforeSend: function() {
                $(".loader").show();
            },
            success: function(response){
                var json_response = JSON.parse(response);
                $('#phone_no').val(json_response.phone_no);
                $('#address').val(json_response.address);
                $('#change_uml').attr('href','../uml/' + json_response.id + '/edit');
            },
            complete:function(data){
                $(".loader").hide();
            }
        });
    });
    
    $(document).ready(function () {
        $('#search_item').click(function(){
            $.ajax({
                url: '{{ route('umlstandard.getUmlStandardInfo') }}',
                type: 'post',
                data: {
                    s_code: $('#standard_code').val(),
                    uml_id: $('#uml_id').val()
                },
                beforeSend: function() {
                    $("#progress_spin").show();
                    $('#standard_code').prop('disabled', true);
                    $('#search_item').prop('disabled', true);
                },
                success: function(response){
                    var json_response = JSON.parse(response);
                    var opt = '';
                    var i = 1;
                    json_response.standard.inspection_prices.forEach(function(dt){
                        opt = opt + '<div class="margin-t-10"><input id="inspection_type_'+json_response.standard_id+'_'+dt.id+'" name="inspection_type_'+json_response.standard_id+'_'+dt.id+'" type="checkbox" class="ins-check" tag_index="'+json_response.standard_id+'" row_index="'+i+'"><span class="inspection-type-text">'+dt.inspection_type+'</span></input><span id="price_'+json_response.standard_id+'_'+i+'" class="inspection-price-text">'+dt.price+'</span></div>'
                        i++;
                    });
                    
                    $('#tStandard tbody').append('<tr id="item-'+json_response.standard_id+'" class="standard-item"><td class="standard-index">' + index + '</td><td>'+json_response.standard_code+'</td><td><input id="q_'+json_response.standard_id+'" name="q_'+json_response.standard_id+'" type="number" class="form-control" style="text-align:right;" value="1" min="1"></input></td><td style="text-align:right;" id="total_'+json_response.standard_id+'">0</td><td><span class="pe-7s-close-circle remove-standard"></span></td></tr><tr id="ins-info-'+json_response.standard_id+'" class="inspection-info"><td colspan="5">Pilih jenis pengecekan yang akan dilakukan:' +opt+'</td></tr>');

                    if($('#item_id').val() == "") $('#item_id').val(json_response.standard_id);
                    else $('#item_id').val($('#item_id').val() + ',' + json_response.standard_id);
                    index++;
                },
                complete:function(data){
                    $('#progress_spin').hide();
                    $('#standard_code').prop('disabled', false);
                    $('#search_item').prop('disabled', false);
                    $('#standard_code').val('');
                    $('#standard_code').focus();
                }
            });
        });

        $('.ins-info').click(function(){

        });

        $(document).on('click', 'input.ins-check', function(){
            var a = $(this).attr('tag_index');
            var b = $(this).attr('row_index');
            var t_id = '#total_' + a;
            var p_id = '#price_' + a + '_' + b;
            var q_id = '#q_' + a;
            var init_val = ($(t_id).text()=="") ? 0 : parseInt($(t_id).text());
            var qty = ($(q_id).val()=="") ? 0 : parseInt($(q_id).val());
            if($(this).prop('checked')==true) $(t_id).text(init_val + (parseInt($(p_id).text()) * qty));
            else $(t_id).text(init_val - (parseInt($(p_id).text())*qty));
        });

        $(document).on('click', 'span.remove-standard', function(){
            $(this).closest('.standard-item').next().remove();
            $(this).closest('.standard-item').remove();
            var i = 1;
            $('td.standard-index').each(function(){
                $(this).text(i);
                i++;
            });
            index--;
        });

        $('#standard_code').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                $('#search_item').click();
                return false;
            }
        });
    });
</script>
@endsection