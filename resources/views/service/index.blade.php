@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content') 
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>QR Code</th>
                        <th>Diterima Oleh</th>
                        <th>Diuji Oleh</th>
                        <th>Tgl Terima</th>
                        <th>Tgl Selesai Uji</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequest->no_order }}</td>
                        <td><a onclick="showToolCodeInfo('{{ $row->ServiceRequestItem->Standard->tool_code }}')" href="#">{{ $row->ServiceRequestItem->Standard->tool_code }}</a></td> 
                        <td>{{ $row->MasterUsers->full_name }}</td>
                        <td>{{ $row->LabStaffOut?$row->LabStaffOut->full_name:"" }}</td>
                        <td>{{ $row->staff_entry_datein }}</td>
                        <td>{{ $row->staff_entry_dateout }}</td>
                        <td>
                        @if($row->stat_service_order=="0")
                        <a href="{{ route('service.result', $row->service_request_item_id) }}" class="btn btn-warning btn-sm">TEST RESULT</a>
                            @if($row->hasil_uji!==null)
                            <a target="_blank" href="{{ route('service.sertifikat', $row->id) }}" class="btn btn-warning btn-sm">PREVIEW</a>
                            <a href="{{ route('service.finish', $row->id) }}" class="btn btn-warning btn-sm">FINISH</a>
                            @endif
                        @elseif($row->is_finish=="0")
                        <a href="{{ route('service.warehouse', $row->id) }}" class="btn btn-warning btn-sm">GUDANG ALAT SELESAI</a>
                        @elseif($row->is_finish=="1")
                        COMPLETED
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <small class="stat-label">Jenis Besaran</small>
                        <h4 class="m-t-xs standard_measurement_type_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Jenis Alat</small>
                        <h4 class="m-t-xs standard_tool_type_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Rincian Alat</small>
                        <h4 class="m-t-xs standard_detail_type_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Merk / Buatan</small>
                        <h4 class="m-t-xs brand-made_in"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Model / Tipe</small>
                        <h4 class="m-t-xs model-tipe"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">No. Seri / No. Identitas</small>
                        <h4 class="m-t-xs no_seri-no_identitas"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Kapasitas / Daya Baca</small>
                        <h4 class="m-t-xs capacity-daya_baca"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Kelas</small>
                        <h4 class="m-t-xs kelas"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Sumber</small>
                        <h4 class="m-t-xs sumber_id"></h4>
                    </div>
                    <div class="col-md-4">
                        <small class="stat-label">Jumlah Per Set</small>
                        <h4 class="m-t-xs jumlah_per_set"></h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function (){
    $('#data_table').DataTable();
});

function showToolCodeInfo(toolCode)
{
    $.post("{{ route('standard.tool_code') }}",{"tool_code":toolCode},function(response){
    
    var res = response[0];
    $("#myModal h4.modal-title").text(res.tool_code);
    $("#myModal h4.standard_measurement_type_id").text(res.standard_type);
    $("#myModal h4.standard_tool_type_id").text(res.attribute_name);
    $("#myModal h4.standard_detail_type_id").text(res.standard_detail_type_name);
    $("#myModal h4.brand-made_in").text(res.brand+" / "+res.nama_negara);
    $("#myModal h4.model-tipe").text(res.model+" / "+res.tipe);
    $("#myModal h4.no_seri-no_identitas").text(res.no_seri+" / "+res.no_identitas);
    $("#myModal h4.capacity-daya_baca").text(res.capacity+" / "+res.daya_baca);
    $("#myModal h4.kelas").text(res.class);
    $("#myModal h4.sumber_id").text(res.nama_sumber);
    $("#myModal h4.jumlah_per_set").text(res.jumlah_per_set);
    $("#myModal").modal();

    },"json");
}
</script>
@endsection