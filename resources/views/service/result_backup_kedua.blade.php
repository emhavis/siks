@extends('layouts.app')
<link href="{{ asset('assets/handsontable/handsontable.full.min.css') }}" rel="stylesheet" media="screen">
<style type="text/css">

td, th, textarea {
    color:#000;
    border-color: #000;
    text-align: left;
}

</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        {!! Form::open(array('id' => 'form_create_labin', 'enctype' => "multipart/form-data")) !!}
        <div class="col-md-6">
            <div class="form-group">
                <label for="metodeuji_id">Metode Uji</label>
                {!! Form::select('metodeuji_id', $mMetodeUji, explode(",",$serviceOrders[0]->metodeuji_id), ['class' => 'form-control multiselect2','id' => 'metodeuji_id', 'required', 'multiple'=>'multiple']) !!}
            </div>
            <div class="form-group">
                <label for="standar_ukuran_id">Standard Ukuran</label>
                {!! Form::select('standar_ukuran_id', $mStandardUkuran, explode(",",$serviceOrders[0]->standar_ukuran_id), ['class' => 'form-control multiselect2','id' => 'standar_ukuran_id', 'required', 'multiple'=>'multiple']) !!}
            </div>
            <div class="form-group margin-t-19">
                <label for="tertelusur">Tertelusur melalui</label>
                {!! Form::text('tertelusur', $serviceOrders[0]->tertelusur, ['class' => 'form-control','id' => 'tertelusur', 'placeholder' => 'Tertelusur ke Satuan Pengukuran SI melalui', 'required']) !!}
            </div>
            <div class="form-group margin-t-19">
                <label for="referensi">Referensi</label>
                {!! Form::text('referensi', $serviceOrders[0]->referensi, ['class' => 'form-control','id' => 'referensi', 'placeholder' => 'Referensi', 'required']) !!}
            </div>
            <div class="form-group margin-t-19">
                <label for="calibrator">Di Kalibrasi Oleh</label>
                {!! Form::text('calibrator', $serviceOrders[0]->calibrator, ['class' => 'form-control','id' => 'calibrator', 'placeholder' => 'Di Kalibrasi oleh', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-filled panel-c-warning">
                <div class="panel-body">
                    <div class="form-group margin-t-19">
                        <label for="suhu_ruang">Kondisi Suhu Ruangan</label>
                        {!! Form::text('suhu_ruang', $serviceOrders[0]->suhu_ruang, ['class' => 'form-control','id' => 'suhu_ruang', 'placeholder' => 'Kondisi Suhu Ruangan', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="lembab_ruang">Kondisi Kelembaban Ruangan</label>
                        {!! Form::text('lembab_ruang', $serviceOrders[0]->lembab_ruang, ['class' => 'form-control','id' => 'lembab_ruang', 'placeholder' => 'Kondisi Kelembaban Ruangan', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="cakupan">Faktor Cakupan</label>
                        {!! Form::text('cakupan', $serviceOrders[0]->cakupan, ['class' => 'form-control','id' => 'cakupan', 'placeholder' => 'Faktor Cakupan', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="remark_sertifikat">Keterangan</label>
                        {!! Form::textarea('remark_sertifikat', $serviceOrders[0]->remark_sertifikat, ['class' => 'form-control','id' => 'remark_sertifikat', 'placeholder' => 'Keterangan', 'size' => '10x5',]) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="tmp_report_id">Pilih Template</label>
                {!! Form::select('tmp_report_id', $template_format_item, explode(",",$serviceOrders[0]->tmp_report_id), ['class' => 'form-control multiselect2','id' => 'tmp_report_id', 'required', 'multiple'=>'multiple','onchange'=>'generateHOT(this)']) !!}
            </div>
            <button role="button" class="btn btn-w-md btn-accent" id="btn_simpan">Proses</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
</div>
<div id="hot_container"></div>
<div id="hot_container_custom"></div>

@endsection

@section('scripts')
<script src="{{ asset('assets/handsontable/handsontable.full.min.js') }}"></script>
<script src="{{ asset('assets/vendor/generate.js') }}"></script>
<script type="text/javascript">

myhot   = new MyHot();
myform  = new MyForm({id:"form_create_labin",is_edit:"false",urlsubmit:'{{ route('service.simpan',$serviceOrders[0]->id) }}',urlsuccess:'{{ route('service') }}'});

var template_setting = '{{ $template_setting }}';
template_setting = template_setting.replace(/(&quot\;)/g,"\"");
template_setting = JSON.parse(template_setting);
// var template_setting = @json($template_setting);
myhot.setTemplateSetting(template_setting);
// myhot.setTemplateSetting({
//     "8": 
//     {
//         "startCols": 5,
//         "color":"#000",
//         "className": "htCenter htMiddle",
//         "mergeCells": [
//             {"row": 0,"col": 0,"rowspan": 1,"colspan": 6},
//             {"row": 0,"col": 6,"rowspan": 1,"colspan": 9},
//             {"row": 0,"col": 15,"rowspan": 1,"colspan": 5},

//             {"row": 1,"col": 1,"rowspan": 1,"colspan": 2},
//             {"row": 1,"col": 4,"rowspan": 1,"colspan": 2},
//             {"row": 1,"col": 6,"rowspan": 1,"colspan": 3},
//             {"row": 1,"col": 9,"rowspan": 1,"colspan": 6},
//             {"row": 1,"col": 15,"rowspan": 1,"colspan": 5},

//             {"row": 2,"col": 1,"rowspan": 1,"colspan": 5},
//             {"row": 2,"col": 6,"rowspan": 1,"colspan": 3},
//             {"row": 2,"col": 9,"rowspan": 1,"colspan": 6},
//             {"row": 2,"col": 15,"rowspan": 1,"colspan": 5},

//             {"row": 3,"col": 1,"rowspan": 1,"colspan": 5},
//             {"row": 3,"col": 6,"rowspan": 1,"colspan": 3},

//             {"row": 4,"col": 6,"rowspan": 1,"colspan": 3},

//             {"row": 5,"col": 1,"rowspan": 1,"colspan": 5},
//             {"row": 5,"col": 6,"rowspan": 1,"colspan": 3},

//             {"row": 6,"col": 0,"rowspan": 1,"colspan": 15},
//             {"row": 6,"col": 15,"rowspan": 1,"colspan": 5},

//             {"row": 7,"col": 1,"rowspan": 1,"colspan": 14},
//             {"row": 7,"col": 15,"rowspan": 1,"colspan": 2},
//             {"row": 7,"col": 18,"rowspan": 1,"colspan": 2},

//             {"row": 8,"col": 1,"rowspan": 1,"colspan": 4},
//             {"row": 8,"col": 5,"rowspan": 1,"colspan": 3},
//             {"row": 8,"col": 8,"rowspan": 1,"colspan": 4},
//             {"row": 8,"col": 12,"rowspan": 1,"colspan": 3},
//             {"row": 8,"col": 15,"rowspan": 1,"colspan": 5},

//             {"row": 9,"col": 1,"rowspan": 1,"colspan": 14},
//             {"row": 9,"col": 15,"rowspan": 1,"colspan": 5},

//             {"row": 10,"col": 0,"rowspan": 1,"colspan": 39},

//             {"row": 11,"col": 1,"rowspan": 1,"colspan": 4},
//             {"row": 11,"col": 5,"rowspan": 1,"colspan": 4},
//             {"row": 11,"col": 9,"rowspan": 1,"colspan": 4},
//             {"row": 11,"col": 16,"rowspan": 1,"colspan": 2},
//             {"row": 11,"col": 18,"rowspan": 1,"colspan": 3},
//             {"row": 11,"col": 21,"rowspan": 1,"colspan": 2},
//             {"row": 11,"col": 23,"rowspan": 1,"colspan": 16},

//             {"row": 13,"col": 0,"rowspan": 1,"colspan": 39},
//             {"row": 34,"col": 0,"rowspan": 1,"colspan": 39}
//         ],
//         "colWidths": [150,60,60,60,60,60,60,60,60,60,60,60,60,60,80,120,60,60,60,60,60]
//     }
// });

var template_header = '{{ $template_header }}';
var template_header2 = template_header.replace(/(&quot\;)/g,"\"");
var template_header3 = JSON.parse(template_header2);
// CARA SIMPAN AMBIL NILAI
console.log(template_header3[8][3][9]);
myhot.setTemplateHeaderHot(template_header3);
// console(template_header3[3]);
// myhot.setTemplateHeaderHot({
//     "8": 
//     [
//             [
//                 "UNIT UNDER TEST (UUT) SPECIFICATION", "", "", "", "", "",
//                 "TIME, LOCATION & CONDITION", "", "", "", "", "", "","","",
//                 "ORDER NUMBER", "", "", "", "",
//                 "","","","","","","","","","","","","","","","","","",""
//             ],
//             [
//                 "Merck / Origin","METTLER TOLEDO","","/","SWITZERLAND","",
//                 "Date","","","22/10/2019","","","","","",
//                 "0001"
//             ],
//             [
//                 "Type","-","","","","",
//                 "Location","","","Mass lab. Dir. Of Metrology","","","","","",
//                 "DENSITY"
//             ],
//             [
//                 "Serial No","15935","","","","",
//                 "T, HR, P Start","","","19,1","oC","55,4","%","932,6","hPa",
//                 "Air Density", "1,2724", "+", "0,0334", "kg/m3"
//             ],
//             [
//                 "Nominal Range","1","mg","~","50","g",
//                 "T, HR, P End","","","19,1","oC","55,4","%","932,6","hPa",
//                 "Ref. Density", "1,2724", "+", "0,0334", "kg/m3"
//             ],
//             [
//                 "Class","M1","","","","",
//                 "T,HR,P rata2 act","","","19,1","oC","55,4","%","932,6","hPa",
//                 "UUT Density", "1,2724", "+", "0,0334", "kg/m3"
//             ],
//             [
//                 "METHOD, REFERENCE & TRACEABILITY","","","","","","","",
//                 "","","","","","","",
//                 "TECHNICIAN"
//             ],
//             [
//                 "METHOD","OIML R-111 (2004), Weights of classes E1, E2, F1, F2, M1, M2, M3  (including standard  weights for testing of high capacity weighing machines and hexagonal weights)","","","","","","",
//                 "","","","","","","",
//                 "Larisa Deviyani", "","","Oki Sri Swastini",""
//             ],
//             [
//                 "Reference","Standard Weight Class :","","","","F2","","","Serial No. :","","","","G 97683 11119052A","","",
//                 "MASS LABORATORY HEAD", "","","",""
//             ],
//             [
//                 "Traceability","Direktorat Metrologi","","","","","","",
//                 "","","","","","","",
//                 "IC", "","","",""
//             ],
//             ["","","","","","","","","","","","","","", "","","",""],
//             [
//                 "Nominal",
//                 "Series 1","","","",
//                 "Series 2","","","",
//                 "Series 3","","","",
//                 "Diff.",
//                 "Density",
//                 "U Density",
//                 "Mass Comp","",
//                 "Reference","","",
//                 "Bouyancy","",
//                 "Uncertainty", "","","","","","","","","","","","","","",
//                 ""
//             ],
//             [
//                 "Mass",
//                 "Ir1","It1","It2","Ir2",
//                 "Ir1","It1","It2","Ir2",
//                 "Ir1","It1","It2","Ir2",
//                 "",
//                 "",
//                 "",
//                 "db","std",
//                 "Mcr","Ucert","Mct",
//                 "C","Status",
//                 "Uw","Usens","Ures","Umr","Udrift","Ubu","Uc","υw","usens","υres","υmr","υdrift","υbu","υeff","k","U (95%)",
//                 ""
//             ],
//             ["","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             [
//                 "g",
//                 "mg","mg","mg","mg",
//                 "mg","mg","mg","mg",
//                 "mg","mg","mg","mg",
//                 "mg",
//                 "kg/m3",
//                 "kg/m3",
//                 "g","g",
//                 "g","g","g",
//                 "C","Status",
//                 "g","g","g","g","g","g","g","","","","","","","","","g",
//                 ""
//             ],
//             ["20000","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["20000*","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["10000","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["5000","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["2000","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["2000*","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["1000","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["500","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["200","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["200*","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["100","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["50","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["20","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["20*","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["10","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["5","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["2","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["2*","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["1","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             [
//                 "mg",
//                 "mg","mg","mg","mg",
//                 "mg","mg","mg","mg",
//                 "mg","mg","mg","mg",
//                 "mg",
//                 "kg/m3",
//                 "kg/m3",
//                 "mg","mg",
//                 "mg","mg","mg",
//                 "C","Status",
//                 "mg","mg","mg","mg","mg","mg","mg","","","","","","","","","mg",
//                 ""
//             ],
//             ["500","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["200","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["200*","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["100","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["50","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["20","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["20*","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["10","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["5","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["2","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["2*","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             ["1","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""],
//             [
//                 "0", "1", "2", "3", "4", "5",
//                 "6", "7", "8", "9", "10", "11", "12",
//                 "13", "14", "15", "16", "17",
//                 "18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39"
//             ]
//     ]
// });

var data_uji = '{{ $serviceOrders[0]->hasil_uji }}';
var data_uji2 = data_uji.replace(/(&quot\;)/g,"\"");
var data_uji3 = JSON.parse(data_uji2);

myhot.setDataHot({
    // "hot_8": [
    //     ["4 WIRE RTD PT 100 (385) MEASUREMENT", "", "", ""],
    //     ["Range", "Penunjukan Alat", "Koreksi", "Ketidakpastian"],
    //     ["-", "-", "-", "-"],
    //     ["-", "-", "-", "-"],
    //     ["-", "-", "-", "-"],
    //     ["-", "-", "-", "-"],
    //     ["-", "-", "-", "-"],
    //     ["-", "-", "-", "-"],
    //     ["-", "-", "-", "-"],
    //     ["-", "-", "-", "-"],
    //     ["-", "-", "-", "-"],
    //     ["-", "-", "-", "-"]
    // ]
});

function generateHOT(obj)
{
    var template_format = $(obj).select2('data');
    myhot.generateMyHot(template_format);
}

$(document).ready(function()
{
    myform.parsingFormElement();
    myform.setFormFormat();

    generateHOT($("#tmp_report_id"));

    $('#btn_simpan').click(function(e)
    {
        e.preventDefault();
        var form_data = "";

        var hot = myhot.getHot();
        var counter = myhot.getCounter();

        for(var i=0; i<counter.length; i++)
        {
            form_data += '&hot_'+counter[i]['id']+'='+JSON.stringify(hot[counter[i]['id']].getData());

            var mergeCells = hot[counter[i]['id']].getPlugin('mergeCells').mergedCellsCollection.mergedCells;
            form_data += '&merge_'+counter[i]['id']+'='+JSON.stringify(mergeCells);
        }
        myform.save(form_data);
    });

    var hot = myhot.getHot();

    var orange = {
        1:[0,6],
        2:[0,6],
        3:[0,6,15,16,17,18,19],
        4:[0,6,15,16,17,18,19],
        5:[0,6,9,10,11,12,13,14,15,16,17,18,19],
        9:[15],
        11:[0,1,5,9,13,14,15,16,18,21,23],
        12:[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]
    };
    var yellow = {
        1:[1,3,4,9,15],
        2:[1,9],
        3:[1,10,12,14],
        4:[1,2,3,4,5,10,12,14],
        5:[1]
    };

    var blue = {
        7:[0,1],
        8:[0,1,6,9,12],
        9:[0,1],
        14:[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25],
        35:[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]
    };

    var black = {
        0:[0,6,15],
        2:[15],
        6:[0,15],
        8:[15],
        10:[0],
        13:[0],
        34:[0]
    };

    var bold = {
        0:[0,6,15],
        1:[0,6],
        2:[0,6,15],
        3:[0,6,15],
        4:[0,6,15],
        5:[0,6,9,10,11,12,13,14,15],
        6:[0,15],
        7:[0],
        8:[0,1,9,15],
        9:[0,15]
    };

    // green = d9ffd5
    // merah = ffd5d5
    // ungu = f1c7ff
    hot[8].updateSettings({
        cells: function (row, col, prop)
        {
            $.each(orange,function(i,v){
                $.each(v, function(i2,v2){
                    var cell = hot[8].getCell(i,v2);
                    cell.style.backgroundColor = "#ffeac6";
                })
            });
            $.each(yellow,function(i,v){
                $.each(v, function(i2,v2){
                    var cell = hot[8].getCell(i,v2);
                    cell.style.backgroundColor = "#faffbe";
                })
            });
            $.each(blue,function(i,v){
                $.each(v, function(i2,v2){
                    var cell = hot[8].getCell(i,v2);
                    cell.style.backgroundColor = "#d5e5ff";
                })
            });
            $.each(black,function(i,v){
                $.each(v, function(i2,v2){
                    var cell = hot[8].getCell(i,v2);
                    cell.style.backgroundColor = "#999";
                })
            });
            $.each(bold,function(i,v){
                $.each(v, function(i2,v2){
                    var cell = hot[8].getCell(i,v2);
                    cell.style.fontWeight = "bold";
                })
            });
        }
    });

    // var cell2 = hot[8].getCell(0,6);
    // cell2.style.backgroundColor = "#ff0071";

    // hot[8].updateSettings({
    //     cells: function (row, col, prop)
    //     {
    //         var cell2 = hot[8].getCell(0,0);
    //         cell2.style.backgroundColor = "#ffcd71";
    //         cell2.style.color = "#000";
    //     }
    // });

    // var set2 = {
    //   data: [
    //         ["DC CURRENT MEASUREMENT", "", "", ""],
    //         ["Range", "Penunjukan Alat", "Koreksi", "Ketidakpastian"],
    //         ["1 - 3", "1", "0.5", "0"],
    //         ["", "2", "0.3", ""],
    //         ["", "3", "0.1", ""],
    //         ["1 - 4", "4", "", ""],
    //         ["", "4", "", ""],
    //         ["", "4", "", ""],
    //         ["", "", "", ""],
    //         ["", "", "", ""],
    //         ["", "", "", ""],
    //         ["", "", "", ""]
    //     ],
    //     "startCols": 5,
    //     "className": "htCenter htMiddle",
    //     "mergeCells": [{
    //         "row": 0,
    //         "col": 0,
    //         "rowspan": 1,
    //         "colspan": 4
    //     }],
    //     "colWidths": [100, 200, 100, 200]
    // };

    // var aa = new Handsontable(document.getElementById("hot_container_custom"),set2);
});


</script>
@endsection