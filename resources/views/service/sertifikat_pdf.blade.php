<html>
<head>
<title>Sertifikat</title>
<style type="text/css">

/* Styles go here */

.page-header, .page-header-space {
  /* height: 100px; */
  height: 0;
}

.page-footer, .page-footer-space {
  height: 40px;
}

.page-footer {
  position: fixed;
  bottom: 0;
  width: 100%;
  border-top: 1px solid black; /* for demo */
  background: white;
  font-family: Helvetica;
  font-size:10pt;
  /* margin-left: 8mm; */
}

.page-header {
  position: fixed;
  top: 0mm;
  width: 100%;
  border-bottom: 1px solid black; /* for demo */
  background: white; /* for demo */
}

.page {
  page-break-after: always;
  font-family: Helvetica;
  font-size:10pt;
  padding:8mm 8mm 0 8mm;
  /* border-bottom: 1px solid #999; */
  min-height: 26cm;
  margin-bottom: 2cm;
  border: #999 solid 1px;
}

#watermark {
    position: fixed;
    font-size: 150pt;
    font-weight: bold;
    color: #ccc;
    top: 30%;
    width: 100%;
    text-align: center;
    opacity: .6;
    transform: rotate(-10deg);
    transform-origin: 50% 50%;
    z-index: -1000;
}

@media print {
   thead {display: table-header-group;}
   tfoot {display: table-footer-group;}

   button {display: none;}

   .page{border: #fff solid 0px;}

   body {margin: 0;}
}

}

</style>
</head>
<body>
    @if(in_array(Auth::user()->user_role,[3,4,8]) || $row->stat_service_order!=="3")
    <div id="watermark">DRAFT</div>
    @endif

    <div class="page-header" style="text-align: center">
    </div>

    <div class="page-footer">
        <ul style="list-style: none;margin:0;">
            <li style="margin: 5px;"><i>Dilarang menggandakan sebagian dari isi sertifikat ini tanpa izin tertulis dari Direktorat Metrologi Bandung. Dokumen ini ditandatangani secara elektronik.</i></li>
            {{-- <li><li>FO.BSNSU.SERT.01.00</li></li> --}}
        </ul>
    </div>

    <table width="100%" cellpadding="0" cellspacing="0">

      <thead>
        <tr>
          <td>
            <!--place holder for the fixed-position header-->
            <div class="page-header-space"></div>
          </td>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>
            <!--*** CONTENT GOES HERE ***-->
            <div class="page">

                <table width="100%" cellpading="0" cellspacing="0">
                    <tr>
                        <td style="width: 3cm; border-right: #000 solid 3px; padding-right: 3px; vertical-align: bottom;" rowspan="5">
                            <img src="{{ asset('assets/images/logo/logo_kemendag_sertifikat.png') }}" width="90%" />
                        </td>
                        <td></td>
                        <td style="text-align: right;">
                            @if($row->no_sertifikat)
                            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->margin(0)->size(70)->errorCorrection('H')->generate($row->no_sertifikat)) !!} ">
                            @else
                            <img src="" alt="No. Sertifikat" style="border:#999 solid 1px; width:70px;height:70px;" />
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 10px; font-size:11pt;">DIREKTORAT JENDERAL PERLINDUNGAN KONSUMEN DAN TERTIB NIAGA</td>
                        <td rowspan="4" style="text-align: right;vertical-align:bottom;">
                            <img src="{{ asset('assets/images/logo/logo-kan.jpg') }}" style="width:150px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 10px; font-size:11pt; padding-bottom:20px">DIREKTORAT METROLOGI</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 10px; font-size:10pt;">Jl. Pasteur No. 27 Bandung 40171</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 10px; font-size:10pt;">Telp. (022) 4203597 Hunting, Fax(022) 4207035</td>
                    </tr>
                </table>
                <br />
                <br />
                <table width="100%" cellpadding="0" cellspacing="0" style="text-align: center;">
                    <tr>
                        <td><u style="font-size: 16pt;font-weight:bold;">SERTIFIKAT {{ strtoupper($row->ServiceRequest->jenis_layanan) }}</u></td>
                    </tr>
                    <tr>
                        <td><i style="font-size: 10pt;">{{ $row->ServiceRequest->jenis_layanan==="verifikasi"?"Verification":"Calibration" }} Certificate</i></td>
                    </tr>
                    <tr>
                        <td style="height:10mm; font-size: 12pt;font-weight:bold;">Nomor : {{ $row->no_sertifikat }}</td>
                    </tr>
                </table>
                <br />
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="60%" style="font-size: 14pt;font-weight:bold;">
                            Nama Alat :  {{ $row->ServiceRequestItem->Standard->StandardToolType->attribute_name }}
                            <br /><i style="font-size: 10pt;font-weight:normal;">Measuring instrument</i>
                        </td>
                        <td width="40%">
                            <table border="1" cellpadding=5 cellspacing=0 width="100%">
                                <tr>
                                    <td style="text-align: center;font-size: 14pt;font-weight:bold;">No Order :</td>
                                    <td style="text-align: center;font-size: 14pt;font-weight:bold;">
                                        <u style="text-align: center;font-size: 14pt;font-weight:bold;">{{ $row->ServiceRequest->no_order }}</u><br />
                                        {{ into_tanggal($row->ServiceRequest->receipt_date) }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table style="margin-left: 20mm;" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="25%" style="font-size: 14pt;font-weight:bold;">Merk / Buatan</td>
                        <td>: {{ $data["merk"] }}</td>
                    </tr>
                    <tr>
                        <td style="font-size:10pt; padding-bottom: 10px;"><i>Trade Mark / Manufactured By</i></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="25%" style="font-size: 14pt;font-weight:bold;">Model / Tipe</td>
                        <td>: {{ $data["type"] }}</td>
                    </tr>
                    <tr>
                        <td style="font-size:10pt; padding-bottom: 10px;"><i>Model / Type</i></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="25%" style="font-size: 14pt;font-weight:bold;">Nomor Seri / Identitas</td>
                        <td>: {{ $data["serialnumber"] }}</td>
                    </tr>
                    <tr>
                        <td style="font-size:10pt; padding-bottom: 10px;"><i>Series Number / Identities</i></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="25%" style="font-size: 14pt;font-weight:bold;">Massa Nominal</td>
                        <td>: {{ $data["nominalrange"] }}</td>
                    </tr>
                    <tr>
                        <td style="font-size:10pt; padding-bottom: 10px;"><i>Nominal Mass</i></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="25%" style="font-size: 14pt;font-weight:bold;">Kelas / Jumlah</td>
                        <td>: {{ $data["kelas"] }} / {{ $data["countUut"] }} Unit</td>
                    </tr>
                    <tr>
                        <td style="font-size:10pt; padding-bottom: 10px;"><i>Class / Quantity</i></td>
                        <td></td>
                    </tr>
                </table>
                <br />
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="25%" style="font-size: 14pt;font-weight:bold;">Pemilik</td>
                        <td style="font-size: 12pt;font-weight:bold;">: {{ $row->ServiceRequest->for_sertifikat=="1"? $row->ServiceRequest->label_sertifikat : $row->ServiceRequestItem->Standard->MasterProfilUml->nama_uml}}</td>
                    </tr>
                    <tr>
                        <td style="font-size:10pt; padding-bottom: 20px;"><i>User</i></td>
                        <td style="padding-bottom: 20px;">{{ $row->ServiceRequest->for_sertifikat=="1"? $row->ServiceRequest->addr_sertifikat : $row->ServiceRequestItem->Standard->MasterProfilUml->alamat}}</td>
                    </tr>
                    @if($row->ServiceRequest->jenis_layanan==="verifikasi")
                    <tr>
                        <td colspan="2" width="25%" style="font-size: 14pt;">Sertifikat ini berlaku sampai dengan {{ into_tanggal(date('Y-m-d', strtotime('+2 year', strtotime($row->staff_entry_dateout)))) }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:10pt; padding-bottom: 20px;"><i>This standar certified is valid until {{ date('dS F Y', strtotime('+2 year', strtotime($row->staff_entry_dateout))) }}</i></td>
                    </tr>
                    @endif
                    <tr>
                        <td colspan="2" width="25%" style="font-size: 14pt;">Sertifikat ini terdiri dari 4 (empat) halaman</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:10pt; padding-bottom: 20px;"><i>This certificate consists of 4 (four) pages</i></td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="50%"></td>
                        <td width="50%">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td></td>
                                    <td>Bandung, {{ into_tanggal($row->ditmet_entry_date) }}</td>
                                </tr>
                                <tr>
                                    <td style="text-align:right">a.n.</td>
                                    <td>Direktur Metrologi</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kepala Balai Pengelolaan</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Standar Nasional Satuan Ukuran,</td>
                                </tr>
                                <tr>
                                    <td style="text-align:right; height: 2cm; vertical-align: middle;"><img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->margin(0)->size(100)->errorCorrection('H')->generate(route('tested.sertifikat',$row->id))) !!} "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Aen Jueni, S.Si</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="page">
                <u>Lampiran Sertifikat No. {{ $row->no_sertifikat }}</u>

                <table width="100%" cellpadding="0" cellspacing="0" style="margin-top:10mm;">
                    <tr>
                        <td width="25%" style="font-size: 12pt;font-weight:bold;">METODE, STANDAR DAN TELUSURAN</td>
                    </tr>
                    <tr>
                        <td style="font-size:10pt; padding-bottom: 10px;"><i>Method, Standard and Traceability</i></td>
                    </tr>
                </table>
                <br />
                <br />
                <table style="margin-left: 20px;" width="100%" cellpadding="10" cellspacing="0">
                    <tr>
                        <td width="25%" style="vertical-align:top;font-size: 12pt;">METODE</td>
                        <td><ul style="padding:0;margin-left:15px;">
                                @foreach(explode(";",$data["refmethod"]) as $method)
                                <li>{{ $method }}</li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" style="font-size: 12pt;">STANDARD</td>
                        <td>: {{ $row->ServiceRequestItem->Standard->StandardToolType->attribute_name }} Standard {{ $data["refstandard"] }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size: 12pt;">Tertelusur ke Satuan Pengukuran SI melalui {{ $data["tertelusur"] }}</td>
                    </tr>
                </table>
                <br />
                <br />
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30%" style="font-size: 12pt;font-weight:bold;">DATA {{ strtoupper($row->ServiceRequest->jenis_layanan) }}</td>
                    </tr>
                    <tr>
                        <td style="font-size:10pt; padding-bottom: 10px;"><i>{{ $row->ServiceRequest->jenis_layanan==="verifikasi"?"Verification":"Calibration" }} Data</i></td>
                    </tr>
                </table>
                <br>
                <table style="margin-left: 20px;" width="100%" cellpadding="10" cellspacing="0">
                    <tr>
                        <td width="30%" style="vertical-align:top;font-size: 12pt;">Penguji</td>
                        <td><ol style="padding:0;margin-left:20px;">
                            <li style="margin-bottom: 5px;">{{ $data["kalibrator"] }}</li>
                            <li>{{ $data["kalibrator2"] }}</li>
                        </ol>
                    </tr>
                    <tr>
                        <td width="30%" style="font-size: 12pt;">Tanggal Pengujian</td>
                        <td>: {{ into_tanggal(date('Y-m-d', strtotime($row->staff_entry_dateout))) }}</td>
                    </tr>
                    <tr>
                        <td width="30%" style="font-size: 12pt;">Lokasi</td>
                        <td>: Laboratorium {{ ucfirst(strtolower($row->MasterLaboratory->nama_lab)) }} Direktorat Metrologi</td>
                    </tr>
                    <tr>
                        <td width="30%" style="vertical-align:top;font-size: 12pt;">Kondisi Ruangan</td>
                        <td><ul style="padding:0;margin-left:15px;">
                            <li style="margin-bottom: 5px;">Suhu : ({{ number_format($data["suhu"],1,'.',',') }} &plusmn; {{ number_format($data["u_suhu"],1,'.',',') }})&#8451</li>
                            <li>Kelembaban : ({{ number_format($data["lembab"],1,'.',',') }} &plusmn; {{ number_format($data["u_lembab"],1,'.',',') }})%</li>
                        </ul>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="page">

                <u>Lampiran Sertifikat No. {{ $row->no_sertifikat }}</u>

                <table width="100%" cellpadding="0" cellspacing="0" style="margin-top:10mm;">
                    <tr>
                        <td width="25%" style="font-size: 12pt;font-weight:bold;">HASIL</td>
                    </tr>
                    <tr>
                        <td style="font-size:10pt; padding-bottom: 10px;"><i>Result</i></td>
                    </tr>
                </table>
                <table width="50%" style="margin-left: 20%" border="1" cellpadding="5" cellspacing="0">
                    <tr>
                        <th>Kelas</th>
                        <th>Massa Nominal</th>
                        <th>Massa Konvensional</th>
                        <th>Ketidakpastian</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>(g)</th>
                        <th>(g)</th>
                        <th>(mg)</th>
                    </tr>
                    @for($incr=15;$incr<=33;$incr++)
                    @if(floatval($hot[$incr][22])>0)
                    @php
                        $a = explode(".",$hot[$incr][41]);
                        $b = str_split($a[1]);
                        $i=0;
                        $k=0;
                        foreach ($b as $bb)
                        {
                            if(intval($bb)>0)
                            {
                                if($k===2) break;
                                $k++;
                            }
                            $i++;
                        }
                    @endphp
                    <tr>
                        <td>{{ $hot[$incr][42] }}</td>
                        <td>{{ $hot[$incr][0] }}</td>
                        <td>{{ number_format($hot[$incr][22],($i+3),",",".") }}</td>
                        <td>{{ number_format(round(floatval($hot[$incr][41]),$i,PHP_ROUND_HALF_ODD),$i,",",".") }}</td>
                    </tr>
                    @endif
                    @endfor
                </table>
            </div>


            <div class="page">
                <u>Lampiran Sertifikat No. {{ $row->no_sertifikat }}</u>
                <table width="50%" style="margin-top:30px;margin-left: 20%" border="1" cellpadding="5" cellspacing="0">
                    <tr>
                        <th>Kelas</th>
                        <th>Massa Nominal</th>
                        <th>Massa Konvensional</th>
                        <th>Ketidakpastian</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>(mg)</th>
                        <th>(mg)</th>
                        <th>(mg)</th>
                    </tr>
                    @for($incr=36;$incr<=47;$incr++)
                    @if(floatval($hot[$incr][22])>0)
                    @php
                        $a = explode(".",$hot[$incr][41]);
                        $b = str_split($a[1]);
                        $i=0;
                        $k=0;
                        foreach ($b as $bb)
                        {
                            if(intval($bb)>0)
                            {
                                $k++;
                            }
                            $i++;
                            if($k===2) break;
                        }
                    @endphp
                    <tr>
                        <td>{{ $hot[$incr][42] }}</td>
                        <td>{{ $hot[$incr][0] }}</td>
                        <td>{{ number_format($hot[$incr][22],($i+3),",",".") }}</td>
                        <td>{{ number_format(round(floatval($hot[$incr][41]),$i,PHP_ROUND_HALF_ODD),$i,",",".") }}</td>
                    </tr>
                    @endif
                    @endfor
                </table>
                <br />
                <br />
                <i>Catatan :</i>
                <ol>
                    <li><i>Hasil yang dinyatakan dalam sertifikat ini, hanya terkait dengan barang yang diverifikasi</i></li>
                    <li><i>Ketidakpastian pengukuran dinyatakan pada tingkat kepercayaan sekitar 95% dengan faktor cakupan k=2.</i></li>
                    {{-- <li><i>Telah dibubuhkan (segel/stiker)* verifikasi nomor ic</i></li> --}}
                    <li><i>Kesalahan anak timbangan tidak memenuhi persyaratan di tabel MPE OIML R-111 untuk kelas E2,sehingga mengalami penurunan kelas</i></li>
                </ol>
                <br>
                <br>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="70%"></td>
                        <td width="30%">
                            <table style="text-align: center" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>Sub Koordinator Pelayanan</td>
                                </tr>
                                <tr>
                                    <td>{{ strtoupper($row->ServiceRequest->jenis_layanan) }} Standar Ukuran,</td>
                                </tr>
                                <tr>
                                    <td style="height: 2cm; vertical-align: bottom;">Ruseno ST</td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000; ">NIP 19730713 200502 1 001</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
          </td>
        </tr>
      </tbody>

      <tfoot>
        <tr>
          <td>
            <!--place holder for the fixed-position footer-->
            <div class="page-footer-space"></div>
          </td>
        </tr>
      </tfoot>
    </table>

  </body>

</html>
