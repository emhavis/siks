<html>
<head>
    <title>Sertifikat</title>
    <style type="text/css">
        table tr td,
        table tr th{
            font-size: 9pt;
            vertical-align: top;
        }
        p.small{
            font-size: 7pt;
        }
        @page
        {
            size: 21cm 29.7cm portrait;
            margin: 1cm 1cm 2cm 2cm;
            padding: 0;
        }
        body,table,p,h1,h2,h3,h4,h5
        {
            margin: 0px;
            font-family: Arial, Helvetica, sans-serif;
        }
        .h-lighter h3,h4
        {
            font-weight: lighter;
        }
    </style>
</head>
<body>
    <table width="100%">
        <tr>
            <td style="text-align: right;">
                Lampiran Sertifikat No. 0038 / PKTN.4.7 / 01 /2019
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3>METODE, STANDAR DAN TELUSURAN</h3>
                <h4><i>Method, Standard and Traceability</i></h4>
            </td>
        </tr>
    </table>
    <br>
    <table style="margin-left: 20px;" width="100%" cellpadding="0" cellspacing="0" class="h-lighter">
        <tr>
            <td style="width: 20%;">
                <h3>- Metode</h3>
            </td>
            <td><h3>: IK-SNSU-601 Kalibrasi Pressure Gauge dengan Digital Pressure Calibrator</h3></td>
        </tr>
        <tr>
            <td>
                <h3>- Standar</h3>
            </td>
            <td><h3>: Pressure Module; AMETEK JOFRA; APM02KPGSG; 11101203</h3></td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>Tertelusur ke Satuan Pengukuran SI melalui Direktorat Metrologi</h3>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3>DATA KALIBRASI</h3>
                <h4><i>Calibration Data</i></h4>
            </td>
        </tr>
    </table>
    <br>
    <table style="margin-left: 20px;" width="100%" cellpadding="0" cellspacing="0" class="h-lighter">
        <tr>
            <td style="width: 20%;">
                <h3>- Referensi</h3>
            </td>
            <td><h3>: PT. Pertamina Gas, tanggal 4 Januari 2019</h3></td>
        </tr>
        <tr>
            <td>
                <h3>- Tanggal Kalibrasi</h3>
            </td>
            <td><h3>: 14 Januari 2019</h3></td>
        </tr>
        <tr>
            <td>
                <h3>- Dikalibrasikan oleh</h3>
            </td>
            <td><h3>: M. Kholid Mawardi, S.T., M.T.</h3></td>
        </tr>
        <tr>
            <td>
                <h3>- Lokasi</h3>
            </td>
            <td><h3>: Lab. Gaya dan Tekanan Direktorat Metrologi</h3></td>
        </tr>
        <tr>
            <td>
                <h3>- Kondisi Ruangan</h3>
            </td>
            <td><h3>: Suhu : (19,5 + 0,5)oC</h3></td>
        </tr>
        <tr>
            <td></td>
            <td><h3>;&nbsp Kelembapan : (67,5 + 3)%</h3></td>
        </tr>
    </table>
    <br>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3>HASIL</h3>
                <h4><i>Result</i></h4>
            </td>
        </tr>
    </table>
    <br>

    <table class="h-lighter" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3><i>Catatan :</i></h3>
                <h3><i>Ketidakpastian pengukuran dinyatakan pada tingkat</i></h3>
                <h3><i>kepercayaan sekitar 95% dengan faktor cakupan k=2</i></h3>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table style="margin-left: 300px; text-align: center;" class="h-lighter" width="80%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h3>Kepala Pelayanan Teknis,</h3>
            </td>
        </tr>
        <tr>
            <td style="height: 2cm;vertical-align: bottom;"><h3>Aen Jueni, S.Si</h3></td>
        </tr>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <table width="100%" style="text-align: center;">
        <tr>
            <td style="border-top: #000 solid 1px;">
                Dilarang menggandakan sebagian dari isi sertifikat ini tanpa izin tertulis dari Direktorat Metrologi Bandung
            </td>
        </tr>
        <tr>
            <td>Halaman 2 dari 2</td>
        </tr>
    </table>
</body>
</html>