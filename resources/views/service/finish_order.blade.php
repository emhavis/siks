@extends('layouts.app')

@section('styles')

@endsection

@section('content') 
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>Layanan<br><span class="c-white">Verifikasi Standar</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-check"></i>
            </div>
            <div class="header-title">
                <h3>Finish Bucket</h3>
                <small>
                    Layanan verifikasi standar Direktorat Metrologi.
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
    <a href="{{ route('service.finish_create') }}" class="btn btn-w-md btn-primary" id="btn_create">Buat Baru</a>
    <!-- <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a> -->

    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tqrcode" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>ID Lab.</th>
                        <th>Staff Penerima</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($data_list->count())
                    @foreach($data_list as $data)
                    <tr>
                        <td>{{ $data->no_order }}</td>
                        <td>LAB. PENGUKURAN MASSA</td>
                        <td>{{ $data->lab_staff }}</td>
                        <td>NEW</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        // $('#serial_number').focus();
    });
</script>
@endsection