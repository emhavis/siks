@extends('layouts.app')
<link href="{{ asset('assets/handsontable/handsontable.full.min.css') }}" rel="stylesheet" media="screen">
<style type="text/css">
    table.input-table td{padding: 1px !important;}
</style>
@section('styles')

@endsection

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-body">
        {!! Form::open(array('id' => 'form_create_labin', 'enctype' => "multipart/form-data")) !!}
        <div class="col-md-6">
            <div class="form-group">
                <label for="metodeuji_id">Metode Uji</label>
                {!! Form::select('metodeuji_id', $mMetodeUji, explode(",",$serviceOrders[0]->metodeuji_id), ['class' => 'form-control multiselect2','id' => 'metodeuji_id', 'required', 'multiple'=>'multiple']) !!}
            </div>
            <div class="form-group">
                <label for="standar_ukuran_id">Standard Ukuran</label>
                {!! Form::select('standar_ukuran_id', $mStandardUkuran, explode(",",$serviceOrders[0]->standar_ukuran_id), ['class' => 'form-control multiselect2','id' => 'standar_ukuran_id', 'required', 'multiple'=>'multiple']) !!}
            </div>
            <div class="form-group margin-t-19">
                <label for="tertelusur">Tertelusur melalui</label>
                {!! Form::text('tertelusur', $serviceOrders[0]->tertelusur, ['class' => 'form-control','id' => 'tertelusur', 'placeholder' => 'Tertelusur ke Satuan Pengukuran SI melalui', 'required']) !!}
            </div>
            <div class="form-group margin-t-19">
                <label for="referensi">Referensi</label>
                {!! Form::text('referensi', $serviceOrders[0]->referensi, ['class' => 'form-control','id' => 'referensi', 'placeholder' => 'Referensi', 'required']) !!}
            </div>
            <div class="form-group margin-t-19">
                <label for="calibrator">Di Kalibrasi Oleh</label>
                {!! Form::text('calibrator', $serviceOrders[0]->calibrator, ['class' => 'form-control','id' => 'calibrator', 'placeholder' => 'Di Kalibrasi oleh', 'required']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-filled panel-c-warning">
                <div class="panel-body">                        
                    <div class="form-group margin-t-19">
                        <label for="suhu_ruang">Kondisi Suhu Ruangan</label>
                        {!! Form::text('suhu_ruang', $serviceOrders[0]->suhu_ruang, ['class' => 'form-control','id' => 'suhu_ruang', 'placeholder' => 'Kondisi Suhu Ruangan', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="lembab_ruang">Kondisi Kelembaban Ruangan</label>
                        {!! Form::text('lembab_ruang', $serviceOrders[0]->lembab_ruang, ['class' => 'form-control','id' => 'lembab_ruang', 'placeholder' => 'Kondisi Kelembaban Ruangan', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="cakupan">Faktor Cakupan</label>
                        {!! Form::text('cakupan', $serviceOrders[0]->cakupan, ['class' => 'form-control','id' => 'cakupan', 'placeholder' => 'Faktor Cakupan', 'required']) !!}
                    </div>
                    <div class="form-group margin-t-19">
                        <label for="remark_sertifikat">Keterangan</label>
                        {!! Form::textarea('remark_sertifikat', $serviceOrders[0]->remark_sertifikat, ['class' => 'form-control','id' => 'remark_sertifikat', 'placeholder' => 'Keterangan', 'size' => '10x5',]) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="tmp_report_id">Pilih Template</label>
                {!! Form::select('tmp_report_id', $template_format_item, explode(",",$serviceOrders[0]->tmp_report_id), ['class' => 'form-control multiselect2','id' => 'tmp_report_id', 'required', 'multiple'=>'multiple','onchange'=>'generateHOT(this)']) !!}
            </div>
            <button role="button" class="btn btn-w-md btn-accent" id="btn_simpan">Proses</button> 
        </div>
        {!! Form::close() !!}
    </div>
</div>
</div>
<div id="hot_container"></div>

@endsection

@section('scripts')
<script src="{{ asset('assets/handsontable/handsontable.full.min.js') }}"></script>
<script src="{{ asset('assets/vendor/generate.js') }}"></script>
<script type="text/javascript">

myhot   = new MyHot();
myform  = new MyForm({id:"form_create_labin",is_edit:"false",urlsubmit:'{{ route('service.simpan',$serviceOrders[0]->id) }}',urlsuccess:'{{ route('service') }}'});

var template_setting = '{{ $template_setting }}';
template_setting = template_setting.replace(/(&quot\;)/g,"\"");
template_setting = JSON.parse(template_setting);
myhot.setTemplateSetting(template_setting);

var template_header = '{{ $template_header }}';
var template_header2 = template_header.replace(/(&quot\;)/g,"\"");
var template_header3 = JSON.parse(template_header2);
// console.log(template_header3);
myhot.setTemplateHeaderHot(template_header3);

var data_uji = '{{ $serviceOrders[0]->hasil_uji }}';
var data_uji2 = data_uji.replace(/(&quot\;)/g,"\"");
var data_uji3 = JSON.parse(data_uji2);
myhot.setDataHot(data_uji3);

function generateHOT(obj)
{
    var template_format = $(obj).select2('data');
    myhot.generateMyHot(template_format);
}

$(document).ready(function()
{
    myform.parsingFormElement();
    myform.setFormFormat();

    generateHOT($("#tmp_report_id"));

    $('#btn_simpan').click(function(e)
    {
        e.preventDefault();
        var form_data = "";

        var hot = myhot.getHot();
        var counter = myhot.getCounter();

        for(var i=0; i<counter.length; i++)
        {
            form_data += '&hot_'+counter[i]['id']+'='+JSON.stringify(hot[counter[i]['id']].getData());
            
            var mergeCells = hot[counter[i]['id']].getPlugin('mergeCells').mergedCellsCollection.mergedCells;
            form_data += '&merge_'+counter[i]['id']+'='+JSON.stringify(mergeCells);
        }
        myform.save(form_data);
    });

    var hot = myhot.getHot();
    var a = hot[1].getCell(row, prop, false).getAttribute("unique-id");
    // var collection = hot[1].getPlugin('mergeCells').mergedCellsCollection.mergedCells;
    // console.log(collection);
    // var collection = hot[2].getPlugin('mergeCells').mergedCellsCollection.mergedCells;
    // console.log(collection);

});


</script>
@endsection