@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>Layanan<br><span class="c-white">Verifikasi</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-user"></i>
            </div>
            <div class="header-title">
                <h3>Pemantauan Layanan Verifikasi</h3>
                <small>
                    Halaman pemantauan proses layanan verifikasi.
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tVerification" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No. Order</th>
                        <th>Tgl. Pekerjaan</th>
                        <th>Nama Instansi/Perusahaan</th>
                        <th>Jenis Layanan</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($serviceOrders->count())
                    @foreach($serviceOrders as $serviceOrder)
                    <tr>
                        <td>{{ $serviceOrder->order_no }}</td>
                        <td>{{ $serviceOrder->service_request->payment_date }}</td>
                        <td>{{ $serviceOrder->service_request->company_name }}</td>
                        <td>{{ $serviceOrder->service_request->service_type->service_type }}</td>
                        <td>{{ $serviceOrder->service_request->status->status }}</td>
                        <td>
                            <a title="lihat" href="{{ route('order.show', $serviceOrder->id) }}" class="btn btn-accent btn-sm" style="margin-right:2px;">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a title="ubah" href="{{ route('order.edit', $serviceOrder->id) }}" class="btn btn-accent btn-sm" style="margin-right:2px;">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function () {
        
        $('#tVerification').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'pantauan_verifikasi', className: 'btn-sm'},
                {extend: 'pdf', title: 'pantauan_verifikasi', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ]
        });

    });
</script>
@endsection