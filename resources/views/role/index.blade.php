@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
    <a href="{{ route('role.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tUser" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>Nama Grup</th>
                        {{-- <th>Status</th> --}}
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($roles->count())
                    @foreach($roles as $role)
                    <tr>
                        <td>{{ ucfirst($role->role_name) }}</td>
                        {{-- <td>{{ $role->is_active=="t"?"Aktif":"Non Aktif" }}</td> --}}
                        <td>
                            <a href="{{ route('role.create', $role->id) }}" class="btn btn-info btn-sm">Edit</a>
                            <a href="#" id="action" data-id="{{ $role->id }}" data-action="delete" class="btn btn-warning btn-sm">Delete</a>
                            {{-- @if($role->is_active!="t")
                            <a href="#" id="action" data-id="{{ $role->id }}" data-action="aktif" class="btn btn-warning btn-sm">Aktif</a>
                            @endif
                            @if($role->is_active=="t")
                            <a href="#" id="action" data-id="{{ $role->id }}" data-action="nonaktif" class="btn btn-warning btn-sm">Non Aktif</a>
                            @endif --}}
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>        
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
$(document).ready(function () {
    
    $('#tUser').DataTable();

    $("a#action").click(function(e)
    {
        // e.preventDefault();
        var data = $(this).data();
        data._token = "{{ csrf_token() }}";
        $.post("{{ route('role.action') }}",data,function(response)
        {
            if(response.status==true)
            {
                toastr["info"]("Data berhasil dihapus", "Info");
                setTimeout(function()
                {
                    window.location.href = "{{ route("role") }}";
                },3000);
            }
            else
            {
                toastr["error"](response["message"], "Error");
            }
        });
    });

});
</script>
@endsection