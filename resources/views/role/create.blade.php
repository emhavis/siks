@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled">
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="role_name">Nama Role/Group</label> 
                        {!!
                            Form::text("role_name",$row?$row->role_name:'',[
                            'class' => 'form-control',
                            'id' => 'role_name',
                            'placeholder' => 'Nama Role/Group',
                            'required'
                            ]);
                        !!}
                    </div>
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function ()
{
    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $.post('{{ route('role.store') }}',formData,function(res)
        {
            if(res.status==false)
            {
                var msg = show_notice(res.messages);
            }
            else
            {
                window.location = '{{ route('role') }}';
            }
        });

    });
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection