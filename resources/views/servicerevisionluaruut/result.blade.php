@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    @if($serviceOrder->serviceRequestItem->status_id == 16)
        <div class="panel-body">
        {!! Form::open(['url' => route('serviceluaruut.confirmcancel', $serviceOrder->id),'files'=>true,'name'=>'cancel_form'])  !!}
        <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis">Jenis</label>
                    {!! Form::text('jenis', $serviceOrder->ServiceRequestItem->uuts->stdtype->uut_type, ['class' => 'form-control','id' => 'jenis', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group input-group" role="group">
                    <span class="input-group-text">Kapasitas dan Satuan </span>
                    {!! Form::text('kapasitas', $serviceOrder->tool_capacity, ['class' => 'form-control','aria-label' => 'kapasitas','id' => 'kapasitas', 'placeholder' => 'Kapasitas']) !!}
                    {!! Form::text('capacity_unit', $serviceOrder->tool_capacity_unit, ['class' => 'form-control','aria-label' => 'capacity_unit','id' => 'capacity_unit','placeholder' => 'Unit']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Merek/Model/Tipe/Nomor Seri</label>
                    {!! Form::text('merek', 
                        $serviceOrder->tool_brand . '/' . 
                        $serviceOrder->tool_model . '/' .
                        $serviceOrder->tool_type . '/' .
                        $serviceOrder->serial_no,
                        ['class' => 'form-control','id' => 'merek', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="merek">Buatan</label>
                    {!! Form::text('buatan', 
                        $serviceOrder->tool_made_in,
                        ['class' => 'form-control','id' => 'buatan']) !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Pemilik</label>
                    {!! Form::text('label_sertifikat', 
                        $serviceOrder->ServiceRequest->label_sertifikat,
                        ['class' => 'form-control','id' => 'label_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::text('addr_sertifikat', 
                        $serviceOrder->ServiceRequest->addr_sertifikat,
                        ['class' => 'form-control','id' => 'addr_sertifikat']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_no">Dasar Pengujian, Nomor</label>
                    {!! Form::text('reference_no', 
                        $serviceOrder->ServiceRequestItem->reference_no,
                        ['class' => 'form-control','id' => 'reference_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="reference_date">Dasar Pengujian, Tanggal</label>
                    {!! Form::text('reference_date', 
                        $serviceOrder->ServiceRequestItem->reference_date,
                        ['class' => 'form-control','id' => 'reference_date', 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pending_notes">Alasan Pembatalan Pemeriksaan/Pengujian</label>
                    {!! Form::textArea('cancel_notes', 
                        $serviceOrder->cancel_notes,
                        ['class' => 'form-control','id' => 'cancel_notes','rows'=>'5']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="cancelation_file" style="padding-bottom:20px">Lampiran Pembatalan</label>
                    {!! Form::file('cancelation_file', null,
                        ['class' => ['form-control','form-control-lg'],'id' => 'cancelation_file']) !!}
                </div>
                <a class="btn btn-danger" href="{{ route('file.show', [$serviceOrder->id,'batal']) }}">Lihat</a>
                <!-- <label><a href="{{route('serviceuut.cancelation_download',$serviceOrder->id)}}">{{$serviceOrder->cancelation_file}}</a></label> -->
            </div>
        </div>
        <button role="submit" class="btn btn-w-md btn-danger" id="btn_simpan">Batal Uji</button>
        {!! Form::close() !!}
            
        </div>
    @else
        <div class="panel-body">
            @if($serviceOrder->subkoordinator_notes != null)
            <div class="alert alert-warning" role="alert">
                {{ $serviceOrder->subkoordinator_notes }}
            </div>
            @endif

            {!! Form::open(['url' => route('servicerevisionluaruut.resultupload', $serviceOrder->id), 'files' => true, 'id' => 'form_result'])  !!}
            
            <input type="hidden" name="id" value="{{ $serviceOrder->id }}" />
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                    <label><h2>{{$serviceOrder->serviceRequest->svc->service_type}} ( {{$serviceOrder->serviceRequestItem->no_order}}) </h2></p> Di order pada tanggal : {{ date("d-m-Y", strtotime($serviceOrder->ServiceRequestItem->order_at)) }}</label>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="jenis_uttp">Jenis UUT</label>
                        {!! Form::text('jenis_uttp', $serviceOrder->ServiceRequestItem->uuts->stdtype->uut_type, ['class' => 'form-control','id' => 'jenis_uut']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="tool_name">Nama Alat</label>
                        {!! Form::text('tool_name', $serviceOrder->tool_name, ['class' => 'form-control','id' => 'tool_name', ]) !!}
                    </div>
                </div>
            </div>

            @if($serviceOrder->tool_capacity_min >0)
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="kapasitas">Kapasitas Minimal</label>
                        {!! Form::text('kapasitas', $serviceOrder->tool_capacity_min, ['class' => 'form-control','id' => 'kapasitas']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="kapasitas">Satuan</label>
                        {!! Form::text('satuan', $serviceOrder->tool_capacity_min_unit, ['class' => 'form-control','id' => 'kapasitas']) !!}
                    </div>
                </div>
            </div>

            @endif


            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="kapasitas">Kapasitas</label>
                        {!! Form::text('kapasitas', $serviceOrder->tool_capacity, ['class' => 'form-control','id' => 'kapasitas']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="kapasitas">Satuan</label>
                        {!! Form::text('satuan', $serviceOrder->tool_capacity_unit, ['class' => 'form-control','id' => 'kapasitas']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="tool_dayabaca">Daya Baca</label>
                        {!! Form::text('tool_dayabaca', $serviceOrder->tool_dayabaca, ['class' => 'form-control','id' => 'tool_dayabaca']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="tool_dayabaca_unit">Satuan</label>
                        {!! Form::text('tool_dayabaca_unit', $serviceOrder->tool_dayabaca_unit, ['class' => 'form-control','id' => 'tool_dayabaca_unit']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="tool_brand">Merek</label>
                        {!! Form::text('tool_brand', $serviceOrder->tool_brand, ['class' => 'form-control','id' => 'tool_brand']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="tool_model">Model/Tipe</label>
                        {!! Form::text('tool_model', $serviceOrder->tool_model, ['class' => 'form-control','id' => 'tool_model']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="tool_serial_no">No Seri/Identitas</label>
                        {!! Form::text('tool_serial_no', $serviceOrder->tool_serial_no, ['class' => 'form-control','id' => 'serial_no']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="tool_class">Kelas</label>
                        {!! Form::text('tool_class', $serviceOrder->tool_class, ['class' => 'form-control','id' => 'tool_class']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="tool_jumlah">Jumlah</label>
                        {!! Form::text('tool_jumlah', $serviceOrder->tool_jumlah, ['class' => 'form-control','id' => 'tool_jumlah']) !!}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="merek">Buatan</label>
                        {!! Form::text('buatan', 
                            $serviceOrder->tool_made_in,
                            ['class' => 'form-control','id' => 'buatan']) !!}
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="label_sertifikat">Pemilik</label>
                        {!! Form::text('label_sertifikat', 
                            $serviceOrder->ServiceRequest->label_sertifikat,
                            ['class' => 'form-control','id' => 'label_sertifikat']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="addr_sertifikat">Alamat Pemilik</label>
                        {!! Form::text('addr_sertifikat', 
                            $serviceOrder->ServiceRequest->addr_sertifikat,
                            ['class' => 'form-control','id' => 'addr_sertifikat']) !!}
                    </div>
                </div>
            </div>

            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="test_by_1">Diuji Oleh (1)</label>
                        {!! Form::text('test_by_1', 
                            $user->full_name,
                            ['class' => 'form-control','id' => 'test_by_1', 'readonly']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="test_by_2">Diuji Oleh (2)</label>
                        {!! Form::select('test_by_2', $users, $serviceOrder->test_by_2, 
                            ['class' => 'form-control select2', 'id' => 'test_by_2']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="staff_entry_datein">Waktu Pengujian, Mulai</label>
                        {!! Form::text('staff_entry_datein', 
                            date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_datein) ? $serviceOrder->staff_entry_datein : date("Y-m-d"))),
                            ['class' => 'form-control','id' => 'staff_entry_datein', 'readonly']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="staff_entry_dateout">Waktu Pengujian, Selesai</label>
                        {!! Form::text('staff_entry_dateout', 
                            date("d-m-Y", strtotime(isset($serviceOrder->staff_entry_dateout) ? $serviceOrder->staff_entry_dateout : date("Y-m-d"))),
                            ['class' => 'form-control','id' => 'staff_entry_dateout', 'readonly']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                        <div class="form-group">
                            <label for="hasil_uji_memenuhi">Status Akreditasi Sertifikat</label>
                            {!! Form::select('is_certified', [False=>'Tidak', True=>'Ya'], null, 
                                ['class' => 'form-control select2', 'id' => 'is_certified']) !!}
                        </div>
                </div>
                <div class="col-md-6">
                        <div class="form-group col-md-6">
                            <label for="hasil_uji_memenuhi">Halaman Sertifikat</label>
                            {!! Form::text('page_num', $serviceOrder->page_num ,
                                ['class' => 'form-control', 'id' => 'page_num']) !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="sertifikat_expired_at">Berlaku Sampai (dd-mm-yyyy)</label>
                            {!! Form::text('sertifikat_expired_at', null,
                            ['class' => 'form-control','id' => 'sertifikat_expired_at']) !!}
                        </div>
                </div>
            </div>
            @if($serviceOrder->ServiceRequest->service_type_id == 1)
            <div class="row">
                <div class="col-md-6">
                        <div class="form-group">
                            <label for="hasil_uji_memenuhi">Memenuhi Syarat</label>
                            {!! Form::select('hasil_uji_memenuhi', [True=>'Ya', False=>'Tidak'], $serviceOrder->hasil_uji_memenuhi, 
                                ['class' => 'form-control select2', 'id' => 'hasil_uji_memenuhi']) !!}
                        </div>
                </div>
                <div class="col-md-6">
                    <!-- <div class="form-group berlaku">
                        <label for="sertifikat_expired_at">Berlaku Sampai (dd-mm-yyyy)</label>
                        {!! Form::text('sertifikat_expired_at', null,
                        ['class' => 'form-control','id' => 'sertifikat_expired_at']) !!}
                    </div> -->
                    <div class="form-group berlaku">
                        <label for="sertifikat_expired_at">Alasan Pembatalan <strong style="color:red;">&nbsp;*</strong></label>
                        {!! Form::textArea('cancel_notes', $serviceOrder->cancel_notes,
                        ['class' => 'form-control','id' => 'cancel_notes','rows'=>4]) !!}
                    </div>
                </div>
            </div>
            @endif
            @if($serviceOrder->ServiceRequest->service_type_id == 2)
            <div class="row">
                <div class="col-md-6">
                        <div class="form-group">
                            <label for="hasil_uji_memenuhi">Memenuhi Syarat</label>
                            {!! Form::select('hasil_uji_memenuhi', [True=>'Ya', False=>'Tidak'], null, 
                                ['class' => 'form-control select2', 'id' => 'hasil_uji_memenuhi']) !!}
                        </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group berlaku">
                    <label for="sertifikat_expired_at">Alasan Pembatalan <strong style="color:red;">&nbsp;*</strong></label>
                    {!! Form::textArea('cancel_notes', null,
                    ['class' => 'form-control','id' => 'cancel_notes','rows'=>4]) !!}
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class ="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="file_skhp">Lampiran Cerapan<strong style="color:red;">&nbsp;*</strong></label>
                                {!! Form::file('file_skhp', null,
                                    ['class' => 'form-control','id' => 'file_skhp']) !!}
                            </div>
                        </div>
                        <div class="cal-md-8">
                            @if($serviceOrder->path_skhp !=null)
                            <div class="input-group">
                                <label style="padding-top:30px"></label>
                                <a  class="btn btn-danger" href="{{ route('file.show', [$serviceOrder->id,'cerapan']) }}">Lihat</a>
                                <label> {{$serviceOrder->path_skhp}}</label>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class ="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="lblC" name ="lblC" for="file_lampiran">Lampiran Sertifikat <strong style="color:red;">&nbsp;*</strong></label>
                                {!! Form::file('file_lampiran', null,
                                    ['class' => 'form-control','id' => 'file_lampiran']) !!}
                            </div>
                        </div>
                        <div class="cal-md-8">
                            @if($serviceOrder->path_skhp !=null)
                            <div class="input-group">
                                <label style="padding-top:30px"></label>
                                <a  class="btn btn-danger" href="{{ route('file.show', [$serviceOrder->id,'lampiran']) }}">Lihat</a>
                                <label> {{$serviceOrder->path_lampiran}}</label>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="stat_sertifikat" id="stat_sertifikat" value="0"/>
            <button role="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan Hasil Uji</button>
            <button class="btn btn-w-md btn-success" id="btn_simpan_submit">Kirim Hasil Uji</button>
            
            {!! Form::close() !!}
        </div>
    @endif
</div>
</div>


@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function ()
    {
        
        $(".berlaku").hide();
        var hasil ='{{$serviceOrder->hasil_uji_memenuhi}}'
        var expired = '{{$serviceOrder->sertifikat_expired_at}}'
        if(!hasil){
            $(".berlaku").show();    
            $("#sertifikat_expired_at").val(expired); 
            document.getElementById('lblC').innerHTML = 'Lampiran Sertifikat <strong style="color:red;">&nbsp;</strong>';
        }
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });
        /*
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        */

        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_alg_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_add_badanhitung",function()
        {
            var table = $("#data_table_ttu_mabbm_badanhitung");
            var insp_item = '<tr>' + 
                '   <td><input type="text" class="form-control" name="brand[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="type[]" /></td>' + 
                '   <td><input type="text" class="form-control" name="serial_no[]" /></td>' + 
                '   <td>' +
                '       <input type="hidden" name="badanhitung_item_id[]" />' +
                '       <button type="button" id="btn_remove_badanhitung" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm_badanhitung').on("click","button#btn_remove_badanhitung",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_alg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_alg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="input_level[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_up[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_down[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_hysteresis[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_alg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mabbm");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="meter_factor[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mabbm').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mg').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mg");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mg').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_ma').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_ma");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_ma').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $('#data_table_ttu_mguwc').on("click","button#btn_add",function()
        {
            var table = $("#data_table_ttu_mguwc");
            var insp_item = '<tr>' + 
                '   <td><input type="number" class="form-control" name="flow_rate[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="error_bkd[]" /></td>' + 
                '   <td><input type="number" class="form-control" name="repeatability[]" /></td>' +
                '   <td><input type="number" class="form-control" name="repeatability_bkd[]" /></td>' +
                '   <td>' +
                '       <input type="hidden" name="item_id[]" />' +
                '       <button type="button" id="btn_remove" class="btn btn-danger btn-sm faa-parent animated-hover"><i class="fa fa-minus faa-flash"></i></button>' +
                '   </td>' +
                '</tr>' ;
            table.append(insp_item);
        });
        $('#data_table_ttu_mguwc').on("click","button#btn_remove",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            $("#stat_sertifikat").val(1);

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#hasil_uji_memenuhi").on("change", function(){
            if($(this).val() ==false){
                $(".berlaku").show();
                document.getElementById('lblC').innerHTML = 'Lampiran Sertifikat <strong style="color:red;">&nbsp;</strong>';
            }else{
                $(".berlaku").hide();
                document.getElementById('lblC').innerHTML = 'Lampiran Sertifikat <strong style="color:red;">&nbsp;*</strong>';
            }
        });
    });
</script>
@endsection