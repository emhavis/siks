@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">

    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            
            <table id="table_data_penjadwalan" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Pendaftaran</th>
                        <th>No Order</th>
                        <th>Nama Pemilik</th>
                        <th>Nama Pemohon</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->no_register }}</td>
                        <td>
                        @if($row->payment_code==null)
                        xx-xxxx-xx-xxx
                        @endif
                        @if($row->payment_code!=null)
                        {{ $row->no_order }}
                        @endif
                        </td>
                        <td>{{ $row->label_sertifikat }}</td>
                        <td>{{ $row->requestor->full_name }}</td>
                        <td>{{ $row->status->status }}
                        @if($row->spuh_payment_valid_date == null)
                        <br/><span class="text-danger">Pembayaran SPUH belum diselesaikan</span>
                        @endif 
                        </td>
                        <td>
                            @if($row->spuh_doc_id != null)
                            <a href="{{ route('schedulinguut.surattugas', ['id' => $row->id, 'stream' => 1]) }}" 
                                class="btn btn-info btn-sm">Surat Tugas</a>
                            @endif
                            @if($row->spuh_payment_valid_date != null)
                            @if($row->status_revisi_spt == null || $row->status_revisi_spt == 0)
                            <button class="btn btn-warning btn-sm btn-mdl" 
                                data-id="{{ $row->id }}"
                                data-noregister="{{ $row->no_register }}"
                                data-pemilik="{{ $row->label_sertifikat }}"
                                data-pemohon="{{ $row->requestor->full_name }}"
                                >Selesai Uji</button>
                            @elseif($row->status_revisi_spt == 3)
                            <a href="{{ route('serviceprocessluaruut.check', $row->id) }}" class="btn btn-warning btn-sm">Order</a>
                            @endif
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>

        </div>
    </div>

</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Revisi SPT</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <input type="hidden" name="id" id="id"/>

                            <div class="form-group">
                                <label>No Pendaftaran</label>
                                <input type="text" name="no_register" id="no_register" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemilik</label>
                                <input type="text" name="pemilik" id="pemilik" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemohon</label>
                                <input type="text" name="pemohon" id="pemohon" class="form-control" readonly required />
                            </div>

                            <div class="form-group">
                                <label>Apakah ada <strong>Perubahan/Revisi SPT</strong> atas permohonan ini?</label>
                                <select name="status_revisi_spt" id="status_revisi_spt" class="form-control select2">
                                    <option value="1">Ya, ada Revisi SPT</option>
                                    <option value="3">Tidak ada Revisi SPT</option>
                                </select>
                            </div>

                            <div class="form-group" id="ket-rev">
                                <label>Keterangan Revisi</label>
                                <textarea name="keterangan_revisi" id="keterangan_revisi" class="form-control"></textarea>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan-revisi" class="btn btn-danger">Simpan</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan").DataTable();
        $('#table_data_penjadwalan tbody').on( 'click', 'button.btn-mdl', function (e) {
            e.preventDefault();
            var id = $(this).data().id;
            var no_register = $(this).data().noregister;
            var pemilik = $(this).data().pemilik;
            var pemohon = $(this).data().pemohon;

            $("#id").val(id);
            $("#no_register").val(no_register);
            $("#pemilik").val(pemilik);
            $("#pemohon").val(pemohon);

            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#status_revisi_spt").select2();
            });
        })
        var form =  $("#form_modal");
        form.validate({
            rules: {
                status_revisi_spt: {
                    required: true,
                },
            },
            messages: {
                status_revisi_spt: 'Status harus dipilih',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                console.log($(e).closest(".form-group"));
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });
        $("#simpan-revisi").click(function(e) {
            e.preventDefault();

            var id = $('#id').val();

            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            if (form.valid()) {
                $.post('{{ route('serviceprocessluaruut.statusrevisi') }}',form_data,function(response)
                {
                    if(response.status===true)
                    {
                        location.reload();
                    }
                    else
                    {
                        var msg = show_notice(response.messages);
                        toastr["error"]("Mohon periksa kembali","Form Invalid");
                    }
                });
            }
        });
    });
</script>
@endsection