@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

@endsection

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(['url' => route('serviceprocessluaruut.savenewitem', $request->id), 'id' => 'form_result'])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Standar Ukuran (UUT)</h4>
            </div>

            {!! Form::hidden('id', $request->id, ['id' => 'id']) !!}
            {!! Form::hidden('owner_id', $request->uttp_owner_id, ['id' => 'owner_id']) !!}
            {!! Form::hidden('uttp_id', $request->uttp_owner_id, ['id' => 'uttp_id']) !!}

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="label_sertifikat">Nama Pemilik</label>
                            {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Pemilik</label>
                            {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>  
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="uttp_type_id">Jenis UUT</label>
                            {!! Form::select('standard_type_id', $standard_types, null, ['class' => 'form-control select2','id' => 'standard_type_id']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="serial_no">Pencarian Berdasarkan Nomor Seri</label>
                            {!! Form::text('serial_no', '', ['class' => 'form-control','id' => 'serial_no']) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                <button type="button" class="btn btn-w-md btn-accent" id="btn_cari">Cari</button> 
                <button type="button" class="btn btn-w-md btn-accent" id="btn_create">Tambah</button> 
                <label>Untuk menambah standar ukuran baru, pastikan jenis UUT sudah terpilih dengan benar.</label>
                
                <br/><br/>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-hover table-responsive-sm" id="t-roll">
                            <thead style="font-weight:bold; color:white;">
                                <tr>
                                    <th>Merek</th>
                                    <th>Model/Tipe</th>
                                    <th>No.Seri</th>
                                    <th>Media Uji</th>
                                    <th>Max.Capacity</th>
                                    <th>Min.Capacity</th>
                                    <th>Satuan</th>
                                    <th>Buatan</th>
                                    <th>Pabrikan</th>
                                    <th>Alamat Pabrik</th>
                                    <th></th>
                                </tr>
                            </thead>   
                        </table> 
                    </div>
                </div>
            </div>
        </div>

        
        </form>

        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/jquery-validation/additional-methods.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $('#standard_type_id').select2();

        $('#t-roll').DataTable({
            "scrollX": true
        });

        $("#btn_cari").click(function(e){
            e.preventDefault();

            var selected = $('#standard_type_id').select2('data')[0].id;
            console.log(selected);

            $.ajax({
                url: '{{ route('serviceprocessluaruut.getuuts') }}',
                type: 'post',
                data: {
                    _token:'{{ csrf_token() }}',
                    owner_id: $('#owner_id').val(),
                    uttp_type_id: selected,
                    serial_no: $('#serial_no').val()
                },
                beforeSend: function() {
                    $(".progress-spin").show();
                },
                success: function(response){
                    //console.log(response);
                    var table = $("#t-roll");
                    $("#t-roll > tbody").empty();
                    response.forEach(it => {
                        var item = 
                        '<tr>' +
                            '<td>' + it.tool_brand + '</td>' +  
                            '<td>' + it.tool_model + '</td>' +  
                            '<td>' + it.serial_no + '</td>' +  
                            '<td>' + it.tool_media + '</td>' +  
                            '<td>' + it.tool_capacity + ' ' + it.tool_capacity_unit + '</td>' +  
                            '<td>' + it.tool_capacity_min + ' ' + it.tool_capacity_unit + '</td>' +  
                            '<td>' + it.tool_capacity_unit + '</td>' +  
                            '<td>' + it.tool_made_in + '</td>' +  
                            '<td>' + it.tool_factory + '</td>' +  
                            '<td>' + it.tool_factory_address + '</td>' +
                            '<td>' + '<button type="button" id="btn_add_item" class="btn btn-danger btn-sm faa-parent animated-hover btn-add-item" data-id="' + it.id + '"><i class="fa fa-add faa-flash"></i> Tambah</button>'  + '</td>' +  
                        '</tr>';
                        table.append(item);
                    });
                },
                complete:function(data){
                    $(".progress-spin").hide();
                    
                }
            });
        });

        $("#btn_create").click(function(e){
            e.preventDefault();

            var selected = $('#standard_type_id').select2('data')[0].id;
            console.log(selected);

            var url = '{{ route("serviceprocessluaruut.createitem", ["id"=>":id", "standard_type_id"=>":standard_type_id"] ) }}';
            url = url.replace(':id', $("#id").val()); 
            url = url.replace(':standard_type_id', selected); 

            window.location.href = url;
        });
        
    });

    $('#t-roll').on("click","button#btn_add_item",function(e) {
        var id = $(e.target).data('id');

        $('#uttp_id').val(id);
        
        var form = $("#form_result");
        if (form.valid()) {
            toastr["warning"]("Sedang menyimpan. Harap tunggu.");
            form[0].submit();
        }
    })

</script>
@endsection