@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
            
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Booking</h4>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="label_sertifikat">Nama Pemilik</label>
                                    {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="addr_sertifikat">Alamat Pemilik</label>
                                    {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                                </div>
                            </div>  
                        </div>

                    </div>
                </div>

                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Pemohon</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_name">Nama Pemohon</label>
                                    {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_name">Jenis Tanda Pengenal</label>
                                    {!! Form::text('id_type_id', $requestor->kantor == "Perusahaan" ? "NIB" : "NPWP", ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                                    {!! Form::text('pic_id_no', $requestor->kantor == "Perusahaan" ? $requestor->nib : $requestor->npwp, ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_phone_no">Nomor Telepon</label>
                                    {!! Form::text('pic_phone_no', $requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="pic_email">Alamat Email</label>
                                    {!! Form::text('pic_email', $requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_from">Usulan Jadwal Mulai</label>
                                    {!! Form::text('scheduled_test_date_from', date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_from', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_to">Usulan Jadwal Selesai</label>
                                    {!! Form::text('scheduled_test_date_to', date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_to', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis_layanan">Jenis Layanan</label>
                                    {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lokasi_pengujian">Lokasi Pengujian</label>
                                    {!! Form::text('lokasi_pengujian', $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' , ['class' => 'form-control','id' => 'lokasi_pengujian', 'readonly']) !!}
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Penugasan dan Penjadwalan</h4>
                    </div>
                    <div class="panel-body" id="panel_staff">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_from">Surat Tugas</label>
                                    {!! Form::text('spuh_spt', $request->spuh_spt,
                                        ['class' => 'form-control','id' => 'spuh_spt', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                @if($request->spuh_doc_id != null)
                                <a href="{{ route('schedulinguut.surattugas', ['id' => $request->id, 'stream' => 1]) }}" 
                                    class="btn btn-info btn-sm">Surat Tugas</a>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inspection_prov_id">Alamat Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                                    {!! Form::text('inspection_prov_id', 
                                        $request->inspectionKabkot->nama .', '. $request->inspectionProv->nama,
                                        ['class' => 'form-control','id' => 'inspection_prov_id', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="spuh_rate">Uang Harian</label>
                                    {!! Form::hidden('spuh_rate_id', $request->spuh_rate_id,
                                        ['class' => 'form-control','id' => 'spuh_rate_id', 'readonly']) !!}
                                    {!! Form::text('spuh_rate', $request->spuh_rate,
                                        ['class' => 'form-control','id' => 'spuh_rate', 'readonly']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_from">Jadwal Waktu Pengujian, Mulai</label>
                                    {!! Form::text('scheduled_test_date_from', 
                                        date("d-m-Y", strtotime(isset($doc->scheduled_test_date_from) ? $doc->scheduled_test_date_from : date("Y-m-d"))),
                                        ['class' => 'form-control','id' => 'scheduled_test_date_from', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="scheduled_test_date_to">Jadwal Waktu Pengujian, Selesai</label>
                                    {!! Form::text('scheduled_test_date_to', 
                                        date("d-m-Y", strtotime(isset($doc->scheduled_test_date_to) ? $doc->scheduled_test_date_to : date("Y-m-d"))),
                                        ['class' => 'form-control','id' => 'scheduled_test_date_to', 'readonly']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="table_data_penjadwalan">Penguji/Pemeriksa</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table_data_penjadwalan" class="table table-striped table-hover table-responsive-sm">
                                    <thead>
                                        <tr>
                                            <th>NIK</th>
                                            <th>Nama</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($staffs as $row)
                                    @if($row->scheduledStaff != null)
                                        <tr>
                                            <td>{{ $row->scheduledStaff->nip }}</td>
                                            <td>{{ $row->scheduledStaff->nama }}</td>
                                        </tr>
                                    @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Item Pengujian</h4>
                    </div>

                    <div class="panel-body">

                        <div class="row"><div class="col-md-6">
                            <a href="{{ route('serviceprocessluaruut.newitem', $request->id) }}"
                                class="btn btn-warning btn-sm" id="btn_add_item">
                                <i class="fa fa-add"></i> Tambah UUT</a><br/><br/>
                        </div></div>
                        <div class="row"><div class="col-md-12">
                            <table class="table table-striped table-hover table-responsive-sm" id="t-roll">
                                <thead style="font-weight:bold; color:white;">
                                    <tr>
                                        <th>No</th>
                                        <th>Jenis UUT</th>
                                        <th>Merek</th>
                                        <th>Model/Tipe</th>
                                        <th>No.Seri</th>
                                        <th>Media Uji</th>
                                        <th>Max.Capacity</th>
                                        <th>Min.Capacity</th>
                                        <th>Satuan</th>
                                        <th>Buatan</th>
                                        <th>Pabrikan</th>
                                        <th>Alamat Pabrik</th>
                                        @if ($request->lokasi_pengujian == 'luar')
                                        <th style="width: 200px;">Alamat Lokasi Pengujian</th>
                                        @else 
                                        @if ($request->service_type_id == 4 || $request->service_type_id == 5)
                                        <th style="width: 200px;">Lokasi Alat</th>
                                        @endif
                                        @endif
                                        <th>Kelengkapan Persyaratan (Break Down) </th>
                                        @if ($request->lokasi_pengujian == 'luar')
                                        <th style="width: 200px;">Item Pengujian</th>
                                        <th>Total Biaya</th>
                                        <th>Confirmed ?</th>
                                        <th></th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $num=0 ?>
                                    @foreach($request->items as $item)
                                        <?php $num +=1 ?>
                                        <tr>
                                            <form id="form_item_{{ $item->id }}">
                                                {!! Form::hidden('id', $item->id) !!}
                                                {!! Form::hidden('uut_id', $item->uut_id) !!}
                                                {!! Form::hidden('standard_type_id', $item->uuts->type_id) !!}
                                                <td>{{ $num}}</td>
                                                <td>{{ $item->uuts->stdtype->uut_type}}</td>
                                                <td>{{ $item->uuts->tool_brand}}</td>
                                                <td>{{ $item->uuts->tool_model}}</td>
                                                <td>{{ $item->uuts->serial_no}}</td>
                                                <td>{{ $item->uuts->tool_media}}</td>
                                                <td>{{ $item->uuts->tool_capacity}}
                                                <td>{{ $item->uuts->tool_capacity_min}}</td>
                                                <td>{{ $item->uuts->tool_capacity_unit }}</td>
                                                <td>{{ $item->uuts->tool_made_in }}</td>
                                                <td>{{ $item->uuts->tool_factory }}</td>
                                                <td>{{ $item->uuts->tool_factory_address }}</td>
                                                @if ($request->lokasi_pengujian == 'luar')
                                                <td>
                                                    {{ $item->location }}, {{ ($item->kabkot ? $item->kabkot->nama : '') . ', ' . ($item->provinsi ? $item->provinsi->nama : '') }}
                                                    <br/>
                                                    ({{ $item->location_lat }}, {{ $item->location_long }})
                                                </td>
                                                @else 
                                                @endif
                                                <td> 
                                                @if ($request->service_type_id == 1)
                                                    @if($item->path_application_letter != null)
                                                        <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $item->id }}"
                                                            class="btn btn-default btn-sm faa-parent animated-hover" >
                                                            <i class="fa fa-download faa-flash"></i> Surat Permohonan
                                                        </a>
                                                    @endif
                                                @elseif ($request->service_type_id == 2)
                                                    @if($item->path_last_certificate != null)
                                                        <a href="<?= config('app.siks_url') ?>/tracking/download_uut/last_certificate/{{ $item->id }}"
                                                            class="btn btn-default btn-sm faa-parent animated-hover" >
                                                            <i class="fa fa-download faa-flash"></i> Sertifikat Sebelumnya
                                                        </a>
                                                    @endif
                                                @endif
                                                </td>
                                                @if ($request->lokasi_pengujian == 'luar')
                                                
                                                <td>
                                                    @foreach($item->inspections as $inspection)
                                                        {{ $inspection->inspectionPrice->inspection_type }}
                                                        @if($loop->remaining)
                                                        <br/>
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>{{ number_format($item->subtotal, 2, ',', '.') }}</td>
                                                <td>{{ $item->status_id === 1 ? 'No' : 'Yes'}}</td>
                                                <td> 
                                                    <a href="{{ route('serviceprocessluaruut.createbookinginspection', $item->id) }}"
                                                    class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_add_inspeksi">
                                                    <i class="fa fa-edit faa-flash"></i> Item Uji</a> 
                                                    <a href="{{ route('serviceprocessluaruut.edititem', ['id'=>$request->id, 'idItem'=>$item->id]) }}"
                                                    class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_add_inspeksi">
                                                    <i class="fa fa-edit faa-flash"></i> Edit Alat</a> 
                                                    
                                                    <button id="hapus" type="button" class="btn btn-warning btn-sm faa-parent animated-hover pull-right btn-hapus" data-id="{{ $item->id }}">
                                                    <i class="fa fa-edit faa-flash"></i> Hapus Alat</a> 
                                                    </button>
                                                </td>
                                                @endif
                                            </form>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div></div>

                    </div>
            
                 </div>

                {!! Form::open(['url' => route('serviceprocessluaruut.savecheck', $request->id), 'files' => true, 'id' => 'form_result'])  !!}
        
                <input type="hidden" name="id" value="{{ $request->id }}" />
                <input type="hidden" name="submit_val" id="submit_val" value="0"/>
                
                <button type="button" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button>  
                <button type="button" class="btn btn-w-md btn-accent" id="btn_simpan_submit">Simpan dan Konfirmasi</button>  

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="hapusModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {!! Form::open(['url' => route('serviceprocessluaruut.hapusitem'), 'id' => 'form_delete'])  !!}
        <div class="modal-content">
            <div class="modal-body">
                <p>Data perlengkapan benar akan dihapus?</p>
                <input type="hidden" name="id" id="item_id" />
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Hapus</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection


@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/jquery-validation/additional-methods.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan, #data_table_check").DataTable();

        $('#t-roll').DataTable({
            "scrollX": true
        });

        $('.is_accepted').select2({
            //placeholder: "- Pilih UML -",
            allowClear: true
        }).on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data.id);
            
            if (data.id == 'tidak') {
                $("#keterangan_tidak_lengkap").prop("readonly", false); 
            } else {
                $("#keterangan_tidak_lengkap").val("");
                $("#keterangan_tidak_lengkap").prop("readonly", true); 
            }
        });
        
        $("#btn_simpan").click(function(e){
            e.preventDefault();

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            $("#submit_val").val(1);

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $('.btn-hapus').click(function(e){
            console.log($(this).data("id"));
            e.preventDefault();
            $("#hapusModal").modal('show');
            $("#item_id").val($(this).data("id"));
        });
    });
</script>
@endsection