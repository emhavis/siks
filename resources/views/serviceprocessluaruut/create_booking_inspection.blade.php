@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

@endsection

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(['url' => route('serviceprocessluaruut.simpanbookinginspection', $item->id)])  !!}
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Standar Ukuran (UUT)</h4>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="standard_type_id">Jenis UUT</label>
                            {!! Form::text('standard_type_id', $item->uuts->stdType->uut_type, ['class' => 'form-control','id' => 'standard_type_id', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="standard_type_id">Nama Alat</label>
                            {!! Form::text('standard_type_id', $item->uuts->tool_name, ['class' => 'form-control','id' => 'standard_type_id', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_brand">Merek/Model/Tipe/No Seri</label>
                            {!! Form::text('tool_brand', $item->uuts->tool_brand . ' / ' .$item->uuts->tool_model . ' / ' . $item->uuts->serial_no, 
                                ['class' => 'form-control','id' => 'tool_brand', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jumlah">Jumlah</label>
                            {!! Form::text('jumlah', $item->uuts->jumlah, ['class' => 'form-control','id' => 'jumlah', 'readonly']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Item Pengujian/pemeriksaan</h4>
            </div>
            <div class="panel-body">
                <table id="table" class="table table-responsive-sm input-table">
                    <thead>
                        <tr>
                            <th>Diuji</th>
                            <th>Jenis Pengujian</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                            <th>Harga Satuan</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($prices as $price)
                        <tr>
                            <td>
                                <?php
                                    $checked = false;
                                    $quantity = null;
                                    foreach($item->inspections as $inspection) {
                                        if ($inspection->inspection_price_id == $price->id) {
                                            $checked = true;
                                            $quantity = $inspection->quantity;
                                            break;
                                        }
                                    } 
                                ?>
                                <input type="hidden" name="id[]" value="{{ $price->id }}" />
                                <input type="checkbox" name="price_selected[]" data-id="{{ $price->id }}" class="price_selected" 
                                    {{ $checked ? 'checked' : '' }}
                                />
                            </td>
                            <td>{{ $price->inspection_type }}</td>
                            <td>
                                <input readonly type="number" name="quantity[]" id="quantity" data-id="{{ $price->id }}" class="form-control quantity" 
                                    value="{{ $quantity }}"
                                />
                            </td>
                            <td>{{ $price->unit }}</td>
                            <td><input readonly type="number" name="price[]" id="price" data-id="{{ $price->id }}" class="form-control price" value="{{ $price->price }}"/></td>
                            <td>
                                <input readonly type="number" name="subtotal[]" id="subtotal" data-id="{{ $price->id }}" class="form-control subtotal" 
                                value="{{ $quantity * $price->price }}"/>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4"></td>
                            <td>Total: </td>
                            <td><input readonly type="number" name="subtotal_item" id="subtotal_item" class="form-control" value="{{ number_format($item->subtotal,0,'.','') }}"/></td>
                        </tr>                            
                    </tfoot>
                </table>
            </div>
        </div>

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Lanjut</button> 
        </form>
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        /*
        let table = $('#table').DataTable({
            ordering: false,
            paging: false,
            searching: false,
            info: false,
            select: {
                style: 'multi',
                selector: 'td:first-child',
            },
        });
       
        $('#table tbody').on( 'click', 'tr', function () {
            $(this).toggleClass('selected');

            var cb = $(this).find("> td:first-child > input:checkbox");
            
            //console.log($(this).hasClass('selected'));
            cb.prop('checked', $(this).hasClass('selected'));

            var qty = $(this).find("> td > input:number");
            qty.attr('readonly', false);
        } );
        */
        $('.price_selected').click(function(e) {
            console.log($(this).is(':checked'));
            var id = $(this).data('id');
            qty = $('.quantity[data-id=' + id + ']');
            if ($(this).is(':checked')) {
                $('.quantity[data-id=' + id + ']').attr('readonly', false);
            } else {
                $('.quantity[data-id=' + id + ']').attr('readonly', true);
                $('.quantity[data-id=' + id + ']').val(null).trigger('change');
            }
        })

        $('.quantity').change(function() {
            
            var id = $(this).data('id');
            
            var qty = $(this).val();
            if (qty == null) {
                qty = 0;
            }
            console.log(qty);
            $('.subtotal[data-id=' + id + ']').val(qty * $('.price[data-id=' + id + ']').val());

            var sum = 0;
            $('.subtotal').each(function(){
                sum += parseFloat($(this).val());  
            });
            console.log(sum);
            $('#subtotal_item').val(sum);
        })
    });

</script>
@endsection