@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<style type"text/css"> 
    .badge-notif-tab {
            position:relative;
    }
    .badge-notif-tab[data-badge]:after {
            content:attr(data-badge);
            position:absolute;
            top:-10px;
            right:-5px;
            font-size:.7em;
            background:#f3a709;
            color:white;
            width:18px;
            height:18px;
            text-align:center;
            line-height:18px;
            border-radius: 50%;
    }
</style>
@endsection

@section('content')
<div class="row">
    <!-- <a href="{{ route('requestuttp.create') }}" class="btn btn-w-md btn-primary" id="btn_create">Pendaftaran</a> -->
    <!-- <a href="{{ route('qrcode.show') }}" class="btn btn-w-md btn-primary" id="btn_print">Print PDF</a> -->

    <ul class="nav nav-tabs" role="tablist" id="tabs">
        <li role="presentation" class="active">
            <a href="#booking_luar" aria-controls="booking_luar" role="tab" data-toggle="tab"
            class="badge-notif-tab" 
                <?php if ((count($bookings_luar)) > 0 ){ ?> 
                    data-badge="{{count($bookings_luar)}}" 
                <?php }else{ } ?>
            >Booking DL</a>
        </li>
        <li role="presentation">
            <a href="#frontdesk_pendaftaran_luar" aria-controls="frontdesk_pendaftaran_luar" role="tab" data-toggle="tab"
            class="badge-notif-tab" 
                <?php if ((count($rows_pendaftaran_luar)) > 0 ){ ?> 
                    data-badge="{{count($rows_pendaftaran_luar)}}" 
                <?php }else{ } ?>
            >Pendaftaran DL</a>
        </li>

        <li role="presentation">
            <a href="#frontdesk_item_luar" aria-controls="frontdesk_item_luar" role="tab" data-toggle="tab"
            class="badge-notif-tab" 
                <?php if ((count($rows_item_luar)) > 0 ){ ?> 
                    data-badge="{{count($rows_item_luar)}}" 
                <?php }else{ } ?>
            >Item Pengujian DL</a>
        </li>
            
    </ul>
    <div class="tab-content" id="nav-tabContent">
        
        <div role="tabpanel" class="tab-pane active" id="booking_luar">
            <br/>
            
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    

                    <table id="table_data_booking_luar" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Booking</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Order</th>
                                <th>Nama Pemohon</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bookings_luar as $row)
                            <tr>
                                <td>{{ $row->booking_no }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->est_total_price,0,',','.') }}</td>
                                <td>{{ $row->Pic ? $row->Pic->full_name : '' }}</td>
                                <td>
                                    <form class="form_create_request">
                                    {!! Form::hidden('booking_id', $row->id) !!}
                                    {!! Form::button('Konfirmasi Booking', ['class' => 'btn btn-w-md btn-accent btn-simpan-luar', 'name' => 'submitbutton', 'value' => 'save']) !!}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div role="tabpanel" class="tab-pane" id="frontdesk_pendaftaran_luar">
            <br/>
            <a href="{{ route('requestuttp.createbooking') }}" class="btn btn-w-md btn-primary" id="btn_create">Konfirmasi Booking</a>

            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    

                    <table id="table_data_pendaftaran" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>Tanggal Pendaftaran</th>
                                <th>No Order</th>
                                <th>Nama Pemilik</th>
                                <th>Nominal Order</th>
                                <th>Nama Pemohon</th>
                                <th>Lokasi DL</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_pendaftaran_luar as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ date("d-m-Y",  strtotime($row->received_date)) }}</td>
                                <td>
                                @if($row->payment_code==null)
                                xx-xxxx-xx-xxx
                                @endif
                                @if($row->payment_code!=null)
                                {{ $row->no_order }}
                                @endif
                                </td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ number_format($row->total_price,0,',','.') }}</td>
                                <td>{{ $row->requestor ? $row->requestor->full_name : '' }}</td>
                                <td>{{ $row->lokasi_dl == 'luar' ? 'Luar Negeri' : 'Dalam Negeri' }}</td>
                                <td>{{ $row->status->status }}</td>
                                <td>
                                    <a href="{{ route('requestuttp.editbooking', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                    <!-- <a href="{{ route('request.destroy', $row->id) }}" class="btn btn-warning btn-sm">Hapus</a> -->
                                    <form id="formDelete">
                                    <!-- Form::hidden('hapus_data_id',$row->id) -->
                                    {!! Form::button('Hapus', ['class' => 'btn btn-warning btn-sm btn-hapus', 'id' => 'hapus','name' => 'hapus', 'value' => 'Hapus',
                                        'data-toggle' => "", 'data-target' => "#hapusModal" , 'data-id' => $row->id, 'data-noreg' => $row->no_register ]) !!}
                                    <!-- <a href="{{ route('requestuttp.destroy', ['id' => $row->id]) }}" class="btn btn-warning btn-sm hapus">Hapus</a> -->
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="frontdesk_item_luar">
            <div class="panel panel-filled table-area">
                <div class="panel-heading">
                    <table id="table_data_item_luar" class="table table-striped table-hover table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No Pendaftaran</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemohon</th>
                                <th>Lokasi DL</th>
                                <th>Lokasi, Provinsi/Negara</th>
                                <th>Waktu Pengujian</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows_item_luar as $row)
                            <tr>
                                <td>{{ $row->no_register }}</td>
                                <td>{{ $row->label_sertifikat }}</td>
                                <td>{{ $row->requestor->full_name }}</td>
                                <td>{{ $row->lokasi_dl == 'luar' ? 'Luar Negeri' : 'Dalam Negeri' }}</td>
                                <td>{{ $row->lokasi_dl == 'luar' ? $row->inspectionNegara->nama_negara : $row->inspectionProv->nama }}</td>
                                <td>{{ date("d-m-Y", strtotime($row->received_date)) }} s/d {{ date("d-m-Y", strtotime($row->estimated_date)) }}</td>
                                <td>
                                    <a target="_blank" href="{{ route('requestluaruttp.pdf', $row->id) }}" class="btn btn-warning btn-sm">Item Uji</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Serah Terima Alat ke Pemohon</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">
                            <input type="hidden" name="id" id="id"/>
                            <input type="hidden" name="requestid" id="requestid"/>
                            <input type="hidden" name="labid" id="labid"/>
                            <input type="hidden" name="instalasiid" id="instalasiid"/>

                            <div class="form-group">
                                <label>No Order</label>
                                <input type="text" name="order_no" id="order_no" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Alat</label>
                                <input type="text" name="alat" id="alat" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemilik</label>
                                <input type="text" name="pemilik" id="pemilik" class="form-control" readonly required />
                            </div>
                            <div class="form-group">
                                <label>Pemohon</label>
                                <input type="text" name="pemohon" id="pemohon" class="form-control" readonly required />
                            </div>

                            <div class="form-group">
                                <label>Nama Penerima Alat</label>
                                <input type="text" name="warehouse_out_nama" id="warehouse_out_nama" class="form-control"  required />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">SERAH TERIMA</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Ingin menghapus data ini ?</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal-hapus">
                            <div class="form-group">
                                <input class ="form-control" type="hidden" name="id" id="id"/>
                                <input class="form-control" type="hidden" name="requestid" id="requestid"/>
                                <input class= "form-control" type="hidden" name="labid" id="labid"/>
                                <input type="hidden" name="instalasiid" id="instalasiid"/>
                            </div>
                            <div class="form-group">
                                <label>No Pendaftaran</label>
                                <input type="text" name="no_registrasi" id="no_registrasi" class="form-control" readonly required />
                            </div>

                            
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="hapus" class="btn btn-accent btn-hapus-req">Hapus</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="invoiceDelete">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Yakin menghapus data ini ?</p>
        <input type="hidden" name="ReqInvid" id="ReqInvid"/>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary">Ya</button> -->
        {!! Form::button('Hapus', ['class' => 'btn btn-warning btn-sm btn-hapus-inv', 'id' => 'invHapus','name' => 'invHapus', 'value' => 'Hapus',
            'data-toggle' => "modal", 'data-target' => "#invoiceDelete"]) !!}
        <button type="button" class="btn btn-secondary" id="batalHapus" data-dismiss="modal">Tidak</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data,#table_data_booking_all,#table_data_booking_luar,#table_data_pendaftaran,#table_data_penagihan,#table_data_validasi,#table_data_kirim,#table_data_process,#table_data_done").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });
        $('#table_data tbody').on( 'click', 'button.btn-simpan', function (e) {
        //$('.btn-simpan').click(function(e){
            e.preventDefault();

            $(this).attr('disabled', true);
            toastr["warning"]("Konfirmasi booking sedang diproses","Form Valid");
  
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {

                $(this).attr('disabled', false);

                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon periksa kembali","Form Invalid");
                }
            });

        });

        $('#table_data_booking_luar tbody').on( 'click', 'button.btn-simpan-luar', function (e) {
        //$('.btn-simpan').click(function(e){
            e.preventDefault();

            $(this).attr('disabled', true);
            toastr["warning"]("Konfirmasi booking sedang diproses","Form Valid");
  
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {

                $(this).attr('disabled', false);

                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon periksa kembali","Form Invalid");
                }
            });

        });

        // $('.btn-hapus').click(function(e){
        //     $(this).hide();
        // });
        $(".btn-hapus").click (function(e){
            $(this).hide();
            <?php $id; ?>
            console.log('ini di klick');
            e.preventDefault();
            var iddelete = $(this).data().id;
            var noreg = $(this).data().noreg;
            $('#noregistrasi').val(noreg);
            console.log(iddelete);
            
            $("#deletemodal").modal().on('shown.bs.modal', function ()
            {
                $('#no_registrasi').val(noreg);

                var route = "{{ route('requestuttp.destroy', ':id') }}";
                route = route.replace(':id', iddelete);

                $('.btn-hapus-req').click(function(){
                    $.get(route,function(response)
                    {
                        
                        console.log(response.status)
                        console.log('{{ route('requestuut.destroy','id') }}');
                        if(response.status == true){
                            $("#deletemodal").modal().hide();
                            toastr["success"](response.messages,"Form Status : Ok");
                            location.reload();
                        }else{
                            toastr["error"](response.messages,"Form Invalid Status : "+response.status);
                        }
                    });
                });
                
            });
            
        });

        $('.btn-hapus-penagihan').click(function(e){
            console.log($(this).data("id"));
        // $('#table_data_pendaftaran tbody').on( 'click', 'button.btn-hapus', function (e) {
            e.preventDefault();
            var s = $("#invoiceDelete").show();
            var idHapus = $(this).data("id")    
            $(this).attr('disabled', true);
            var form = $(this).parents('form:first');

            var form_data = form.serialize();
            form_data += 'id=' +$(this).data("id") + '&_token={{ csrf_token() }}';

            // var reqId = form_data.id;
            // const form = form_data;
            // const data = new URLSearchParams(new FormData(form).entries());
                
            // var idHapus = document.getElementsByName("hapus_data_id");

            $("#Reqid").val(idHapus);
        });

        $("#hapusMod").click(function(e){
            var id = $("#Reqid").val();
            e.preventDefault();

            $("#hapusModal").hide();
            $(this).attr('disabled', true);

            var form = $(this).parents('form:first');

            var form_hapus = form.serialize();
            form_hapus += 'id=' +$(this).data("id") + '&_token={{ csrf_token() }}';

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax(
                {
                    type: "GET",
                    url: 'requestuttp/destroy/' + id,
                    data: form_hapus,
                    dataType: 'json',
                    success: function (response) {
                        if(response.status == true){
                            toastr["success"](response.messages,"Form Valid");
                            location.reload();
                        }else{
                            toastr["error"](response.messages,"Invalid");
                            location.reload();
                        }
                    },
                    error: function(xhr) {
                    toastr["error"]("Data gagal dihapus","Form Invalid");
                }
                });
            console.log(id)
        });

        $("#invHapus").click(function(e){
            var id = $("#Reqid").val();
            e.preventDefault();

            $("#invoiceDelete").hide();
            $(this).attr('disabled', true);

            var form = $(this).parents('form:first');

            var form_hapus = form.serialize();
            form_hapus += 'id=' +$(this).data("id") + '&_token={{ csrf_token() }}';

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax(
                {
                    type: "GET",
                    url: 'requestuttp/destroy/' + id,
                    data: form_hapus,
                    dataType: 'json',
                    success: function (response) {
                        if(response.status == true){
                            toastr["success"](response.messages,"Form Valid");
                            location.reload();
                        }else{
                            toastr["error"](response.messages,"Invalid");
                            location.reload();
                        }
                    },
                    error: function(xhr) {
                    toastr["error"]("Data gagal dihapus","Form Invalid");
                }
                });
            console.log(id)
        });

        //$('.btn-cetak-tag').click(function(e){
        $('#table_data_kirim tbody').on( 'click', 'button.btn-cetak-tag', function (e) {
            e.preventDefault();

            var url = '{{ route("requestuttp.tag", ":id") }}';
            url = url.replace(':id', $(this).data("id"));

            window.open(url, '_blank');
            window.focus();
            //location.reload();
        });

        //$('.btn-kirim-alat').click(function(e){
        $('#table_data_kirim tbody').on( 'click', 'button.btn-kirim-alat', function (e) {
            e.preventDefault();
            
            var url = '{{ route("requestuttp.instalasi") }}';
            //url = url.replace(':id', $(this).data("id"));

            //location.reload();

            var form_data = 'id=' +$(this).data("id") + '&_token={{ csrf_token() }}';

            $.post(url,form_data,function(response)
            {
                if(response.status===true)
                {
                    //location.reload();
                    toastr["success"]("Berhasil dikirim. Silakan refresh jika diperlukan.","Form Valid");
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksa kembali","Form Invalid");
                }
            });
        });

        $("#no_order").change(function(e) {        
            e.preventDefault();
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.instalasiqr') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    //location.reload();
                    toastr["success"]("Berhasil dikirim","Form Valid");
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksa kembali","Form Invalid");
                }
            });
        });

        $('#refresh_btn').click(function(e) {
            e.preventDefault();
            location.reload();
        });

        // TABS
        $('#tabs a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });

        var hash = window.location.hash;
        $('#tabs a[href="' + hash + '"]').tab('show');
        // END TABS
    });
</script>
@endsection