@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')

<div class="row">
    {!! Form::open(array('id' => 'form_confirm_payment')) !!}
        <div class="panel panel-filled" id="panel_create">
            <div class="loader">
                <div class="loader-bar"></div>
            </div>      
            <div class="panel-heading" >
                <h4>Validasi Pembayaran</h4>
            </div>
            <div class="panel-body">
                <!--
                <div class="form-group">
                    <label for="payment_date">Tanggal Pembayaran</label> 
                    <input id="payment_date" name="payment_date" class="form-control" type="text" autocomplete="off" placeholder="Tanggal Pembayaran" required value="<?php echo date("Y-m-d")?>">
                </div>
                -->
                <div class="form-group">
                    <label for="no_register">No Register</label> 
                    <input id="no_register" name="no_register" class="form-control" type="text" 
                        value="{{ $request->no_register }}"
                        placeholder="No Register" autocomplete="off" readonly>
                </div>
                @if ($request->total_price > 0)
                <div class="form-group">
                    <label for="billing_code">Kode Billing</label> 
                    <input id="billing_code" name="billing_code" class="form-control" type="text" placeholder="Kode Billing" autocomplete="off" 
                        value="{{ $request->billing_code }}" readonly>
                </div>
                <div class="form-group">
                    <label for="payment_date">Tanggal Pembayaran</label> 
                    <input id="payment_date" name="payment_date" class="form-control" type="text" autocomplete="off" placeholder="Tanggal Pembayaran" readonly 
                        value="{{ date('d-m-Y', strtotime($request->payment_date)) }}">
                </div>
                <div class="form-group">
                    <label for="payment_code">No. Pembayaran</label> 
                    <input id="payment_code" name="payment_code" class="form-control" type="text" autocomplete="off" readonly
                        value="{{ $request->payment_code }}">
                </div>
                @else
                @if ($request->lokasi_pengujian == 'dalam')
                <p>Tidak ada pembayaran dikarenakan tidak terdapat tagihan</p>
                @endif
                @endif
                @if ($request->lokasi_pengujian == 'luar')
                <div class="form-group">
                    <label for="spuh_no">No SPUH</label> 
                    <input id="spuh_no" name="spuh_no" class="form-control" type="text" placeholder="No SPUH" autocomplete="off" 
                        value="{{ $request->spuh_no }}" readonly>
                </div>
                <div class="form-group">
                    <label for="spuh_payment_date">Tanggal Pembayaran</label> 
                    <input id="spuh_payment_date" name="spuh_payment_date" class="form-control" type="text" autocomplete="off" placeholder="Tanggal Pembayaran" readonly 
                        value="{{ date('d-m-Y', strtotime($request->spuh_payment_date)) }}">
                </div>
                @endif
                @if ($request->file_payment_ref != null)
                <a href="<?= config('app.siks_url') ?>/tracking/download_paymentref/payment_ref/{{ $request->id }}" class="btn btn-w-md btn-default">Dokumen Bukti Pembayaran PNBP</a>
                @endif
                @if ($request->file_spuh_payment_ref != null)
                <a href="<?= config('app.siks_url') ?>/tracking/download_paymentref/spuh_payment_ref/{{ $request->id }}" class="btn btn-w-md btn-default">Dokumen Bukti Pembayaran SPUH</a>
                @endif
            </div>
        </div>
        <div class="text-right">
            {!! Form::submit('Valid', ['class' => 'btn btn-w-md btn-success', 'id'=>'btn_simpan', 'name' => 'submitbutton', 'value' => 'save']) !!}
            {!! Form::submit('Tidak Valid', ['class' => 'btn btn-w-md btn-danger', 'id'=>'btn_simpan_no', 'name' => 'submitbutton', 'value' => 'save']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){

    /*
    $('input#payment_date').datepicker({
      format:"yyyy-mm-dd",
      autoclose:true
    });
    */

    $("#btn_simpan").click(function(e){
        e.preventDefault();

        $('#btn_simpan').attr('disabled', true);
        $('#btn_simpan_no').attr('disabled', true);

        $("#panel_create").toggleClass("ld-loading");

        var form_data = $("#form_confirm_payment").serialize();
        form_data += "&id={{ $id }}";
        $.post('{{route('requestuttp.validsave')}}',form_data,function(res)
        {
            $('#btn_simpan').attr('disabled', false);
            $('#btn_simpan_no').attr('disabled', false);

            if(res[0]==false)
            {
                var msg = show_notice(res[1]);
                //$("#panel_create").toggleClass("ld-loading");
                toastr["error"](res[1],"Form Invalid");
            }
            else
            {
                window.location = '{{ route('requestuttp') }}';
            }
        },"json");
    });
    $("#btn_simpan_no").click(function(e){
        e.preventDefault();
        $('#btn_simpan').attr('disabled', true);
        $('#btn_simpan_no').attr('disabled', true);

        $("#panel_create").toggleClass("ld-loading");

        var form_data = $("#form_confirm_payment").serialize();
        form_data += "&id={{ $id }}";
        $.post('{{route('requestuttp.novalidsave')}}',form_data,function(res)
        {
            $('#btn_simpan').attr('disabled', false);
            $('#btn_simpan_no').attr('disabled', false);

            if(res[0]==false)
            {
                var msg = show_notice(res[1]);
                console.log(msg);
                //$("#panel_create").toggleClass("ld-loading");
                toastr["error"](res[1],"Form Invalid");
            }
            else
            {
                window.location = '{{ route('requestuttp') }}';
            }
        },"json");
    });
});

function show_notice(msg)
{
    var errmessage = {
        'payment_date':'Tanggal input wajib diisi',
        'payment_code':'Nomor pembayarn(struk bayar) wajib diisi'
    };

    $(document).find("small.wajib-isi").remove();
            var str = '<div class="panel panel-filled panel-c-danger">'+
        '<div class="panel-body">'+
            '<b>Terjadi kesalahan.</b>'+
            'Telah terjadi kesalahan, hubungi administrator untuk info lebih lanjut.'+
            '<ul>';
            $.each(msg,function(i,v){
                str += '<li>'+errmessage[i]+'</li>';
                $(document).find("label[for='"+i+"']").append("<small class='text-warning wajib-isi'> wajib diisi</small>");
            });
    str += '</ul>'+
        '</div>'+
    '</div>';

    return str;
}
</script>
@endsection