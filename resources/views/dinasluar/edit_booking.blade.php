@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        
    </div>
    <div class="panel-body">
        <form id="form_create_request">
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Booking</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="booking_id">No Booking</label>
                            {!! Form::text('booking_no', $booking->booking_no, ['class' => 'form-control','id' => 'booking_no', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="uttp_owner_id">Nama Pemilik</label>
                            {!! Form::hidden('uttp_owner_id', $request->uttp_owner_id, ['id' => 'uttp_owner_id']) !!}
                            {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'label_sertifikat', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Pemilik</label>
                            {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>  
                </div>
            </div>
        </div>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pemohon</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Nama Pemohon</label>
                            {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Jenis Tanda Pengenal</label>
                            {!! Form::text('id_type_id', $requestor->kantor == "Perusahaan" ? "NIB" : "NPWP", ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                            {!! Form::text('pic_id_no', $requestor->kantor == "Perusahaan" ? $requestor->nib : $requestor->npwp, ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_phone_no">Nomor Telepon</label>
                            {!! Form::text('pic_phone_no', $requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_email">Alamat Email</label>
                            {!! Form::text('pic_email', $requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @if ($request->lokasi_pengujian == 'dalam')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="received_date">Tanggal Masuk Alat</label>
                            {!! Form::text('received_date', date("d-m-Y", strtotime(isset($request->received_date) ? $request->received_date : date("Y-m-d"))), ['class' => 'date form-control','id' => 'receipt_date', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="estimate_date">Tanggal Perkiraan Selesai</label>
                            {!! Form::text('estimated_date', date("d-m-Y", strtotime(isset($request->estimated_date) ? $request->estimated_date : date("Y-m-d"))), ['class' => 'date form-control','id' => 'estimated_date', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="no_surat_permohonan">No Surat Permohonan</label>
                            {!! Form::text('no_surat_permohonan', $request->no_surat_permohonan, ['class' => 'form-control','id' => 'no_surat_permohonan', 'placeholder' => 'No Surat Permohonan', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tgl_surat_permohonan">Tanggal Surat Permohonan</label>
                            {!! Form::text('tgl_surat_permohonan', date("d-m-Y", strtotime(isset($request->tgl_surat_permohonan) ? $request->tgl_surat_permohonan : date("Y-m-d"))), ['class' => 'form-control','id' => 'tgl_surat_permohonan', 'placeholder' => 'Tanggal Surat Permohonan', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="file_surat_permohonan">Surat Permohonan</label>
                            <div class="input-group">
                                <a href="<?= config('app.siks_url') ?>/tracking/download_permohonan/{{ $request->id }}" class="btn btn-warning btn-sm">DOWNLOAD</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_from">Usulan Jadwal Mulai</label>
                            {!! Form::text('scheduled_test_date_from', date("d-m-Y", strtotime(isset($request->scheduled_test_date_from) ? $request->scheduled_test_date_from : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_from', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date_to">Usulan Jadwal Selesai</label>
                            {!! Form::text('scheduled_test_date_to', date("d-m-Y", strtotime(isset($request->scheduled_test_date_to) ? $request->scheduled_test_date_to : date("Y-m-d"))), ['class' => 'form-control','id' => 'scheduled_test_date_to', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lokasi_pengujian">Lokasi Pengujian</label>
                            {!! Form::text('jenis_layanan', $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor', ['class' => 'form-control','id' => 'lokasi_pengujian_view', 'readonly']) !!}
                            <!--
                            <select class="form-control" name="lokasi_pengujian" id="lokasi_pengujian">
                                <option value="dalam" <?= $request->lokasi_pengujian == 'dalam' ? 'selected' : '' ?> >Dalam Kantor</option>
                                <option value="luar" <?= $request->lokasi_pengujian == 'luar' ? 'selected' : '' ?>>Luar Kantor</option>  
                            </select>
                            -->
                        </div>
                        
                    </div>
                </div>
                @if ($request->lokasi_pengujian == 'dalam')
                @else
                <!--
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inspection_loc">Alamat Lokasi Pengujian</label>
                            {!! Form::text('inspection_loc', $request->inspection_loc, ['class' => 'form-control','id' => 'inspection_loc', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inspection_prov_id">Alamat Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                            {!! Form::text('inspection_prov_id', ($request->inspectionKabkot ? $request->inspectionKabkot->nama : '') . ', ' . ($request->inspectionProv ? $request->inspectionProv->nama : ''), ['class' => 'form-control','id' => 'inspection_prov_id', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                -->
                @endif
            </div>
        </div>


        <!-- Tes-->

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Item Pengujian</h4>
            </div>

            <div class="panel-body">
                <table class="table table-striped table-hover table-responsive-sm" id="t-roll">
                <thead style="font-weight:bold; color:white;">
                    <tr>
                        <th>No</th>
                        <th>Jenis Alat Ukur</th>
                        <th>Merek</th>
                        <th>Model/Tipe</th>
                        <th>No.Seri</th>
                        <th>Media Uji</th>
                        <th>Max.Capacity</th>
                        <th>Min.Capacity</th>
                        <th>Satuan</th>
                        <th>Buatan</th>
                        <th>Pabrikan</th>
                        <th>Alamat Pabrik</th>
                        @if ($request->lokasi_pengujian == 'luar')
                        <th style="width: 200px;">Alamat Lokasi Pengujian</th>
                        @else 
                        @if ($request->service_type_id == 4 || $request->service_type_id == 5)
                        <th style="width: 200px;">Lokasi Alat</th>
                        @endif
                        @endif
                        <th>Kelengkapan Persyaratan (Break Down) </th>
                        @if ($request->lokasi_pengujian == 'dalam')
                        <th style="width: 200px;">Item Pengujian</th>
                        <th>Total Biaya</th>
                        <th>Confirmed ?</th>
                        <th></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    <?php $num=0 ?>
                    @foreach($request->items as $item)
                        <?php $num +=1 ?>
                        <tr>
                            <form id="form_item_{{ $item->id }}">
                                {!! Form::hidden('id', $item->id) !!}
                                {!! Form::hidden('uttp_id', $item->uttp_id) !!}
                                {!! Form::hidden('uttp_type_id', $item->uttp->type_id) !!}
                                <td>{{ $num}}</td>
                                <td>{{ $item->uttp->type->uttp_type}}</td>
                                <td>{{ $item->uttp->tool_brand}}</td>
                                <td>{{ $item->uttp->tool_model}}</td>
                                <td>{{ $item->uttp->serial_no}}</td>
                                <td>{{ $item->uttp->tool_media}}</td>
                                <td>{{ $item->uttp->tool_capacity}}
                                <td>{{ $item->uttp->tool_capacity_min}}</td>
                                <td>{{ $item->uttp->tool_capacity_unit }}</td>
                                <td>{{ $item->uttp->tool_made_in }}</td>
                                <td>{{ $item->uttp->tool_factory }}</td>
                                <td>{{ $item->uttp->tool_factory_address }}</td>
                                @if ($request->lokasi_pengujian == 'luar')
                                <td>
                                    {{ $item->location }}, {{ ($item->kabkot ? $item->kabkot->nama : '') . ', ' . ($item->provinsi ? $item->provinsi->nama : '') }}
                                    <br/>
                                    ({{ $item->location_lat }}, {{ $item->location_long }})
                                </td>
                                @else 
                                @if ($request->service_type_id == 4 || $request->service_type_id == 5)
                                <td>{{ $item->location }}</td>
                                @endif
                                @endif
                                <td> 
                                    @if ($request->service_type_id == 4)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/type_approval_certificate/{{ $item->id }}"
                                        class="btn btn-default btn-sm faa-parent animated-hover" >
                                        <i class="fa fa-download faa-flash"></i> Surat Persetujuan Tipe
                                    </a>
                                    @elseif ($request->service_type_id == 5)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/last_certificate/{{ $item->id }}"
                                        class="btn btn-default btn-sm faa-parent animated-hover" >
                                        <i class="fa fa-download faa-flash"></i> Sertifikat Sebelumnya
                                    </a>
                                    @elseif ($request->service_type_id == 6 || $request->service_type_id == 7)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/application_letter/{{ $item->id }}"
                                        class="btn btn-default btn-sm faa-parent animated-hover" >
                                        <i class="fa fa-download faa-flash"></i> Surat Permohonan
                                    </a>
                                    @if ($request->path_calibration_manual != null)
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/calibration_manual/{{ $item->id }}"
                                        class="btn btn-default btn-sm faa-parent animated-hover" >
                                        <i class="fa fa-download faa-flash"></i> Manual Kalibrasi
                                    </a>
                                    @endif
                                    <a href="<?= config('app.siks_url') ?>/tracking/download/manual_book/{{ $item->id }}"
                                        class="btn btn-default btn-sm faa-parent animated-hover" >
                                        <i class="fa fa-download faa-flash"></i> Buku Manual
                                    </a>
                                    @endif
                                </td>
                                @if ($request->lokasi_pengujian == 'dalam')
                                <td>
                                    @foreach($item->inspections as $inspection)
                                        {{ $inspection->inspectionPrice->inspection_type }}
                                    @endforeach
                                </td>
                                <td>{{ number_format($item->subtotal, 2, ',', '.') }}</td>
                                <td>{{ $item->status_id === 1 ? 'No' : 'Yes'}}</td>
                                <td> <a href="{{ route('requestuttp.createbookinginspection', $item->id) }}"
                                class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_add_inspeksi">
                                <i class="fa fa-edit faa-flash"></i> Item Uji</a> </td>
                                @endif
                            </form>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        
        </div>

        @if($request->lokasi_pengujian == 'luar')
        <div class="panel panel-filled">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="is_complete">Kelengkapan</label>
                            {!! Form::select('is_complete', ['ya'=>'Lengkap','tidak'=>'Tidak Lengkap'], null, 
                                 ['class' => 'form-control select2','id' => 'is_complete', 'style' => 'width:100%']) !!}
                        </div>
                    </div>
                    <div class="col-md-6" id="tidak_lengkap">
                        <div class="form-group">
                            <label for="not_complete_notes">Keterangan Tidak Lengkap</label>
                            {!! Form::textarea('not_complete_notes', $request->not_complete_notes, ['class' => 'form-control','id' => 'not_complete_notes', 'readonly']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        </form>

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button> 
        <button type="button" class="btn btn-w-md btn-accent" id="btn_submit">Simpan dan Konfirmasi</button> 
        
    </div>
</div>
</div>

<div class="modal fade" tabindex="-1" id="historyModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="m-t-none">Riwayat Pengujian/Pemeriksaan</h5>
                <br/>
                <table id="inspection_history" class="table table-responsive-sm input-table">
                    <thead>
                        <tr>
                            <th>No Register</th>
                            <th>No Order</th>
                            <th>No SKHP</th>
                            <th>Item Pengujian</th>
                            <th>Tanggal, Mulai</th>
                            <th>Tanggal, Selesai</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        $('#t-roll').DataTable({
            "scrollX": true
        });
        $('#booking_id, #lokasi_pengujian, .negara').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $('#is_complete').select2({
            //placeholder: "- Pilih UML -",
            allowClear: true
        }).on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data.id);
            
            if (data.id == 'tidak') {
                $("#not_complete_notes").prop("readonly", false); 
            } else {
                $("#not_complete_notes").val("");
                $("#not_complete_notes").prop("readonly", true); 
            }
        });
        /*
        $('#uttp_owner_id').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true,
            ajax: {
                url: '{{ route('requestuttp.getowners') }}',
                data: function (params) {
                    var query = {
                        q: params.term,
                    }
                    
                    return query;
                },
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.nama };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            var route = "{{ route('requestuttp.getowner', ':id') }}";
            route = route.replace(':id', e.params.data.id);
            $.get(route,function(res)
            {
                $('#addr_sertifikat').val(res.alamat);
            });
        });
        */

        $('.satuan').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true,
            tags: true,
            ajax: {
                url: function (params) {
                    var form = $(this).closest('form');
                    var type = form.find('input:hidden[name=uttp_type_id]').val();
                    var route = "{{ route('requestuttp.getunits', ':id') }}";
                    route = route.replace(':id', type);
                    return route;
                },
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.unit, text: obj.unit };
                        })
                    };
                },
            }
        });

        /*
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
        */

        $('#btn_simpan').click(function(e){
            e.preventDefault();
            
            $('#btn_submit').attr('disabled', true);
            $('#btn_simpan').attr('disabled', true);

            toastr["warning"]("Sedang menyimpan. Harap tunggu.");

            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuttp.simpaneditbooking', $request->id) }}',form_data,function(response)
            {

                $('#btn_submit').attr('disabled', false);
                $('#btn_simpan').attr('disabled', false);

                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}';
                }
                else
                {
                    //var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksa kembali","Form Invalid");
                }
            });

        });

        $('#btn_submit').click(function(e){
            e.preventDefault();

            $('#btn_submit').attr('disabled', true);
            $('#btn_simpan').attr('disabled', true);

            toastr["warning"]("Sedang menyimpan. Harap tunggu.");
            
            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('requestuttp.submitbooking', $request->id) }}',form_data,function(response)
            {
                $('#btn_submit').attr('disabled', false);
                $('#btn_simpan').attr('disabled', false);

                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}';
                }
                else
                {
                    //var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksa kembali. Pilih item uji.","Form Invalid");
                }
            });

            
        });

        $('.btn-save-edit').click(function(e){
            e.preventDefault();
            var id = $(this).data().id;
            
            var form_data = $("#form_item_" + id).serialize();

            form_data += '&_token={{ csrf_token() }}';

            var postroute = "{{ route('requestuttp.edititem', ':id') }}";
            postroute = postroute.replace(':id', id);
            
            $.post(postroute,form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = "{{ route('requestuttp.editbooking', $request->id) }}";
                }
                else
                {
                    //var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

        $('.btn-history').click(function(e){
            e.preventDefault();
            var id = $(this).data().id;
            
            var route = "{{ route('requestuttp.gethistory', ':id') }}";
            route = route.replace(':id', id);
            //$.get(route,function(res)
            //{
                
            //});
            $("#inspection_history").DataTable().destroy();
            $("#inspection_history").DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: route,
                    type: 'GET'
                },
                "ajax": function(data, callback, settings) {
                    
                    $.ajax({
                        url: route,
                        type: "GET",
                        success: function(res) {
                            console.log(res); 
                            callback({
                                recordsTotal: res.length,
                                recordsFiltered: res.length,
                                data: res
                            });
                        }
                    });

                },
                "columns": [
                    { data: 'no_register' },
                    { data: 'no_order' },
                    { data: 'no_sertifikat' },
                    { data: 'inspections' },
                    { data: 'staff_entry_datein' },
                    { data: 'staff_entry_dateout' },
                ],
            });

            $('#historyModal').modal('show');

        });
    });

</script>
@endsection