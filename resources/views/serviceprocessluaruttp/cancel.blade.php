@extends('layouts.app')
<style type="text/css">
</style>

@section('content')
<div class="row">
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="label_sertifikat">Nama Pemilik</label>
                    {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="addr_sertifikat">Alamat Pemilik</label>
                    {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                </div>
            </div>  
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pic_name">Nama Pemohon</label>
                    {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pic_name">Jenis Tanda Pengenal</label>
                    {!! Form::text('id_type_id', $requestor->kantor == "Perusahaan" ? "NIB" : "NPWP", ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                    {!! Form::text('pic_id_no', $requestor->kantor == "Perusahaan" ? $requestor->nib : $requestor->npwp, ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pic_phone_no">Nomor Telepon</label>
                    {!! Form::text('pic_phone_no', $requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pic_email">Alamat Email</label>
                    {!! Form::text('pic_email', $requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="jenis_layanan">Jenis Layanan</label>
                    {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="lokasi_pengujian">Lokasi Pengujian</label>
                    {!! Form::text('lokasi_pengujian', $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' , ['class' => 'form-control','id' => 'lokasi_pengujian', 'readonly']) !!}
                    
                </div>
                
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="inspection_prov_id">Alamat Lokasi Pengujian, Kabupaten/Kota, Provinsi</label>
                    {!! Form::text('inspection_prov_id', 
                        $request->inspectionKabkot->nama .', '. $request->inspectionProv->nama,
                        ['class' => 'form-control','id' => 'inspection_prov_id', 'readonly']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            
            <div class="col-md-6">
                <div class="form-group">
                    <label for="scheduled_test_date_from">Jadwal Waktu Pengujian, Mulai</label>
                    {!! Form::text('scheduled_test_date_from', 
                        date("d-m-Y", strtotime(isset($doc->date_from) ? $doc->date_from : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'scheduled_test_date_from', 'readonly']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="scheduled_test_date_to">Jadwal Waktu Pengujian, Selesai</label>
                    {!! Form::text('scheduled_test_date_to', 
                        date("d-m-Y", strtotime(isset($doc->date_to) ? $doc->date_to : date("Y-m-d"))),
                        ['class' => 'form-control','id' => 'scheduled_test_date_to', 'readonly']) !!}
                </div>
            </div>
        </div>

        {!! Form::open(['url' => route('serviceprocessluaruttp.confirmcancel', $request->id)])  !!}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="pending_notes">Alasan Pembatalan Pemeriksaan/Pengujian</label>
                    {!! Form::text('cancel_notes', 
                        '',
                        ['class' => 'form-control','id' => 'cancel_notes']) !!}
                </div>
            </div>
        </div>
        <button role="submit" class="btn btn-w-md btn-danger" id="btn_simpan">Batal Uji</button>
        {!! Form::close() !!}
        
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function ()
    {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
    });

</script>
@endsection