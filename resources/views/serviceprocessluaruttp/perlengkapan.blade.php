@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>      
        <div class="panel-heading">
            <div class="panel-body">
            
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Booking</h4>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="label_sertifikat">Nama Pemilik</label>
                                    {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="addr_sertifikat">Alamat Pemilik</label>
                                    {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                                </div>
                            </div>  
                        </div>

                    </div>
                </div>

                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Alat</h4>
                    </div>

                    <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="jenis_uttp">Jenis UTTP</label>
                                {!! Form::text('jenis_uttp', 
                                    $item->uttp->type->uttp_type, ['class' => 'form-control','id' => 'jenis_uttp', 'readonly']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kapasitas">Merek/Model/Tipe/No Seri</label>
                                {!! Form::text('tool_brand', 
                                    $item->uttp->tool_brand . '/' . $item->uttp->tool_model . '/' . $item->uttp->serial_no, 
                                    ['class' => 'form-control','id' => 'tool_brand', 'readonly' ]) !!} 
                            </div>
                        </div>
                    </div>
                </div>

                
                <div class="panel panel-filled">
                    <div class="panel-heading" >
                        <h4>Informasi Perlengkapan</h4>
                    </div>

                    <div class="panel-body">

                        <div class="row"><div class="col-md-6">
                            <a href="{{ route('serviceprocessluaruttp.newperlengkapan', $item->id) }}"
                                class="btn btn-warning btn-sm" id="btn_add_item">
                                <i class="fa fa-add"></i> Tambah Perlengkapan</a><br/><br/>
                        </div></div>
                        <div class="row"><div class="col-md-12">
                            <table class="table table-striped table-hover table-responsive-sm" id="t-roll">
                                <thead style="font-weight:bold; color:white;">
                                    <tr>
                                        <th>No</th>
                                        <th>Jenis Alat Ukur</th>
                                        <th>Merek</th>
                                        <th>Model/Tipe</th>
                                        <th>No.Seri</th>
                                        <th>Media Uji</th>
                                        <th>Max.Capacity</th>
                                        <th>Min.Capacity</th>
                                        <th>Satuan</th>
                                        <th>Buatan</th>
                                        <th>Pabrikan</th>
                                        <th>Alamat Pabrik</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $num=0 ?>
                                    @foreach($perlengkapans as $row)
                                        <?php $num +=1 ?>
                                        <tr>
                                            <form id="form_item_{{ $item->id }}">
                                                {!! Form::hidden('id', $row->id) !!}
                                                {!! Form::hidden('uttp_id', $row->uttp_id) !!}
                                                {!! Form::hidden('uttp_type_id', $row->uttp->type_id) !!}
                                                <td>{{ $num}}</td>
                                                <td>{{ $row->uttp->type->uttp_type}}</td>
                                                <td>{{ $row->uttp->tool_brand}}</td>
                                                <td>{{ $row->uttp->tool_model}}</td>
                                                <td>{{ $row->uttp->serial_no}}</td>
                                                <td>{{ $row->uttp->tool_media}}</td>
                                                <td>{{ $row->uttp->tool_capacity}}
                                                <td>{{ $row->uttp->tool_capacity_min}}</td>
                                                <td>{{ $row->uttp->tool_capacity_unit }}</td>
                                                <td>{{ $row->uttp->tool_made_in }}</td>
                                                <td>{{ $row->uttp->tool_factory }}</td>
                                                <td>{{ $row->uttp->tool_factory_address }}</td>
                                                <td> <a href="{{ route('serviceprocessluaruttp.editperlengkapan', ['id'=>$request->id, 'idItem'=>$item->id, 'idPerlengkapan'=>$row->id]) }}"
                                                    class="btn btn-warning btn-sm faa-parent animated-hover pull-right" id="btn_add_inspeksi">
                                                    <i class="fa fa-edit faa-flash"></i> Edit Perlengkapan</a> 
                                                    
                                                    <button id="hapus" type="button" class="btn btn-warning btn-sm faa-parent animated-hover pull-right btn-hapus" data-id="{{ $row->id }}">
                                                    <i class="fa fa-edit faa-flash"></i> Hapus Perlengkapan</a> 
                                                    </button>
                                                </td>
                                            </form>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div></div>

                    </div>
            
                 </div>

                
                <a href="{{ route('serviceprocessluaruttp.check', $request->id) }}" class="btn btn-w-md btn-accent">Kembali</a>


            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="hapusModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {!! Form::open(['url' => route('serviceprocessluaruttp.hapusperlengkapan'), 'id' => 'form_delete'])  !!}
        <div class="modal-content">
            <div class="modal-body">
                <p>Data perlengkapan benar akan dihapus?</p>
                <input type="hidden" name="id" id="perlengkapan_id" />
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Hapus</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection


@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendor/jquery-validation/additional-methods.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan, #data_table_check").DataTable();

        $('#t-roll').DataTable({
            "scrollX": true
        });

        $('.is_accepted').select2({
            //placeholder: "- Pilih UML -",
            allowClear: true
        }).on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data.id);
            
            if (data.id == 'tidak') {
                $("#keterangan_tidak_lengkap").prop("readonly", false); 
            } else {
                $("#keterangan_tidak_lengkap").val("");
                $("#keterangan_tidak_lengkap").prop("readonly", true); 
            }
        });
        
        $("#btn_simpan").click(function(e){
            e.preventDefault();

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $("#btn_simpan_submit").click(function(e){
            e.preventDefault();

            $("#submit_val").val(1);

            var form = $("#form_result");
            if (form.valid()) {
                toastr["warning"]("Sedang menyimpan. Harap tunggu.");
                form[0].submit();
            }
        });

        $('#t-roll tbody').on('click', 'button.btn-hapus', function (e) {
            console.log($(this).data("id"));
            e.preventDefault();
            $("#hapusModal").modal('show');
            $("#perlengkapan_id").val($(this).data("id"));
        });
    });
</script>
@endsection