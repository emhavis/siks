@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>

@endsection

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        {!! Form::open(['url' => route('serviceprocessluaruttp.simpanitem', ['id' => $request->id, 'uttp_type_id'=>$type->id]), 'id' => 'form_result'])  !!}
        
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Alat Ukur</h4>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="label_sertifikat">Nama Pemilik</label>
                            {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Pemilik</label>
                            {!! Form::textarea('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>  
                </div>

                <div class="alert alert-danger" role="alert">
                    <h5 class="alert-heading">Mohon Diperhatikan!</h5>
                    <p>
                        <ol type="1">
                            <li>Saat melengkapi <strong>spesifikasi alat</strong>, harap diperiksa dan dipastikan tidak salah/tertukar:
                                <ol type="a">
                                    <li><strong>Meter Arus vs Sistem Meter Arus;</strong></li>
                                    <li><strong>Meter Gas vs Sistem Meter Gas;</strong></li>
                                    <li><strong>CTMS BBM vs CTMS LNG/LPG;</strong></li>
                                </ol>
                            </li>
                            <li>Meter Arus atau Meter Gas yang diuji bersamaan dengan perlengkapannya (di lokasi terpasang), didaftarkan sebagai <strong>Sistem Meter Arus/Sistem Meter Gas</strong>.</li>
                            <li>Meter Arus atau Meter Gas yang diuji mandiri (di lab atau instalasi), didaftarkan sebagai <strong>Meter Arus/Meter Gas</strong>.</li>
                        </ol>
                    </p>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_uttp">Jenis UTTP</label>
                            {!! Form::text('jenis_uttp', $type->uttp_type, ['class' => 'form-control','id' => 'jenis_uttp', 'readonly']) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis">Jenis Kebutuhan Sertifikat</label>
                            {!! Form::select('jenis', ['sertifikat'=>'Perlu Sertifikat','perlengkapan'=>'Sebagai Perlengkapan (Halaman 2)'], 
                                                null, 
                                                ['class' => 'form-control','id' => 'jenis', 'style' => 'width:100%']) !!}
                                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" id="item_id_grp">
                            <label for="item_id">Perlengkapan pada UTTP</label>
                            {!! Form::select('item_id', $items, 
                                                null, 
                                                ['class' => 'form-control','id' => 'item_id', 'style' => 'width:100%']) !!}
                                           
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Merek</label>
                            {!! Form::text('tool_brand', '', ['class' => 'form-control','id' => 'tool_brand' ]) !!} 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Model/Tipe</label>
                            {!! Form::text('tool_model', '', ['class' => 'form-control','id' => 'tool_model']) !!} 
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No Seri</label>
                            {!! Form::text('tool_serial_no', '', ['class' => 'form-control','id' => 'tool_serial_no']) !!} 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Media Uji/Komoditas</label>
                            {!! Form::text('tool_media', '', ['class' => 'form-control','id' => 'tool_media']) !!} 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Kapasitas Maksimum</label>
                            {!! Form::text('tool_capacity', '', ['class' => 'form-control','id' => 'tool_capacity' ]) !!} 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kapasitas">Kapasitas Minimum</label>
                            {!! Form::text('tool_capacity_min', '', ['class' => 'form-control','id' => 'tool_capacity_min']) !!} 
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Satuan Kapasitas</label>
                            {!! Form::select('tool_capacity_unit', $units, null, ['class' => 'form-control','id' => 'tool_capacity_unit']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tool_made_in_id">Buatan</label>
                            {!! Form::select('tool_made_in_id', $negara, null, ['class' => 'form-control','id' => 'tool_made_in_id']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="factory">Nama Pabrikan</label>
                            {!! Form::text('tool_factory','',
                                ['class' => 'form-control','id' => 'tool_factory']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="factory_addr">Alamat Pabrikan</label>
                            {!! Form::text('tool_factory_address', '',
                                ['class' => 'form-control','id' => 'tool_factory_address']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Lokasi Alat</label>
                            {!! Form::text('location_alat', '',
                                ['class' => 'form-control','id' => 'location_alat']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="location_lat">Lokasi Alat (latitude, format desimal)</label>
                            {!! Form::text('location_lat', '', ['class' => 'form-control','id' => 'location_lat' ]) !!} 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="location_long">Lokasi Alat (longitude, format desimal)</label>
                            {!! Form::text('location_long', '', ['class' => 'form-control','id' => 'location_long']) !!} 
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Lokasi Pengujian</label>
                            {!! Form::text('location', '',
                                ['class' => 'form-control','id' => 'location']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="location_kabkot_id">Lokasi Pengujian (Kabupaten/Kota)</label>
                            {!! Form::select('location_kabkot_id', $kabkot, null, ['class' => 'form-control','id' => 'location_kabkot_id']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

       

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan</button> 
        </form>
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function ()
    {
        $('#tool_capacity_unit').select2({
            //placeholder: "- Pilih UML -",
            tags: true,
        });
        $('#tool_made_in_id').select2();
        $('#location_kabkot_id').select2();

        $('#item_id_grp').hide()
        $('#jenis').select2()
        .on('select2:select', function (e) {
            var data = e.params.data;
            
            if (data.id == 'sertifikat') {
                $("#item_id_grp").hide();
            } else {
                $("#item_id_grp").show(); 
            }
        });
        $('#item_id').select2();

        
    });

</script>
@endsection