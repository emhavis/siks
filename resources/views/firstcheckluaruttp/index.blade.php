@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">

    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            
            <table id="table_data_penjadwalan" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Pendaftaran</th>
                        <th>No Order</th>
                        <th>Nama Pemilik</th>
                        <th>Nama Pemohon</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->no_register }}</td>
                        <td>
                        @if($row->payment_code==null)
                        xx-xxxx-xx-xxx
                        @endif
                        @if($row->payment_code!=null)
                        {{ $row->no_order }}
                        @endif
                        </td>
                        <td>{{ $row->label_sertifikat }}</td>
                        <td>{{ $row->requestor->full_name }}</td>
                        <td>{{ $row->status->status }}</td>
                        <td>
                            <a href="{{ route('firstcheckluaruttp.check', $row->id) }}" class="btn btn-warning btn-sm">Pemeriksaan Awal</a>
                            @if($row->lokasi_pengujian == 'luar')
                                <a target="_blank" href="{{ env('SKHP_URL') . '/publicterm/create/'.$row->booking_id }}" class="btn btn-warning btn-sm">Kaji Ulang</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>

        </div>
    </div>

</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan").DataTable();
        
    });
</script>
@endsection