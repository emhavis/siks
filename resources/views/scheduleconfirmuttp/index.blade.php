@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">

    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            
            <table id="table_data_penjadwalan" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Pendaftaran</th>
                        <th>No Order</th>
                        <th>Nama Pemilik</th>
                        <th>Nominal Order</th>
                        <th>Nama Pemesan</th>
                        <th>Jadwal</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->no_register }}</td>
                        <td>
                        @if($row->payment_code==null)
                        xx-xxxx-xx-xxx
                        @endif
                        @if($row->payment_code!=null)
                        {{ $row->no_order }}
                        @endif
                        </td>
                        <td>{{ $row->label_sertifikat }}</td>
                        <td>{{ number_format($row->total_price,0,',','.') }}</td>
                        <td>{{ $row->requestor->full_name }}</td>
                        
                        <td>{{ $row->received_date }}</td>
                        <td>{{ $row->status->status }}</td>
                        <td>
                            @if($row->scheduled_test_confirmstaf_at == null)
                            <a href="{{ route('scheduleconfirmuttp.schedule', $row->id) }}" class="btn btn-warning btn-sm">Konfirmasi</a>
                            @else
                            Terkonfirmasi
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
       
    
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#table_data_penjadwalan").DataTable();
        $('.select2').select2({
            // placeholder: "- Pilih UML -",
            allowClear: true
        });

        $('.btn-simpan').click(function(e){
            e.preventDefault();
            
            var form = $(this).parents('form:first');
            var form_data = form.serialize();
            form_data += '&_token={{ csrf_token() }}';

            
            $.post('{{ route('requestuttp.simpanbooking') }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('requestuttp') }}' + '/editbooking/' + response.id;
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });
    });
</script>
@endsection