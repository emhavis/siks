@extends('layouts.app')

@section('content')
<div class="row">
<div id="alert_board"></div>
<div class="panel panel-filled" id="panel_create">
    <div class="loader">
        <div class="loader-bar"></div>
    </div>      
    <div class="panel-heading">
        <div class="panel-body">
        
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Booking</h4>
            </div>
            <div class="panel-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="label_sertifikat">Label Sertifikat</label>
                            {!! Form::text('label_sertifikat', $request->label_sertifikat, ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!} 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="addr_sertifikat">Alamat Sertifikat</label>
                            {!! Form::text('addr_sertifikat', $request->addr_sertifikat, ['class' => 'form-control','id' => 'addr_sertifikat', 'readonly']) !!}
                        </div>
                    </div>  
                </div>
            </div>
        </div>

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Pendaftar</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Nama Pendaftar</label>
                            {!! Form::text('pic_name', $requestor->full_name, ['class' => 'form-control','id' => 'pic_name', 'readonly']) !!}
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_name">Jenis Tanda Pengenal</label>
                            {!! Form::text('id_type_id', $requestor->kantor == "Perusahaan" ? "NIB" : "NPWP", ['class' => 'form-control','id' => 'id_type_id', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_id_no">Nomor ID Tanda Pengenal</label>
                            {!! Form::text('pic_id_no', $requestor->kantor == "Perusahaan" ? $requestor->nib : $requestor->npwp, ['class' => 'form-control','id' => 'pic_id_no',  'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_phone_no">Nomor Telepon</label>
                            {!! Form::text('pic_phone_no', $requestor->phone, ['class' => 'form-control','id' => 'pic_phone_no', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pic_email">Alamat Email</label>
                            {!! Form::text('pic_email', $requestor->email, ['class' => 'form-control','id' => 'pic_email', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="received_date">Tanggal Masuk Alat</label>
                            {!! Form::text('received_date', date("d-m-Y", strtotime(isset($request->received_date) ? $request->received_date : date("Y-m-d"))), ['class' => 'date form-control','id' => 'receipt_date', 'placeholder' => 'Tanggal Masuk Alat', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="estimate_date">Tanggal Perkiraan Selesai</label>
                            {!! Form::text('estimated_date', date("d-m-Y", strtotime(isset($request->estimated_date) ? $request->estimated_date : date("Y-m-d"))), ['class' => 'date form-control','id' => 'estimated_date', 'placeholder' => 'Tanggal Perkiraan Selesai', 'autocomplete' => 'off', 'readonly']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_layanan">Jenis Layanan</label>
                            {!! Form::text('jenis_layanan', $request->jenis_layanan, ['class' => 'form-control','id' => 'jenis_layanan', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lokasi_pengujian">Lokasi Pengujian</label>
                            {!! Form::text('lokasi_pengujian', $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' , ['class' => 'form-control','id' => 'lokasi_pengujian', 'readonly']) !!}
                             
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        

        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Informasi Item Pengujian</h4>
            </div>

            <div class="panel-body">

                @foreach($request->items as $item)
                <div class="panel panel-filled panel-c-danger">
                    <div class="panel-body">
                        <form id="form_item_{{ $item->id }}">
                        {!! Form::hidden('id', $item->id) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jenis</label>
                                    {!! Form::text('serial_no', $item->uttp->type->uttp_type, ['class' => 'form-control','id' => 'serial_no', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Merek</label>
                                    {!! Form::text('tool_brand', $item->uttp->tool_brand, ['class' => 'form-control','id' => 'tool_brand', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Model/Tipe</label>
                                    {!! Form::text('tool_model', $item->uttp->tool_model, ['class' => 'form-control','id' => 'tool_model', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nomor Seri</label>
                                    {!! Form::text('serial_no', $item->uttp->serial_no, ['class' => 'form-control','id' => 'serial_no', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Media Uji/Komoditas</label>
                                    {!! Form::text('tool_media', $item->uttp->tool_media, ['class' => 'form-control','id' => 'tool_media', 'readonly']) !!} 
                                </div>
                            </div>
                            <!--
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Tipe</label>
                                    {!! Form::text('tool_type', $item->uttp->tool_type, ['class' => 'form-control','id' => 'tool_type', 'readonly']) !!} 
                                </div>
                            </div>
                            -->
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Buatan</label>
                                    {!! Form::text('tool_made_in', $item->uttp->tool_made_in, ['class' => 'form-control','id' => 'tool_made_in', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Kapasitas</label>
                                    {!! Form::text('tool_capacity', $item->uttp->tool_capacity, ['class' => 'form-control','id' => 'tool_capacity', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Satuan Kapasitas</label>
                                    {!! Form::text('tool_capacity_unit', $item->uttp->tool_capacity_unit, ['class' => 'form-control','id' => 'tool_capacity_unit', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        @if($request->lokasi_pengujian == 'luar' && $item->location != null)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Lokasi Penempatan</label>
                                    {!! Form::text('location', $item->location, ['class' => 'form-control','id' => 'location', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        @endif
                        <!--
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pabrikan</label>
                                    {!! Form::text('tool_factory', $item->uttp->tool_factory, ['class' => 'form-control','id' => 'tool_factory', 'readonly']) !!} 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Alamat Pabrikan</label>
                                    {!! Form::text('tool_factory_address', $item->uttp->tool_factory_address, ['class' => 'form-control','id' => 'tool_factory_address', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Jumlah Pengujian</label>
                                    {!! Form::text('quantity', $item->quantity, ['class' => 'form-control','id' => 'quantity', 'readonly']) !!} 
                                </div>
                            </div>
                        </div>
                        </form>

                        <div class="row">
                            <div class="col-md-12">
                                <table id="standard_item_inspeksi" class="table table-responsive-sm input-table">
                                    <thead>
                                        <tr>
                                            <th>Jenis Pengujian</th>
                                            <th>Jumlah</th>
                                            <th>Satuan</th>
                                            <th>Harga Satuan</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($item->inspections as $inspection)
                                        <tr>
                                            <td>{{ $inspection->inspectionPrice->inspection_type }}</td>
                                            <td>{{ $inspection->quantity }}</td>
                                            <td>{{ $inspection->inspectionPrice->unit }}</td>
                                            <td>{{ number_format($inspection->price, 2, ',', '.') }}</td>
                                            <td>{{ number_format($inspection->quantity * $inspection->price, 2, ',', '.') }}</td>
                                           
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td>Subtotal: </td>
                                            <td><input readonly type="text" name="subtotal[]" id="subtotal" class="form-control" value="{{ number_format($item->subtotal, 2, ',', '.') }}"/></td>
                                            <td></td>
                                        </tr>                            
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>    
                @endforeach

            </div>
        </div>

        <form id="form_create_request">
        <div class="panel panel-filled">
            <div class="panel-heading" >
                <h4>Penugasan dan Penjadwalan</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_id">Nama Penguji/Pemeriksa</label>
                            {!! Form::text('scheduled_test_id', Auth::user()->full_name, ['class' => 'form-control','id' => 'serial_no', 'readonly']) !!} 
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="scheduled_test_date">Jadwal Waktu Pengujian</label>
                            {!! Form::text('scheduled_test_date', 
                                date("d-m-Y", strtotime(isset($request->received_date) ? $request->received_date : date("Y-m-d"))),
                                ['class' => 'date form-control','id' => 'scheduled_test_date', 'readonly']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        </form>

        <button type="submit" class="btn btn-w-md btn-accent" id="btn_simpan">Simpan dan Konfirmasi</button>  
        
        </div>
    </div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
    var index = 1;

    var dt = new Object();

    dt.inspection_prices = new Array();
    dt.standard_items = new Array();
    dt.inspection_price_ids = new Array();
    dt.inspection_price_jumlahs = new Array();
    dt.total = 0;

    $(document).ready(function ()
    {
        
        $('#btn_simpan').click(function(e){
            e.preventDefault();
            
            $("#alert_board").empty();

            var form_data = $("#form_create_request").serialize();

            form_data += '&_token={{ csrf_token() }}';


            $.post('{{ route('scheduleconfirmuttp.confirmschedule', $request->id) }}',form_data,function(response)
            {
                if(response.status===true)
                {
                    window.location = '{{ route('scheduleconfirmuttp') }}';
                }
                else
                {
                    var msg = show_notice(response.messages);
                    toastr["error"]("Mohon Periksan kembali","Form Invalid");
                }
            });
        });

      
    });

</script>
@endsection