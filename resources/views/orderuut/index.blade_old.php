@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="data_table" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Alat</th>
                        <th>Kapasitas</th>
                        <th>Pengujian</th>
                        <th>Kaji Ulang Permintaan</th>
                        <th>Tgl Order</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                    <tr>
                        <td>{{ $row->ServiceRequestItem ? $row->serviceRequestItem->no_order : ''}}</td>
                        <td>
                            {{ $row->tool_brand }}/{{ $row->tool_model }} ({{ $row->tool_serial_no ? $row->tool_serial_no : ''}})
                            <br/>{{ $row->uutType->uut_type }}
                        </td>
                        <td>{{$row ? $row->tool_capacity : ''}}/{{ $row ? $row->tool_capacity_unit : ''}}</td>
                        <td>
                            @foreach($row->ServiceRequestItem->inspections as $inspection)
                            {{ $inspection->inspectionPrice->inspection_type }}
                            <br/>
                            @endforeach
                        </td>
                        <td>
                            {{ $row->ServiceRequestItem->keterangan }}
                        </td>
                        <td>{{ date("d-m-Y", strtotime($row->ServiceRequestItem->order_at)) }}</td>
                        <td>
                        @if($loop->index == 0)
                        <!-- if($idfirst == $row->ServiceRequestItem->id) -->
                        <button class="btn btn-warning btn-sm btn-mdl" 
                            data-id="{{ $row->ServiceRequestItem->id }}" 
                            data-requestid="{{ $row->serviceRequest->id }}"
                            data-labid="{{ $laboratory_id }}">PROSES</button>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="prosesmodal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="m-t-none">Proses Order</h4>
                <!-- <p class="alert alert-danger" id="alert_board" style="display: none;"></p> -->
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_modal">

                            <input type="hidden" name="id" id="id"/>
                            <input type="hidden" name="requestid" id="requestid"/>
                            <input type="hidden" name="labid" id="labid"/>
                            <input type="hidden" name="instalasiid" id="instalasiid"/>
                            <div class="form-group">
                                <label>Laboratorium</label>
                                <input type="text" name="lab_nama" id="lab_nama" class="form-control" readonly required />
                            </div>
                            <!-- <div class="form-group">
                                <label>Lab </label>
                                <input type="text" name="instalasi_nama" id="instalasi_nama" class="form-control" readonly required />
                                <!--
                                <select name="instalasi_id" id="instalasi_id" class="form-control select2" required></select>
                                
                            </div> -->
                            <div class="form-group">
                                <label>Pengujian/Pemeriksaan</label>
                                <textarea name="inspections" id="inspections" readonly
                                    class="form-control"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" id="simpan" class="btn btn-accent">PROSES</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>

<script type="text/javascript">
$(document).ready(function()
{
    $('#data_table').DataTable();

    $("#data_table tbody").on("click","button.btn-mdl",function(e){
        e.preventDefault();
        var id = $(this).data().id;
        var requestid = $(this).data().requestid;
        var labid = $(this).data().labid;

        $("#id").val(id);
        $("#requestid").val(requestid);
        $("#labid").val(labid);

        var route = "{{ route('laboratory.getbyid', ':id') }}";
        route = route.replace(':id', labid);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#lab_nama").val(res.lab.nama_lab);
                
            });
        },"json");

        var route = "{{ route('orderuut.getinspections', ':id') }}";
        route = route.replace(':id', id);

        $.get(route, function(res){
            $("#prosesmodal").modal().on('shown.bs.modal', function ()
            {
                $("#inspections").val(res.inspections.join('<br/>'));
            });
        },"json");
    });

    $("#simpan").click(function(e) {
        e.preventDefault();

        var id = $('#id').val();
        var lab_id = $('labid').val();
        var requestid = $('#requestid').val();
        var instalasiid = $('#instalasiid').val();

        var form = $('#form_modal');
        var form_data = form.serialize();
        form_data += '&_token={{ csrf_token() }}';

        
        $.post('{{ route('orderuut.proses') }}',form_data,function(response)
        {
            if(response.status===true)
            {
                location.reload();
            }
            else
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Mohon Periksan kembali","Form Invalid");
            }
        });
        
        $("#prosesmodal").modal('hide');
        
        // var route = "{{ route('orderuut.proses', ':id') }}";
        // route = route.replace(':id', id);
        
        // window.location = route;
        
    });

});

</script>
@endsection