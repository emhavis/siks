@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="row col-md-6">
    <div class="panel panel-filled" id="panel_create">
    `   <div class="loader">
            <div class="loader-bar"></div>
        </div>  
        <div class="panel-heading">
            <div class="panel-body">
                <form id="createGroup" action="{{ route('uutowners.store')}}" enctype="multipart/form-data">
                    @if($id)
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="inspection_type">Nama</label> 
                        {!!
                            Form::text("nama",$row?$row->nama:'',[
                            'class' => 'form-control',
                            'id' => 'nama',
                            'placeholder' => 'Nama Owner',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_type">NPWP</label> 
                        {!!
                            Form::text("npwp",$row?$row->npwp:'',[
                            'class' => 'form-control',
                            'id' => 'npwp',
                            'placeholder' => 'NPWP Numbers',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_type">NIB</label> 
                        {!!
                            Form::text("nib",$row?$row->nib:'',[
                            'class' => 'form-control',
                            'id' => 'nib',
                            'placeholder' => 'NIB Numbers',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="price">Alamat</label> 
                        {!!
                            Form::text("alamat",$row?$row->alamat:'',[
                            'class' => 'form-control',
                            'id' => 'price',
                            'placeholder' => 'Price',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_template_id">Nama Kota</label> 
                        {!!
                            Form::select("kota_id",
                            $kabupatenkota,
                            $row?$row->kota_id:'',[
                            'class' => 'form-control',
                            'id' => 'kota_id',
                            'placeholder' => 'Kabupaten Kota ',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="price">Email</label> 
                        {!!
                            Form::email("email",$row?$row->email:'',[
                            'class' => 'form-control',
                            'id' => 'email',
                            'placeholder' => 'Email',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="price">Telepon</label> 
                        {!!
                            Form::text("telepon",$row?$row->telepon:'',[
                            'class' => 'form-control',
                            'id' => 'telepon',
                            'placeholder' => 'Telephone Number',
                            'required'
                            ]);
                        !!}
                    </div>
                    <div class="form-group">
                        <label for="inspection_type">Penanggung Jawab</label> 
                        {!!
                            Form::text("penanggung_jawab",$row?$row->penanggung_jawab:'',[
                            'class' => 'form-control',
                            'id' => 'penanggung_jawab',
                            'placeholder' => 'Penanggung Jawab',
                            'required'
                            ]);
                        !!}
                    </div>
                    <button type="submit" id="submit" class="btn btn-default">Submit</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function ()
{
    document.getElementById('addForm').style.display= 'none';  
    var checkbox = document.querySelector("input[name=has_range]");
    checkbox.checked =''

    checkbox.addEventListener('change', function() {
    if (this.checked) {
        document.getElementById('addForm').style.display= 'block';; 
    } else {
        document.getElementById('addForm').style.display= 'none';  
    }
    });
    $('#template_id').select2({
    });
    $("#submit").click(function(e)
    {
        e.preventDefault();

        var formData = $("#createGroup").serialize();

        $("#panel_create").toggleClass("ld-loading");
        $.post("{{ route('insprice.store') }}",formData,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            console.log(response.messages)
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
            }
            else
            {
                window.location = "{{ route('uutowners') }}";
            }
        });

    });
});

function show_notice(msg)
{
    $(document).find("small.text-warning").remove();
    $.each(msg,function(i,v)
    {
        $(document).find("label[for='"+i+"']").append("<small class='text-warning m-l-xs'>"+v+"</small>");
    });

    return;
}
</script>
@endsection