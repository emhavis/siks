@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            <div class="pull-right text-right" style="line-height: 14px">
                <small>Master Data<br> <span class="c-white">Grup</span></small>
            </div>
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-add-user"></i>
            </div>
            <div class="header-title">
                <h3>Edit Standar</h3>
                <small>
                    Halaman Penambahan Master Standar
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>

<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_edit">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>        
        <div class="panel-heading">
            <div class="panel-body">
                <!-- {{$standard}} -->
                {!! Form::open(array(
                  'route' => array('standard.update', $standard[0]->id),
                  'method' => 'PATCH',
                  'id' => 'form_edit_standard', 'enctype' => "multipart/form-data")) !!}

                    <div class="form-group">
                    {!! QrCode::size(100)->margin(0.5)->generate($standard[0]->tool_code); !!}
                    </div>
                    <input type="hidden" name="id" value="{{$id}}" />
                    <div class="form-group">
                        <label for="uml_id">Nama Unit Metrologi Legal</label>
                        {!! Form::text('uml_id', $standard[0]->uml_name,
                            ['class' => 'form-control',
                             'id' => 'uml_id',
                             'readonly']) !!}
                    </div>
                    <div class="form-group">
                        <label for="uml_sub_id">Nama Sub Unit Metrologi Legal</label>
                        {!! Form::text('uml_sub_id', $standard[0]->uml_sub_name,
                            ['class' => 'form-control',
                             'id' => 'uml_id',
                             'readonly']) !!}
                    </div>
            <div class="panel panel-filled panel-c-accent">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="standard_measurement_type_id">Jenis Besaran</label>
                        {!! Form::text('standard_measurement_type_id', $standard[0]->standard_type,
                            ['class' => 'form-control',
                             'id' => 'standard_measurement_type_id',
                             'readonly']) !!}
                    </div>
                    <div class="form-group">
                        <label for="standard_tool_type_id">Jenis Alat</label>
                        {!! Form::text('standard_tool_type_id', $standard[0]->attribute_name,
                            ['class' => 'form-control',
                             'id' => 'standard_tool_type_id',
                             'readonly']) !!}
                    </div>
                    <div class="form-group">
                        <label for="standard_detail_type_id">Rincian Alat</label>
                        {!! Form::text('standard_detail_type_id', $standard[0]->standard_detail_type_name,
                            ['class' => 'form-control',
                             'id' => 'standard_detail_type_id',
                             'readonly']) !!}
                    </div>
                </div>
            </div>
                    <div class="form-group">
                        <label for="sumber_id">Sumber</label>
                        {!! Form::select('sumber_id', $msumber, $standard[0]->sumber_id,
                            ['class' => 'form-control',
                             'id' => 'sumber_id',
                             'placeholder' => '- Pilih Sumber -',
                             'required']) !!}

                    </div>
                    <div class="form-group">
                        <label for="brand">Merk</label>
                        {!! Form::text('brand', $standard[0]->brand,
                                       ['class' => 'form-control',
                                        'id' => 'brand',
                                        'placeholder' => 'Merk',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="made_in">Buatan</label>
                        {!! Form::select('made_in', $standardNegara, $standard[0]->made_in,
                            ['class' => 'form-control',
                             'id' => 'made_in',
                             'placeholder' => '- Pilih Negara -',
                             'required']) !!}

                    </div>
                    <div class="form-group">
                        <label for="model">Model</label>
                        {!! Form::text('model', $standard[0]->model,
                                       ['class' => 'form-control',
                                        'id' => 'model',
                                        'placeholder' => 'Model',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="model">Tipe</label>
                        {!! Form::text('tipe', $standard[0]->tipe,
                                       ['class' => 'form-control',
                                        'id' => 'tipe',
                                        'placeholder' => 'Tipe Alat',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="tool_code">Nomor Seri</label>
                        {!! Form::text('no_seri', $standard[0]->no_seri,
                                       ['class' => 'form-control',
                                        'id' => 'no_seri',
                                        'placeholder' => 'Nomor Seri',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="tool_code">Nomor Identitas</label>
                        {!! Form::text('no_identitas', $standard[0]->no_identitas,
                                       ['class' => 'form-control',
                                        'id' => 'no_identitas',
                                        'placeholder' => 'Nomor Identitas',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="capacity">Kapasitas</label>
                        {!! Form::text('capacity', $standard[0]->capacity,
                                       ['class' => 'form-control',
                                        'id' => 'capacity',
                                        'placeholder' => 'Kapasitas',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="capacity">Daya Baca</label>
                        {!! Form::text('daya_baca', $standard[0]->daya_baca,
                                       ['class' => 'form-control',
                                        'id' => 'daya_baca',
                                        'placeholder' => 'Daya Baca',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="class">Kelas</label>
                        {!! Form::text('class', $standard[0]->class,
                                       ['class' => 'form-control',
                                        'id' => 'class',
                                        'placeholder' => 'Kelas',
                                        'required']) !!}
                    </div>
                    <div class="form-group">
                        <label for="class">Jumlah Per Set</label>
                        {!! Form::text('jumlah_per_set', $standard[0]->jumlah_per_set,
                                       ['class' => 'form-control',
                                        'id' => 'jumlah_per_set',
                                        'required']) !!}
                    </div>
                    <button type="button" id="btn_cancel" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</button>
                    <button type="button" id="btn_submit" class="btn btn-warning"><i class="fa fa-save"></i> Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.min.js') }}"></script>
<script type='text/javascript'>

var stat = true;
var stat2 = true;

$(document).ready(function()
{
    // var masking = "{{ str_limit($standard[0]->tool_code, 9, '')}}" + "9999";

    $('#sumber_id, #made_in').select2();

    // $("#tool_code").inputmask(masking);

    $("#jumlah_per_set").inputmask({'alias': 'numeric', 'placeholder': '0'});
    
    // $('#uml_id').change(function(){
    //     $.ajax({
    //         url: '{{ route('uml.getumlinfo') }}',
    //         type: 'post',
    //         data: {
    //             id: this.value
    //         },
    //         beforeSend: function() {
    //             $("#progress_spin").show();
    //         },
    //         success: function(response){
    //             var json_response = JSON.parse(response);
    //             var kode_kabkota = json_response.kabupatenkota.kode;

    //             masking = kode_kabkota +  masking.substring(5, 13);
    //             $("#tool_code").inputmask(masking);
    //         },
    //         complete:function(data){
    //             $("#progress_spin").hide();
    //         }
    //     });
    // });

    // $('#standard_measurement_type_id').change(function(){
    //     // get info of measurement unit
    //     var kode_besaran = ("0" + this.value).slice(-2);
    //     masking = masking.substring(0, 6) + kode_besaran + masking.substring(8, 13);
    //     $("#tool_code").inputmask(masking);

    //     // get list of standard tool type
    //     $.ajax({
    //         url: '{{ route('standard.standartooltypefordropdown') }}',
    //         type: 'post',
    //         data: {
    //             standardMeasurementTypeId: this.value
    //         },
    //         beforeSend: function() {
    //             $(".progress-spin").show();
    //         },
    //         success: function(response){
    //             var standard_drop_list = $('#standard_tool_type_id');
    //             standard_drop_list.empty();
    //             var a = new Array();
    //             $.each(response, function(index, element) {
    //                 a.push({"id":index,"text":element});
    //             });
    //             standard_drop_list.select2({data:a});
    //         },
    //         complete:function(data){
    //             $(".progress-spin").hide();
    //             if(stat)
    //             {
    //                 stat = false;
    //                 $("#standard_tool_type_id").val({{$standard[0]->standard_tool_type_id}}).select2();
    //                 $("#standard_tool_type_id").trigger("change");                    
    //             }
    //         }
    //     });
    // });

    // $('#standard_tool_type_id').change(function(){
    //     $.ajax({
    //         url: '{{ route('standard.standardetailtypefordropdown') }}',
    //         type: 'post',
    //         data: {
    //             standardToolTypeId: this.value
    //         },
    //         beforeSend: function() {
    //             $(".progress-spin").show();
    //         },
    //         success: function(response){
    //             var standard_drop_list = $('#standard_detail_type_id');
    //             standard_drop_list.empty();
    //             var a = new Array();
    //             // var b = new Array();
    //             // standard_drop_list.append("<option>- Pilih Jenis Alat -</option>");
    //             $.each(response, function(index, element) {
    //                 a.push({"id":index,"text":element});
    //                 // standard_drop_list.append("<option value='"+ index +"'>" + element + "</option>");
    //             });
    //             standard_drop_list.select2({data:a});
    //         },
    //         complete:function(data){
    //             $(".progress-spin").hide();
    //             if(stat2)
    //             {
    //                 stat2 = false;
    //                 $("#standard_detail_type_id").val({{$standard[0]->standard_detail_type_id}}).select2();
    //             }
    //         }
    //     });
    // });

    $("#btn_cancel").click(function(e){
        e.preventDefault();
        window.location = '{{ route('standard.index') }}';
    });

    $("#btn_submit").click(function(e){
        e.preventDefault();
        $("#alert_board").empty();

        var form_object = $("#form_edit_standard");
        var form_data = $("#form_edit_standard").serialize();

        $("#panel_edit").toggleClass("ld-loading");

        $.post('{{ route('standard.update', $standard[0]->id) }}',form_data,function(response){
            if(response[0]==false)
            {
                var msg = show_notice(response[1]);
                $("#alert_board").html(msg);
                $("#panel_edit").toggleClass("ld-loading");
            }
            else
            {
                window.location = '{{ route('standard.index') }}';
            }
        },"json");
    });

    // $("#standard_measurement_type_id").trigger("change");
    // $('#uml_id, #standard_measurement_type_id, #standard_tool_type_id, #standard_detail_type_id').attr("disabled",true);
});

function show_notice(msg)
{
    var str = '<div class="panel panel-filled panel-c-danger">'+
        '<div class="panel-body">'+
            '<b>Terjadi kesalahan.</b>'+
            'Telah terjadi kesalahan, hubungi administrator untuk info lebih lanjut.'+
            '<ul>';
            $.each(msg,function(i,v){
                str += '<li>'+v+'</li>';
            });
    str += '</ul>'+
        '</div>'+
    '</div>';

    return str;
}

    // $('#standard_measurement_unit_id').change(function() {

    //     $.getJSON("/standard/standartooltypefordropdown/", { standardMeasurementTypeId : $(this).val() }, function(data) {});

    //     var standard_drop_list = $('#standard_tool_type_id');
    //     standard_drop_list.empty();

    //     $.each(data, function(index, element) {
    //         standard_drop_list.append("<option value='"+ element.id +"'>" + element.name + "</option>");
    //     });

    // });
    // });
</script>
@endsection
