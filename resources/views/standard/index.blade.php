@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}"/>
@endsection

@section('content')
<div class="row">
    <a href="{{ route('standard.create') }}" class="btn btn-w-md btn-primary">Buat Baru</a>
    <div class="panel panel-filled table-area">
        <div class="panel-heading">
            <table id="tStandard" class="table table-striped table-hover table-responsive-sm">
                <thead>
                    <tr>
                        <th>QR Code Alat</th>
                        <th>UML</th>
                        <th>Sub UML</th>
                        <th>Besaran</th>
                        <th>Tipe Alat</th>
                        <th>Rincian Alat</th>
                        <th>Merk</th>
                        <th>Model</th>
                        <th>Kapasitas/Daya Baca</th>
                        <th>Kelas</th>
                        <!-- <th>Deskripsi</th> -->
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @if($rows->count())
                    @foreach($rows as $standard)
                    <tr>
                        <td>{{ $standard->tool_code }}</td>
                        <td>{{ $standard->MasterUml->uml_name }}</td>
                        <td>{{ $standard->MasterSubUml->uml_sub_name }}</td>
                        <td>{{ $standard->standardmeasurementtype->standard_type }}</td>
                        <td>{{ $standard->standardtooltype?$standard->standardtooltype->attribute_name:'' }}</td>
                        <td>{{ $standard->standarddetailtype?$standard->standarddetailtype->standard_detail_type_name:'' }}</td>
                        <td>{{ $standard->brand }}</td>
                        <td>{{ $standard->model }}</td>
                        <td>{{ $standard->capacity }}</td>
                        <td>{{ $standard->class }}</td>
                        <!-- <td>{{ $standard->description }}</td> -->
                        <td>
                            <a href="{{ route('standard.create',$standard->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#tStandard').DataTable();
    });
</script>
@endsection
