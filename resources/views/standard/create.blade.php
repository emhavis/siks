@extends('layouts.app')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
@section('styles')

@endsection

@section('content')
<div class="row">
    <div id="alert_board"></div>
    <div class="panel panel-filled" id="panel_create">
        <div class="loader">
            <div class="loader-bar"></div>
        </div>        
        <div class="panel-heading">
            <div class="panel-body">
                {!! Form::open(array('id' => 'form_create_standard', 'enctype' => "multipart/form-data")) !!}
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="tool_code">QR Code Alat</label>
                        <span class="input-group">
                            {!! Form::text('tool_code', $standard?$standard->tool_code:"",
                               ['class' => 'form-control',
                                'style' => 'font-size:16px;',
                                'id' => 'tool_code',
                                'placeholder' => '000.0.00.0000',
                                'required']) !!}
                            <span class="input-group-btn">
                                <button id="search_item" type="button" class="btn btn-accent">Cari</button>
                                <a class="btn btn-success" href="{{ route('qrcode') }}">
                                    Buat Baru
                                </a>    
                            </span>
                        </span>                        
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="uml_id2" id="uml_id2" {!! $standard?"value=".$standard->uml_id:"" !!} />
                        <label for="uml_id">Nama Unit Metrologi Legal</label>
                        {!! Form::text('uml_id', $standard?$standard->MasterUml->uml_name:"",
                            ['class' => 'form-control',
                             'id' => 'uml_id',
                             'placeholder' => 'UML',
                             'readonly']) !!}

                    </div>
                    <div class="form-group">
                        <input type="hidden" name="uml_sub_id2" id="uml_sub_id2" {!! $standard?"value=".$standard->uml_sub_id:"" !!} />
                        <label for="uml_sub_id">Nama Sub Unit Metrologi Legal</label>
                        {!! Form::text('uml_sub_id', $standard?$standard->MasterSubUml->uml_sub_name:"",
                            ['class' => 'form-control',
                             'id' => 'uml_sub_id',
                             'placeholder' => 'Sub UML',
                             'readonly']) !!}

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-filled panel-c-accent">
                        <div class="panel-body">
                            <div class="form-group">
                                <input type="hidden" name="standard_measurement_type_id2" {!! $standard?"value=".$standard->standard_measurement_type_id:"" !!} id="standard_measurement_type_id2" />
                                <label for="standard_measurement_type_id">Jenis Besaran</label>
                                {!! Form::text('standard_measurement_type_id', $standard?$standard->StandardMeasurementType->standard_type:"",
                                    ['class' => 'form-control',
                                     'id' => 'standard_measurement_type_id',
                                     'placeholder' => 'Jenis Besaran',
                                     'readonly']) !!}
                            </div>
                            <div class="form-group">
                                <label for="standard_tool_type_id">Jenis Alat</label>
                                {!! Form::select('standard_tool_type_id', $standard?[$standard->standard_tool_type_id=>$standard->StandardToolType->attribute_name]:[], $standard?$standard->standard_tool_type_id:null,
                                    ['class' => 'form-control',
                                     'id' => 'standard_tool_type_id',
                                     'placeholder' => '- Pilih Jenis Alat -',
                                     'required']) !!}
                                <span id="progress_spin" style="float:right;display:none;">
                                    <i class="fa fa-circle-o-notch fa-spin"></i>
                                </span>
                            </div>
                            <div class="form-group">
                                <label for="standard_detail_type_id">Rincian Alat</label>
                                {!! Form::select('standard_detail_type_id', $standard?[$standard->standard_detail_type_id=>$standard->StandardDetailType->standard_detail_type_name]:[], $standard?$standard->standard_detail_type_id:null,
                                    ['class' => 'form-control',
                                     'id' => 'standard_detail_type_id',
                                     'placeholder' => '- Pilih Rincian Alat -',
                                     'required']) !!}
                                <span id="progress_spin" style="float:right;display:none;">
                                    <i class="fa fa-circle-o-notch fa-spin"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <h4>Identitas Alat</h4>
                    </div>
                    <div class="panel-body">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sumber_id">Sumber</label>
                                {!! Form::select('sumber_id', $msumber, $standard?$standard->sumber_id:null,
                                    ['class' => 'form-control',
                                     'id' => 'sumber_id',
                                     'placeholder' => '- Pilih Sumber -',
                                     'required']) !!}

                            </div>
                            <div class="form-group">
                                <label for="brand">Merk</label>
                                {!! Form::text('brand', $standard?$standard->brand:null,
                                               ['class' => 'form-control',
                                                'id' => 'brand',
                                                'placeholder' => 'Merk',
                                                'autocomplete' => 'off',
                                                'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="made_in">Buatan</label>
                                {!! Form::select('made_in', $standardNegara, $standard?$standard->made_in:null,
                                    ['class' => 'form-control',
                                     'id' => 'made_in',
                                     'placeholder' => '- Pilih Negara -',
                                     'required']) !!}

                            </div>
                {{-- "id" => 190
                "tool_code" => "025.3.01.0002"
                "standard_tool_type_id" => 15
                "standard_measurement_unit_id" => null
                "standard_measurement_type_id" => 1
                "description" => "Verifikasi BSML"
                "deleted_at" => null
                "standard_detail_type_id" => 379
                "brand" => "TEST"
                "model" => "TEST"
                "capacity" => "1234"
                "class" => "WER"
                "uml_id" => 210
                "made_in" => 6
                "tipe" => "TEST"
                "no_identitas" => "STET"
                "daya_baca" => "WER"
                "jumlah_per_set" => 12
                "no_seri" => "1234"
                "uml_sub_id" => 6
                "sumber_id" => 2
                "created_by" => 1
                "tanggal_verifikasi" => "2020-01-15"
                "no_sertifikasi_verifikasi" => "123"
                "standard_name_type_id" => 1
                "satuan" => null
                "deskripsi_alat" => null                 --}}                            <div class="form-group">
                                <label for="model">Model</label>
                                {!! Form::text('model', $standard?$standard->model:null,
                                               ['class' => 'form-control',
                                                'id' => 'model',
                                                'placeholder' => 'Model',
                                                'autocomplete' => 'off',
                                                'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="tipe">Tipe</label>
                                {!! Form::text('tipe', $standard?$standard->tipe:null,
                                               ['class' => 'form-control',
                                                'id' => 'tipe',
                                                'placeholder' => 'Tipe Alat',
                                                'autocomplete' => 'off',
                                                'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="no_seri">Nomor Seri</label>
                                {!! Form::text('no_seri', $standard?$standard->no_seri:null,
                                               ['class' => 'form-control',
                                                'id' => 'no_seri',
                                                'placeholder' => 'Nomor Seri',
                                                'autocomplete' => 'off',
                                                'required']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-filled panel-c-info">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="no_identitas">Nomor Identitas</label>
                                        {!! Form::text('no_identitas', $standard?$standard->no_identitas:null,
                                                       ['class' => 'form-control',
                                                        'id' => 'no_identitas',
                                                        'placeholder' => 'Nomor Identitas',
                                                        'autocomplete' => 'off',
                                                        'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="capacity">Kapasitas</label>
                                        {!! Form::text('capacity', $standard?$standard->capacity:null,
                                                       ['class' => 'form-control',
                                                        'id' => 'capacity',
                                                        'placeholder' => 'Kapasitas',
                                                        'autocomplete' => 'off',
                                                        'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="daya_baca">Daya Baca</label>
                                        {!! Form::text('daya_baca', $standard?$standard->daya_baca:null,
                                                       ['class' => 'form-control',
                                                        'id' => 'daya_baca',
                                                        'placeholder' => 'Daya Baca',
                                                        'autocomplete' => 'off',
                                                        'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="class">Kelas</label>
                                        {!! Form::text('class', $standard?$standard->class:null,
                                                       ['class' => 'form-control',
                                                        'id' => 'class',
                                                        'placeholder' => 'Kelas',
                                                        'autocomplete' => 'off',
                                                        'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="jumlah_per_set">Jumlah Per Set</label>
                                        {!! Form::text('jumlah_per_set', $standard?$standard->jumlah_per_set:null,
                                                       ['class' => 'form-control',
                                                        'id' => 'jumlah_per_set',
                                                        'required']) !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <a href='{{ route('standard') }}' id="btn_cancel" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</a>
                    <button type="button" id="btn_submit" class="btn btn-warning"><i class="fa fa-save"></i> Simpan</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ asset('assets/vendor/inputmask/jquery.inputmask.min.js') }}"></script>
<script type='text/javascript'>

var el = new Object();

$(document).ready(function(){

    el.masking = "999.9.99.9999";

    $('#standard_tool_type_id, #standard_detail_type_id, #made_in, #sumber_id').select2({
        allowClear: true
    });

    $("#tool_code").inputmask(el.masking);

    $("#jumlah_per_set").inputmask({'alias': 'numeric', 'placeholder': '0'});

    $('#search_item').click(function()
    {

    });

    $('#search_item').click(function()
    {
        $.post('{{ route('qrcode.info') }}',
        { 
            tool_code: $('#tool_code').val(),
            _token: "{{ csrf_token() }}"
        },
        function(response)
        {
            res = response[0];
            $("#uml_id").val(res["uml_name"]);
            $("#uml_id2").val(res["uml_id"]);
            $("#uml_sub_id").val(res["uml_sub_name"]);
            $("#uml_sub_id2").val(res["uml_sub_id"]);
            $("#standard_measurement_type_id").val(res["standard_type"]);
            $("#standard_measurement_type_id2").val(res["standard_measurement_type_id"]);

            $('#standard_tool_type_id').empty();
            $('#standard_detail_type_id').empty();

            $.get('{{ route('tooltypedropdown') }}/'+res["standard_measurement_type_id"],function(response)
            {
                var standard_drop_list = $('#standard_tool_type_id');

                standard_drop_list.append("<option>- Pilih Jenis Alat -</option>");
                $.each(response, function(index, element) {
                    standard_drop_list.append("<option value='"+ index +"'>" + element + "</option>");
                });
            });

        });
    });

    $('#standard_tool_type_id').change(function()
    {
        $.get('{{ route('detailtypedropdown') }}/'+this.value,function(response)
        {
            var standard_drop_list = $('#standard_detail_type_id');
            standard_drop_list.empty();

            standard_drop_list.append("<option>- Pilih Jenis Alat -</option>");
            $.each(response, function(index, element)
            {
                standard_drop_list.append("<option value='"+ index +"'>" + element + "</option>");
            });
        });
    });

    $("#btn_submit").click(function(e)
    {
        e.preventDefault();

        // var form_object = $("#form_create_standard");
        var form_data = $("#form_create_standard").serialize();
        form_data += "&_token={{ csrf_token() }}";
        {!! $id?"form_data += '&id=".$id."'":"" !!}

        $("#panel_create").toggleClass("ld-loading");
        $.post('{{ route('standard.store') }}',form_data,function(response)
        {
            $("#panel_create").toggleClass("ld-loading");
            if(response.status==false)
            {
                var msg = show_notice(response.messages);
                toastr["error"]("Cek Ulang Form!","Error");
            }
            else
            {
                toastr["success"]("Data terlah disimpan","Info");
            }
        });
    });

});

</script>

@endsection
