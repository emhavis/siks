<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>SIMPEL UPTP IV</title>

    <!-- Vendor styles -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/fontawesome/css/font-awesome.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/vendor/animate.css/animate.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.css') }}"/>

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('assets/styles/pe-icons/pe-icon-7-stroke.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/styles/pe-icons/helper.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/styles/stroke-icons/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/styles/style.css') }}">
</head>
<body class="blank">

<!-- Wrapper-->
<div class="wrapper">


    <!-- Main content-->
    <section class="content">
        <div class="container-center animated slideInDown">

            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-unlock"></i>
                </div>
                <div class="header-title">
                    <h3>Login</h3>
                    <small>
                        Please enter your credentials to login.
                    </small>
                </div>
            </div>

            <div class="panel panel-filled">
                <div class="panel-body">
                    <form id="loginForm" action="{{ route('login') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label" for="username">{{ __('Username') }}</label>
                            <input type="text" placeholder="username" title="Please enter you username" required value="" name="username" id="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" value="{{ old('username') }}" autocomplete="off">
                            @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                            <span class="help-block small">Your unique username to app</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="password">{{ __('Password') }}</label>
                            <input id="password" name="password" type="password" title="Please enter your password" placeholder="******" required value="" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            <span class="help-block small">Your strong password</span>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-accent">{{ __('Login') }}</button>
                            <a class="btn btn-default" href="register.html">Register</a>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="{{ asset('assets/vendor/pacejs/pace.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- App scripts -->
<script src="{{ asset('assets/scripts/luna.js') }}"></script>

</body>

</html>