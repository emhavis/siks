<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('/getUmlInfo', 'UmlController@getUMLInfo')->name('uml.getumlinfo');

// // CONTROLLER QRCODE
// Route::post('/qrcodeinfoByUml', 'QrcodeController@qrcodeinfoByUml')->name('qrcode.qrcodeinfoByUml');
// Route::post('/qrcodefordropdown', 'QrcodeController@qrcodefordropdown')->name('qrcode.qrcodefordropdown');
// Route::post('/qrcodesprint', 'QrcodeController@qrcodesprint')->name('qrcode.qrcodesprint');
// // Route::post('/getdata', 'QrcodeController@getdata')->name('qrcode.getdata');

// // CONTROLLER USER
// // Route::post('/simpan_user', 'UserController@simpan_user')->name('user.simpan_user');

// // CONTROLLER UML
// Route::post('/create_standard_simpan', 'UmlController@create_standard_simpan')->name('uml.create_standard_simpan');
// // Route::post('/edit_standard_simpan', 'UmlController@edit_standard_simpan')->name('uml.edit_standard_simpan');

// // CONTROLLER STANDARD
// // Route::post('/inspectionpricefordropdown', 'StandardController@inspectionpricefordropdown')->name('standard.inspectionpricefordropdown');
// Route::post('/inspectionpriceinfo', 'StandardController@inspectionpriceinfo')->name('standard.inspectionpriceinfo');
// Route::post('/profilumlinfo', 'StandardController@profilumlinfo')->name('standard.profilumlinfo');
// Route::post('/subumlfordropdown', 'StandardController@subumlfordropdown')->name('standard.subumlfordropdown');
// Route::post('/umlstandardinfo', 'StandardController@umlstandardinfo')->name('standard.umlstandardinfo');
// // Route::post('/standarmeasurementtypefordropdown', 'StandardController@standarmeasurementtypefordropdown')->name('standard.standarmeasurementtypefordropdown');
// // Route::post('/standartooltypefordropdown', 'StandardController@standartooltypefordropdown')->name('standard.standartooltypefordropdown');
// // Route::post('/standardetailtypefordropdown', 'StandardController@standardetailtypefordropdown')->name('standard.standardetailtypefordropdown');
// // Route::post('/qrcodeinfo', 'StandardController@qrcodeinfo')->name('standard.qrcodeinfo');
// // Route::post('/simpanmasterdata', 'StandardController@simpanmasterdata')->name('standard.simpanmasterdata');
// // Route::post('/simpanmasterlab', 'StandardController@simpanmasterlab')->name('standard.simpanmasterlab');

// // CONTROLLER SERVICE REQUEST
// Route::post('/simpan', 'ServiceRequestController@simpan')->name('service.simpan');
// Route::post('/paymentsave/{id}', 'ServiceRequestController@paymentsave')->name('service.paymentsave');
// Route::post('/get_service_request_items', 'ServiceRequestController@get_service_request_items')->name('service.get_service_request_items');
// Route::post('/get_service_orders', 'ServiceRequestController@get_service_orders')->name('service.get_service_orders');
// Route::post('/get_service_order_outs', 'ServiceRequestController@get_service_order_outs')->name('service.get_service_order_outs');
// Route::post('/get_tool_code_info', 'ServiceRequestController@get_tool_code_info')->name('service.get_tool_code_info');
// Route::post('/get_service_order_out_finish', 'ServiceRequestController@get_service_order_out_finish')->name('service.get_service_order_out_finish');
// Route::post('/get_service_order_finish', 'ServiceRequestController@get_service_order_finish')->name('service.get_service_order_finish');
// // Route::post('/stafsimpan_result/{id}/', 'ServiceRequestController@stafsimpan_result')->name('service.stafsimpan_result');
