<?php
use App\MasterLaboratorySeries;
use App\StandardDetailType;
use App\StandardInspectionPrice;
use App\StandardMeasurementType;
use App\StandardToolType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->to('/login');
});

Auth::routes();

Route::get('logout', function(){
	Auth::logout();
	return redirect()->to('login');
})->name('logout');

Route::get('/secure/csrf-token', function (Request $request) {
	$token = csrf_token();
	return $token;
});
Route::post('secure/auth', 'Auth\SecureController@authenticate')->name('secure.auth');
//Route::get('secure/client', 'Auth\SecureController@client')->name('secure.client');

// Term
// Route::get('term/{labid}/{token}', 'TermController@term')->name('service.kuitansi');
Route::get('term/{labid}', 'TermController@term')->name('document.term');
Route::get('term/{labid}/download', 'TermController@downloadPDF')->name('document.download');
// DOCUMENT DOWNLOAD FROM OTHER APP
Route::get('documentuttp/print_revision/{id}/{jenis_sertifikat}/{token}', 'DocumentUttpController@print_revision')->name('documentuttp.print_revision');

Route::get('documentuttp/print/{id}/{jenis_sertifikat}/{token}', 'DocumentUttpController@print')->name('documentuttp.print');
Route::get('documentuttp/download/{id}/{token}', 'DocumentUttpController@download')->name('documentuttp.download');
Route::get('documentuttp/valid/{jenis_sertifikat}/{token}', 'DocumentUttpController@valid')->name('documentuttp.valid');

Route::get('documentuttp/invoice/{id}/{token}', 'DocumentUttpController@invoice')->name('documentuttp.invoice');
Route::get('documentuttp/invoicetuhp/{id}/{token}', 'DocumentUttpController@invoicetuhp')->name('documentuttp.invoicetuhp');
Route::get('documentuttp/kuitansi/{id}/{token}', 'DocumentUttpController@kuitansi')->name('documentuttp.kuitansi');
Route::get('documentuttp/buktiorder/{id}/{token}', 'DocumentUttpController@buktiorder')->name('documentuttp.buktiorder');

Route::get('documentuttp/surattugas/{id}/{token}', 'DocumentUttpController@surattugas')->name('documentuttp.surattugas');

// DOCUMENT DOWNLOAD FROM OTHER APP FOR SNSU (UUT)
Route::get('documentuut/print/{id}/{jenis_sertifikat}/{token}', 'DocumentUutController@print')->name('documentuut.print');
// Route::get('documentuut/print/{id}/{token}', 'DocumentUutController@print')->name('documentuut.print');
Route::get('documentuut/download/{id}/{token}', 'DocumentUutController@download')->name('documentuut.download');
Route::get('documentuut/valid/{jenis_sertifikat}/{token}', 'DocumentUutController@valid')->name('documentuut.valid');

// Surat permohonan download
Route::get('documentuut/download-sp/{id}/{token}', 'DocumentUutController@download_sp')->name('documentuut.download-sp');

Route::get('documentuut/invoice/{id}/{token}', 'DocumentUutController@invoice')->name('documentuut.invoice');
Route::get('documentuut/invoicetuhp/{id}/{token}', 'DocumentUutController@invoicetuhp')->name('documentuut.invoicetuhp');
Route::get('documentuut/kuitansi/{id}/{token}', 'DocumentUutController@kuitansi')->name('documentuut.kuitansi');
Route::get('documentuut/buktiorder/{id}/{token}', 'DocumentUutController@buktiorder')->name('documentuut.buktiorder');


Route::post('mailer/send', 'MailerController@send')->name('mailer.send');

Route::get('approveuttp/secret', 'ApproveUttpController@secrethashing')->name('approveuttp.secret');

// Route::get('tester', 'TesterController@index')->name('tester');
// Route::get('tester/ok', 'TesterController@ok')->name('tester.ok');
// Route::post('tester/save', 'TesterController@save')->name('tester.save');

//test notif
Route::get('expiredorderuttp', 'ExpiredOrderUttpController@index')->name('expiredorderuttp');
Route::get('expiredorderuttp/notif', 'ExpiredOrderUttpController@notif')->name('expiredorderuttp.notif');
Route::get('expiredorderuttp/export', 'ExpiredOrderUttpController@export')->name('expiredorderuttp.export');

Route::middleware(['auth'])->group(function ()
{
	Route::get('home', 'HomeController@index')->name('home');

	Route::get('userprofil', 'UserprofilController@index')->name('userprofil');
	Route::post('userprofil/store', 'UserprofilController@store')->name('userprofil.store');
	Route::post('userprofil/storePassword', 'UserprofilController@storePassword')->name('userprofil.storePassword');

	Route::get('ckeditor', 'CKEditorController@index');
	Route::post('ckeditor/upload', 'CKEditorController@upload')->name('ckeditor.upload');

	

	Route::middleware(['CheckAdmin'])->group(function ()
	{
		Route::get('user', 'UserController@index')->name('user');
		Route::get('user/create/{id?}', 'UserController@create')->name('user.create');
		Route::post('user/store', 'UserController@store')->name('user.store');
		Route::post('user/action', 'UserController@action')->name('user.action');

		Route::get('role', 'RoleController@index')->name('role');
		Route::get('role/create/{id?}', 'RoleController@create')->name('role.create');
		Route::post('role/store', 'RoleController@store')->name('role.store');
		Route::post('role/action', 'RoleController@action')->name('role.action');

		Route::get('article', 'ArticleController@index')->name('article');
		Route::get('article/create', 'ArticleController@create')->name('article.create');
		Route::post('article/store', 'ArticleController@store')->name('article.store');
		Route::get('article/edit/{id}', 'ArticleController@edit')->name('article.edit');
		Route::post('article/update/{id}', 'ArticleController@update')->name('article.update');
		Route::post('article/delete/{id}', 'ArticleController@delete')->name('article.delete');

		Route::get('file', 'ArticleFileController@index')->name('file');
		Route::get('file/create', 'ArticleFileController@create')->name('file.create');
		Route::post('file/store', 'ArticleFileController@store')->name('file.store');
		Route::get('file/edit/{id}', 'ArticleFileController@edit')->name('file.edit');
		Route::post('file/update/{id}', 'ArticleFileController@update')->name('file.update');
		Route::post('file/delete/{id}', 'ArticleFileController@delete')->name('file.delete');

		Route::get('sbm', 'SbmController@index')->name('sbm');
		Route::get('sbm/create/{id?}', 'SbmController@create')->name('sbm.create');
		Route::post('sbm/store', 'SbmController@store')->name('sbm.store');
		Route::post('sbm/action', 'SbmController@action')->name('sbm.action');
	});

	Route::get('customer', 'CustomerController@index')->name('customer');
	Route::get('customertu', 'CustomerTUController@index')->name('customertu');
	Route::get('customertu/{id}', 'CustomerTUController@read')->name('customertu.read');

	Route::get('laboratory', 'LaboratoryController@index')->name('laboratory');
	Route::get('laboratory/create/{id?}', 'LaboratoryController@create')->name('laboratory.create');
	Route::post('laboratory/store', 'LaboratoryController@store')->name('laboratory.store');
	Route::post('laboratory/action', 'LaboratoryController@action')->name('laboratory.action');

	Route::get('laboratory/getbyid/{id?}', 'LaboratoryController@getbyid')->name('laboratory.getbyid');

	Route::get('instalasi', 'InstalationController@index')->name('instalasi');
	Route::get('instalasi/create/{id?}', 'InstalationController@create')->name('instalasi.create');
	Route::post('instalasi/store', 'InstalationController@store')->name('instalasi.store');
	Route::post('instalasi/action', 'InstalationController@action')->name('instalasi.action');
	Route::post('instalasi/saveKajiUlang', 'InstalationController@saveKajiUlang')->name('instalasi.saveKajiUlang');

	Route::get('holiday', 'HolidayController@index')->name('holiday');
	Route::get('holiday/create/{id?}', 'HolidayController@create')->name('holiday.create');
	Route::post('holiday/store', 'HolidayController@store')->name('holiday.store');
	Route::post('holiday/action', 'HolidayController@action')->name('holiday.action');

	Route::get('petugas', 'PetugasController@index')->name('petugas');
	Route::get('petugas/create/{id?}', 'PetugasController@create')->name('petugas.create');
	Route::post('petugas/store', 'PetugasController@store')->name('petugas.store');
	Route::post('petugas/action', 'PetugasController@action')->name('petugas.action');

	Route::get('measurementtype', 'MeasurementTypeController@index')->name('measurementtype');
	Route::get('measurementtype/create', 'MeasurementTypeController@create')->name('measurementtype.create');
	Route::post('measurementtype/store', 'MeasurementTypeController@store')->name('measurementtype.store');
	// Route::post('measurementtype/inspection_price', 'MeasurementtypeController@inspection_price')->name('measurementtype.inspection_price');
	// Route::post('measurementtype/measurement_type', 'MeasurementtypeController@measurement_type')->name('measurementtype.measurement_type');
	// Route::post('measurementtype/tool_type', 'MeasurementtypeController@tool_type')->name('measurementtype.tool_type');
	// Route::post('measurementtype/detail_type', 'MeasurementtypeController@detail_type')->name('measurementtype.detail_type');

	Route::get('standard', 'StandardController@index')->name("standard");
	Route::get('standard/create/{id?}', 'StandardController@create')->name('standard.create');
	Route::post('standard/store', 'StandardController@store')->name('standard.store');
	Route::post('standard/tool_code', 'StandardController@tool_code')->name('standard.tool_code');
	Route::get('standard/info/{id?}', 'StandardController@info')->name('standard.info');
	Route::post('standard/inspectionpriceinfo', 'StandardController@inspectionpriceinfo')->name('standard.inspectionpriceinfo');
	Route::post('standard/get_tool_code_info', 'StandardController@get_tool_code_info')->name('service.get_tool_code_info');

	Route::get('qrcode', 'QrcodeController@index')->name('qrcode');
	Route::get('qrcode/create/{id?}', 'QrcodeController@create')->name('qrcode.create');
	Route::post('qrcode/store', 'QrcodeController@store')->name('qrcode.store');
	Route::get('qrcode/show', 'QrcodeController@show')->name('qrcode.show');
	Route::post('qrcode/getdata', 'QrcodeController@getdata')->name('qrcode.getdata');
	Route::post('qrcode/info', 'QrcodeController@info')->name('qrcode.info');
	Route::post('qrcode/prints', 'QrcodeController@prints')->name('qrcode.prints');
	Route::post('qrcode/dropdown', 'QrcodeController@dropdown')->name('qrcode.dropdown');

	// CONTROLLER UML
	Route::get('umlstandard', 'UmlStandardController@index')->name('umlstandard');
	Route::get('umlstandard/create', 'UmlStandardController@create')->name('umlstandard.create');
	Route::post('umlstandard/store', 'UmlStandardController@store')->name('umlstandard.store');
	Route::get('umlstandard/proses/{id}/', 'UmlStandardController@proses')->name('umlstandard.proses');
	Route::get('umlstandard/update', 'UmlStandardController@update')->name('umlstandard.update');
	Route::post('umlstandard/edit', 'UmlStandardController@edit')->name('umlstandard.edit');
	Route::get('umlstandard/getumlinfo/{id?}', 'UmlStandardController@getumlinfo')->name('umlstandard.getumlinfo');

	Route::get('request', 'RequestController@index')->name('request');
	Route::get('request/create', 'RequestController@create')->name('request.create');
	Route::get('request/createbooking', 'RequestController@createBooking')->name('request.createbooking');
	Route::post('request/simpan', 'RequestController@simpan')->name('request.simpan');
	Route::post('request/simpanbooking', 'RequestController@simpanBooking')->name('request.simpanbooking');

	Route::get('request/payment/{id}', 'RequestController@payment')->name('request.payment');
	Route::post('request/paymentsave', 'RequestController@paymentsave')->name('request.paymentsave');
	Route::get('request/paymentcancel/{id}', 'RequestController@paymentcancel')->name('request.paymentcancel');
	Route::get('request/pdf/{id}', 'RequestController@pdf')->name('request.pdf');
	Route::get('request/destroy/{id}', 'RequestController@destroy')->name('request.destroy');

	Route::get('requesthistoryuttp', 'RequestHistoryUttpController@index')->name('requesthistoryuttp');
	

	Route::get('requestuttp', 'RequestUttpController@index')->name('requestuttp');
	Route::get('requestuttp/create', 'RequestUttpController@create')->name('requestuttp.create');
	Route::get('requestuttp/createbooking', 'RequestUttpController@createbooking')->name('requestuttp.createbooking');
	Route::post('requestuttp/simpanbooking', 'RequestUttpController@simpanbooking')->name('requestuttp.simpanbooking');
	Route::get('requestuttp/editbooking/{id}', 'RequestUttpController@editbooking')->name('requestuttp.editbooking');
	Route::post('requestuttp/simpaneditbooking/{id}', 'RequestUttpController@simpaneditbooking')->name('requestuttp.simpaneditbooking');
	Route::post('requestuttp/submitbooking/{id}', 'RequestUttpController@submitbooking')->name('requestuttp.submitbooking');
	Route::get('requestuttp/destroy/{id}', 'RequestUttpController@destroy')->name('requestuttp.destroy');

	Route::post('requestuttp/edititem/{id}', 'RequestUttpController@edititem')->name('requestuttp.edititem');
	Route::get('requestuttp/createbookinginspection/{id}', 'RequestUttpController@createbookinginspection')->name('requestuttp.createbookinginspection');
	Route::post('requestuttp/simpanbookinginspection/{id}', 'RequestUttpController@simpanbookinginspection')->name('requestuttp.simpanbookinginspection');
	Route::post('requestuttp/hapusbookinginspection/{id}', 'RequestUttpController@hapusbookinginspection')->name('requestuttp.hapusbookinginspection');
	Route::get('requestuttp/getprices/{id}', 'RequestUttpController@getprices')->name('requestuttp.getprices');
	Route::get('requestuttp/getowner/{id}', 'RequestUttpController@getowner')->name('requestuttp.getowner');
	Route::get('requestuttp/getowners', 'RequestUttpController@getowners')->name('requestuttp.getowners');
	Route::get('requestuttp/getunits/{uttp_type_id}', 'RequestUttpController@getunits')->name('requestuttp.getunits');
	Route::post('requestuttp/saveuttp/{id}', 'RequestUttpController@saveuttp')->name('requestuttp.saveuttp');
	

	Route::get('requestuttp/pdf/{id}', 'RequestUttpController@pdf')->name('requestuttp.pdf');
	Route::get('requestuttp/kuitansi/{id}', 'RequestUttpController@kuitansi')->name('requestuttp.kuitansi');
	Route::get('requestuttp/buktiorder/{id}', 'RequestUttpController@buktiorder')->name('requestuttp.buktiorder');
	Route::get('requestuttp/payment/{id}', 'RequestUttpController@payment')->name('requestuttp.payment');
	Route::post('requestuttp/paymentsave', 'RequestUttpController@paymentsave')->name('requestuttp.paymentsave');
	Route::get('requestuttp/valid/{id}', 'RequestUttpController@valid')->name('requestuttp.valid');
	Route::post('requestuttp/validsave', 'RequestUttpController@validsave')->name('requestuttp.validsave');
	Route::post('requestuttp/novalidsave', 'RequestUttpController@novalidsave')->name('requestuttp.novalidsave');
	Route::post('requestuttp/instalasi', 'RequestUttpController@instalasi')->name('requestuttp.instalasi');
	Route::post('requestuttp/instalasiqr', 'RequestUttpController@instalasiqr')->name('requestuttp.instalasiqr');
	
	Route::get('requestuttp/tag/{id}', 'RequestUttpController@tag')->name('requestuttp.tag');
	Route::get('requestuttp/gethistory/{id}', 'RequestUttpController@gethistory')->name('requestuttp.gethistory');
	
	Route::post('requestuttp/disc/{id}/{price}', 'RequestUttpController@setPotongan')->name('requestuttp.discount');
	

	Route::get('pembayaranluaruttp', 'PembayaranLuarUttpController@index')->name('pembayaranluaruttp');
	Route::get('pembayaranluartuhputtp', 'PembayaranLuarTuhpUttpController@index')->name('pembayaranluartuhputtp');
	Route::get('resumespuhuttp', 'ResumeSpuhUttpController@index')->name('resumespuhuttp');
	Route::get('resumespuhuttp/paid/{id}/{state}', 'ResumeSpuhUttpController@paid')->name('resumespuhuttp.paid');

	Route::get('revisionuttp', 'RevisionUttpController@index')->name('revisionuttp');
	Route::get('revisionuttp/edit/{id}', 'RevisionUttpController@edit')->name('revisionuttp.edit');
	Route::post('revisionuttp/simpanedit/{id}', 'RevisionUttpController@simpanedit')->name('revisionuttp.simpanedit');

	Route::get('revisionorderuttp', 'RevisionOrderUttpController@index')->name('revisionorderuttp');
	Route::get('revisionorderuttp/order/{id}', 'RevisionOrderUttpController@order')->name('revisionorderuttp.order');
	Route::post('revisionorderuttp/simpanorder/{id}', 'RevisionOrderUttpController@simpanorder')->name('revisionorderuttp.simpanorder');

	Route::get('revisionapproveuttp', 'RevisionApprovalUttpController@index')->name('revisionapproveuttp');
	Route::get('revisionapproveuttp/approvesubko/{id}', 'RevisionApprovalUttpController@approvesubko')->name('revisionapproveuttp.approvesubko');
	Route::post('revisionapproveuttp/approvesubmitsubko/{id}', 'RevisionApprovalUttpController@approvesubmitsubko')->name('revisionapproveuttp.approvesubmitsubko');
	Route::get('revisionapproveuttp/approve/{id}', 'RevisionApprovalUttpController@approve')->name('revisionapproveuttp.approve');
	Route::post('revisionapproveuttp/approvesubmit/{id}', 'RevisionApprovalUttpController@approvesubmit')->name('revisionapproveuttp.approvesubmit');
	Route::get('revisionapproveuttp/preview/{id}', 'RevisionApprovalUttpController@preview')->name('revisionapproveuttp.preview');
	Route::get('revisionapproveuttp/print/{id}', 'RevisionApprovalUttpController@print')->name('revisionapproveuttp.print');

	Route::get('revisionapproveuut', 'RevisionApprovalUutController@index')->name('revisionapproveuut');
	Route::get('revisionapproveuut/approvesubko/{id}', 'RevisionApprovalUutController@approvesubko')->name('revisionapproveuut.approvesubko');
	Route::post('revisionapproveuut/approvesubmitsubko/{id}', 'RevisionApprovalUutController@approvesubmitsubko')->name('revisionapproveuut.approvesubmitsubko');
	Route::get('revisionapproveuut/approve/{id}', 'RevisionApprovalUutController@approve')->name('revisionapproveuut.approve');
	Route::post('revisionapproveuut/approvesubmit/{id}', 'RevisionApprovalUutController@approvesubmit')->name('revisionapproveuut.approvesubmit');
	Route::get('revisionapproveuut/preview/{id}', 'RevisionApprovalUutController@preview')->name('revisionapproveuut.preview');
	Route::get('revisionapproveuut/print/{id}', 'RevisionApprovalUutController@print')->name('revisionapproveuut.print');

	Route::get('approveuttp', 'ApproveUttpController@index')->name('approveuttp');
	Route::get('approveuttp/approvekalab/{id}', 'ApproveUttpController@approvekalab')->name('approveuttp.approvekalab');
	Route::post('approveuttp/approvesubmitkalab/{id}', 'ApproveUttpController@approvesubmitkalab')->name('approveuttp.approvesubmitkalab');
	Route::post('approveuttp/approvesubmitkalaball', 'ApproveUttpController@approvesubmitkalaball')->name('approveuttp.approvesubmitkalaball');
	Route::get('approveuttp/approvesubko/{id}', 'ApproveUttpController@approvesubko')->name('approveuttp.approvesubko');
	Route::post('approveuttp/approvesubmitsubko/{id}', 'ApproveUttpController@approvesubmitsubko')->name('approveuttp.approvesubmitsubko');
	Route::post('approveuttp/approvesubmitsubkoall', 'ApproveUttpController@approvesubmitsubkoall')->name('approveuttp.approvesubmitsubkoall');
	Route::get('approveuttp/approve/{id}', 'ApproveUttpController@approve')->name('approveuttp.approve');
	Route::post('approveuttp/approvesubmit/{id}', 'ApproveUttpController@approvesubmit')->name('approveuttp.approvesubmit');
	Route::post('approveuttp/approvesubmitall', 'ApproveUttpController@approvesubmitall')->name('approveuttp.approvesubmitall');
	Route::get('approveuttp/download/{id}', 'ApproveUttpController@download')->name('approveuttp.download');
	Route::get('approveuttp/preview/{id}', 'ApproveUttpController@preview')->name('approveuttp.preview');
	Route::get('approveuttp/print/{id}', 'ApproveUttpController@print')->name('approveuttp.print');
	Route::get('approveuttp/previewTipe/{id}', 'ApproveUttpController@previewTipe')->name('approveuttp.previewTipe');
	Route::get('approveuttp/printTipe/{id}', 'ApproveUttpController@printTipe')->name('approveuttp.printTipe');
	Route::get('approveuttp/previewSETipe/{id}', 'ApproveUttpController@previewSETipe')->name('approveuttp.previewSETipe');
	Route::get('approveuttp/printSETipe/{id}', 'ApproveUttpController@printSETipe')->name('approveuttp.printSETipe');
	Route::get('approveuttp/downloadReview/{type}/{id}', 'ApproveUttpController@downloadReview')->name('approveuttp.downloadReview');

	Route::get('delegationuttp', 'DelegationUttpController@index')->name('delegationuttp');
	Route::get('delegationuttp/create', 'DelegationUttpController@create')->name('delegationuttp.create');
	Route::post('delegationuttp/store', 'DelegationUttpController@store')->name('delegationuttp.store');
	Route::get('delegationuttp/edit/{id}', 'DelegationUttpController@edit')->name('delegationuttp.edit');
	Route::post('delegationuttp/edit/{id}', 'DelegationUttpController@update')->name('delegationuttp.update');
	Route::post('delegationuttp/delete/{id}', 'DelegationUttpController@destroy')->name('delegationuttp.destroy');

	Route::get('approveuut', 'ApproveUutController@index')->name('approveuut');
	Route::get('approveuut/approvekalab/{id}', 'ApproveUutController@approvekalab')->name('approveuut.approvekalab');
	Route::post('approveuut/approvesubmitkalab/{id}', 'ApproveUutController@approvesubmitkalab')->name('approveuut.approvesubmitkalab');
	Route::post('approveuut/approvesubmitkalaball', 'ApproveUutController@approvesubmitkalaball')->name('approveuut.approvesubmitkalaball');
	Route::get('approveuut/approvesubko/{id}', 'ApproveUutController@approvesubko')->name('approveuut.approvesubko');
	Route::post('approveuut/approvesubmitsubko/{id}', 'ApproveUutController@approvesubmitsubko')->name('approveuut.approvesubmitsubko');
	Route::post('approveuut/approvesubmitsubkoall', 'ApproveUutController@approvesubmitsubkoall')->name('approveuut.approvesubmitsubkoall');
	Route::get('approveuut/approve/{id}', 'ApproveUutController@approve')->name('approveuut.approve');
	Route::post('approveuut/approvesubmit/{id}', 'ApproveUutController@approvesubmit')->name('approveuut.approvesubmit');
	Route::post('approveuut/approvesubmitall', 'ApproveUutController@approvesubmitall')->name('approveuut.approvesubmitall');
	Route::get('approveuut/download/{id}', 'ApproveUutController@download')->name('approveuut.download');
	Route::get('approveuut/downloadLampiran/{id}', 'ApproveUutController@downloadLampiran')->name('approveuut.downloadLampiran');
	Route::get('approveuut/preview/{id}', 'ApproveUutController@preview')->name('approveuut.preview');
	Route::get('approveuut/print/{id}', 'ApproveUutController@print')->name('approveuut.print');
	Route::get('approveuut/downloadReview/{type}/{id}', 'ApproveUutController@downloadReview')->name('approveuut.downloadReview');

	Route::get('historyorderuttp', 'HistoryOrderUttpController@index')->name('historyorderuttp');
	Route::get('historyorderuut', 'HistoryOrderUutController@index')->name('historyorderuut');
	
	Route::get('evaluasiorderinsituuttp', 'EvaluasiOrderInsituUttpController@index')->name('evaluasiorderinsituuttp');
	Route::get('evaluasiorderinsituuttp/export', 'EvaluasiOrderInsituUttpController@fileExport')->name('evaluasiorderinsituuttp.export');
	
	Route::get('progressorderuttp', 'ProgressOrderUttpController@index')->name('progressorderuttp');
	
	// Route::get('expiredorderuttp', 'ExpiredOrderUttpController@index')->name('expiredorderuttp');
	// Route::get('expiredorderuttp/notif', 'ExpiredOrderUttpController@notif')->name('expiredorderuttp.notif');
	// Route::get('expiredorderuttp/export', 'ExpiredOrderUttpController@export')->name('expiredorderuttp.export');
	
	Route::get('arsiporderuttp', 'ArsipOrderUttpController@index')->name('arsiporderuttp');
	

	// Ajax route
	Route::post('historyorderuut', 'HistoryOrderUutController@listJson')->name('history.listJson');

	Route::get('historyorderuut/downloadLampiran/{id}', 'HistoryOrderUutController@downloadLampiran')->name('historyorderuut.downloadLampiran');

	Route::get('serviceuttp/download/{id}', 'ServiceUttpController@download')->name('serviceuttp.download');
	Route::get('serviceuttp/print/{id}/{stream?}', 'ServiceUttpController@print')->name('serviceuttp.print');
	Route::get('serviceuttp/printTipe/{id}/{stream?}', 'ServiceUttpController@printTipe')->name('serviceuttp.printTipe');
	Route::get('serviceuttp/printSETipe/{id}/{stream?}', 'ServiceUttpController@printSETipe')->name('serviceuttp.printSETipe');

	Route::get('reportuttp', 'ReportUttpController@index')->name('reportuttp');


	Route::get('firstcheckluaruttp', 'FirstCheckLuarUttpController@index')->name('firstcheckluaruttp');
	Route::get('firstcheckluaruttp/cek/{id}', 'FirstCheckLuarUttpController@check')->name('firstcheckluaruttp.check');
	Route::get('firstcheckluaruttp/downloadbukti/{docId}', 'FirstCheckLuarUttpController@downloadBukti')->name('firstcheckluaruttp.downloadbukti');
	Route::post('firstcheckluaruttp/savecek/{id}', 'FirstCheckLuarUttpController@savecheck')->name('firstcheckluaruttp.savecheck');
	

	Route::get('serviceprocessluaruttp', 'ServiceProcessLuarUttpController@index')->name('serviceprocessluaruttp');
	Route::get('serviceprocessluaruttp/cek/{id}', 'ServiceProcessLuarUttpController@check')->name('serviceprocessluaruttp.check');
	Route::post('serviceprocessluaruttp/savecek/{id}', 'ServiceProcessLuarUttpController@savecheck')->name('serviceprocessluaruttp.savecheck');
	Route::get('serviceprocessluaruttp/createbookinginspection/{id}', 'ServiceProcessLuarUttpController@createbookinginspection')->name('serviceprocessluaruttp.createbookinginspection');
	Route::post('serviceprocessluaruttp/simpanbookinginspection/{id}', 'ServiceProcessLuarUttpController@simpanbookinginspection')->name('serviceprocessluaruttp.simpanbookinginspection');
	Route::post('serviceprocessluaruttp/hapusbookinginspection/{id}', 'ServiceProcessLuarUttpController@hapusbookinginspection')->name('serviceprocessluaruttp.hapusbookinginspection');
	Route::get('serviceprocessluaruttp/newitem/{id}', 'ServiceProcessLuarUttpController@newitem')->name('serviceprocessluaruttp.newitem');
	Route::post('serviceprocessluaruttp/savenewitem/{id}', 'ServiceProcessLuarUttpController@savenewitem')->name('serviceprocessluaruttp.savenewitem');
	Route::get('serviceprocessluaruttp/createitem/{id}/{uttp_type_id}', 'ServiceProcessLuarUttpController@createitem')->name('serviceprocessluaruttp.createitem');
	Route::post('serviceprocessluaruttp/simpanitem/{id}/{uttp_type_id}', 'ServiceProcessLuarUttpController@simpanitem')->name('serviceprocessluaruttp.simpanitem');
	Route::post('serviceprocessluaruttp/getuttps', 'ServiceProcessLuarUttpController@getuttps')->name('serviceprocessluaruttp.getuttps');
	Route::get('serviceprocessluaruttp/edititem/{id}/{idItem}', 'ServiceProcessLuarUttpController@edititem')->name('serviceprocessluaruttp.edititem');
	Route::post('serviceprocessluaruttp/simpanedititem/{id}/{idItem}', 'ServiceProcessLuarUttpController@simpanedititem')->name('serviceprocessluaruttp.simpanedititem');
	Route::post('serviceprocessluaruttp/hapusitem', 'ServiceProcessLuarUttpController@hapusitem')->name('serviceprocessluaruttp.hapusitem');
	Route::get('serviceprocessluaruttp/perlengkapan/{id}', 'ServiceProcessLuarUttpController@perlengkapan')->name('serviceprocessluaruttp.perlengkapan');
	Route::get('serviceprocessluaruttp/newperlengkapan/{id}', 'ServiceProcessLuarUttpController@newperlengkapan')->name('serviceprocessluaruttp.newperlengkapan');
	Route::post('serviceprocessluaruttp/simpanperlengkapan/{id}', 'ServiceProcessLuarUttpController@simpanperlengkapan')->name('serviceprocessluaruttp.simpanperlengkapan');
	Route::post('serviceprocessluaruttp/getunits', 'ServiceProcessLuarUttpController@getunits')->name('serviceprocessluaruttp.getunits');
	Route::get('serviceprocessluaruttp/editperlengkapan/{id}/{idItem}/{idPerlengkapan}', 'ServiceProcessLuarUttpController@editperlengkapan')->name('serviceprocessluaruttp.editperlengkapan');
	Route::post('serviceprocessluaruttp/simpaneditperlengkapan/{id}/{idItem}/{idPerlengkapan}', 'ServiceProcessLuarUttpController@simpaneditperlengkapan')->name('serviceprocessluaruttp.simpaneditperlengkapan');
	Route::post('serviceprocessluaruttp/hapusperlengkapan', 'ServiceProcessLuarUttpController@hapusperlengkapan')->name('serviceprocessluaruttp.hapusperlengkapan');
	Route::post('serviceprocessluaruttp/statusrevisi', 'ServiceProcessLuarUttpController@statusRevisiSPT')->name('serviceprocessluaruttp.statusrevisi');
	
	Route::get('serviceprocessluaruttp/cancel/{id}', 'ServiceProcessLuarUttpController@cancel')->name('serviceprocessluaruttp.cancel');
	Route::post('serviceprocessluaruttp/confirmcancel/{id}', 'ServiceProcessLuarUttpController@confirmcancel')->name('serviceprocessluaruttp.confirmcancel');


	Route::get('doctuinsituuttp', 'DocumentTUInsituUttpController@index')->name('doctuinsituuttp');
	Route::get('doctuinsituuttp/list/{id}', 'DocumentTUInsituUttpController@list')->name('doctuinsituuttp.list');
	Route::get('doctuinsituuttp/create/{id}', 'DocumentTUInsituUttpController@create')->name('doctuinsituuttp.create');
	Route::post('doctuinsituuttp/create/{id}', 'DocumentTUInsituUttpController@store')->name('doctuinsituuttp.store');
	Route::get('doctuinsituuttp/download/{id}/{tipe}', 'DocumentTUInsituUttpController@download')->name('doctuinsituuttp.download');
	Route::get('doctuinsituuttp/edit/{id}', 'DocumentTUInsituUttpController@edit')->name('doctuinsituuttp.edit');
	Route::post('doctuinsituuttp/edit/{id}', 'DocumentTUInsituUttpController@update')->name('doctuinsituuttp.update');


	Route::get('docinsituuttp', 'DocumentInsituUttpController@index')->name('docinsituuttp');
	Route::get('docinsituuttp/approve/{id}', 'DocumentInsituUttpController@approval')->name('docinsituuttp.approval');
	Route::post('docinsituuttp/approve/{id}', 'DocumentInsituUttpController@approve')->name('docinsituuttp.approve');
	Route::get('docinsituuttp/generate-token', 'DocumentInsituUttpController@generateToken')->name('docinsituuttp.generate');
	Route::get('docinsituuttp/integritas/{id}', 'DocumentInsituUttpController@integritas')->name('docinsituuttp.integritas');
	
	Route::get('redocinsituuttp', 'ReDocumentInsituUttpController@index')->name('redocinsituuttp');
	Route::get('redocinsituuttp/approve/{id}', 'ReDocumentInsituUttpController@approval')->name('redocinsituuttp.approval');
	Route::post('redocinsituuttp/approve/{id}', 'ReDocumentInsituUttpController@approve')->name('redocinsituuttp.approve');
	Route::get('redocinsituuttp/generate-token', 'ReDocumentInsituUttpController@generateToken')->name('redocinsituuttp.generate');
	

	Route::get('orderinsituuttp', 'OrderInsituUttpController@index')->name('orderinsituuttp');
	Route::get('orderinsituuttp/order/{id}', 'OrderInsituUttpController@order')->name('orderinsituuttp.order');
	Route::post('orderinsituuttp/order/{id}', 'OrderInsituUttpController@saveorder')->name('orderinsituuttp.saveorder');


	Route::get('serviceluaruttp/test/{id}', 'ServiceLuarUttpController@test')->name('serviceluaruttp.test');
	Route::post('serviceluaruttp/savetest/{id}', 'ServiceLuarUttpController@savetest')->name('serviceluaruttp.savetest');
	//Route::get('serviceluaruttp/result/{id}', 'ServiceLuarUttpController@result')->name('serviceluaruttp.result');
	//Route::post('serviceluaruttp/resultupload/{id}', 'ServiceLuarUttpController@resultupload')->name('serviceluaruttp.resultupload');
	
	//Route::get('serviceluaruttp/proses/{id}', 'ServiceLuarUttpController@proses')->name('serviceluaruttp.proses');
	//Route::post('serviceluaruttp/saveproses/{id}', 'ServiceLuarUttpController@saveproses')->name('serviceluaruttp.saveproses');
	Route::get('serviceluaruttp/pending/{id}', 'ServiceLuarUttpController@pending')->name('serviceluaruttp.pending');
	Route::post('serviceluaruttp/savepending/{id}', 'ServiceLuarUttpController@savepending')->name('serviceluaruttp.savepending');
	Route::get('serviceluaruttp/continue/{id}', 'ServiceLuarUttpController@continue')->name('serviceluaruttp.continue');
	Route::post('serviceluaruttp/savecontinue/{id}', 'ServiceLuarUttpController@savecontinue')->name('serviceluaruttp.savecontinue');
	Route::get('serviceluaruttp/cancel/{id}', 'ServiceLuarUttpController@cancel')->name('serviceluaruttp.cancel');
	Route::post('serviceluaruttp/savecancel/{id}', 'ServiceLuarUttpController@savecancel')->name('serviceluaruttp.savecancel');

	Route::get('serviceluaruttp/createbookinginspection/{id}', 'ServiceLuarUttpController@createbookinginspection')->name('serviceluaruttp.createbookinginspection');
	Route::post('serviceluaruttp/simpanbookinginspection/{id}', 'ServiceLuarUttpController@simpanbookinginspection')->name('serviceluaruttp.simpanbookinginspection');
	Route::post('serviceluaruttp/hapusbookinginspection/{id}', 'ServiceLuarUttpController@hapusbookinginspection')->name('serviceluaruttp.hapusbookinginspection');
	
	Route::get('serviceluaruttp/perlengkapan/{id}', 'ServiceLuarUttpController@perlengkapan')->name('serviceluaruttp.perlengkapan');
	Route::get('serviceluaruttp/newperlengkapan/{id}', 'ServiceLuarUttpController@newperlengkapan')->name('serviceluaruttp.newperlengkapan');
	Route::post('serviceluaruttp/simpanperlengkapan/{id}', 'ServiceLuarUttpController@simpanperlengkapan')->name('serviceluaruttp.simpanperlengkapan');
	Route::get('serviceluaruttp/editperlengkapan/{id}/{idItem}/{idPerlengkapan}', 'ServiceLuarUttpController@editperlengkapan')->name('serviceluaruttp.editperlengkapan');
	Route::post('serviceluaruttp/simpaneditperlengkapan/{id}/{idItem}/{idPerlengkapan}', 'ServiceLuarUttpController@simpaneditperlengkapan')->name('serviceluaruttp.simpaneditperlengkapan');
	Route::post('serviceluaruttp/hapusperlengkapan', 'ServiceLuarUttpController@hapusperlengkapan')->name('serviceluaruttp.hapusperlengkapan');
	

	Route::get('requestluaruttp', 'RequestLuarUttpController@index')->name('requestluaruttp');
	Route::get('requestluaruttp/proses/{id}', 'RequestLuarUttpController@proses')->name('requestluaruttp.proses');
	Route::post('requestluaruttp/saveproses/{id}', 'RequestLuarUttpController@saveproses')->name('requestluaruttp.saveproses');
	Route::get('requestluaruttp/selesai/{id}', 'RequestLuarUttpController@selesai')->name('requestluaruttp.selesai');
	Route::post('requestluaruttp/saveselesai/{id}', 'RequestLuarUttpController@saveselesai')->name('requestluaruttp.saveselesai');
	Route::get('requestluaruttp/createbookinginspection/{id}', 'RequestLuarUttpController@createbookinginspection')->name('requestluaruttp.createbookinginspection');
	Route::post('requestluaruttp/simpanbookinginspection/{id}', 'RequestLuarUttpController@simpanbookinginspection')->name('requestluaruttp.simpanbookinginspection');
	Route::post('requestluaruttp/hapusbookinginspection/{id}', 'RequestLuarUttpController@hapusbookinginspection')->name('requestluaruttp.hapusbookinginspection');
	Route::get('requestluaruttp/payment/{id}', 'RequestLuarUttpController@payment')->name('requestluaruttp.payment');
	Route::post('requestluaruttp/paymentsave', 'RequestLuarUttpController@paymentsave')->name('requestluaruttp.paymentsave');
	Route::get('requestluaruttp/valid/{id}', 'RequestLuarUttpController@valid')->name('requestluaruttp.valid');
	Route::post('requestluaruttp/validsave', 'RequestLuarUttpController@validsave')->name('requestluaruttp.validsave');
	Route::post('requestluaruttp/novalidsave', 'RequestLuarUttpController@novalidsave')->name('requestluaruttp.novalidsave');
	Route::get('requestluaruttp/pending/{id}', 'RequestLuarUttpController@pending')->name('requestluaruttp.pending');
	Route::post('requestluaruttp/savepending/{id}', 'RequestLuarUttpController@savepending')->name('requestluaruttp.savepending');
	Route::get('requestluaruttp/continue/{id}', 'RequestLuarUttpController@continue')->name('requestluaruttp.continue');
	Route::post('requestluaruttp/savecontinue/{id}', 'RequestLuarUttpController@savecontinue')->name('requestluaruttp.savecontinue');
	Route::get('requestluaruttp/cancel/{id}', 'RequestLuarUttpController@cancel')->name('requestluaruttp.cancel');
	Route::post('requestluaruttp/savecancel/{id}', 'RequestLuarUttpController@savecancel')->name('requestluaruttp.savecancel');

	Route::get('requestluaruttp/pdf/{id}', 'RequestLuarUttpController@pdf')->name('requestluaruttp.pdf');
	Route::get('requestluaruttp/pdftuhp/{id}', 'RequestLuarUttpController@pdftuhp')->name('requestluaruttp.pdftuhp');
	Route::get('requestluaruttp/paymenttuhp/{id}', 'RequestLuarUttpController@paymenttuhp')->name('requestluaruttp.paymenttuhp');
	Route::post('requestluaruttp/paymenttuhpsave', 'RequestLuarUttpController@paymenttuhpsave')->name('requestluaruttp.paymenttuhpsave');

	// set potongan
	Route::post('requestluaruttp/disc/{id}/{price}', 'RequestLuarUttpController@setPotongan')->name('requestluaruttp.discount');
	
	Route::get('requestluaruttp/validtuhp/{id}', 'RequestLuarUttpController@validtuhp')->name('requestluaruttp.validtuhp');
	Route::post('requestluaruttp/validtuhpsave', 'RequestLuarUttpController@validtuhpsave')->name('requestluaruttp.validtuhpsave');
	Route::post('requestluaruttp/novalidtuhpsave', 'RequestLuarUttpController@novalidtuhpsave')->name('requestluaruttp.novalidtuhpsave');

	Route::get('requestluaruttp/extend/{id}', 'RequestLuarUttpController@extend')->name('requestluaruttp.extend');
	Route::post('requestluaruttp/saveextend/{id}', 'RequestLuarUttpController@saveextend')->name('requestluaruttp.saveextend');

	Route::get('requestluaruttp/printspt/{id}/{stream?}', 'RequestLuarUttpController@printspt')->name('requestluaruttp.printspt');

	Route::get('requestluaruttp/editpnbp/{id}', 'RequestLuarUttpController@editpnbp')->name('requestluaruttp.editpnbp');
	
	Route::get('petugasdl', 'PetugasDLController@index')->name('petugasdl');
	

	Route::get('schedulinguttp', 'SchedulingUttpController@index')->name('schedulinguttp');
	Route::get('schedulinguttp/schedule/{id}', 'SchedulingUttpController@schedule')->name('schedulinguttp.schedule');
	Route::post('schedulinguttp/schedule/{id}', 'SchedulingUttpController@confirmschedule')->name('schedulinguttp.confirmschedule');
	Route::get('schedulinguttp/staff', 'SchedulingUttpController@getstaffes')->name('schedulinguttp.staff');
	Route::get('schedulinguttp/surattugas/{id}/{stream?}/{docId?}', 'SchedulingUttpController@print')->name('schedulinguttp.surattugas');
	Route::get('schedulinguttp/cancel/{id}', 'SchedulingUttpController@cancel')->name('schedulinguttp.cancel');
	
	Route::get('reschedulinguttp', 'RevisiSchedulingUttpController@index')->name('reschedulinguttp');
	Route::get('reschedulinguttp/schedule/{id}', 'RevisiSchedulingUttpController@schedule')->name('reschedulinguttp.schedule');
	Route::post('reschedulinguttp/schedule/{id}', 'RevisiSchedulingUttpController@confirmschedule')->name('reschedulinguttp.confirmschedule');
	
	Route::get('histdocinsituuttp', 'HistoryDocumentInsituUttpController@index')->name('histdocinsituuttp');
	Route::get('histdocinsituuttp/list/{id}', 'HistoryDocumentInsituUttpController@list')->name('histdocinsituuttp.list');
	Route::get('histdocinsituuttp/download/{id}/{tipe}', 'HistoryDocumentInsituUttpController@download')->name('histdocinsituuttp.download');
	

	Route::get('resumeuttp', 'ResumeUttpController@index')->name('resumeuttp');
	Route::get('rekapresumeuttp', 'RekapResumeUttpController@index')->name('rekapresumeuttp');

	Route::get('dashboardtu', 'DashboardTUController@index')->name('dashboardtu');
	Route::get('dashboardtusnsu', 'DashboardTUSnsuController@index')->name('dashboardtusnsu');
	Route::get('dashboardmonitoruttp', 'DashboardMonitorUttpController@index')->name('dashboardmonitoruttp');
	Route::get('dashboardmonitoruttp/details', 'DashboardMonitorUttpController@details')->name('dashboardmonitoruttp.details');
	Route::get('dashboardmonitoruut', 'DashboardMonitorUutController@index')->name('dashboardmonitoruut');
	Route::get('dashboardwarehouseuttp', 'DashboardWarehouseUttpController@index')->name('dashboardwarehouseuttp');
	Route::get('dashboardwarehouseuut', 'DashboardWarehouseUutController@index')->name('dashboardwarehouseuut');
	Route::get('dashboardmonitoruut/details', 'DashboardMonitorUutController@details')->name('dashboardmonitoruut.details');

	Route::get('warehouseuttp', 'WarehouseUttpController@index')->name('warehouseuttp');
	Route::post('warehouseuttp/warehouse', 'WarehouseUttpController@warehouse')->name('warehouseuttp.warehouse');
	Route::post('warehouseuttp/getdata', 'WarehouseUttpController@getdata')->name('warehouseuttp.getdata');
	
	Route::get('warehousereceiveuttp', 'WarehouseReceiveUttpController@index')->name('warehousereceiveuttp');
	Route::post('warehousereceiveuttp/warehouse', 'WarehouseReceiveUttpController@warehouse')->name('warehousereceiveuttp.warehouse');
	Route::get('warehousehistoryuttp', 'WarehouseHistoryUttpController@index')->name('warehousehistoryuttp');
	Route::get('warehousehistoryuttp/print/{id}', 'WarehouseHistoryUttpController@print')->name('warehousehistoryuttp.print');
	
	/* Warehouse uut*/ 
	Route::get('warehouseuut', 'WarehouseUutController@index')->name('warehouseuut');
	Route::post('warehouseuut/warehouse', 'WarehouseUutController@warehouse')->name('warehouseuut.warehouse');
	Route::post('warehouseuut/getdata', 'WarehouseUutController@getdata')->name('warehouseuut.getdata');
	// Ajax route
	Route::post('warehouseuut/list-data', 'WarehouseUutController@listData')->name('warehouseuut.listData');
	
	Route::get('warehousereceiveuut', 'WarehouseReceiveUutController@index')->name('warehousereceiveuut');
	Route::post('warehousereceiveuut/warehouse', 'WarehouseReceiveUutController@warehouse')->name('warehousereceiveuut.warehouse');
	Route::get('warehousehistoryuut', 'WarehouseHistoryUutController@index')->name('warehousehistoryuut');
	Route::get('warehousehistoryuut/print/{id}', 'WarehouseHistoryUutController@print')->name('warehousehistoryuut.print');
	

	Route::get('revisiontechorderuttp/download/{id}', 'RevisionTechOrderUttpController@download')->name('revisiontechorderuttp.download');
	Route::get('revisiontechorderuttp/print/{id}/{stream?}', 'RevisionTechOrderUttpController@print')->name('revisiontechorderuttp.print');
	Route::get('revisiontechorderuttp/printTipe/{id}/{stream?}', 'RevisionTechOrderUttpController@printTipe')->name('revisiontechorderuttp.printTipe');
	Route::get('revisiontechorderuttp/printSETipe/{id}/{stream?}', 'RevisionTechOrderUttpController@printSETipe')->name('revisiontechorderuttp.printSETipe');

	Route::get('vieweruttp', 'ViewerUttpController@index')->name('vieweruttp');

	Route::get('serviceluaruttp', 'ServiceLuarUttpController@index')->name('serviceluaruttp');
	Route::get('serviceluaruttp/result/{id}', 'ServiceLuarUttpController@result')->name('serviceluaruttp.result');
	Route::post('serviceluaruttp/resultupload/{id}', 'ServiceLuarUttpController@resultupload')->name('serviceluaruttp.resultupload');
	Route::get('serviceluaruttp/editalat/{id}', 'ServiceLuarUttpController@editalat')->name('serviceluaruttp.editalat');
	Route::post('serviceluaruttp/saveeditalat/{id}', 'ServiceLuarUttpController@saveeditalat')->name('serviceluaruttp.saveeditalat');

	Route::get('servicerevisionluaruttp', 'ServiceRevisionLuarUttpController@index')->name('servicerevisionluaruttp');
	Route::get('servicerevisionluaruttp/result/{id}', 'ServiceRevisionLuarUttpController@result')->name('servicerevisionluaruttp.result');
	Route::post('servicerevisionluaruttp/resultupload/{id}', 'ServiceRevisionLuarUttpController@resultupload')->name('servicerevisionluaruttp.resultupload');
		
	Route::get('servicerevisionluaruttp/perlengkapan/{id}', 'ServiceRevisionLuarUttpController@perlengkapan')->name('servicerevisionluaruttp.perlengkapan');
	Route::get('servicerevisionluaruttp/newperlengkapan/{id}', 'ServiceRevisionLuarUttpController@newperlengkapan')->name('servicerevisionluaruttp.newperlengkapan');
	Route::post('servicerevisionluaruttp/simpanperlengkapan/{id}', 'ServiceRevisionLuarUttpController@simpanperlengkapan')->name('servicerevisionluaruttp.simpanperlengkapan');
	Route::get('servicerevisionluaruttp/editperlengkapan/{id}/{idItem}/{idPerlengkapan}', 'ServiceRevisionLuarUttpController@editperlengkapan')->name('servicerevisionluaruttp.editperlengkapan');
	Route::post('servicerevisionluaruttp/simpaneditperlengkapan/{id}/{idItem}/{idPerlengkapan}', 'ServiceRevisionLuarUttpController@simpaneditperlengkapan')->name('servicerevisionluaruttp.simpaneditperlengkapan');
	Route::post('servicerevisionluaruttp/hapusperlengkapan', 'ServiceRevisionLuarUttpController@hapusperlengkapan')->name('servicerevisionluaruttp.hapusperlengkapan');
	


	Route::get('servicehistoryluaruttp', 'ServiceHistoryLuarUttpController@index')->name('servicehistoryluaruttp');
		
	Route::middleware(['CheckStaff'])->group(function ()
	{
		Route::get('order', 'OrderController@index')->name('order');
		Route::get('order/proses/{item}/{request}/', 'OrderController@proses')->name('order.proses');

		Route::get('service', 'ServiceController@index')->name('service');
		Route::get('service/create', 'ServiceController@create')->name('service.create');
		Route::get('service/result/{id}', 'ServiceController@result')->name('service.result');
		Route::post('service/simpan', 'ServiceController@simpan')->name('service.simpan');
		Route::post('service/recount', 'ServiceController@recount')->name('service.recount');
		Route::get('service/finish/{id}', 'ServiceController@finish')->name('service.finish');
		Route::get('service/warehouse/{id}', 'ServiceController@warehouse')->name('service.warehouse');
		Route::get('service/history', 'ServiceController@history')->name('service.history');
		Route::get('service/sertifikat/{id}/', 'TestedController@sertifikat')->name('service.sertifikat');

		Route::get('receiveuttp', 'ReceiveUttpController@index')->name('receiveuttp');
		//Route::get('receiveuttp/proses/{id}', 'ReceiveUttpController@proses')->name('receiveuttp.proses');
		Route::post('receiveuttp/proses', 'ReceiveUttpController@proses')->name('receiveuttp.proses');
		
		Route::get('orderuttp', 'OrderUttpController@index')->name('orderuttp');
		//Route::get('orderuttp/proses/{id}', 'OrderUttpController@proses')->name('orderuttp.proses');
		Route::post('orderuttp/proses', 'OrderUttpController@proses')->name('orderuttp.proses');
		Route::get('orderuttp/getinspections/{id}', 'OrderUttpController@getinspections')->name('orderuttp.getinspections');

		Route::get('serviceuttp', 'ServiceUttpController@index')->name('serviceuttp');
		Route::get('serviceuttp/test/{id}', 'ServiceUttpController@test')->name('serviceuttp.test');
		Route::post('serviceuttp/savetest/{id}', 'ServiceUttpController@savetest')->name('serviceuttp.savetest');
		Route::post('serviceuttp/savetestqr', 'ServiceUttpController@savetestqr')->name('serviceuttp.savetestqr');
		Route::get('serviceuttp/result/{id}', 'ServiceUttpController@result')->name('serviceuttp.result');
		Route::post('serviceuttp/resultupload/{id}', 'ServiceUttpController@resultupload')->name('serviceuttp.resultupload');
		Route::get('serviceuttp/warehouse/{id}', 'ServiceUttpController@warehouse')->name('serviceuttp.warehouse');
		
		Route::get('serviceuttp/preview/{id}', 'ServiceUttpController@preview')->name('serviceuttp.preview');
		//Route::get('serviceuttp/print/{id}', 'ServiceUttpController@print')->name('serviceuttp.print');
		Route::get('serviceuttp/previewTipe/{id}', 'ServiceUttpController@previewTipe')->name('serviceuttp.previewTipe');
		Route::get('serviceuttp/previewSETipe/{id}', 'ServiceUttpController@previewSETipe')->name('serviceuttp.previewSETipe');
		
		Route::get('serviceuttp/pending/{id}', 'ServiceUttpController@pending')->name('serviceuttp.pending');
		Route::post('serviceuttp/confirmpending/{id}', 'ServiceUttpController@confirmpending')->name('serviceuttp.confirmpending');
		Route::get('serviceuttp/continue/{id}', 'ServiceUttpController@continue')->name('serviceuttp.continue');
		Route::post('serviceuttp/confirmcontinue/{id}', 'ServiceUttpController@confirmcontinue')->name('serviceuttp.confirmcontinue');

		Route::get('serviceuttp/cancel/{id}', 'ServiceUttpController@cancel')->name('serviceuttp.cancel');
		Route::post('serviceuttp/confirmcancel/{id}', 'ServiceUttpController@confirmcancel')->name('serviceuttp.confirmcancel');


		
		Route::get('servicerevisionuttp', 'ServiceRevisionUttpController@index')->name('servicerevisionuttp');
		Route::get('servicerevisionuttp/result/{id}', 'ServiceRevisionUttpController@result')->name('servicerevisionuttp.result');
		Route::post('servicerevisionuttp/resultupload/{id}', 'ServiceRevisionUttpController@resultupload')->name('servicerevisionuttp.resultupload');
		
		Route::get('servicerevisionuttp/cancel/{id}', 'ServiceRevisionUttpController@cancel')->name('servicerevisionuttp.cancel');
		Route::post('servicerevisionuttp/confirmcancel/{id}', 'ServiceRevisionUttpController@confirmcancel')->name('servicerevisionuttp.confirmcancel');


		
		Route::get('servicehistoryuttp', 'ServiceHistoryUttpController@index')->name('servicehistoryuttp');
		Route::post('servicehistoryuttp/warehouse', 'ServiceHistoryUttpController@warehouse')->name('servicehistoryuttp.warehouse');

		

		Route::get('scheduleconfirmuttp', 'ScheduleConfirmUttpController@index')->name('scheduleconfirmuttp');
		Route::get('scheduleconfirmuttp/schedule/{id}', 'ScheduleConfirmUttpController@schedule')->name('scheduleconfirmuttp.schedule');
		Route::post('scheduleconfirmuttp/schedule/{id}', 'ScheduleConfirmUttpController@confirmschedule')->name('scheduleconfirmuttp.confirmschedule');
		
		Route::get('revisiontechorderuttp', 'RevisionTechOrderUttpController@index')->name('revisiontechorderuttp');
		Route::get('revisiontechorderuttp/proses/{id}', 'RevisionTechOrderUttpController@proses')->name('revisiontechorderuttp.proses');
		Route::post('revisiontechorderuttp/simpanproses/{id}', 'RevisionTechOrderUttpController@simpanproses')->name('revisiontechorderuttp.simpanproses');

		
	});

	Route::middleware(['CheckKalab'])->group(function ()
	{
		Route::get('tested', 'TestedController@index')->name('tested');
		Route::get('tested/proses/{id}/', 'TestedController@proses')->name('tested.proses');
		Route::get('tested/cancel/{id}/', 'TestedController@CancelHasilUji')->name('tested.cancel');
		Route::get('tested/history/', 'TestedController@history')->name('tested.history');
		Route::get('tested/sertifikat/{id}/', 'TestedController@sertifikat')->name('tested.sertifikat');
	});

	Route::middleware(['CheckDitmet'])->group(function ()
	{
		Route::get('finished', 'FinishedController@index')->name('finished');
		Route::get('finished/history/', 'FinishedController@history')->name('finished.history');
		Route::get('finished/proses/{id}/', 'FinishedController@proses')->name('finished.proses');
		Route::get('finished/sertifikat/{id}/', 'TestedController@sertifikat')->name('finished.sertifikat');
	});

	/*
	Route::middleware(['CheckKoordinator'])->group(function ()
	{
		Route::get('schedulinguttp', 'SchedulingUttpController@index')->name('schedulinguttp');
		Route::get('schedulinguttp/schedule/{id}', 'SchedulingUttpController@schedule')->name('schedulinguttp.schedule');
		Route::post('schedulinguttp/schedule/{id}', 'SchedulingUttpController@confirmschedule')->name('schedulinguttp.confirmschedule');
	});
	*/

	// MODEL
	Route::get('labseries', function(MasterLaboratorySeries $db)
	{
		return $db->dropdown();
	})->name('labseries');

	Route::get('measurementtype/dropdown', function(StandardMeasurementType $db)
	{
		return $db->dropdown("ajax");
	})->name('measurementtypedropdown');

	Route::get('tooltype/dropdown/{id?}', function(StandardToolType $db, $id)
	{
		return $db->dropdown($id);
	})->name('tooltypedropdown');

	Route::get('detailtype/dropdown/{id?}', function(StandardDetailType $db, $id)
	{
		return $db->dropdown($id);
	})->name('detailtypedropdown');

	Route::get('iprice/dropdown/{id?}', function($id, StandardInspectionPrice $db)
	{
		return $db->dropdown($id);
	})->name('ipricedropdown');

	Route::get('templates', 'TemplatesController@index')->name('templates');
	Route::get('templates/create', 'TemplatesController@create')->name('templates.create');

	//Route::get('frontdesk', 'FrontdeskController@index')->name('frontdesk');

	Route::post('umlsub/dropdown', 'UmlSubController@dropdown')->name('umlsub.dropdown');

	//Route::get('detailtype', 'DetailTypeController@index')->name('detailtype');

	// Route::resource('uml', 'UMLController');
	//Route::resource('laporan', 'LaporanController');

	// CONTROLLER SERVICE REQUEST
	Route::get('/service/select/{id}/{serial_number?}', 'ServiceRequestController@select')->name('service.select');
	Route::get('/service/standard/{id}', 'ServiceRequestController@standard')->name('service.standard');
	Route::get('/service/request/{serial_number}', 'ServiceRequestController@request')->name('service.request');

	// Route::get('/service/payment/{id}', 'ServiceRequestController@payment')->name('service.payment');
	// Route::get('/requestpdf/{id}', 'ServiceRequestController@requestpdf')->name('service.requestpdf');
	// Route::get('/orderpdf/{id}', 'ServiceRequestController@orderpdf')->name('service.orderpdf');
	// Route::get('/payment/{id}', 'ServiceRequestController@payment')->name('service.payment');
	// Route::post('/service/confirm/{id}', 'ServiceRequestController@confirm')->name('service.confirm');
	// Route::post('/service/search', 'ServiceRequestController@search')->name('service.search');
	// Route::post('/service/additem', 'ServiceRequestController@additem')->name('service.additem');

	// Route::get('/stafindex', 'ServiceRequestController@stafindex')->name('service.stafindex');
	// Route::get('/labin_simpan/{item}/{request}/', 'ServiceRequestController@labin_simpan')->name('service.labin_simpan');
	// Route::get('/stafproses_finish/{id}/', 'ServiceRequestController@stafproses_finish')->name('service.stafproses_finish');
	// Route::get('/stafproses_result/{id}', 'ServiceRequestController@stafproses_result')->name('service.stafproses_result');
	// Route::get('/stafproses_send/{id}/', 'ServiceRequestController@stafproses_send')->name('service.stafproses_send');
	# Route::get('/stafhistory/', 'ServiceRequestController@stafhistory')->name('service.stafhistory');
	// Route::get('/tuindex/', 'ServiceRequestController@tuindex')->name('service.tuindex');

	// Route::get('/ditmet_approve', 'ServiceRequestController@ditmet_approve')->name('service.ditmet_approve');
	// Route::get('/ditmet_simpan/{id}/', 'ServiceRequestController@ditmet_simpan')->name('service.ditmet_simpan');

	// Route::get('/sertifikat_pdf/{id}', 'ServiceRequestController@sertifikat_pdf')->name('service.sertifikat_pdf');

	// CONTROLLER STANDARD
	// Route::get('/masterdata', 'StandardController@masterdata')->name('standard.masterdata');
	// Route::post('/standard/simpan', 'StandardController@simpan')->name('standard.simpan');
	// Route::get('/masterlab', 'StandardController@masterlab')->name('standard.masterlab');
	// Route::get('/masterlab_create', 'StandardController@masterlab_create')->name('standard.masterlab_create');

	// Route::get('/uttps', 'LaporanController@uttps')->name('laporan.uttps');

	Route::get('booking/byid/{id?}', 'ServiceBookingController@getbyid')->name('servicebooking.getbyid');
	Route::get('insitems', 'UttpInspetionItems@index')->name('insitems');
	Route::get('insitems/create', 'UttpInspetionItems@create')->name('insitems.create');
	Route::post('insitems/store', 'UttpInspetionItems@store')->name('insitems.store');
	Route::post('insitems/edit', 'UttpInspetionItems@edit')->name('insitems.edit');
	Route::get('insitems/delete/{id}', 'UttpInspetionItems@delete')->name('insitems.delete');

	#inspection price route
	Route::get('insprice', 'UttpInspetionPriceController@index')->name('insprice');
	Route::get('insprice/create', 'UttpInspetionPriceController@create')->name('insprice.create');
	Route::get('insprice/store', 'UttpInspetionPriceController@store')->name('insprice.store');
	Route::get('insprice/edit/{id}', 'UttpInspetionPriceController@edit')->name('insprice.edit');
	Route::patch('insprice/update/{id}', 'UttpInspetionPriceController@update')->name('insprice.update');
	Route::get('insprice/delete/{id}', 'UttpInspetionPriceController@delete')->name('insprice.delete');

	#inspection price range
	#Route::get('inspricerange', 'UttpInspetionPriceRangesController@index')->name('inspricerange');
	#Route::get('inspricerange/create', 'UttpInspetionPriceRangesController@create')->name('inspricerange.create');
	#Route::post('inspricerange/store', 'UttpInspetionPriceRangesController@store')->name('inspricerange.store');

	#inspection price route
	Route::get('uttpowners', 'UttpOwnersController@index')->name('uttpowners');
	Route::get('uttpowners/create', 'UttpOwnersController@create')->name('uttpowners.create');
	Route::get('uttpowners/store', 'UttpOwnersController@store')->name('uttpowners.store');
	Route::get('uttpowners/edit/{id}', 'UttpOwnersController@edit')->name('uttpowners.edit');
	Route::patch('uttpowners/update/{id}', 'UttpOwnersController@update')->name('uttpowners.update');
	Route::get('uttpowners/delete/{id}', 'UttpOwnersController@delete')->name('uttpowners.delete');

	#Standard inspection price
	Route:: get('standardinspectionprices','StandardInspectionPricesController@index')->name('standardprices');
	Route:: get('standardinspectionprices/create','StandardInspectionPricesController@create')->name('standardprices.create');
	Route:: get('standardinspectionprices/store','StandardInspectionPricesController@store')->name('standardprices.store');

	#Survey Question
	Route :: get('question', 'SurveyQuestionController@index')->name('question');
	Route :: get('question/create', 'SurveyQuestionController@create')->name('question.create');
	Route :: get('question/store', 'SurveyQuestionController@store')->name('question.store');
	Route :: get('question/edit/{id}', 'SurveyQuestionController@edit')->name('question.edit');
	Route :: patch('question/update/{id}', 'SurveyQuestionController@update')->name('question.update');
	Route :: get('question/delete/{id}', 'SurveyQuestionController@delete')->name('question.delete');

	Route :: get('surveyresult', 'SurveyResultController@index')->name('surveyresult');
	Route :: get('surveyresult/query/{id}', 'SurveyResultController@query')->name('surveyresult.query');
	Route :: get('surveyresult/export', 'SurveyResultController@export')->name('surveyresult.export');

	Route :: get('surveyresultuut', 'SurveyResultUutController@index')->name('surveyresultuut');
	Route :: get('surveyresultuut/query/{id}', 'SurveyResultUutController@query')->name('surveyresultuut.query');
	Route :: get('surveyresultuut/export', 'SurveyResultUutController@export')->name('surveyresultuut.export');

	#Master uttp Sla
	Route :: get('uttpsla', 'MasterUttpSlaController@index')->name('uttpsla');
	Route :: get('uttpsla/create', 'MasterUttpSlaController@create')->name('uttpsla.create');
	Route :: get('uttpsla/store', 'MasterUttpSlaController@store')->name('uttpsla.store');
	Route :: get('uttpsla/edit/{id}', 'MasterUttpSlaController@edit')->name('uttpsla.edit');
	Route :: patch('uttpsla/update/{id}', 'MasterUttpSlaController@update')->name('uttpsla.update');
	Route :: get('uttpsla/delete/{id}', 'MasterUttpSlaController@delete')->name('uttpsla.delete');
	
	#Master Uttp Types
	Route :: get('uttptype', 'MasterUttpTypeController@index')->name('uttptype');
	Route :: get('uttptype/create', 'MasterUttpTypeController@create')->name('uttptype.create');
	Route :: get('uttptype/store', 'MasterUttpTypeController@store')->name('uttptype.store');
	Route :: get('uttptype/edit/{id}', 'MasterUttpTypeController@edit')->name('uttptype.edit');
	Route :: patch('uttptype/update/{id}', 'MasterUttpTypeController@update')->name('uttptype.update');
	Route :: get('uttptype/delete/{id}', 'MasterUttpTypeController@delete')->name('uttptype.delete');
	Route :: post('uttptype/saveKajiUlang', 'MasterUttpTypeController@saveKajiUlang')->name('uttptype.saveKajiUlang');

	#Master Uttp Units
	Route :: get('uttpunit', 'MasterUttpUnitController@index')->name('uttpunit');
	Route :: get('uttpunit/create', 'MasterUttpUnitController@create')->name('uttpunit.create');
	Route :: get('uttpunit/store', 'MasterUttpUnitController@store')->name('uttpunit.store');
	Route :: get('uttpunit/edit/{id}', 'MasterUttpUnitController@edit')->name('uttpunit.edit');
	Route :: patch('uttpunit/update/{id}', 'MasterUttpUnitController@update')->name('uttpunit.update');
	Route :: get('uttpunit/delete/{id}', 'MasterUttpUnitController@delete')->name('uttpunit.delete');

	#for import export
	Route::get('file-import-export', 'ResumeUttpController@fileImportExport')->name('fileImportExport');
	Route::post('file-import', 'ResumeUttpController@fileImportImport')->name('file-import');
	Route::get('file-export', 'ResumeUttpController@fileExport')->name('file-export');
	Route::get('export', 'ResumeUttpController@export')->name('export');
	Route::get('resume-spuh-export', 'ResumeSpuhUttpController@fileExport')->name('resume-spuh-export');
	

	#Export UUT(SNSU)
	#for import export
	Route::get('_file-import-export', 'ResumeUutController@fileImportExport')->name('_fileImportExport');
	Route::post('_file-import', 'ResumeUutController@fileImportImport')->name('_file-import');
	Route::get('_file-export', 'ResumeUutController@fileExport')->name('_file-export');
	Route::get('_export', 'ResumeUutController@export')->name('_export');
	Route::get('_resume-spuh-export', 'ResumeSpuhUutController@fileExport')->name('_resume-spuh-export');
	

	#for request uut Routes
	Route::get('requestuut', 'RequestUutController@index')->name('requestuut');
	Route::get('requestuut/create', 'RequestUutController@create')->name('requestuut.create');
	Route::get('requestuut/createbooking', 'RequestUutController@createbooking')->name('requestuut.createbooking');
	Route::post('requestuut/simpanbooking', 'RequestUutController@simpanbooking')->name('requestuut.simpanbooking');
	Route::get('requestuut/editbooking/{id}', 'RequestUutController@editbooking')->name('requestuut.editbooking');
	Route::post('requestuut/simpaneditbooking/{id}', 'RequestUutController@simpaneditbooking')->name('requestuut.simpaneditbooking');
	Route::post('requestuut/submitbooking/{id}', 'RequestUutController@submitbooking')->name('requestuut.submitbooking');
	Route::get('requestuut/pdf/{id}', 'RequestUutController@pdf')->name('requestuut.pdf');
	Route::get('requestuut/kuitansi/{id}', 'RequestUutController@kuitansi')->name('requestuut.kuitansi');
	Route::get('requestuut/buktiorder/{id}', 'RequestUutController@buktiorder')->name('requestuut.buktiorder');
	Route::get('requestuut/payment/{id}', 'RequestUutController@payment')->name('requestuut.payment');
	Route::post('requestuut/paymentsave', 'RequestUutController@paymentsave')->name('requestuut.paymentsave');
	Route::get('requestuut/valid/{id}', 'RequestUutController@valid')->name('requestuut.valid');
	Route::post('requestuut/validsave', 'RequestUutController@validsave')->name('requestuut.validsave');
	Route::post('requestuut/novalidsave', 'RequestUutController@novalidsave')->name('requestuut.novalidsave');
	Route::post('requestuut/instalasi/{id}', 'RequestUutController@instalasi')->name('requestuut.instalasi');
	Route::post('requestuut/instalasi', 'RequestUutController@instalasi')->name('requestuut.instalasi');
	Route::get('requestuut/tag/{id}', 'RequestUutController@tag')->name('requestuut.tag');
	Route::get('requestuut/gethistory/{id}', 'RequestUutController@gethistory')->name('requestuut.gethistory');
	Route::post('requestuut/kirimqr', 'RequestUutController@kirimqr')->name('requestuut.kirimqr');
	Route::get('requestuut/destroy/{id}', 'RequestUutController@destroy')->name('requestuut.destroy');

	Route::post('requestuut/edititem/{id}', 'RequestUutController@edititem')->name('requestuut.edititem');
	Route::get('requestuut/createbookinginspection/{id}', 'RequestUutController@createbookinginspection')->name('requestuut.createbookinginspection');
	Route::post('requestuut/simpanbookinginspection/{id}', 'RequestUutController@simpanbookinginspection')->name('requestuut.simpanbookinginspection');
	Route::post('requestuut/hapusbookinginspection/{id}', 'RequestUutController@hapusbookinginspection')->name('requestuut.hapusbookinginspection');
	Route::get('requestuut/getprices/{id}', 'RequestUutController@getprices')->name('requestuut.getprices');
	Route::get('requestuut/getowner/{id}', 'RequestUutController@getowner')->name('requestuut.getowner');
	Route::get('requestuut/getowners', 'RequestUutController@getowners')->name('requestuut.getowners');
	//Pendaftartan
	Route::get('requestuut#frontdesk_pendaftaran', 'RequestUutController@index')->name('requestuut#frontdesk_pendaftaran');
	Route::get('requestuut/destroy/{id}', 'RequestUutController@destroy')->name('requestuut.destroy');
	// ajax
	Route::post('requestuut/data-dalam-all', 'RequestUutController@index_all')->name('request.booking.all');
	Route::post('requestuut/data-dalam-done', 'RequestUutController@index_done')->name('request.booking.done');
	Route::post('requestuut/data-dalam-process', 'RequestUutController@index_process')->name('request.booking.process');

	// Perubahan Permintaan Kaji Ulang
	Route::post('requestuut/permintaan/change/{id}', 'RequestUutController@permintaanChange')->name('requestuut.permintaan.change');

	
	# this is for uut owners rooting
	Route::get('uutowners', 'UutOwnersController@index')->name('uutowners');
	Route::get('uutowners/create', 'UutOwnersController@create')->name('uutowners.create');
	Route::get('uutowners/store', 'UutOwnersController@store')->name('uutowners.store');
	Route::get('uutowners/edit/{id}', 'UutOwnersController@edit')->name('uutowners.edit');
	Route::patch('uutowners/update/{id}', 'UutOwnersController@update')->name('uutowners.update');
	Route::get('uutowners/delete/{id}', 'UutOwnersController@delete')->name('uutowners.delete');

	#update Ajax Route
	Route::post('requestuut/updatestandard/{id}', 'RequestUutController@updateStandard')->name('requestuut.updateStandard');
	Route::get('requestuut/editRequestItem/{id}', 'RequestUutController@editRequestItem')->name('requestuut.editRequestItem');
	Route ::get('requestuut/getStandard/{id}', 'RequestUutController@getStandard')->name('requestuut.getStandard');

	#For Receive UUT
	Route::get('receiveuut', 'ReceiveUutController@index')->name('receiveuut');
	// Route::get('receiveuut/proses/{id}', 'ReceiveUutController@proses')->name('receiveuut.proses');
	Route::post('receiveuut/proses', 'ReceiveUutController@proses')->name('receiveuut.proses');

	Route::get('orderuut', 'OrderUutController@index')->name('orderuut');
	// Route::get('orderuut/proses/{id}', 'OrderUutController@proses')->name('orderuut.proses');
	Route::post('orderuut/proses', 'OrderUutController@proses')->name('orderuut.proses');
	Route::get('orderuut/getinspections/{id}', 'OrderUutController@getinspections')->name('orderuut.getinspections');


	// UNTUK SERVICE ORDER UTTP
	Route::get('serviceuut/download/{id}', 'ServiceUutController@download')->name('serviceuut.download');
	Route::get('serviceuut/downloadLampiran/{id}', 'ServiceUutController@downloadLampiran')->name('serviceuut.downloadLampiran');
	Route::get('serviceuut/cancelation_download/{id}', 'ServiceUutController@cancelation_download')->name('serviceuut.cancelation_download');
	Route::get('serviceuut/print/{id}', 'ServiceUutController@print')->name('serviceuut.print');

	Route::get('serviceuut/printTipe/{id}', 'ServiceUutController@printTipe')->name('serviceuut.printTipe');
	Route::get('serviceuut', 'ServiceUutController@index')->name('serviceuut');
	Route::get('serviceuut/test/{id}', 'ServiceUutController@test')->name('serviceuut.test');
	Route::post('serviceuut/savetest/{id}', 'ServiceUutController@savetest')->name('serviceuut.savetest');
	Route::post('serviceuut/savetestqr', 'ServiceUutController@savetestqr')->name('serviceuut.savetestqr');
	Route::get('serviceuut/result/{id}', 'ServiceUutController@result')->name('serviceuut.result');
	Route::post('serviceuut/resultupload/{id}', 'ServiceUutController@resultupload')->name('serviceuut.resultupload');
	Route::get('serviceuut/warehouse/{id}', 'ServiceUutController@warehouse')->name('serviceuut.warehouse');
	Route::get('serviceuut/download/{id}', 'ServiceUutController@download')->name('serviceuut.download');
	Route::get('serviceuut/preview/{id}', 'ServiceUutController@preview')->name('serviceuut.preview');
	
	Route::get('serviceuut/previewTipe/{id}', 'ServiceUutController@previewTipe')->name('serviceuut.previewTipe');
	
	Route::get('serviceuut/pending/{id}', 'ServiceUutController@pending')->name('serviceuut.pending');
	Route::post('serviceuut/confirmpending/{id}', 'ServiceUutController@confirmpending')->name('serviceuut.confirmpending');
	Route::get('serviceuut/continue/{id}', 'ServiceUutController@continue')->name('serviceuut.continue');
	Route::post('serviceuut/confirmcontinue/{id}', 'ServiceUutController@confirmcontinue')->name('serviceuut.confirmcontinue');

	Route::get('serviceuut/cancel/{id}', 'ServiceUutController@cancel')->name('serviceuut.cancel');
	Route::post('serviceuut/confirmcancel/{id}', 'ServiceUutController@confirmcancel')->name('serviceuut.confirmcancel');

	// Serviice Revision Controller UUT
	Route::get('servicerevisionuut', 'ServiceRevisionUutController@index')->name('servicerevisionuut');
	Route::get('servicerevisionuut/result/{id}', 'ServiceRevisionUutController@result')->name('servicerevisionuut.result');
	Route::post('servicerevisionuut/resultupload/{id}', 'ServiceRevisionUutController@resultupload')->name('servicerevisionuut.resultupload');
	Route::get('servicerevisionuut/sendback', 'ServiceRevisionUutController@returnback')->name('servicerevisionuut.back');
	Route::post('servicerevisionuut/getorders', 'ServiceRevisionUutController@getOrders')->name('servicerevisionuut.getorders');
	Route::get('servicerevisionuut/sendtolab/{id}/{oid}', 'ServiceRevisionUutController@sendtolab')->name('servicerevisionuut.sendtolab');
	// service history
	Route::get('servicehistoryuut', 'ServiceHistoryUutController@index')->name('servicehistoryuut');
	Route::get('servicehistoryuut/{year}', 'ServiceHistoryUutController@getByYear')->name('servicehistoryuut.by-year');
	Route::post('servicehistoryuut/warehouse', 'ServiceHistoryUutController@warehouse')->name('servicehistoryuut.warehouse');

	// untuk UUT
	Route::get('schedulinguut', 'SchedulingUutController@index')->name('schedulinguut');
	Route::get('schedulinguut/schedule/{id}', 'SchedulingUutController@schedule')->name('schedulinguut.schedule');
	Route::post('schedulinguut/schedule/{id}', 'SchedulingUutController@confirmschedule')->name('schedulinguut.confirmschedule');
	Route::get('schedulinguut/staff', 'SchedulingUutController@getstaffes')->name('schedulinguut.staff');
	Route::get('schedulinguut/surattugas/{id}/{stream?}/{docId?}', 'SchedulingUutController@print')->name('schedulinguut.surattugas');
	Route::get('schedulinguut/cancel/{id}', 'SchedulingUutController@cancel')->name('schedulinguut.cancel');
	Route::get('schedulinguut/delete', 'SchedulingUutController@deleteRequest')->name('schedulinguut.delete');
	
	Route::get('reschedulinguut', 'RevisiSchedulingUutController@index')->name('reschedulinguut');
	Route::get('reschedulinguut/schedule/{id}', 'RevisiSchedulingUutController@schedule')->name('reschedulinguut.schedule');
	Route::post('reschedulinguut/schedule/{id}', 'RevisiSchedulingUutController@confirmschedule')->name('reschedulinguut.confirmschedule');
	

	Route::get('firstcheckluaruut', 'FirstCheckLuarUutController@index')->name('firstcheckluaruut');
	Route::get('firstcheckluaruut/cek/{id}', 'FirstCheckLuarUutController@check')->name('firstcheckluaruut.check');
	Route::get('firstcheckluaruut/downloadbukti/{docId}', 'FirstCheckLuarUutController@downloadBukti')->name('firstcheckluaruut.downloadbukti');
	Route::post('firstcheckluaruut/savecek/{id}', 'FirstCheckLuarUutController@savecheck')->name('firstcheckluaruut.savecheck');
	
	Route::get('docinsituuut', 'DocumentInsituUutController@index')->name('docinsituuut');
	Route::get('docinsituuut/approve/{id}', 'DocumentInsituUutController@approval')->name('docinsituuut.approval');
	Route::post('docinsituuut/approve/{id}', 'DocumentInsituUutController@approve')->name('docinsituuut.approve');
	Route::get('docinsituuut/integritas/{id}', 'DocumentInsituUutController@integritas')->name('docinsituuut.integritas');
	
	Route::get('redocinsituuut', 'ReDocumentInsituUutController@index')->name('redocinsituuut');
	Route::get('redocinsituuut/approve/{id}', 'ReDocumentInsituUutController@approval')->name('redocinsituuut.approval');
	Route::post('redocinsituuut/approve/{id}', 'ReDocumentInsituUutController@approve')->name('redocinsituuut.approve');
	Route::get('redocinsituuut/generate-token', 'ReDocumentInsituUutController@generateToken')->name('redocinsituuut.generate');
	

	Route::get('serviceprocessluaruut', 'ServiceProcessLuarUutController@index')->name('serviceprocessluaruut');
	Route::get('serviceprocessluaruut/cek/{id}', 'ServiceProcessLuarUutController@check')->name('serviceprocessluaruut.check');
	Route::post('serviceprocessluaruut/savecek/{id}', 'ServiceProcessLuarUutController@savecheck')->name('serviceprocessluaruut.savecheck');
	Route::post('serviceprocessluaruut/statusrevisi', 'ServiceProcessLuarUutController@statusRevisiSPT')->name('serviceprocessluaruut.statusrevisi');
	
	Route::get('serviceprocessluaruut/createbookinginspection/{id}', 'ServiceProcessLuarUutController@createbookinginspection')->name('serviceprocessluaruut.createbookinginspection');
	Route::post('serviceprocessluaruut/simpanbookinginspection/{id}', 'ServiceProcessLuarUutController@simpanbookinginspection')->name('serviceprocessluaruut.simpanbookinginspection');
	Route::post('serviceprocessluaruut/hapusbookinginspection/{id}', 'ServiceProcessLuarUutController@hapusbookinginspection')->name('serviceprocessluaruut.hapusbookinginspection');
	Route::get('serviceprocessluaruut/newitem/{id}', 'ServiceProcessLuarUutController@newitem')->name('serviceprocessluaruut.newitem');
	Route::post('serviceprocessluaruut/savenewitem/{id}', 'ServiceProcessLuarUutController@savenewitem')->name('serviceprocessluaruut.savenewitem');
	Route::get('serviceprocessluaruut/createitem/{id}/{uttp_type_id}', 'ServiceProcessLuarUutController@createitem')->name('serviceprocessluaruut.createitem');
	Route::post('serviceprocessluaruut/simpanitem/{id}/{uttp_type_id}', 'ServiceProcessLuarUutController@simpanitem')->name('serviceprocessluaruut.simpanitem');
	Route::post('serviceprocessluaruut/getuuts', 'ServiceProcessLuarUutController@getuuts')->name('serviceprocessluaruut.getuuts');
	Route::get('serviceprocessluaruut/edititem/{id}/{idItem}', 'ServiceProcessLuarUutController@edititem')->name('serviceprocessluaruut.edititem');
	Route::post('serviceprocessluaruut/simpanedititem/{id}/{idItem}', 'ServiceProcessLuarUutController@simpanedititem')->name('serviceprocessluaruut.simpanedititem');
	Route::post('serviceprocessluaruut/hapusitem', 'ServiceProcessLuarUutController@hapusitem')->name('serviceprocessluaruut.hapusitem');
	Route::post('serviceprocessluaruut/getunits', 'ServiceProcessLuarUutController@getunits')->name('serviceprocessluaruut.getunits');
	
	Route::get('pembayaranluartuhpuut', 'PembayaranLuarTuhpUutController@index')->name('pembayaranluartuhpuut');
	Route::get('pembayaranluaruut', 'PembayaranLuarUutController@index')->name('pembayaranluaruut');

	Route::get('resumespuhuut', 'ResumeSpuhUutController@index')->name('resumespuhuut');
	Route::get('resumespuhuut/paid/{id}/{state}', 'ResumeSpuhUutController@paid')->name('resumespuhuut.paid');


	Route::get('doctuinsituuut', 'DocumentTUInsituUutController@index')->name('doctuinsituuut');
	Route::get('doctuinsituuut/list/{id}', 'DocumentTUInsituUutController@list')->name('doctuinsituuut.list');
	Route::get('doctuinsituuut/create/{id}', 'DocumentTUInsituUutController@create')->name('doctuinsituuut.create');
	Route::post('doctuinsituuut/create/{id}', 'DocumentTUInsituUutController@store')->name('doctuinsituuut.store');
	Route::get('doctuinsituuut/download/{id}/{tipe}', 'DocumentTUInsituUutController@download')->name('doctuinsituuut.download');
	Route::get('doctuinsituuut/edit/{id}', 'DocumentTUInsituUutController@edit')->name('doctuinsituuut.edit');
	Route::post('doctuinsituuut/edit/{id}', 'DocumentTUInsituUutController@update')->name('doctuinsituuut.update');


	//Resume SNSU
	Route::get('resumeuut', 'ResumeUutController@index')->name('resumeuut');
	Route::get('rekapresumeuut', 'RekapResumeUutController@index')->name('rekapresumeuut');

	//Revision UUT
	Route::get('revisionuut', 'RevisionUutController@index')->name('revisionuut');
	Route::get('revisionuut/edit/{id}', 'RevisionUutController@edit')->name('revisionuut.edit');
	Route::post('revisionuut/simpanedit/{id}', 'RevisionUutController@simpanedit')->name('revisionuut.simpanedit');

	//Revision Order UUT
	Route::get('revisionorderuut', 'RevisionOrderUutController@index')->name('revisionorderuut');
	Route::get('revisionorderuut/order/{id}', 'RevisionOrderUutController@order')->name('revisionorderuut.order');
	Route::post('revisionorderuut/simpanorder/{id}', 'RevisionOrderUutController@simpanorder')->name('revisionorderuut.simpanorder');

	// Requestluar UUT
	Route::get('requestluaruut', 'RequestLuarUutController@index')->name('requestluaruut');
	Route::get('requestluaruut/proses/{id}', 'RequestLuarUutController@proses')->name('requestluaruut.proses');
	Route::post('requestluaruut/saveproses/{id}', 'RequestLuarUutController@saveproses')->name('requestluaruut.saveproses');
	Route::get('requestluaruut/selesai/{id}', 'RequestLuarUutController@selesai')->name('requestluaruut.selesai');
	Route::post('requestluaruut/saveselesai/{id}', 'RequestLuarUutController@saveselesai')->name('requestluaruut.saveselesai');
	Route::get('requestluaruut/createbookinginspection/{id}', 'RequestLuarUutController@createbookinginspection')->name('requestluaruut.createbookinginspection');
	Route::post('requestluaruut/simpanbookinginspection/{id}', 'RequestLuarUutController@simpanbookinginspection')->name('requestluaruut.simpanbookinginspection');
	Route::post('requestluaruut/hapusbookinginspection/{id}', 'RequestLuarUutController@hapusbookinginspection')->name('requestluaruut.hapusbookinginspection');
	Route::get('requestluaruut/payment/{id}', 'RequestLuarUutController@payment')->name('requestluaruut.payment');
	Route::post('requestluaruut/paymentsave', 'RequestLuarUutController@paymentsave')->name('requestluaruut.paymentsave');
	Route::get('requestluaruut/valid/{id}', 'RequestLuarUutController@valid')->name('requestluaruut.valid');
	Route::post('requestluaruut/validsave', 'RequestLuarUutController@validsave')->name('requestluaruut.validsave');
	Route::post('requestluaruut/novalidsave', 'RequestLuarUutController@novalidsave')->name('requestluaruut.novalidsave');
	Route::get('requestluaruut/pending/{id}', 'RequestLuarUutController@pending')->name('requestluaruut.pending');
	Route::post('requestluaruut/savepending/{id}', 'RequestLuarUutController@savepending')->name('requestluaruut.savepending');
	Route::get('requestluaruut/continue/{id}', 'RequestLuarUutController@continue')->name('requestluaruut.continue');
	Route::post('requestluaruut/savecontinue/{id}', 'RequestLuarUutController@savecontinue')->name('requestluaruut.savecontinue');
	Route::get('requestluaruut/cancel/{id}', 'RequestLuarUutController@cancel')->name('requestluaruut.cancel');
	Route::post('requestluaruut/savecancel/{id}', 'RequestLuarUutController@savecancel')->name('requestluaruut.savecancel');

	Route::get('requestluaruut/pdf/{id}', 'RequestLuarUutController@pdf')->name('requestluaruut.pdf');
	Route::get('requestluaruut/pdftuhp/{id}', 'RequestLuarUutController@pdftuhp')->name('requestluaruut.pdftuhp');
	Route::get('requestluaruut/paymenttuhp/{id}', 'RequestLuarUutController@paymenttuhp')->name('requestluaruut.paymenttuhp');
	Route::post('requestluaruut/paymenttuhpsave', 'RequestLuarUutController@paymenttuhpsave')->name('requestluaruut.paymenttuhpsave');
	
	Route::get('requestluaruut/validtuhp/{id}', 'RequestLuarUutController@validtuhp')->name('requestluaruut.validtuhp');
	Route::post('requestluaruut/validtuhpsave', 'RequestLuarUutController@validtuhpsave')->name('requestluaruut.validtuhpsave');
	Route::post('requestluaruut/novalidtuhpsave', 'RequestLuarUutController@novalidtuhpsave')->name('requestluaruut.novalidtuhpsave');

	Route::get('requestluaruut/extend/{id}', 'RequestLuarUutController@extend')->name('requestluaruut.extend');
	Route::post('requestluaruut/saveextend/{id}', 'RequestLuarUutController@saveextend')->name('requestluaruut.saveextend');

	Route::get('requestluaruut/editpnbp/{id}', 'RequestLuarUutController@editpnbp')->name('requestluaruut.editpnbp');
	

	// service luat uut
	Route::get('serviceluaruut', 'ServiceLuarUutController@index')->name('serviceluaruut');
	Route::get('serviceluaruut/result/{id}', 'ServiceLuarUutController@result')->name('serviceluaruut.result');
	Route::post('serviceluaruut/resultupload/{id}', 'ServiceLuarUutController@resultupload')->name('serviceluaruut.resultupload');
	Route::get('serviceluaruut/editalat/{id}', 'ServiceLuarUutController@editalat')->name('serviceluaruut.editalat');
	Route::post('serviceluaruut/saveeditalat/{id}', 'ServiceLuarUutController@saveeditalat')->name('serviceluaruut.saveeditalat');
	Route::post('serviceluaruut/savetest/{id}', 'ServiceLuarUutController@savetest')->name('serviceluaruut.savetest');
	Route::post('serviceluaruut/savetestqr', 'ServiceLuarUutController@savetestqr')->name('serviceluaruut.savetestqr');

	Route::get('serviceluaruut/preview/{id}', 'ServiceLuarUutController@preview')->name('serviceluaruut.preview');
	Route::get('serviceluaruut/previewTipe/{id}', 'ServiceLuarUutController@previewTipe')->name('serviceluaruut.previewTipe');
	Route::get('serviceluaruut/previewSETipe/{id}', 'ServiceLuarUutController@previewSETipe')->name('serviceluaruut.previewSETipe');

	Route::get('serviceluaruut/print/{id}', 'ServiceLuarUutController@print')->name('serviceluaruut.print');
	Route::get('serviceluaruut/download/{id}', 'ServiceLuarUutController@download')->name('serviceluaruut.download');
	Route::get('serviceluaruut/downloadLampiran/{id}', 'ServiceLuarUutController@downloadLampiran')->name('serviceluaruut.downloadLampiran');
	
	Route::get('serviceluaruut/pending/{id}', 'ServiceLuarUutController@pending')->name('serviceluaruut.pending');
	Route::post('serviceluaruut/confirmpending/{id}', 'ServiceLuarUutController@confirmpending')->name('serviceluaruut.confirmpending');
	Route::get('serviceluaruut/continue/{id}', 'ServiceLuarUutController@continue')->name('serviceluaruut.continue');
	Route::post('serviceluaruut/confirmcontinue/{id}', 'ServiceLuarUutController@confirmcontinue')->name('serviceluaruut.confirmcontinue');

	Route::get('serviceluaruut/cancel/{id}', 'ServiceLuarUutController@cancel')->name('serviceluaruut.cancel');
	Route::post('serviceluaruut/confirmcancel/{id}', 'ServiceLuarUutController@confirmcancel')->name('serviceluaruut.confirmcancel');

	Route::get('servicerevisionluaruut', 'ServiceRevisionLuarUutController@index')->name('servicerevisionluaruut');
	Route::get('servicerevisionluaruut/result/{id}', 'ServiceRevisionLuarUutController@result')->name('servicerevisionluaruut.result');
	Route::post('servicerevisionluaruut/resultupload/{id}', 'ServiceRevisionLuarUutController@resultupload')->name('servicerevisionluaruut.resultupload');
	

	Route::get('historyluaruut', 'HistoryLuarOrderUutController@index')->name('historyluaruut');
	Route::get('servicehistoryluaruut', 'ServiceHistoryLuarUutController@index')->name('servicehistoryluaruut');

	#viewer
	Route::get('serviceuut/show/{id}/{type}', 'ServiceUutController@show')->name('file.show');

	// //serviceuut
	// Route::get('serviceuut', 'ServiceUutController@index')->name('serviceuut');
	// Route::get('serviceuut/test/{id}', 'ServiceUutController@test')->name('serviceuut.test');
	// Route::post('serviceuut/savetest/{id}', 'ServiceUutController@savetest')->name('serviceuut.savetest');
	// Route::post('serviceuut/savetestqr', 'ServiceUutController@savetestqr')->name('serviceuut.savetestqr');
	// Route::get('serviceuut/result/{id}', 'ServiceUutController@result')->name('serviceuut.result');
	// Route::post('serviceuut/resultupload/{id}', 'ServiceUutController@resultupload')->name('serviceuut.resultupload');
	// Route::get('serviceuut/warehouse/{id}', 'ServiceUutController@warehouse')->name('serviceuut.warehouse');
	
	// Route::get('serviceuut/preview/{id}', 'ServiceUutController@preview')->name('serviceuut.preview');
	// Route::get('serviceuut/previewTipe/{id}', 'ServiceUutController@previewTipe')->name('serviceuut.previewTipe');
	// Route::get('serviceuut/previewSETipe/{id}', 'ServiceUutController@previewSETipe')->name('serviceuut.previewSETipe');
	
	// Route::get('serviceuut/pending/{id}', 'ServiceUutController@pending')->name('serviceuut.pending');
	// Route::post('serviceuut/confirmpending/{id}', 'ServiceUutController@confirmpending')->name('serviceuut.confirmpending');
	// Route::get('serviceuut/continue/{id}', 'ServiceUutController@continue')->name('serviceuut.continue');
	// Route::post('serviceuut/confirmcontinue/{id}', 'ServiceUutController@confirmcontinue')->name('serviceuut.confirmcontinue');

	// Route::get('serviceuut/cancel/{id}', 'ServiceUutController@cancel')->name('serviceuut.cancel');
	// Route::post('serviceuut/confirmcancel/{id}', 'ServiceUutController@confirmcancel')->name('serviceuut.confirmcancel');

	// Route::middleware(['CheckStaff'])->group(function ()
	// {
	// 	Route::get('setting-price', 'PriceSetting@create')->name('setting.price');
	// 	Route::get('setting-type', 'PriceSetting@index_type')->name('setting.type');
	// 	Route::post('setting-save', 'PriceSetting@save')->name('save');
	// 	Route::post('setting-price-save', 'PriceSetting@priceSave')->name('price.save');
	// 	Route::get('setting-price-edit/{id}/{data}', 'PriceSetting@edit')->name('price.edit');
	// 	Route::post('setting-price-edit', 'PriceSetting@editSave')->name('edit.save');
	// 	Route::post('setting-price-type', 'PriceSetting@typeSave')->name('type.save');
	// 	Route::post('setting-price-find/{id}', 'PriceSetting@getPrice')->name('price.find');
	// 	Route::post('setting-price-data', 'PriceSetting@getData')->name('price.data');
	// 	Route::post('setting-price-uml', 'PriceSetting@getDataUml')->name('price.uml');

	// 	Route::get('setting-type-create', 'PriceSetting@createType')->name('type.create');
	// 	Route::get('setting-price-create', 'PriceSetting@createPrice')->name('price.create');
	// 	Route::get('setting-type-edit/{id}', 'PriceSetting@editType')->name('type.edit');
	// 	Route::post('setting-type-edit-save/{id}', 'PriceSetting@editTypeSave')->name('type.edit.save');
	// });

	Route::get('setting-price', 'PriceSetting@create')->name('setting.price');
	Route::get('setting-type', 'PriceSetting@index_type')->name('setting.type');
	Route::post('setting-save', 'PriceSetting@save')->name('save');
	Route::post('setting-price-save', 'PriceSetting@priceSave')->name('price.save');
	Route::get('setting-price-edit/{id}/{data}', 'PriceSetting@edit')->name('price.edit');
	Route::post('setting-price-edit', 'PriceSetting@editSave')->name('edit.save');
	Route::post('setting-price-type', 'PriceSetting@typeSave')->name('type.save');
	Route::post('setting-price-find/{id}', 'PriceSetting@getPrice')->name('price.find');
	Route::post('setting-price-data', 'PriceSetting@getData')->name('price.data');
	Route::post('setting-price-uml', 'PriceSetting@getDataUml')->name('price.uml');

	Route::get('setting-type-create', 'PriceSetting@createType')->name('type.create');
	Route::get('setting-price-create', 'PriceSetting@createPrice')->name('price.create');
	Route::get('setting-type-edit/{id}', 'PriceSetting@editType')->name('type.edit');
	Route::post('setting-type-edit-save/{id}', 'PriceSetting@editTypeSave')->name('type.edit.save');

});