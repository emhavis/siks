<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterUsers extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';

	public function MasterLaboratory()
	{
	  return $this->belongsTo('App\MasterLaboratory','laboratory_id');
	}

	public function MasterUml()
	{
	  return $this->belongsTo('App\MasterUml','uml_id');
	}

	public function MasterRoles()
	{
	  return $this->belongsTo('App\MasterRoles','user_role');
	}

	public function PetugasUttp()
	{
	  return $this->belongsTo('App\MasterPetugasUttp','petugas_uttp_id');
	}

	public function PetugasUut()
	{
	  return $this->belongsTo('App\MasterPetugasUttp','petugas_uut_id');
	}
}
