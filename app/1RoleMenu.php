<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    protected $table = 'role_menus';
	public $timestamps = false;
	protected $guarded = array('id');

    protected $fillable = [
        'role_id', 'menu_id'
    ];
}
