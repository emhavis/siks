<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttpTTUCTMSGauge extends Model
{
    protected $table = 'service_order_uttp_ttu_ctms_gauge';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "jenis",
        "tank",
        "serial_no", 
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
