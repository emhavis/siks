<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StandardMeasurementUnit extends Model
{
    protected $table = 'standard_measurement_units';
    protected $primaryKey = 'id';
    
    // public function dropdown()
    // {
    //     return $this->orderBy('id')->pluck('measurement_unit', 'id');
    // }

	public function dropdown()
	{
        $rows = $this->get();

        $data[] = "-- Pilih --";
        foreach($rows as $row)
        {
            $data[$row->measurement_unit] = $row->measurement_unit;
        }

        return $data;
	}

}
