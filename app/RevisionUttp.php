<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevisionUttp extends Model 
{
    protected $table = 'certificate_revision_uttps';
    protected $primaryKey = 'id';

    protected $fillable = [
        'requestor_id','booking_id','request_id','order_id','status','service_type_id',
        'uttp_type_id','tool_brand', 'tool_model', 'tool_type', 'tool_capacity',
        'tool_factory', 'tool_factory_address', 'tool_made_in', 
        'label_sertifikat', 'addr_sertifikat', 'others',
        'no_sertifikat', 'no_service',
        'staff_id', 'staff_at', 'seri_revisi', 'status_approval', 'status_revision',
        'subko_id', 'subko_at', 'kabalai_id', 'kabalai_at',
        'file_lamp_skhp', 'path_lamp_skhp',

        'preview_count', 'tool_capacity_min', 'tool_serial_no', 'tool_media', 'tool_media_pengukuran',
    ];

    public function booking()
    {
        return $this->belongsTo('App\ServiceBooking','booking_d');
    }

    public function request()
    {
        return $this->belongsTo('App\ServiceRequestUttp','request_id');
    }

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps','order_id');
    }

    public function requestor()
    {
        return $this->belongsTo('App\Customer','requestor_id');
    } 

    public function masterstatus()
    {
        return $this->belongsTo('App\MasterRevisionStatus','status');
    } 

    public function MasterUsers()
    {
        return $this->belongsTo('App\MasterUsers',"created_by");
    }

    public function uttpType()
    {
        return $this->belongsTo('App\MasterUttpType','uttp_type_id');
    } 
}