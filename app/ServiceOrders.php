<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ServiceOrders extends Model
{
    protected $table = 'service_orders';
    protected $primaryKey = 'id';

    protected $fillable = [
        "id",
        "stat_service_order",
        "supervisor_staff",
        "supervisor_entry_date",
        "staff_entry_dateout",
        "service_type_id",
        "file_skhp",
        "path_skhp",
        "stat_sertifikat",
        "stat_service_order",
        "subkoordinator_id",
        "subkoordinator_date",
        "kabalai_date",
        "kalab_date",
        "persyaratan_teknis_id",
        "test_by_1", "test_by_2",
        "file_lampiran","path_lampiran",
        "file_lampiran_kalab","path_lampiran_kalab",
        "file_lampiran_kabalai","path_lampiran_kabalai",
        "file_review_subkoordinator","path_review_subkoordinator",
        "hasil_uji_memenuhi","stat_warehouse",'sertifikat_expired_at','subkoordinator_notes',
        "cancel_at","cancelation_file","cancel_notes","is_certified",

        'warehouse_in_id', 'warehouse_in_at', 'warehouse_out_id', 'warehouse_out_at', 'warehouse_out_nama',

        'tool_type_id', 'tool_serial_no', 'tool_brand', 'tool_model', 'tool_type', 'tool_capacity',
        'tool_capacity_unit', 'tool_factory', 'tool_factory_address', 'tool_made_in', 'tool_made_in_id',
        'tool_owner_id', 'tool_media', 'tool_name', 'tool_class', 'tool_jumlah', 'tool_dayabaca', 'tool_dayabaca_unit', 
        'uut_id','scheduled_test_date_from',
        'scheduled_test_date_to','page_num','tool_capacity_min','tool_capacity_min_unit',

        'location', 'location_lat', 'location_long',
        "no_surat_tipe_int","no_sertifikat_year",
        "no_sertifikat_tipe_year"
    ];

    public function MasterUsers()
    {
        return $this->belongsTo('App\MasterUsers',"lab_staff");
    }

    public function LabStaffOut()
    {
        return $this->belongsTo('App\MasterUsers',"lab_staff_out");
    }

    public function SupervisorStaff()
    {
        return $this->belongsTo('App\MasterUsers',"supervisor_staff");
    }

    public function DitmetStaff()
    {
        return $this->belongsTo('App\MasterUsers',"ditmet_staff");
    }

    public function ServiceRequest()
    {
        return $this->belongsTo('App\ServiceRequest',"service_request_id");
    }

    public function ServiceRequestItem()
    {
        return $this->belongsTo('App\ServiceRequestItem',"service_request_item_id");
    }

    public function MasterLaboratory()
    {
        return $this->belongsTo('App\MasterLaboratory',"laboratory_id");
    }

    public function inspectionRequestItemInspection()
    {
        return $this->belongsTo('App\ServiceRequestItemInspection',"service_request_item_inspection_id");
    }

    public function inspections()
    {
        return $this->hasMany('App\ServiceOrderInspections','order_id');
    }
    
    public function persyaratanTeknis()
    {
        return $this->belongsTo('App\OIML',"persyaratan_teknis_id");
    }

    public function KaLab()
    {
        return $this->belongsTo('App\MasterUsers',"kalab_id");
    }

    public function SubKoordinator()
    {
        return $this->belongsTo('App\MasterUsers',"subkoordinator_id");
    }

    public function KaBalai()
    {
        return $this->belongsTo('App\MasterUsers',"kabalai_id");
    }

    public function ttu()
    {
        return $this->hasOne('App\ServiceOrderUttpTTUInspection','order_id');
    }

    public function test1(){
        return $this->belongsTo('App\MasterUsers',"test_by_1");
    }

    public function test2(){
        return $this->belongsTo('App\MasterUsers',"test_by_2");
    }
    public function ttuPerlengkapans(){
        return $this->hasMany('App\ServiceOrderUutTTUPerlengkapan',"order_id");
    }

    public function uut()
    {
        return $this->belongsTo('App\Uut','uut_id');
    }

    public function uutType()
    {
        return $this->belongsTo('App\MasterStandardType','tool_type_id');
    }
}
