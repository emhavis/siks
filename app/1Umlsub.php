<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Umlsub extends Model
{
    protected $table = 'uml_sub';
	public $timestamps = false;
	protected $guarded = array('id');
	protected $primaryKey = 'id';

    protected $fillable = [
        'uml_id',
        'uml_sub_name', 
        'address', 
        'phone_no'
    ];

    public static $rules = array(
        'uml_sub_name' => 'required'
    );

}
