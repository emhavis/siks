<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    protected $table = 'service_types';
	public $timestamps = false;
	protected $guarded = array('id');
	protected $primaryKey = 'id';

    protected $fillable = [
        'service_type'
    ];

    public static $rules = array(
        'service_type' => 'required'
    );
}
