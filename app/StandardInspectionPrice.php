<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StandardInspectionPrice extends Model
{
    protected $table = 'uut_inspection_prices';
    protected $primaryKey = 'id';
    protected $fillable = [
            'service_type_id',
            'inspection_type',
            'price',
            'created_at',
            'updated_at',
            'deleted_at',
            'has_range',
            'unit',
            'lab_id',
            'inspection_template_id',
            'user_type',
            ];

    public function ServiceRequestItemInspection()
    {
        return $this->hasMany('App\ServiceRequestItemInspection',"standard_inspection_price_id");
    }
    
    public function StandardMeasurementUnit()
    {
        return $this->belongsTo('App\StandardMeasurementUnit',"standard_measurement_unit_id");
    }
    
    public function dropdown($id)
    {
        // ->select('inspection_type', 'id', 'price',)
        $rows = $this->where('standard_detail_type_id', $id)
        ->orderBy('id')
        ->get();

        $data = [];
        foreach($rows as $row)
        {
            $data[] = [
                'id'=>$row->id,
                'price'=>$row->price,
                'inspection_type'=>$row->inspection_type,
                'unit'=>StandardMeasurementUnit::whereId($row->standard_measurement_unit_id)->value('measurement_unit')
            ];
        }

        return response($data);
    }

    // public function inspectionType(){
    //     return $this->belongsTo('App\StandardDetailType','standard_detail_type_id');
    // }

    public function lab(){
        return $this->belongsTo('App\MasterLAboratory','lab_id');
    }
}
