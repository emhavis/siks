<?php

namespace App\MyClass;

use App\MasterLaboratory;
use App\MasterUml;
use App\MasterRoles;
use App\MasterUsers;

use App\ServiceOrderUttps;
use App\Notif\QueriesNotif;

use Illuminate\Support\Facades\Auth;

class MyProjects
{
    private $MasterLaboratory;
    private $MasterUml;
    private $MasterRoles;
    private $MasterUsers;
    private $module;
    private $user_lab ;
    private $user_instalasi;

    public function __construct()
    {
        $this->MasterLaboratory    = new MasterLaboratory();
        $this->MasterUml           = new MasterUml();
        $this->MasterRoles         = new MasterRoles();
        $this->MasterUsers         = new MasterUsers();
        $this->user_lab            = '';
        $this->user_instalasi      ='';
    }

    public function Setup($module=null)
    {
        $this->module = $module;

        $attribute = $this->MasterUsers
        ->whereId(Auth::id())
        ->whereIsActive("t")
        ->first();

        if($attribute)
        {
            $attribute = [];
            $attribute["module"] = $this->module;
            $attribute["role_name"] = $this->MasterRoles->where("id",Auth::user()->user_role)->value("role_name");
            $attribute["nama_lab"] = $this->MasterLaboratory->where("id",Auth::user()->laboratory_id)->value("nama_lab");
            $attribute["uml_name"] = $this->MasterUml->where("id",Auth::user()->uml_id)->value("uml_name");
            $attribute["menu"] = $this->getmenu();

            // if(
            //     session()->has("isAuthenticated")===false
            //     || session()->has("role")===false
            // )
            // if(session()->has("role")===false)
            // {
                // session()->put("isAuthenticated",true);
            //     session()->put("role_name",$attribute["role_name"]);
            //     session()->save();
            // }

        }

        return $attribute;
    }

    public function getmenu()
    {
        $nilai = [];
        $menu = [];
        $menu['Profil Pengguna'] = 'userprofil';
        
        switch (Auth::user()->user_role)
        {
            case '2': 
                $menu['Setting User'] = [
                'Profil'=>'userprofil',
                'Pengguna'=>'user',
                'Roles/Group'=>'role',
                'Petugas' => 'petugas',
                ];

                $menu['Master Data'] = [
                // 'Master Standard'=>'measurementtype',
                'Rincian Alat'=>'measurementtype',
                'Laboratorium'=>'laboratory',
                'Instalasi'=>'instalasi',
                // 'Master Template Result'=>'templates',
                'Hari Libur'=>'holiday',
                'SBM' => 'sbm',
                ];

                /*
                $menu['Standard'] = [
                'QR Codes'=>'qrcode',
                'Master Standar'=>'standard',
                'UML Register Standar'=>'umlstandard'
                ];

                $menu['Layanan'] = [
                'SNSU'=>'request',
                'Uji UTTP'=>'requestuttp',
                ];
                */

                // $menu['Pendaftaran'] = 'request';

                break;
                
            case '3':
                $menu['Proses Standard'] = 'tested';
                $menu['Persetujuan SKHP'] = 'approveuttp';
                $menu['Riwayat Order UTTP'] = 'historyorderuttp';
                $menu['History Standard'] = 'tested.history';
                break;
            case '4':
                
                $menu['Terima Alat'] = 'receiveuut';
                $menu['Ambil Order'] = 'orderuut';
                $menu["Dalam Kantor UUT"] =[
                    'Proses Order'=>'serviceuut',
                    'Perbaikan Draft' => ['link'=>'servicerevisionuut','nilai'=> count(QueriesNotif::GetNotifRevisionUut())],
                    'Riwayat Pelayanan' =>[ 'sub' =>QueriesNotif::getServiceYears(),
                    ],
                ];
                
                $menu["Dinas Luar UUT"] =[
                    'Proses Awal' => ['link' => 'firstcheckluaruut', 'nilai'=> QueriesNotif::GetNotifFirstCheckLuarUut()],
                    
                    'Proses Uji'=>['link'=>'serviceprocessluaruut','nilai'=> QueriesNotif::GetNotifServiceProcessLuarUut()],
                    'Proses Order'=>['link'=>'serviceluaruut','nilai'=> QueriesNotif::GetNotifServiceLuarUut()],
                    'Riwayat Pelayanan'=>'servicehistoryluaruut',
                    'Revisi Draft' => 'servicerevisionluaruut'
                ];

                $menu['Dinas Luar UTTP'] = [
                    'Proses Awal' => ['link' => 'firstcheckluaruttp', 'nilai'=> QueriesNotif::GetNotifFirstCheckLuarUttp()],
                    //'Surat Tugas DL' => 'orderinsituuttp',
                    //"Proses Uji DL" => 'requestluaruttp',
                    'Proses Uji'=>['link'=>'serviceprocessluaruttp','nilai'=> QueriesNotif::GetNotifServiceProcessLuarUttp()],
                    'Proses Order'=>['link'=>'serviceluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceLuarUttp())],
                    'Revisi Draft'=>['link'=>'servicerevisionluaruttp','nilai'=> 0],
                    'Riwayat Order'=>['link'=>'servicehistoryluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceHistoryLuarUttp())]
                ];
                $menu['SUML Data Manager'] = [
                    'Kembalikan Order' => 'servicerevisionuut/sendback',
                ];
                // $menu['Dinas Luar UTTP'] = [
                //     'Proses Awal DL' => ['link' => 'firstcheckluaruttp', 'nilai'=> QueriesNotif::GetNotifFirstCheckLuarUttp()],
                //     //'Surat Tugas DL' => 'orderinsituuttp',
                //     //"Proses Uji DL" => 'requestluaruttp',
                //     'Proses Uji DL'=>['link'=>'serviceprocessluaruttp','nilai'=> QueriesNotif::GetNotifServiceProcessLuarUttp()],
                //     'Proses Order DL'=>['link'=>'serviceluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceLuarUttp())],
                //     'Revisi Draft DL'=>['link'=>'servicerevisionluaruttp','nilai'=> 0],
                //     'Riwayat Order DL'=>['link'=>'servicehistoryluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceHistoryLuarUttp())]
                // ];

                $menu['Dinas Luar UTTP'] = [
                    'Proses Awal DL' => ['link' => 'firstcheckluaruttp', 'nilai'=> QueriesNotif::GetNotifFirstCheckLuarUttp()],
                    //'Surat Tugas DL' => 'orderinsituuttp',
                    //"Proses Uji DL" => 'requestluaruttp',
                    'Proses Uji DL'=>['link'=>'serviceprocessluaruttp','nilai'=> QueriesNotif::GetNotifServiceProcessLuarUttp()],
                    'Proses Order DL'=>['link'=>'serviceluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceLuarUttp())],
                    'Revisi Draft DL'=>['link'=>'servicerevisionluaruttp','nilai'=> QueriesNotif::GetNotifServiceRevisionLuarUttp()],
                    'Riwayat Order DL'=>['link'=>'servicehistoryluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceHistoryLuarUttp())]
                ];
                
                break;
            case '5':
                $menu['Pelanggan'] = 'customer';
                $menu['BPUTTP'] = [
                    'Dalam Kantor'=>'requestuttp?type=kn',
                    'Dinas Luar'=>'requestuttp?type=dl',
                    'Riwayat KN' => 'requesthistoryuttp?type=kn',
                    'Riwayat DL' => 'requesthistoryuttp?type=dl'
                ];

                $menu['SUML'] = [
                    'Dalam Kantor'=>'requestuut?type=kn',
                    'Dinas Luar'=>'requestuut?type=dl',
                    'Riwayat KN' => 'requestuut',
                    ];
                
                $menu['SUML Data Manager'] = [
                    // 'Kembalikan Order' => 'servicerevisionuut/sendback',
                    'Harga Pengujian' => 'setting-price'
                ];
                break;
            case '6':
                /*
                $menu['Proses Data'] = 'finished';
                $menu['History Data'] = 'finished.history';
                */
                $menu['Riwayat Order UTTP'] = 'historyorderuttp';
                $menu['Riwayat Order UUT'] = 'historyorderuut';
                break;
            case '7':
                $menu['Register Standard'] = 'umlstandard';
                break;
            case '8':
                //$menu['Report Standard'] = 'frontdesk';
                $menu['Pelanggan'] = 'customertu';
                $menu["Resume UTTP"] = 'resumeuttp';
                //$menu['Penjadwalan DL'] = 'schedulinguttp';
                //$menu["Dinas Luar"] = 'requestluaruttp';
                //$menu['Pengaduan SKHP'] = 'revisionuttp';
                //$menu['Perbaikan SKHP'] = 'revisionorderuttp';

                //$menu['Invoice DL'] = 'pembayaranluaruttp';
                $menu['Surat Tugas DL'] = 'doctuinsituuttp'; 
                $menu['Evaluasi Layanan DL'] = 'evaluasiorderinsituuttp';

                $menu['Arsip Order UTTP'] = 'arsiporderuttp';
                
                break;
            case '9':
                $menu['Petugas DL'] =  'petugasdl';
                $menu['Penjadwalan DL'] =  ['link' => 'schedulinguttp', 'nilai' => QueriesNotif::GetNotifSchedulingUttp()];
                $menu['Revisi SPT DL'] =  ['link' => 'reschedulinguttp', 'nilai' => QueriesNotif::GetNotifReSchedulingUttp()];
                
                $menu['Persetujuan SKHP'] = ['link' => 'approveuttp', 'nilai' => count(QueriesNotif::GetNotifApproveSubko())];
                $menu['Progress Order UTTP'] = 'progressorderuttp';
                $menu['Riwayat Order UTTP'] = 'historyorderuttp';
                $menu['Riwayat Surat Tugas DL'] = 'histdocinsituuttp';
                $menu['Pengaduan SKHP'] = ['link' => 'revisionuttp', 'nilai' => 0];
                //$menu['Perbaikan SKHP'] = 'revisionorderuttp'
                $menu['Persetujuan Perbaikan'] = ['link' => 'revisionapproveuttp', 'nilai' => 0];

                $menu['Hasil Survey'] = 'surveyresult';
                $menu['Evaluasi Layanan DL'] = 'evaluasiorderinsituuttp';

                $menu['Dinas Luar UTTP'] = [
                    'Proses Awal DL' => ['link' => 'firstcheckluaruttp', 'nilai'=> QueriesNotif::GetNotifFirstCheckLuarUttp()],
                    //'Surat Tugas DL' => 'orderinsituuttp',
                    //"Proses Uji DL" => 'requestluaruttp',
                    'Proses Uji DL'=>['link'=>'serviceprocessluaruttp','nilai'=> QueriesNotif::GetNotifServiceProcessLuarUttp()],
                    'Proses Order DL'=>['link'=>'serviceluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceLuarUttp())],
                    'Revisi Draft DL'=>['link'=>'servicerevisionluaruttp','nilai'=> QueriesNotif::GetNotifServiceRevisionLuarUttp()],
                    'Riwayat Order DL'=>['link'=>'servicehistoryluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceHistoryLuarUttp())]
                ];

                $menu['Masa Berlaku UTTP'] = 'expiredorderuttp';
                break;
            case '10':
                $menu['Persetujuan SKHP'] = ['link' => 'approveuttp', 'nilai' => count(QueriesNotif::GetNotifApproveKabalai())];;
                $menu['Riwayat Order UTTP'] = 'historyorderuttp';
                $menu['Persetujuan Perbaikan'] = 'revisionapproveuttp';
                
                $menu['Surat Tugas DL UTTP'] = ['link' => 'docinsituuttp', 'nilai' => QueriesNotif::GetNotifDocInsituUttp()];
                $menu['Revisi SPT DL UTTP'] = ['link' => 'redocinsituuttp', 'nilai' => QueriesNotif::GetNotifReDocInsituUttp()];
                
                $menu['Petugas DL'] =  'petugasdl';
                $menu['Riwayat Surat Tugas DL'] = 'histdocinsituuttp';
                $menu['Hasil Survey'] = 'surveyresult';
                $menu['Evaluasi Layanan DL'] = 'evaluasiorderinsituuttp';
                $menu['Masa Berlaku UTTP'] = 'expiredorderuttp';

                break;
            case '12':
                $menu['Terima UTTP'] = 'receiveuttp';
                //$menu['Terima UUT'] = 'receiveuut';
                //$menu['Konfirm Dinas Luar'] = 'scheduleconfirmuttp';
                //$menu['Order'] = ['UTTP' =>'orderuttp', 'UUT'=>'orderuut'];
                $menu['Ambil Order'] = 'orderuttp';
                //$menu['Order Dinas Luar'] = 'orderluaruttp';

                $menu['Dalam Kantor UTTP'] = [
                    'Proses Order KN'=>['link'=>'serviceuttp','nilai'=> count(QueriesNotif::GetNotifServiceUttp())],
                    'Revisi Draft KN'=>['link'=>'servicerevisionuttp','nilai'=> count(QueriesNotif::GetNotifRevisionUttp())],
                    'Riwayat Order KN'=>['link'=>'servicehistoryuttp','nilai'=>count(QueriesNotif::GetNotifHistoryUttp())],
                ];

                $menu['Dinas Luar UTTP'] = [
                    'Proses Awal DL' => ['link' => 'firstcheckluaruttp', 'nilai'=> QueriesNotif::GetNotifFirstCheckLuarUttp()],
                    //'Surat Tugas DL' => 'orderinsituuttp',
                    //"Proses Uji DL" => 'requestluaruttp',
                    'Proses Uji DL'=>['link'=>'serviceprocessluaruttp','nilai'=> QueriesNotif::GetNotifServiceProcessLuarUttp()],
                    'Proses Order DL'=>['link'=>'serviceluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceLuarUttp())],
                    'Revisi Draft DL'=>['link'=>'servicerevisionluaruttp','nilai'=> QueriesNotif::GetNotifServiceRevisionLuarUttp()],
                    'Riwayat Order DL'=>['link'=>'servicehistoryluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceHistoryLuarUttp())]
                ];

                $menu['Pengaduan'] = [
                    'Ambil Order' =>['link'=> 'revisionorderuttp','nilai'=>'1'],
                    'Proses Perbaikan' => ['link'=> 'revisiontechorderuttp','nilai'=>'1'],
                ];

                /*
                $menu['Proses Order KN'] = 'serviceuttp';
                $menu['Proses Order DL'] = 'serviceluaruttp';
                $menu['Revisi Draft KN'] = 'servicerevisionuttp';
                $menu['Revisi Draft DL'] = 'servicerevisionluaruttp';
                $menu['Riwayat Order KN'] = 'servicehistoryuttp';
                $menu['Riwayat Order DL'] = 'servicehistoryluaruttp';
                */
                // $menu['Perbaikan SKHP'] = 'revisiontechorderuttp';
                break;
            case '13':
                //$menu['Penjadwalan'] = 'schedulinguttp';
                break;
            case '14':
                /*
                $menu['Pengambilan'] = [
                    'UTTP' => 'warehousereceiveuttp',
                    'UUT' => 'warehousereceiveuut',
                ];
                $menu['Pengembalian'] = [
                    'UTTP' => 'warehouseuttp',
                    'UUT' => 'warehouseuut',
                ];
                $menu['Riwayat'] = [
                    'Riwayat UTTP' => 'warehousehistoryuttp',
                    'Riwayat UUT' => 'warehousehistoryuut',
                ];
                */
                
                $menu['Pengambilan UTTP'] = 'warehousereceiveuttp';
                $menu['Pengambilan UUT'] = 'warehousereceiveuut';
                
                $menu['Pengembalian UTTP'] = 'warehouseuttp';
                $menu['Pengembalian UUT'] = 'warehouseuut';

                $menu['Riwayat UTTP'] = 'warehousehistoryuttp';
                $menu['Riwayat UUT'] = 'warehousehistoryuut';
                
                break;
            /* UUT sidebar mapping */
            case '15':              
                $menu['Persetujuan Sertifikat'] = 'approveuut';
                $menu['Riwayat Order UUT'] = 'historyorderuut';
            case '16':
                // $menu['Penjadwalan DL'] = 'schedulinguut';
                $menu['Petugas DL'] =  'petugasdl';
                $menu['Penjadwalan DL'] =  ['link' => 'schedulinguut', 'nilai' => 0];
                $menu['Revisi SPT DL'] =  ['link' => 'reschedulinguut', 'nilai' => 0];
                
                
                $menu['Persetujuan Sertifikat'] = 'approveuut';
                $menu['Riwayat Order'] = 'historyorderuut';
                $menu['Perbaikan Sertifikat'] = 'revisionapproveuut';
                $menu['Hasil Survey'] = 'surveyresultuut';
                
                break;
                break;
            case '22':
                $menu['Persetujuan Sertifikat'] = ['link' => 'approveuut', 'nilai' => count(QueriesNotif::GetNotifApproveKabalaiUut())];;
                $menu['Riwayat Order'] = 'historyorderuut';
                $menu['Perbaikan Sertifikat'] = 'revisionapproveuut';
                $menu['Surat Tugas DL'] = 'docinsituuut';
                $menu['Revisi SPT DL UTTP'] = ['link' => 'redocinsituuut', 'nilai' => 0];

                $menu['Hasil Survey'] = 'surveyresultuut';
                break;
            case '24':
                $menu['Terima UUT'] = 'receiveuut';
                //$menu['Konfirm Dinas Luar'] = 'scheduleconfirmuttp';
                //$menu['Order'] = ['UTTP' =>'orderuttp', 'UUT'=>'orderuut'];
                //$menu['Order Dinas Luar'] = 'orderluaruttp';
                $menu['Ambil Order'] = 'orderuut';
                $menu['Proses Order'] = 'serviceuut';
                $menu['Perbaikan SKHP'] = 'revisiontechorderuut';
                break;
            case '25':
                //$menu['Report Standard'] = 'frontdesk';
                $menu['Penjadwalan DL'] = 'schedulinguut';
                $menu["Resume SNSU"] = 'resumeuut';
                //$menu["Permintaan DL"] = 'requestluaruut';
                // $menu["Pelayanan DL"] = 'serviceluaruut';
                //$menu['Pengaduan SKHP'] = 'revisionuut';
                //$menu['Perbaikan SKHP'] = 'revisionorderuut';
                $menu['Riwayat Order'] = 'historyorderuut';
                // $menu["Riwayat Order DL"] = 'historyluaruut';
                // $menu["Perbaikan Sertifikat"] = 'revisionapproveuut';
                $menu['Surat Tugas DL'] = 'doctuinsituuut';
                break;
            case '26':
                $menu['Pengambilan UTTP'] = 'warehousereceiveuttp';
                $menu['Pengambilan UUT'] = 'warehousereceiveuut';
                
                $menu['Pengembalian UTTP'] = 'warehouseuttp';
                $menu['Pengembalian UUT'] = 'warehouseuut';

                $menu['Riwayat UTTP'] = 'warehousehistoryuttp';
                $menu['Riwayat UUT'] = 'warehousehistoryuut';
                break;
            case '27' :
                $menu['Verifikasi SET'] = 'vieweruttp';
                break;

            // BENDAHARA UTTP
            case '28' :
                $menu['Invoice PNBP DL'] = 'pembayaranluaruttp';
                $menu['Invoice SPUH DL'] = 'pembayaranluartuhputtp';
                $menu['Resume SPUH'] = 'resumespuhuttp';
                
                break;

            // BENDAHARA SUML
            case '29' :
                $menu['Invoice PNBP DL'] = 'pembayaranluaruut';
                $menu['Invoice SPUH DL'] = 'pembayaranluartuhpuut';
                $menu['Resume SPUH'] = 'resumespuhuut';
                
                break;
        }

        if(isset(Auth::user()->petugas_uut_id)){
            $menu["Dinas Luar UUT"] =[
                'Proses Awal' => ['link' => 'firstcheckluaruut', 'nilai'=> QueriesNotif::GetNotifFirstCheckLuarUut()],
                
                'Proses Uji'=>['link'=>'serviceprocessluaruut','nilai'=> QueriesNotif::GetNotifServiceProcessLuarUut()],
                'Proses Order'=>['link'=>'serviceluaruut','nilai'=> QueriesNotif::GetNotifServiceLuarUut()],
                'Riwayat Pelayanan'=>'servicehistoryluaruut',
                'Revisi Draft' => 'servicerevisionluaruut'
            ];
        }

        if (Auth::user()->petugas_uttp_id != null) 
        {
            if (!array_key_exists('Dinas Luar UTTP', $menu)) 
            {
                $menu['Dinas Luar UTTP'] = [
                    'Proses Awal DL' => ['link' => 'firstcheckluaruttp', 'nilai'=> QueriesNotif::GetNotifFirstCheckLuarUttp()],
                    //'Surat Tugas DL' => 'orderinsituuttp',
                    //"Proses Uji DL" => 'requestluaruttp',
                    'Proses Uji DL'=>['link'=>'serviceprocessluaruttp','nilai'=> QueriesNotif::GetNotifServiceProcessLuarUttp()],
                    'Proses Order DL'=>['link'=>'serviceluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceLuarUttp())],
                    'Revisi Draft DL'=>['link'=>'servicerevisionluaruttp','nilai'=> QueriesNotif::GetNotifServiceRevisionLuarUttp()],
                    'Riwayat Order DL'=>['link'=>'servicehistoryluaruttp','nilai'=> count(QueriesNotif::GetNotifServiceHistoryLuarUttp())]
                ];
            }
        }

        $mainmenu = null;
        $submenu = null;
        foreach ($menu as $label => $route) 
        {
            if(is_array($route))
            {
                foreach ($route as $label2 => $route2) 
                {
                    if($this->module==$route2)
                    {
                        $mainmenu = $label;
                        $submenu = $label2;
                    }
                }
            }
            else
            {
                if($this->module==$route)
                {
                    $mainmenu = $label;
                    $submenu = null;
                }
            }
        }


        $menu['active']["mainmenu"] = $mainmenu;
        $menu['active']["submenu"] = $submenu;

        return $menu;
    }
}
