<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestStatus extends Model
{
    protected $table = 'request_status';
	public $timestamps = false;
	protected $guarded = array('id');
	protected $primaryKey = 'id';

    protected $fillable = [
        'status'
    ];

    public static $rules = array(
        'status' => 'required'
    );
}
