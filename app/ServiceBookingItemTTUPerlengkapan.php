<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceBookingItemTTUPerlengkapan extends Model
{
    protected $table = 'service_booking_item_ttu_perlengkapan';
	protected $primaryKey = 'id';

    protected $fillable = ['booking_item_id', 'uttp_id',  ];

}
