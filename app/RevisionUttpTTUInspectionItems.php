<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevisionUttpTTUInspectionItems extends Model 
{
    protected $table = 'cert_rev_uttp_ttu_insp_items';
    protected $primaryKey = 'id';

    protected $fillable = [
        "cert_rev_id",
        "input_level",
        "error_up",
        "error_down",
        "error_hysteresis",
        "flow_rate",
        "error",
        "meter_factor",
        "repeatability",
    ];

    public function revision()
    {
        return $this->belongsTo('App\RevisionUttp',"cert_rev_id");
    }
}