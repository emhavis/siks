<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterPetugasUttp extends Model
{
    protected $table = 'petugas_uttp';
    protected $primaryKey = 'id';

	public $timestamps = false;

    protected $fillable = [
        'nama', 'nip', 'email', 'is_active',
        'flag_unit', 'jabatan', 'pangkat'
    ];

}
