<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttpInsituLaporan extends Model 
{
    protected $table = 'service_order_uttp_insitu_laporan';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    protected $fillable = [
        'service_order_id', 'service_request_id', 'service_request_item_id',

        'ringkasan', 'kendala_teknis', 'kendala_non_teknis',
        'metode_tindakan', 'saran_masukan',
    ];

    public function ServiceRequest()
    {
        return $this->belongsTo('App\ServiceRequestUttp',"service_request_id");
    }

    public function ServiceRequestItem()
    {
        return $this->belongsTo('App\ServiceRequestUttpItem',"service_request_item_id");
    }

    public function ServiceOrder()
    {
        return $this->belongsTo('App\ServiceOrderUttp',"service_order_id");
    }

}