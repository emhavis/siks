<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterDocTypes extends Model
{
    protected $table = 'master_doctypes';
    protected $primaryKey = 'id';

    public function dropdown()
    {
        return $this->orderBy('id')->pluck('id_type_name', 'id');
    }

}
