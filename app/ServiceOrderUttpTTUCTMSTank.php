<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttpTTUCTMSTank extends Model
{
    protected $table = 'service_order_uttp_ttu_ctms_tank';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "jenis",
        "instansi",
        "no_sertifikat", 
        "tanggal",
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
