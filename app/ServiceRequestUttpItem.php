<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestUttpItem extends Model
{
    protected $table = 'service_request_uttp_items';
    protected $primaryKey = 'id';
    protected $fillable = ['status_id','no_order','cetak_tag_created', 'cetak_tag_updated', 
        'delivered_at', 'received_at','order_at',
        'location', 'location_prov_id', 'location_kabkot_id',
        'location_lat', 'location_long', 'location_alat',
    ];

    public function serviceRequest()
    {
        return $this->belongsTo('App\ServiceRequestUttp','request_id');
    }

    public function uttp()
    {
        return $this->belongsTo('App\Uttp','uttp_id');
    }

    public function inspections()
    {
        return $this->hasMany('App\ServiceRequestUttpItemInspection','request_item_id');
    }

    public function perlengkapans()
    {
        return $this->hasMany('App\ServiceRequestUttpItemTTUPerlengkapan','request_item_id');
    }
    
    public function status()
    {
        return $this->belongsTo('App\MasterStatus','status_id');
    } 

    public function provinsi()
    {
        return $this->belongsTo('App\MasterProvince','location_prov_id');
    } 

    public function kabkot()
    {
        return $this->belongsTo('App\MasterKabupatenKota','location_kabkot_id');
    } 

    public function negara()
    {
        return $this->belongsTo('App\MasterNegara','location_negara_id');
    } 
}
