<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
// use Illuminate\Database\Eloquent\SoftDeletes;

class MasterStandardUkuran extends Model
{
    // use SoftDeletes;
    protected $table = 'master_standar_ukuran';
      // public $timestamps = false;                         // untuk data master tidak dipakai
      // protected $guarded = array('id');
      protected $primaryKey = 'id';

    // protected $fillable = [
    //     'nama_negara',
    //     'ibukota_negara'
    // ];
    public function dropdown()
    {
        return $this->orderBy('id')->select(DB::raw("CONCAT(kategori_standar_ukuran,' - ',nama_standar_ukuran) AS nama_standar_ukuran"), 'id')->pluck('nama_standar_ukuran','id');
    }

}
