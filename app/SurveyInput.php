<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SurveyInput extends Model
{
    protected $table = 'survey_input';
	protected $primaryKey = 'id';

    public function requestUttp()
    {
      return $this->belongsTo('App\ServiceRequestUttp','request_id');
    }

    public function requestUut()
    {
      return $this->belongsTo('App\ServiceRequest','request_uut_id');
    }
}
