<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttpTTUCTMS extends Model
{
    protected $table = 'service_order_uttp_ttu_ctms';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "sistem", "jenis",
        "no_sertifikat", "merek", "tipe", "serial_no",
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
