<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StandarInspectionType extends Model
{
  protected $table = 'uut_inspection_price_type';
  protected $primaryKey = 'id';
  protected $fillable = [
      'inspection_price_id',
      'uut_type_id'
    ];
  
  public function StandardToolType()
  {
    return $this->belongsTo('App\StandardToolType','standard_tool_type_id');
  }
  
  public function StandardInspectionPrice()
  {
    return $this->hasMany('App\StandardInspectionPrice','standard_detail_type_id');
  }

}
