<?php

namespace App\Excel\Imports;

use App\ServiceOrderUttps;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ServiceOrderUttps([
            //
            [
                "id" => $row[0],
                "stat_service_order" => $row[1],
                "supervisor_staff" => $row[2],
                "supervisor_entry_date" => $row[3],
                "staff_entry_dateout" => $row[4],
                "path_skhp" => $row[5],
                "file_skhp"=> $row[6],
                "lab_staff_out" => $row[7],
                "hasil_uji_memenuhi" => $row[8],
                "test_by_1" => $row[9],
                "test_by_2" => $row[10],
                "persyaratan_teknis_id" => $row[11],
                "stat_sertifikat","uttp_sla_id" => $row[12],
            ]
        ]);
    }
}
