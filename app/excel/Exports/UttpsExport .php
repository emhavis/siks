<?php

namespace App\Excel\Exports;

use App\ServiceOrderUttps;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class UttpsExport implements FromView
{
    public function view(): View
    {
        return view('resumeuttp.index', [
            'invoices' => ServiceOrderUttps::all()
        ]);
    }
    
}