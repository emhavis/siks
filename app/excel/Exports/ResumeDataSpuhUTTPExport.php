<?php

namespace App\Excel\Exports;

use App\ServiceOrderUttps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


// for titiles
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class ResumeDataSpuhUTTPExport extends DefaultValueBinder implements FromCollection, WithHeadings,WithEvents,WithTitle,WithCustomStartCell,WithColumnFormatting,WithCustomValueBinder
{
    protected $s_date, $e_date, $data;
    public function __construct($data)
    {
        $this->data=$data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
            return $this->data;
    }

    public function headings() :array
    {
        return [
            " Nomor Surat Tugas ",
            " Nomor Order ",
            " Nomor Pendaftaran ",
            " Wilayah/Provinsi ", 
            " Nama Pemilik ",
            " Nama Pemohon ",
            " Mulai Penugasan ",
            " Selesai Penugasan ",
            " Jumlah Hari ",
            " Penguji/Pemeriksa 1",
            " Penguji/Pemeriksa 2",
            " Jumlah Penguji",
            " SBM SPUH ",
            " Nominal SPUH "
        ];
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            BeforeExport::class => function(BeforeExport $event) {
                $event->writer->getProperties()->setTitle('Laporan Resume SPUH UTTP');
            },
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->mergeCells('A1:I1');
                $event->sheet->setCellValue('A1','Laporan Resume SPUH UTTP');

                $event->sheet->getDelegate()->getRowDimension('1')->setRowHeight(25);
                $event->sheet->getDelegate()->getRowDimension('2')->setRowHeight(23);
                $event->sheet->getStyle('1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getStyle('2')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle('A1:I1')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getStyle('A1:I1')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('A1:I1')->getAlignment()->setHorizontal('center');
                // 
                $event->sheet->getStyle('A2:I2')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('A2:I2')->getAlignment()->setHorizontal('center');
                //
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(10);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(10);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(10);
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(25);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(25);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(32);
                $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(35);
                
            },
        ];
    }
    public function title(): string
    {
    	return 'Laporan SPUH UTTP';
    }
    public function startCell(): string
    {
        return 'A2';
    }
    public function columnFormats(): array
    {
        return [
            //'H' => NumberFormat::FORMAT_TEXT,
            'I' => NumberFormat::FORMAT_TEXT,
        ];
    }
    public function bindValue(Cell $cell, $value)
    {
        if ($cell->getColumn() == 'I' && is_numeric($value))
        {
            $cell->setValueExplicit((float) $value, DataType::TYPE_NUMERIC);

            return true;
        }

        return parent::bindValue($cell, $value);
    }
}
