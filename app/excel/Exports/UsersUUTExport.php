<?php

namespace App\Excel\Exports;

use App\ServiceOrders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


// for titiles
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class UsersUUTExport extends DefaultValueBinder implements FromCollection, WithHeadings,WithEvents,WithTitle,WithCustomStartCell,WithColumnFormatting,WithCustomValueBinder
{
    protected $s_date, $e_date, $data;
    public function __construct($data)
    {
        $this->data=$data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
            return $this->data;
    }

    public function headings() :array
    {
        // MAKE HEADER OF DATA
        return [
            "N.P"," Nomor Order ","User ID","Nama Terdaftar", " Nama Pemesan ", " Nama Pemilik "," Jumlah Alat ", 
            " Rincian Alat ", " Obyek Pengujian " , " Tarif PNBP ", " Billing Code ",
            " NTPN "," No Kwitansi " ," Tgl Terima ", " Tgl. Selesai ",
            " Status SLA "," Laboratory ","Layanan"
        ];
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            BeforeExport::class => function(BeforeExport $event) {
                $event->writer->getProperties()->setTitle('Ini Contoh Title Laporan SNSU');
            },
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->mergeCells('A1:R1');
                $event->sheet->setCellValue('R1','Laporan Data Perkembangan SNSU');

                $event->sheet->getDelegate()->getRowDimension('1')->setRowHeight(25);
                $event->sheet->getDelegate()->getRowDimension('2')->setRowHeight(23);
                $event->sheet->getStyle('1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getStyle('2')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle('A1:R1')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getStyle('A1:R1')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('A1:R1')->getAlignment()->setHorizontal('center');
                // 
                $event->sheet->getStyle('A2:R2')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('A2:R2')->getAlignment()->setHorizontal('center');
                // $event->sheet->getStyle('A3')->getAlignment()->setHorizontal('center'); //ACJK
                // $event->sheet->getStyle('A3')->getAlignment()->setVertical('center');
                //
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(8);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(25);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(8);
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(32);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(10);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(23);
                //$event->sheet->getDelegate()->getColumnDimension('I')->setWidth(23);
                $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(23);
                $event->sheet->getDelegate()->getColumnDimension('J')->setWidth(14);
                $event->sheet->getDelegate()->getColumnDimension('K')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('L')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('M')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('N')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('O')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('P')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('Q')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('R')->setWidth(10);
            },
        ];
    }
    public function title(): string
    {
    	return 'Laporan SNSU';
    }
    public function startCell(): string
    {
        return 'A2';
    }
    public function columnFormats(): array
    {
        return [
            'I' => NumberFormat::FORMAT_TEXT,
            'J' => NumberFormat::FORMAT_TEXT,
        ];
    }
    public function bindValue(Cell $cell, $value)
    {
        if ($cell->getColumn() == 'I' || $cell->getColumn() == 'J')
        {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        return parent::bindValue($cell, $value);
    }
}
