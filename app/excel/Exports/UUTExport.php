<?php

namespace App\Excel\Exports;

use App\ServiceOrders;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class UUTExport implements FromView
{
    public function view(): View
    {
        return view('resumeuut.index', [
            'invoices' => ServiceOrders::all()
        ]);
    }
    
}