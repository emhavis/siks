<?php

namespace App\Excel\Exports;

use App\ServiceOrderUttps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


// for titiles
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class EvaluasiOrderInsituUTTPExport extends DefaultValueBinder implements FromCollection, WithHeadings,WithEvents,WithTitle,WithCustomStartCell,WithColumnFormatting,WithCustomValueBinder
{
    protected $s_date, $e_date, $data;
    public function __construct($data)
    {
        $this->data=$data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
            return $this->data;
    }

    public function headings() :array
    {
        return [
            " Nomor Order ", " Layanan ", " Nomor Surat Tugas ",  
            " Nama Pemilik ", " Nama Pemohon ", 
            " Alat ", " Lokasi ", " Penguji/Pemeriksa ", 
            " Ringkasan ",
            " Kendala Teknis ",
            " Kendala Non Teknis ",
            " Metode/Tindakan ",
            " Saran/Masukan "
        ];
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            BeforeExport::class => function(BeforeExport $event) {
                $event->writer->getProperties()->setTitle('Ini Contoh Title Laporan UTTPS');
            },
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->mergeCells('A1:M1');
                $event->sheet->setCellValue('A1','Evaluasi Order Insitu UTTP');

                $event->sheet->getDelegate()->getRowDimension('1')->setRowHeight(25);
                $event->sheet->getDelegate()->getRowDimension('2')->setRowHeight(23);
                $event->sheet->getStyle('1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getStyle('2')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle('A1:M1')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getStyle('A1:M1')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('A1:M1')->getAlignment()->setHorizontal('center');
                // 
                $event->sheet->getStyle('A2:M2')->getAlignment()->setVertical('center');
                $event->sheet->getStyle('A2:M2')->getAlignment()->setHorizontal('center');
                //
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(10);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(25);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(25);
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(32);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(23);
                $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(50);
                $event->sheet->getDelegate()->getColumnDimension('J')->setWidth(50);
                $event->sheet->getDelegate()->getColumnDimension('K')->setWidth(50);
                $event->sheet->getDelegate()->getColumnDimension('L')->setWidth(50);
                $event->sheet->getDelegate()->getColumnDimension('M')->setWidth(50);
            },
        ];
    }
    public function title(): string
    {
    	return 'Evaluasi Order Insitu UTTP';
    }
    public function startCell(): string
    {
        return 'A2';
    }
    public function columnFormats(): array
    {
        return [
           // 'I' => NumberFormat::FORMAT_TEXT,
           //  'J' => NumberFormat::FORMAT_TEXT,
        ];
    }
    public function bindValue(Cell $cell, $value)
    {
        //if ($cell->getColumn() == 'I' || $cell->getColumn() == 'J')
        //{
            //$cell->setValueExplicit($value, DataType::TYPE_STRING);

        //    return true;
        //}

        return parent::bindValue($cell, $value);
    }
}
