<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ServiceRequestItemInspection extends Model
{
    protected $table = 'service_request_item_inspections';
	// public $timestamps = false;
	// protected $guarded = array('id');
    protected $fillable = [
        'status_id',
        'service_request_item_id',
        'inspection_price_id',
        'price',
        'quantity',
        'created_at',
        'updated_at',
        'status_id',
        'status_sertifikat',
        'status_uut',
        'laboratory_id',
    ];


    public function item()
    {
        return $this->belongsTo('App\ServiceRequestItem','service_request_item_id');
    }

    public function prices()
    {
        return $this->belongsTo('App\ServiceRequestItem','service_request_item_id');
    }

    public function StandardInspectionPrice()
    {
        return $this->belongsTo('App\StandardInspectionPrice','inspection_price_id');
    }

    public function inspectionPrice(){
        return $this-> belongsTo('App\StandardInspectionPrice', 'inspection_price_id');
    }

    public function lab(){
        return $this-> belongsTo('App\MasterLaboratory', 'laboratory_id');
    }
    
}
