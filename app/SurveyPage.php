<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SurveyPage extends Model
{
    protected $table = 'survey_page';
	protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'description', 'sequence'  
    ];

    public function survey()
    {
      return $this->belongsTo('App\Survey','survey_id');
    }
}
