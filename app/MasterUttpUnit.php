<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MasterUttpUnit extends Model
{
    protected $table = 'master_uttp_units';
	protected $primaryKey = 'id';
    public $timestamps = false;


    public function uttptype(){
        return $this->BelongsTo('App\MasterUttpType','uttp_type_id');
    }

}
