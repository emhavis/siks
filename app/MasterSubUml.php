<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterSubUml extends Model
{
    protected $table = 'uml_sub';
    protected $primaryKey = 'id';
    
    public function MasterUml()
    {
        return $this->belongsTo('App\MasterUml','uml_id');
    }

}
