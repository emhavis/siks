<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterLaboratory extends Model
{
    protected $table = 'master_laboratory';
	protected $primaryKey = 'id';

    protected $fillable = [
        'nama_lab'
    ];

    /*
    public function MasterUsers()
    {
        return $this->hasMany('App\MasterUsers','laboratory_id');
    }
    */

	public function dropdown()
	{
		return $this->pluck("nama_lab","id");
    }
}
