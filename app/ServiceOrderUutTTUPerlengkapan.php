<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUutTTUPerlengkapan extends Model
{
    protected $table = 'service_order_uut_ttu_perlengkapan';
    protected $primaryKey = 'id';
    protected $fillable = ['order_id', 'uut_id', 'keterangan'];

    public function item()
    {
        return $this->belongsTo('App\ServiceOrders','order_id');
    }

    public function uuts()
    {
        return $this->belongsTo('App\Uut','uut_id');
    }

    
}
