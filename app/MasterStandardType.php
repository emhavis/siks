<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
// use Illuminate\Database\Eloquent\SoftDeletes;

class MasterStandardType extends Model
{
    // use SoftDeletes;
    protected $table = 'master_standard_types';
      // public $timestamps = false;                         // untuk data master tidak dipakai
      // protected $guarded = array('id');
      protected $primaryKey = 'id';

    protected $fillable = [
      'uut_type',
      'level',
      'jangka_waktu',
      'lab_id',
      'unit_id',
      'oiml_id',
      'kelompok',
      'kemampuan',
    ];
    // public function dropdown()
    // {
    //     return $this->orderBy('id')->select(DB::raw("CONCAT(kategori_standar_ukuran,' - ',nama_standar_ukuran) AS nama_standar_ukuran"), 'id')->pluck('nama_standar_ukuran','id');
    // }

    public function lab()
    {
      return $this->belongsTo('App\MasterLaboratory','lab_id');
    }
    public function oiml()
    {
      return $this->belongsTo('App\OIML','oiml_id');
    }

}
