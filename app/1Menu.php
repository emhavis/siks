<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';
	public $timestamps = false;
	protected $guarded = array('id');

    protected $fillable = [
        'menu_name', 'route'
    ];

    public static $rules = array(
        'menu_name' => 'required',
        'route' => 'required|unique:menus,route'
    );
}
