<?php

namespace App\Notif;

use App\ServiceOrderUttps;
use App\ServiceOrders;
use App\MasterUsers;
use App\ServiceRequestUttp;
use App\ServiceRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QueriesNotif {

    private $user_lab ;
    private $user_instalasi;

    /*
        This id for Notification Queries
    */
    public static function GetNotifServiceUttp()
    {
        // $this->user_instalasi = Auth::user()->instalasi_id;
        $count = ServiceOrderUttps::whereIn("stat_service_order",[0,1,2,3,4])
        ->whereHas('ServiceRequestItem', function($query) 
        {
            $query->where("status_id",">=", 12);
        })
        ->whereHas('ServiceRequest', function($query) 
        {
            $query->where("lokasi_pengujian","=", 'dalam');
        })
        ->where("is_finish",0)
        ->where(function($query) {
            $query->where('stat_sertifikat', 0)
                  ->orWhereNull('stat_sertifikat');
        })
        ->where(function($query) {
            $query->whereNull('test_by_1')
                  ->orWhere('test_by_1', Auth::id())
                  ->orWhere('test_by_2', Auth::id());
        })
        ->whereNull('subkoordinator_notes')
        ->where('laboratory_id', MasterUsers::where("id",Auth::id())->first()->laboratory_id)
        ->orderBy('staff_entry_datein','desc')
        ->get();

        return $count;
    }

    public static function GetNotifServiceLuarUttp()
    {
        $petugas_id = Auth::user()->petugas_uttp_id;
        $count = ServiceOrderUttps::join('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_order_uttps.service_request_item_id')
            ->join('service_request_uttps', 'service_request_uttps.id', '=', 'service_order_uttps.service_request_id')
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->whereIn("stat_service_order",[0,1,2,3,4])
            ->whereIn('service_request_uttp_items.status_id', [13, 18, 19, 20])
            ->where('service_request_uttps.lokasi_pengujian', 'luar')
            ->where('service_request_uttp_insitu_staff.scheduled_id', $petugas_id)
            /*
            ->whereHas('ServiceRequestItem', function($query)
            {
                $query->where("status_id", "=", 13);
            })
            ->whereHas('ServiceRequest', function($query) use ($petugas_id) 
            {
                $query->where("lokasi_pengujian","=", 'luar')
                ->whereHas('spuhDoc', function($query) use ($petugas_id)  {
                    $query->whereHas('listOfStaffs', function($query) use ($petugas_id)  {
                        $query->where('scheduled_id', $petugas_id);
                    });
                });
            })
            */
            ->where("is_finish",0)
            ->where(function($query) {
                $query->where('stat_sertifikat', 0)
                      ->orWhereNull('stat_sertifikat');
            })
            ->whereNull('subkoordinator_notes')
            //->where('laboratory_id', $laboratory_id)
            //->where('instalasi_id', $instalasi->id)
            ->where(function($query) {
                $query->whereNull('test_by_1')
                    ->orWhere('test_by_1', Auth::id())
                    ->orWhere('test_by_2', Auth::id());
            })
            ->orderBy('staff_entry_datein','desc')
            ->select('service_order_uttps.*')
            ->get();

        return $count;
    }

    public static function GetNotifFirstCheckLuarUttp()
    {
        $petugas_id = Auth::user()->petugas_uttp_id;
        if ($petugas_id == null) {
            $petugas_id = Auth::user()->petugas_uut_id;
        }
        $count = ServiceRequestUttp::where('status_id', 4)
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->where('service_request_uttp_insitu_staff.scheduled_id', $petugas_id)
            ->select('service_request_uttps.*')
            ->distinct()
            ->orderBy('received_date','desc')->count();

        return $count;
    }

    public static function GetNotifServiceProcessLuarUttp()
    {
        $petugas_id = Auth::user()->petugas_uttp_id;
        $count = ServiceRequestUttp::where('status_id', 12)
        ->where(function($query) {
            $query->whereIn('status_revisi_spt', [0,3])
                ->orWhereNull('status_revisi_spt');
        })
        ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
        ->where('service_request_uttp_insitu_staff.scheduled_id', $petugas_id)
        ->select('service_request_uttps.*')
        ->orderBy('received_date','desc')->count();

        return $count;
    }

    public static function GetNotifServiceHistoryLuarUttp()
    {
        $petugas_id = Auth::user()->petugas_uttp_id;
        $count = ServiceOrderUttps::whereIn("stat_service_order",[0,1,2,3,4])
        ->whereHas('ServiceRequestItem', function($query) 
        {
            $query->whereIn("status_id", [13,14,16]);
        })
        ->whereHas('ServiceRequest', function($query) 
        {
            $query->where("lokasi_pengujian","=", 'luar');
        })
        ->where('test_by_1', Auth::id())
        ->where('stat_sertifikat', '>=', 1)
        ->orderBy('staff_entry_datein','desc')
        ->get();
        return $count;
    }

    public static function GetNotifRevisionUttp()
    {
        $count = ServiceOrderUttps::whereIn("stat_service_order",[0,1,2,3,4])
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id", 13);
            })
            ->whereHas('ServiceRequest', function($query) 
            {
                $query->where("lokasi_pengujian","=", 'dalam');
            })
            ->where("is_finish",0)
            ->where(function($query) {
                $query->where('stat_sertifikat', 0)
                      ->orWhereNull('stat_sertifikat');
            })
            ->where(function($query) {
                $query->whereNull('test_by_1')
                      ->orWhere('test_by_1', Auth::id())
                      ->orWhere('test_by_2', Auth::id());
            })
            ->whereNotNull('subkoordinator_notes')
            //->where('lab_staff', Auth::id())
            ->where('laboratory_id', MasterUsers::where("id",Auth::id())->first()->laboratory_id)
            ->orderBy('staff_entry_datein','desc')
            ->get();

        return $count;
    }

    public static function GetNotifServiceRevisionLuarUttp()
    {
        $count = ServiceOrderUttps::whereIn("stat_service_order",[0,1,2,3,4])
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id", 13);
            })
            ->whereHas('ServiceRequest', function($query) 
            {
                $query->where("lokasi_pengujian","=", 'luar');
            })
            ->where("is_finish",0)
            ->where(function($query) {
                $query->where('stat_sertifikat', 0)
                    ->orWhereNull('stat_sertifikat');
            })
            ->where('test_by_1', Auth::id())
            ->whereNotNull('subkoordinator_notes')
            ->orderBy('staff_entry_datein','desc')
            ->count();


        return $count;
    }

    public static function GetNotifHistoryUttp()
    {
        $count = ServiceOrderUttps::whereIn("stat_service_order",[0,1,2,3,4])
        ->whereHas('ServiceRequestItem', function($query) 
        {
            $query->whereIn("status_id", [13,14,16]);
        })
        ->whereHas('ServiceRequest', function($query) 
        {
            $query->where("lokasi_pengujian","=", 'dalam');
        })
        ->where('stat_sertifikat', '>=', 1)
        ->where('laboratory_id', MasterUsers::where("id",Auth::id())->first()->laboratory_id)
        ->orderBy('staff_entry_datein','desc')
        ->get();

        return $count;
    }

    public static function GetNotifApproveSubko()
    {
        $count 
        = ServiceOrderUttps::with([
            'ServiceRequest', 'ServiceRequestItem', 'ServiceRequestItem.uttp',
            'MasterUsers', 'LabStaffOut',
        ])
        ->select("service_order_uttps.*")
        ->join('service_request_uttps', 'service_request_uttps.id', '=', 'service_order_uttps.service_request_id')
        ->whereIn("stat_service_order",[0,1,2,3,4])
        ->where("stat_sertifikat",1)
        ->where("is_finish",0)
        
        ->orderBy('staff_entry_datein','desc')->get();

        return $count;
    }

    public static function GetNotifApproveKabalai()
    {
        $count 
        = ServiceOrderUttps::with([
            'ServiceRequest', 'ServiceRequestItem', 'ServiceRequestItem.uttp',
            'MasterUsers', 'LabStaffOut',
        ])
        ->select("service_order_uttps.*")
        ->join('service_request_uttps', 'service_request_uttps.id', '=', 'service_order_uttps.service_request_id')
        ->whereIn("stat_service_order",[0,1,2,3,4])
        ->where("stat_sertifikat","=",2)
        ->where("is_finish",0)
        
        ->orderBy('staff_entry_datein','desc')->get();

        return $count;
    }

    public static function GetNotifSchedulingUttp()
    {
        $count = ServiceRequestUttp::where('status_id', 3)
            ->orderBy('received_date','desc')
            ->count();

        return $count;
    }

    public static function GetNotifReSchedulingUttp()
    {
        $count = ServiceRequestUttp::where('status_id', 12)
            ->where('status_revisi_spt', 1)
            ->count();

        return $count;
    }

    public static function GetNotifDocInsituUttp()
    {
        $count = ServiceRequestUttp::where('status_id', 22)
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->select('service_request_uttps.*')
            ->distinct()
            ->get();

        $count = count($count);

        return $count;
    }

    public static function GetNotifReDocInsituUttp()
    {

        $count = ServiceRequestUttp::where('status_id', 12)
            ->where('status_revisi_spt', 2)->count();

        return $count;
    }

    /*
        ==================================================
        - Unutk SNSU
        =================================================
    */

    public static function GetNotifRevisionUut()
    {

        $laboratory_id = Auth::user()->laboratory_id;

        $laboratories=[1,2];
        if($laboratory_id ==1 || $laboratory_id ==12 || $laboratory_id ==13 || $laboratory_id ==14){
            $laboratories =[1,12,13,14];
        }
        elseif($laboratory_id ==2 || $laboratory_id ==18 || $laboratory_id ==19)
        {
            $laboratories = [2,18,19];

        }
        elseif($laboratory_id ==3 || $laboratory_id ==15 || $laboratory_id ==16 || $laboratory_id ==17){
            $laboratories = [3,15,16,17];
        }else{
            $laboratories =[$laboratory_id];
        }
        $count = ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
        ->whereHas('ServiceRequestItem', function($query) 
        {
            $query->where("status_id", 13);
        })
        ->where("is_finish",0)
        ->where(function($query) {
            $query->where('stat_sertifikat', 0)
                    ->orWhere('stat_sertifikat',1)
                    ->orWhereNull('stat_sertifikat');
        })
        ->whereNotNull('subkoordinator_notes')
        ->whereIn('laboratory_id', $laboratories)   
        ->orderBy('staff_entry_datein','desc')
        ->get();

        return $count;
    }

    public static function GetNotifFirstCheckLuarUut()
    {
        $petugas_id = Auth::user()->petugas_uttp_id;
        if ($petugas_id == null) {
            $petugas_id = Auth::user()->petugas_uut_id;
        }
        $count = ServiceRequest::where('status_id', 4)
            ->leftJoin('service_request_uut_insitu_staff', 'service_request_uut_insitu_staff.doc_id', '=', 'service_requests.spuh_doc_id')
            ->where('service_request_uut_insitu_staff.scheduled_id', $petugas_id)
            ->select('service_requests.*')
            ->distinct()
            ->orderBy('received_date','desc')->count();

        return $count;
    }

    public static function GetNotifServiceLuarUut()
    {
        $petugas_id = Auth::user()->petugas_uut_id;
        if ($petugas_id == null) {
            $petugas_id = Auth::user()->petugas_uttp_id;
        }
        $count = ServiceOrders::join('service_request_items', 'service_request_items.id', '=', 'service_orders.service_request_item_id')
            ->join('service_requests', 'service_requests.id', '=', 'service_orders.service_request_id')
            ->leftJoin('service_request_uut_insitu_staff', 'service_request_uut_insitu_staff.doc_id', '=', 'service_requests.spuh_doc_id')
            ->whereIn("stat_service_order",[0,1,2,3,4])
            //->where('service_request_items.status_id', 13)
            ->where('service_requests.lokasi_pengujian', 'luar')
            ->where('service_request_uut_insitu_staff.scheduled_id', $petugas_id)
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id",">=", 11);
            })
            ->whereHas('ServiceRequest', function($query) 
            {
                $query->where("lokasi_pengujian","=", 'luar');
            })

            ->where("is_finish",0)
            ->where(function($query) {
                $query->where('stat_sertifikat', 0)
                    ->orWhereNull('stat_sertifikat');
            })
            ->whereNull('subkoordinator_notes')
            ->where(function($query) {
                $query->whereNull('test_by_1')
                    ->orWhere('test_by_1', Auth::id())
                    ->orWhere('test_by_2', Auth::id());
            })
            ->orderBy('staff_entry_datein','desc')
            ->select('service_orders.*')
            ->count();

        return $count;
    }

    public static function GetNotifApproveSubkoUut()
    {
        $count 
        = ServiceOrders::with([
            'ServiceRequest', 'ServiceRequestItem', 'ServiceRequestItem.uttp',
            'MasterUsers', 'LabStaffOut',
        ])
        ->select("service_orders.*")
        ->join('service_requests', 'service_requests.id', '=', 'service_orders.service_request_id')
        ->whereIn("stat_service_order",[0,1,2,3,4])
        ->where("stat_sertifikat",1)
        ->where("is_finish",0)
        
        ->orderBy('staff_entry_datein','desc')->get();

        return $count;
    }

    public static function GetNotifApproveKabalaiUut()
    {
        $count 
        = ServiceOrders::with([
            'ServiceRequest', 'ServiceRequestItem', 'ServiceRequestItem.uuts',
            'MasterUsers', 'LabStaffOut',
        ])
        ->select("service_orders.*")
        ->join('service_requests', 'service_requests.id', '=', 'service_orders.service_request_id')
        ->whereIn("stat_service_order",[0,1,2,3,4])
        ->where("stat_sertifikat","=",2)
        ->where("is_finish",0)
        
        ->orderBy('staff_entry_datein','desc')->get();

        return $count;
    }

    public static function GetNotifServiceProcessLuarUut()
    {
        $petugas_id = Auth::user()->petugas_uut_id;
        $count = ServiceRequest::where('status_id', 12)
        ->where(function($query) {
            $query->whereIn('status_revisi_spt', [0,3])
                ->orWhereNull('status_revisi_spt');
        })
        ->leftJoin('service_request_uut_insitu_staff', 'service_request_uut_insitu_staff.doc_id', '=', 'service_requests.spuh_doc_id')
        ->where('service_request_uut_insitu_staff.scheduled_id', $petugas_id)
        ->select('service_requests.*')
        ->orderBy('received_date','desc')->count();

        return $count;
    }

    public static function getServiceYears()
    {

        $laboratory_id = Auth::user()->laboratory_id;

        $laboratories=[1,2];
        if($laboratory_id ==1 || $laboratory_id ==12 || $laboratory_id ==13 || $laboratory_id ==14){
            $laboratories =[1,12,13,14];
        }
        elseif($laboratory_id ==2 || $laboratory_id ==18 || $laboratory_id ==19)
        {
            $laboratories = [2,18,19];

        }
        elseif($laboratory_id ==3 || $laboratory_id ==15 || $laboratory_id ==16 || $laboratory_id ==17){
            $laboratories = [3,15,16,17];
        }else{
            $laboratories =[$laboratory_id];
        }
        $count = ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
        ->whereHas('ServiceRequestItem', function($query) 
        {
            $query->where("status_id",">=", 13);
        })
        ->where("is_finish",0)
        ->where(function($query) {
            $query->whereRaw('stat_sertifikat > 0');
                    // ->orWhere('stat_sertifikat',">",1)
                    // ->orWhereNull('stat_sertifikat');
        })
        ->whereNull('subkoordinator_notes')
        ->whereIn('laboratory_id', $laboratories)   
        ->orderBy('staff_entry_datein','desc')
        ->select(DB::raw('extract(YEAR from staff_entry_datein) as year'))
        ->get()->toArray();
        // make reqular array
        $years =[];
        if(count($count) > 0){
            if(is_array($count)){
                foreach($count as $c){
                    array_push($years, $c['year']);
                }
            }
        }

        $years = array_unique($years);
        return $years;
    }


}