<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttps extends Model
{
    protected $table = 'service_order_uttps';
    protected $primaryKey = 'id';
    protected $dateFormat = 'Y-m-d H:i:s';

    protected $fillable = [
        "id",
        "stat_service_order",
        "supervisor_staff",
        "supervisor_entry_date",
        "staff_entry_dateout",
        "path_skhp",
        "file_skhp",
        "lab_staff_out",
        "hasil_uji_memenuhi",
        "test_by_1",
        "test_by_2",
        "persyaratan_teknis_id",
        "stat_sertifikat","uttp_sla_id",

        'pending_status', 'pending_notes', 'pending_created', 'pending_ended', 'pending_estimated',

        'cancel_at', 'cancel_notes', 'cancel_id',
        
        'qrcode_file', 'qrcode_token', 'qrcode_tipe_token', 'qrcode_skhpt_token',

        'sertifikat_expired_at',

        'ujitipe_completed', 'no_surat_tipe', 'no_sertifikat_tipe', 'no_sertifikat',

        'tanda_pegawai_berhak', 'tanda_daerah', 'tanda_sah', 

        'daya_baca', 'kelas_keakurasian', 'interval_skala_verifikasi', 'konstanta',
        'kelas_single_axle_load', 'kelas_single_group_load', 'metode_pengukuran', 'sistem_jaringan',
        'kelas_temperatur', 'rasio_q', 'diameter_nominal', 'kecepatan',
        'volume_bersih', 'diameter_tangki', 'tinggi_tangki',
        'kelas_massa_kendaraan', 'jumlah_nozzle', 'jenis_pompa',

        'has_set', 'is_skhpt', 'set_memenuhi',

        'kabalai_id', 'kabalai_notes', 'kabalai_date',
        'file_review_kabalai', 'path_review_kabalai',

        'subkoordinator_id', 'subkoordinator_date', 'subkoordinator_notes', 
        'file_review_subkoordinator', 'path_review_subkoordinator',

        'tool_type_id', 'tool_serial_no', 'tool_brand', 'tool_model', 'tool_type', 'tool_capacity',
        'tool_capacity_unit', 'tool_factory', 'tool_factory_address', 'tool_made_in', 'tool_made_in_id',
        'tool_owner_id', 'tool_media', 'tool_name', 'tool_capacity_min', 
        'uttp_id',

        'catatan_hasil_pemeriksaan', 'stat_warehouse', 'dasar_pemeriksaan',

        'warehouse_in_id', 'warehouse_in_at', 'warehouse_out_id', 'warehouse_out_at', 'warehouse_out_nama',

        'location', 'location_lat', 'location_long',

        "hasil_pemeriksaan_memenuhi",

        "tool_media_pengukuran",

        "mulai_uji", "selesai_uji",

        "no_sertifikat_int", "no_sertifikat_tipe_int", "no_surat_tipe_int", 
        "no_sertifikat_year", "no_sertifikat_tipe_year", "no_surat_tipe_year",

        "preview_count",

        "satuan_suhu", "satuan_output",

        "satuan_error",

        'test_by_2_sertifikat',

        'location_lat', 'location_long', 'location_alat',

        'meter_daya_baca',
    
        'draft_submit_at','expired_notif1','expired_notif2','expired_notif3',
    ];

    public function MasterUsers()
    {
        return $this->belongsTo('App\MasterUsers',"lab_staff");
    }

    public function TestBy1()
    {
        return $this->belongsTo('App\MasterUsers',"test_by_1");
    }

    public function TestBy2()
    {
        return $this->belongsTo('App\MasterUsers',"test_by_2");
    }

    public function LabStaffOut()
    {
        return $this->belongsTo('App\MasterUsers',"lab_staff_out");
    }

    public function SupervisorStaff()
    {
        return $this->belongsTo('App\MasterUsers',"supervisor_staff");
    }

    public function DitmetStaff()
    {
        return $this->belongsTo('App\MasterUsers',"ditmet_staff");
    }

    public function ServiceRequest()
    {
        return $this->belongsTo('App\ServiceRequestUttp',"service_request_id");
    }

    public function ServiceRequestItem()
    {
        return $this->belongsTo('App\ServiceRequestUttpItem',"service_request_item_id");
    }

    public function ServiceRequestItemInspection()
    {
        return $this->belongsTo('App\ServiceRequestUttpItemInspection',"service_request_item_inspection_id");
    }

    public function MasterLaboratory()
    {
        return $this->belongsTo('App\MasterLaboratory',"laboratory_id");
    }

    public function instalasi()
    {
        return $this->belongsTo('App\MasterInstalasi',"instalasi_id");
    }

    public function persyaratanTeknis()
    {
        return $this->belongsTo('App\OIML',"persyaratan_teknis_id");
    }

    public function inspections()
    {
        return $this->hasMany('App\ServiceOrderUttpInspections','order_id')->orderBy('order_no');
    }

    public function KaLab()
    {
        return $this->belongsTo('App\MasterUsers',"kalab_id");
    }

    public function SubKoordinator()
    {
        return $this->belongsTo('App\MasterUsers',"subkoordinator_id");
    }

    public function KaBalai()
    {
        return $this->belongsTo('App\MasterUsers',"kabalai_id");
    }

    public function ttu()
    {
        return $this->hasOne('App\ServiceOrderUttpTTUInspection','order_id');
    }

    public function ttuItems()
    {
        return $this->hasMany('App\ServiceOrderUttpTTUInspectionItems','order_id');
    }

    public function ttuBadanHitungs()
    {
        return $this->hasMany('App\ServiceOrderUttpTTUInspectionBadanHitung','order_id');
    }

    public function StatusSla(){
        return $this->belongsTo('App\MasterUttpSla',"uttp_sla_id");
    }

    public function ttuPerlengkapans()
    {
        return $this->hasMany('App\ServiceOrderUttpTTUPerlengkapan','order_id')->orderBy('id', 'asc');
    }

    public function uttp()
    {
        return $this->belongsTo('App\Uttp','uttp_id');
    }

    public function uttpType()
    {
        return $this->belongsTo('App\MasterUttpType','tool_type_id');
    }

    public function ttuSuhus()
    {
        return $this->hasMany('App\ServiceOrderUttpTTUInspectionSuhu','order_id');
    }

    public function ctmsTank()
    {
        return $this->hasMany('App\ServiceOrderUttpTTUCTMSTank','order_id');
    }

    public function ctms()
    {
        return $this->hasOne('App\ServiceOrderUttpTTUCTMS','order_id');
    }

    public function ctmsPerlengkapan()
    {
        return $this->hasMany('App\ServiceOrderUttpTTUCTMSPerlengkapan','order_id');
    }

    public function ctmsGauge()
    {
        return $this->hasMany('App\ServiceOrderUttpTTUCTMSGauge','order_id');
    }

    public function ctmsSistem()
    {
        return $this->hasMany('App\ServiceOrderUttpTTUCTMSSistem','order_id');
    }
}

