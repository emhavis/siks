<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttpTTUCTMSSistem extends Model
{
    protected $table = 'service_order_uttp_ttu_ctms_sistem';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "jenis",
        "no_sertifikat", 
        "serial_no_ma",
        "serial_no_pt",
        "serial_no_tt",
        "serial_no_fc",
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
