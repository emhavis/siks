<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UutInspectionItemTemplate extends Model
{
    protected $table = 'uut_inspection_item_template';
	protected $primaryKey = 'id';

}
