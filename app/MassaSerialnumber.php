<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassaSerialnumber extends Model
{
  protected $table = 'massa_serialnumber';
  protected $primaryKey = 'id';

  public function dropdown()
  {
    $rows = $this->get();

    $data[] = "-- Pilih --";
    foreach($rows as $row)
    {
        $data[$row->id] = "[".$row->id."] ".$row->kelas.", ".$row->serialnumber;
    }

    return $data;
  }

}
