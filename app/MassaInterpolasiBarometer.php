<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassaInterpolasiBarometer extends Model
{
  protected $table = 'massa_interpolasi_barometer';
  protected $primaryKey = 'id';

}
