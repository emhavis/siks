<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterUttpType extends Model
{
    protected $table = 'master_uttp_types';
	protected $primaryKey = 'id';

    public $timestamps = false;

    public function oiml()
    {
        return $this->belongsTo('App\OIML','oiml_id');
    }

    public function instalasi()
    {
        return $this->belongsTo('App\MasterInstalasi','instalasi_id');
    }
}
