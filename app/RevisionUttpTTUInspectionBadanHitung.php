<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevisionUttpTTUInspectionBadanHitung extends Model 
{
    protected $table = 'cert_rev_uttp_ttu_badanhitung';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        "order_id",
        "brand",
        "type",
        "serial_no",
    ];

    public function revision()
    {
        return $this->belongsTo('App\RevisionUttp',"cert_rev_id");
    }
}