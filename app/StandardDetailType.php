<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StandardDetailType extends Model
{
  protected $table = 'standard_detail_types';
  protected $primaryKey = 'id';
  protected $fillable = [
    'standard_detail_type_name',
    'deleted_at',
    'standard_tool_type_id',
    'created_at',
    'updated_at',
    'is_active',
    ];
  
  public function StandardToolType()
  {
    return $this->belongsTo('App\StandardToolType','standard_tool_type_id');
  }
  
  public function StandardInspectionPrice()
  {
    return $this->hasMany('App\StandardInspectionPrice','standard_detail_type_id');
  }

  public function dropdown($id)
  {
      $row = $this
      ->where('standard_tool_type_id', $id)
      ->orderBy('id')
      ->pluck('standard_detail_type_name', 'id');
      
      return response($row);
  }

  public function toolType(){
    return $this->belongsTo('App\StandardToolType','standard_tool_type_id');
  }

}
