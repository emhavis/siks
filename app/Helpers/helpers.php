<?php

if (!function_exists('encode_id'))
{
    function encode_id($params)
    {
        if($params==null) return $params;

        $secure = ['val' => $params];
        if(is_array($params))
        {
            $secure = $params;
        }

        return preg_replace('/[=]+$/', '', base64_encode(serialize($secure)));
    }
}

if (!function_exists('decode_id'))
{
    function decode_id($params)
    {
        if($params==null) return $params;
        
        $secure = unserialize(base64_decode($params));
        if(count($secure)>1)
        {
            return $secure;
        }
        return $secure['val'];
    }
}

if (!function_exists('unmask_id'))
{
    function unmask_id($id)
    {
        if(ctype_alnum($id))
        {
            return decode_id($id);
        }
        return $id;
    }
}

if (!function_exists('mask_id'))
{
    function mask_id($id)
    {
        if(is_int(intval($id)) && intval($id)>0)
        {
            return encode_id($id);
        }
        else
        {
            return $id;
        }
    }
}

if (!function_exists('userlogout'))
{
    function userlogout()
    {
        session()->flush();
        Illuminate\Support\Facades\Auth::logout();
        return redirect()->route('login');
    }
}

if (!function_exists('error_messages'))
{
    function error_messages()
    {
        return [
            "required"=>"Wajib isi",
            "date"=>"Invalid Date Format",
            "after"=>"Harus setelahnya",
            "numeric"=>"Invalid Numeric Format",
            "email"=>"Invalid Email Format"
        ];
    }
}
// if (!function_exists('get_status_approval'))
// {
//     function get_status_approval($id,$module,$mode=null)
//     {
//         $row = App\Approves::
//         whereIdTrxDocument($id)
//         ->whereDocumentType($module)
//         ->whereCreatedBy(session()->get("id_muser"))
//         ->whereIdMrole(session()->get("id_mrole"))
//         ->whereIdMgroup(session()->get("id_mgroup"))
//         ->with("muser")
//         ->orderBy("id","desc")
//         ->first();

//         if($row)
//         {
//             if($mode==="row")
//             {
//                 return $row;
//             }
//             return $row->status;
//         }
//         return null;
//     }
// }

// if (!function_exists('munit_get_by_nama'))
// {
//     function munit_get_by_nama($nama,$column)
//     {
//         $row = App\Munit::whereNama($nama)->first();
//         if($column)
//         {
//             return $row->$column;
//         }
//         return $row;
//     }
// }

// if (!function_exists('get_namas_munit'))
// {
//     function get_namas_munit()
//     {
//         $munit = App\Munit::get();
//         $id_munit = array();
//         foreach ($munit as $row)
//         {
//             $nama_munit = strtolower(trim($row->nama));
//             $id_munit[$nama_munit] = $row->id;
//             if($row->nama_alias)
//             {
//                 $aliases = explode(";", $row->nama_alias);
//                 foreach ($aliases as $alias)
//                 {
//                     $nama_munit = strtolower(trim($alias));
//                     $id_munit[$nama_munit] = $row->id;
//                 }
//             }
//         }
//         return $id_munit;
//     }
// }

// if (!function_exists('mitem_get_by_id'))
// {
//     function mitem_get_by_id($id,$column=null)
//     {
//         $row = App\Mitem::whereId($id)->first();
//         if($column)
//         {
//             return $row->$column;
//         }
//         return $row;
//     }
// }

// if (!function_exists('mcostcode_get_by_id'))
// {
//     function mcostcode_get_by_id($id,$column=null)
//     {
//         $row = App\Mcostcode::whereId($id)->first();
//         if($column!==null)
//         {
//             return $row->$column;
//         }
//         return $row;
//     }
// }

if (!function_exists('alpha_series'))
{
    function alpha_series()
    {
        return array(1=>"a","b","c","d","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
    }
}

// KODEFIKASI LEVEL 3 DAN 8 (ONE DIGIT)
// INPUT: CODE ONE DIGIT
// OUTPUT: CODE ONE DIGIT
if (!function_exists('codefication_one_digit'))
{
    function codefications_one_digit($itemcode)
    {
        $alpha = alpha_series();

        if(is_numeric($itemcode))
        {
            if($itemcode<9)
            {
                $itemcode++;
            }
            else
            {
                $itemcode = "a";
                $alphaid = 1;
            }
        }
        else
        {
            if($itemcode=="z")
            {
                $itemcode = 1;
            }
            else
            {
                $alphaid = intval(array_search($itemcode, $alpha));
                $alphaid++;
                $itemcode =  $alpha[$alphaid];
            }
        }

        return $itemcode;
    }
}

// KODEFIKASI UNTUK SELAIN LABOUR
if (!function_exists('codefications'))
{
    function codefications($code,$stat,$alphaid,$alphacode)
    {
        $alpha = alpha_series();

        if($code=="9z")
        {
            $stat = 2;
            $alphacode = 0;
            $alphaid = 1;
        }

        if($code=="z9")
        {
            $stat = 3;
            $alphacode = 1;
            $alphaid = 0;
        }

        if($code=="zz" && $stat==3)
        {
            $code = 0;
            $alphaid = 0;
            $alphacode = 1;
            $stat = 1;
        }

        if(is_numeric($code) && $code<9)
        {
            $code = intval($code)+1;
            return array($code,$stat,$alphaid,$alphacode);
        }
        else
        {
            if($stat==1)
            {
                if($alphaid<25)
                {
                    $alphaid++;
                }
                else
                {
                    $alphaid = 1;
                    $alphacode++;
                    if($alphacode>10)
                    {
                        $alphacode = 1;
                    }
                }

                $code = $alphacode.$alpha[$alphaid];
            }
            else if($stat==2)
            {
                if($alphacode<9)
                {
                    $alphacode++;
                }
                else
                {
                    $alphacode = 1;
                    $alphaid++;
                }

                $code = $alpha[$alphaid].$alphacode;
            }
            else if($stat==3)
            {
                if($alphaid<25)
                {
                    $alphaid++;
                }
                else
                {
                    $alphaid = 1;
                    $alphacode++;
                    if($alphacode>26)
                    {
                        $alphacode = 1;
                    }
                }

                $code = $alpha[$alphacode].$alpha[$alphaid];
            }

            return array($code,$stat,$alphaid,$alphacode);
        }
    }
}

// UNTUK MENDAPATKAN NILAI BARU SETELAH SEBELUMNYA DI GENERATE
// HANYA UNTUK SATU KODE
if (!function_exists('codefications_next'))
{
    function codefications_next($basecode)
    {
        $position = App\Mproject_attributes::whereParam($basecode)
        ->whereIdMproject(session()->get("id_mproject"))
        ->value("param_value");
        // dd($position);

        foreach ($position as $key => $value)
        {
            switch ($key)
            {
                case "maincode"           : $maincode           = $value; break;
                case "maincode_alphaid"   : $maincode_alphaid   = $value; break;
                case "maincode_alphacode" : $maincode_alphacode = $value; break;
                case "maincode_stat"      : $maincode_stat      = $value; break;
                case "subcode"            : $subcode            = $value; break;
                case "subcode_alphaid"    : $subcode_alphaid    = $value; break;
                case "subcode_alphacode"  : $subcode_alphacode  = $value; break;
                case "subcode_stat"       : $subcode_stat       = $value; break;
                case "itemcode"           : $itemcode           = $value; break;
            }
        }

        $itemcode = codefications_one_digit($itemcode);
        //MAIN 4-5
        if(is_numeric($maincode) && $maincode<10)
        {
            $maincode = sprintf('%02d',$maincode);
        }

        //SUBCODE 6-7
        if(is_numeric($subcode) && $subcode<10)
        {
            $subcode = sprintf('%02d',$subcode);
        }

        // $costcode = $basecode."-".$maincode."-".$subcode."-".$itemcode;
        $costcode = $basecode.$maincode.$subcode.$itemcode;

        // ADD MAINCODE
        if($subcode==="zz" && $itemcode==="z")
        {
            $arr = codefications($maincode,$maincode_stat,$maincode_alphaid,$maincode_alphacode);
            $maincode = $arr[0];
            $maincode_stat = $arr[1];
            $maincode_alphaid = $arr[2];
            $maincode_alphacode = $arr[3];
        }

        // ADD SUBCODE
        if($itemcode==="z")
        {
            $arr = codefications($subcode,$subcode_stat,$subcode_alphaid,$subcode_alphacode);
            $subcode = $arr[0];
            $subcode_stat = $arr[1];
            $subcode_alphaid = $arr[2];
            $subcode_alphacode = $arr[3];
        }

        $position = array
        (
            "maincode" => $maincode,
            "maincode_alphaid" => $maincode_alphaid,
            "maincode_alphacode" => $maincode_alphacode,
            "maincode_stat" => $maincode_stat,
            "subcode" => $subcode,
            "subcode_alphaid" => $subcode_alphaid,
            "subcode_alphacode" => $subcode_alphacode,
            "subcode_stat" => $subcode_stat,
            "itemcode" => $itemcode
        );

        App\Mproject_attributes::updateOrCreate(
            [
                "id_mproject"=>session()->get("id_mproject"),
                "param"=>$basecode
            ],
            [
                "param_value"=>$position
            ]
        );

        return $costcode;
    }
}

if (!function_exists('into_tanggal'))
{
    function into_tanggal($date,$mode=null)
    {
        switch (date("N",strtotime($date))) 
        {
            case 1: $day = "Senin"; break;
            case 2: $day = "Selasa"; break;
            case 3: $day = "Rabu"; break;
            case 4: $day = "Kamis"; break;
            case 5: $day = "Jumat"; break;
            case 6: $day = "Sabtu"; break;
            case 7: $day = "Minggu"; break;
        }

        switch (date("n",strtotime($date))) 
        {
            case 1: 
                $romawi = "I";
                $month = "Januari"; 
                break;
            case 2: 
                $romawi = "II";
                $month = "Februari"; 
                break;
            case 3: 
                $romawi = "III";
                $month = "Maret"; 
                break;
            case 4: 
                $romawi = "IV";
                $month = "April"; 
                break;
            case 5: 
                $romawi = "V";
                $month = "Mei"; 
                break;
            case 6: 
                $romawi = "VI";
                $month = "Juni"; 
                break;
            case 7: 
                $romawi = "VII";
                $month = "Juli"; 
                break;
            case 8: 
                $romawi = "VIII";
                $month = "Agustus"; 
                break;
            case 9: 
                $romawi = "IX";
                $month = "September"; 
                break;
            case 10: 
                $romawi = "X";
                $month = "Oktober"; 
                break;
            case 11: 
                $romawi = "XI";
                $month = "November"; 
                break;
            case 12: 
                $romawi = "XII";
                $month = "Desember"; 
                break;
        }

        if($mode=="romawi") return $romawi;
        else if($mode=="hari") return $day.", ".date("d",strtotime($date))." ".$month." ".date("Y",strtotime($date));
        else return date("d",strtotime($date))." ".$month." ".date("Y",strtotime($date));
    }
}

if (!function_exists('format_long_date'))
{
    function format_long_date($date) {
        $formatted = date("j-m-Y", strtotime($date));

        $parts = explode("-", $formatted);
        switch ($parts[1]) {
            case '01':
                $parts[1] = 'Januari';
                break;
            case '02':
                $parts[1] = 'Februari';
                break;
            case '03':
                $parts[1] = 'Maret';
                break;
            case '04':
                $parts[1] = 'April';
                break;
            case '05':
                $parts[1] = 'Mei';
                break;
            case '06':
                $parts[1] = 'Juni';
                break;
            case '07':
                $parts[1] = 'Juli';
                break;
            case '08':
                $parts[1] = 'Agustus';
                break;
            case '09':
                $parts[1] = 'September';
                break;
            case '10':
                $parts[1] = 'Oktober';
                break;
            case '11':
                $parts[1] = 'November';
                break;
            case '12':
                $parts[1] = 'Desember';
                break;
            default:
                $parts[1] = 'Januari';
                break;
        };

        return implode(" ", $parts);
    }
}
if (!function_exists('to_words'))
{
    function to_words($number) {

        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

    function to_words_idn($number) {

        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'Nol',
            1                   => 'Satu',
            2                   => 'Dua',
            3                   => 'Tiga',
            4                   => 'Empat',
            5                   => 'Lima',
            6                   => 'Enam',
            7                   => 'Tujuh',
            8                   => 'Delapan',
            9                   => 'Sembilan',
            10                  => 'Sepuluh',
            11                  => 'Sebelas',
            12                  => 'Dua Belas',
            13                  => 'Tiga Belas',
            14                  => 'Empat Belas',
            15                  => 'Lima Belas',
            16                  => 'Enam Belas',
            17                  => 'Tujuh Belas',
            18                  => 'Delapan Belas',
            19                  => 'Sembilan Belas',
            20                  => 'Dua Puluh',
            30                  => 'Tiga Puluh',
            40                  => 'Empat Puluh',
            50                  => 'Lima Puluh',
            60                  => 'Enam Puluh',
            70                  => 'Tujuh Puluh',
            80                  => 'Delapan Puluh',
            90                  => 'Sembilan Puluh',
            100                 => 'Seratus',
            1000                => 'Seribu',
            1000000             => 'Satu Juta',
            1000000000          => 'Satu Miliar',
            1000000000000       => 'Satu Triliun',
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }
}