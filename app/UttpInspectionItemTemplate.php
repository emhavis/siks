<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UttpInspectionItemTemplate extends Model
{
    protected $table = 'uttp_inspection_item_template';
	protected $primaryKey = 'id';

}
