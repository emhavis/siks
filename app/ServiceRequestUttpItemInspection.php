<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestUttpItemInspection extends Model
{
    protected $table = 'service_request_uttp_item_inspections';
	// public $timestamps = false;
	// protected $guarded = array('id');
    protected $primaryKey = 'id';
    protected $fillable = ['status_id','status_sertifikat','status_uttp'];

    public function item()
    {
        return $this->belongsTo('App\ServiceRequestUttpItem','request_item_id');
    }

    public function inspectionPrice()
    {
        return $this->belongsTo('App\UttpInspectionPrice','inspection_price_id');
    }
}
