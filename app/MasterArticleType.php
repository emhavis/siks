<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterArticleType extends Model
{
    protected $table = 'master_article_types';
	protected $primaryKey = 'id';
    protected $fillable = [
        'article_type'
    ];

}