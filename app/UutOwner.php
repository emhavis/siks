<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UutOwner extends Model
{
    protected $table = 'uut_owners';
	protected $primaryKey = 'id';
    protected $fillable = [
        'nama', 'alamat', 'email', 'kota_id', 'telepon', 'nib', 'npwp'
    ];

}
