<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevisionUttpTTUInspection extends Model 
{
    protected $table = 'cert_rev_uttp_ttu_inspection';
    protected $primaryKey = 'id';

    protected $fillable = [
        "cert_rev_id",
        "tank_no",
        "tag_no",
        "totalisator",
        "kfactor"
    ];

    public function revision()
    {
        return $this->belongsTo('App\RevisionUttp',"cert_rev_id");
    }
}