<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StandardUut extends Model
{
    protected $table = 'standard_uut';
	protected $primaryKey = 'id';
    

    public function StandardMeasurementType()
    {
        return $this->belongsTo('App\StandardMeasurementType','standard_measurement_type_id');
    }

    public function StandardToolType()
    {
        return $this->belongsTo('App\StandardToolType','standard_tool_type_id');
    }

    public function StandardDetailType()
    {
        return $this->belongsTo('App\StandardDetailType','standard_detail_type_id');
    }
}
