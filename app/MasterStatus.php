<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterStatus extends Model
{
    protected $table = 'master_request_status';
	protected $primaryKey = 'id';
}
