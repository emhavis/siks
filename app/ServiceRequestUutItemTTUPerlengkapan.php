<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestUutItemTTUPerlengkapan extends Model
{
    protected $table = 'service_request_uut_item_ttu_perlengkapan';
    protected $primaryKey = 'id';
    protected $fillable = ['request_item_id', 'uttp_id'];

    public function item()
    {
        return $this->belongsTo('App\ServiceRequestUttpItem','request_item_id');
    }

    public function uuts()
    {
        return $this->belongsTo('App\Uut','uut_id');
    }

    
}
