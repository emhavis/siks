<?php

namespace App\Http\Repositories;

use App\Standard;
use App\StandardInspectionPrice;
use App\StandardMeasurementType;
use App\StandardToolType;
use App\StandardDetailType;
use App\StandardMeasurementUnit;


class StandardRepository
{
    protected $standardModel;

    public function __construct(Standard $standard)
    {
        $this->standardModel = $standard;
    }

    public function getStandards()
    {
        return Standard::all();
    }

    public function getStandardById($id)
    {
        return Standard::where('id', $id)
                         ->first();
    }

    public function getStandardInspectionPricesByStandardId($standard_id)
    {
        return StandardInspectionPrice::where('standard_id', $standard_id)
                                        ->get();
    }

    public function createStandard($request){

        $newStandard = new Standard;

        // field list
        $newStandard->tool_code = $request->tool_code;
        $newStandard->standard_tool_type_id = $request->standard_tool_type_id;
        $newStandard->standard_measurement_unit_id = $request->standard_measurement_unit_id;
        $newStandard->standard_measurement_type_id = $request->standard_measurement_type_id;
        $newStandard->standard_detail_type_id = $request->standard_detail_type_id;
        $newStandard->description = $request->description;
        $newStandard->brand = $request->brand;
        $newStandard->capacity = $request->capacity;
        $newStandard->class = $request->class;
        $newStandard->model = $request->model;

        $newStandard->save();

        return $newStandard;
    }

    public function updateStandard($request, $id){

        $newStandard = $this->getStandardById($id);

        // field list
        $newStandard->tool_code = $request->tool_code;
        $newStandard->standard_tool_type_id = $request->standard_tool_type_id;
        $newStandard->standard_measurement_unit_id = $request->standard_measurement_unit_id;
        $newStandard->standard_measurement_type_id = $request->standard_measurement_type_id;
        $newStandard->standard_detail_type_id = $request->standard_detail_type_id;
        $newStandard->description = $request->description;
        $newStandard->brand = $request->brand;
        $newStandard->capacity = $request->capacity;
        $newStandard->class = $request->class;
        $newStandard->model = $request->model;

        $newStandard->save();

        return $newStandard;
    }

    public function getStandardMeasurementTypeForDropdown()
    {
        return StandardMeasurementType::orderBy('id')
                                      ->pluck('standard_type', 'id');
    }

    public function getStandardMeasurementUnitForDropdown()
    {
        return StandardMeasurementUnit::orderBy('id')
                                      ->pluck('measurement_unit', 'id');
    }

    public function getStandardToolTypeForDropDown($standardMeasurementTypeId){

        return StandardToolType::where('standard_measurement_type_id', $standardMeasurementTypeId)
                                ->orderBy('id')
                                ->pluck('attribute_name', 'id');

    }

    public function getStandardDetailTypeForDropDown($standardToolTypeId){

        return StandardDetailType::where('standard_tool_type_id', $standardToolTypeId)
                                ->orderBy('id')
                                ->pluck('standard_detail_type_name', 'id');

    }
}
