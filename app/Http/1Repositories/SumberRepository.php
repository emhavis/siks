<?php

namespace App\Http\Repositories;

use App\Sumber;

class SumberRepository
{
    protected $model;

    public function __construct(Sumber $sumber)
    {
        $this->model = $sumber;
    }

    // public function getSumber()
    // {
    //     return $this->model->all();
    // }

    public function sumberDropdown()
    {
        return $this->model->orderBy('id')
                    ->pluck('nama_sumber', 'id');
    }

}
