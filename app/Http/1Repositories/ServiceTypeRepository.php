<?php

namespace App\Http\Repositories;

use App\ServiceType;

class ServiceTypeRepository 
{
    protected $serviceTypeModel;

    public function __construct(ServiceType $serviceType) 
    {    
        $this->serviceTypeModel = $serviceType;
    }

    public function getServiceTypesForDropdown() 
    {
        return ServiceType::orderBy('id')
                            ->pluck('service_type', 'id');
    }
}