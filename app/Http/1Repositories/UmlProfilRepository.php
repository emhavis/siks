<?php

namespace App\Http\Repositories;

use App\UmlProfil;

class UmlProfilRepository
{
    protected $umlProfilModel;

    public function __construct(UmlProfil $umlProfil)
    {
        $this->umlProfilModel = $umlProfil;
    }

    public function getUmlProfils()
    {
        return UmlProfil::all();
    }

    public function getUmlProfilsForDropDown()
    {
        return UmlProfil::orderBy('id')
                    ->pluck('uml_name', 'id');
    }

    public function getUmlProfilById($id)
    {
        return UmlProfil::where('id', $id)
                    ->first();
    }

    public function updateUmlProfil($id, $uml_name, $address, $phone_no)
    {
        $uml = UmlProfil::find($id);
        $uml->uml_name = $uml_name;
        $uml->address = $address;
        $uml->phone_no = $phone_no;

        $uml->save();
    }
}
