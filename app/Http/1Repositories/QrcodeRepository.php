<?php

namespace App\Http\Repositories;

use App\Qrcode;

class QrcodeRepository
{
    protected $qrcodeModel;

    public function __construct(Qrcode $qrcode)
    {
        $this->qrcodeModel = $qrcode;
    }

    public function getQRCODEs()
    {
        return Qrcode::all();
    }

    public function getQrcodeList($uml_id,$standard_measurement_type_id,$from_qrcode,$to_qrcode)
    {
        $from_qrcode = intval($from_qrcode);
        $to_qrcode = intval($to_qrcode);

        if($from_qrcode>$to_qrcode)
        {
            $to_qrcode = $from_qrcode;
        }
        
        return Qrcode::where('uml_id',$uml_id)
                    ->where('standard_measurement_type_id',$standard_measurement_type_id)
                    ->whereBetween('id', [$from_qrcode, $to_qrcode])
                    ->pluck('tool_code');
    }

    public function getQRCODEsForDropDown($uml_id,$standard_measurement_type_id)
    {
        return Qrcode::where('uml_id',$uml_id)
                    ->where('standard_measurement_type_id',$standard_measurement_type_id)
                    ->pluck('tool_code', 'id');
    }

    public function getQRCODEById($id)
    {
        return Qrcode::with('tool_code')->where('id', $id)
                    ->first();
    }

    public function updateQRCODE($id, $tool_code, $uml_id, $standard_measurement_type_id)
    {
        $qrcode = Qrcode::find($id);
        $qrcode->tool_code = $tool_code;
        $qrcode->uml_id = $uml_id;
        $qrcode->standard_measurement_type_id = $standard_measurement_type_id;

        $qrcode->save();
    }
}
