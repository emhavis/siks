<?php

namespace App\Http\Repositories;

use App\Uml;

class UmlRepository
{
    protected $umlModel;

    public function __construct(UML $uml)
    {
        $this->umlModel = $uml;
    }

    public function getUMLs()
    {
        return UML::all();
    }

    public function getUMLsForDropDown()
    {
        return UML::orderBy('id')
                    ->pluck('uml_name', 'id');
    }

    public function getUMLById($id)
    {
        return UML::with('kabupatenkota')->where('id', $id)
                    ->first();
    }

    public function updateUML($id, $uml_name, $address, $phone_no)
    {
        $uml = UML::find($id);
        $uml->uml_name = $uml_name;
        $uml->address = $address;
        $uml->phone_no = $phone_no;

        $uml->save();
    }
}
