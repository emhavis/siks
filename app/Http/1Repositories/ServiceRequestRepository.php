<?php

namespace App\Http\Repositories;

use App\ServiceRequest;
use App\ServiceRequestItem;
use App\ServiceRequestItemInspection;
use Log;

class ServiceRequestRepository 
{
    protected $serviceRequestModel;

    public function __construct(ServiceRequest $serviceRequest) 
    {    
        $this->serviceRequestModel = $serviceRequest;
    }

    public function getServiceRequests() 
    {
        return ServiceRequest::get();
    }

    public function getServiceRequest($id)
    {
        return ServiceRequest::where('id', $id)
                    ->first();
    }

    public function getServiceRequestItems($id)
    {
        return ServiceRequestItem::where('service_request_id', $id)
                    ->get();
    }
    
    // public function createServiceRequest($serviceRequestData, $noreg, $user_id) 
    public function createServiceRequest($serviceRequestData,$noreg) 
    {
        $user_id = 3;
        $newServiceRequest = new ServiceRequest;
        $newServiceRequest->receipt_date = $this->formatServerDate($serviceRequestData->receipt_date);
        $newServiceRequest->estimate_date = $serviceRequestData->estimate_date;
        $newServiceRequest->uml_id = $serviceRequestData->uml_id;
        $newServiceRequest->pic_name = $serviceRequestData->pic_name;
        $newServiceRequest->pic_phone_no = $serviceRequestData->pic_phone_no;
        $newServiceRequest->id_type_id = $serviceRequestData->id_type_id;
        $newServiceRequest->pic_id_no = $serviceRequestData->pic_id_no;
        $newServiceRequest->service_type_id = 1;
        $newServiceRequest->status_id = 1;
        $newServiceRequest->created_at = date('Y-m-d H:i:s');
        $newServiceRequest->created_by = $user_id;
        $newServiceRequest->no_register = $noreg;
        $newServiceRequest->label_sertifikat = $serviceRequestData->label_sertifikat;

        $newServiceRequest->save();

        return $newServiceRequest;
    }

    public function updateServiceRequest($serviceRequestData, $service_request_id, $user_id) 
    {
        $serviceRequest = ServiceRequest::find($service_request_id);
        $serviceRequest->receipt_date = $this->formatServerDate($serviceRequestData->receipt_date);
        $serviceRequest->uml_standards_id = $serviceRequestData->uml_standards_id;
        $serviceRequest->entry_pic_name = $serviceRequestData->entry_pic_name;
        $serviceRequest->pic_phone_no = $serviceRequestData->pic_phone_no;
        $serviceRequest->service_type_id = 1;
        $serviceRequest->status_id = 1;
        $serviceRequest->updated_at = date('Y-m-d H:i:s');
        $serviceRequest->updated_by = $user_id;

        $serviceRequest->save();
    }

    public function updatePaymentServiceRequest($service_request_id, $payment_date, $order_id)
    {
        $serviceRequest = ServiceRequest::find($service_request_id);
        $serviceRequest->payment_date = $this->formatServerDate($payment_date);
        $serviceRequest->order_id = $order_id;
        $serviceRequest->status_id = 2;

        $serviceRequest->save();
    }

    public function deleteServiceRequest($service_request_id)
    {
        $serviceRequest = ServiceRequest::find($service_request_id);
        $serviceRequest->is_deleted = true;

        $serviceRequest->save();
    }

    public function createServiceRequestItem($service_request_id, $uml_standard_id, $quantity) 
    {
        $newItem = new ServiceRequestItem;
        $newItem->service_request_id = $service_request_id;
        $newItem->uml_standard_id = $uml_standard_id;
        $newItem->quantity = $quantity;
        
        $newItem->save();

        return $newItem;
    }

    public function createServiceRequestItemInspection($service_request_item_id, $standard_inspection_price_id, $price, $jumlah) 
    {
        $newItem = new ServiceRequestItemInspection;
        $newItem->service_request_item_id = $service_request_item_id;
        $newItem->standard_inspection_price_id = $standard_inspection_price_id;
        $newItem->price = $price;
        $newItem->qty_inspection = $jumlah;
        
        $newItem->save();

        return $newItem;
    }

    private function formatServerDate($old_date) 
    {
        $day = substr($old_date, 0, 2);
        $month = substr($old_date, 3, 2);
        $year = substr($old_date, 6, 4);

        return date_create($year . '-' . $month . '-' . $day);
    }
}
