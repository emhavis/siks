<?php

namespace App\Http\Repositories;

use App\StandardDetailType;         

class StandardDetailTypeRepository 
{

    public function __construct() 
    {    
        //
    }

    public function getStandardDetailTypes() 
    {
        return StandardDetailType::all();
    }

    public function createStandardDetailType($standard_detail_type_name){
        $newStandardDetailType = new StandardDetailType;

        // field list
        $newStandardDetailType->standard_detail_type_name = $standard_detail_type_name;

        $newStandardDetailType->save();

        return $newStandardDetailType;
    }

}