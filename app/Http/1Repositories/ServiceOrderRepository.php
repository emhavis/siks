<?php

namespace App\Http\Repositories;

use App\ServiceOrders;
use Log;

class ServiceOrderRepository 
{
    protected $serviceOrderModel;

    public function __construct(ServiceOrders $serviceOrder) 
    {    
        $this->serviceOrderModel = $serviceOrder;
    }

    public function getServiceOrders()
    {
        return ServiceOrders::all()
                             ->sortByDesc('id');
    }

    public function createServiceOrder($order_no, $service_request_id)
    {
        $newServiceOrder = new ServiceOrders;
        $newServiceOrder->order_no = $order_no;
        $newServiceOrder->service_request_id = $service_request_id;
        
        $newServiceOrder->save();

        return $newServiceOrder;
    }
}
