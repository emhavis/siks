<?php

namespace App\Http\Repositories;

use App\RequestStatus;
use Log;

class RequestStatusRepository 
{
    protected $requestStatusModel;

    public function __construct(RequestStatus $requestStatus) 
    {    
        $this->requestStatusModel = $requestStatus;
    }

    public function getRequestStatuses() 
    {
        return RequestStatus::orderBy('id')
                              ->get();
    }
}