<?php

namespace App\Http\Repositories;

use App\UmlStandard;

class UmlStandardRepository 
{
    protected $umlStandardModel;

    public function __construct(UmlStandard $umlStandard) 
    {    
        $this->umlStandardModel = $umlStandard;
    }

    public function getUmlStandardByStandardCode($standard_code, $uml_id) 
    {
        return UmlStandard::where('standard_code', $standard_code)
                            ->where('uml_id', $uml_id)
                            ->first();
    }

    public function getUmlStandardById($id)
    {
        return UmlStandard::where('id', $id)
                            ->first();
    }
}