<?php

namespace App\Http\Repositories;

use App\Umlsub;

class UmlSubRepository
{
    protected $umlModel;

    public function __construct(Umlsub $uml)
    {
        $this->umlModel = $uml;
    }

    public function getUmlSubs()
    {
        return Umlsub::all();
    }

    public function getUmlSubsForDropDown($id)
    {   
        if(intval($id)==210)
        {
            return Umlsub::orderBy('uml_sub_name')->where("id_uml",$id)->pluck('uml_sub_name', 'id');
        }
        else
        {
            return Umlsub::orderBy('uml_sub_name')->where("id_uml","<>",210)->pluck('uml_sub_name', 'id');
        }
    }

    // public function getUmlById($id)
    // {
    //     return UML::with('kabupatenkota')->where('id', $id)
    //                 ->first();
    // }

    public function updateUmlSub($id, $uml_sub_name, $address, $phone_no)
    {
        $uml = Umlsub::find($id);
        $uml->uml_name = $uml_sub_name;
        $uml->address = $address;
        $uml->phone_no = $phone_no;

        $uml->save();
    }
}
