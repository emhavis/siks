<?php

namespace App\Http\Repositories;

use App\Standard;
use App\StandardInspectionPrice;
use App\StandardMeasurementType;
use App\StandardToolType;
use App\StandardDetailType;
use App\StandardMeasurementUnit;

 
class StandardRepository
{
    protected $standardModel;

    public function __construct(Standard $standard)
    {
        $this->standardModel = $standard;
    }

    public function getStandards()
    {
        return Standard::all();
    }

    public function getStandardById($id)
    {
        // return Standard::find($id);
        return Standard::where('id', $id)->first();
    }

    public function getStandardInspectionPricesByStandardId($standard_id)
    {
        return StandardInspectionPrice::where('standard_id', $standard_id)
                                        ->get();
    }

    public function createStandard($request){

        $newStandard = new Standard;

        $newStandard->tool_code = $request->tool_code;
        $newStandard->uml_id = $request->uml_id2;
        $newStandard->uml_sub_id = $request->uml_sub_id2;
        $newStandard->standard_measurement_type_id = $request->standard_measurement_type_id2;
        $newStandard->standard_tool_type_id = $request->standard_tool_type_id;
        $newStandard->standard_detail_type_id = $request->standard_detail_type_id;
        // $newStandard->description = $request->description;

        $newStandard->sumber_id = $request->sumber_id;
        $newStandard->brand = $request->brand;
        $newStandard->made_in = $request->made_in;
        $newStandard->model = $request->model;
        $newStandard->tipe = $request->tipe;
        $newStandard->no_seri = $request->no_seri;
        $newStandard->no_identitas = $request->no_identitas;
        $newStandard->capacity = $request->capacity;
        $newStandard->daya_baca = $request->daya_baca;
        $newStandard->class = $request->class;
        $newStandard->jumlah_per_set = $request->jumlah_per_set;

        $newStandard->save();

        return $newStandard;
    }

    public function updateStandard($request, $id){

        $newStandard = $this->getStandardById($id);

        $newStandard->sumber_id = $request->sumber_id;
        $newStandard->brand = $request->brand;
        $newStandard->made_in = $request->made_in;
        $newStandard->model = $request->model;
        $newStandard->tipe = $request->tipe;
        $newStandard->no_seri = $request->no_seri;
        $newStandard->no_identitas = $request->no_identitas;
        $newStandard->capacity = $request->capacity;
        $newStandard->daya_baca = $request->daya_baca;
        $newStandard->class = $request->class;
        $newStandard->jumlah_per_set = $request->jumlah_per_set;

        $newStandard->save();

        return $newStandard;
    }

    public function getStandardMeasurementTypeForDropdown()
    {
        return StandardMeasurementType::orderBy('id')
                                      ->pluck('standard_type', 'id');
    }

    public function getStandardMeasurementUnitForDropdown()
    {
        return StandardMeasurementUnit::orderBy('id')
                                      ->pluck('measurement_unit', 'id');
    }

    public function getStandardToolTypeForDropDown($standardMeasurementTypeId){

        return StandardToolType::where('standard_measurement_type_id', $standardMeasurementTypeId)
                                ->orderBy('id')
                                ->pluck('attribute_name', 'id');

    }

    public function getStandardDetailTypeForDropDown($standardToolTypeId){

        return StandardDetailType::where('standard_tool_type_id', $standardToolTypeId)
                                ->orderBy('id')
                                ->pluck('standard_detail_type_name', 'id');

    }

    public function getInspectionPriceForDropDown($inspectionPriceId)
    {
        return StandardInspectionPrice::where('standard_detail_type_id', $inspectionPriceId)
                                ->orderBy('id')
                                ->pluck('inspection_type', 'id');

    }

    public function exists_tool_code($tool_code)
    {
        return Standard::where('tool_code', $tool_code)->get();
    }

}
