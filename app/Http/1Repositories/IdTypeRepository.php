<?php

namespace App\Http\Repositories;
use App\MasterDoctypes;

class IdTypeRepository 
{
    protected $serviceTypeModel;
    public function __construct(MasterDoctypes $idType) 
    {    
        $this->idTypeModel = $idType;
    }
    public function getIdTypesForDropdown() 
    {
        return MasterDoctypes::orderBy('id')
                       ->pluck('id_type_name', 'id');
    }
}