<?php

namespace App\Http\Repositories;

use App\Negara;

class NegaraRepository
{
    protected $negaraModel;

    public function __construct(Negara $negara)
    {
        $this->negaraModel = $negara;
    }

    public function getNEGARAs()
    {
        return Negara::all();
    }

    public function getNEGARAsForDropDown()
    {
        return Negara::orderBy('id')
                    ->pluck('nama_negara', 'id');
    }

    public function getNEGARAById($id)
    {
        return Negara::with('nama_negara')->where('id', $id)
                    ->first();
    }

    public function updateNEGARA($id, $nama_negara, $ibukota_negara)
    {
        $negara = Negara::find($id);
        $negara->nama_negara = $nama_negara;
        $negara->ibukota_negara = $ibukota_negara;

        $negara->save();
    }
}
