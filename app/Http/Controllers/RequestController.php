<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MasterServiceType;
use App\MasterDocTypes;
use App\ServiceRequest;
use App\MasterDocNumber;
use App\MasterUml;
use App\StandardInspectionPrice;
use App\MyClass\MyProjects;
use App\ServiceRequestItem;
use App\ServiceRequestItemInspection;
use App\ServiceBooking;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class RequestController extends Controller
{
    // protected $StandardInspectionPrice;
    protected $MyProjects;
    // protected $ServiceRequest;
    protected $MasterDocTypes;
    protected $MasterServiceType;
    protected $MasterUml;
    protected $ServiceBooking;
    // protected $MasterDocNumber;
    // protected $ServiceRequestItemInspection;
    // protected $ServiceRequestItem;

    public function __construct()
    {

        $this->MyProjects = new MyProjects();
        // $this->StandardInspectionPrice = new StandardInspectionPrice();
        // $this->ServiceRequest = new ServiceRequest();
        $this->MasterServiceType = new MasterServiceType();
        $this->MasterDocTypes = new MasterDocTypes();
        // $this->MasterDocNumber = new MasterDocNumber();
        $this->MasterUml = new MasterUml();
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceRequestItemInspection = new ServiceRequestItemInspection();
        $this->ServiceBooking = new ServiceBooking();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("request");
        $bookings = $this->ServiceBooking
            ->whereIn('service_type_id', [1,2])
            ->whereNotNull('booking_no')
            ->where('requested', false)
            ->where('lokasi_pengujian', 'dalam')
            ->orderBy('id')
            ->get();
        $rows = ServiceRequest::orderBy('received_date','desc')->get();
        // print_r($bookings);
        return view('request.index', compact('rows','attribute','bookings'));
    }

    public function create()
    {
        $attribute = $this->MyProjects->setup("request");

        $serviceTypes = $this->MasterServiceType->dropdown();
        $umls = $this->MasterUml->dropdown();
        $idTypes = $this->MasterDocTypes->dropdown();

        return view('request.create', compact('serviceTypes', 'idTypes', 'umls','attribute'));
    }

    public function createBooking()
    {
        $attribute = $this->MyProjects->setup("request");

        $serviceTypes = $this->MasterServiceType->dropdown();
        $bookings = $this->ServiceBooking
            ->whereIn('service_type_id', [1,2])
            ->whereNotNull('booking_no')
            ->where('requested', false)
            ->orderBy('id')->pluck('booking_no', 'id');
        $idTypes = $this->MasterDocTypes->dropdown();

        return view('request.create_by_booking', compact('serviceTypes', 'idTypes', 'bookings','attribute'));
    }

    public function simpan(Request $request)
    {
        $response["status"] = false;

        $rules['uml_id'] = ['required'];
        $rules['receipt_date'] = ['required','date'];
        $rules['estimate_date'] = ['required','date','after:receipt_date'];
        $rules['pic_name'] = ['required'];
        $rules['pic_phone_no'] = ['required'];
        $rules['pic_email'] = ['required',"email"];
        $rules['id_type_id'] = ['required'];
        $rules['pic_id_no'] = ['required'];
        $rules['standard_code'] = ['required'];
        $rules['total'] = ['required'];
        $rules['jenis_layanan'] = ['required'];
        $rules['inspection_price_ids'] = ['required'];

        if($request->for_sertifikat=="1")
        {
            $rules['label_sertifikat'] = ['required'];
            $rules['addr_sertifikat'] = ['required'];
        }

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        $no_register = MasterDocNumber::where("init","no_register")->value("param_value");
        $no_order = MasterDocNumber::where("init","no_order")->value("param_value");

        if ($validation->passes())
        {
            $alat = 0;
            $noreg_num = intval($no_register)+1;
            $noreg = sprintf("%04d",$noreg_num).date("-m-Y");

            $noorder_num = intval($no_order)+1;
            $noorder = date("y-").sprintf("%04d",$noorder_num)."-";
            // dd($request->all());
            $data = [];
            $keys = array_keys($rules);
            foreach($request->all() as $k=>$v)
            {
                if(in_array($k,$keys))
                {
                    $data[$k] = $request->get($k);
                }
            }
            $data["receipt_date"] = date("Y-m-d", strtotime($data["receipt_date"]));
            $data["estimate_date"] = date("Y-m-d", strtotime($data["estimate_date"]));

            $data["total_price"] = floatval(str_replace(",", "", $data["total"]));
            unset($data["total"]);
            unset($data["inspection_price_ids"]);
            $data["no_register"] = $noreg;
            $data["created_by"] = Auth::id();

            // dd($request->all());

            $serviceRequestID = ServiceRequest::insertGetId($data);

            if(isset($request->inspection_price_ids) &&
                count($request->inspection_price_ids)>0
            )
            {
                $noorder = $noorder.sprintf("%02d",count($request->inspection_price_ids))."-";

                foreach($request->inspection_price_ids as $standard_id=>$inspection_price_ids)
                {
                    $inspection_price_ids = array_unique(explode(",", $inspection_price_ids));

                    $serviceRequestItemID = ServiceRequestItem::insertGetId([
                    "service_request_id"=>$serviceRequestID,
                    "uml_standard_id"=>$standard_id,
                    "quantity"=>count($inspection_price_ids)
                    ]);

                    foreach($inspection_price_ids as $inspection_price_id)
                    {
                        $alat++;
                        $price = StandardInspectionPrice::whereId($inspection_price_id)->value("price");

                        ServiceRequestItemInspection::insert([
                            "service_request_item_id"=>$serviceRequestItemID,
                            "standard_inspection_price_id"=>$inspection_price_id,
                            "price"=>$price,
                            "qty_inspection"=>$request->inspection_price_jumlahs[$inspection_price_id]
                        ]);
                    }
                }

                $noorder = $noorder.sprintf("%03d",$alat);

                ServiceRequest::whereId($serviceRequestID)->update(["no_order"=>$noorder]);

                MasterDocNumber::where("init","no_register")->update(["param_value"=>$noreg_num]);

                MasterDocNumber::where("init","no_order")->update(["param_value"=>$noorder_num]);

                $response["status"] = true;
                $response["messages"] = "Data berhasil disimpan";
            }
            else
            {
                $response["messages"]["standard_code"] = 'Minimal Satu Standard Tool wajib diisi';
            }
        }

        return response($response);
    }

    public function simpanBooking(Request $request)
    {
        $response["status"] = false;

        $rules['booking_id'] = ['required'];
        $rules['receipt_date'] = ['required','date'];
        $rules['estimate_date'] = ['required','date','after:receipt_date'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        $no_register = MasterDocNumber::where("init","no_register")->value("param_value");
        $no_order = MasterDocNumber::where("init","no_order")->value("param_value");

        if ($validation->passes())
        {
            $alat = 0;
            $noreg_num = intval($no_register)+1;
            $noreg = sprintf("%04d",$noreg_num).date("-m-Y");

            $noorder_num = intval($no_order)+1;
            $noorder = date("y-").sprintf("%04d",$noorder_num)."-";
            // dd($request->all());
            $data = [];
            $keys = array_keys($rules);
            foreach($request->all() as $k=>$v)
            {
                if(in_array($k,$keys))
                {
                    $data[$k] = $request->get($k);
                }
            }

            $data["booking_id"] = $request->get("booking_id");
            $booking = $this->ServiceBooking->find($data["booking_id"]);

            $data["pic_name"] = $booking->Pic->full_name;
            $data["pic_phone_no"] = $booking->Pic->phone;
            
            $data["receipt_date"] = date("Y-m-d", strtotime($data["receipt_date"]));
            $data["estimate_date"] = date("Y-m-d", strtotime($data["estimate_date"]));

            //$data["total_price"] = floatval(str_replace(",", "", $data["total"]));
            unset($data["total"]);
            unset($data["inspection_price_ids"]);
            $data["no_register"] = $noreg;
            $data["created_by"] = Auth::id();

            // dd($request->all());

            $serviceRequestID = ServiceRequest::insertGetId($data);

            if(isset($request->inspection_price_ids) &&
                count($request->inspection_price_ids)>0
            )
            {
                

                $response["status"] = true;
                $response["messages"] = "Data berhasil disimpan";
            }
            else
            {
                $response["messages"]["standard_code"] = 'Minimal Satu Standard Tool wajib diisi';
            }
        }

        return response($response);
    }

    public function pdf($id)
    {
        $row = ServiceRequest::find($id);
        return view('request.pdf',compact('row'));
    }

    public function payment($id)
    {
        $attribute = $this->MyProjects->setup("request");
        return view('service.payment',compact('id',"attribute"));
    }

    public function paymentsave(Request $request)
    {
        $validation = Validator::make($request->all(),
        [
            "payment_date"=>'required',
            "payment_code"=>'required'
        ]);

        if ($validation->passes())
        {
            ServiceRequest::whereId($request->id)
            ->update([
                "payment_date"=>$request->payment_date,
                "payment_code"=>$request->payment_code,
                "stat_service_request"=>1
            ]);

            return response([true,"success"]);
        }
        else
        {
            return response([false,$validation->messages()]);
        }
    }

    public function paymentcancel($id)
    {
        ServiceRequest::whereId($id)
        ->where("stat_service_request",1)
        ->update([
            "payment_date"=>null,
            "payment_code"=>null,
            "stat_service_request"=>0
        ]);

        return Redirect::route('request');
    }

    public function destroy($id)
    {
        $row = ServiceRequest::whereId($id)
        ->whereNull("payment_date")
        ->whereNull("payment_code")
        ->first();

        if($row)
        {
            ServiceRequest::find($id)->delete();
            // DELETE CHILD BY TRIGGER
            // $rows = ServiceRequestItem::where("service_request_id",$id)->pluck("id");
            // ServiceRequestItem::where("service_request_id",$id)->delete();
            // ServiceRequestItemInspection::whereIn("service_request_item_id",$rows)->delete();
        }
        
        return Redirect::route('request');
    }
}
