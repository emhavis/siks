<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceRequestItem;
use App\ServiceOrders;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("order");

        // LABORATORY_ID === STANDARD_MEASUREMENT_TYPE_ID
        // ID HARUS SAMA
        $laboratory_id = Auth::user()->laboratory_id;
        $rows = ServiceRequestItem::where("stat_service_request_item",0)
        ->with([
            'ServiceRequest' => function ($query)
            {
                $query->orderBy('receipt_date', 'asc');
            }
        ])
        ->whereHas('Standard', function($query)use($laboratory_id)
        {
            $query->where("standard_measurement_type_id",$laboratory_id);
        })
        ->get();

        return view('order.index',compact('rows','attribute'));
    }    

    public function proses($service_request_item_id,$service_request_id)
    {
        ServiceOrders::insert([
            "laboratory_id"=>Auth::user()->laboratory_id, //===UML_STANDARD_ID
            "service_request_id"=>$service_request_id,
            "service_request_item_id"=>$service_request_item_id,
            "lab_staff"=>Auth::id(),
            "staff_entry_datein"=>date("Y-m-d")
        ]);

        ServiceRequest::whereId($service_request_id)
        ->update(["stat_service_request"=>2]); //ON PROSES

        ServiceRequestItem::whereId($service_request_item_id)
        ->update(["stat_service_request_item"=>1]); //ON PROSES

        return Redirect::route('order');
    }

}
