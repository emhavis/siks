<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\MasterTemplate;
use App\SurveyPage;
use App\SurveyQuestion;
use App\SurveySurvey;

class SurveyQuestionController extends Controller
{
    private $MyProjects;
    private $SurveyQuestion;
    private $SurveyPage;
    // private $SurveySurvey;
    // private $SurveyInput;
    private $MasterTemplate;
    public function __construct()
    {
        $this->SurveyQuestion = new SurveyQuestion();
        $this->MyProjects = new MyProjects();
        $this->MasterTemplate = new MasterTemplate();
        $this->SurveyPage = new SurveyPage();
    }
    public function index(){
        $data = SurveyQuestion :: with(
            [
                'MasterTemplate' => function ($query){
                    $query->orderBy('id', 'asc');
                },
            
            ])->get();
        $attribute = $this->MyProjects->setup("insitems");
        return View('surveyquestion.index',compact('data','attribute'));
    }

    public function create($id = null){
        $attribute = $this->MyProjects->setup("question");
        $page = $this->SurveyPage->pluck('title','id');

        $row = null;

        if($id)
        {
            $row = $this->SurveyQuestion->whereId($id)->first();
        }

        $templates = $this->MasterTemplate->pluck('title_tmp', 'laboratory_id');

        return View('surveyquestion.create',compact('row','id','attribute','templates','page'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules['page_id'] = ['required'];
        $rules['sequence'] = ['required'];
        $rules ['question'] = ['required'];
        $rules ['description'] = ['required'];
        $rules ['question_type'] = ['required'];
        $rules ['possible_answers'] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $request['created'] = date('Y-m-d H:i:s');
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->SurveyQuestion->whereId($id)->update($request->all());
            }
            else
            {
                $this->SurveyQuestion->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return redirect()->route('question')->with($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->UttpInspectionItem->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
    public function edit($id)
    {
        $attribute = $this->MyProjects->setup("question");
        $page = $this->SurveyPage->pluck('title','id');
        $templates = $this->MasterTemplate->pluck('title_tmp', 'laboratory_id');
        $row  = $this->SurveyQuestion->find($id);
        // dd($row);
        return view('surveyquestion.edit', compact('row','page','templates','attribute'));
    }    
    public function update(Request $request)
    {
        $response["status"] = false;

        $rules['page_id'] = ['required'];
        $rules['sequence'] = ['required'];
        $rules ['question'] = ['required'];
        $rules ['description'] = ['required'];
        $rules ['question_type'] = ['required'];
        $rules ['possible_answers'] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $request['updated'] = date('Y-m-d H:i:s');
            unset($request["_token"]);
            unset($request["_method"]);
            $id = $request->id;

            $request->request->remove('id');

            $this->SurveyQuestion->whereId($id)->update($request->all());

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return redirect()->route('question')->with($response);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->SurveyQuestion::whereId($id)
        ->first();

        if($row)
        {
            $this->SurveyQuestion::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('question')->with($response);
    }

}