<?php 

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\MasterDocNumber;
use App\MyClass\MyProjects;
use App\ServiceOrders;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class FinishedController extends Controller
{
    protected $MasterDocNumber;
    protected $ServiceOrders;
    protected $MyProjects;

    public function __construct()
    {
        $this->MasterDocNumber = new MasterDocNumber();
        $this->ServiceOrders = new ServiceOrders();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("finished");

        $data_list = ServiceOrders::where("stat_service_order",2)
        ->orderBy('supervisor_entry_date','desc')
        ->get();

        return view('finished.index',compact('data_list','attribute'));
    }

    public function proses($service_order_id)
    {
        $no_sertifikat = $this->MasterDocNumber->where("init","no_sertifikat")->value("param_value");

        $sertifikat = sprintf("%04d",$no_sertifikat).' / PKTN.4.7 / 01 / 2019';

        $this->ServiceOrders
        ->where("id",$service_order_id)
        ->update([
            "stat_service_order"=>3,
            "ditmet_staff"=>Auth::id(),
            "ditmet_entry_date"=>date("Y-m-d"),
            "no_sertifikat"=>$sertifikat
        ]);


        $this->MasterDocNumber->where("init","no_sertifikat")->update(["param_value"=>($no_sertifikat+1)]);
        return Redirect::route('finished');
    }


    public function history()
    {
        $attribute = $this->MyProjects->setup();
        
        $data_list = ServiceOrders::where("stat_service_order","<>",2)
        ->orderBy('supervisor_entry_date','desc')
        ->get();

        return view('finished.history',compact('data_list','attribute'));
    }   

}
