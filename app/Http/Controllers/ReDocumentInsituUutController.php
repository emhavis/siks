<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use App\ServiceRequest;
use App\Customer;
use App\ServiceRequestUutInsituDoc;
use App\ServiceRequestUutInsituStaff;
use App\UttpInsituCheckItem;
use App\ServiceRequestUutInsituDocCheck;
use App\HistoryUut;
use App\ServiceRequestItem;
use App\MasterServiceType;
use App\ServiceOrderUttps;
use App\MasterUsers;
use App\MasterPetugasUttp;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Mail\SuratTugasInsituUut;
use App\Mail\InvoiceTUHPUUT;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

class ReDocumentInsituUutController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("redocinsituuut");

        $rows = ServiceRequest::where('status_id', 12)
            ->where('status_revisi_spt', 2)
            ->select("*")
            ->addSelect(
                DB::raw("(select count(id) from service_request_uut_insitu_doc 
                    where request_id = service_requests.id and jenis='revisi' and (is_accepted = false or is_accepted is null)) as cnt"))
            ->orderBy('received_date','desc')->get();


        return view('redocinsituuut.index',compact('rows','attribute'));
    }

    public function approval($id) 
    {
        $attribute = $this->MyProjects->setup("redocinsituuut");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis','revisi')->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUutInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        return view('redocinsituuut.approve', compact(['request', 'requestor', 'doc', 'staffs',  'attribute']));
    }

    public function approve($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("redocinsituuut");

        $requestEntity = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);

        $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis','revisi')->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUutInsituStaff::where("doc_id", $doc->id)
                ->whereNotNull('scheduled_id')
                ->get();
        $rules = [];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
        
            if ($request->get("is_approved") === "ya" ) 
            {
                
                $token = $this->hashing($doc->doc_no, $requestEntity->no_order);

                // simpan doc
                $doc->update([
                    
                    'accepted_date' => date("Y-m-d"),
                    'accepted_by_id' => Auth::id(),

                    'token' => $token,
                ]);


                // UNTUK CUSTOMER
                $customerEmail = $requestEntity->requestor->email;;
                //Mail::to($customerEmail)->send(new BookingConfirmation($requestEntity));
                ProcessEmailJob::dispatch($customerEmail, new SuratTugasInsituUut($requestEntity, $staffs, null, $doc))
                    ->onQueue('emails');

               
                // PAYMENT TUHP
                if ($requestEntity->lokasi_pengujian == 'luar') {
                    if ($doc->invoiced_price > 0) {
                        ServiceRequest::whereId($id)
                        ->update([
                            "spuh_spt"=>$doc->doc_no,
                            "spuh_no" => $requestEntity->spuh_no,
                            "spuh_billing_date"=>date("Y-m-d"),
                            "spuh_payment_date" => null,
                            "spuh_payment_status_id" => 6,
                            "status_revisi_spt"=>3,
                            "spuh_doc_id"=>$doc->id,
                            "spuh_price"=>$doc->price,
                            "spuh_inv_price"=>$doc->invoiced_price,
                        ]);

                        $items = ServiceRequestItem::where("request_id", $id)->orderBy('id', 'asc')->get();

                        foreach($items as $item) {
                            $history = new HistoryUut();
                            $history->request_status_id = $requestEntity->status_id;
                            $history->request_id = $requestEntity->id;
                            $history->request_item_id = $item->id;
                            $history->spuh_payment_status_id = 6;
                            $history->user_id = Auth::id();
                            $history->save();
                        }

                        if ($doc->payment_date == null) {
                            $doc->billing_date = date("Y-m-d");
                        }
                        
                        $doc->billing_date = date("Y-m-d");
                        $doc->save();

                        $customerEmail = $requestEntity->requestor->email;
                        //Mail::to($customerEmail)->send(new Invoice($svcRequest));
                        ProcessEmailJob::dispatch($customerEmail, new InvoiceTUHPUUT($requestEntity, $doc))->onQueue('emails');
                    }
                    else {
                        //$doc->update([
                        //    'valid_bukti_bayar' => date("Y-m-d"),
                        //]);
                
                        ServiceRequest::whereId($request->id)
                        ->update([
                            //"spuh_payment_valid_date" => date("Y-m-d"),
                            'spuh_payment_status_id' => 8,
                            'status_revisi_spt' => 3,

                            "spuh_spt"=>$doc->doc_no,
                            "spuh_no" => $requestEntity->spuh_no,
                            
                            "spuh_doc_id"=>$doc->id,
                            "spuh_price"=>$doc->price,
                            "spuh_inv_price"=>$doc->invoiced_price,
                        ]);

                        $items = ServiceRequestItem::where("request_id", $id)->orderBy('id', 'asc')->get();

                        foreach($items as $item) {
                            $history = new HistoryUut();
                            $history->request_status_id = $requestEntity->status_id;
                            $history->request_id = $requestEntity->id;
                            $history->request_item_id = $item->id;
                            $history->spuh_payment_status_id = 8;
                            $history->user_id = Auth::id();
                            $history->save();
                        }
                    }
                }

            } else {
                
                ServiceRequest::whereId($id)
                ->update([
                    "status_revisi_spt"=> 1,
                ]);

                $doc->update([
                    'revisi_status_spt' => 1,
                    'approval_note' => $request->get('approval_note'),
                ]);

            }
            return Redirect::route('redocinsituuut');
        } else {
            return Redirect::route('redocinsituuut.approval', $id);
        }
    }

    private function hashing($username, $password) {
        $token = Hash::make($username.':'.$password);
        if (strpos($token, '/') !== false) {
            $token = $this->hashing($username, $password);
        } 
        return $token;
    }
}