<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrders;
use App\ServiceOrderInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\ServiceRequestItemInspection;
use App\ServiceRequestItem;
use App\MasterUsers;
use App\MasterServiceType;
use App\MasterStandardType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrderUutTTUInspection;
use App\ServiceOrderUutTTUInspectionItems;
use App\ServiceOrderUutTTUInspectionBadanHitung;
use App\ServiceOrderUutTTUPerlengkapan;
use App\ServiceOrderUutEvaluasiTipe;

use App\Mail\Pending;
use Illuminate\Support\Facades\Mail;
use App\Http\Libraries\PDFWatermark;
use App\Http\Libraries\PDFWatermarker;

use App\Jobs\ProcessEmailJob;

use PDF;

use App\MasterInstalasi;
use App\HistoryUut;
use LynX39\LaraPdfMerger\Facades\PdfMerger;
use setasign\Fpdi\Tcpdf\Fpdi;
use Illuminate\Support\Str;

class ServiceUutController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    protected $MasterNegara;

    public function __construct()
    {
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("serviceuut");

        $laboratory_id = Auth::user()->laboratory_id;
        $instalasi_id = Auth::user()->instalasi_id;
        // dd($laboratory_id);
        $laboratories=[1,2];
        if($laboratory_id ==1 || $laboratory_id ==12 || $laboratory_id ==13 || $laboratory_id ==14){
            $laboratories =[1,12,13,14];
        }
        elseif($laboratory_id ==2 || $laboratory_id ==18 || $laboratory_id ==19)
        {
            $laboratories = [2,18,19];

        }elseif($laboratory_id == 6 || $laboratory_id == 20 || $laboratory_id == 21 || $laboratory_id == 22)
        {
            $laboratories = [6,20,21,22];

        }
        elseif($laboratory_id ==3 || $laboratory_id ==15 || $laboratory_id ==16 || $laboratory_id ==17){
            $laboratories = [3,15,16,17];
        }elseif($laboratory_id == 6 || $laboratory_id == 20 || $laboratory_id == 21 || $laboratory_id == 22){
            $laboratories = [6,20,21,22];
        }
        else{
            $laboratories =[$laboratory_id];
        }
        
        $rows = ServiceOrders::with([
            'ServiceRequest', 'ServiceRequestItem', 'ServiceRequestItem.uuts',
            'MasterUsers', 'LabStaffOut',
        ])->whereIn("stat_service_order", [0, 1, 2, 3, 4])
        ->whereHas('ServiceRequest', function($query){
            $query->where('lokasi_pengujian', '=', 'dalam');
        })
        ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id",">=", 12)
                      ->where("status_id","!=", 16);
            })
            ->whereHas('MasterLaboratory', function($query) use( $laboratory_id,$laboratories){
                $query->whereIn('id',$laboratories);
            })
            ->where("is_finish", 0)
            ->where(function($query) {
                $query->where('stat_sertifikat', 0)
                      ->orWhereNull('stat_sertifikat');
            })
            ->whereNull('subkoordinator_notes')
            // ->orWhere('subkoordinator_notes','')
            ->orderBy('staff_entry_datein', 'desc')
            ->get();

        if (Auth::user()->user_role == '26') {
            //return view for warehouse
            return view('serviceuut.warehouse_index', compact('rows', 'attribute'));
        } else {
            //return view for other staff
            return view('serviceuut.index', compact('rows', 'attribute'));
        }
    }

    public function test($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuut.test', compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function savetest($id, Request $request)
    {
        $order = ServiceOrders::find($id);
        $de ='';  
        if($request->get("staff_entry_dateout") !=null || $request->get("staff_entry_dateout") !='')   
        {
            $de = $request->get("staff_entry_dateout");
        } else
        {
            $de = date("Y-m-d");
        }

        $order->update([
            "stat_service_order" => 1,
            "staff_entry_dateout" => date("Y-m-d",strtotime($de)),
            "stat_warehouse" => 0,
        ]);
        $item = ServiceRequestItem::find($order->service_request_item_id);
        $serviceRequest = ServiceRequest::find($order->service_request_id);

        if ($serviceRequest->service_type_id == 1 || $serviceRequest->service_type_id == 2) {
            $inspectionItems = DB::table('uut_inspection_items')
                //->where('uut_type_id', $item->uuts->type_id)
                //->where('template_id', $inspection->inspectionPrice->inspection_template_id)
                ->where('template_id', $item->uuts->stdtype->template_id)
                ->orderBy('order_no', 'asc')->get();
            foreach ($inspectionItems as $inspectionItem) {
                ServiceOrderInspections::insert([
                    'order_id' => $order->id,
                    'inspection_item_id' => $inspectionItem->id
                ]);
            }
        }

        /*
        $inspection->update([
            'status_id' => 13
        ]);

        $inspections_count = DB::table('service_request_uut_item_inspections')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_13 == $inspections_count[0]->count_all) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>13]);
        }
        */
        $item->status_id = 13;
        $item->save();

        ServiceRequestItemInspection::where("service_request_item_id", $item->id)->update([
            "status_id" => 13,
        ]);

        $items_count = DB::table('service_request_items')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_id', $order->service_request_id)->get();

        if ($items_count[0]->count_13 == $items_count[0]->count_all) {
            ServiceRequest::find($order->service_request_id)->update(['status_id' => 13]);
        }
        

        //return Redirect::route('serviceuut');
        return Redirect::route('serviceuut.result', $id);
    }

    public function savetestqr(Request $request)
    {
        $item = ServiceRequestItem::where('no_order', $request->no_order)->first();
        $order = ServiceOrders::where('service_request_item_id', $item->id)->first();

        $order->update([
            "stat_service_order" => 1,
            "staff_entry_dateout" => date("Y-m-d"),
            "stat_warehouse" => 0,
        ]);

        //$inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
        $item = ServiceRequestItem::find($order->service_request_item_id);
        $serviceRequest = ServiceRequest::find($order->service_request_id);

        /* if ($serviceRequest->service_type_id == 4 || $serviceRequest->service_type_id == 5) {
            foreach ($item->perlengkapans as $perlengkapan) {
                $ttuPerlengkapanModel = new ServiceOrderUutTTUPerlengkapan();
                $ttuPerlengkapan = [
                    'order_id' => $order->id,
                    'uut_id' => $perlengkapan->uut_id
                ];
                $ttuPerlengkapanModel->create($ttuPerlengkapan);
            }
        } else */
        if ($serviceRequest->service_type_id == 1 || $serviceRequest->service_type_id == 2) {
            $inspectionItems = DB::table('uut_inspection_items')
                //->where('uut_type_id', $item->uuts->type_id)
                //->where('template_id', $inspection->inspectionPrice->inspection_template_id)
                ->where('template_id', $item->uuts->stdtype->template_id)
                ->orderBy('order_no', 'asc')->get();
            foreach ($inspectionItems as $inspectionItem) {
                ServiceOrderInspections::insert([
                    'order_id' => $order->id,
                    'inspection_item_id' => $inspectionItem->id
                ]);
            }
        }

        /*
        $inspection->update([
            'status_id' => 13
        ]);

        $inspections_count = DB::table('service_request_uut_item_inspections')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_13 == $inspections_count[0]->count_all) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>13]);
        }
        */
        $item->status_id = 13;
        $item->save();

        ServiceRequestItemInspection::where("request_item_id", $item->id)->update([
            "status_id" => 13,
        ]);

        $items_count = DB::table('service_request_items')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_id', $order->service_request_id)->get();

        if ($items_count[0]->count_13 == $items_count[0]->count_all) {
            ServiceRequest::find($order->service_request_id)->update(['status_id' => 13]);
        }

        //return Redirect::route('serviceuut');
        return Redirect::route('serviceuut.result', $order->id);
    }

    public function result($id)
    {
        $is_at = false;
        $attribute = $this->MyProjects->setup("serviceuut");
        $serviceOrder = ServiceOrders::find($id);

        //$users = MasterUsers::pluck('full_name', 'id');
        $users = MasterUsers::whereNotNull('petugas_uut_id')
            ->whereHas('PetugasUut', function($query) 
            {
                $query->where('flag_unit', 'snsu')
                    ->where('is_active', true);
            })
            ->pluck('full_name', 'id')->prepend('-- Pilih Pegawai --','');
            
        $user = MasterUsers::find(Auth::id());

        $oimls = DB::table("master_oimls")->pluck('oiml_name', 'id');
        $inspectionItems = ServiceOrderInspections::where('order_id', $id)
            ->orderBy('id', 'asc')->get();
        $sertifikat_expired_at = date(
            'Y-m-d',
            strtotime($serviceOrder->staff_entry_dateout . ' + ' .
                ($serviceOrder->ServiceRequestItem->uuts->stdtype->jangka_waktu == null ?  0 :
                    $serviceOrder->ServiceRequestItem->uuts->stdtype->jangka_waktu) .
                ' year')
        );

        $ttu = ServiceOrderUutTTUInspection::where('order_id', $id)->first();
        $ttuItems = ServiceOrderUutTTUInspectionItems::where('order_id', $id)->get();
        $badanHitungs = ServiceOrderUutTTUInspectionBadanHitung::where('order_id', $id)->get();
        $ttuPerlengkapans = ServiceOrderUutTTUPerlengkapan::where('order_id', $id)->get();
        $tipe = ServiceOrderUutEvaluasiTipe::where('order_id', $id)->first();

        return view('serviceuut.result', compact(
            'serviceOrder',
            'attribute',
            'users',
            'user',
            'oimls',
            'ttu',
            'ttuItems',
            'ttuPerlengkapans',
            'badanHitungs',
            'inspectionItems',
            'sertifikat_expired_at',
            'tipe',
        ));
    }

    public function resultupload($id, Request $request)
    {
        // dd($request->all());
        $rules=[];
        $order = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
            'ServiceRequestItem.uuts.stdtype.oiml',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);
        
        if($request->hasil_uji_memenuhi ==0 || $request->hasil_uji_memenuhi ==False)
        {
            $rules['cancel_notes'] = ['required'];
            if($order->path_skhp ==null){
                $rules['file_skhp'] = ['required'];
            }
        }else{
            if( $order->path_lampiran == null){
                $rules['file_lampiran'] = ['required'];
                // $skhp= is_file(storage_path("app/public/sertifikat/".$order->path_lampiran));
            }
            elseif($order->path_skhp ==null){
                $rules['file_skhp'] = ['required'];
            }
        }
        // $rules['file_skhp'] = ['required'];
        // $rules['file_lampiran'] = ['required'];
        //$rules['test_by_2'] = ['required'];
        // if($serviceOrder.path_skhp !=null){

        // }

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        $serviceRequestUpdate = ServiceRequest::find($order->service_request_id);

        if ($validation->passes()) {
            if($order->path_lampiran ==null && $order->path_skhp ==null){
                $file = $request->file('file_skhp');
                $fileLampiran = $request->file('file_lampiran');

                if($fileLampiran)
                {
                    $data['file_lampiran'] = $fileLampiran->getClientOriginalName();   
                    $pathLampiran = $fileLampiran->store(
                        'sertifikat',
                        'public'
                    );
                    $data['path_lampiran'] = $pathLampiran;

                }elseif($file){
                    $data['file_skhp'] = $file->getClientOriginalName();
                    $path = $file->store(
                        'skhp',
                        'public'
                    );
    
                    $data['path_skhp'] = $path;
                }

            }else{
                if($request->file('file_skhp') != null){
                    $file = $request->file('file_skhp');
                    $data['file_skhp'] = $file->getClientOriginalName();
                    $path = $file->store(
                        'skhp',
                        'public'
                    );
                    $data['path_skhp'] = $path;
                }if($request->file('file_lampiran') != null){
                    $fileLampiran = $request->file('file_lampiran');
                    $data['file_lampiran'] = $fileLampiran->getClientOriginalName();
                    $pathLampiran = $fileLampiran->store(
                        'sertifikat',
                        'public'
                    );
                    $data['path_lampiran'] = $pathLampiran;
                }
            }
            $unit='';
            $unitmin ='';
            $dayabaca='';

            $kapasitas_req= trim(preg_replace('/\s+/',' ', $request->get("satuan")));
            $kapasitas_min_req= trim(preg_replace('/\s+/',' ', $request->get("tool_capacity_min_unit")));
            $dayabaca_req= trim(preg_replace('/\s+/',' ', $request->get("tool_dayabaca_unit")));

            if($kapasitas_min_req =="°C" || $kapasitas_min_req =="⁰C"){
                $unitmin='&deg;C';
            }elseif($kapasitas_min_req =="°F" || $kapasitas_min_req =="⁰F"){
                $unitmin='&deg;F';
            }elseif($kapasitas_min_req =="°K" || $kapasitas_min_req == "⁰K"){
                $unitmin='&deg;K';
            }
            // elseif($kapasitas_min_req =="Ω" || $kapasitas_min_req =="Ω"){
            //     $unitmin='&#8486;';
            // }
            else{
                $unitmin = $kapasitas_min_req;
            }

            if($kapasitas_req =="°C" || $kapasitas_req =="⁰C"){
                $unit='&deg;C';
            }elseif($kapasitas_req =="°F" || $kapasitas_req =="⁰F"){
                $unit='&deg;F';
            }elseif($kapasitas_req =="°K" || $kapasitas_req == "⁰K"){
                $unit='&deg;K';
            }
            // elseif($kapasitas_req =="Ω" || $kapasitas_req == "Ω"){
            //     $unit='&#8486;';
            // }
            else{
                $unit = $kapasitas_req;
            }
            if($dayabaca_req =="°C" || $dayabaca_req == "⁰C"){
                $dayabaca='&deg;C';
            }elseif($dayabaca_req =="°F" || $dayabaca_req =="⁰F"){
                $dayabaca='&deg;F';
            }elseif($dayabaca_req =="°K" || $dayabaca_req == "⁰K"){
                $dayabaca='&deg;K';
            }
            
            else{
                $dayabaca = $dayabaca_req;
            }

            $stat_s ='';
            if($request->get("stat_sertifikat") < 1){
                $stat_s = null;
            }else{
                $stat_s = $request->get("stat_sertifikat");
            }

            $data['test_by_1'] = Auth::id();
            $data['test_by_2'] = $request->get("test_by_2");
            $data['persyaratan_teknis_id'] = $request->get("persyaratan_teknis_id");
            $data['stat_sertifikat'] = 1;
            $data['stat_service_order'] = 2;
            $data['stat_sertifikat'] = $stat_s;
            $data['hasil_uji_memenuhi'] = $request->get("hasil_uji_memenuhi"); 
            $data["tool_capacity"] = $request->get("kapasitas");
            $data["tool_capacity_unit"] = $unit; //$request->get("satuan");
            $data["tool_made_in"] = $request->get("buatan");

            $data["tool_capacity_min"] = $request->get("tool_capacity_min");
            $data["tool_capacity_min_unit"] = $unitmin; //$request->get("satuan");

            $data["tool_name"] = $request->get("tool_name");
            $data["tool_dayabaca"] = $request->get("tool_dayabaca");
            $data["tool_dayabaca_unit"] = $dayabaca;//$request->get("tool_dayabaca_unit");
            $data["tool_brand"] = $request->get("tool_brand");
            $data["tool_model"] = $request->get("tool_model");
            $data["tool_serial_no"] = $request->get("tool_serial_no");
            $data["tool_class"] = $request->get("tool_class");
            $data["tool_jumlah"] = $request->get("tool_jumlah");
            $data["is_certified"] = $request->get("is_certified");
            $data["page_num"] = $request->get("page_num");
            $data["cancel_notes"] = $request->get("cancel_notes");
            
            $dataRequest["addr_sertifikat"] = $request->get("addr_sertifikat");
            $dataRequest["label_sertifikat"] = $request->get("label_sertifikat");
            // Update Data On Service Request
            $serviceRequestUpdate->update($dataRequest);

            if($request->get("sertifikat_expired_at") !='' || $request->get("sertifikat_expired_at") !=null){
                $data['sertifikat_expired_at'] = date('Y-m-d', strtotime($request->get("sertifikat_expired_at")));
            }

            // if ($order->ServiceRequest->service_type_id == 4 || $order->ServiceRequest->service_type_id == 5) {
            //     $data['sertifikat_expired_at'] = date("Y-m-d", strtotime($request->input("sertifikat_expired_at")));

            //     ServiceOrderUutTTUPerlengkapan::where("order_id", $id)->delete();

            //     if ($request->has('perlengkapan_id')) {
            //         foreach ($request->get('perlengkapan_id') as $index => $value) {
            //             ServiceOrderUutTTUPerlengkapan::create([
            //                 "order_id" => $id,
            //                 "uut_id" => $request->input("perlengkapan_uut_id." . $index),
            //                 "keterangan" => $request->input("perlangkapan_keterangan." . $index),
            //             ]);
            //         }
            //     }

            //     if ($order->ServiceRequestItem->uuts->stdtype->kelompok == 'Automatic Level Gauge') {
            //         ServiceOrderUutTTUInspectionBadanHitung::where("order_id", $id)->delete();
            //         ServiceOrderUutTTUInspection::where("order_id", $id)->delete();
            //         ServiceOrderUutTTUInspectionItems::where("order_id", $id)->delete();

            //         foreach ($request->get('badanhitung_item_id') as $index => $value) {
            //             ServiceOrderUutTTUInspectionBadanHitung::insert([
            //                 "order_id" => $id,
            //                 "type" => $request->input("type." . $index),
            //                 "serial_no" => $request->input("serial_no." . $index),
            //             ]);
            //         }

            //         ServiceOrderUutTTUInspection::insert([
            //             "order_id" => $id,
            //             "tank_no" => $request->input("tank_no"),
            //             "tag_no" => $request->input("tag_no"),
            //         ]);

            //         foreach ($request->get('item_id') as $index => $value) {
            //             ServiceOrderUutTTUInspectionItems::insert([
            //                 "order_id" => $id,
            //                 "input_level" => $request->input("input_level." . $index),
            //                 "error_up" => $request->input("error_up." . $index),
            //                 "error_down" => $request->input("error_down." . $index),
            //                 "error_hysteresis" => $request->input("error_hysteresis." . $index),
            //             ]);
            //         }
            //     }
            //     if ($order->ServiceRequestItem->uuts->stdtype->kelompok == 'Meter Arus BBM') {
            //         ServiceOrderUutTTUInspectionBadanHitung::where("order_id", $id)->delete();
            //         ServiceOrderUutTTUInspection::where("order_id", $id)->delete();
            //         ServiceOrderUutTTUInspectionItems::where("order_id", $id)->delete();

            //         foreach ($request->get('badanhitung_item_id') as $index => $value) {
            //             ServiceOrderUutTTUInspectionBadanHitung::insert([
            //                 "order_id" => $id,
            //                 "brand" => $request->input("brand." . $index),
            //                 "type" => $request->input("type." . $index),
            //                 "serial_no" => $request->input("serial_no." . $index),
            //             ]);
            //         }

            //         ServiceOrderUutTTUInspection::insert([
            //             "order_id" => $id,
            //             "totalisator" => $request->input("totalisator"),
            //             "kfactor" => $request->input("kfactor"),
            //         ]);

            //         foreach ($request->get('item_id') as $index => $value) {
            //             ServiceOrderUutTTUInspectionItems::insert([
            //                 "order_id" => $id,
            //                 "flow_rate" => $request->input("flow_rate." . $index),
            //                 "error" => $request->input("error." . $index),
            //                 "meter_factor" => $request->input("meter_factor." . $index),
            //                 "repeatability_bkd" => $request->input("repeatability." . $index),
            //             ]);
            //         }
            //     }
            //     if ($order->ServiceRequestItem->uuts->stdtype->kelompok == 'Meter Gas') {
            //         ServiceOrderUutTTUInspection::where("order_id", $id)->delete();
            //         ServiceOrderUutTTUInspectionItems::where("order_id", $id)->delete();

            //         ServiceOrderUutTTUInspection::insert([
            //             "order_id" => $id,
            //             "totalisator" => $request->input("totalisator"),
            //             "kfactor" => $request->input("kfactor"),
            //         ]);

            //         foreach ($request->get('item_id') as $index => $value) {
            //             ServiceOrderUutTTUInspectionItems::insert([
            //                 "order_id" => $id,
            //                 "flow_rate" => $request->input("flow_rate." . $index),
            //                 "error" => $request->input("error." . $index),
            //                 "repeatability" => $request->input("repeatability." . $index),
            //             ]);
            //         }
            //     }
            //     if ($order->ServiceRequestItem->uuts->stdtype->kelompok == 'Meter Air') {
            //         ServiceOrderUutTTUInspection::where("order_id", $id)->delete();
            //         ServiceOrderUutTTUInspectionItems::where("order_id", $id)->delete();

            //         ServiceOrderUutTTUInspection::insert([
            //             "order_id" => $id,
            //             "totalisator" => $request->input("totalisator"),
            //         ]);

            //         foreach ($request->get('item_id') as $index => $value) {
            //             ServiceOrderUutTTUInspectionItems::insert([
            //                 "order_id" => $id,
            //                 "flow_rate" => $request->input("flow_rate." . $index),
            //                 "error" => $request->input("error." . $index),
            //                 "repeatability" => $request->input("repeatability." . $index),
            //             ]);
            //         }
            //     }
            //     if ($order->ServiceRequestItem->uuts->stdtype->kelompok == 'Meter Gas (USM WetCal)') {
            //         ServiceOrderUutTTUInspection::where("order_id", $id)->delete();
            //         ServiceOrderUutTTUInspectionItems::where("order_id", $id)->delete();

            //         ServiceOrderUutTTUInspection::insert([
            //             "order_id" => $id,
            //             "totalisator" => $request->input("totalisator"),
            //             "kfactor" => $request->input("kfactor"),
            //         ]);

            //         foreach ($request->get('item_id') as $index => $value) {
            //             ServiceOrderUutTTUInspectionItems::insert([
            //                 "order_id" => $id,
            //                 "flow_rate" => $request->input("flow_rate." . $index),
            //                 "error" => $request->input("error." . $index),
            //                 "error_bkd" => $request->input("error_bkd." . $index),
            //                 "repeatability" => $request->input("repeatability." . $index),
            //                 "repeatability_bkd" => $request->input("repeatability_bkd." . $index),
            //             ]);
            //         }
            //     }
            // } elseif ($order->ServiceRequest->service_type_id == 6 || $order->ServiceRequest->service_type_id == 7) {
            //     $inspectionItems = ServiceOrderInspections::where('order_id', $id)
            //         ->orderBy('id', 'asc')->get();

            //     $memenuhi = true;
            //     foreach ($inspectionItems as $inspectionItem) {
            //         if ($request->has("is_accepted_" . $inspectionItem->id)) {
            //             $memenuhi = $memenuhi && !($request->get("is_accepted_" . $inspectionItem->id) === "tidak");
            //             $val = $request->get("is_accepted_" . $inspectionItem->id) === "ya" ? true : ($request->get("is_accepted_" . $inspectionItem->id) === "tidak" ? false : null);

            //             ServiceOrderInspections::find($inspectionItem->id)->update([
            //                 'is_accepted' => $val
            //             ]);
            //         }
            //     }
            //     $data['hasil_uji_memenuhi'] = $memenuhi;

            //     $mustInpects = DB::table('uut_inspection_prices')
            //         ->join('uut_inspection_price_types', 'uut_inspection_price_types.inspection_price_id', '=', 'uut_inspection_prices.id')
            //         ->leftJoin('service_request_item_inspections', 'service_request_item_inspections.inspection_price_id', '=', 'uut_inspection_prices.id')
            //         ->leftJoin('service_orders', 'service_orders.service_request_item_inspection_id', '=', 'service_request_item_inspections.id')
            //         ->where('uut_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
            //         ->where('uut_inspection_price_types.uut_type_id', $order->ServiceRequestItem->uuts->type_id)
            //         ->where(function ($query) {
            //             $query->whereNull('service_orders.ujitipe_completed')
            //                 ->orWhere('service_orders.ujitipe_completed', false);
            //         })
            //         ->selectRaw('uut_inspection_prices.id as price_id, service_orders.id as order_id')
            //         ->get();
                
            //     $notOrderedYet = $mustInpects->filter(function ($value, $key) {
            //         return $value->order_id == null;
            //     });

            //     if (count($notOrderedYet) == 0) {
            //         $ordersToUpdate = $mustInpects->map(function ($item, $key) {
            //             return $item->order_id;
            //         })->toArray();

            //         ServiceOrders::whereIn('id', $ordersToUpdate)->update([
            //             'ujitipe_completed' => true
            //         ]);

            //         ServiceOrderUutEvaluasiTipe::where("order_id", $id)->delete();
            //         ServiceOrderUutEvaluasiTipe::insert([
            //             "order_id" => $id,
            //             "kelas_akurasi" => $request->input("kelas_akurasi"),
            //             "daya_baca" => $request->input("daya_baca"),
            //             "interval_skala_verifikasi" => $request->input("interval_skala_verifikasi"),
            //         ]);
            //     }
            // }

            $order->update($data);
            /*
            $inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
            $inspection->update([
                'status_sertifikat' => 1
            ]);
            */

            //$this->checkAndUpdateFinishOrder($id);

            return Redirect::route('serviceuut');
        } else {
            return Redirect::route('serviceuut.result', $id);
        }
    }

    public function approve($id)
    {
        $attribute = $this->MyProjects->setup("serviceuut");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis',
            'inspections' => function ($query) {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        return view('serviceuut.approve', compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function approvesubmit($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest',
        ])->find($id);
        $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
        $lastNo = $serviceType->last_no;
        $nextNo = intval($lastNo) + 1;
        $noSertifikat = sprintf("%d", $nextNo) . '/25/'.$order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'IN': 'EX' . date("m/Y");        
        $noService = $serviceType->prefix . '-' . sprintf("%05d", $nextNo);

        ServiceOrders::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            "no_service" => $noService,
            "supervisor_staff" => Auth::id(),
            "supervisor_entry_date" => date("Y-m-d"),
            "stat_service_order" => 3
        ]);

        MasterServiceType::where("id", $id)->update(["last_no" => $nextNo]);

        $this->checkAndUpdateFinishOrder($id);

        return Redirect::route('serviceuut');
    }

    public function warehouse($id)
    {
        $order = ServiceOrders::find($id);
        ServiceOrders::whereId($id)->update(["stat_warehouse" => 1]);

        // $inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
        // $inspection->update([
        //     'status_uut' => 1
        // ]);

        $this->checkAndUpdateFinishOrder($id);

        return Redirect::route('serviceuut');
    }

    public function finishorder($service_order_id)
    {
        DB::table("service_orders")
            ->where("id", $service_order_id)
            ->update(["is_finish" => 1]);
        return Redirect::route('serviceuut');
    }

    public function download($id)
    {
        $order = ServiceOrders::find($id);
        if($order->path_skhp !=null && $order->file_skhp !=null){
            $nolam = str_replace('/','_',$order->no_sertifikat);
            return Storage::disk('public')->download($order->path_skhp, 'Cerapan--'.$nolam.'.pdf');//$order->file_skhp);
        }else{
            abort(404);
        }
    }

    public function cancelation_download($id)
    {
        $order = ServiceOrders::find($id);
        if($order->cancelation_file !=null && $order->cancelation_file !=null){
            $nolam = str_replace('/','_',$order->no_sertifikat);
            return Storage::disk('public')->download($order->cancelation_file, 'Cerapan--'.$nolam.'.pdf');//$order->file_skhp);
        }else{
            abort(404);
        }
    }

    public function downloadLampiran($id)
    {
        $order = ServiceOrders::find($id);

        if($order->path_lampiran != null && $order->file_lampiran !=null){
            $nolam = str_replace('/','_',$order->no_sertifikat);
            return Storage::disk('public')->download($order->path_lampiran, 'Lampiran-'.$nolam.'.pdf');//$order->file_lampiran);
        }else{
            abort(404);
        }
    }

    public function preview($id)
    {
        $pdfMerger = PdfMerger::init();
        $order = ServiceOrders::find($id);
        
         $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = '';
        // route('documentuut.valid', [
        //     'jenis_sertifikat' => 'skhp',
        //     'token' => $order->qrcode_token,
        // ]);

        $blade ='serviceuut.skhp_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([

            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo'),
            'defaultPaperSize' => 'a4',
            'defaultMediaType' => 'print',
        ])
            ->loadview($blade, compact('order', 'qrcode_generator', 'view'));

        return $pdf->stream();
    }

    public function print($id,$stream=0)
    {
        $pdfMerger = PdfMerger::init();
        $order = ServiceOrders::find($id);
         $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = route('documentuut.valid',
         [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);
        
        // $blade = 'serviceuut.skhp_pdf';
        $blade ='serviceuut.skhp_tipe_pdf';
        

        $view = false;

        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo'),
            'defaultPaperSize' => 'a4',
            'defaultMediaType' => 'print',
        ])
            ->loadview($blade, compact('order', 'qrcode_generator', 'view'));
        
            $file = public_path('storage/images/file.pdf');

            if(! file_exists($file)){
                // Storage::delete('public/tmp/file.pdf');
            }

        Storage::put('public/tmp/file.pdf', $pdf->output());
        $pdfMerger->addPDF(storage_path('app/public/tmp/file.pdf'),'all');
        if($order->kabalai_id ==null || $order->kabalai_id =''){
            return $pdf->stream();
        }

        if($order->path_lampiran !=null){
            $pdfMerger->addPDF(storage_path('app/public/'.$order->path_lampiran),'all');
            $pdfMerger->merge();
            $pdfMerger->save(storage_path('app/public/sample.pdf'));
            $this->doWatermark('app/public/sample.pdf',$order->qrcode_token,$order->no_sertifikat,$order->id);
            
            $no = str_replace('/','_',$order->no_sertifikat);
            $no ='Lampiran-No.'.$no;
            
            $file = storage_path('app/public/tmp/SERTIFIKAT-'.$no.'.pdf','P');
            if(is_file($file))
            {
                return response()->download($file);
                // 1. possibility
                Storage::delete($file);
                // 2. possibility
                unlink(storage_path($file));
            }
        }
        if($order->cancelation_file !=null){
            if($order->no_sertifikat ==null){
                $order->no_sertifikat ="UNDEF";
            }
            $pdfMerger->addPDF(storage_path('app/public/'.$order->cancelation_file),'all');
            $pdfMerger->merge();
            $pdfMerger->save(storage_path('app/public/sample_cancel.pdf'));
            $this->doWatermark('app/public/sample_cancel.pdf',$order->qrcode_token,$order->no_sertifikat,$order->id);
            
            $no = str_replace('/','_',$order->no_sertifikat);
            $no ='Lampiran-No.'.$no;
            $file = storage_path('app/public/tmp/SBL-'.$no.'.pdf','P');

            if(is_file($file))
            {
                return response()->download($file);
                // 1. possibility
                Storage::delete($file);
                // 2. possibility
                unlink(storage_path($file));
            }
        }
        return $pdf->stream();
    }

    public function previewTipe($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis',
            'inspections' => function ($query) {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $otherOrders = DB::table('uut_inspection_prices')
            ->join('uut_inspection_price_types', 'uut_inspection_price_types.inspection_price_id', '=', 'uut_inspection_prices.id')
            ->leftJoin('service_request_item_inspections', 'service_request_item_inspections.inspection_price_id', '=', 'uut_inspection_prices.id')
            ->join('service_orders', 'service_orders.service_request_item_inspection_id', '=', 'service_request_item_inspections.id')
            ->where('uut_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
            ->where('uut_inspection_price_types.uut_type_id', $order->ServiceRequestItem->uuts->type_id)
            ->where('service_orders.ujitipe_completed', true)
            ->selectRaw('service_orders.*')
            ->get();

        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuut.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);

        $blade = 'serviceuut.skhp_pdf_tipe';

        $view = true;
        //dd($order);
        return view($blade, compact(
            'order',
            'otherOrders',
            'qrcode_generator',
            'view'
        ));
    }

    public function printTipe($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis',
            'inspections' => function ($query) {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $fie_name = 'skhp' . $order->ServiceRequestItem->no_order;
        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuut.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);

        $blade = 'serviceuut.skhp_pdf_tipe';

        $view = false;

        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true,
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo')
        ])
            ->loadview($blade, compact('order', 'qrcode_generator', 'view'));

        return $pdf->download('skhp.pdf');
    }

    private function checkAndUpdateFinishOrder($id)
    {
        $order = ServiceOrders::find($id);

        /*
        $inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
        if ($inspection->status_sertifikat == 1 && $inspection->status_uttp == 1) {
            $inspection->update(['status_id' => 14]); 
        }
        
        $inspections_count = DB::table('service_request_item_inspections')
            ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
       */ /*
        if ($inspections_count[0]->count_14 == $inspections_count[0]->count_all) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>14]);
        }
        
        ServiceRequestItem::find($order->service_request_item_id)->update(['status_id' => 14]);
        ServiceRequestItemInspection::where("request_item_id", $order->service_request_item_id)->update([
            "status_id" => 14,
        ]);
        */

        if ($order->stat_warehouse == 1 && $order->stat_sertifikat == 3) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestItemInspection::where("service_request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequest::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUut();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                $history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }

    public function pending($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuut.pending', compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmpending($id, Request $request)
    {
        $order = ServiceOrders::find($id);

        $order->update([
            "pending_status" => 1,
            "pending_created" => date('Y-m-d H:i:s'),
            "pending_notes" => $request->get("pending_notes"),
            //"pending_estimated" => date("Y-m-d", strtotime($request->get("pending_estimated"))),
        ]);

        ServiceRequest::find($order->service_request_id)->update(['status_id' => 15]);

        $customerEmail = $order->ServiceRequest->requestor->email;
        //Mail::to($customerEmail)->send(new Pending($order));
        ProcessEmailJob::dispatch($customerEmail, new Pending($order))->onQueue('emails');

        return Redirect::route('serviceuut');
    }

    public function continue($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuut.continue', compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcontinue($id, Request $request)
    {
        $order = ServiceOrders::find($id);

        $order->update([
            "pending_status" => 0,
            "pending_ended" => date('Y-m-d H:i:s'),
        ]);

        ServiceRequest::find($order->service_request_id)->update(['status_id' => 12]);

        return Redirect::route('serviceuut');
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuut.cancel', compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcancel($id, Request $request)
    {
        $order = ServiceOrders::find($id);
        $cancelation_file_req = $request->file("cancelation_file");

        // $rules['cancelation_file'] = ['required'];
        $rules['cancel_notes'] = ['required'];

        $validation= Validator::make($request->all(),$rules);
        $response["messages"] = "Data kurang lengkap";

        if($validation->passes()){
            if($cancelation_file_req !=null && is_file($cancelation_file_req)){
                $data["cancelation_file"] = $cancelation_file_req->getClientOriginalName();
                $cancelation_file = $cancelation_file_req->store(
                    'cancelation_files',
                    'public'
                );

                $data["cancelation_file"] = $cancelation_file;
            }
            
            $data["cancel_at"] = date('Y-m-d H:i:s');
            $data["cancel_notes"] = $request->get("cancel_notes");
            $data["stat_service_order"] = 4;
            // $data["stat_sertifikat"] =0;
            $data["tool_capacity_unit"] = $request->get("capacity_unit");
            $data["tool_capacity"] = $request->get("kapasitas");
            $data["tool_made_in"] = $request->get("buatan");
            // Service Request
            $dtCertificate["label_sertifikat"] = $request->get("label_sertifikat");
            $dtCertificate["addr_sertifikat"] = $request->get("addr_sertifikat");

            $data["cancel_id"] = Auth::id();
            $data["stat_sertifikat"] =1;
            $data["status_id"] =16;
            $data["subkoordinator_notes"] =null;
            $data["batal_type"] =1;
            $order->update($data);

            $itemUpdate = ServiceRequestItem::find($order->service_request_item_id);
            $itemSave = $itemUpdate ->update([
                "status_id" => 16
            ]);
            $reqService = ServiceRequest::find($order->service_request_id);
            if($itemSave){
                $reqService->update($dtCertificate);
            }

            $inspection = ServiceRequestItemInspection::where("service_request_item_id",$order->ServiceRequestItem->id);
            $inspection->update(['status_id' => 16]);

            $history = new HistoryUut();
            
            $history->request_status_id = 16;
            $history->request_id = $order->service_request_id;
            $history->request_item_id = $order-> service_request_item_id;
            $history->order_id = $order->id;
            $history->order_status_id = 1;
            $history->user_id = Auth::id();
            $history->save();

            return Redirect::route("serviceuut");
        }else{
            return Redirect::route("serviceuut.cancel",$order->id);
        }

        return Redirect::route('serviceuut');
    }

    public function doWatermark($file,$qrcode_token,$no_sertifikat,$id)
    {//$file, $text_image){
        $fileloc='';
        $order = ServiceOrders::find($id);
        if($order->no_sertifikat !=null){
            $no_sertifikat = "Lampiran-No.".$order->no_sertifikat;
        }
        if($order->no_sertifikat ==null){
            $no_sertifikat = "UNDEF";
        }
        // $no_sertifikat = "Lampiran Sertifikat No.".$no_sertifikat;

        $pdf = new \setasign\Fpdi\Tcpdf\Fpdi();
        
        $qrcode_generator = route('documentuut.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);

        // make folder
        $directoryPath = storage_path('app/public/tmp/'.$id.'/');
        if (!file_exists($directoryPath)) {
            mkdir($directoryPath, 0777, true);
        }

        $qrCodePath = $directoryPath . $id . '-qrcode.png';
        $qrcodeContent = \SimpleSoftwareIO\QrCode\Facades\QrCode::format("png")
            ->size(100)
            ->margin(0.5)
            ->errorCorrection("H")
            ->generate($qrcode_generator, $qrCodePath);

        // Encode the QR code as Base64
        $qrcode = base64_encode($qrcodeContent);
        
        $pathtoQR = $qrCodePath;
        // Set source PDF file     
        if($order->cancel_at !=null && $order->cancel_notes != null){
            $fileloc = storage_path('app/public/sample_cancel.pdf');
        }else{
            $fileloc = storage_path('app/public/sample.pdf');
        }     
        // $fileloc = storage_path('app/public/sample.pdf');
        
        if(file_exists($fileloc)){ 
            $pagecount = $pdf->setSourceFile($fileloc); 
        }else{ 
            die('File PDF Tidak ditemukan.!'); 
        } 
        
        // Add watermark image to PDF pages 
        for($i=1;$i<=$pagecount;$i++){ 
            
           /*  $tpl = $pdf->importPage($i); 
            $pdf->addPage();

            $size = $pdf->getTemplateSize($tpl); 
            $pdf->useTemplate($tpl, 1, 1, $size['width'], $size['height'], TRUE); 
            
            //Put the watermark 
            $xxx_final = ($size['width']-60); 
            $yyy_final = ($size['height']-25);  */

            $tplidx = $pdf->importPage($i);
            $specs = $pdf->getTemplateSize($tplidx);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->addPage($specs['height'] > $specs['width'] ? 'P' : 'L');
            $pdf->useTemplate($tplidx, null, null, $specs['width'], $specs['height'], true);

            $pdf->SetTextColor(0, 0, 0);
            $_x_text = ($specs['width']- 60) - ($pdf->GetStringWidth($order->no_sertifikat, '', '',10)/2.8);
            $_y_text = $specs['height']- $specs['height'] +5+5;

            //setting for name of kabalai

            $_x_Hkab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama :'Kabalai', '', '',10)/2.8);
            $_y_Hkab = $specs['height']-70;

            // line2
            $_x_HL2kab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama :'Kabalai', '', '',10)/2.8);
            $_y_HL2kab = $specs['height']-65;

            // line3
            $_x_HL3kab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama :'Kabalai', '', '',10)/2.8);
            $_y_HL3kab = $specs['height']-60;

            //Writing nip
            $_x_Nkab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama : 'Kabalai', '', '',10)/2.8);
            $_y_Nkab = $specs['height']-25;

            //setting for name of kabalai
            $_x_kab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama : 'Kabalai', '', '',10)/2.8);
            $_y_kab = $specs['height']-25;

            $_x = ($specs['width']-50) - ($pdf->GetStringWidth($order->no_sertifikat, '', '', 10)/2.8);
            $_y = $specs['height'] -15;
            
            if($i >=2){
                $pdf->SetFont('', 'italic', 10);
                $pdf->SetXY($_x_text, $_y_text);
                $pdf->setAlpha(1);
                $pdf->Write(0,$order->no_sertifikat);
            }

            // $pdf->Image($qrcode, $xxx_final, $yyy_final, 0, 0, 'png'); 
            if($i == $pagecount){
                if($order->cancel_at ==null|| $order->cancel_notes ==null){
                    //Setting header and name kabalai
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Hkab, $_y_Hkab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Ketua Tim Pelayanan Teknis");

                    //Line 2
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_HL2kab, $_y_HL2kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Balai Pengelolaan");

                    //Line 3-1
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_HL3kab, $_y_HL3kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Standar Ukuran Metrologi Legal");

                    //writing NIP
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Nkab, $_y_Nkab);
                    $pdf->setAlpha(1);
                    // $pdf->Write(0,"NIP : ".$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nip :'');

                    //Setting header and name kabalai
                    // $pdf->SetFont('', 'U', 10);
                    $pdf->SetFont('', 10);
                    $pdf->SetXY($_x_kab, $_y_kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nama :'SUBKOORDINATOR');

                    // $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $_x-17,$_y-38, 25, 27, 'png');
                    $pdf->Image($pathtoQR, $_x-17,$_y-38, 25, 27, 'png');
                    // $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $xxx_final,$yyy_final-45, 33, 33, 'png'); 
                    // Storage::delete('app/public/tmp/'.$id.'/qrcode.png');
                }else{
                    //Setting header and name kabalai
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Hkab, $_y_Hkab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Ketua Tim Pelayanan Teknis");

                    //Line 2
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_HL2kab, $_y_HL2kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Balai SNSU");

                    //Line 3-1
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_HL3kab, $_y_HL3kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Standar Ukuran Metrologi Legal");

                    //writing NIP
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Nkab, $_y_Nkab);
                    $pdf->setAlpha(1);
                    // $pdf->Write(0,"NIP : ".$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nip :'');

                    //Setting header and name kabalai
                    $pdf->SetFont('', 'U', 10);
                    $pdf->SetXY($_x_kab, $_y_kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nama :'SUBKOORDINATOR');

                    $pdf->Image($pathtoQR, $_x-17,$_y-36, 25, 27, 'png'); 
                    // $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $_x-17,$_y-36, 25, 27, 'png'); 
                    // $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $xxx_final,$yyy_final-45, 33, 33, 'png'); 
                    // Storage::delete('app/public/tmp/'.$id.'/qrcode.png');
                }
            }
        } 
        
        // Output PDF with watermark 
        $no = str_replace('/','_',$no_sertifikat);
        if($order->cancel_at != null && $order->cancel_notes !=null){
            $down = $pdf->Output(storage_path('app/public/tmp/SBL-'.$no.'.pdf'),'F');
        }else{
            $down = $pdf->Output(storage_path('app/public/tmp/SERTIFIKAT-'.$no.'.pdf'),'F');
        }
        
        if($down){
            // Storage::delete('app/public/tmp/SERTIFIKAT-'.$no.'.pdf');
        }
    }

    public function show($id,$tipe) {
        $dbFile = ServiceOrders::find($id);
        $realPath='';
        if($tipe == "lampiran"){
            $realPath=storage_path('app/public/'.$dbFile->path_lampiran);
        }
        else if($tipe=="cerapan"){
            $realPath=storage_path('app/public/'.$dbFile->path_skhp);
        }
        else{ 
            
            $realPath=storage_path('app/public/'.$dbFile->cancelation_file);
        }
        if(file_exists($realPath)){
            // $realPath=str_replace(".pdf",".pdfz",$realPath);
            $file =$realPath;
            $filename = $realPath;
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $dbFile->cancelation_file . '"');
            header('Content-Transfer-Encoding: binary');
            // header('Content-Length: ' . filesize($realPath));
            header('Accept-Ranges: bytes');
            @readfile($file);
            exit;
        }else{
            echo "$realPath:No File Exist";

        }
        // $dbFile = ServiceOrders::find($id);
        // return response($dbFile->cancelation_file)
        //     ->withHeaders([
        //         'Content-type: application/pdf',
        //         'Content-Disposition: attachment; filename=' . $dbFile->cancelation_file
        //     ]);

    }
}
