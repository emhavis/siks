<?php

namespace App\Http\Controllers;

use App\MasterUsers;
use App\MasterRoles;
use App\MasterLaboratory;
use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\MasterTemplate;
use App\StandardInspectionPrices;
use App\StandardDetailType;
use App\StandardInspectionPrice;
use App\StandardMeasurementUnit;

class StandardInspectionPricesController extends Controller
{
    private $MyProjects;
    private $StandardInspectionPrices;
    private $MasterTemplate;
    private $StandardDetailType;
    private $StandardMeasurementUnit;
    public function __construct()
    {
        $this->StandardInspectionPrices = new StandardInspectionPrice();
        $this->MyProjects = new MyProjects();
        $this->MasterTemplate = new MasterTemplate();
        $this->StandardDetailType = new StandardDetailType();
        $this->StandardMeasurementUnit = new StandardMeasurementUnit();

    }
    public function index(){
        $data = StandardInspectionPrice::get();
        // dd($data[0]);
        $attribute = $this->MyProjects->setup("stinsprice");
        return View('standardinspectionprices.index',compact('data','attribute'));
    }

    public function create($id = null){
        $attribute = $this->MyProjects->setup("insitems");

        $row = null;

        if($id)
        {
            $row = $this->MasterInstalasi->whereId($id)->first();
        }

        $templates = $this->MasterTemplate->pluck('title_tmp', 'laboratory_id');
        $measurementtype = $this->StandardMeasurementUnit->pluck('abbr','id');
        $standarddetails = $this->StandardDetailType->pluck('standard_detail_type_name','id');
        // dd($standarddetails);
        return View('standardinspectionprices.create',
            compact('row','id','attribute','templates','standarddetails','measurementtype')
        );
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["name"] = ['required'];
        $rules["template_id"] = ['required'];
        $rules["order_no"] = ['required', 'integer', 'min:0'];
        $rules["no"] = ['required', 'integer', 'min:0'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->StandardInspectionPrices->whereId($id)->update($request->all());
            }
            else
            {
                $this->StandardInspectionPrices->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->StandardInspectionPrices->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
    public function edit($id)
    {
        $row  = $this->StandardInspectionPrices->find($id);
        dd($row);
        return view('uttp_inspection_items.edit', compact('row'));
    }    
    public function update(Request $request)
    {
        $response["status"] = false;

        $rules['brand'] = ['required'];
        $rules['made_in'] = ['required'];
        $rules['model'] = ['required'];
        $rules['tipe'] = ['required'];
        $rules['no_seri'] = ['required'];
        $rules['no_identitas'] = ['required'];
        $rules['capacity'] = ['required'];
        $rules['daya_baca'] = ['required'];
        $rules['class'] = ['required'];
        $rules['jumlah_per_set'] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $id = $request->id;

            $request->request->remove('id');

            $this->Standard->whereId($id)->update($request);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return response($response);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->StandardInspectionPrices::whereId($id)
        ->first();

        if($row)
        {
            $this->UttpInspectionItem::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('insitems')->with($response);
    }

}