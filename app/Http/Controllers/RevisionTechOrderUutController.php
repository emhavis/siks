<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\MyClass\MyProjects;
use App\RevisionUut;
use App\RevisionUutTTUInspection;
use App\RevisionUutTTUInspectionItems;
use App\RevisionUutTTUInspectionBadanHitung;

class RevisionTechOrderUutController extends Controller
{
    // protected $StandardInspectionPrice;
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("revisiontechorderuut");

        $laboratory_id = Auth::user()->laboratory_id;
        $instalasi_id = Auth::user()->instalasi_id;

        $rows_processing = RevisionUut::whereIn('status', [3])
            ->join('service_orders', 'service_orders.id', '=', 'certificate_revision_uut.order_id')
            ->where('service_orders.laboratory_id', $laboratory_id)
            ->select('certificate_revision_uut.*')
            ->orderBy('updated_at','desc')->get();

        return view('revisiontechorderuut.index', compact('rows_processing', 'attribute'));
    }

    public function proses($id)
    {
        $attribute = $this->MyProjects->setup("revisiontechorderuut");

        $revision = RevisionUut::find($id);
        $ttu = RevisionUutTTUInspection::where('cert_rev_id', $id)->first();
        $ttuItems = RevisionUutTTUInspectionItems::where('cert_rev_id', $id)->get();
        $badanHitungs = RevisionUutTTUInspectionBadanHitung::where('cert_rev_id', $id)->get();

        return view('revisiontechorderuut.edit', compact(['attribute', 'revision',
            'ttu',
            'ttuItems',
            'badanHitungs',
        ]));
    }

    public function simpanproses($id)
    {
        $attribute = $this->MyProjects->setup("revisiontechorderuut");
        
        $revision = RevisionUut::find($id);
        
        $file = $request->file('file_skhp');
        if ($file != null) {
            $data['file_lamp_skhp'] = $file->getClientOriginalName();

            $path = $file->store(
                'skhp',
                'public'
            );
            $data['path_lamp_skhp'] = $path;
        }
        if ($revision->order->ServiceRequest->service_type_id == 4 || $revision->order->ServiceRequest->service_type_id == 5) {
            
            if ($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge') {
                RevisionUutTTUInspectionBadanHitung::where("cert_rev_id", $id)->delete();
                RevisionUutTTUInspection::where("cert_rev_id", $id)->delete();
                RevisionUutTTUInspectionItems::where("cert_rev_id", $id)->delete();

                foreach($request->get('badanhitung_item_id') as $index=>$value) {
                    RevisionUutTTUInspectionBadanHitung::insert([
                        "cert_rev_id" => $id,
                        "type" => $request->input("type.".$index),
                        "serial_no" => $request->input("serial_no.".$index),
                    ]);
                }

                RevisionUutTTUInspection::insert([
                    "cert_rev_id" => $id,
                    "tank_no" => $request->input("tank_no"),
                    "tag_no" => $request->input("tag_no"),
                ]);

                foreach($request->get('item_id') as $index=>$value) {
                    RevisionUutTTUInspectionItems::insert([
                        "cert_rev_id" => $id,
                        "input_level" => $request->input("input_level.".$index),
                        "error_up" => $request->input("error_up.".$index),
                        "error_down" => $request->input("error_down.".$index),
                        "error_hysteresis" => $request->input("error_hysteresis.".$index),
                    ]);
                }
            }
            if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM') {
                RevisionUutTTUInspectionBadanHitung::where("cert_rev_id", $id)->delete();
                RevisionUutTTUInspection::where("cert_rev_id", $id)->delete();
                RevisionUutTTUInspectionItems::where("cert_rev_id", $id)->delete();

                foreach($request->get('badanhitung_item_id') as $index=>$value) {
                    RevisionUutTTUInspectionBadanHitung::insert([
                        "cert_rev_id" => $id,
                        "brand" => $request->input("brand.".$index),
                        "type" => $request->input("type.".$index),
                        "serial_no" => $request->input("serial_no.".$index),
                    ]);
                }

                RevisionUutTTUInspection::insert([
                    "cert_rev_id" => $id,
                    "totalisator" => $request->input("totalisator"),
                    "kfactor" => $request->input("kfactor"),
                ]);

                foreach($request->get('item_id') as $index=>$value) {
                    RevisionUutTTUInspectionItems::insert([
                        "cert_rev_id" => $id,
                        "flow_rate" => $request->input("flow_rate.".$index),
                        "error" => $request->input("error.".$index),
                        "meter_factor" => $request->input("meter_factor.".$index),
                        "repeatability" => $request->input("repeatability.".$index),
                    ]);
                }
            }
            if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas') {
                RevisionUutTTUInspection::where("cert_rev_id", $id)->delete();
                RevisionUutTTUInspectionItems::where("cert_rev_id", $id)->delete();

                RevisionUutTTUInspection::insert([
                    "cert_rev_id" => $id,
                    "totalisator" => $request->input("totalisator"),
                    "kfactor" => $request->input("kfactor"),
                ]);

                foreach($request->get('item_id') as $index=>$value) {
                    RevisionUutTTUInspectionItems::insert([
                        "cert_rev_id" => $id,
                        "flow_rate" => $request->input("flow_rate.".$index),
                        "error" => $request->input("error.".$index),
                        "repeatability" => $request->input("repeatability.".$index),
                    ]);
                }
            }
            if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air') {
                RevisionUutTTUInspection::where("cert_rev_id", $id)->delete();
                RevisionUutTTUInspectionItems::where("cert_rev_id", $id)->delete();

                RevisionUutTTUInspection::insert([
                    "cert_rev_id" => $id,
                    "totalisator" => $request->input("totalisator"),
                ]);

                foreach($request->get('item_id') as $index=>$value) {
                    RevisionUutTTUInspectionItems::insert([
                        "cert_rev_id" => $id,
                        "flow_rate" => $request->input("flow_rate.".$index),
                        "error" => $request->input("error.".$index),
                        "repeatability" => $request->input("repeatability.".$index),
                    ]);
                }
            }
            if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)') {
                RevisionUutTTUInspection::where("cert_rev_id", $id)->delete();
                RevisionUutTTUInspectionItems::where("cert_rev_id", $id)->delete();

                RevisionUutTTUInspection::insert([
                    "cert_rev_id" => $id,
                    "totalisator" => $request->input("totalisator"),
                    "kfactor" => $request->input("kfactor"),
                ]);

                foreach($request->get('item_id') as $index=>$value) {
                    RevisionUutTTUInspectionItems::insert([
                        "cert_rev_id" => $id,
                        "flow_rate" => $request->input("flow_rate.".$index),
                        "error" => $request->input("error.".$index),
                        "error_bkd" => $request->input("error_bkd.".$index),
                        "repeatability" => $request->input("repeatability.".$index),
                        "repeatability_bkd" => $request->input("repeatability_bkd.".$index),
                    ]);
                }
            }
        }

        $revision->update([
            "status" => 3,
            "lab_staff_id" => Auth::id(),
            "lab_staff_at" => date("Y-m-d H:i:s"),
            "file_lamp_skhp" => data["file_lamp_skhp"] != null ? data["file_lamp_skhp"] : null,
            "path_lamp_skhp" => data["path_lamp_skhp"] != null ? data["path_lamp_skhp"] : null,
            //"seri_revisi" => $next,
            "status_approval" => 0,
            "status_revision" => 3,
        ]);

        return Redirect::route('revisiontechorderuut');
    }

}
