<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use App\ServiceRequest;
use App\Customer;
use App\ServiceRequestUutInsituDoc;
use App\ServiceRequestUutInsituStaff;
use App\UttpInsituCheckItem;
use App\ServiceRequestUutInsituDocCheck;
use App\HistoryUut;
use App\ServiceRequestItem;
use App\ServiceRequestItemInspection;
use App\MasterStandardType;
use App\Uut;
use App\MasterServiceType;
use App\ServiceOrders;
use App\MasterUttpUnit;
use App\ServiceOrderUttpInspections;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ServiceProcessLuarUutController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();

        $this->ServiceRequest = new ServiceRequest();
        $this->ServiceRequestItem = new ServiceRequestItem();
        $this->ServiceRequestItemInspection = new ServiceRequestItemInspection();
        
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruut");

        $petugas_id = Auth::user()->petugas_uttp_id;
        if ($petugas_id == null) {
            $petugas_id = Auth::user()->petugas_uut_id;
        }
        
        $rows = ServiceRequest::where('status_id', 12)
            ->where(function($query) {
                $query->whereIn('status_revisi_spt', [0,3])
                    ->orWhereNull('status_revisi_spt');
            })
            ->leftJoin('service_request_uut_insitu_staff', 'service_request_uut_insitu_staff.doc_id', '=', 'service_requests.spuh_doc_id')
            ->where('service_request_uut_insitu_staff.scheduled_id', $petugas_id)
            ->select('service_requests.*')
            ->orderBy('received_date','desc')->get();

        return view('serviceprocessluaruut.index',compact('rows','attribute'));
    }

    public function check($id) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruut");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUutInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        return view('serviceprocessluaruut.check', compact(['request', 'requestor', 'doc', 'staffs',  'attribute']));
    }

    public function downloadBukti($docId)
    {
        $doc = ServiceRequestUttpInsituDoc::find($docId);
        
        return Storage::disk('public')->download($doc->path_bukti_pemeriksaan_awal, $doc->file_bukti_pemeriksaan_awal);
    }

    public function savecheck($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruut");

        $serviceRequest = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);

        $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUutInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        $rules = [];
        if ($request->get('submit_val') == 1 && $doc->path_bukti_pemeriksaan_awal == null) {
           // $rules['file_bukti_pemeriksaan_awal'] = ['required','mimes:pdf,jpg,jpeg,png'];
        } else {
           // $rules['file_bukti_pemeriksaan_awal'] = ['mimes:pdf,jpg,jpeg,png'];
        }

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            
            if ($request->get('submit_val') == 1)
            {
                $status = 13;
                $serviceRequest->update([
                    "status_id" => $status,
                    "payment_status_id"=>5,
                ]);

                $doc->update([
                    "checked_at" => date("Y-m-d H:i:s.u"),
                    "checked_by" => Auth::id(),
                ]);

                //$items = ServiceRequestUttpItem::where("request_id", $id)->get();
                // CREATE ORDERS
                $items = ServiceRequestItem::where("service_request_id", $id)->orderBy('id', 'asc')->get();

                $serviceType = MasterServiceType::find($serviceRequest->service_type_id);
                //$no_order = $serviceType->last_order_no;
                $no_order = $serviceRequest->no_order;

                $prefix = $serviceType->prefix;

                $alat = count($items);
                $noorder_num = intval($no_order)+1;
                //$noorder = $prefix.'-'.date("y-").sprintf("%04d",$noorder_num)."-";
                $noorder = $no_order . '-';
                //$nokuitansi = $prefix.'-'.date("y-").sprintf("%04d",$noorder_num);
                $nokuitansi = $no_order;

                ServiceRequest::whereId($id)
                ->update([
                    //"spuh_spt"=>$spuh_spt,
                    "status_id"=>$status,
                    //"no_order"=>$nokuitansi,
                ]);

                $no = 0;
                foreach($items as $item) {
                    $no = $no + 1;
                    $noorder_alat = $noorder.sprintf("%03d",$no);

                    $has_set = false;
                    $is_skhpt = false;
            
                    $only_pemeriksaan = false;
                    
                    if (count($item->inspections) == 1 && $item->inspections[0]->inspectionPrice->is_pemeriksaan == true) {
                        $is_skhpt = true;
                        $only_pemeriksaan = true;
                    }

                    ServiceOrders::insert([
                        "location"              => $item->location,

                        "laboratory_id"         => $item->uuts->stdType->lab->id,
                        //"laboratory_id"         => $item->uuts->stdType->instalasi->lab_id,
                        //"instalasi_id"          => $item->uuts->stdType->instalasi_id,
                        "service_request_id"    => $item->service_request_id,
                        "service_request_item_id"=> $item->id,
                        //"service_request_item_inspection_id"=>$inspection->id,
                        //"lab_staff"=>Auth::id(),
                        //"staff_entry_datein"=>date("Y-m-d")
                        'tool_type_id'			=> $item->uuts->type_id ? $item->uuts->type_id :null,
                        'tool_serial_no'		=> $item->uuts->serial_no ? $item->uuts->serial_no : null,
                        'tool_brand'			=> $item->uuts->tool_brand ? $item->uuts->tool_brand:null,
                        'tool_model'			=> $item->uuts->tool_model ? $item->uuts->tool_model : null,
                        'tool_type'				=> $item->uuts->tool_type ? $item->uuts->tool_type:null,
                        'tool_capacity'			=> $item->uuts->tool_capacity ? $item->uuts->tool_capacity : '',
                        'tool_capacity_unit'	=> $item->uuts->tool_capacity_unit ? $item->uuts->tool_capacity_unit :null ,
                        'tool_capacity_min'	    => $item->uuts->tool_capacity_min ? $item->uuts->tool_capacity_min :null ,
                        'tool_capacity_min_unit'=> $item->uuts->tool_capacity_min_unit ? $item->uuts->tool_capacity_min_unit :null ,
                        'tool_factory'			=> $item->uuts->tool_factory ? $item->uuts->tool_factory :null,
                        'tool_factory_address'	=> $item->uuts->tool_factory_address ? $item->uuts->tool_factory_address:null,
                        'tool_made_in'			=> $item->uuts->tool_made_in ? $item->uuts->tool_made_in :null,
                        'tool_made_in_id'		=> $item->uuts->tool_made_in_id ? $item->uuts->tool_made_in_id : null ,
                        'tool_owner_id'			=> $item->uuts->owner_id ? $item->uuts->owner_id : null,
                        'tool_media'			=> $item->uuts->tool_media ? $item->uuts->tool_media : null,
                        'tool_name'				=> $item->uuts->tool_name ? $item->uuts->tool_name : null,
                        'tool_class'		    => $item->uuts->class ? $item->uuts->class : null,
                        'tool_jumlah'		    => $item->uuts->jumlah ? $item->uuts->jumlah : null,
                        'tool_dayabaca'		    => $item->uuts->tool_dayabaca ? $item->uuts->tool_dayabaca :null,
                        'tool_dayabaca_unit'    => $item->uuts->tool_dayabaca_unit ? $item->uuts->tool_dayabaca_unit : null,

                        'uut_id'               => $item->uut_id,

                        "stat_service_order"    => 1,
                        'staff_entry_datein'    => $doc->date_from,
                        "staff_entry_dateout"   => $doc->date_to,

                        "stat_warehouse"        => -1,
                        "has_set"               => $has_set,
                        "is_skhpt"              => $is_skhpt
                    ]);

                    ServiceRequestItem::where("id", $item->id)
                    ->update([
                        "status_id"=>$status,
                        "no_order"=>$noorder_alat,
                        "order_at"=> date("Y-m-d H:i:s"),
                    ]);


                    $history = new HistoryUut();
                    $history->request_status_id = $status; 
                    $history->request_id = $id;
                    $history->request_item_id = $item->id;
                    //$history->order_id = $order->id;
                    //$history->order_status_id = 1;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = $status;
                    $item->save();
                }
            }
            return Redirect::route('serviceprocessluaruut');
        } else {
            return Redirect::route('serviceprocessluaruut.check', $id);
        }
    }

    public function createbookinginspection($id)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruut");

        $item = ServiceRequestItem::find($id);
        
        $prices = DB::table('uut_inspection_prices')
            ->join('uut_inspection_price_types', 'uut_inspection_price_types.inspection_price_id', '=', 'uut_inspection_prices.id')
            ->whereNull('user_type')
            ->where('uut_inspection_price_types.uut_type_id', $item->uuts->type_id)
            ->select('uut_inspection_prices.*')->get();

        return view('serviceprocessluaruut.create_booking_inspection', compact('item', 'prices', 'attribute'));
    }

    public function simpanbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruut");

        ServiceRequestItemInspection::where('request_item_id', $id)->delete();
        
        foreach($request->get('quantity') as $index=>$value) {
            if ($value != null) {

                $dataInspection = new ServiceRequestItemInspection();
                $dataInspection->service_request_item_id = $id;
                $dataInspection->quantity = $value;
                $dataInspection->price = $request->get('price')[$index];
                $dataInspection->inspection_price_id = $request->get('id')[$index];

                $dataInspection->status_id = 1;
                $dataInspection->save();
            }

        }

        $item = ServiceRequestItem::find($id);
        
        $itemAggregate = ServiceRequestItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('service_request_item_id', $id)
            ->first();

        ServiceRequestItem::where('id', $id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = ServiceRequestItem::selectRaw('count(service_request_item_inspections.id) quantity, coalesce(sum(service_request_item_inspections.price * service_request_item_inspections.quantity), 0) subtotal')
            ->join('service_request_item_inspections', 'service_request_item_inspections.service_request_item_id', '=', 'service_request_items.id')
            ->where('service_request_id', $item->serviceRequest->id)
            ->first();

        $svcRequest = ServiceRequest::selectRaw("service_requests.id, service_requests.received_date, service_requests.received_date + max(master_laboratory.sla_day) * interval '1 day' max_estimated_date")
            ->join('service_request_items', 'service_request_items.service_request_id', '=', 'service_requests.id')
            ->join('service_request_item_inspections', 'service_request_item_inspections.service_request_item_id', '=', 'service_request_items.id')
            ->join('uut_inspection_prices', 'service_request_item_inspections.inspection_price_id', '=', 'uut_inspection_prices.id')
            ->join('standard_uut', 'standard_uut.id', '=', 'service_request_items.uut_id')
            ->join('master_standard_types', 'master_standard_types.id', '=', 'standard_uut.type_id')
            ->join('master_laboratory', 'master_standard_types.lab_id', '=', 'master_laboratory.id')
            ->groupBy('service_requests.id', 'service_requests.received_date')
            ->where('service_requests.id', $item->serviceRequest->id)
            ->first();

        $itemCount = ServiceRequestItem::where("service_request_id", $item->serviceRequest->id)->count();
        $request = ServiceRequest::find($item->serviceRequest->id);

        // $no_order = $request->no_order;
        // $parts = explode("-",$no_order);
        // $parts[3] = sprintf("%03d",intval($itemCount));
        // $no_order = implode("-", $parts);

        ServiceRequest::where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal,
            'estimated_date' => $svcRequest ? $svcRequest->max_estimated_date : null,
            //'no_order' => $no_order,
        ]);

        return Redirect::route('serviceprocessluaruut.check', $item->serviceRequest->id);
    }

    public function hapusbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $inspection = ServiceRequestUttpItemInspection::where('id', $id)->first();
        $inspection->delete();
        $item_id = $inspection->request_item_id;

        $item = $this->ServiceRequestUttpItem
            ->with(['serviceRequest'])
            ->find($item_id);
        
        $itemAggregate = ServiceRequestUttpItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $item->id)
            ->first();

        $this->ServiceRequestUttpItem->where('id', $item->id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = $this->ServiceRequestUttpItem
            ->selectRaw('count(service_request_uttp_item_inspections.id) quantity, coalesce(sum(service_request_uttp_item_inspections.price * service_request_uttp_item_inspections.quantity), 0) subtotal')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        $this->ServiceRequestUttp->where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal
        ]);

        return Redirect::route('serviceprocessluaruttp.check', $item->serviceRequest->id);
    }

    function newitem($id)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruut");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);

        $standard_types = MasterStandardType::orderBy('id')->pluck('uut_type', 'id');

        return view('serviceprocessluaruut.add_booking_item', 
            compact('request',
            'standard_types', 
            'attribute'));
    }

    function savenewitem($id, Request $request)
    {
        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $uttp = Uttp::find($request->get('uttp_id'));

        $dataItem["request_id"] = $req->id;
        //$dataItem["quantity"] = $item->quantity;
        //$dataItem["subtotal"] = $item->est_subtotal;
        $dataItem["uttp_id"] = $uttp->id;

        //$dataItem["location"] = $request->location;
        $dataItem["location_prov_id"] = $req->inspection_prov_id;
        $dataItem["location_kabkot_id"] = $req->inspection_kabkot_id;

        /*
        $dataItem["file_application_letter"] = $item->file_application_letter;
        $dataItem["file_last_certificate"] = $item->file_last_certificate;
        $dataItem["file_manual_book"] = $item->file_manual_book;
        $dataItem["file_type_approval_certificate"] = $item->file_type_approval_certificate;
        $dataItem["file_calibration_manual"] = $item->file_calibration_manual;

        $dataItem["path_application_letter"] = $item->path_application_letter;
        $dataItem["path_last_certificate"] = $item->path_last_certificate;
        $dataItem["path_manual_book"] = $item->path_manual_book;
        $dataItem["path_type_approval_certificate"] = $item->path_type_approval_certificate;
        $dataItem["path_calibration_manual"] = $item->path_calibration_manual;

        $dataItem["reference_no"] = $item->reference_no;
        $dataItem["reference_date"] = $item->reference_date;
        */

        $dataItem["status_id"] = $req->status_id;

        $serviceItemID = ServiceRequestUttpItem::insertGetId($dataItem);

        return Redirect::route('serviceprocessluaruttp.check', $id);
    }

    function createitem($id, $standard_type_id)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruut");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);

        $type = MasterStandardType::find($standard_type_id);

        $units = DB::table('master_standard_units')
                ->where('uut_type_id', $type->unit_id)
                ->pluck('unit', 'unit');

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        $items = $request->items->map(function ($item, $key) {
            return [
                'label' => $item->uuts->stdtype->uut_type . ': ' .
                    $item->uuts->serial_no . ' (' . 
                    $item->uuts->tool_brand . ' / ' . 
                    $item->uuts->tool_model . ')',
                'id' => $item->id
            ];
        })->pluck('label', 'id');
        //dd([$items, $request->items->pluck('uttp.type.uttp_type', 'id')]);

        return view('serviceprocessluaruut.create_booking_item', compact('request', 'items', 'type', 'units', 'negara', 'attribute'));
    }

    public function simpanitem($id, $standard_type_id, Request $request) 
    {
        $req = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
        ->find($id);
        
        $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();

        $uut = Uut::create([
            'owner_id' => $req->uut_owner_id,
            'type_id' => $standard_type_id,
            'serial_no' => $request->get('tool_serial_no'),
            'tool_brand' => $request->get('tool_brand'),
            'tool_model' => $request->get('tool_model'),
            'tool_capacity' => $request->get('tool_capacity'),
            'tool_factory' => $request->get('tool_factory'),
            'tool_factory_address' => $request->get('tool_factory_address'),
            'tool_made_in' => $negara->nama_negara,
            'tool_made_in_id' => $request->get('tool_made_in_id'),     
            'tool_capacity_unit' => $request->get('tool_capacity_unit'),
            'tool_media' => $request->get('tool_media'),
            'tool_capacity_min' => $request->get('tool_capacity_min'),
        ]);

        $dataItem["service_request_id"] = $req->id;
        $dataItem["uut_id"] = $uut->id;

        //$dataItem["location"] = $request->location;
        $dataItem["location_prov_id"] = $req->inspection_prov_id;
        $dataItem["location_kabkot_id"] = $req->inspection_kabkot_id;

        $dataItem["status_id"] = $req->status_id;

        $serviceItemID = ServiceRequestItem::insertGetId($dataItem);

        return Redirect::route('serviceprocessluaruut.check', $id);
    }

    public function edititem($id, $idItem) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruut");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);

        $item = ServiceRequestItem::find($idItem);

        $units = DB::table('master_standard_units')
                    ->where('uut_type_id', $item->uuts->stdType->unit_id)
                    ->pluck('unit', 'unit')
                    ->prepend($item->uuts->tool_capacity_unit, $item->uuts->tool_capacity_unit);

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        return view('serviceprocessluaruut.edit_booking_item', 
            compact('request', 'item', 'units', 'negara', 'attribute'));
    }

    public function simpanedititem($id, $idItem, Request $request) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruut");

        $req = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);

        $item = ServiceRequestItem::find($idItem);

        $uut = Uut::find($item->uut_id);

        $data['tool_capacity'] = $request->get("tool_capacity");
        $data['tool_capacity_min'] = $request->get("tool_capacity_min");
        $data['tool_capacity_unit'] = $request->get("tool_capacity_unit");

        $data['tool_brand'] = $request->get("tool_brand");
        $data['tool_model'] = $request->get("tool_model");
        $data['serial_no'] = $request->get("tool_serial_no");

        $data['tool_media'] = $request->get("tool_media");

        $data['tool_made_in_id'] = $request->get('tool_made_in_id');
        $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();
        $data['tool_made_in'] = $negara->nama_negara;

        $data['tool_factory'] = $request->get("tool_factory");
        $data['tool_factory_address'] = $request->get("tool_factory_address");

        $uut->update($data);

        return Redirect::route('serviceprocessluaruut.check', $id);
    }

    public function hapusitem(Request $request) 
    {
        $item = ServiceRequestItem::find($request->get('id'));
        $request_id = $item->service_request_id;

        Uut::find($item->uut_id)->delete();
        ServiceRequestItemInspection::where('service_request_item_id', $item->id)->delete();
        // ServiceRequestItemTTUPerlengkapan::where('request_item_id', $item->id)->delete();

        $item->delete();

        return Redirect::route('serviceprocessluaruut.check', $request_id); 
    }

    public function getuuts(Request $request) {
        $owner_id = $request->get('owner_id');
        $standard_type_id = $request->get('standard_type_id');
        $uuts = Uut::where('owner_id', $owner_id)->where('type_id', $standard_type_id);

        if ($request->has('serial_no')) {
              $uuts = $uuts->where('serial_no', 'like', '%'.$request->get('serial_no').'%');
        }

        return response()->json($uuts->get());
    }

    public function perlengkapan($id) {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $item = ServiceRequestUttpItem::find($id);

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($item->serviceRequest->id);

        $perlengkapans = ServiceRequestUttpItemTTUPerlengkapan::where('request_item_id', $id)->get();

        return view('serviceprocessluaruttp.perlengkapan', 
            compact('request', 'item', 'perlengkapans', 'attribute'));
    }

    function newperlengkapan($id)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $item = ServiceRequestUttpItem::find($id);

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($item->serviceRequest->id);

        $uttp_types = MasterUttpType::orderBy('id')->get();
        $uttp_type_id = $uttp_types[0]->id;
        $uttp_types = $uttp_types->pluck('uttp_type', 'id');
        

        $units = DB::table('master_uttp_units')
                ->where('uttp_type_id', $uttp_type_id)
                ->pluck('unit', 'unit');

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        return view('serviceprocessluaruttp.create_perlengkapan', 
            compact('request', 'item', 'units',  'uttp_types', 'negara', 'attribute'));
    }

    public function simpanperlengkapan($id, Request $request) 
    {
        $item = ServiceRequestUttpItem::find($id);

        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
        ->find($item->serviceRequest->id);
        
        $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();

        $uttp = Uttp::create([
            'owner_id' => $req->uttp_owner_id,
            'type_id' => $request->get('uttp_type_id'),
            'serial_no' => $request->get('tool_serial_no'),
            'tool_brand' => $request->get('tool_brand'),
            'tool_model' => $request->get('tool_model'),
            'tool_capacity' => $request->get('tool_capacity'),
            'tool_factory' => $request->get('tool_factory'),
            'tool_factory_address' => $request->get('tool_factory_address'),
            'tool_made_in' => $negara->nama_negara,
            'tool_made_in_id' => $request->get('tool_made_in_id'),     
            'tool_capacity_unit' => $request->get('tool_capacity_unit'),
            'tool_media' => $request->get('tool_media'),
            'tool_capacity_min' => $request->get('tool_capacity_min'),
        ]);

        //$item = ServiceRequestUttpItem::find($request->get('item_id'));

        $dataPerlengkapan["request_item_id"] =  $item->id;
        $dataPerlengkapan["uttp_id"] = $uttp->id;

        $perelengkapan = ServiceRequestUttpItemTTUPerlengkapan::insert($dataPerlengkapan);

        return Redirect::route('serviceprocessluaruttp.perlengkapan', $id);
    }

    public function getunits(Request $request) {
        $uttp_type_id = $request->get('uttp_type_id');
        $units = MasterUttpUnit::where('uttp_type_id', $uttp_type_id);

        if ($request->has('unit')) {
              $units = $uttps->where('unit', 'like', '%'.$request->get('unit').'%');
        }

        return response()->json($units->get());
    }

    public function editperlengkapan($id, $idItem, $idPerlengkapan) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $perlengkapan = ServiceRequestUttpItemTTUPerlengkapan::find($idPerlengkapan);

        $item = ServiceRequestUttpItem::find($idItem);

        $units = DB::table('master_uttp_units')
                    ->where('uttp_type_id', $item->uttp_type_id)
                    ->pluck('unit', 'unit')
                    ->prepend($item->uttp->tool_capacity_unit, $item->uttp->tool_capacity_unit);

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        return view('serviceprocessluaruttp.edit_perlengkapan', 
            compact('request', 'item', 'perlengkapan', 'units', 'negara', 'attribute'));
    }

    public function simpaneditperlengkapan($id, $idItem, $idPerlengkapan, Request $request) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $item = ServiceRequestUttpItem::find($idItem);

        $perlengkapan = ServiceRequestUttpItemTTUPerlengkapan::find($idPerlengkapan);

        $uttp = Uttp::find($perlengkapan->uttp_id);

        $data['tool_capacity'] = $request->get("tool_capacity");
        $data['tool_capacity_min'] = $request->get("tool_capacity_min");
        $data['tool_capacity_unit'] = $request->get("tool_capacity_unit");

        $data['tool_brand'] = $request->get("tool_brand");
        $data['tool_model'] = $request->get("tool_model");
        $data['serial_no'] = $request->get("tool_serial_no");

        $data['tool_media'] = $request->get("tool_media");

        $data['tool_made_in_id'] = $request->get('tool_made_in_id');
        $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();
        $data['tool_made_in'] = $negara->nama_negara;

        $data['tool_factory'] = $request->get("tool_factory");
        $data['tool_factory_address'] = $request->get("tool_factory_address");

        $uttp->update($data);

        return Redirect::route('serviceprocessluaruttp.perlengkapan', $idItem);
    }

    public function hapusperlengkapan(Request $request) 
    {
        $perlengkapan = ServiceRequestUttpItemTTUPerlengkapan::find($request->get('id'));

        Uttp::find($perlengkapan->uttp_id)->delete();
        $request_item_id = $perlengkapan->request_item_id;
        $perlengkapan->delete();

        return Redirect::route('serviceprocessluaruttp.perlengkapan', $request_item_id); 
    }

    public function statusRevisiSPT(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Request tidak ditemukan';

        $req = null;
        if ($request->has('id')) {
            $req = ServiceRequest::find($request->get('id'));
        } 

        if ($req != null) {
            $req->status_revisi_spt = $request->get('status_revisi_spt');
            if ($request->get('status_revisi_spt') == 1) {
                $doc = ServiceRequestUutInsituDoc::find($req->spuh_doc_id);
                $doc->keterangan_revisi = $request->get('keterangan_revisi');
                $doc->update();
            }
            $req->update();

            $response["status"] = true;
            $response["messages"] = "Status revisi SPT telah diubah";
        }
        
        return response($response);
    }
}