<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use App\ServiceRequestUttp;
use App\Customer;
use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\UttpInsituCheckItem;
use App\ServiceRequestUttpInsituDocCheck;
use App\HistoryUttp;
use App\ServiceRequestUttpItem;
use App\MasterServiceType;
use App\ServiceOrderUttps;
use App\MasterUsers;
use App\ServiceRequestUttpStaff;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DocumentTUInsituUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("doctuinsituuttp");

        $rows = ServiceRequestUttp::whereIn('status_id', [11,12,13,14,19,20,22])
            ->whereNotNull('spuh_doc_id')
            ->whereNotNull('spuh_spt')
            ->select('service_request_uttps.*')
            ->orderBy('received_date','desc')->get();

        $rows_status = ServiceRequestUttp::whereIn('status_id', [11,12,13,14,19,20,22])
            ->whereNotNull('spuh_doc_id')
            ->whereNotNull('spuh_spt')
            ->where('status_revisi_spt', 1)
            ->select('service_request_uttps.*')
            ->orderBy('received_date','desc')->get();

        $rows_bukti = ServiceRequestUttp::whereIn('status_id', [11,12,13,14,19,20,22])
            ->whereNotNull('spuh_doc_id')
            ->whereNotNull('spuh_spt')
            ->where('status_revisi_spt', 2)
            ->select('service_request_uttps.*')
            ->orderBy('received_date','desc')->get();

        return view('doctuinsituuttp.index',compact('rows','rows_status','rows_bukti','attribute'));
    }

    public function list($id) 
    {
        $attribute = $this->MyProjects->setup("doctuinsituuttp");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();
        $docs = ServiceRequestUttpInsituDoc::with(['listOfStaffs'])
            ->where('request_id', $id)->orderBy('id', 'desc')->get();
            
        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        return view('doctuinsituuttp.list', compact(['request', 'requestor', 'doc', 'docs', 'staffs',  'attribute']));
    }

    public function create($id) 
    {
        $attribute = $this->MyProjects->setup("doctuinsituuttp");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->where('jenis','inisial')->orderBy('id', 'desc')->first();

        $doc_revisi_count = ServiceRequestUttpInsituDoc::where('request_id', $id)->where('jenis','revisi')->count();

        $docno_parts = explode("/PKTN.4.5/ST/", $doc->doc_no);
        $doc_no = $docno_parts[0] . '.R' . ($doc_revisi_count + 1) . "/PKTN.4.5/ST/" . $docno_parts[1];

        //dd([$doc->doc_no, $doc_revisi_count, $doc_no]);
        
        $kabalais = MasterUsers::where("user_role", 10) //kabalai
            ->whereHas('PetugasUttp', function($query) {
                $query->where('flag_unit', 'uttp')->where('is_active', 1);
            })
            ->pluck('full_name', 'id');

        $staffes = ServiceRequestUttpStaff::where("request_id", $id)->get();

        return view('doctuinsituuttp.create', compact(['request', 'staffes', 'requestor', 'doc',  'doc_no', 'kabalais', 'attribute']));
    }

    public function store($id, Request $request) 
    {
        $attribute = $this->MyProjects->setup("doctuinsituuttp");

        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = Customer::find($req->requestor_id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->where('jenis','inisial')->orderBy('id', 'desc')->first();

        $doc_inisial = ServiceRequestUttpInsituDoc::where('request_id', $req->id)->where('jenis', 'inisial')->first();
        
        $kabalais = MasterUsers::where("user_role", 10) //kabalai
            ->whereHas('PetugasUttp', function($query) {
                $query->where('flag_unit', 'uttp')->where('is_active', 1);
            })
            ->pluck('full_name', 'id');

        //$rules['file_spt'] = ['required','mimes:pdf,jpg,jpeg,png'];
        $rules['doc_no'] = ['required'];
        $rules['accepted_date'] = ['required'];
        $rules['accepted_by_id'] = ['required'];
        $rules['date_from'] = ['required'];
        $rules['date_to'] = ['required'];
        $rules['scheduled_id_1'] = ['required'];
        //$rules['scheduled_id_2'] = ['required'];
        $rules['keterangan_revisi'] = ['required'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $file_spt = $request->file('file_spt');
            
            $data['file_spt'] = null;
            $data['path_spt'] = null;
            if ($file_spt != null) {
                $data['file_spt'] = $file_spt->getClientOriginalName();

                $path = $file_spt->store(
                    'skhp',
                    'public'
                );

                $data['path_spt'] = $path;
            }
            
            $file_spuh = $request->file('file_spuh');
            
            $data['file_spuh'] = null;
            $data['path_spuh'] = null;
            if ($file_spuh != null) {
                $data['file_spuh'] = $file_spuh->getClientOriginalName();

                $path = $file_spuh->store(
                    'skhp',
                    'public'
                );

                $data['path_spuh'] = $path;
            }

            $file_bukti_bayar = $request->file('file_bukti_bayar');

            $data['file_bukti_bayar'] = null;
            $data['path_bukti_bayar'] = null;
            if ($file_bukti_bayar != null) {
                $data['file_bukti_bayar'] = $file_bukti_bayar->getClientOriginalName();

                $path = $file_bukti_bayar->store(
                    'skhp',
                    'public'
                );

                $data['path_bukti_bayar'] = $path;
            }

            $data["accepted_date"] = date("Y-m-d", strtotime($request->get("accepted_date")));

            $data["date_from"] = date("Y-m-d", strtotime($request->get("date_from")));
            $data["date_to"] = date("Y-m-d", strtotime($request->get("date_to")));

            $dt1 = new \DateTime($data['date_from']);
            $dt2 = new \DateTime($data['date_to']);
            $interval = $dt1->diff($dt2);

            $staffs[] = $request->get('scheduled_id_1');
            $staffs[] = $request->get('scheduled_id_2');

            $int_interval = ((int)$interval->format('%a') + 1);


            //dd($data);

            $doc = ServiceRequestUttpInsituDoc::create([
                "request_id" => $id,
                "doc_no"=>$request->get('doc_no'),
                "spuh_year"=>date("Y"),
                //"doc_no" => $request->get('doc_no'),
                //"rate" => $data["spuh_rate"],
                //"rate_id" => $data["spuh_rate_id"],
                //"price" => $data["spuh_price"],
                //"invoiced_price" => $data["spuh_price"],
                "staffs" => implode(";",$staffs),
                "date_from" => $data["date_from"],
                "date_to" => $data["date_to"],
                "days" => ((int)$interval->format('%a') + 1),
                "accepted_date" => $data['accepted_date'],
                "accepted_by_id" => $request->get('accepted_by_id'),

                "file_spt" => $data["file_spt"],
                "path_spt" => $data["path_spt"],
                "file_spuh" => $data["file_spuh"],
                "path_spuh" => $data["path_spuh"],
                "file_bukti_bayar" => $data["file_bukti_bayar"],
                "path_bukti_bayar" => $data["path_bukti_bayar"],

                "keterangan_revisi"=>$request->get('keterangan_revisi'),
                "jenis" => "revisi",

                "add_days" => $int_interval - $doc_inisial->days,

                "rate" => $doc_inisial->rate,
                "rate_id" => $doc_inisial->rate_id,

                "spuh_rate1" => $doc_inisial->spuh_rate1,
                "spuh_rate2" => $doc_inisial->spuh_rate2,
                "spuh_rate_negara_id" => $doc_inisial->spuh_rate_negara_id,
            ]);

            ServiceRequestUttpStaff::where("request_id", $id)->delete();
            
            ServiceRequestUttpStaff::insert([
                "request_id" => $req->id,
                "scheduled_id" => $request->get("scheduled_id_1"),
            ]); 
            ServiceRequestUttpStaff::insert([
                "request_id" => $req->id,
                "scheduled_id" => $request->get("scheduled_id_2"),
            ]);
        
            ServiceRequestUttpInsituStaff::insert([
                "doc_id" => $doc->id,
                "scheduled_id" => $request->get("scheduled_id_1"),
            ]);
            ServiceRequestUttpInsituStaff::insert([
                "doc_id" => $doc->id,
                "scheduled_id" => $request->get("scheduled_id_2"),
            ]);

            $status_revisi_spt = 2;
            if ($file_spuh != null && $file_bukti_bayar != null) {
                $status_revisi_spt = 3;
            }
            $req->update([
                //"spuh_doc_id" => $doc->id,
                "status_revisi_spt" => $status_revisi_spt
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";

            return redirect()->route('doctuinsituuttp.list', $id)->with('response', $response);

        }

        $response["status"] = false;

        //dd($response);

        return redirect()->route('doctuinsituuttp.create', $id)->with('response', $response);
    }

    public function download($id, String $tipe)
    {
        $doc = ServiceRequestUttpInsituDoc::find($id);

        $path = 'path_' . $tipe;
        $file = 'file_' . $tipe;

        return Storage::disk('public')->download($doc[$path], $doc[$file]);
    }

    public function edit($id) 
    {
        $attribute = $this->MyProjects->setup("doctuinsituuttp");

        $doc = ServiceRequestUttpInsituDoc::find($id);

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($doc->request_id);
        $requestor = Customer::find($request->requestor_id);

        $kabalais = MasterUsers::where("user_role", 10) //kabalai
            ->whereHas('PetugasUttp', function($query) {
                $query->where('flag_unit', 'uttp')->where('is_active', 1);
            })
            ->pluck('full_name', 'id');

        $staffes = ServiceRequestUttpStaff::where("request_id", $doc->request_id)->get();

        return view('doctuinsituuttp.edit', compact(['request', 'staffes', 'requestor', 'doc',  'kabalais', 'attribute']));
    }

    public function update($id, Request $request) 
    {
        $attribute = $this->MyProjects->setup("doctuinsituuttp");

        $doc = ServiceRequestUttpInsituDoc::find($id);


        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($doc->request_id);
        $requestor = Customer::find($req->requestor_id);

        $doc_inisial = ServiceRequestUttpInsituDoc::where('request_id', $req->id)->where('jenis', 'inisial')->first();

    
        $kabalais = MasterUsers::where("user_role", 10) //kabalai
            ->whereHas('PetugasUttp', function($query) {
                $query->where('flag_unit', 'uttp')->where('is_active', 1);
            })
            ->pluck('full_name', 'id');

        //$rules['file_spt'] = ['required','mimes:pdf,jpg,jpeg,png'];
        
        $rules['doc_no'] = ['required'];
        $rules['accepted_date'] = ['required'];
        $rules['accepted_by_id'] = ['required'];
        $rules['date_from'] = ['required'];
        $rules['date_to'] = ['required'];
        $rules['scheduled_id_1'] = ['required'];
        //$rules['scheduled_id_2'] = ['required'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $file_spt = $request->file('file_spt');
            
            //$data['file_spt'] = null;
            //$data['path_spt'] = null;
            if ($file_spt != null) {
                $data['file_spt'] = $file_spt->getClientOriginalName();

                $path = $file_spt->store(
                    'skhp',
                    'public'
                );

                $data['path_spt'] = $path;
            }
            
            $file_spuh = $request->file('file_spuh');
            
            //$data['file_spuh'] = null;
            //$data['path_spuh'] = null;
            if ($file_spuh != null) {
                $data['file_spuh'] = $file_spuh->getClientOriginalName();

                $path = $file_spuh->store(
                    'skhp',
                    'public'
                );

                $data['path_spuh'] = $path;
            }

            $file_bukti_bayar = $request->file('file_bukti_bayar');

            //$data['file_bukti_bayar'] = null;
            //$data['path_bukti_bayar'] = null;
            if ($file_bukti_bayar != null) {
                $data['file_bukti_bayar'] = $file_bukti_bayar->getClientOriginalName();

                $path = $file_bukti_bayar->store(
                    'skhp',
                    'public'
                );

                $data['path_bukti_bayar'] = $path;
            }

            $data["accepted_date"] = date("Y-m-d", strtotime($request->get("accepted_date")));

            $data["date_from"] = date("Y-m-d", strtotime($request->get("date_from")));
            $data["date_to"] = date("Y-m-d", strtotime($request->get("date_to")));

            $dt1 = new \DateTime($data['date_from']);
            $dt2 = new \DateTime($data['date_to']);
            $interval = $dt1->diff($dt2);

            $staffs[] = $request->get('scheduled_id_1');
            $staffs[] = $request->get('scheduled_id_2');

            $data["request_id"] = $req->id;
            $data["doc_no"] = $request->get('doc_no');
            $data["spuh_year"] = date("Y");
            $data["staffs"] = implode(";",$staffs);
            $data["days"] = ((int)$interval->format('%a') + 1);
            $data["accepted_by_id"] = $request->get('accepted_by_id');

            $data["keterangan_revisi"] = $request->get('keterangan_revisi');

            $int_interval = $data["days"];
            $data["add_days"] = $int_interval - $doc_inisial->days;

            $data["rate"] = $doc_inisial->spuh_rate;
            $data["rate_id"] = $doc_inisial->rate_id;

            $data["spuh_rate1"] = $doc_inisial->spuh_rate1;
            $data["spuh_rate2"] = $doc_inisial->spuh_rate2;
            $data["spuh_rate_negara_id"] = $doc_inisial->spuh_rate_negara_id;

            $data["is_accepted"] = $request->get('stat_spuh') == 1;

            $data["price"] = $data["add_days"] * ($data["spuh_rate1"] + $data["spuh_rate2"]);
            $data["invoiced_price"] = $data["price"];

            $doc->update($data);

            ServiceRequestUttpStaff::where("request_id", $req->id)->delete();
            ServiceRequestUttpStaff::insert([
                "request_id" => $req->id,
                "scheduled_id" => $request->get("scheduled_id_1"),
            ]); 
            ServiceRequestUttpStaff::insert([
                "request_id" => $req->id,
                "scheduled_id" => $request->get("scheduled_id_2"),
            ]);
        
            ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->delete();
            ServiceRequestUttpInsituStaff::insert([
                "doc_id" => $doc->id,
                "scheduled_id" => $request->get("scheduled_id_1"),
            ]);
            ServiceRequestUttpInsituStaff::insert([
                "doc_id" => $doc->id,
                "scheduled_id" => $request->get("scheduled_id_2"),
            ]);
            

            //update
            $doc = ServiceRequestUttpInsituDoc::find($id);
            if ($doc->file_spuh != null && $doc->file_bukti_bayar != null) {
                $req->update([
                    "status_revisi_spt" => 3
                ]);
            }

            // SUBMIT
            if ($request->get('stat_spuh') == 1) {
                
                ServiceRequestUttp::whereId($req->id)
                ->update([
                    /*
                    "spuh_spt"=>$spuh_spt,
                    "status_id"=>$status,
                    "no_order"=>$nokuitansi,
                    */
                    "spuh_payment_status_id"=>6,

                    "spuh_billing_date"=>date("Y-m-d"),
                    "spuh_payment_date" => null,

                    "spuh_price" => $data["days"] * ($data["spuh_rate1"] + $data["spuh_rate2"]),
                    "spuh_inv_price" => $data["add_days"] * ($data["spuh_rate1"] + $data["spuh_rate2"]),

                    "spuh_doc_id" => $id,
                ]);

                $items = ServiceRequestUttpItem::where("request_id", $req->id)->orderBy('id', 'asc')->get();


                $no = 0;
                foreach($items as $item) {

                    $history = new HistoryUttp();
                    $history->request_status_id = $item->status_id; 
                    $history->request_id = $req->id;
                    $history->request_item_id = $item->id;
                    $history->spuh_payment_status_id = 6; 
                    $history->user_id = Auth::id();
                    $history->save();

                    //$item->status_id = $status;
                    //$item->save();
                }
            }


            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";

            return redirect()->route('doctuinsituuttp.list', $req->id)->with('response', $response);

        }

        $response["status"] = false;

        return redirect()->route('doctuinsituuttp.edit', $id)->with('response', $response);
    }
}