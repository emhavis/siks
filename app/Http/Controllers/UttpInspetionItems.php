<?php

namespace App\Http\Controllers;

use App\MasterUsers;
use App\MasterRoles;
use App\MasterLaboratory;
use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\MasterTemplate;
use App\UttpInspectionItem;

class UttpInspetionItems extends Controller
{
    private $MyProjects;
    private $UttpInspectionItem;
    private $MasterTemplate;
    public function __construct()
    {
        $this->UttpInspectionItem = new UttpInspectionItem();
        $this->MyProjects = new MyProjects();
        $this->MasterTemplate = new MasterTemplate();
    }
    public function index(){
        $data = UttpInspectionItem :: with(
            [
                'MasterTemplate' => function ($query){
                    $query->orderBy('id', 'asc');
                },
            
            ])->get();
        $attribute = $this->MyProjects->setup("insitems");
        return View('uttp_inspection_items.index',compact('data','attribute'));
    }

    public function create($id = null){
        $attribute = $this->MyProjects->setup("insitems");

        $row = null;

        if($id)
        {
            $row = $this->MasterInstalasi->whereId($id)->first();
        }

        $templates = $this->MasterTemplate->pluck('title_tmp', 'laboratory_id');

        return View('insitems.create',compact('row','id','attribute','templates'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["name"] = ['required'];
        $rules["template_id"] = ['required'];
        $rules["order_no"] = ['required', 'integer', 'min:0'];
        $rules["no"] = ['required', 'integer', 'min:0'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->UttpInspectionItem->whereId($id)->update($request->all());
            }
            else
            {
                $this->UttpInspectionItem->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->UttpInspectionItem->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
    public function edit($id)
    {
        $row  = $this->UttpInspectionItem->find($id);
        dd($row);
        return view('uttp_inspection_items.edit', compact('row'));
    }    
    public function update(Request $request)
    {
        $response["status"] = false;

        $rules['brand'] = ['required'];
        $rules['made_in'] = ['required'];
        $rules['model'] = ['required'];
        $rules['tipe'] = ['required'];
        $rules['no_seri'] = ['required'];
        $rules['no_identitas'] = ['required'];
        $rules['capacity'] = ['required'];
        $rules['daya_baca'] = ['required'];
        $rules['class'] = ['required'];
        $rules['jumlah_per_set'] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $id = $request->id;

            $request->request->remove('id');

            $this->Standard->whereId($id)->update($request);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return response($response);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->UttpInspectionItem::whereId($id)
        ->first();

        if($row)
        {
            $this->UttpInspectionItem::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('insitems')->with($response);
    }

}