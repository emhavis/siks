<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\ServiceOrderUttps;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\HistoryUttp;
use App\MasterInstalasi;

class OrderUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("orderuttp");

        // LABORATORY_ID === STANDARD_MEASUREMENT_TYPE_ID
        // ID HARUS SAMA
        $laboratory_id = Auth::user()->laboratory_id;
        //$instalasi_id = Auth::user()->instalasi_id;

        $instalasiList = MasterInstalasi::where('lab_id', $laboratory_id)->orderBy('id','asc')
            ->get();

        foreach($instalasiList as $instalasi) {
            $rows[$instalasi->id] = ServiceOrderUttps::join('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_order_uttps.service_request_item_id' )
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id", 10);
            })
            ->whereHas('instalasi', function($query)use($laboratory_id, $instalasi)
            {
                $query->where("lab_id",$laboratory_id)
                    ->where("id",$instalasi->id);
            })
            ->whereNull('test_by_1')
            ->orderBy('service_request_uttp_items.order_at', 'asc')
            ->orderBy('service_request_uttp_items.received_at', 'asc')
            ->orderBy('service_request_uttp_items.delivered_at', 'asc')
            ->with([
                'ServiceRequestItem' => function ($query)
                {
                    $query
                        ->orderBy('order_at', 'asc')
                        ->orderBy('received_at', 'asc')
                        ->orderBy('delivered_at', 'asc');
                }
            ])
            ->get();
            
        }
        //dd($rows);

        return view('orderuttp.index',compact('rows','attribute','laboratory_id', 'instalasiList'));
    }    

    public function proses(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        $laboratory_id = Auth::user()->laboratory_id;
        //$instalasi_id = Auth::user()->instalasi_id;
        $instalasi_id = $request->instalasi_id;
        
        /*
        $rows = ServiceRequestUttpItem::where('status_id', 10)
            ->whereHas('uttp.type.instalasi', function($query)use($laboratory_id, $instalasi_id)
            {
                $query->where("lab_id",$laboratory_id)
                    ->where("id",$instalasi_id);
            })
            ->orderBy('order_at', 'asc')
            ->orderBy('received_at', 'asc')
            ->orderBy('delivered_at', 'asc')
            //->take(1)
            ->toSql();
            dd([$rows,$laboratory_id, $instalasi_id]);
        */

        $rows = ServiceOrderUttps::join('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_order_uttps.service_request_item_id' )
                ->whereHas('ServiceRequestItem', function($query) 
                {
                    $query->where("status_id", 10);
                })
                ->whereHas('instalasi', function($query)use($laboratory_id, $instalasi_id)
                {
                    $query->where("lab_id",$laboratory_id)
                        ->where("id",$instalasi_id);
                })
                ->whereNull('test_by_1')
                ->orderBy('service_request_uttp_items.order_at', 'asc')
                ->orderBy('service_request_uttp_items.received_at', 'asc')
                ->orderBy('service_request_uttp_items.delivered_at', 'asc')
                ->with([
                    'ServiceRequestItem' => function ($query)
                    {
                        $query
                            ->orderBy('order_at', 'asc')
                            ->orderBy('received_at', 'asc')
                            ->orderBy('delivered_at', 'asc');
                    }
                ])
                ->first();
                //dd([$rows,$laboratory_id, $instalasi_id]);

        if ($request->has('id')) {
            $item = ServiceRequestUttpItem::where("id", $request->id)->first();
        } else {
            $item = ServiceRequestUttpItem::where("no_order", $request->no_order)->first();
        }

        if ($item == null) {
            return response($response);
        }

        if ($item->id != $rows->service_request_item_id) {
            $response["messages"] = 'Alat bukan pada urutan pertama';
            return response($response);
        }

        //$item = ServiceRequestUttpItem::where("id", $request->id)->first();
        $item->status_id = 12;
        $item->save();

        $history = new HistoryUttp();
        $history->request_status_id = 12;
        $history->request_id = $item->request_id;
        $history->request_item_id = $item->id;
        $history->user_id = Auth::id();
        $history->save();
        
        
        ServiceRequestUttpItemInspection::where("request_item_id", $item->id)->update([
            "status_id"=>12,
        ]);

        $items_count = DB::table('service_request_uttp_items')
            ->selectRaw('sum(case when status_id = 12 then 1 else 0 end) count_12, count(id) count_all')
            ->where('request_id', $item->request_id)->get();

        if ($items_count[0]->count_12 == $items_count[0]->count_all) {
            ServiceRequestUttp::find($item->request_id)->update(['status_id'=>12]);
        }

        ServiceOrderUttps::where('service_request_item_id', $item->id)
        ->update([
            //"laboratory_id"=>Auth::user()->laboratory_id, //===UML_STANDARD_ID
            //"instalasi_id"=>$inspection->inspectionPrice->instalasi_id,
            //"service_request_id"=>$inspection->item->request_id,
            //"service_request_item_id"=>$inspection->request_item_id,
            //"service_request_item_inspection_id"=>$inspection->id,
            "lab_staff"=>Auth::id(),
            "staff_entry_datein"=>date("Y-m-d"),
            "mulai_uji"=>date("Y-m-d"),
        ]);

        $response["status"] = true;
        $response["messages"] = "Alat telah dikerjakan";
        
        return response($response);
    }

    /*
    public function proses($id)
    {
        $inspection = ServiceRequestUttpItemInspection::with([
            'inspectionPrice', 
            'item',
            'item.serviceRequest'
        ])
            ->find($id);

        ServiceOrderUttps::insert([
            "laboratory_id"=>Auth::user()->laboratory_id, //===UML_STANDARD_ID
            "instalasi_id"=>$inspection->inspectionPrice->instalasi_id,
            "service_request_id"=>$inspection->item->request_id,
            "service_request_item_id"=>$inspection->request_item_id,
            "service_request_item_inspection_id"=>$inspection->id,
            "lab_staff"=>Auth::id(),
            "staff_entry_datein"=>date("Y-m-d")
        ]);

        $inspection->update(["status_id"=>12]);

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id >= 12 then 1 else 0 end) count_12, count(id) count_all')
            ->where('request_item_id', $inspection->request_item_id)->get();
        
        if ($inspections_count[0]->count_12 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::whereId($inspection->request_item_id)
                ->update(["status_id"=>12]); // PENGUJIAN 
        }

        $items_count = DB::table('service_request_uttp_items')
            ->selectRaw('sum(case when status_id >= 12 then 1 else 0 end) count_12, count(id) count_all')
            ->where('request_id', $inspection->item->request_id)->get();

        if ($items_count[0]->count_12 == $items_count[0]->count_all) {
            ServiceRequestUttp::whereId($inspection->item->request_id)
                ->update(["status_id"=>12]); // PENGUJIAN
        }

        return Redirect::route('orderuttp');
    }
    */
    

    public function takeorder($id)
    {
        $attribute = $this->MyProjects->setup("takeorder");

        $request = ServiceRequestUttps::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        return view('requestuttp.edit_booking', compact('request', 'attribute'));
    }

    public function getinspections($id)
    {
        $item = ServiceRequestUttpItem::find($id);
        
        $inspection_types = [];
        foreach($item->inspections as $itemInspection) {
            $inspection_types[] = $itemInspection->inspectionPrice->inspection_type;
        }

        $response["inspections"] = $inspection_types;

        return response($response);
    }
}
