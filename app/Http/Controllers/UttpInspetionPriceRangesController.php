<?php

namespace App\Http\Controllers;

use App\MasterUsers;
use App\MasterRoles;
use App\MasterLaboratory;
use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\UttpInspectionItemTemplate;
use App\UttpInspectionPrice;
use App\MasterInstalasi;
use App\MasterServiceType;
use App\UttpInspectionPriceRange;

class UttpInspetionPriceRangesController extends Controller
{
    private $MyProjects;
    private $UttpInspectionPrice;
    private $UttpInspectionPriceRange;
    private $UttpInspectionTemplate;
    private $MasterInstalasi;
    private $MasterServiceType;
    public function __construct()
    {
        $this->UttpInspectionPrice = new UttpInspectionPrice();
        $this->UttpInspectionPriceRange = new UttpInspectionPriceRange();
        $this->MyProjects = new MyProjects();
        $this->UttpInspectionTemplate = new UttpInspectionItemTemplate();
        $this->MasterInstalasi = new MasterInstalasi();
        $this->MasterServiceType = new MasterServiceType();
    }
    public function index(){
        $data = UttpInspectionPriceRange :: get();
        $attribute = $this->MyProjects->setup("uttpinspectionprice");
        return View('uttpinspectionpriceranges.index',compact('data','attribute'));
    }

    public function create($id = null){
        $attribute = $this->MyProjects->setup("uttpinspectionprice");

        $row = null;

        if($id)
        {
            $row = $this->MasterInstalasi->whereId($id)->first();
        }

        $templates = $this->UttpInspectionTemplate->pluck('name', 'id');
        $instalasi = $this->MasterInstalasi->pluck('nama_instalasi', 'id');
        $services = $this->MasterServiceType->pluck('service_type', 'id');
        $prices = $this->UttpInspectionPrice->pluck('inspection_type', 'id');

        return View('uttpinspectionpriceranges.create',compact('row','id','attribute','templates','instalasi','services','prices'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["min_range"] = ['required'];
        $rules["inspetion_price_id"] = ['required'];
        $rules["max_range"] = ['required'];
        $rules["price_unit"] = ['required'];
        $rules["price"] = ['required'];
        $rules["range_unit"] = ['required'];
        $rules["inspection_type"] = ['required'];
        //generating date when inserted 
        $request['created_at'] = date('Y-m-d H:i:s');
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->UttpInspectionPrice->whereId($id)->update($request->all());
            }
            else
            {
                $this->UttpInspectionPrice->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->UttpInspectionItem->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
    public function edit($id)
    {
        $row  = $this->UttpInspectionItem->find($id);
        return view('uttp_inspection_items.edit', compact('row'));
    }    
    public function update(Request $request)
    {
        $response["status"] = false;

        $rules['brand'] = ['required'];
        $rules['made_in'] = ['required'];
        $rules['model'] = ['required'];
        $rules['tipe'] = ['required'];
        $rules['no_seri'] = ['required'];
        $rules['no_identitas'] = ['required'];
        $rules['capacity'] = ['required'];
        $rules['daya_baca'] = ['required'];
        $rules['class'] = ['required'];
        $rules['jumlah_per_set'] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $id = $request->id;

            $request->request->remove('id');

            $this->Standard->whereId($id)->update($request);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return response($response);
    }

}