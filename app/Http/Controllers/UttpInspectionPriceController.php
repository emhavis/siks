<?php

namespace App\Http\Controllers;

use App\UttpInspectionPrice;
use App\MasterServiceType;
use App\MasterInstalasi;
use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UttpInspectionPriceController extends Controller
{
    private $UttpInspectionPrice;
    private $MyProjects;

    public function __construct() 
    {
        $this->UttpInspectionPrice = new UttpInspectionPrice();
        $this->MasterInstalasi = new MasterInstalasi();
        $this->MasterServiceType = new MasterServiceType();
        $this->MyProjects = new MyProjects();
    }

    public function index() 
    {
        $attribute = $this->MyProjects->setup("insprice");

        $prices = $this->UttpInspectionPrice->get();
        
        return view('uttpinspectionprice.index', compact('prices','attribute'));
    }

    public function create($id=null) 
    {
        $attribute = $this->MyProjects->setup("insprice");

        $row = null;

        if($id)
        {
            $row = $this->UttpInspectionPrice->whereId($id)->first();
        }

        $instalasi = $this->MasterInstalasi->pluck('nama_instalasi', 'id');
        $types = $this->MasterServiceType->pluck('service_type', 'id');

        return view('uttpinspectionprice.create',compact('row','id','attribute','instalasi','types'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["inspection_type"] = ['required'];
        $rules["service_type_id"] = ['required'];
        $rules["instalasi_id"] = ['required'];
        $rules["inspection_template_id"] = ['required'];
        $rules["price"] = ['required', 'integer', 'min:0'];
        $rules["unit"] = ['required'];
        $rules["has_range"] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            $request["has_range"] = $request->get("has_range") === "ya";
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $request["updated_at"] = date("Y-m-d H:i:s");
                $this->UttpInspectionPrice->whereId($id)->update($request->all());
            }
            else
            {
                $request["created_at"] = date("Y-m-d H:i:s");
                $this->UttpInspectionPrice->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->UttpInspectionPrice->find($request->id);

        if(count($row->MasterUsers)>0)
        {
            $response["message"] = "Nama Laboratorium sudah terpakai di User List";
        }
        else
        {
            if($request->action=="delete")
            {
                $row->delete();
                $response["status"] = true;
            }
        }

        return response($response);
    }
}
