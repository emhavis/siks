<?php

namespace App\Http\Controllers;

use App\MasterUsers;
use App\MasterRoles;
use App\MasterLaboratory;
use App\MasterUml;
use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Validator;

class UserprofilController extends Controller
{
    private $MasterUsers;
    private $MasterRoles;
    private $MasterLaboratory;
    private $MasterUml;
    private $MyProjects;

    public function __construct() 
    {
        $this->MasterUsers = new MasterUsers();
        $this->MasterRoles = new MasterRoles();
        $this->MasterLaboratory = new MasterLaboratory();
        $this->MasterUml = new MasterUml();
        $this->MyProjects = new MyProjects();
    }

    public function index() 
    {
        $attribute = $this->MyProjects->setup("userprofil");
        if($attribute==null) return userlogout();
        $id = Auth::id();
        $row = $this->MasterUsers->whereId($id)->first();

        return view('userprofil.create',compact('row','attribute'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;

        $rules["full_name"] = ['required'];
        $rules["username"] = ['required',Rule::unique('users')->ignore(Auth::id())];
        $rules["email"] = ['required','email',Rule::unique('users')->ignore(Auth::id())];
        
        $validation = Validator::make($request->all(),$rules,error_messages());
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            $this->MasterUsers->whereId(Auth::id())->update($request->all());

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = true;
        switch ($request->action) 
        {
            case 'delete':
                $this->MasterUsers->whereId($request->id)->delete();
                break;
            case 'aktif':
                $this->MasterUsers->whereId($request->id)->update(["is_active"=>"t"]);
                break;
            case 'nonaktif':
                $this->MasterUsers->whereId($request->id)->update(["is_active"=>null]);
                break;
        }

        return response($response);
    }

    public function storePassword(Request $request) 
    {
        $response["status"] = false;

        $rules["password"] = ['required'];
        $rules["newpassword"] = ['required', 'confirmed'];

        $validation = Validator::make($request->all(),$rules,error_messages());
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            $user = Auth::user();
            /*
            dd([$user->password, $request->get('password'), 
                Hash::make($request->get('password')), 
                Hash::check($request->get('password'), $user->password)]);
            */

            if ( Hash::check($request->get('password'), $user->password) ) {
                $this->MasterUsers->whereId(Auth::id())->update([
                    "password" => Hash::make($request->get('newpassword'))
                ]);

                $response["status"] = true;
                $response["messages"] = "Data berhasil disimpan";
            } else {
                $response["messages"] = "Password lama tidak sesuai";
            }
        }

        return response($response);
    }

}
