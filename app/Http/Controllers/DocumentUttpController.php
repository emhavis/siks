<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrderUttps;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpStaff;
use App\ServiceRequestUttpInsituDoc;

use App\RevisionUttp;
use App\Customer;
use App\ServiceRequestUttpInsituStaff;

use LynX39\LaraPdfMerger\Facades\PdfMerger;
use setasign\Fpdi\Tcpdf\Fpdi;
use Illuminate\Support\Str;

use PDF;

class DocumentUttpController extends Controller
{
    
    public function __construct()
    {

    }

    private function checkToken($token) 
    {
        $token = DB::table('user_access_tokens')
                    ->where('token', $token)
                    ->where('expired_at', '>', date("Y-m-d H:i:s"))
                    ->first();

        return $token;
    }

    public function print($id, $jenis_sertifikat, $token)
    {
        $token = $this->checkToken($token);

        if ($token) {
            $order = ServiceOrderUttps::find($id);

            if ($jenis_sertifikat == 'skhp') {
                $file_name = 'SKHP '.($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);
                
                //$qrcode_generator = route('documentuttp.valid', $order->qrcode_token);
                $qrcode_generator = route('documentuttp.valid', [
                    'jenis_sertifikat' => $jenis_sertifikat,
                    'token' => $order->qrcode_token,
                ]);
    
                $blade = 'serviceuttp.skhp_tipe_pdf';
                if ($order->ServiceRequest->service_type_id == 4 || 
                    $order->ServiceRequest->service_type_id == 5) {
                        $blade = 'serviceuttp.skhp_ttu_pdf';

                        if ($order->ServiceRequestItem->uttp->type->kelompok == 'CTMS Gas' || 
                            $order->ServiceRequestItem->uttp->type->kelompok == 'CTMS BBM') {
                                $blade = 'serviceuttp.skhp_ttu_ctms_pdf';
                        }
                }
    
                $view = false;
    
                $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 
                        'isRemoteEnabled' => true,
                        'tempDir' => public_path(),
                        'chroot'  => public_path('assets/images/logo'),
                        'defaultPaperSize' => 'a4',
                        'defaultMediaType' => 'print',
                    ])
                    ->loadview($blade,compact('order', 'qrcode_generator', 'view'));
    
                return $pdf->download($file_name . '.pdf');
            } elseif ($jenis_sertifikat == 'skhpt') {
                $file_name = 'SKHPT '.($order->no_sertifikat_tipe ? $order->no_sertifikat_tipe : $order->ServiceRequestItem->no_order);
               
                //$qrcode_generator = route('documentuttp.valid', $order->qrcode_token);
                $qrcode_generator = route('documentuttp.valid', [
                    'jenis_sertifikat' => $jenis_sertifikat,
                    'token' => $order->qrcode_skhpt_token,
                ]);
    
                $blade = 'serviceuttp.skhpt_tipe_pdf';
                
                $view = false;
    
                $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 
                        'isRemoteEnabled' => true,
                        'tempDir' => public_path(),
                        'chroot'  => public_path('assets/images/logo'),
                        'defaultPaperSize' => 'a4',
                        'defaultMediaType' => 'print',
                    ])
                    ->loadview($blade,compact('order', 'qrcode_generator', 'view'));
    
                return $pdf->download($file_name . '.pdf');

            } elseif ($jenis_sertifikat == 'set') {
                $file_name = 'SET '.($order->no_surat_tipe ? $order->no_surat_tipe : $order->ServiceRequestItem->no_order);

                $order = ServiceOrderUttps::with([
                    'ServiceRequest', 
                    'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
                    'MasterUsers', 'LabStaffOut',
                    'persyaratanTeknis', 
                    'inspections' => function ($query)
                    {
                        $query->orderBy('id', 'asc');
                    },
                ])->find($id);
        
                $otherOrders = ServiceOrderUttps::whereHas(
                    'ServiceRequestItem', function($query) use($order)
                    {
                        $query->where("uttp_id",$order->ServiceRequestItem->uttp_id);  
                    })
                    ->get();
        
                //$qrcode_generator = route('documentuttp.valid', $order->qrcode_tipe_token);
                $qrcode_generator = route('documentuttp.valid', [
                    'jenis_sertifikat' => $jenis_sertifikat,
                    'token' => $order->qrcode_tipe_token,
                ]);
        
                $blade = 'serviceuttp.set_tipe_pdf';
        
                $view = false;

                $pdf = PDF::setOptions([
                    'isHtml5ParserEnabled' => true, 
                    'isRemoteEnabled' => true,
                    'tempDir' => public_path(),
                    'chroot'  => public_path('assets/images/logo'),
                    'defaultPaperSize' => 'a4',
                    'defaultMediaType' => 'print',
                ])
                ->loadview($blade,compact(['order', 'otherOrders', 'qrcode_generator', 'view']));
           
                return $pdf->download($file_name . '.pdf');
            } 
        }

        abort(403);
    }

    public function print_revision($id, $jenis_sertifikat, $token)
    {
        $revision = RevisionUttp::find($id);
        // $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);
        
        $file_name = 'SKHP '.($revision->order->no_sertifikat ? $revision->order->no_sertifikat : $revision->order->ServiceRequestItem->no_order);

        $next_count = $revision->preview_count + 1;
        $revision->update([
            'preview_count' => $next_count
        ]);

        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $revision->order->qrcode_token,
        ]);
       
        $blade = 'revisiontechorderuttp.skhp_tipe_pdf';
        if ($revision->request->service_type_id == 4 || 
            $revision->request->service_type_id == 5) {
                $blade = 'revisiontechorderuttp.skhp_ttu_pdf';
        }

        $view = false;
        
        $order = $revision->order;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact('revision','order', 'qrcode_generator', 'view'));

        return $pdf->download($file_name . '.pdf');
    }

    public function print_old($id, $token)
    {
        $token = $this->checkToken($token);

        if ($token) {
            $order = ServiceOrderUttps::find($id);

            $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

            $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

            $blade = 'serviceuttp.skhp_tipe_pdf';
            if ($order->ServiceRequest->service_type_id == 4 || 
                $order->ServiceRequest->service_type_id == 5) {
                    $blade = 'serviceuttp.skhp_ttu_pdf';
            }

            $view = false;

            $pdf = PDF::setOptions([
                    'isHtml5ParserEnabled' => true, 
                    'isRemoteEnabled' => true,
                    'tempDir' => public_path(),
                    'chroot'  => public_path('assets/images/logo'),
                    'defaultPaperSize' => 'a4',
                    'defaultMediaType' => 'print',
                ])
                ->loadview($blade,compact('order', 'qrcode_generator', 'view'));

            return $pdf->download($file_name . '.pdf');
        }

        abort(403);
    }

    public function print_old2($id, $token)
    {
        $token = $this->checkToken($token);

        if ($token) {
            $order = ServiceOrderUttps::with([
                'ServiceRequest', 
                'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
                'MasterUsers', 'LabStaffOut',
                'persyaratanTeknis', 
                'inspections' => function ($query)
                {
                    $query->orderBy('id', 'asc');
                },
            ])->find($id);
    
            $file_name = 'skhp '.$order->no_sertifikat;
    
            /*
            $qrcode_generator = 
                "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
                "Ditandatangai oleh: " . "\r\n" .
                ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
                'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
                date("Y-m-d H:i:s",  strtotime($order->updated_at));
            */
            $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);
    
            $blade = 'serviceuttp.skhp_tipe_pdf';
            if ($order->ServiceRequest->service_type_id == 4 || 
                $order->ServiceRequest->service_type_id == 5) {
                    $blade = 'serviceuttp.skhp_ttu_pdf';
            }
    
            $view = false;
            $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo')
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));
            //$pdf = PDF::loadview($blade,compact('order', 'qrcode_generator'));
    
            return $pdf->download($file_name . '.pdf');
            //return view($blade,compact('order','qrcode_generator')); 
        }

        abort(403);
    }

    // public function download($id, $token)
    // {
    //     $token = $this->checkToken($token);

    //     if ($token) {
    //         $order = ServiceOrderUttps::find($id);

    //         return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
    //     }

    //     abort(403);
    // }

    public function download($id,$tokens)
    {
        $token = $this->checkToken($tokens);
        if(!$token){
            abort(403);
        }

        $pdfMerger = PdfMerger::init();
        $order = ServiceOrderUttps::find($id);
        date_default_timezone_set("Asia/Singapore");

        if(
            (null != $order && isset($order->kabalai_date) && 
            $order->kabalai_date < date('Y-m-d',strtotime('19-09-2024')) || 
            null == $order->kabalai_date)
        ){
            return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
        }

        if ($order->no_sertifikat == null) {
            return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
        }

        if(
            $order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5
        ){
            $this->MakeClips('app/public/sample.pdf',$order->qrcode_token,$order->no_sertifikat,$order->id);
            $no = str_replace('/','_',$order->no_sertifikat);
            $no = "Lampiran-No.".$no.".pdf";

            return Storage::disk('public')->download("uttp-lampiran/".$no);
        }
        return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
    }

    public function valid_old($token)
    {
        if ($token) {
            $order = ServiceOrderUttps::with([
                'ServiceRequest', 
                'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
                'MasterUsers', 'LabStaffOut',
                'persyaratanTeknis', 
                'inspections' => function ($query)
                {
                    $query->orderBy('id', 'asc');
                },
            ])->where('qrcode_token', $token)
            ->first();
    
            $file_name = 'skhp'.$order->ServiceRequest->no_order;
    
            /*
            $qrcode_generator = 
                "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
                "Ditandatangai oleh: " . "\r\n" .
                ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
                'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
                date("Y-m-d H:i:s",  strtotime($order->updated_at));
                */
            $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);
            
    
            $blade = 'serviceuttp.skhp_pdf';
            if ($order->ServiceRequest->service_type_id == 4 || 
                $order->ServiceRequest->service_type_id == 5) {
                    $blade = 'serviceuttp.skhp_pdf_ttu';
            }
    
            $view = true;
            /*
            $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo')
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));
            */
            //$pdf = PDF::loadview($blade,compact('order', 'qrcode_generator'));
    
            //return $pdf->download('skhp.pdf');
            return view($blade,compact('order','qrcode_generator', 'view')); 
        }

        abort(403);
    }

    public function valid($jenis_sertifikat, $token)
    {
        if ($token && $jenis_sertifikat) {

            if ($jenis_sertifikat != 'surat_tugas') {

                $order = ServiceOrderUttps::with([
                    'ServiceRequest', 
                    'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
                    'MasterUsers', 'LabStaffOut',
                    'persyaratanTeknis', 
                    'inspections' => function ($query)
                    {
                        $query->orderBy('id', 'asc');
                    },
                ]);
                
                if ($jenis_sertifikat == 'skhp') {
                    $order = $order->where('qrcode_token', $token);
                } elseif ($jenis_sertifikat == 'skhpt') {
                    $order = $order->where('qrcode_skhpt_token', $token);
                } elseif ($jenis_sertifikat == 'set') {
                    $order = $order->where('qrcode_tipe_token', $token);
                }
                
                $order = $order->first();

                $revision = RevisionUttp::where('order_id', $order->id)->first();
        
                $blade = 'serviceuttp.valid';
        
                $view = true;
                return view($blade,compact(['order','view', 'jenis_sertifikat', 'revision']));

            } else {

                $doc = ServiceRequestUttpInsituDoc::with([
                    'request', 'listOfStaffs', 'acceptedBy'                    
                ])
                    ->where('token', $token)->first();

                $blade = 'schedulinguttp.valid';
        
                $view = true;
                return view($blade,compact(['doc', 'view']));
            } 
        }

        abort(403);
    }

    public function invoice($id, $token)
    {
        $token = $this->checkToken($token);
        
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUttpInsituDoc::where('request_id', $id)->get();

        if ($token) {
            $row = ServiceRequestUttp::find($id);

            if ($row->lokasi_pengujian == 'luar') {
                return view('requestluaruttp.pdf',compact(['row', 'staffes', 'docs']));
            } else {
                return view('requestuttp.pdf',compact(['row','staffes','docs']));
            }
        }
        
        abort(403);
    }


    public function invoicetuhp($id, $token)
    {
        $token = $this->checkToken($token);
        
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->orderBy('id', 'asc')->get();
        $docs = ServiceRequestUttpInsituDoc::where('request_id', $id)->get();

        if ($token) {
            $row = ServiceRequestUttp::find($id);
            return view('requestluaruttp.pdf_tuhp',compact(['row','staffes','docs']));
        }
        
        abort(403);
    }

    public function kuitansi($id, $token)
    {
        $token = $this->checkToken($token);
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUttpInsituDoc::where('request_id', $id)->get();

        if ($token) {
            $row = ServiceRequestUttp::find($id);
            $terbilang = $this->convert($row->total_price);
            $terbilangSPUH = $this->convert($row->spuh_price);

            //$terbilangSPUH = $this->convert($row->spuh_price);
            $terbilangSPUH = [];
            $terbilangActSPUH = [];
            foreach($docs as $doc) {
                $terbilangSPUH[$doc->id] = $this->convert($doc->invoiced_price);
                if ($doc->act_price != null && $doc->act_price > $doc->price) {
                    $terbilangActSPUH[$doc->id] = $this->convert($doc->act_price - $doc->price);
                }
            }

            return view('requestuttp.kuitansi_pdf',
                compact(['row', 'staffes', 'docs', 'terbilang','terbilangSPUH','terbilangActSPUH']));
        }

        abort(403);
    }

    private function convert($number)
    {
        //$number = str_replace('.', '', $number);
        //dd(is_float((float)$number));
        //if ( ! is_float($number)) throw new Exception("Please input number.");
        $number = (float)$number;
        $base    = array('nol', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
        $numeric = array('1000000000000000', '1000000000000', '1000000000000', 1000000000, 1000000, 1000, 100, 10, 1);
        $unit    = array('kuadriliun', 'triliun', 'biliun', 'milyar', 'juta', 'ribu', 'ratus', 'puluh', '');
        $str     = null;
        $i = 0;
        if ($number == 0) {
            $str = 'nol';
        } else {
            while ($number != 0) {
                $count = (int)($number / $numeric[$i]);
                if ($count >= 10) {
                    $str .= static::convert($count) . ' ' . $unit[$i] . ' ';
                } elseif ($count > 0 && $count < 10) {
                    $str .= $base[$count] . ' ' . $unit[$i] . ' ';
                }
                $number -= $numeric[$i] * $count;
                $i++;
            }
            $str = preg_replace('/satu puluh (\w+)/i', '\1 belas', $str);
            $str = preg_replace('/satu (ribu|ratus|puluh|belas)/', 'se\1', $str);
            $str = preg_replace('/\s{2,}/', ' ', trim($str));
        }
        return $str;
    }

    public function buktiorder($id, $token)
    {
        $token = $this->checkToken($token);

        if ($token) {
            $row = ServiceRequestUttp::find($id);
            return view('requestuttp.order_pdf',compact('row'));
        }

        abort(403);
    }

    public function surattugas($id, $token)
    {
        $token = $this->checkToken($token);

        if ($token) {
            $row = ServiceRequestUttp::find($id);
            //$doc = ServiceRequestUttpInsituDoc::where('id', $row->spuh_doc_id)->first();
            $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();
            $doc_inisial = ServiceRequestUttpInsituDoc::where('request_id', $id)->where('jenis', 'inisial')->first();

            $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
                    ->find($id);
            $requestor = Customer::find($request->requestor_id);

            //$doc = ServiceRequestUttpInsituDoc::find($request->spuh_doc_id);
            $staffes = ServiceRequestUttpInsituStaff::where('doc_id', $doc->id)->get();

            $file_name = 'Surat Tugas '.($request->no_order ? $request->no_order : 'XXXXXX');

            $qrcode_generator = route('documentuttp.valid', [
                'jenis_sertifikat' => 'surat_tugas',
                'token' => $doc->token,
            ]);
    
            $blade = 'schedulinguttp.surat_tugas_pdf';

            $view = false;
    
            $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact('request', 'requestor', 'doc', 'doc_inisial', 'staffes', 'qrcode_generator', 'view'));

            return $pdf->stream();
            
            /*
            if ($doc->jenis == 'inisial')
            {
                
            } else {
                return Storage::disk('public')->download($doc->spuh_path, $doc->spuh_file);
            }
                */
            
        }
        
        abort(403);
    }

    public function MakeClips($strfilepath,$qrcode_token,$no_sertifikat,$id_order)
    {
        $fileloc='';
        $no_sertifikat ='XXX';
        $pathtoQR = storage_path('app/public/tmp/qrcode.png');;
        $order = ServiceOrderUttps::with(['ServiceRequestItem'])->find($id_order);
        // dd($order->ServiceRequestItem);
        if($order->no_sertifikat !=null){
            $no_sertifikat = "Lampiran-No.".$order->no_sertifikat;
        }

        $pdf = new \setasign\Fpdi\Tcpdf\Fpdi();
        
        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);
        
        if(file_exists($pathtoQR)){ 
            $fs = new \Illuminate\Filesystem\Filesystem;
            $fs->cleanDirectory(storage_path('app/public/tmp')) ;
            $fs->cleanDirectory(storage_path('app/public/uttp-lampiran')) ;
        }
        $qrcode =  base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator,storage_path('app/public/tmp/qrcode.png')));
        
        if($qrcode){
            $pathtoQR = storage_path('app/public/tmp/qrcode.png');
        }

        $fileloc = storage_path('app/public/'.$order->path_skhp);
        // dd($fileloc);
        if(file_exists($fileloc)){ 
            $pagecount = $pdf->setSourceFile($fileloc); 
        }else{ 
            die('File PDF Tidak ditemukan.!'); 
        } 
        // Add watermark image to PDF pages 
        for($i=1;$i<=$pagecount;$i++){ 
            $tplidx = $pdf->importPage($i);
            $specs = $pdf->getTemplateSize($tplidx);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->addPage($specs['height'] > $specs['width'] ? 'P' : 'L');
            $pdf->useTemplate($tplidx, null, null, $specs['width'], $specs['height'], true);
            
            /**  Header Lampiran
             * Setting text
            */
            $pdf->SetXY(5, 5);
            $_x_text = ($specs['width']- 60) - ($pdf->GetStringWidth($order->no_sertifikat, '', '',10)/2.8);
            $_y_text = $specs['height']- $specs['height'] +5+5 - 10;

            //setting for name of kabalai
            $_x_Hkab = ($specs['width'] -100 ) - ($pdf->GetStringWidth('Nomor', '', '',10)/2.8);
            $_y_Hkab = $specs['height']-90 - 10;

            // line2
            $_x_HL2kab = ($specs['width'] -100 ) - ($pdf->GetStringWidth('Pemilik', '', '',10)/2.8);
            $_y_HL2kab = $specs['height']-86 - 10;

            // line3
            $_x_HL3kab = ($specs['width'] -100 ) - ($pdf->GetStringWidth('Tempat', '', '',10)/2.8);
            $_y_HL3kab = $specs['height']-82 - 10;

            //setting for name of kabalai
            $_x_kab = ($specs['width'] -100 ) - ($pdf->GetStringWidth( 'Kabalai', '', '',10)/2.8);
            $_y_kab = $specs['height']-49 - 10;

            //Writing nip
            $_x_Nkab = ($specs['width'] -100 ) - ($pdf->GetStringWidth( 'Kabalai', '', '',10)/2.8);
            $_y_Nkab = $specs['height']-45 - 10;
            //QR
            $_x = ($specs['width']-70) - ($pdf->GetStringWidth($order->no_sertifikat, '', '', 10)/2.8);
            $_y = $specs['height'] -15 - 10;

            // if($i ==1){
            $pdf->SetFont('', '', 9);
            $pdf->SetXY($_x_text, $_y_text);
            $pdf->setAlpha(1);
            $pdf->Write(0,"Lampiran ".$order->no_sertifikat);
            // }
            /**
             * QR Code
             */
            if($i == 1){
                $b = "Bandung".',';
                $text = $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y'));
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_Hkab, $_y_Hkab -4);
                $pdf->setAlpha(1);
                $pdf->Write(0, $b);

                $b = "Bandung".',';
                $text = $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y'));
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_Hkab + 17, $_y_Hkab -4);
                $pdf->setAlpha(1);
                $pdf->Write(0, $text);

                //Setting header and name kabalai
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_Hkab, $_y_Hkab);
                $pdf->setAlpha(1);
                $pdf->Write(0,"Kepala Balai Pengujian");

                //Line 2
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_HL2kab, $_y_HL2kab);
                $pdf->setAlpha(1);
                $pdf->Write(0,"Alat Ukur, Alat Takar, Alat Timbang");

                //Line 3-1
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_HL3kab, $_y_HL3kab);
                $pdf->setAlpha(1);
                $pdf->Write(0,"dan Alat Perlengkapan");

                //writing NIP
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_Nkab, $_y_Nkab);
                $pdf->setAlpha(1);
                $pdf->Write(0,"NIP : ". $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nip : '' );

                //Setting header and name kabalai
                $pdf->SetFont('', 'U', 10);
                $pdf->SetXY($_x_kab, $_y_kab);
                $pdf->setAlpha(1);
                $pdf->Write(0,$order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP');
                // QR
                $pdf->Image($pathtoQR, $_x-9.8,$_y-61.3, 25, 26, 'png'); 
                
            }
        } 
        
        // Output PDF with watermark 
        $no = str_replace('/','_',$no_sertifikat);
        if($order->cancel_at != null && $order->cancel_notes !=null){
            $down = $pdf->Output(storage_path('app/public/tmp/SBL-'.$no.'.pdf'),'F');
        }else{
            $down = $pdf->Output(storage_path('app/public/uttp-lampiran/'.$no.'.pdf'),'F');
        }
        // $down = $pdf->Output(storage_path('app/public/tmp/SERTIFIKAT-'.$no.'.pdf'),'F');
        if($down){
            // Storage::delete('app/public/tmp/SERTIFIKAT-'.$no.'.pdf');
        }
    }
}
