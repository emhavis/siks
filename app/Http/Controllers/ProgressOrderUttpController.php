<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Mail\CertificateDone;
use App\Mail\Cancel;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;

class ProgressOrderUttpController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("progressorderuttp");

        $laboratory_id = Auth::user()->laboratory_id;
        $approval_before = Auth::user()->user_role == 3 || Auth::user()->user_role == 9 ? 0 : 2;
        
        /*
        $rows = ServiceOrderUttps::whereIn("stat_service_order",[0,1,2,3,4])
        ->orderBy('staff_entry_datein','desc');
        */

        /*
        if (Auth::user()->user_role == 3) {
            $rows = $rows->where('laboratory_id', $laboratory_id)
                ->whereIn("service_request_uttps.service_type_id", [6,7]);
        }
        if (Auth::user()->user_role == 9) {
            $rows = $rows->whereIn("service_request_uttps.service_type_id", [4,5]);
        }
        */

        $rows = ServiceOrderUttps::whereRaw('1=1')
        ->join('service_request_uttps as sru','service_order_uttps.service_request_id','=','sru.id')
        ->join('service_request_uttp_items as srui','service_order_uttps.service_request_item_id','=','srui.id')
        //->join('service_request_uttp_item_inspections as sruii','srui.id','=','sruii.request_item_id')
        ->join('master_instalasi as mi','service_order_uttps.instalasi_id','=','mi.id')
        ->join('master_request_status as s', 'srui.status_id', '=', 's.id')
        ->leftJoin('users as us', 'service_order_uttps.lab_staff', '=', 'us.id')
        ->leftJoin('users as us1', 'service_order_uttps.test_by_1', '=', 'us1.id')
        ->leftJoin('users as us2', 'service_order_uttps.test_by_2', '=', 'us2.id')
        ->whereIn("service_order_uttps.stat_service_order",[0,1,2])
        //->select('service_order_uttps.id')
        ->addSelect(
            DB::raw(
                'distinct service_order_uttps.id, 
                    srui.no_order, 
                    service_order_uttps.tool_brand,
                    service_order_uttps.tool_model,
                    service_order_uttps.tool_type, 
                    service_order_uttps.tool_serial_no,
                    us.full_name lab_staff,
                    us1.full_name test_by_1,
                    us2.full_name test_by_2,
                    service_order_uttps.staff_entry_datein,
                    service_order_uttps.staff_entry_dateout,
                    service_order_uttps.stat_sertifikat,
                    service_order_uttps.stat_warehouse,
                    service_order_uttps.file_skhp,
                    service_order_uttps.is_skhpt,
                    service_order_uttps.has_set,
                    service_order_uttps.ujitipe_completed,
                    service_order_uttps.cancel_at,
                    service_order_uttps.cancel_id,
                    sru.service_type_id,
                    s.status,
                    srui.order_at::date order_at,
                case 
                when
                    service_order_uttps.kabalai_date is null 
                        then \'ONP\'
                when 
                    service_order_uttps.kabalai_date is not null and weekdays_sql(srui.order_at::date, coalesce(service_order_uttps.kabalai_date, current_date))  
                        <= (case when sru.service_type_id in (4,5) then mi.sla_day else mi.sla_tipe_day end)
                        then \'ONT\'
                else \'LATE\'
                end as sla_status, 
                weekdays_sql(srui.order_at::date, coalesce(service_order_uttps.kabalai_date, current_date)) as sla_days,
                case
                    when weekdays_sql(srui.order_at::date, coalesce(service_order_uttps.kabalai_date, current_date)) 
                        <= (case when sru.service_type_id in (4,5) then mi.sla_mid_day else mi.sla_tipe_mid_day end)
                        then \'GREEN\'
                    when weekdays_sql(srui.order_at::date, coalesce(service_order_uttps.kabalai_date, current_date)) 
                        > (case when sru.service_type_id in (4,5) then mi.sla_mid_day else mi.sla_tipe_mid_day end)
                        and 
                        weekdays_sql(srui.order_at::date, coalesce(service_order_uttps.kabalai_date, current_date)) 
                        <= (case when sru.service_type_id in (4,5) then mi.sla_day else mi.sla_tipe_day end)
                        then \'YELLOW\'
                    else \'RED\'
                end as sla_status_day,
                sru.lokasi_pengujian,
                sru.id request_id,
                sru.is_integritas,
                sru.spuh_doc_id
                '))
        //->orderBy('service_order_uttps.id');
        ->orderBy('service_order_uttps.staff_entry_datein','desc');

        $rows = $rows->get();
        $rows_kn = $rows->filter(function ($value, $key) {
            return $value->lokasi_pengujian == 'dalam';
        });
        $rows_dl = $rows->filter(function ($value, $key) {
            return $value->lokasi_pengujian == 'luar';
        });

        return view('progressorderuttp.index',compact('attribute', 'rows_dl', 'rows_kn'));
    }

}
