<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\Article;
use App\MasterArticleType;

class ArticleController extends Controller
{
    private $MyProjects;
    private $Article;

    public function __construct()
    {
        $this->Article = new Article();
        $this->MasterArticleType = new MasterArticleType();
        $this->MyProjects = new MyProjects();
    }

    public function index(){
        // \DB::enableQueryLog();
        $data = $this->Article->all();

        $attribute = $this->MyProjects->setup("article");

        return View('article.index',compact('data','attribute'));
    }

    public function create(){
        $attribute = $this->MyProjects->setup("article");
        //$types = $this->MasterArticleType->pluck('article_type','id');

        return View('article.create',compact(['attribute']));
    }

    public function store(Request $request)
    {
        $response["article"] = false;
        $rules["title"] = ['required'];
        $rules["content"] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);

            $data['title'] = $request->get('title');
            $data['content'] = $request->get('content');
            $data['featured'] = $request->get('featured') ? true : false;
            $data['published'] = $request->get('published') ? true : false;
            $data['priority_index'] = $request->get('priority_index');
            $data['excerpt'] = $request->get('excerpt');
            //$data['article_type_id'] = $request->get('article_type_id');

            if($request->hasFile('cover_image')){
                $file = $request->file('cover_image');
                $data['cover_image'] = $file->getClientOriginalName();

                $path = $file->store(
                    'images',
                    'public'
                );

                $data['cover_image_path'] = $path;
            }
            $this->Article->create($data);
            
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return redirect()->route('article')->with($response);
    }

    public function edit($id)
    {
        $attribute = $this->MyProjects->setup("article");
        $row  = $this->Article->find($id);
        return view('article.edit', compact([
            'row','attribute',
        ]));
    }   

    public function update($id, Request $request)
    {
        $response["status"] = false;
        $response["article"] = false;
        $rules["title"] = ['required'];
        $rules["content"] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $this->Article->whereId($id)->update(
            [
                "title" => $request->get('title'),
                "content" => $request->get('content'),
                "published" => $request->get('published') ? true : false,
                "excerpt" => $request->get('excerpt'),
                "featured" => $request->get('featured') ? true : false,
                "article_type_id" => $request->get('article_type_id'),
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return redirect()->route('article')->with($response);
    }
    
    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->Article::whereId($id)->first();

        if($row)
        {
            $this->Article::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('article')->with($response);
    }

}