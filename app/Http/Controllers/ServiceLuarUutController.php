<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrders;
use App\ServiceOrderInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\ServiceRequestItemInspection;
use App\ServiceRequestItem;
use App\MasterUsers;
use App\MasterServiceType;
use App\MasterStandardType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrderUutTTUInspection;
use App\ServiceOrderUutTTUInspectionItems;
use App\ServiceOrderUutTTUInspectionBadanHitung;
use App\ServiceOrderUutTTUPerlengkapan;
use App\ServiceOrderUutEvaluasiTipe;
use App\ServiceRequestUutInsituStaff;

use App\Mail\Pending;
use Illuminate\Support\Facades\Mail;
use App\Http\Libraries\PDFWatermark;
use App\Http\Libraries\PDFWatermarker;

use App\Jobs\ProcessEmailJob;

use PDF;

use App\MasterInstalasi;
use App\HistoryUttp;
use LynX39\LaraPdfMerger\Facades\PdfMerger;
use setasign\Fpdi\Tcpdf\Fpdi;
use Illuminate\Support\Str;

use App\ServiceOrderUutInsituLaporan;

class ServiceLuarUutController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    protected $MasterNegara;

    public function __construct()
    {
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("serviceluaruut");

        $laboratory_id = Auth::user()->laboratory_id;
        $instalasi_id = Auth::user()->instalasi_id;

        //$petugas_id = Auth::user();
        // dd($petugas_id);

        $petugas_id = Auth::user()->petugas_uut_id;
        if ($petugas_id == null) {
            $petugas_id = Auth::user()->petugas_uttp_id;
        }

        $rows = ServiceOrders::join('service_request_items', 'service_request_items.id', '=', 'service_orders.service_request_item_id')
            ->join('service_requests', 'service_requests.id', '=', 'service_orders.service_request_id')
            ->leftJoin('service_request_uut_insitu_staff', 'service_request_uut_insitu_staff.doc_id', '=', 'service_requests.spuh_doc_id')
            ->whereIn("stat_service_order",[0,1,2,3,4])
            //->where('service_request_items.status_id', 13)
            ->where('service_requests.lokasi_pengujian', 'luar')
            ->where('service_request_uut_insitu_staff.scheduled_id', $petugas_id)
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id",">=", 11);
            })
            ->whereHas('ServiceRequest', function($query) 
            {
                $query->where("lokasi_pengujian","=", 'luar');
            })

            ->where("is_finish",0)
            ->where(function($query) {
                $query->where('stat_sertifikat', 0)
                      ->orWhereNull('stat_sertifikat');
            })
            ->whereNull('subkoordinator_notes')
            ->where(function($query) {
                $query->whereNull('test_by_1')
                      ->orWhere('test_by_1', Auth::id())
                      ->orWhere('test_by_2', Auth::id());
            })
            ->orderBy('staff_entry_datein','desc')
            ->select('service_orders.*')
            ->get();

        return view('serviceluaruut.index',compact('rows','attribute'));
    }

    public function test($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuut.test', compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function savetest($id, Request $request)
    {
        $order = ServiceOrders::find($id);        
        $order->update([
            "stat_service_order" => 1,
            "staff_entry_dateout" => date("Y-m-d"),
            "stat_warehouse" => 0,
        ]);
        $item = ServiceRequestItem::find($order->service_request_item_id);
        $serviceRequest = ServiceRequest::find($order->service_request_id);

        if ($serviceRequest->service_type_id == 1 || $serviceRequest->service_type_id == 2) {
            $inspectionItems = DB::table('uut_inspection_items')
                //->where('uut_type_id', $item->uuts->type_id)
                //->where('template_id', $inspection->inspectionPrice->inspection_template_id)
                ->where('template_id', $item->uuts->stdtype->template_id)
                ->orderBy('order_no', 'asc')->get();
            foreach ($inspectionItems as $inspectionItem) {
                ServiceOrderInspections::insert([
                    'order_id' => $order->id,
                    'inspection_item_id' => $inspectionItem->id
                ]);
            }
        }

        /*
        $inspection->update([
            'status_id' => 13
        ]);

        $inspections_count = DB::table('service_request_uut_item_inspections')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_13 == $inspections_count[0]->count_all) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>13]);
        }
        */
        $item->status_id = 13;
        $item->save();

        ServiceRequestItemInspection::where("service_request_item_id", $item->id)->update([
            "status_id" => 13,
        ]);

        $items_count = DB::table('service_request_items')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_id', $order->service_request_id)->get();

        if ($items_count[0]->count_13 == $items_count[0]->count_all) {
            ServiceRequest::find($order->service_request_id)->update(['status_id' => 13]);
        }

        //return Redirect::route('serviceuut');
        return Redirect::route('serviceuut.result', $id);
    }

    public function savetestqr(Request $request)
    {
        $item = ServiceRequestItem::where('no_order', $request->no_order)->first();
        $order = ServiceOrders::where('service_request_item_id', $item->id)->first();

        $order->update([
            "stat_service_order" => 1,
            "staff_entry_dateout" => date("Y-m-d"),
            "stat_warehouse" => 0,
        ]);

        //$inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
        $item = ServiceRequestItem::find($order->service_request_item_id);
        $serviceRequest = ServiceRequest::find($order->service_request_id);

        /* if ($serviceRequest->service_type_id == 4 || $serviceRequest->service_type_id == 5) {
            foreach ($item->perlengkapans as $perlengkapan) {
                $ttuPerlengkapanModel = new ServiceOrderUutTTUPerlengkapan();
                $ttuPerlengkapan = [
                    'order_id' => $order->id,
                    'uut_id' => $perlengkapan->uut_id
                ];
                $ttuPerlengkapanModel->create($ttuPerlengkapan);
            }
        } else */
        if ($serviceRequest->service_type_id == 1 || $serviceRequest->service_type_id == 2) {
            $inspectionItems = DB::table('uut_inspection_items')
                //->where('uut_type_id', $item->uuts->type_id)
                //->where('template_id', $inspection->inspectionPrice->inspection_template_id)
                ->where('template_id', $item->uuts->stdtype->template_id)
                ->orderBy('order_no', 'asc')->get();
            foreach ($inspectionItems as $inspectionItem) {
                ServiceOrderInspections::insert([
                    'order_id' => $order->id,
                    'inspection_item_id' => $inspectionItem->id
                ]);
            }
        }

        /*
        $inspection->update([
            'status_id' => 13
        ]);

        $inspections_count = DB::table('service_request_uut_item_inspections')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_13 == $inspections_count[0]->count_all) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>13]);
        }
        */
        $item->status_id = 13;
        $item->save();

        ServiceRequestItemInspection::where("request_item_id", $item->id)->update([
            "status_id" => 13,
        ]);

        $items_count = DB::table('service_request_items')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_id', $order->service_request_id)->get();

        if ($items_count[0]->count_13 == $items_count[0]->count_all) {
            ServiceRequest::find($order->service_request_id)->update(['status_id' => 13]);
        }

        //return Redirect::route('serviceuut');
        return Redirect::route('serviceuut.result', $order->id);
    }

    public function result($id)
    {
        $is_at = false;
        $attribute = $this->MyProjects->setup("serviceuut");
        $serviceOrder = ServiceOrders::find($id);

        // $users = MasterUsers::where('user_role',4)->pluck('full_name', 'id');
        

        $user = MasterUsers::find(Auth::id());

        

        $oimls = DB::table("master_oimls")->pluck('oiml_name', 'id');
        $inspectionItems = ServiceOrderInspections::where('order_id', $id)
            ->orderBy('id', 'asc')->get();
        $sertifikat_expired_at = date(
            'Y-m-d',
            strtotime($serviceOrder->staff_entry_dateout . ' + ' .
                ($serviceOrder->ServiceRequestItem->uuts->stdtype->jangka_waktu == null ?  0 :
                    $serviceOrder->ServiceRequestItem->uuts->stdtype->jangka_waktu) .
                ' year')
        );

        $ttu = ServiceOrderUutTTUInspection::where('order_id', $id)->first();
        $ttuItems = ServiceOrderUutTTUInspectionItems::where('order_id', $id)->get();
        $badanHitungs = ServiceOrderUutTTUInspectionBadanHitung::where('order_id', $id)->get();
        $ttuPerlengkapans = ServiceOrderUutTTUPerlengkapan::where('order_id', $id)->get();
        $tipe = ServiceOrderUutEvaluasiTipe::where('order_id', $id)->first();


        $docs = ServiceRequestUutInsituStaff::where('doc_id', $serviceOrder->ServiceRequest->spuh_doc_id)->get();
        $docsIds = $docs->map(function ($item, $key) {
            return $item->scheduled_id;
        })->all();   
        $users = MasterUsers::whereNotNull('petugas_uut_id')
            ->whereHas('PetugasUut', function($query) use ($docsIds)
            {
                $query->where('flag_unit', 'snsu')
                    ->where('is_active', true)
                    ->whereIn('id', $docsIds);
            });
            //->where('id', '<>', $user->id);
            
        $user2 = $users->first();
        // dd([$user->id,$docsIds]);
        if ($user2 == null) {
            $users = MasterUsers::whereNotNull('petugas_uut_id')
            ->whereHas('PetugasUttp', function($query) use ($docsIds)
            {
                $query->where('flag_unit', 'uttp')
                    ->where('is_active', true)
                    ->whereIn('id', $docsIds);
            })
            ->where('id', '<>', $user->id);

            $user2 = $users->first();
        }

        $users = $users->pluck('full_name', 'id')->prepend('-- Pilih Pegawai --','');

        $laporan = ServiceOrderUutInsituLaporan::where('service_order_id', $id)->first();
        
        return view('serviceluaruut.result', compact(
            'serviceOrder',
            'attribute',
            'users',
            'user',
            'user2',
            'oimls',
            'ttu',
            'ttuItems',
            'ttuPerlengkapans',
            'badanHitungs',
            'inspectionItems',
            'sertifikat_expired_at',
            'tipe',
            'laporan'
        ));
    }

    public function resultupload($id, Request $request)
    {
        $order = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
            'ServiceRequestItem.uuts.stdtype.oiml',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        $rules=[];
        if($request->hasil_uji_memenuhi ==0 || $request->hasil_uji_memenuhi ==False)
        {
            $rules['cancel_notes'] = ['required'];
            if($order->path_skhp ==null){
                $rules['file_skhp'] = ['required'];
            }
        }else{
            if( $order->path_lampiran == null){
                $rules['file_lampiran'] = ['required'];
                // $skhp= is_file(storage_path("app/public/sertifikat/".$order->path_lampiran));
            }
            elseif($order->path_skhp ==null){
                $rules['file_skhp'] = ['required'];
            }
        }

        // $rules['file_skhp'] = ['required'];
        // $rules['test_by_2'] = ['required'];
        // $rules['file_lampiran'] = ['required'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        $serviceRequestUpdate = ServiceRequest::find($order->service_request_id);

        if ($validation->passes()) {
            // laporan
            $laporan = ServiceOrderUutInsituLaporan::where('service_order_id', $id)->first();
            
            if ($laporan == null) {
                ServiceOrderUutInsituLaporan::insert([
                    'service_order_id'          => $id,
                    'service_request_id'        => $order->ServiceRequest->id,
                    'service_request_item_id'   => $order->ServiceRequestItem->id,
                    'ringkasan'                 => $request->get('ringkasan'),
                    'kendala_teknis'            => $request->get('kendala_teknis'),
                    'kendala_non_teknis'        => $request->get('kendala_non_teknis'),
                    'metode_tindakan'           => $request->get('metode_tindakan'),
                    'saran_masukan'             => $request->get('saran_masukan'),
                ]);
            } else {
                $laporan->update([
                    'ringkasan'                 => $request->get('ringkasan'),
                    'kendala_teknis'            => $request->get('kendala_teknis'),
                    'kendala_non_teknis'        => $request->get('kendala_non_teknis'),
                    'metode_tindakan'           => $request->get('metode_tindakan'),
                    'saran_masukan'             => $request->get('saran_masukan'),
                ]);
            }


            if($order->path_lampiran ==null && $order->path_skhp ==null){
                $file = $request->file('file_skhp');
                $fileLampiran = $request->file('file_lampiran');

                if($fileLampiran)
                {
                    $data['file_lampiran'] = $fileLampiran->getClientOriginalName();   
                    $pathLampiran = $fileLampiran->store(
                        'sertifikat',
                        'public'
                    );
                    $data['path_lampiran'] = $pathLampiran;

                }elseif($file){
                    $data['file_skhp'] = $file->getClientOriginalName();
                    $path = $file->store(
                        'skhp',
                        'public'
                    );
    
                    $data['path_skhp'] = $path;
                }

            }else{
                if($request->file('file_skhp') != null){
                    $file = $request->file('file_skhp');
                    $data['file_skhp'] = $file->getClientOriginalName();
                    $path = $file->store(
                        'skhp',
                        'public'
                    );
                    $data['path_skhp'] = $path;
                }if($request->file('file_lampiran') != null){
                    $fileLampiran = $request->file('file_lampiran');
                    $data['file_lampiran'] = $fileLampiran->getClientOriginalName();
                    $pathLampiran = $fileLampiran->store(
                        'sertifikat',
                        'public'
                    );
                    $data['path_lampiran'] = $pathLampiran;
                }
            }

            $unit='';
            $unitmin ='';
            $dayabaca='';

            $kapasitas_req= trim(preg_replace('/\s+/',' ', $request->get("satuan")));
            $kapasitas_min_req= trim(preg_replace('/\s+/',' ', $request->get("tool_capacity_min_unit")));
            $dayabaca_req= trim(preg_replace('/\s+/',' ', $request->get("tool_dayabaca_unit")));

            if($kapasitas_min_req =="°C" || $kapasitas_min_req =="⁰C"){
                $unitmin='&deg;C';
            }elseif($kapasitas_min_req =="°F" || $kapasitas_min_req =="⁰F"){
                $unitmin='&deg;F';
            }elseif($kapasitas_min_req =="°K" || $kapasitas_min_req == "⁰K"){
                $unitmin='&deg;K';
            }
            // elseif($kapasitas_min_req =="Ω" || $kapasitas_min_req =="Ω"){
            //     $unitmin='&#8486;';
            // }
            else{
                $unitmin = $kapasitas_min_req;
            }

            if($kapasitas_req =="°C" || $kapasitas_req =="⁰C"){
                $unit='&deg;C';
            }elseif($kapasitas_req =="°F" || $kapasitas_req =="⁰F"){
                $unit='&deg;F';
            }elseif($kapasitas_req =="°K" || $kapasitas_req == "⁰K"){
                $unit='&deg;K';
            }
            // elseif($kapasitas_req =="Ω" || $kapasitas_req == "Ω"){
            //     $unit='&#8486;';
            // }
            else{
                $unit = $kapasitas_req;
            }
            if($dayabaca_req =="°C" || $dayabaca_req == "⁰C"){
                $dayabaca='&deg;C';
            }elseif($dayabaca_req =="°F" || $dayabaca_req =="⁰F"){
                $dayabaca='&deg;F';
            }elseif($dayabaca_req =="°K" || $dayabaca_req == "⁰K"){
                $dayabaca='&deg;K';
            }
            
            else{
                $dayabaca = $dayabaca_req;
            }

            $stat_s ='';
            if($request->get("stat_sertifikat") < 1){
                $stat_s = null;
            }else{
                $stat_s = $request->get("stat_sertifikat");
            }

            $data['test_by_1'] = Auth::id();
            $data['test_by_2'] = $request->get("test_by_2") > 0 ? $request->get("test_by_2") : null;

            $data['test_by_2_sertifikat'] = $request->has('test_by_2_sertifikat');

            $data['persyaratan_teknis_id'] = $request->get("persyaratan_teknis_id");
            $data['stat_service_order'] = 1;

            $data['stat_sertifikat'] = $stat_s;
            $data['hasil_uji_memenuhi'] = $request->get("hasil_uji_memenuhi");

            $data["tool_capacity"] = $request->get("kapasitas");
            $data["tool_capacity_unit"] = $unit; //$request->get("satuan");
            $data["tool_made_in"] = $request->get("buatan");

            $data["tool_capacity_min"] = $request->get("tool_capacity_min");
            $data["tool_capacity_min_unit"] = $unitmin; //$request->get("satuan");

            $data["tool_name"] = $request->get("tool_name");
            $data["tool_dayabaca"] = $request->get("tool_dayabaca");
            $data["tool_dayabaca_unit"] = $dayabaca;//$request->get("tool_dayabaca_unit");
            $data["tool_brand"] = $request->get("tool_brand");
            $data["tool_model"] = $request->get("tool_model");
            $data["tool_serial_no"] = $request->get("tool_serial_no");
            $data["tool_class"] = $request->get("tool_class");
            $data["tool_jumlah"] = $request->get("tool_jumlah");
            $data["is_certified"] = $request->get("is_certified");
            $data["page_num"] = $request->get("page_num");
            $data["cancel_notes"] = $request->get("cancel_notes");
            
            $dataRequest["addr_sertifikat"] = $request->get("addr_sertifikat");
            $dataRequest["label_sertifikat"] = $request->get("label_sertifikat");

            $data['test_by_2_sertifikat'] = $request->has('test_by_2_sertifikat');

            $serviceRequestUpdate->update($dataRequest);

            ServiceRequestItem::where('id', $order->service_request_item_id)->update([
                'location_lat' => $request->get('location_lat'),
                'location_long'=> $request->get('location_long')
            ]);

            if($request->get("sertifikat_expired_at") !='' || $request->get("sertifikat_expired_at") !=null){
                $data['sertifikat_expired_at'] = date('Y-m-d', strtotime($request->get("sertifikat_expired_at")));
            }

            $order->update($data);
            /*
            $inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
            $inspection->update([
                'status_sertifikat' => 1
            ]);
            */

            //$this->checkAndUpdateFinishOrder($id);

            return Redirect::route('serviceluaruut');
        } else {
            return Redirect::route('serviceluaruut.result', $id);
        }
    }

    public function approve($id)
    {
        $attribute = $this->MyProjects->setup("serviceuut");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis',
            'inspections' => function ($query) {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        return view('serviceuut.approve', compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function approvesubmit($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest',
        ])->find($id);
        $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
        $lastNo = $serviceType->last_no;
        $nextNo = intval($lastNo) + 1;
        $noSertifikat = sprintf("%d", $nextNo) . '/25/'.$order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'IN': 'EX' . date("m/Y");        
        $noService = $serviceType->prefix . '-' . sprintf("%05d", $nextNo);

        ServiceOrders::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            "no_service" => $noService,
            "supervisor_staff" => Auth::id(),
            "supervisor_entry_date" => date("Y-m-d"),
            "stat_service_order" => 3
        ]);

        MasterServiceType::where("id", $id)->update(["last_no" => $nextNo]);

        $this->checkAndUpdateFinishOrder($id);

        return Redirect::route('serviceuut');
    }

    public function warehouse($id)
    {
        $order = ServiceOrders::find($id);
        ServiceOrders::whereId($id)->update(["stat_warehouse" => 1]);

        // $inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
        // $inspection->update([
        //     'status_uut' => 1
        // ]);

        $this->checkAndUpdateFinishOrder($id);

        return Redirect::route('serviceuut');
    }

    public function finishorder($service_order_id)
    {
        DB::table("service_orders")
            ->where("id", $service_order_id)
            ->update(["is_finish" => 1]);
        return Redirect::route('serviceuut');
    }

    public function download($id)
    {
        $order = ServiceOrders::find($id);
        if($order->path_skhp !=null && $order->file_skhp !=null){
            $nolam = str_replace('/','_',$order->no_sertifikat);
            return Storage::disk('public')->download($order->path_skhp, 'Cerapan--'.$nolam.'.pdf');//$order->file_skhp);
        }else{
            abort(404);
        }
    }

    public function downloadLampiran($id)
    {
        $order = ServiceOrders::find($id);

        if($order->path_lampiran != null && $order->file_lampiran !=null){
            $nolam = str_replace('/','_',$order->no_sertifikat);
            return Storage::disk('public')->download($order->path_lampiran, 'Lampiran-'.$nolam.'.pdf');//$order->file_lampiran);
        }else{
            abort(404);
        }
    }

    public function preview($id)
    {
        $pdfMerger = PdfMerger::init();
        $order = ServiceOrders::find($id);
        
         $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = route('documentuut.valid', ['jenis_sertifikat' => 'skhp','token' => $order->qrcode_token]);

        $blade ='serviceuut.skhp_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([

            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo'),
            'defaultPaperSize' => 'a4',
            'defaultMediaType' => 'print',
        ])
            ->loadview($blade, compact('order', 'qrcode_generator', 'view'));

        return $pdf->stream();

        /*$order = ServiceOrders::find($id);
        
        $order = ServiceOrders::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);
        */
        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        /* $qrcode_generator = route('documentuut.valid', $order->qrcode_token);
        
        $blade  = 'serviceuut.skhp_tipe_pdf';*/
        /* if (
            $order->ServiceRequest->service_type_id == 4 ||
            $order->ServiceRequest->service_type_id == 5
        ) {
            $blade = 'serviceuut.skhp_pdf_ttu';
        } */

       /*  $view = true;

        return view($blade, compact(
            'order',
            'qrcode_generator',
            'view'
        )); */
    }

    public function print($id,$stream=0)
    {
        $pdfMerger = PdfMerger::init();
        $order = ServiceOrders::find($id);

        /*
        $order = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis',
            'inspections' => function ($query) {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);*/
         $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuut.valid', ['jenis_sertifikat' => 'skhp','token' => $order->qrcode_token]);
        
        // $blade = 'serviceuut.skhp_pdf';
        $blade ='serviceuut.skhp_tipe_pdf';
        /* if (
            $order->ServiceRequest->service_type_id == 1 ||
            $order->ServiceRequest->service_type_id == 2
        ) {
            $blade = 'serviceuut.skhp_pdf_ttu';
        }

        $view = false;

        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true,
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo')
        ])
            ->loadview($blade, compact('order', 'qrcode_generator', 'view'));

        return $pdf->download('skhp.pdf'); */

        $view = false;

        $pdf = PDF::setOptions([
            /* 'preferCSSPageSize' => true,
            'marginTop' => 0.4,
            'marginBottom' => 0.2,
            'marginLeft' => 0.2,
            'marginRight' => 0.2,
            'isHtml5ParserEnabled' => true,
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo') */

            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo'),
            'defaultPaperSize' => 'a4',
            'defaultMediaType' => 'print',
        ])
            ->loadview($blade, compact('order', 'qrcode_generator', 'view'));
        
            $file = public_path('storage/images/file.pdf');

            if(! file_exists($file)){
                Storage::delete('public/tmp/file.pdf');
            }

        Storage::put('public/tmp/file.pdf', $pdf->output());
        $pdfMerger->addPDF(storage_path('app/public/tmp/file.pdf'),'all');
        if($order->path_lampiran !=null){
            /* $pdfMerger->addPDF(storage_path('app/public/'.$order->path_lampiran),'all');
            $pdfMerger->merge();
            Storage::delete('public/tmp/file.pdf');

            return $pdfMerger->save("SERTIFIKAT.pdf", "download"); */
            $filetomerge = storage_path('app/public/'.$order->path_lampiran);
            if($filetomerge){
                $pdfMerger->addPDF($filetomerge,'all');
                $pdfMerger->merge();
            }
            $pdfMerger->save(storage_path('app/public/sample.pdf'));
            $this->doWatermark('app/public/sample.pdf',$order->qrcode_token,$order->no_sertifikat,$order->id);
            //Deleting tmp file
            Storage::delete('public/tmp/file.pdf');
            Storage::delete('public/tmp/qrcode.png');
            Storage::delete('app/public/sample.pdf');
            //
            $no = str_replace('/','_',$order->no_sertifikat);
            $no ='Lampiran Sertifikat No.'.$no;
            $file = storage_path('app/public/tmp/SERTIFIKAT-'.$no.'.pdf','P');

            if(is_file($file))
            {
                return response()->download($file);
                // 1. possibility
                Storage::delete($file);
                // 2. possibility
                unlink(storage_path($file));
            }
        }
        return $pdf->stream();
    }

    public function previewTipe($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis',
            'inspections' => function ($query) {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $otherOrders = DB::table('uut_inspection_prices')
            ->join('uut_inspection_price_types', 'uut_inspection_price_types.inspection_price_id', '=', 'uut_inspection_prices.id')
            ->leftJoin('service_request_item_inspections', 'service_request_item_inspections.inspection_price_id', '=', 'uut_inspection_prices.id')
            ->join('service_orders', 'service_orders.service_request_item_inspection_id', '=', 'service_request_item_inspections.id')
            ->where('uut_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
            ->where('uut_inspection_price_types.uut_type_id', $order->ServiceRequestItem->uuts->type_id)
            ->where('service_orders.ujitipe_completed', true)
            ->selectRaw('service_orders.*')
            ->get();

        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuut.valid', $order->qrcode_token);

        $blade = 'serviceuut.skhp_pdf_tipe';

        $view = true;
        //dd($order);
        return view($blade, compact(
            'order',
            'otherOrders',
            'qrcode_generator',
            'view'
        ));
    }

    public function printTipe($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis',
            'inspections' => function ($query) {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $fie_name = 'skhp' . $order->ServiceRequest->no_order;
        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuut.valid', $order->qrcode_token);

        $blade = 'serviceuut.skhp_pdf_tipe';

        $view = false;

        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true,
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo')
        ])
            ->loadview($blade, compact('order', 'qrcode_generator', 'view'));

        return $pdf->download('skhp.pdf');
    }

    private function checkAndUpdateFinishOrder($id)
    {
        $order = ServiceOrders::find($id);

        /*
        $inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
        if ($inspection->status_sertifikat == 1 && $inspection->status_uttp == 1) {
            $inspection->update(['status_id' => 14]); 
        }
        
        $inspections_count = DB::table('service_request_item_inspections')
            ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
       */ /*
        if ($inspections_count[0]->count_14 == $inspections_count[0]->count_all) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>14]);
        }
        
        ServiceRequestItem::find($order->service_request_item_id)->update(['status_id' => 14]);
        ServiceRequestItemInspection::where("request_item_id", $order->service_request_item_id)->update([
            "status_id" => 14,
        ]);
        */

        if ($order->stat_warehouse == 1 && $order->stat_sertifikat == 3) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestItemInspection::where("service_request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequest::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUttp();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                $history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }

    public function pending($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuut.pending', compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmpending($id, Request $request)
    {
        $order = ServiceOrders::find($id);

        $order->update([
            "pending_status" => 1,
            "pending_created" => date('Y-m-d H:i:s'),
            "pending_notes" => $request->get("pending_notes"),
            //"pending_estimated" => date("Y-m-d", strtotime($request->get("pending_estimated"))),
        ]);

        ServiceRequest::find($order->service_request_id)->update(['status_id' => 15]);

        $customerEmail = $order->ServiceRequest->requestor->email;
        //Mail::to($customerEmail)->send(new Pending($order));
        // ProcessEmailJob::dispatch($customerEmail, new Pending($svcRequest))->onQueue('emails');

        return Redirect::route('serviceuut');
    }

    public function continue($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuut.continue', compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcontinue($id, Request $request)
    {
        $order = ServiceOrders::find($id);

        $order->update([
            "pending_status" => 0,
            "pending_ended" => date('Y-m-d H:i:s'),
        ]);

        ServiceRequest::find($order->service_request_id)->update(['status_id' => 12]);

        return Redirect::route('serviceuut');
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uut', 'ServiceRequestItem.uut.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuut.cancel', compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcancel($id, Request $request)
    {
        $order = ServiceOrders::find($id);

        $order->update([
            "cancel_at" => date('Y-m-d H:i:s'),
            "cancel_notes" => $request->get("cancel_notes"),
            "cancel_id" => Auth::id(),
            "stat_service_order" => 4,
            "stat_sertifikat" => 0,
            //"status_id" => 16,
        ]);

        $inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
        $inspection->update(['status_id' => 16]);

        return Redirect::route('serviceuut');
    }

    public function doWatermark($file,$qrcode_token,$no_sertifikat,$id)
    {//$file, $text_image){
        $fileloc='';
        $order = ServiceOrders::find($id);
        if($order->no_sertifikat !=null){
            $no_sertifikat = "Lampiran-No.".$order->no_sertifikat;
        }
        if($order->no_sertifikat ==null){
            $no_sertifikat = "UNDEF";
        }
        // $no_sertifikat = "Lampiran Sertifikat No.".$no_sertifikat;

        $pdf = new \setasign\Fpdi\Tcpdf\Fpdi();
        
        $qrcode_generator = route('documentuut.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);

        // make folder
        $directoryPath = storage_path('app/public/tmp/'.$id.'/');
        if (!file_exists($directoryPath)) {
            mkdir($directoryPath, 0777, true);
        }

        $qrCodePath = $directoryPath . $id . '-qrcode.png';
        $qrcodeContent = \SimpleSoftwareIO\QrCode\Facades\QrCode::format("png")
            ->size(100)
            ->margin(0.5)
            ->errorCorrection("H")
            ->generate($qrcode_generator, $qrCodePath);

        // Encode the QR code as Base64
        $qrcode = base64_encode($qrcodeContent);
        
        $pathtoQR = $qrCodePath;
        // Set source PDF file     
        if($order->cancel_at !=null && $order->cancel_notes != null){
            $fileloc = storage_path('app/public/sample_cancel.pdf');
        }else{
            $fileloc = storage_path('app/public/sample.pdf');
        }     
        // $fileloc = storage_path('app/public/sample.pdf');
        
        if(file_exists($fileloc)){ 
            $pagecount = $pdf->setSourceFile($fileloc); 
        }else{ 
            die('File PDF Tidak ditemukan.!'); 
        } 
        
        // Add watermark image to PDF pages 
        for($i=1;$i<=$pagecount;$i++){ 
            
           /*  $tpl = $pdf->importPage($i); 
            $pdf->addPage();

            $size = $pdf->getTemplateSize($tpl); 
            $pdf->useTemplate($tpl, 1, 1, $size['width'], $size['height'], TRUE); 
            
            //Put the watermark 
            $xxx_final = ($size['width']-60); 
            $yyy_final = ($size['height']-25);  */

            $tplidx = $pdf->importPage($i);
            $specs = $pdf->getTemplateSize($tplidx);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->addPage($specs['height'] > $specs['width'] ? 'P' : 'L');
            $pdf->useTemplate($tplidx, null, null, $specs['width'], $specs['height'], true);

            $pdf->SetTextColor(0, 0, 0);
            $_x_text = ($specs['width']- 60) - ($pdf->GetStringWidth($order->no_sertifikat, '', '',10)/2.8);
            $_y_text = $specs['height']- $specs['height'] +5+5;

            //setting for name of kabalai

            $_x_Hkab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama :'Kabalai', '', '',10)/2.8);
            $_y_Hkab = $specs['height']-70;

            // line2
            $_x_HL2kab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama :'Kabalai', '', '',10)/2.8);
            $_y_HL2kab = $specs['height']-65;

            // line3
            $_x_HL3kab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama :'Kabalai', '', '',10)/2.8);
            $_y_HL3kab = $specs['height']-60;

            //Writing nip
            $_x_Nkab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama : 'Kabalai', '', '',10)/2.8);
            $_y_Nkab = $specs['height']-25;

            //setting for name of kabalai
            $_x_kab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama : 'Kabalai', '', '',10)/2.8);
            $_y_kab = $specs['height']-25;

            $_x = ($specs['width']-50) - ($pdf->GetStringWidth($order->no_sertifikat, '', '', 10)/2.8);
            $_y = $specs['height'] -15;
            
            if($i >=2){
                $pdf->SetFont('', 'italic', 10);
                $pdf->SetXY($_x_text, $_y_text);
                $pdf->setAlpha(1);
                $pdf->Write(0,$order->no_sertifikat);
            }

            // $pdf->Image($qrcode, $xxx_final, $yyy_final, 0, 0, 'png'); 
            if($i == $pagecount){
                if($order->cancel_at ==null|| $order->cancel_notes ==null){
                    //Setting header and name kabalai
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Hkab, $_y_Hkab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Ketua Tim Pelayanan Teknis");

                    //Line 2
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_HL2kab, $_y_HL2kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Balai Pengelolaan");

                    //Line 3-1
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_HL3kab, $_y_HL3kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Standar Ukuran Metrologi Legal");

                    //writing NIP
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Nkab, $_y_Nkab);
                    $pdf->setAlpha(1);
                    // $pdf->Write(0,"NIP : ".$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nip :'');

                    //Setting header and name kabalai
                    // $pdf->SetFont('', 'U', 10);
                    $pdf->SetFont('', 10);
                    $pdf->SetXY($_x_kab, $_y_kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nama :'SUBKOORDINATOR');

                    // $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $_x-17,$_y-38, 25, 27, 'png');
                    $pdf->Image($pathtoQR, $_x-17,$_y-38, 25, 27, 'png');
                    // $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $xxx_final,$yyy_final-45, 33, 33, 'png'); 
                    // Storage::delete('app/public/tmp/'.$id.'/qrcode.png');
                }else{
                    //Setting header and name kabalai
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Hkab, $_y_Hkab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Ketua Tim Pelayanan Teknis");

                    //Line 2
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_HL2kab, $_y_HL2kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Balai SNSU");

                    //Line 3-1
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_HL3kab, $_y_HL3kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Standar Ukuran Metrologi Legal");

                    //writing NIP
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Nkab, $_y_Nkab);
                    $pdf->setAlpha(1);
                    // $pdf->Write(0,"NIP : ".$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nip :'');

                    //Setting header and name kabalai
                    $pdf->SetFont('', 'U', 10);
                    $pdf->SetXY($_x_kab, $_y_kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nama :'SUBKOORDINATOR');

                    $pdf->Image($pathtoQR, $_x-17,$_y-36, 25, 27, 'png'); 
                    // $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $_x-17,$_y-36, 25, 27, 'png'); 
                    // $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $xxx_final,$yyy_final-45, 33, 33, 'png'); 
                    // Storage::delete('app/public/tmp/'.$id.'/qrcode.png');
                }
            }
        } 
        
        // Output PDF with watermark 
        $no = str_replace('/','_',$no_sertifikat);
        if($order->cancel_at != null && $order->cancel_notes !=null){
            $down = $pdf->Output(storage_path('app/public/tmp/SBL-'.$no.'.pdf'),'F');
        }else{
            $down = $pdf->Output(storage_path('app/public/tmp/SERTIFIKAT-'.$no.'.pdf'),'F');
        }
        
        if($down){
            // Storage::delete('app/public/tmp/SERTIFIKAT-'.$no.'.pdf');
        }
    }

    /* private function doWatermark($file,$qrcode_token) 
    {

        $qrcode_generator = route('documentuut.valid', $qrcode_token);
        $qrcode =  base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format("png")
                                ->size(123)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator,storage_path('app/public/tmp/qrcode.png')));

        $watermark = new PDFWatermark(storage_path('app/public/tmp/qrcode.png')); 
        
        //Place watermark behind original PDF content. Default behavior places it over the content.
        $watermark->setAsBackground();
        //Set the position
        $watermark->setPosition('bottomright');
        $oriPath =storage_path($file);

        $tarPath = storage_path('app\public\CERTIFICATE'.time().'.pdf');
        $watermarker = new PDFWatermarker($oriPath,$tarPath,$watermark); 
        // dd($watermarker);
        //Set page range. Use 1-based index.
        $watermarker->setPageRange(1);
        //Save the new PDF to its specified location
        $watermarker->savePdf(); 
        

        return $file;
    } */

    // function custom_put_contents($source_url='',$local_path=''){
    public function custom_put_contents(){

        $time_limit = ini_get('max_execution_time');
        $memory_limit = ini_get('memory_limit');
    
        set_time_limit(0);
        ini_set('memory_limit', '-1');      
    
        // $remote_contents=file_get_contents($source_url);
        // $response=file_put_contents($local_path, $remote_contents);
    
        set_time_limit($time_limit);
        ini_set('memory_limit', $memory_limit); 
    
        return null;
    }
}
