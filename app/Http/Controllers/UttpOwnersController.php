<?php

namespace App\Http\Controllers;

use App\MasterUsers;
use App\MasterRoles;
use App\MasterLaboratory;
use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\MasterKabupatenKota;
use App\UttpOwners;

class UttpOwnersController extends Controller
{
    private $MyProjects;
    private $UttpOwners;
    private $MasterKabupatenKota;
    public function __construct()
    {
        $this->UttpOwners = new UttpOwners();
        $this->MyProjects = new MyProjects();
        $this->MasterKabupatenKota = new MasterKabupatenKota();
    }
    public function index(){
        // \DB::enableQueryLog();
        $data = UttpOwners :: join('master_kabupatenkota','master_kabupatenkota.id','=','kota_id')
        ->get(['uttp_owners.*','master_kabupatenkota.nama as namakota']);
        $attribute = $this->MyProjects->setup("uttpowners");
        return View('uttpowners.index',compact('data','attribute'));
    }

    public function create($id = null){
        $attribute = $this->MyProjects->setup("insitems");

        $row = null;

        if($id)
        {
            $row = $this->MasterInstalasi->whereId($id)->first();
        }

        $kabupatenkota = $this->MasterKabupatenKota->pluck('nama', 'id');

        return View('uttpowners.create',compact('row','id','attribute','kabupatenkota'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["nama"] = ['required'];
        $rules["kota_id"] = ['required'];
        $rules["npwp"] = ['required', 'integer', 'min:0'];
        $rules["nib"] = ['required', 'integer', 'min:0'];
        $rules["email"] = ['required'];
        $rules["penanggung_jawab"] = ['required'];
        $rules["telepon"] = ['required'];
        #set time when omserted data
        $request["created_at"] = date('Y-m-d H:i:s');
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->UttpOwners->whereId($id)->update($request->all());
            }
            else
            {
                $this->UttpOwners->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return redirect()->route('uttpowners')->with($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->UttpInspectionItem->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
    public function edit($id)
    {
        $kabupatenkota = $this->MasterKabupatenKota->pluck('nama', 'id');
        $attribute = $this->MyProjects->setup("uttpinspectionprice");
        $row  = $this->UttpOwners->find($id);
        return view('uttpowners.edit', compact(
            'row','attribute',
            'kabupatenkota'
        ));
    }   
    public function update(Request $request)
    {
        $response["status"] = false;

        $rules["nama"] = ['required'];
        $rules["kota_id"] = ['required'];
        $rules["npwp"] = ['required', 'integer', 'min:0'];
        $rules["nib"] = ['required', 'integer', 'min:0'];
        $rules["email"] = ['required'];
        $rules["penanggung_jawab"] = ['required'];
        $rules["telepon"] = ['required'];
        #set time when omserted data
        $request["updated_at"] = date('Y-m-d H:i:s');

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $id = $request->id;

            $request->request->remove('id');

            $this->UttpOwners->whereId($id)->update([
                "nama" => $request['nama'],
                "kota_id" => $request['kota_id'],
                "npwp" => $request['npwp'],
                "nib" => $request['nib'],
                "email" => $request['email'],
                "penanggung_jawab" => $request['penanggung_jawab'],
                "updated_at" => $request['updated_at'],
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return redirect()->route('uttpowners')->with($response);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->UttpOwners::whereId($id)
        ->first();

        if($row)
        {
            $this->UttpOwners::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('uttpowners')->with($response);
    }

}