<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Mail\CertificateDone;
use App\Mail\Cancel;
use App\Mail\ServiceDone;
use Illuminate\Support\Facades\Mail;

use PDF;

use App\HistoryUttp;
use App\Uttp;
use App\UttpOwner;

use App\Jobs\ProcessEmailJob;
use App\Delegation;

use App\ServiceOrderUttpInsituLaporan;

class ApproveUttpController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("approveuttp");

        //$laboratory_id = Auth::user()->laboratory_id;

        /*
        $approval_before = Auth::user()->user_role == 3 ? 0 : 
            (Auth::user()->user_role == 9 ? 1 : 2);
        */
        $role = Auth::user()->user_role;
        $approval_before_role = $role == 3 || $role == 9 ? 1 : 2;

        $delegasis = Delegation::where('user_delegatee_id', Auth::id())
                ->where('berlaku_mulai', '<=', date('Y-m-d'))
                ->where('berlaku_sampai', '>=', date('Y-m-d'))
                ->get();
        $approval_before_delegasi = null;
        $delegator = null;
        $role_delegasi = null;
        if (count($delegasis) > 0) {
            $delegator = MasterUsers::find($delegasis[0]->user_delegator_id);
            $role_delegasi = $delegator->user_role;
            $delegator = $delegasis[0]->delegator->full_name;

            $approval_before_delegasi = $role_delegasi == 3 || $role_delegasi == 9 ? 1 : 2;
        }
        
                
        if ($approval_before_role != null) {
            $rows = ServiceOrderUttps::with([
                    'ServiceRequest', 'ServiceRequestItem', 'ServiceRequestItem.uttp',
                    'MasterUsers', 'LabStaffOut',
                ])
                ->select("service_order_uttps.*")
                ->join('service_request_uttps', 'service_request_uttps.id', '=', 'service_order_uttps.service_request_id')
                ->whereIn("stat_service_order",[0,1,2,3,4])
                ->where("stat_sertifikat", $approval_before_role)
                ->where("is_finish",0);
        }
            

        if ($approval_before_delegasi != null) {
            $rows2 = ServiceOrderUttps::with([
                    'ServiceRequest', 'ServiceRequestItem', 'ServiceRequestItem.uttp',
                    'MasterUsers', 'LabStaffOut',
                ])
                ->select("service_order_uttps.*")
                ->join('service_request_uttps', 'service_request_uttps.id', '=', 'service_order_uttps.service_request_id')
                ->whereIn("stat_service_order",[0,1,2,3,4])
                ->where("stat_sertifikat", $approval_before_delegasi)
                ->where("is_finish",0);

            if ($approval_before_role != null) {
                $rows = $rows->union($rows2);
            } else {
                $rows = $rows2;
            }
        }
        
        $rows = $rows->orderBy('staff_entry_datein','desc');

        /*
        if (Auth::user()->user_role == 3) {
            $rows = $rows->where('laboratory_id', $laboratory_id)
                ->whereIn("service_request_uttps.service_type_id", [6,7]);
        }
        if (Auth::user()->user_role == 9) {
            $rows = $rows->whereIn("service_request_uttps.service_type_id", [4,5]);
        }
        */

        $rows = $rows->get();

        $rows_kn = $rows->filter(function ($value, $key) {
            return $value->ServiceRequest->lokasi_pengujian == 'dalam';
        });
        $rows_dl = $rows->filter(function ($value, $key) {
            return $value->ServiceRequest->lokasi_pengujian == 'luar';
        });

   
        //dd($laboratory_id);
        //dd([Auth::user()->user_role, $rows, $approval_before_role, $approval_before_delegasi]);

        return view('approveuttp.index',compact('rows_kn','rows_dl', 'attribute','approval_before_delegasi','delegator'));
    }

    public function approvekalab($id)
    {
        $attribute = $this->MyProjects->setup("approveuttp");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        return view('approveuttp.approve',compact(
            'serviceOrder', 'attribute', 'delegator',
        ));
    }

    public function approvesubmitkalab($id, Request $request)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
        ])->find($id);
       
        ServiceOrderUttps::whereId($id)->update([
            "kalab_id" => Auth::id(),
            "kalab_date" => date("Y-m-d"),
            "stat_sertifikat"=>$request->get("is_approved") === "ya" ? 2 : null,
            "kalab_notes"=>$request->get("notes")
        ]);
        
        return Redirect::route('approveuttp');
    }

    private function updateapprovekalab($id)
    {
        // approval 1 = kalab

        ServiceOrderUttps::whereId($id)->update([
            "kalab_id" => Auth::id(),
            "kalab_date" => date("Y-m-d"),
            "stat_sertifikat"=> 2,
        ]);
        
        return Redirect::route('approveuttp');
    }

    public function approvesubko($id)
    {
        $attribute = $this->MyProjects->setup("approveuttp");

        $delegator = null;
        $approval_before_delegasi = null;

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $laporan = ServiceOrderUttpInsituLaporan::where('service_order_id', $id)->first();

        return view('approveuttp.approve',compact(
            'serviceOrder', 'attribute', 'delegator', 'approval_before_delegasi', 'laporan',
        ));
    }

    public function approvesubmitsubko($id, Request $request)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 'ServiceRequestItem'
        ])->find($id);

        $file_review = null;
        $path_review = null;
        if ($request->get("is_approved") === "ya" ) {}
        else {
            if ($request->hasFile('file_review')) {
                $file = $request->file('file_review');
                $file_review = $file->getClientOriginalName();
                $path = $file->store(
                    'review_skhp',
                    'public'
                );
                $path_review = $path;
            }
        }
       
        ServiceOrderUttps::whereId($id)->update([
            "subkoordinator_id" => Auth::id(),
            "subkoordinator_date" => date("Y-m-d"),
            "stat_sertifikat"=>$request->get("is_approved") === "ya" ? 2 : 0,
            "file_review_subkoordinator"=>$request->get("is_approved") === "ya" ? null : $file_review,
            "path_review_subkoordinator"=>$request->get("is_approved") === "ya" ? null : $path_review,
            "subkoordinator_notes"=>$request->get("notes")
        ]);

        $history = new HistoryUttp();
        $history->request_status_id = $order->ServiceRequestItem->status_id;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = $request->get("is_approved") === "ya" ? 2 : 0;
        $history->warehouse_status_id = $order->stat_warehouse;
        $history->user_id = Auth::id();
        $history->save();
        
        return Redirect::route('approveuttp');
    }

    private function updateapprovesubko($id) {
        // approval 1 = subko
        ServiceOrderUttps::whereId($id)->update([
            "subkoordinator_id" => Auth::id(),
            "subkoordinator_date" => date("Y-m-d"),
            "stat_sertifikat"=> 2,
        ]);

        $order = ServiceOrderUttps::find($id);

        $history = new HistoryUttp();
        $history->request_status_id = 13;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = 2;
        $history->warehouse_status_id = $order->stat_warehouse;
        $history->user_id = Auth::id();
        $history->save();
    }

    public function approve($id)
    {
        $attribute = $this->MyProjects->setup("approveuttp");

        $delegasis = Delegation::where('user_delegatee_id', Auth::id())
                ->where('berlaku_mulai', '<=', date('Y-m-d'))
                ->where('berlaku_sampai', '>=', date('Y-m-d'))
                ->get();
        $approval_before_delegasi = null;
        $delegator = null;
        $role_delegasi = null;
        if (count($delegasis) > 0) {
            $delegator = MasterUsers::find($delegasis[0]->user_delegator_id);
            $role_delegasi = $delegator->user_role;
            $delegator = $delegasis[0]->delegator->full_name;
        }
        $approval_before_delegasi = $role_delegasi == 3 || $role_delegasi == 9 ? 1 : 2;

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $laporan = ServiceOrderUttpInsituLaporan::where('service_order_id', $id)->first();

        return view('approveuttp.approve',compact(
            'serviceOrder', 'attribute', 'delegator', 'approval_before_delegasi', 'laporan',
        ));
    }

    public function approvesubmit($id, Request $request)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 'ServiceRequest.Owner'
        ])->find($id);

        $delegasis = Delegation::where('user_delegatee_id', Auth::id())
                ->where('berlaku_mulai', '<=', date('Y-m-d'))
                ->where('berlaku_sampai', '>=', date('Y-m-d'))
                ->get();
        $approval_before_delegasi = null;
        $delegator = null;
        $role_delegasi = null;
        if (count($delegasis) > 0) {
            $delegator = MasterUsers::find($delegasis[0]->user_delegator_id);
            $role_delegasi = $delegator->user_role;
            $delegator = $delegasis[0];
        }
        $approval_before_delegasi = $role_delegasi == 3 || $role_delegasi == 9 ? 1 : 2;

        $noSertifikat = null;
        $noService = null;
        $file_review = null;
        $path_review = null;
        $noSertifikatTipe = null;
        if ($request->get("is_approved") === "ya" ) {
            $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
            //$lastNo = $serviceType->last_no;
            //$nextNo = intval($lastNo)+1;
            $prefix = $serviceType->prefix;
            $kndl = $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'KN' : 'DL';


            $lastOrder = ServiceOrderUttps::where("id", "<>", $id)
                ->whereNotNull("no_sertifikat_int")
                ->where('no_sertifikat_year', date("Y"))
                ->orderBy("no_sertifikat_int", "desc")
                ->select("no_sertifikat_int")
                ->first();

            if ($lastOrder != null) 
            {
                $lastNo = $lastOrder->no_sertifikat_int;
                $nextNo = intval($lastNo)+1;
            } else {
                $lastNo = 0;
                $nextNo = 1;
            }
            
            if ($order->cancel_at != null){
                $kndl = 'KET';
            }
            //$kndl = 'KN';
            $noSertifikat = $prefix.'.'.sprintf("%04d", $nextNo).'/PKTN.4.5/'.$kndl.'/'.date("m/Y");
            $noService = $serviceType->prefix.'-'.date("y").'-'.sprintf("%04d", $nextNo);
            
            //if ($order->ServiceRequest->service_type_id == 6 || $order->ServiceRequest->service_type_id == 7) {
            //    $noSertifikatTipe = $prefix.'T.'.sprintf("%04d", $nextNo).'/PKTN.4.8/'.$kndl.'/'.date("m/Y");
            //}
            $noSertifikatTipe = $order->no_sertifikat_tipe;
        } else {
            if ($request->hasFile('file_review')) {
                $file = $request->file('file_review');
                $file_review = $file->getClientOriginalName();
                $path = $file->store(
                    'review_skhp',
                    'public'
                );
                $path_review = $path;
            }
        }

        $token = $this->hashing($noSertifikat, $noService);
        $skhpttoken = $this->hashing($noSertifikatTipe, $noService);

        $sertifikat_expired_at = date('Y-m-d', strtotime($order->sertifikat_expired_at));
        if ($order->ServiceRequest->service_type_id == 4 || $order->ServiceRequest->service_type_id == 5) {
            
            if ($sertifikat_expired_at == null || $sertifikat_expired_at == '1970-01-01') {
                $sertifikat_expired_at = date('Y-m-d', 
                    strtotime( $order->staff_entry_dateout . ' + '. 
                    ($order->ServiceRequestItem->uttp->type->masa_berlaku_ttu == null ?  0 : 
                    $order->ServiceRequestItem->uttp->type->masa_berlaku_ttu) .
                    ' year'));
            }
        }

        if ($request->get("is_approved") === "ya" ) {
            ServiceOrderUttps::whereId($id)->update([
                "no_sertifikat" => $noSertifikat,
                //"no_sertifikat_tipe" => $noSertifikatTipe,
                "no_service" => $noService,
                "kabalai_id" => ($delegator != null 
                    && $approval_before_delegasi != null 
                    && $approval_before_delegasi == $order->stat_sertifikat)
                     ? $delegator->user_delegator_id : Auth::id(),
                "an_kabalai_id" => ($delegator != null 
                    && $approval_before_delegasi != null 
                    && $approval_before_delegasi == $order->stat_sertifikat) 
                    ? Auth::id() : null,
                "kabalai_date" => date("Y-m-d"),
                "stat_sertifikat"=>$request->get("is_approved") === "ya" ? 3 : 1,
                "kabalai_notes"=>$request->get("notes"),
                "file_review_kabalai"=>$request->get("is_approved") === "ya" ? null : $file_review,
                "path_review_kabalai"=>$request->get("is_approved") === "ya" ? null : $path_review,
                "stat_service_order"=>$request->get("is_approved") === "ya" ? 3 : 1,
                "qrcode_token"=>$token,
                "qrcode_skhpt_token" => $skhpttoken,
                "sertifikat_expired_at" => $sertifikat_expired_at,

                "no_sertifikat_int" => $nextNo,
                "no_sertifikat_year" => date("Y"),
            ]);
        } else {
            ServiceOrderUttps::whereId($id)->update([
                "no_sertifikat" => $noSertifikat,
                //"no_sertifikat_tipe" => $noSertifikatTipe,
                "no_service" => $noService,
                "kabalai_id" => ($delegator != null 
                    && $approval_before_delegasi != null 
                    && $approval_before_delegasi == $order->stat_sertifikat)
                     ? $delegator->user_delegator_id : Auth::id(),
                "an_kabalai_id" => ($delegator != null 
                    && $approval_before_delegasi != null 
                    && $approval_before_delegasi == $order->stat_sertifikat) 
                    ? Auth::id() : null,
                "kabalai_date" => date("Y-m-d"),
                "stat_sertifikat"=>$request->get("is_approved") === "ya" ? 3 : 1,
                "kabalai_notes"=>$request->get("notes"),
                "file_review_kabalai"=>$request->get("is_approved") === "ya" ? null : $file_review,
                "path_review_kabalai"=>$request->get("is_approved") === "ya" ? null : $path_review,
                "stat_service_order"=>$request->get("is_approved") === "ya" ? 3 : 1,
                "qrcode_token"=>$token,
                "qrcode_skhpt_token" => $skhpttoken,
                "sertifikat_expired_at" => $sertifikat_expired_at,
            ]);
        }

        $history = new HistoryUttp();
        $history->request_status_id = $order->ServiceRequestItem->status_id;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = $request->get("is_approved") === "ya" ? 3 : 1;
        $history->warehouse_status_id = $order->stat_warehouse;
        $history->user_id = Auth::id();
        $history->save();

        if ($request->get("is_approved") === "ya" ) {
            if ($order->ujitipe_completed == true) {
                $lastOrderSET = ServiceOrderUttps::where("id", "<>", $id)
                ->whereNotNull("no_surat_tipe_int")
                ->where('no_surat_tipe_year', date("Y"))
                ->orderBy("no_surat_tipe_int", "desc")
                ->select("no_surat_tipe_int")
                ->first();

                if ($lastOrderSET != null) 
                {
                    $lastNoSET = $lastOrderSET->no_surat_tipe_int;
                    $nextNoSET = intval($lastNoSET)+1;
                } else {
                    $lastNoSET = 0;
                    $nextNoSET = 1;
                }

                $noSuratTipe = 'ET.'.sprintf("%04d", $nextNoSET).'/PKTN.4.5/'.date("m/Y");
                $mustInspects = DB::table('uttp_inspection_prices')
                        ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                        ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
                        ->leftJoin('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_request_uttp_item_inspections.request_item_id')
                        ->leftJoin('service_order_uttps', 'service_order_uttps.service_request_item_id', '=', 'service_request_uttp_items.id')
                        ->where('uttp_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
                        ->where('uttp_inspection_price_types.uttp_type_id', $order->ServiceRequestItem->uttp->type_id)
                        ->where(function($query) {
                            $query->where('service_order_uttps.ujitipe_completed', true);
                        })
                        ->selectRaw('uttp_inspection_prices.id as price_id, service_order_uttps.id as order_id')
                        ->get();
                $ordersToUpdate = $mustInspects->map(function ($item, $key) {
                    return $item->order_id;
                })->toArray();

                $token_tipe = $this->hashing($noSuratTipe, $noService);

                ServiceOrderUttps::whereIn('id', $ordersToUpdate)->update([
                    'no_surat_tipe' => $noSuratTipe,
                    'qrcode_tipe_token'=>$token_tipe,

                    "no_surat_tipe_int" => $nextNoSET,
                    "no_surat_tipe_year" => date("Y"),
                ]);
            }
        }
        if ($order->has_set == true) {
            $lastOrderSET = ServiceOrderUttps::where("id", "<>", $id)
                ->whereNotNull("no_surat_tipe_int")
                ->where('no_surat_tipe_year', date("Y"))
                ->orderBy("no_surat_tipe_int", "desc")
                ->select("no_surat_tipe_int")
                ->first();

            if ($lastOrderSET != null) 
            {
                $lastNoSET = $lastOrderSET->no_surat_tipe_int;
                $nextNoSET = intval($lastNoSET)+1;
            } else {
                $lastNoSET = 0;
                $nextNoSET = 1;
            }

            $noSuratTipe = 'ET.'.sprintf("%04d", $nextNoSET).'/PKTN.4.5/'.date("m/Y");
            
            $token_tipe = $this->hashing($noSuratTipe, $noService);

            ServiceOrderUttps::where('id', $id)->update([
                'no_surat_tipe' => $noSuratTipe,
                'qrcode_tipe_token'=>$token_tipe,

                "no_surat_tipe_int" => $nextNoSET,
                "no_surat_tipe_year" => date("Y"),
            ]);
        }


        if ($request->get("is_approved") === "ya" ) {
            MasterServiceType::where("id",$serviceType->id)->update(["last_no"=>$nextNo]);

            Uttp::where("id", $order->uttp_id)->update([
                "serial_no" => $order->tool_serial_no,
                "tool_brand" => $order->tool_brand,
                "tool_model" => $order->tool_model,
                "tool_media" => $order->tool_media,
                "tool_capacity" => $order->tool_capacity,
                "tool_capacity_min" => $order->tool_capacity_min,
                "tool_capacity_unit" => $order->tool_capacity_unit,
                "tool_made_in_id" => $order->tool_made_in_id,
                "tool_factory" => $order->tool_factory,
                "tool_factory_address" => $order->tool_factory_address,
            ]);
            //dd([$order->tool_owner_id, $order->ServiceRequest->label_sertifikat, $order->ServiceRequest->addr_sertifikat]);
            UttpOwner::where("id", $order->tool_owner_id)->update([
                "nama" => $order->ServiceRequest->label_sertifikat,
                "alamat" => $order->ServiceRequest->addr_sertifikat
            ]);

            /*
            $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
            $inspection->update(['status_sertifikat' => 1]); 
            */
            ServiceRequestUttpItemInspection::where('request_item_id', $order->service_request_item_id)
                ->update(['status_sertifikat' => 1]); 

            $this->checkAndUpdateFinishOrder($id);

            $customerEmail = $order->ServiceRequest->requestor->email;

            if ($order->cancel_at == null) {
                //Mail::to($customerEmail)->send(new CertificateDone($order));
                ProcessEmailJob::dispatch($customerEmail, new CertificateDone($order))->onQueue('emails');
            } else {
                //Mail::to($customerEmail)->send(new Cancel($order));
                ProcessEmailJob::dispatch($customerEmail, new Cancel($order))->onQueue('emails');
            }
             
        }
        
        return Redirect::route('approveuttp');
    }

    public function secrethashing(Request $request)
    {
        $no = $request->get('no');
        $order = $request->get('order');
        return $this->hashing($no, $order);
    }

    private function hashing($username, $password) {
        $token = Hash::make($username.':'.$password);
        if (strpos($token, '/') !== false) {
            $token = $this->hashing($username, $password);
        } 
        return $token;
    }

    public function approvesubmitall(Request $request)
    {
        $ids = explode(",", $request->get('ids'));
        $orders = ServiceOrderUttps::with([
            'ServiceRequest', 
        ])->whereIn('id', $ids)->get();

        foreach ($orders as $order) {
            if ($order->stat_sertifikat == null) {
                $this->updateapprovesubko($order->id);
                /*
                if ($order->ServiceRequest->service_type_id == 6 || $order->ServiceRequest->service_type_id == 7) {
                    // approval 1 == kalab
                    $this->updateapprovekalab($order->id);
                } elseif ($order->ServiceRequest->service_type_id == 4 || $order->ServiceRequest->service_type_id == 5) {
                    // approval 1 == subko
                    $this->updateapprovesubko($order->id);
                }
                */
            } elseif ($order->stat_sertifikat == 1) {
                // approval 2 == subko --> mestinya tidak ada
                $this->updateapprovesubko($order->id);
            } elseif ($order->stat_sertifikat == 2) {
                // approval 3 == kabalai
                $this->updateapprove($order->id, $order);
            }
        }
 
        return Redirect::route('approveuttp');
    }

    private function updateapprove($id, $order) {
        $noSertifikat = null;
        $noService = null;

        $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
        //$lastNo = $serviceType->last_no;
        //$nextNo = intval($lastNo)+1;
        $prefix = $serviceType->prefix;
        $kndl = $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'KN' : 'DL';

        $lastOrder = ServiceOrderUttps::where("id", "<>", $id)
                ->whereNotNull("no_sertifikat_int")
                ->where('no_sertifikat_year', date("Y"))
                ->orderBy("no_sertifikat_int", "desc")
                ->select("no_sertifikat_int")
                ->first();

        $lastNo = $lastOrder->no_sertifikat_int;
        $nextNo = intval($lastNo)+1;

        if ($order->cancel_at != null){
            $kndl = 'KET';
        }
        //$kndl = 'KN';
        //$noSertifikat = sprintf("%d", $nextNo).'/PKTN.4.8/KHP/'.$kndl.'/'.date("m/Y");
        //$noService = $serviceType->prefix.'-'.date("y").'-'.sprintf("%05d", $nextNo);

        $noSertifikat = $prefix.'.'.sprintf("%04d", $nextNo).'/PKTN.4.5/'.$kndl.'/'.date("m/Y");
        $noService = $serviceType->prefix.'-'.date("y").'-'.sprintf("%04d", $nextNo);

        //if ($order->ServiceRequest->service_type_id == 6 || $order->ServiceRequest->service_type_id == 7) {
        //    $noSertifikatTipe = $prefix.'T.'.sprintf("%04d", $nextNo).'/PKTN.4.8/'.$kndl.'/'.date("m/Y");
        //}
        $noSertifikatTipe = $order->no_sertifikat_tipe;

        $token = $this->hashing($noSertifikat, $noService);
        $skhpttoken = $this->hashing($noSertifikatTipe, $noService);

        $sertifikat_expired_at = null;
        if ($order->ServiceRequest->service_type_id == 4 || $order->ServiceRequest->service_type_id == 5) {
            
            $sertifikat_expired_at = date('Y-m-d', 
                //trtotime( date("Y-m-d") . ' + '. 
                strtotime( $order->staff_entry_dateout . ' + '. 
                ($order->ServiceRequestItem->uttp->type->masa_berlaku_ttu == null ?  0 : 
                $order->ServiceRequestItem->uttp->type->masa_berlaku_ttu) .
                ' year'));
        }

        /*
        ServiceOrderUttps::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            "no_service" => $noService,
            "kabalai_id" => Auth::id(),
            "kabalai_date" => date("Y-m-d"),
            "stat_sertifikat"=>3,
            "stat_service_order"=>3
        ]);
        */

        ServiceOrderUttps::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            //"no_sertifikat_tipe" => $noSertifikatTipe,
            "no_service" => $noService,
            "kabalai_id" => Auth::id(),
            "kabalai_date" => date("Y-m-d"),
            "stat_sertifikat" => 3,
            "stat_service_order" => 3,
            "qrcode_token" => $token,
            "qrcode_skhpt_token" => $skhpttoken,
            "sertifikat_expired_at" => $sertifikat_expired_at,

            "no_sertifikat_int" => $nextNo,
            "no_sertifikat_year" => date("Y"),
        ]);


        $history = new HistoryUttp();
        $history->request_status_id = 13;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = 3;
        $history->warehouse_status_id = $order->stat_warehouse;
        $history->user_id = Auth::id();
        $history->save();

        if ($order->ujitipe_completed == true) {

            $lastOrderSET = ServiceOrderUttps::where("id", "<>", $id)
                ->whereNotNull("no_surat_tipe_int")
                ->where('no_surat_tipe_year', date("Y"))
                ->orderBy("no_surat_tipe_int", "desc")
                ->select("no_surat_tipe_int")
                ->first();

            if ($lastOrderSET != null) 
            {
                $lastNoSET = $lastOrderSET->no_surat_tipe_int;
                $nextNoSET = intval($lastNoSET)+1;
            } else {
                $lastNoSET = 0;
                $nextNoSET = 1;
            }

            $noSuratTipe = 'ET.'.sprintf("%04d", $nextNoSET).'/PKTN.4.5/'.date("m/Y");
            $mustInspects = DB::table('uttp_inspection_prices')
                    ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                    ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
                    ->leftJoin('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_request_uttp_item_inspections.request_item_id')
                    ->leftJoin('service_order_uttps', 'service_order_uttps.service_request_item_id', '=', 'service_request_uttp_items.id')
                    ->where('uttp_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
                    ->where('uttp_inspection_price_types.uttp_type_id', $order->ServiceRequestItem->uttp->type_id)
                    ->where(function($query) {
                        $query->where('service_order_uttps.ujitipe_completed', true);
                    })
                    ->selectRaw('uttp_inspection_prices.id as price_id, service_order_uttps.id as order_id')
                    ->get();
            $ordersToUpdate = $mustInspects->map(function ($item, $key) {
                return $item->order_id;
            })->toArray();

            $token_tipe = $this->hashing($noSuratTipe, $noService);

            ServiceOrderUttps::whereIn('id', $ordersToUpdate)->update([
                'no_surat_tipe' => $noSuratTipe,
                'qrcode_tipe_token'=>$token_tipe,

                "no_surat_tipe_int" => $nextNoSET,
                "no_surat_tipe_year" => date("Y"),
            ]);
        }

        if ($order->has_set == true) {
            $lastOrderSET = ServiceOrderUttps::where("id", "<>", $id)
                ->whereNotNull("no_surat_tipe_int")
                ->where('no_surat_tipe_year', date("Y"))
                ->orderBy("no_surat_tipe_int", "desc")
                ->select("no_surat_tipe_int")
                ->first();

            if ($lastOrderSET != null) 
            {
                $lastNoSET = $lastOrderSET->no_surat_tipe_int;
                $nextNoSET = intval($lastNoSET)+1;
            } else {
                $lastNoSET = 0;
                $nextNoSET = 1;
            }

            $noSuratTipe = 'ET.'.sprintf("%04d", $nextNoSET).'/PKTN.4.5/'.date("m/Y");
            
            $token_tipe = $this->hashing($noSuratTipe, $noService);

            ServiceOrderUttps::where('id', $id)->update([
                'no_surat_tipe' => $noSuratTipe,
                'qrcode_tipe_token'=>$token_tipe,

                "no_surat_tipe_int" => $nextNoSET,
                "no_surat_tipe_year" => date("Y"),
            ]);
        }

        MasterServiceType::where("id",$serviceType->id)->update(["last_no"=>$nextNo]);

        Uttp::where("id", $order->uttp_id)->update([
            "serial_no" => $order->tool_serial_no,
            "tool_brand" => $order->tool_brand,
            "tool_model" => $order->tool_model,
            "tool_media" => $order->tool_media,
            "tool_capacity" => $order->tool_capacity,
            "tool_capacity_min" => $order->tool_capacity_min,
            "tool_capacity_unit" => $order->tool_capacity_unit,
            "tool_made_in_id" => $order->tool_made_in_id,
            "tool_factory" => $order->tool_factory,
            "tool_factory_address" => $order->tool_factory_address,
        ]);

        UttpOwner::where("id", $order->tool_owner_id)->update([
            "nama" -> $order->ServiceRequest->label_sertifikat,
            "alamat" -> $order->ServiceRequest->addr_sertifikat
        ]);

        /*
        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        $inspection->update(['status_sertifikat' => 1]); 
        */

        ServiceRequestUttpItemInspection::where('request_item_id', $order->service_request_item_id)
                ->update(['status_sertifikat' => 1]); 

        $this->checkAndUpdateFinishOrder($id);

        $customerEmail = $order->ServiceRequest->requestor->email;

        if ($order->cancel_at == null) {
            //Mail::to($customerEmail)->send(new CertificateDone($order));
            ProcessEmailJob::dispatch($customerEmail, new CertificateDone($order))->onQueue('emails');
        } else {
            //Mail::to($customerEmail)->send(new Cancel($order));
            ProcessEmailJob::dispatch($customerEmail, new Cancel($order))->onQueue('emails');
        }
    }

    public function download($id)
    {
        $order = ServiceOrderUttps::find($id);

        return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
    }

    public function preview($id)
    {
        $order = ServiceOrderUttps::find($id);
       
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_tipe_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_ttu_pdf';
        }

        $view = true;
        //dd($order->inspections);
        return view($blade,compact(
            'order', 'qrcode_generator', 'view'
        ));
    }

    public function previewTipe($id)
    {
        $order = ServiceOrderUttps::find($id);
       
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhpt_tipe_pdf';

        $view = true;
        //dd($order->inspections);
        return view($blade,compact(
            'order', 'qrcode_generator', 'view'
        ));
    }

    public function print($id)
    {
        $order = ServiceOrderUttps::find($id);

        $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_tipe_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_ttu_pdf';
        }

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));

        return $pdf->download($file_name . '.pdf');
    }

    public function printTipe($id)
    {
        $order = ServiceOrderUttps::find($id);

        $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhpt_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));

        return $pdf->download($file_name . '.pdf');
    }

    public function previewSETipe($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $otherOrders = ServiceOrderUttps::whereHas(
            'ServiceRequestItem', function($query) use($order)
            {
                $query->where("uttp_id",$order->ServiceRequestItem->uttp_id);  
            })
            ->get();

        /*
        $otherOrders = DB::table('uttp_inspection_prices')
            ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->join('service_order_uttps', 'service_order_uttps.service_request_item_inspection_id', '=', 'service_request_uttp_item_inspections.id')
            ->where('uttp_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
            ->where('uttp_inspection_price_types.uttp_type_id', $order->ServiceRequestItem->uttp->type_id)
            ->where('service_order_uttps.ujitipe_completed', true)
            ->selectRaw('service_order_uttps.*')
            ->get();
            */
        
        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_tipe_token);

        $blade = 'serviceuttp.set_tipe_pdf';

        $view = true;
        //dd($order);
        return view($blade,compact(
            'order', 'otherOrders', 'qrcode_generator', 'view'
        ));
    }

    public function printSETipe($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $file_name = 'SET '.($order->no_surat_tipe ? $order->no_surat_tipe : $order->ServiceRequestItem->no_order);

        $otherOrders = ServiceOrderUttps::whereHas(
            'ServiceRequestItem', function($query) use($order)
            {
                $query->where("uttp_id",$order->ServiceRequestItem->uttp_id);  
            })
            ->get();
        /*
        $otherOrders = DB::table('uttp_inspection_prices')
            ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->join('service_order_uttps', 'service_order_uttps.service_request_item_inspection_id', '=', 'service_request_uttp_item_inspections.id')
            ->where('uttp_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
            ->where('uttp_inspection_price_types.uttp_type_id', $order->ServiceRequestItem->uttp->type_id)
            ->where('service_order_uttps.ujitipe_completed', true)
            ->selectRaw('service_order_uttps.*')
            ->get();
            */
        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_tipe_token);

        $blade = 'serviceuttp.set_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact(['order', 'otherOrders', 'qrcode_generator', 'view']));
       
        return $pdf->download($file_name . '.pdf');
    }

    private function checkAndUpdateFinishOrder($id) 
    {
        $order = ServiceOrderUttps::find($id);

        $orders_count = DB::table('service_order_uttps')
            ->selectRaw('sum(case when stat_sertifikat = 3 then 1 else 0 end) count_sert_3, count(id) count_all')
            ->where('service_request_id', $order->service_request_id)->get();

        if ($orders_count[0]->count_sert_3 == $orders_count[0]->count_all) {
            ServiceRequestUttp::find($order->service_request_id)->update(['status_sertifikat'=>3]);
        }

        if ((
                ($order->ServiceRequest->lokasi_pengujian == 'dalam' && $order->stat_warehouse == 2) 
                 ||
                 ($order->ServiceRequest->lokasi_pengujian == 'luar' 
                    && $order->stat_warehouse == -1
                    && $order->ServiceRequest->payment_valid_date != null)
            ) && $order->stat_sertifikat == 3) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestUttpItemInspection::where("request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUttp();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                //$history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }

            $customerEmail = $order->ServiceRequest->requestor->email;

            ProcessEmailJob::dispatch($customerEmail, new ServiceDone($order))->onQueue('emails');
            
        }
    }

    public function downloadReview($type, $id)
    {
        $order = ServiceOrderUttps::find($id);

        if ($type == 'subko') {
            return Storage::disk('public')->download($order->path_review_subkoordinator, $order->file_review_subkoordinator);
        } else {
            return Storage::disk('public')->download($order->path_review_kabalai, $order->file_review_kabalai);
        }
    }
}
