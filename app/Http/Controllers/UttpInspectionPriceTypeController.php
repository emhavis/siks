<?php

namespace App\Http\Controllers;

use App\UttpInspectionPriceType;
use App\MasterUttpType;
use App\UttpInspectionPrice;
use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class UttpInspectionPriceTypeController extends Controller
{
    private $UttpInspectionPriceType;
    private $MyProjects;

    public function __construct() 
    {
        $this->UttpInspectionPriceType = new UttpInspectionPriceType();
        $this->UttpInspectionPrice = new UttpInspectionPrice();
        $this->MasterUttpType = new MasterUttpType();
        $this->MyProjects = new MyProjects();
    }

    public function index() 
    {
        $attribute = $this->MyProjects->setup("uttpInspectionPriceType");

        $prices = $this->UttpInspectionPriceType->get();
        
        return view('uttpinspectionpricetype.index', compact('prices','attribute'));
    }

    public function create($id=null) 
    {
        $attribute = $this->MyProjects->setup("uttpInspectionPriceType");

        $row = null;

        if($id)
        {
            $row = $this->UttpInspectionPriceType->whereId($id)->first();
        }

        $prices = $this->UttpInspectionPrice->select(
            DB::raw("CONCAT(inspection_type, ' (', price, ' per ', unit, ')') AS inspection_price"), "id"
        )->pluck('inspection_price', 'id');
        $types = $this->MasterUttpType->pluck('uttp_type', 'id');

        return view('uttpinspectionpricetype.create',compact('row','id','attribute','prices','types'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["inspection_type"] = ['required'];
        $rules["service_type_id"] = ['required'];
        $rules["instalasi_id"] = ['required'];
        $rules["inspection_template_id"] = ['required'];
        $rules["price"] = ['required', 'integer', 'min:0'];
        $rules["unit"] = ['required'];
        $rules["has_range"] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            $request["has_range"] = $request->get("has_range") === "ya";
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $request["updated_at"] = date("Y-m-d H:i:s");
                $this->UttpInspectionPrice->whereId($id)->update($request->all());
            }
            else
            {
                $request["created_at"] = date("Y-m-d H:i:s");
                $this->UttpInspectionPrice->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->UttpInspectionPrice->find($request->id);

        if(count($row->MasterUsers)>0)
        {
            $response["message"] = "Nama Laboratorium sudah terpakai di User List";
        }
        else
        {
            if($request->action=="delete")
            {
                $row->delete();
                $response["status"] = true;
            }
        }

        return response($response);
    }
}
