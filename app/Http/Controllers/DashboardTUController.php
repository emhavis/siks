<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DashboardTUController extends Controller
{
    private $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("dashboardtu");
        if($attribute==null) return userlogout();

        $start = null;
        $end = null;

        $q_sum_all = DB::table('service_order_uttps')
            ->join('service_request_uttp_item_inspections', 'service_order_uttps.service_request_item_inspection_id', '=', 'service_request_uttp_item_inspections.id')
            ->join('service_request_uttp_items', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->join('service_request_uttps', 'service_request_uttp_items.request_id', '=', 'service_request_uttps.id')
            ->join('uttp_inspection_prices', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->join('master_instalasi', 'uttp_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->selectRaw("master_instalasi.nama_instalasi,
            count(distinct service_request_uttp_items.id) as jumlah,
            sum(case when service_request_uttps.status_id = 14 then 1 else 0 end) as jumlah_selesai,
            sum(case when service_request_uttps.status_id not in (14, 16) then 1 else 0 end) as jumlah_proses,
            sum(case when service_request_uttps.status_id = 14 and 
                DATE_PART('day', service_order_uttps.kabalai_date::timestamp - service_request_uttps.received_date::timestamp) 
                    <= (case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end) then 1 else 0 end) as jumlah_sesuai_sla,
            sum(case when service_request_uttps.status_id = 14 and 
                DATE_PART('day', service_order_uttps.kabalai_date::timestamp - service_request_uttps.received_date::timestamp) 
                    > (case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end) then 1 else 0 end) as jumlah_lebih_sla
            ")
            ->groupBy('master_instalasi.nama_instalasi');

        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
        
            $q_sum_all = $q_sum_all->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            $q_sum_all = $q_sum_all->where('staff_entry_datein', '>=', $start);
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            $q_sum_all = $q_sum_all->where('staff_entry_datein', '<=', $end);
        }
            
        $summaries = $q_sum_all->get();
        $instalasi = $q_sum_all->pluck('nama_instalasi');

        $q_sum_ttu = $q_sum_all->whereIn('service_request_uttps.service_type_id', [4,5]);
        $summaries_ttu = $q_sum_ttu->get();
        $instalasi_ttu = $q_sum_ttu->pluck('nama_instalasi');

        $q_sum_tipe = $q_sum_all->whereIn('service_request_uttps.service_type_id', [6,7]);
        $summaries_tipe = $q_sum_tipe->get();
        $instalasi_tipe = $q_sum_tipe->pluck('nama_instalasi');
        
        return view('dashboardtu.home',compact([
            'attribute', 
            'summaries', 
            'instalasi',
            'summaries_ttu', 
            'instalasi_ttu',
            'summaries_tipe', 
            'instalasi_tipe',
            'start', 'end',
        ]));
    }
}
