<?php

namespace App\Http\Controllers;

use App\StandardDetailType;
use App\StandardToolType;
use App\StandardMeasurementType;
use App\StandardInspectionPrice;
use App\MyClass\MyProjects;
use App\StandardMeasurementUnit;
use Illuminate\Http\Request;

class MeasurementTypeController extends Controller
{
    private $StandardMeasurementType;
    private $StandardMeasurementUnit;
    // private $StandardInspectionPrice;
    private $StandardToolType;
    private $StandardDetailType;
    private $MyProjects;

    public function __construct()
    {
        $this->StandardMeasurementType = new StandardMeasurementType();
        $this->StandardMeasurementUnit = new StandardMeasurementUnit();
        // $this->StandardInspectionPrice = new StandardInspectionPrice();
        $this->StandardToolType = new StandardToolType();
        $this->StandardDetailType = new StandardDetailType();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup('detailtype');

        $row = $this->StandardDetailType->get();
        return view('measurementtype.index', compact('row','attribute'));
    }

    public function create()
    {
        $attribute = $this->MyProjects->setup("detailtype");

        $units = $this->StandardMeasurementUnit->dropdown();
        $standardMeasurementTypes = $this->StandardMeasurementType->dropdown();

        return view('measurementtype.create', compact('standardMeasurementTypes',"units",'attribute'));
    }

    public function store(Request $request)
    {
        switch($request->oper)
        {
            case "add":
                if(strlen($request->standard_name)>0)
                {
                    switch ($request->type)
                    {
                        case 'standard_measurement_type_id':
                            $this->StandardMeasurementType
                            ->insert(["standard_type"=>$request->standard_name]);
                            return response([true]);
                            break;
                        case 'standard_tool_type_id':
                            if(intval($request->standard_measurement_type_id)>0)
                            {
                                $this->StandardToolType
                                ->insert(["attribute_name"=>$request->standard_name,"standard_measurement_type_id"=>$request->standard_measurement_type_id]);
                                return response([true]);
                            }
                            else
                            {
                                return response([false,"Standard Measurement tidak boleh kosong"]);
                            }
                            break;
                        case 'standard_detail_type_id':
                            if(intval($request->standard_tool_type_id)>0)
                            {
                                $this->StandardDetailType
                                ->insert(["standard_detail_type_name"=>$request->standard_name,"standard_tool_type_id"=>$request->standard_tool_type_id]);
                                return response([true]);
                            }
                            else
                            {
                                return response([false,"Standard Tool tidak boleh kosong"]);
                            }
                            break;
                        case 'inspection_price_id':
                            if(intval($request->standard_detail_type_id)>0 && intval($request->price)>0)
                            {
                                $id = StandardMeasurementUnit::where("measurement_unit",$request->unit)->value("id");
                                StandardInspectionPrice::insert([
                                    "inspection_type"=>$request->standard_name,
                                    "standard_detail_type_id"=>$request->standard_detail_type_id,
                                    "standard_measurement_unit_id"=>$id,
                                    "price"=>$request->price
                                ]);
                                return response([true]);
                            }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     else
                            {
                                return response([false,"Standard Detail tidak boleh kosong. Harga Inspeksi tidak boleh kosong"]);
                            }
                            break;
                    }
                }
                else
                {
                    return response([false,"Data Kosong. Harap diisi lebih dahulu"]);
                }
                break;
            case "edit":
                if(strlen($request->standard_name)>0)
                {
                    switch ($request->type)
                    {
                        case 'standard_measurement_type_id':
                            $this->StandardMeasurementType
                            ->where("id",$request->standard_measurement_type_id)
                            ->update(["standard_type"=>$request->standard_name]);
                            return response([true]);
                            break;
                        case 'standard_tool_type_id':
                            if(intval($request->standard_measurement_type_id)>0)
                            {
                                $this->StandardToolType
                                ->where("id",$request->standard_tool_type_id)
                                ->update(["attribute_name"=>$request->standard_name,"standard_measurement_type_id"=>$request->standard_measurement_type_id]);
                                return response([true]);
                            }
                            else
                            {
                                return response([false,"Standard Measurement tidak boleh kosong"]);
                            }
                            break;
                        case 'standard_detail_type_id':
                            if(intval($request->standard_tool_type_id)>0)
                            {
                                $this->StandardDetailType
                                ->where("id",$request->standard_detail_type_id)
                                ->update(["standard_detail_type_name"=>$request->standard_name,"standard_tool_type_id"=>$request->standard_tool_type_id]);
                                return response([true]);
                            }
                            else
                            {
                                return response([false,"Standard Tool tidak boleh kosong"]);
                            }
                            break;
                        case 'inspection_price_id':
                            if(intval($request->standard_detail_type_id)>0 && intval($request->price)>0)
                            {
                                $id = StandardMeasurementUnit::where("measurement_unit",$request->unit)->value("id");
                                // dd($id);
                                StandardInspectionPrice::whereId($request->inspection_price_id)
                                ->update([
                                    "inspection_type"=>$request->standard_name,
                                    "standard_detail_type_id"=>$request->standard_detail_type_id,
                                    "standard_measurement_unit_id"=>$id,
                                    "price"=>$request->price
                                ]);
                                return response([true]);
                            }
                            else
                            {
                                return response([false,"Standard Detail tidak boleh kosong<br>Harga Inspeksi tidak boleh kosong"]);
                            }
                            break;
                    }
                }
                else
                {
                    return response([false,"Data Kosong. Harap diisi lebih dahulu"]);
                }
                break;
            case "del":
                switch ($request->type)
                {
                    case 'standard_measurement_type_id':
                        $row = $this->StandardMeasurementType->find($request->standard_measurement_type_id);
                        if(count($row->StandardToolType))
                        {
                            return response([false,"Measurement type masih ada data di Tool type"]);
                        }
                        else
                        {
                            $row->delete();
                            return response([true]);
                        }
                        break;
                    case 'standard_tool_type_id':
                        $row = $this->StandardToolType->find($request->standard_tool_type_id);
                        if(count($row->StandardDetailType))
                        {
                            return ([false,"Tool type masih ada data di Detail type"]);
                        }
                        else
                        {
                            $row->delete();
                            return response([true]);
                        }
                        break;
                    case 'standard_detail_type_id':
                        $row = $this->StandardDetailType->find($request->standard_detail_type_id);
                        if(count($row->StandardInspectionPrice)>0)
                        {
                            return response([false,"Detail Type masih ada data di Inspection Price Data"]);
                        }
                        else
                        {
                            $row->delete();
                            return response([true]);
                        }
                        break;
                    case 'inspection_price_id':
                        $row = StandardInspectionPrice::find($request->inspection_price_id);
                    
                        if(count($row->ServiceRequestItemInspection)>0)
                        {
                            return response([false,"Inspection Price Data terdapat di transaksi"]);
                        }
                        else
                        {
                            $row->delete();
                            return response([true]);
                        }
                        break;
                }
                break;
        }

    }

    // public function detail_type(Request $request)
    // {
    //     $row = $this->StandardDetailType
    //     ->where('standard_tool_type_id', $request->standardToolTypeId)
    //     ->orderBy('id')
    //     ->pluck('standard_detail_type_name', 'id');
        
    //     return response($row);
    // }

    // public function tool_type(Request $request)
    // {
    //     $row = $this->StandardToolType
    //     ->where('standard_measurement_type_id', $request->standardMeasurementTypeId)
    //     ->orderBy('id')
    //     ->pluck('attribute_name', 'id');

    //     return response($row);
    // }

    // public function measurement_type(Request $request)
    // {
    //     $row = $this->StandardMeasurementType
    //     ->orderBy('id')
    //     ->pluck('standard_type', 'id');

    //     return response($row);
    // }

    // public function inspection_price(Request $request)
    // {
    //     $row = $this->StandardInspectionPrice
    //     ->where('standard_detail_type_id', $request->standardDetailTypeId)
    //     ->orderBy('id')
    //     ->select('inspection_type', 'id', 'price')
    //     ->get();

    //     return response($row);
    // }
}
