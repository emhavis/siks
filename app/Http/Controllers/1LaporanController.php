<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadeRequest;
use App\Http\Repositories\StandardRepository;
use App\Http\Repositories\UmlRepository;
use App\Http\Repositories\QrcodeRepository;
use App\Qrcode;
use Validator;
use Redirect;
use Auth;
use Session;
use URL;
use Log;
use PDF;
use Storage;

class LaporanController extends Controller
{
    protected $standardRepo;
    protected $umlRepo;
	protected $QRCODERepo;

    public function __construct(QrcodeRepository $qrcodeRepo, StandardRepository $standardRepo, UmlRepository $umlRepo)
    {
        $this->middleware('auth', ['except' => ['qrcodefordropdown','qrcodesprint','qrcodeinfoByUml']]);
        $this->standardRepo = $standardRepo;
        $this->umlRepo = $umlRepo;
        $this->QRCODERepo = $qrcodeRepo;
    }

    public function index(Request $request)
    {
        
    }

    public function uttps()
    {
        return view('laporan.uttps');
    }

}
