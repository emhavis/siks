<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Delegation;
use App\MasterUsers;

class DelegationUttpController extends Controller
{
    private $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("delegationuttp");

        $filter = $request->get('filter') ? $request->get('filter') : 'active';

        $delegator_user = MasterUsers::find(Auth::id());

        $delegations = Delegation::where('user_delegator_id', $delegator_user->id)
            ->select('user_delegations.*')
            ->selectRaw('case when berlaku_sampai >= ? 
                then 1 else 0 end as status', [date('Y-m-d')]);

        if ($filter == 'active') {
            $delegations = $delegations
                ->where('berlaku_sampai', '>=', date('Y-m-d'));
        } elseif ($filter == 'history') {
            $delegations = $delegations
                ->where('berlaku_sampai', '<', date('Y-m-d'));
        } else {

        }

        $delegations = $delegations->get();
        //dd($delegations);

        return view('delegationuttp.index',compact(
            'attribute',
            'delegations',
            'filter',
        ));
    }

    public function create()
    {
        $attribute = $this->MyProjects->setup("delegationuttp");

        $delegatee_users = MasterUsers::whereNotNull('petugas_uttp_id')
            ->whereHas('PetugasUttp', function($query) 
            {
                $query->where('flag_unit', 'uttp')
                    ->where('is_active', true);
            })
            ->pluck('full_name', 'id')->prepend('-- Pilih Pegawai --','');

        $delegator_user = MasterUsers::find(Auth::id());

        return view('delegationuttp.create',compact(
            'attribute',
            'delegatee_users',
            'delegator_user',
        ));
    }

    public function store(Request $request)
    {
        $response["delegation"] = false;
        $rules["user_delegatee_id"] = ['required'];
        $rules["berlaku_mulai"] = ['required','date'];
        $rules["berlaku_sampai"] = ['required','date','after:berlaku_mulai'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $data['user_delegator_id'] = $request->get('user_delegator_id');
            $data['user_delegatee_id'] = $request->get('user_delegatee_id');
            $data['berlaku_mulai'] = date("Y-m-d", strtotime($request->get('berlaku_mulai')));
            $data['berlaku_sampai'] = date("Y-m-d", strtotime($request->get('berlaku_sampai')));

            $delegation = new Delegation();
            if (!$delegation->create($data)) {
                $response["status"] = false;
                $response["messages"] = "Data gagal disimpan";

            }
            
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";

            return redirect()->route('delegationuttp')->with('response', $response);
        }

        return redirect()->route('delegationuttp')->with('response', $response);
    }

    public function edit($id)
    {
        $attribute = $this->MyProjects->setup("delegationuttp");

        $delegation = Delegation::find($id);

        $delegatee_users = MasterUsers::whereNotNull('petugas_uttp_id')
            ->whereHas('PetugasUttp', function($query) 
            {
                $query//->where('flag_unit', 'uttp')
                    ->where('is_active', true);
            })
            ->pluck('full_name', 'id')->prepend('-- Pilih Pegawai --','');

        $delegator_user = MasterUsers::find(Auth::id());

        return view('delegationuttp.edit',compact(
            'attribute',
            'delegation',
            'delegatee_users',
            'delegator_user',
        ));
    }

    public function update($id, Request $request)
    {
        $delegation = Delegation::find($id);

        $response["delegation"] = false;
        $rules["user_delegatee_id"] = ['required'];
        $rules["berlaku_mulai"] = ['required','date'];
        $rules["berlaku_sampai"] = ['required','date','after:berlaku_mulai'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $data['user_delegator_id'] = $request->get('user_delegator_id');
            $data['user_delegatee_id'] = $request->get('user_delegatee_id');
            $data['berlaku_mulai'] = date("Y-m-d", strtotime($request->get('berlaku_mulai')));
            $data['berlaku_sampai'] = date("Y-m-d", strtotime($request->get('berlaku_sampai')));

            if (!$delegation->update($data)) {
                $response["status"] = false;
                $response["messages"] = "Data gagal disimpan";
            }
            
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";

            return redirect()->route('delegationuttp')->with('response', $response);
        }

        return redirect()->route('delegationuttp')->with('response', $response);
    }

    public function destroy(Request $request)
    {
        $id = $request->get('id');
        $row = Delegation::find($id);

        if($row)
        {
            Delegation::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('delegationuttp')->with('response', $response);
    }
}