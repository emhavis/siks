<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MyClass\MyProjects;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\RevisionUttp;

use PDF;

class RevisionApprovalUttpController extends Controller
{
    

    public function __construct()
    {
        $this->MyProjects = new MyProjects();

        $this->seri_revisi_arrays = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    }

    public function index()
    {
        $approval_before = Auth::user()->user_role == 9 ? 0 : 1;

        $attribute = $this->MyProjects->setup("revisionapproveuttp");

        $rows = RevisionUttp::whereIn('status', [3])
            ->whereIn("status_approval", [$approval_before])
            ->orderBy('staff_at','desc')->get();

        return view('revisionapproveuttp.index',compact('rows','attribute'));
    }

    public function approvesubko($id)
    {
        $attribute = $this->MyProjects->setup("approveuttp");

        $revision = RevisionUttp::find($id);

        return view('revisionapproveuttp.edit',compact(
            'revision', 'attribute'
        ));
    }

    public function approvesubmitsubko($id, Request $request)
    {
        $revision = RevisionUttp::find($id);
     
        $revision->update([
            "subko_id" => Auth::id(),
            "subko_at" => date("Y-m-d H:i:s"),
            "status_approval" => 1,
        ]);

        return Redirect::route('revisionapproveuttp');
    }

    public function approve($id)
    {
        $attribute = $this->MyProjects->setup("approveuttp");

        $revision = RevisionUttp::find($id);

        return view('revisionapproveuttp.edit',compact(
            'revision', 'attribute'
        ));
    }

    public function approvesubmit($id, Request $request)
    {
        $revision = RevisionUttp::find($id);

        $revision_others = RevisionUttp::where('order_id', $revision->order_id)
            ->where('status_approval', 2)
            ->where('id', '<>', $id)
            ->orderBy('id', 'desc')
            ->get();
        if (count($revision_others) > 0) {
            $last_seri_revisi = $revision_others[0]->seri_revisi;
        } else {
            $last_seri_revisi = 0;
        }

        $seri_revisi = intval($last_seri_revisi)+1;
        
        $no_sertifikat = $revision->order->no_sertifikat;
        $no_service = $revision->order->no_service;
        $no_sertifikat_tipe = $revision->order->no_sertifikat_tipe;
        $no_surat_tipe = $revision->order->no_surat_tipe;

        $no_sertifikat_parts = explode("/", $no_sertifikat);
        $no_sertifikat_parts[0]= $no_sertifikat_parts[0] . $this->seri_revisi_arrays[$seri_revisi - 1];
        $no_sertifikat = implode("/", $no_sertifikat_parts);

        $no_service = $no_service . $this->seri_revisi_arrays[$seri_revisi - 1];

        $no_sertifikat_tipe_parts = explode("/", $no_sertifikat_tipe);
        $no_sertifikat_tipe_parts[0]= $no_sertifikat_tipe_parts[0] . $this->seri_revisi_arrays[$seri_revisi - 1];
        $no_sertifikat_tipe = implode("/", $no_sertifikat_tipe_parts);

        $no_surat_tipe_parts = explode("/", $no_surat_tipe);
        $no_surat_tipe_parts[0]= $no_surat_tipe_parts[0] . $this->seri_revisi_arrays[$seri_revisi - 1];
        $no_surat_tipe = implode("/", $no_surat_tipe_parts);

        $revision->update([
            "kabalai_id" => Auth::id(),
            "kabalai_at" => date("Y-m-d H:i:s"),
            "status_approval" => 2,
            "seri_revisi" => $seri_revisi,
            "no_sertifikat" => $no_sertifikat,
            "no_service" => $no_service,
            "no_sertifikat_tipe" => $no_sertifikat_tipe,
            "no_surat_tipe" => $no_surat_tipe,
            "status" => 4,
        ]);
        
        
        return Redirect::route('revisionapproveuttp');
    }

    public function download($id)
    {
        $order = ServiceOrderUttps::find($id);

        return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
    }

    public function preview($id)
    {
        $order = RevisionUttp::find($id);

        $qrcode_generator = route('documentuttp.valid', $order->order->qrcode_token);

        $blade = 'revisionapproveuttp.skhp_pdf';
        if ($order->order->ServiceRequest->service_type_id == 4 || 
            $order->order->ServiceRequest->service_type_id == 5) {
                $blade = 'revisionapproveuttp.skhp_pdf_ttu';
        }

        $view = true;
        //dd($order);
        return view($blade,compact(
            'order', 'qrcode_generator', 'view'
        ));
    }

    public function print($id)
    {
        $order = RevisionUttp::find($id);

        $qrcode_generator = route('documentuttp.valid', $order->order->qrcode_token);
        
        $blade = 'revisionapproveuttp.skhp_pdf';
        if ($order->order->ServiceRequest->service_type_id == 4 || 
            $order->order->ServiceRequest->service_type_id == 5) {
                $blade = 'revisionapproveuttp.skhp_pdf_ttu';
        }

        $view = false;
      

        $file_name = 'skhp'.$order->order->ServiceRequest->no_order;
        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo')
        ])
        ->loadview($blade,compact('order', 'qrcode_generator', 'view'));

        return $pdf->download('skhp.pdf');  
    }

}
