<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class SecureController extends Controller
{

    public function authenticate(Request $request)
    {
        $response["status"] = false;
        $rules["username"] = ['required'];
        $rules["password"] = ['required'];
        $rules["client"] = ['required'];
        $rules["secret_key"] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $client = DB::table('client_apps')
                ->where('client_name', $request->get('client'))
                ->where('secret_key', $request->get('secret_key'))
                ->first();

            $response["messages"] = "Client tidak dikenali";

            if ($client) {
                $credentials = $request->only('username', 'password');

                $response["messages"] = "User atau passowrd tidak dikenali";

                if (Auth::attempt($credentials)) {

                    $token = DB::table('user_access_tokens')
                        ->where('client_id', $client->id)
                        ->where('user_id', Auth::id())
                        ->where('expired_at', '>', date("Y-m-d H:i:s"))
                        ->first();

                    if ($token) {
                        $response["token"] = $token->token;
                    } else {
                        //$response["token"] = Hash::make($request->get('username').':'.$request->get('password'));
                        $response["token"] = $this->hashing($request->get('username'), $request->get('password'));

                        DB::table('user_access_tokens')->insert([
                            'client_id' => $client->id,
                            'user_id' => Auth::id(),
                            'token' => $response["token"],
                            'created_at' => date("Y-m-d H:i:s"),
                            'expired_at' => date("Y-m-d H:i:s", strtotime('+6 hours'))
                        ]);
                    }

                    $response["status"] = true;
                    $response["messages"] = "Berhasil login";

                    
                }
            }

            
        }
        return response($response);
    }

    private function hashing($username, $password) {
        $token = Hash::make($username.':'.$password);
        if (strpos($token, '/') !== false) {
            $token = $this->hashing($username, $password);
        } 
        return $token;
    }

    /*
    public function client() 
    {
        DB::table('client_apps')->insert([
            'id' => 1,
            'client_name' => 'siks',
            'secret_key' => Hash::make('siks-metrologi')
        ]);
        DB::table('client_apps')->insert([
            'id' => 2,
            'client_name' => 'skhp',
            'secret_key' => Hash::make('skhp-metrologi')
        ]);

        return response("ok");
    }
    */
    
}
