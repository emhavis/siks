<?php

namespace App\Http\Controllers;

use App\MasterPetugasUttp;
use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PetugasController extends Controller
{
    private $Petugas;
    private $MyProjects;

    public function __construct() 
    {
        $this->Petugas = new MasterPetugasUttp();
        $this->MyProjects = new MyProjects();
    }

    public function index() 
    {
        $attribute = $this->MyProjects->setup("petugas");

        $petugas = $this->Petugas->get();
        return view('petugas.index', compact('petugas','attribute'));
    }

    public function create($id=null) 
    {
        $attribute = $this->MyProjects->setup("petugas");

        $row = null;

        if($id)
        {
            $row = $this->Petugas->find($id);
        }

        $pangkats = \DB::table('pangkat_golongan')
            ->selectRaw("pangkat || ' (' || golongan || ')' as pangkat")
            ->pluck('pangkat', 'pangkat');

        return view('petugas.create',compact('row','id','pangkats', 'attribute'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["nama"] = ['required'];
        $rules["nip"] = ['required'];
        $rules["email"] = ['required'];
        $rules["pangkat"] = ['required'];
        $rules["jabatan"] = ['required'];
        $rules["flag_unit"] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                //$request["updated_at"] = date("Y-m-d H:i:s");
                $request['is_active'] = true;
                $this->Petugas->find($id)->update($request->all());
            }
            else
            {
                //$request["created_at"] = date("Y-m-d H:i:s");
                //$request["updated_at"] = date("Y-m-d H:i:s");
                $this->Petugas->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->Petugas->find($request->id);

        
        switch ($request->action) 
        {
            case 'delete':
                $row->delete();
                $response["status"] = true;
                break;
            case 'aktif':
                $row->update(['is_active' => true]);
                $response["status"] = true;
                break;
            case 'nonaktif':
                $row->update(['is_active' => false]);
                $response["status"] = true;
                break;
        }

        return response($response);
    }
}
