<?php

namespace App\Http\Controllers;

use App\MasterPetugasUttp;
use App\ServiceRequestUttp;

use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PriceSetting extends Controller
{
    private $Petugas;
    private $MyProjects;

    public function __construct() 
    {
        $this->Petugas = new MasterPetugasUttp();
        $this->MyProjects = new MyProjects();
    }

    public function create(){
        $attribute = $this->MyProjects->setup("settings");
        $tarif =  DB::table("uut_inspection_prices as a")
                ->join("uut_inspection_price_types as c","c.inspection_price_id","a.id")
                ->join("master_standard_types as b","c.uut_type_id","b.id")
                ->select("a.*")
                ->where("b.id",1)
                ->get();

        $nonuml = DB::table("uut_inspection_prices as a")
                    ->whereNull("a.user_type")
                    ->orderBy('a.inspection_type')
                    ->get();

        $uml = DB::table("uut_inspection_prices as a")
                    ->where("a.user_type","UML")
                    ->orderBy('a.inspection_type')
                    ->get();
        
        $standard = DB::table('master_standard_types')
                    ->where('is_active','true')
                    ->select('id','uut_type')
                    ->pluck('uut_type','id');

        return view('settings/price.create',compact('attribute','nonuml', 'uml' , 'tarif','standard'));
    }

    public function save(Request $request){
        if(count($request->get('ids_uml')) > 0 && (count($request->get('ids_uml')) === count($request->get('ids_nonuml')))){
            if((null !== $request->get('ids_uml')) && gettype($request->get('ids_uml') == 'array')){
                $tipe = $request->get('type');
                $del = DB::table('uut_inspection_price_types')->where('uut_type_id',$tipe)->get();
                $countDel = $del->count();
                if($countDel > 0){
                    DB::table('uut_inspection_price_types')->where('uut_type_id',$tipe)->delete();
                }
                for($i = 0; $i < count($request->get('ids_uml')); $i ++){
                    $uml = $request->get('ids_uml');
                    $nonuml = $request->get('ids_nonuml');  
                    
                    $a =DB::table('uut_inspection_price_types')->insert([
                        'uut_type_id' => $tipe,
                        'inspection_price_id' => $uml[$i],
                    ]);
                    $b =DB::table('uut_inspection_price_types')->insert([
                        'uut_type_id' => $tipe,
                        'inspection_price_id' => $nonuml[$i],
                    ]);
                }
            }
            \Session::flash('alert-success', 'Penambahan harga berhasil');
            return redirect()->route('setting.price');
        }else{
            \Session::flash('alert-danger', 'Jumlah penujian untuk UML dan non UML harus sama');
            return redirect()->route('setting.price');
        }


    }

    public function getData(Request $request){
        // dd($request->get('id'));
        $nonuml = DB::table("uut_inspection_prices as a")
                    ->whereNull("a.user_type")
                    ->orderBy('a.inspection_type')
                    ->get()->toArray();
        return response(['data' => json_encode($nonuml,JSON_UNESCAPED_UNICODE)]);
    }
    // public function getDataType(Request $request){
    //     dd($request->get('id'));
    //     $nonuml = DB::table("uut_inspection_prices as a")
    //                 ->whereNull("a.user_type")
    //                 ->orderBy('a.inspection_type')
    //                 ->get()->toArray();
    //     return response(['data' => json_encode($nonuml,JSON_UNESCAPED_UNICODE)]);
    // }
    public function getDataUml(Request $request){
        $uml = DB::table("uut_inspection_prices as a")
                    ->whereNotNull("a.user_type")
                    ->orderBy('a.inspection_type')
                    ->get()->toArray();
        return response(['data' => json_encode($uml,JSON_UNESCAPED_UNICODE)]);
    }

    public function getPrice(Request $request){
        $id =0;
        if(isset($request->id)){
            $id = $request->id;
        }
        
        $tarif = DB::table("uut_inspection_prices as a")
            ->join("uut_inspection_price_types as c","c.inspection_price_id","a.id")
            ->join("master_standard_types as b","c.uut_type_id","b.id")
            ->select("a.*")
            ->where("b.id",$id)
            ->get();

        return $tarif;
    }

    public function priceSave(Request $request){
        unset($request['_token']);
        // unset($request['id']);
        
        $id = DB::table('uut_inspection_prices')->max('id');
        $request['id'] = $id + 1;
        $request['user_type'] = null;
        $request['service_type_id'] = 1;
        DB::table('uut_inspection_prices')->insert($request->all());
        $request['id'] = $id + 2;
        $request['user_type'] = 'UML';
        $request['service_type_id'] = 2;
        $request['price'] = 0;
        DB::table('uut_inspection_prices')->insert($request->all());
        return redirect()->route('setting.price');
    }

    public function edit($id,$price){
        $attribute = $this->MyProjects->setup("settings");
        return view('settings/price.edit_price',compact('attribute','id','price'));
    }

    public function editSave(Request $request){
        unset($request['_token']);
        // unset($request['id']);
        $request['price'] = substr($request->get('price'),2);
        $request['price'] = strtok($request->get('price'), ",");
        $request['price'] = str_replace(".",'',$request->get('price'));
        
        $id = $request->get('id');
        $data = DB::table('uut_inspection_prices')->where('id',$id)->get()[0];
        DB::table('uut_inspection_prices')
        ->where('id',$id)
        ->limit(1)
        ->update([
            'price' => $request['price']
        ]);
        
        return redirect()->route('setting.price');
    }

    public function typeSave(Request $request){
        $id = DB::table('master_standard_types')->max('id');
        $request['id'] = $id + 1;
        unset($request['_token']);
        $request['level'] = 1;
        $request['oiml_id'] = 1;
        $request['oiml_id'] = 1;
        DB::table('master_standard_types')->insert($request->all());
        return redirect()->route('setting.price');
    }

    public function index() 
    {
        $attribute = $this->MyProjects->setup("settings");
        return view('petugasdl.index', compact('attribute'));
    }

    public function index_type() 
    {
        $attribute = $this->MyProjects->setup("Settings Type");
        $types = DB::table("master_standard_types as a")
        ->select("a.*")
        ->get();
        return view('settings/price.index_type', compact(['attribute','types']));
    }

    public function createPrice(){
        $attribute = $this->MyProjects->setup("settings");
        $lab = DB::table('master_laboratory')
            ->where('is_active',true)
            ->where('tipe','standard')
            ->pluck('nama_lab','id');

        $units = DB::table('master_standard_units')
                ->pluck('unit','id');

        $tools = DB::table('master_standard_types')
                ->where('is_active',true)
                ->pluck('uut_type','id');
        
        return view('settings/price.create_price', compact('attribute','lab','units','tools'));
    }
    public function createType(){
        $attribute = $this->MyProjects->setup("settings");
            $lab = DB::table('master_laboratory')
            ->where('is_active',true)
            ->where('tipe','standard')
            ->pluck('nama_lab','id');

        $units = DB::table('master_standard_units')
                ->pluck('unit','id');

        $tools = DB::table('master_standard_types')
                ->where('is_active',true)
                ->pluck('uut_type','id');
                
        return view('settings/price.create_type', compact('attribute','lab','units','tools'));
    }

    public function editTypeSave(Request $request, $id){
        unset($request['_token']);
        unset($request['id']);
        $request['level'] = 1;
        $request['oiml_id'] = 1;
        $request['oiml_id'] = 1;
        DB::table('master_standard_types')->where('id',$id)->update($request->all());
        return redirect()->route('setting.price');
    }

    public function editType($id){
        $attribute = $this->MyProjects->setup("settings");
            $lab = DB::table('master_laboratory')
            ->where('is_active',true)
            ->where('tipe','standard')
            ->pluck('nama_lab','id');

        $units = DB::table('master_standard_units')
                ->pluck('unit','id');

        $tool = DB::table('master_standard_types')
                ->where('id',$id)->first();
        
        return view('settings/price.edit_type', compact('attribute','lab','units','tool'));
    }

}
