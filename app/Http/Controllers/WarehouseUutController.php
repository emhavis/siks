<?php 

namespace App\Http\Controllers;

use App\HistoryUut;
use Illuminate\Http\Request;
use App\ServiceRequestUttpItem;
use App\MyClass\MyProjects;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\ServiceOrders;
use App\ServiceRequest;
use App\ServiceRequestItem;
use App\ServiceRequestItemInspection;

class WarehouseUutController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("warehouseuut");

        $rows = ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
                ->whereHas('ServiceRequestItem', function($query) 
                {
                    $query->where("status_id",13);
                    $query->orWhere("status_id",14);
                })
                ->where('stat_warehouse', 1)
                ->orderBy('staff_entry_datein','desc')->get();
                // ->skip(0)->take(500)
                // ->get();
        // dd($rows);
        return view('warehouseuut.index',compact(['attribute', 'rows']));
    }    

    public function warehouse(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';
        
        if ($request->has('id')) {
            $order = ServiceOrders::find($request->id);
        } else {
            $order = ServiceOrders::whereHas('ServiceRequestItem', function ($query) use ($request) {
                    $query->where('no_order', $request->no_order);
                })->first();
        }

        if ($order != null) {
            $stat = ServiceOrders::whereId($order->id)->update([
                "stat_warehouse"=>2,
                "warehouse_out_id" => Auth::id(),
                "warehouse_out_at" => date("Y-m-d H:i:s.u"),
                "warehouse_out_nama" => $request->warehouse_out_nama,
            ]);

            $history = new HistoryUut();
            $history->request_status_id = 13;
            $history->request_id = $order->service_request_id;
            $history->request_item_id = $order->service_request_item_id;
            $history->order_id = $order->id;
            $history->order_status_id = $order->stat_sertifikat;
            $history->warehouse_status_id = 2;
            $history->user_id = Auth::id();
            $history->save();

            $this->checkAndUpdateFinishOrder($order->id);

            $response["status"] = true;
            $response["messages"] = "Alat telah diterima pemilik/pemohon";
        }
        
        return response($response);
    }

    private function checkAndUpdateFinishOrder($id) 
    {
        $order = ServiceOrders::find($id);

        if ($order->stat_warehouse == 2 && $order->stat_sertifikat == 3) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestItemInspection::where("service_request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequest::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUut();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                //$history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }

    public function getdata(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        if ($request->has('no_order')) {
            $order = ServiceOrders::whereHas('ServiceRequestItem', function ($query) use ($request) {
                    $query->where('no_order', $request->no_order);
                })
                ->first();
        
            $data["tool_brand"] = $order->tool_brand;
            $data["tool_model"] = $order->tool_model;
            $data["tool_serial_no"] = $order->tool_serial_no;
            $data["pemilik"] = $order->ServiceRequest->label_sertifikat;
            $data["pemohon"] = $order->ServiceRequest->requestor->full_name;
            $data["no_order"] = $order->ServiceRequestItem->no_order;

            $response["status"] = true;
            $response["messages"] = 'Alat ditemukan';
            $response["data"] = $data;
        }

        return response($response);
    }

    public function listData(Request $request){
        // Page Length
        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;
        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';
        
        // Main Query
        $callback = function($query) use ($request) {
            if($request->has('search')){
                $sv =strtolower($request->get('search')['value']);
                $query->whereRaw("lower(no_order) like '%".$sv."%'");
            }
            $query->where('status_id', '=', 13)->orWhere("status_id",14);
        };
        $rows = ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
                ->whereHas('ServiceRequestItem',$callback)->with(['uutType','ServiceRequestItem' => $callback,'ServiceRequest','ServiceRequest.items','ServiceRequest.requestor'])
                ->where('stat_warehouse', 1);
        
        
        $query = $rows->orderBy("id", $orderBy);
        $recordsFiltered = $recordsTotal = $query->count();
        $data = $rows->skip($skip)->take($pageLength)->get();

        return response()->json(["draw"=> $request->draw, "recordsTotal"=> $recordsTotal, "recordsFiltered" => $recordsFiltered, 'data' => $data], 200);
    }
}
