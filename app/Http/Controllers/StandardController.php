<?php

namespace App\Http\Controllers;

// use App\Http\Repositories\StandardRepository;
// use App\Http\Repositories\UmlRepository;
// use App\Http\Repositories\NegaraRepository;
// use App\Http\Repositories\SumberRepository;

use App\Standard;
use App\Qrcode;
use App\StandardMeasurementUnit;
use App\StandardMeasurementType;
use App\MasterNegara;
use App\MasterSumber;
use App\MasterUml;
use App\MyClass\MyProjects;
use App\StandardInspectionPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StandardController extends Controller
{
    protected $MasterNegara;
    protected $MasterSumber;
    protected $MasterUml;
    // protected $Standard;
    // protected $Qrcode;
    protected $StandardMeasurementUnit;
    protected $StandardMeasurementType;
    // protected $StandardInspectionPrice;
    protected $MyProjects;

    public function __construct()
    {
        $this->MasterNegara = new MasterNegara();
        $this->MasterSumber = new MasterSumber();
        $this->MasterUml = new MasterUml();
        // $this->Standard = new Standard();
        // $this->Qrcode = new Qrcode();
        $this->MyProjects = new MyProjects();
        $this->StandardMeasurementUnit = new StandardMeasurementUnit();
        $this->StandardMeasurementType = new StandardMeasurementType();
        // $this->StandardInspectionPrice = new StandardInspectionPrice();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("standard");
        if($attribute==null) return userlogout();

        $rows = Standard::get();
        return view('standard.index', compact('rows','attribute'));
    }

    public function create(Request $request, $id=null)
    {
        $attribute = $this->MyProjects->setup('standard');
        $standard = null;

        if($id)
        {
            $standard = Standard::where('standards.id',$id)
            ->first();
        }

        $standardMeasurementTypes = $this->StandardMeasurementType->dropdown();
        $standardMeasurementUnits = $this->StandardMeasurementUnit->dropdown();
        $standardNegara = $this->MasterNegara->dropdown();
        $msumber = $this->MasterSumber->dropdown();
        $umls = $this->MasterUml->dropdown();

        return view('standard.create', compact(
          'standardMeasurementTypes',
          'standardMeasurementUnits',
          'standardNegara',
          "standard",
          'msumber',
          'attribute',
          "id",
          'umls'));
    }

    // public function masterdata(Request $request)
    // {
    //     $standardMeasurementTypes = $this->standardRepo->getStandardMeasurementTypeForDropdown();
    //     return view('standard.masterdata', compact('standardMeasurementTypes'));
    // }

    // public function simpanmasterdata(Request $request)
    // {
    //     try
    //     {
    //         switch($request->oper)
    //         {
    //             case "add":
    //                 if(strlen($request->standard_name)>0)
    //                 {
    //                     switch ($request->type)
    //                     {
    //                         case 'standard_measurement_type_id':
    //                             DB::table("standard_measurement_types")
    //                             ->insert(["standard_type"=>$request->standard_name]);
    //                             echo json_encode(array(true));
    //                             break;
    //                         case 'standard_tool_type_id':
    //                             if(intval($request->standard_measurement_type_id)>0)
    //                             {
    //                                 DB::table("standard_tool_types")
    //                                 ->insert(["attribute_name"=>$request->standard_name,"standard_measurement_type_id"=>$request->standard_measurement_type_id]);
    //                                 echo json_encode(array(true));
    //                             }
    //                             else
    //                             {
    //                                 echo json_encode(array(false,"Standard Measurement tidak boleh kosong"));
    //                             }
    //                             break;
    //                         case 'standard_detail_type_id':
    //                             if(intval($request->standard_tool_type_id)>0)
    //                             {
    //                                 DB::table("standard_detail_types")
    //                                 ->insert(["standard_detail_type_name"=>$request->standard_name,"standard_tool_type_id"=>$request->standard_tool_type_id]);
    //                                 echo json_encode(array(true));
    //                             }
    //                             else
    //                             {
    //                                 echo json_encode(array(false,"Standard Tool tidak boleh kosong"));
    //                             }
    //                             break;
    //                         case 'inspection_price_id':
    //                             if(intval($request->standard_detail_type_id)>0 && intval($request->price)>0)
    //                             {
    //                                 DB::table("standard_inspection_prices")
    //                                 ->insert([
    //                                     "inspection_type"=>$request->standard_name,
    //                                     "standard_detail_type_id"=>$request->standard_detail_type_id,
    //                                     "price"=>$request->price
    //                                 ]);
    //                                 echo json_encode(array(true));
    //                             }
    //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      else
    //                             {
    //                                 echo json_encode(array(false,"Standard Detail tidak boleh kosong. Harga Inspeksi tidak boleh kosong"));
    //                             }
    //                             break;
    //                     }
    //                 }
    //                 else
    //                 {
    //                     echo json_encode(array(false,"Data Kosong. Harap diisi lebih dahulu"));
    //                 }
    //                 break;
    //             case "edit":
    //                 if(strlen($request->standard_name)>0)
    //                 {
    //                     switch ($request->type)
    //                     {
    //                         case 'standard_measurement_type_id':
    //                             DB::table("standard_measurement_types")
    //                             ->where("id",$request->standard_measurement_type_id)
    //                             ->update(["standard_type"=>$request->standard_name]);
    //                             echo json_encode(array(true));
    //                             break;
    //                         case 'standard_tool_type_id':
    //                             if(intval($request->standard_measurement_type_id)>0)
    //                             {
    //                                 DB::table("standard_tool_types")
    //                                 ->where("id",$request->standard_tool_type_id)
    //                                 ->update(["attribute_name"=>$request->standard_name,"standard_measurement_type_id"=>$request->standard_measurement_type_id]);
    //                                 echo json_encode(array(true));
    //                             }
    //                             else
    //                             {
    //                                 echo json_encode(array(false,"Standard Measurement tidak boleh kosong"));
    //                             }
    //                             break;
    //                         case 'standard_detail_type_id':
    //                             if(intval($request->standard_tool_type_id)>0)
    //                             {
    //                                 DB::table("standard_detail_types")
    //                                 ->where("id",$request->standard_detail_type_id)
    //                                 ->update(["standard_detail_type_name"=>$request->standard_name,"standard_tool_type_id"=>$request->standard_tool_type_id]);
    //                                 echo json_encode(array(true));
    //                             }
    //                             else
    //                             {
    //                                 echo json_encode(array(false,"Standard Tool tidak boleh kosong"));
    //                             }
    //                             break;
    //                         case 'inspection_price_id':
    //                             if(intval($request->standard_detail_type_id)>0 && intval($request->price)>0)
    //                             {
    //                                 DB::table("standard_inspection_prices")
    //                                 ->where("id",$request->inspection_price_id)
    //                                 ->update([
    //                                     "inspection_type"=>$request->standard_name,
    //                                     "standard_detail_type_id"=>$request->standard_detail_type_id,
    //                                     "price"=>$request->price
    //                                 ]);
    //                                 echo json_encode(array(true));
    //                             }
    //                             else
    //                             {
    //                                 echo json_encode(array(false,"Standard Detail tidak boleh kosong<br>Harga Inspeksi tidak boleh kosong"));
    //                             }
    //                             break;
    //                     }
    //                 }
    //                 else
    //                 {
    //                     echo json_encode(array(false,"Data Kosong. Harap diisi lebih dahulu"));
    //                 }
    //                 break;
    //             case "del":
    //                 switch ($request->type)
    //                 {
    //                     case 'standard_measurement_type_id':
    //                         $is_exists = DB::table('standard_tool_types')
    //                         ->where('standard_measurement_type_id',$request->standard_measurement_type_id)->exists();
    //                         if($is_exists)
    //                         {
    //                             echo json_encode(array(false,"Measurement type masih ada data di Tool type"));
    //                         }
    //                         else
    //                         {
    //                             DB::table("standard_measurement_types")
    //                             ->where("id",$request->standard_measurement_type_id)
    //                             ->delete();
    //                             echo json_encode(array(true));
    //                         }
    //                         break;
    //                     case 'standard_tool_type_id':
    //                         $is_exists = DB::table('standard_detail_types')
    //                         ->where('standard_tool_type_id',$request->standard_tool_type_id)->exists();
    //                         if($is_exists)
    //                         {
    //                             echo json_encode(array(false,"Tool type masih ada data di Detail type"));
    //                         }
    //                         else
    //                         {
    //                             DB::table("standard_tool_types")
    //                             ->where("id",$request->standard_tool_type_id)
    //                             ->delete();
    //                             echo json_encode(array(true));
    //                         }
    //                         break;
    //                     case 'standard_detail_type_id':
    //                         $is_exists = DB::table('standard_inspection_prices')
    //                         ->where('standard_detail_type_id',$request->standard_detail_type_id)->exists();
    //                         if($is_exists)
    //                         {
    //                             echo json_encode(array(false,"Detail type masih ada data di Inspeksi type"));
    //                         }
    //                         else
    //                         {
    //                             DB::table("standard_detail_types")
    //                             ->where("id",$request->standard_detail_type_id)
    //                             ->delete();
    //                             echo json_encode(array(true));
    //                         }
    //                         break;
    //                     case 'inspection_price_id':
    //                         DB::table("standard_inspection_prices")
    //                         ->where("id",$request->inspection_price_id)
    //                         ->delete();
    //                         echo json_encode(array(true));
    //                         break;
    //                 }
    //                 break;
    //         }

    //     }
    //     catch(Exception $e)
    //     {
    //         echo json_encode(array(false,$e->errorInfo));
    //     }
    // }

    public function store(Request $request)
    {
        $response["status"] = false;

        $rules['uml_id'] = ['required'];
        $rules['uml_sub_id'] = ['required'];
        $rules['sumber_id'] = ['required'];
        // CHECK IF EXISTS IN QRCODE
        // CHECK IF NOT EXISTS IN STANDARDS
        $rules['tool_code'] = ['required','exists:qrcodes,tool_code','unique:standards,tool_code'];
        $rules['standard_measurement_type_id'] = ['required'];
        $rules['standard_tool_type_id'] = ['required'];
        $rules['standard_detail_type_id'] = ['required'];
        $rules['brand'] = ['required'];
        $rules['made_in'] = ['required'];
        $rules['model'] = ['required'];
        $rules['tipe'] = ['required'];
        $rules['no_seri'] = ['required'];
        $rules['no_identitas'] = ['required'];
        $rules['capacity'] = ['required'];
        $rules['daya_baca'] = ['required'];
        $rules['class'] = ['required'];
        $rules['jumlah_per_set'] = ['required','numeric'];

        if($request->id)
        {
            // CHECK IF NOT EXISTS IN STANDARDS EXCEPT ID=190
            $rules['tool_code'] = ['required','exists:qrcodes,tool_code','unique:standards,tool_code,'.$request->id.',id'];
        }

        $validation = Validator::make($request->all(), $rules, error_messages());
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            if($request->id)
            {
                $keys = array_keys($rules);
                $rows = $request->toArray();
                foreach($rows as $k=>$v)
                {
                    if(!in_array($k,$keys))
                    {
                        unset($rows[$k]);
                    }
                }
                unset($rows["uml_id"]);
                unset($rows["uml_sub_id"]);
                unset($rows["standard_measurement_type_id"]);
                
                $rows["uml_id"] = $request->uml_id2;
                $rows["uml_sub_id"] = $request->uml_sub_id2;
                $rows["standard_measurement_type_id"] = $request->standard_measurement_type_id2;

                Standard::whereId($request->id)->update($rows);
            }
            else
            {
                Standard::insert($request);
            }
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    // public function edit($id)
    // {
    //     $attribute = $this->MyProjects->setup();

    //     $standard = $this->Standard
    //     ->leftJoin("uml","uml.id","=","standards.uml_id")
    //     ->leftJoin("uml_sub","uml_sub.id","=","standards.uml_sub_id")
    //     ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","standards.standard_measurement_type_id")
    //     ->leftJoin("standard_tool_types","standard_tool_types.id","=","standards.standard_tool_type_id")
    //     ->leftJoin("standard_detail_types","standard_detail_types.id","=","standards.standard_detail_type_id")
    //     ->where('standards.id',$id)
    //     ->select("standards.*","standard_type","attribute_name","standard_detail_type_name","uml_name","uml_sub_name")
    //     ->get();

    //     $standardNegara = $this->MasterNegara->dropdown();

    //     $msumber = $this->MasterSumber->dropdown();
    //     // $umls = $this->umlRepo->getUMLsForDropDown();

    //     return view('standard.edit', compact(
    //       'standard',
    //       'id',
    //       'standardNegara',
    //       'attribute',
    //       'msumber'
    //     ));
    // }

    // public function update(Request $request)
    // {
    //     try
    //     {
    //         $validation = Validator::make($request->all(), Standard::$rules_update);
    //         if ($validation->passes())
    //         {
    //             $id = $request->id;

    //             $request->request->remove('id');

    //             $this->standardRepo->updateStandard($request, $id);
    //             echo json_encode(array(true,"Data berhasil disimpan"));
    //         }
    //         else
    //         {
    //             echo json_encode(array(false, $validation->messages()));
    //         }
    //     }
    //     catch(Exception $e)
    //     {
    //         echo json_encode(array(false,$e->errorInfo));
    //     }
    // }


// INFO OBJECT

    // public function profilumlinfo(Request $request)
    // {
    //     $profilumlinfo = DB::table('profil_uml')
    //     ->join("uml_sub","uml_sub.profil_uml_id","=","profil_uml.id")
    //     ->where('profil_uml_id',$request->id)
    //     ->select('kode_daerah')
    //     ->get();

    //     return $profilumlinfo;
    // }

    public function inspectionpriceinfo(Request $request)
    {
        // ->join("standard_measurement_units","standard_measurement_units.id","=","standard_inspection_prices.standard_measurement_unit_id")
        // ->select('standard_inspection_prices.id',"inspection_type","price","standard_measurement_unit_id","standard_measurement_units.measurement_unit")
        $rows = StandardInspectionPrice::where('standard_detail_type_id',$request->standard_detail_type_id)
        ->get();
        // dd($rows);

        $i = 0;
        $data = [];
        foreach($rows as $row)
        {
            $data[$i] = $row->toArray();
            $data[$i]["measurement_unit"] = $row->StandardMeasurementUnit->measurement_unit;
            $i++;
        }
        // dd($data);
        return response($data);
    }

    // public function qrcodeinfo(Request $request)
    // {
    //     $umlstandard = DB::table('qrcodes')
    //     ->leftJoin("uml","uml.id","=","qrcodes.uml_id")
    //     ->leftJoin("uml_sub","uml_sub.id","=","qrcodes.uml_sub_id")
    //     ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","qrcodes.standard_measurement_type_id")

    //     ->where('tool_code',$request->tool_code)
    //     ->select("qrcodes.*","uml_name","uml_sub_name","standard_type")
    //     ->get();

    //     echo json_encode($umlstandard);
    // }

    public function info($id)
    {
        // ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","standards.standard_measurement_type_id")
        // ->leftJoin("standard_tool_types","standard_tool_types.id","=","standards.standard_tool_type_id")
        // ->leftJoin("standard_detail_types","standard_detail_types.id","=","standards.standard_detail_type_id")
        $row = Standard::where('tool_code',$id)->first();
            
        if($row)
        {
            $data["id"] = $row->id;
            $data["tool_code"] = $row->tool_code;
            $data["jumlah_per_set"] = $row->jumlah_per_set;
            $data["standard_measurement_type_id"] = $row->standard_measurement_type_id;
            $data["standard_tool_type_id"] = $row->standard_tool_type_id;
            $data["standard_detail_type_id"] = $row->standard_detail_type_id;
            $data["standard_type"] = $row->StandardMeasurementType->standard_type;
            $data["attribute_name"] = $row->StandardToolType->attribute_name;
            $data["standard_detail_type_name"] = $row->StandardDetailType->standard_detail_type_name;

            return response($data);
        }
        else
        {
            return response(["status"=>false]);
        }

    }

    // public function tool_code(Request $request)
    // {
    //     $infos = array();

    //     if(strlen($request->tool_code)>0)
    //     {
    //         $infos = $this->Standard
    //         ->where('tool_code', $request->tool_code)
    //         ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","standards.standard_measurement_type_id")
    //         ->leftJoin("standard_tool_types","standard_tool_types.id","=","standards.standard_tool_type_id")
    //         ->leftJoin("standard_detail_types","standard_detail_types.id","=","standards.standard_detail_type_id")
    //         ->leftJoin("msumber","msumber.id","=","standards.sumber_id")
    //         ->leftJoin("master_negara","master_negara.id","=","standards.made_in")
    //         ->select("standards.*","nama_negara","nama_sumber","standard_type","attribute_name","standard_detail_type_name")
    //         ->get();
    //     }

    //     return response($infos);
    // }

    // public function get_tool_code_info(Request $request)
    // {
    //     $infos = array();

    //     if(strlen($request->tool_code)>0)
    //     {
    //         $infos = DB::table('standards')
    //         ->where('tool_code', $request->tool_code)
    //         ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","standards.standard_measurement_type_id")
    //         ->leftJoin("standard_tool_types","standard_tool_types.id","=","standards.standard_tool_type_id")
    //         ->leftJoin("standard_detail_types","standard_detail_types.id","=","standards.standard_detail_type_id")
    //         ->leftJoin("msumber","msumber.id","=","standards.sumber_id")
    //         ->leftJoin("master_negara","master_negara.id","=","standards.made_in")
    //         ->select("standards.*","nama_negara","nama_sumber","standard_type","attribute_name","standard_detail_type_name")
    //         ->get();
    //     }

    //     echo response($infos);
    // }

}
