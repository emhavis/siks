<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadeRequest;
use App\Http\Repositories\ServiceRequestRepository;
use App\Http\Repositories\ServiceOrderRepository;
use App\Http\Repositories\ServiceTypeRepository;
use App\Http\Repositories\UmlStandardRepository;
use App\Http\Repositories\UmlRepository;
use App\Http\Repositories\IdTypeRepository;
use App\Http\Repositories\StandardRepository;
use App\Utilities\Helper;
use App\ServiceRequest;
use Validator;
use Redirect;
use Session;
use Auth;
use PDF;
use Log;
use Storage;

class ServiceRequestController extends Controller
{
    protected $serviceRequestRepo, $serviceOrderRepo, $serviceTypeRepo, $umlStandardRepo;

    public function __construct(ServiceRequestRepository $serviceRequestRepo, ServiceOrderRepository $serviceOrderRepo, ServiceTypeRepository $serviceTypeRepo, UmlStandardRepository $umlStandardRepo, IdTypeRepository $idTypeRepo, UmlRepository $umlRepo, StandardRepository $standardRepo)
    {
        $this->middleware('auth', ['except' => ['simpan','paymentsave',"get_service_request_items","labin_simpan","stafproses_finish","stafproses_send","kaproses","kaditproses","get_tool_code_info","get_service_orders","get_service_order_outs","get_service_order_finish","get_service_order_out_finish","stafsimpan_result","history"]]);

        $this->serviceRequestRepo = $serviceRequestRepo;
        $this->serviceOrderRepo = $serviceOrderRepo;
        $this->serviceTypeRepo = $serviceTypeRepo;
        $this->umlStandardRepo = $umlStandardRepo;
        $this->idTypeRepo = $idTypeRepo;
        $this->umlRepo = $umlRepo;
        $this->standardRepo = $standardRepo;
    }

    public function index()
    {
        $requests = DB::table("service_requests")
        ->leftJoin("uml","uml.id","=","service_requests.uml_id")
        ->select(
            'no_register',
            'no_order',
            'service_requests.id',
            'total_price',
            'uml_name',
            'pic_name',
            'payment_date',
            'payment_code')
        ->orderBy('receipt_date','desc')
        ->get();

        return view('service.index', compact('requests'));
    }

    public function request($serial_number)
    {
        try
        {
            $umlStandard = $this->umlStandardRepo->getUmlStandardBySerialNumber($serial_number);
            $idTypes = $this->idTypeRepo->getIdTypesForDropdown();

            Session::flash('returnURL', FacadeRequest::fullUrl());
            
        } catch(Exception $e)
        {
            Log::error('Error at opening new service request form with message: ' . $e->errorInfo);
        }

        return view('service.request', compact('umlStandard', 'idTypes'));
    }

    public function create()
    {
        $serviceTypes = $this->serviceTypeRepo->getServiceTypesForDropdown();
        $umls = $this->umlRepo->getUMLsForDropDown();
        $idTypes = $this->idTypeRepo->getIdTypesForDropdown();
            
        Session::flash('returnURL', FacadeRequest::fullUrl());
        
        return view('service.create', compact('serviceTypes', 'idTypes', 'umls'));
    }

    public function simpan(Request $request)
    {
        // $user = Auth::user();
        // var_dump($user);
        // exit;
        if($request->for_sertifikat=="1")
        {        
            $validation = Validator::make($request->all(), [
            'uml_id' => 'required',
            'label_sertifikat' => 'required',
            'addr_sertifikat' => 'required',
            'receipt_date' => 'required',
            'estimate_date' => 'required',
            'pic_name' => 'required',
            'pic_phone_no' => 'required',
            'pic_email' => 'required',
            'id_type_id' => 'required',
            'pic_id_no' => 'required'
            ]);
        }
        else
        {
            $validation = Validator::make($request->all(), [
            'uml_id' => 'required',
            'receipt_date' => 'required',
            'estimate_date' => 'required',
            'pic_name' => 'required',
            'pic_phone_no' => 'required',
            'pic_email' => 'required',
            'id_type_id' => 'required',
            'pic_id_no' => 'required'
            ]);            
        }

        $no_register = DB::table("configs")->where("init","no_register")->value("param_value");

        $no_order = DB::table("configs")->where("init","no_order")->value("param_value");

        // var_dump($request->inspection_price_ids);
        // exit;
        if ($validation->passes())
        {
            $alat = 0;
            // $serviceRequest = $this->serviceRequestRepo->createServiceRequest($request, Auth::user()->id);
            // $serviceRequest = $this->serviceRequestRepo->createServiceRequest($request, $request->user()->id);
            // $serviceRequest = $this->serviceRequestRepo->createServiceRequest($request, Auth::id());
            $noreg_num = intval($no_register)+1;
            $noreg = sprintf("%04d",$noreg_num).date("-m-Y");

            $noorder_num = intval($no_order)+1;
            $noorder = date("y-").sprintf("%04d",$noorder_num)."-";

            $serviceRequest = $this->serviceRequestRepo->createServiceRequest($request,$noreg);

            if(isset($request->inspection_price_ids) && 
                count($request->inspection_price_ids)>0
            )
            {
                $noorder = $noorder.sprintf("%02d",count($request->inspection_price_ids))."-";

                foreach($request->inspection_price_ids as $standard_id=>$inspection_price_ids)
                {
                    $inspection_price_ids = array_unique(explode(",", $inspection_price_ids));

                    $serviceRequestItem = $this->serviceRequestRepo->createServiceRequestItem(
                        $serviceRequest->id, $standard_id, count($inspection_price_ids));

                    foreach($inspection_price_ids as $inspection_price_id)
                    {
                        $alat++;
                        $price = DB::table("standard_inspection_prices")
                        ->where("id",$inspection_price_id)
                        ->value("price");

                        $serviceRequestItemInspection = $this->serviceRequestRepo->createServiceRequestItemInspection(
                            $serviceRequestItem->id, 
                            $inspection_price_id, 
                            $price, 
                            $request->inspection_price_jumlahs[$inspection_price_id]
                        );
                    }
                }
                $noorder = $noorder.sprintf("%03d",$alat);

                DB::table("service_requests")->where("id",$serviceRequest->id)->update(["no_order"=>$noorder]);

                DB::table("configs")->where("init","no_register")->update(["param_value"=>$noreg_num]);

                DB::table("configs")->where("init","no_order")->update(["param_value"=>$noorder_num]);

                echo json_encode(array(true,"success"));
            }
            else
            {
                echo json_encode(array(false,array("standard_code"=>'Minimal Satu Standard Tool wajib diisi')));
            }
        }
        else 
        {
            echo json_encode(array(false,$validation->messages()));
        }
    }

    public function show($id)
    {
        try 
        {
            $helper = new Helper();
            $serviceRequest = $this->serviceRequestRepo->getServiceRequest($id);
            $serviceRequest->receipt_date = $helper->formatDisplayDate($serviceRequest->receipt_date);

        } catch(Exception $e)
        {
            Log::error('Error at opening service request with id ' . $id . ' form with message: ' . $e->errorInfo);
        }

        return view('service.show', compact('serviceRequest'));
    }

    public function edit($id)
    {
        try 
        {
            $helper = new Helper();

            $umls = $this->umlRepo->getUMLsForDropDown();
            $serviceRequest = $this->serviceRequestRepo->getServiceRequest($id);
            $idTypes = $this->idTypeRepo->getIdTypesForDropdown();
            $standards = $this->serviceRequestRepo->getServiceRequestItems($id);

            $serviceRequest->receipt_date = $helper->formatDisplayDate($serviceRequest->receipt_date);

        } catch(Exception $e)
        {
            Log::error('Error at opening service request with id ' . $id . ' form with message: ' . $e->errorInfo);
        }

        return view('service.edit', compact('serviceRequest', 'umls', 'idTypes', 'standards'));
    }

    public function update(Request $request, $id)
    {
        try 
        {
            $validation = Validator::make($request->all(), ServiceRequest::$rules);

            if ($validation->passes())
            {
                $this->serviceRequestRepo->updateServiceRequest($request, $id, Auth::user()->id);
            }
            else 
            {
                return Redirect::back()->withInput()->withErrors($validation->messages());
            }
        }
        catch(Exception $e)
        {
            Log::error('Error at updating service request (id ' . $id . ') with message: ' . $e->errorInfo);

            return Redirect::back()->withInput()->withErrors($e->errorInfo);
        }

        return Redirect::route('service.index');
    }

    public function select($id, $serial_number = null)
    {
        try
        {
            $umlStandard = null;
            $idTypes = $this->idTypeRepo->getIdTypesForDropdown();

            if($serial_number != '')
            {
                $umlStandard = $this->umlStandardRepo->getUmlStandardBySerialNumber($serial_number);
            }

            Session::flash('returnURL', FacadeRequest::fullUrl());
            
        } catch(Exception $e)
        {
            Log::error('Error at opening new service request form with message: ' . $e->errorInfo);
        }

        return view('service.select', compact('id', 'umlStandard'));
    }

    public function search(Request $request)
    {
        try
        {
            $umlStandard = $this->umlStandardRepo->getUmlStandardBySerialNumber($request->serial_number);
            
            if(!$umlStandard)
            {
                return Redirect::back()->withInput()->with("NotFound", "Standard dengan nomor seri <b>" . $request->serial_number . "</b> tidak ditemukan.");
            }
            
        } catch(Exception $e)
        {
            Log::error('Error at opening new service request form with message: ' . $e->errorInfo);
        }

        return Redirect::route('service.select', [$request->request_id, $request->serial_number]);
    }

    public function requestpdf(Request $request,$id)
    {
        $id = $id;

        // $alamat_badan_metrologi = DB::table("profil_")
        $mode = 'register';


        $requests = DB::table("service_requests")
        ->leftJoin("uml","uml.id","=","service_requests.uml_id")
        ->leftJoin("users","users.id","=","service_requests.created_by")
        ->leftJoin("service_request_items","service_requests.id","=","service_request_items.service_request_id")
        ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
        ->leftJoin("uml_sub","uml_sub.id","=","standards.uml_sub_id")
        ->leftJoin("standard_tool_types","standard_tool_types.id","=","standards.standard_tool_type_id")
        ->leftJoin("service_request_item_inspections","service_request_items.id","=","service_request_item_inspections.service_request_item_id")
        ->leftJoin("standard_inspection_prices","standard_inspection_prices.id","=","service_request_item_inspections.standard_inspection_price_id")
        ->where("service_requests.id",$id)
        ->select(
            'service_requests.id',
            'uml_name',
            "uml_sub_name",
            "uml.address",
            "tool_code",
            'attribute_name',
            'receipt_date',
            "jumlah_per_set",
            "qty_inspection",
            "inspection_type",
            "subtotal_price",
            'pic_name',
            'no_register',
            'users.full_name')
        // ->orderBy('receipt_date','desc')
        ->get();


        // $pdf = PDF::loadview('qrcode.qrcode_pdf',compact('qrcode_list'));

        $file_name = 'register_'.$requests[0]->no_register;

        $pdf = PDF::loadview('service.request_pdf',compact('requests','mode'));
        // return view('service.request_pdf',compact('requests'));

        Storage::put('public/pdf/'.$file_name.'.pdf', $pdf->output());

        return $pdf->download($file_name.'.pdf');

        // return $pdf->stream();
    }

    public function orderpdf(Request $request,$id)
    {
        $id = $id;

        // belum dikerjakan
        $mode = 'order';

        $requests = DB::table("service_requests")
        ->leftJoin("uml","uml.id","=","service_requests.uml_id")
        ->leftJoin("users","users.id","=","service_requests.created_by")
        ->leftJoin("service_request_items","service_requests.id","=","service_request_items.service_request_id")
        ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
        ->leftJoin("uml_sub","uml_sub.id","=","standards.uml_sub_id")
        ->leftJoin("standard_tool_types","standard_tool_types.id","=","standards.standard_tool_type_id")
        ->leftJoin("service_request_item_inspections","service_request_items.id","=","service_request_item_inspections.service_request_item_id")
        ->leftJoin("standard_inspection_prices","standard_inspection_prices.id","=","service_request_item_inspections.standard_inspection_price_id")
        ->where("service_requests.id",$id)
        ->select(
            'service_requests.id',
            'uml_name',
            "uml_sub_name",
            "uml.address",
            "tool_code",
            'attribute_name',
            'receipt_date',
            "jumlah_per_set",
            "qty_inspection",
            "inspection_type",
            "subtotal_price",
            'pic_name',
            'no_register',
            'no_order',
            'users.full_name')
        // ->orderBy('receipt_date','desc')
        ->get();


        // $pdf = PDF::loadview('qrcode.qrcode_pdf',compact('qrcode_list'));

        $file_name = 'order_'.$requests[0]->no_order;

        $pdf = PDF::loadview('service.request_pdf',compact('requests','mode'));
        // return view('service.request_pdf',compact('requests'));

        Storage::put('public/pdf/'.$file_name.'.pdf', $pdf->output());

        return $pdf->download($file_name.'.pdf');

        // return $pdf->stream();
    }

    public function additem(Request $request)
    {
        // try
        // {
        //     $newItems = $this->serviceRequestRepo->createServiceRequestItem($request->service_request_id, $request->uml_standard_id);

        // } catch(Exception $e)
        // {
        //     Log::error('Error at adding new standard for service request form with message: ' . $e->errorInfo);
        // }

        if(strtoupper($request->save_button) == 'SIMPAN')
        {
            return Redirect::route('service.edit', $request->service_request_id);
        } else {
            return Redirect::route('service.select', ['id' => $request->service_request_id, 'serial_number' => '']);
        }
    }

    public function payment($id)
    {
        return view('service.payment',compact('id'));
    }

    public function paymentsave(Request $request, $id)
    {
        $validation = Validator::make($request->all(), 
        [
            "payment_date"=>'required',
            "payment_code"=>'required'
        ]);

        if ($validation->passes())
        {
            // $q = DB::table("service_requests")
            // ->leftJoin("service_request_items","service_request_items.service_request_id","=","service_requests.id")
            // ->leftJoin("service_request_item_inspections","service_request_item_inspections.service_request_item_id","=","service_request_items.service_request_item_id")
            // ->where("service_requests.id",$id)
            // ->select()
            // ->get();            

            // 19-xxxx-qrcode-alat

            DB::table("service_requests")
            ->where("id",$id)
            ->update(array(
                "payment_date"=>$request->payment_date,
                "payment_code"=>$request->payment_code,
                "stat_service_request"=>1
            ));

            echo json_encode(array(true,"success"));
        }
        else 
        {
            echo json_encode(array(false,$validation->messages()));
        }
    }

    public function stafproses_result($id)
    {
        $mMetodeUji = DB::table("master_metodeuji")->orderBy('id')->pluck('nama_metode', 'id');
        $mStandardUkuran = DB::table("master_standar_ukuran")->orderBy('id')->select(DB::raw("CONCAT(kategori_standar_ukuran,' - ',nama_standar_ukuran) AS nama_standar_ukuran"), 'id')->pluck('nama_standar_ukuran','id');
        $serviceOrders = DB::table("service_orders")
        ->where("service_request_item_id",$id)
        ->get();

        $template_format_item = DB::table("mtmp_report")
        ->where("laboratory_id",Auth::user()->laboratory_id)
        ->pluck('title_tmp', 'id');

        $template_rows = DB::table("mtmp_report")
        ->where("laboratory_id",Auth::user()->laboratory_id)
        ->select("header_tmp","column_tmp","id")->get();

        $template_header = $template_setting = array();
        foreach ($template_rows as $row)
        {
            if($row->header_tmp!=NULL) $template_header[$row->id] = json_decode($row->header_tmp,true);
            if($row->column_tmp!=NULL) $template_setting[$row->id] = json_decode($row->column_tmp,true);
        }

        $template_header = json_encode($template_header);
        $template_setting = json_encode($template_setting);

        return view('service.stafproses_result',compact('mMetodeUji','id','serviceOrders','mStandardUkuran','template_format_item','template_header','template_setting'));
    }

    public function stafsimpan_result(Request $request, $id)
    {
        $validation = Validator::make($request->all(),
        [
            'metodeuji_id'=>'required',
            'standar_ukuran_id'=>'required',
            'cakupan'=>'required',
            'tertelusur'=>'required',
            'referensi'=>'required',
            'calibrator'=>'required',
            'suhu_ruang'=>'required',
            'lembab_ruang'=>'required'
        ]);
        // var_dump($request->all());
        if ($validation->passes())
        {
            $hasil_uji = array();
            $report_setting = array();
            for($i=1;$i<=10;$i++)
            {
                $element = "hot_".$i;
                $element_setting = "merge_".$i;
                if($request->has($element)){
                    $hasil_uji[$element] = json_decode($request->$element,true);
                    $report_setting[$element_setting] = json_decode($request->$element_setting,true);
                    foreach ($hasil_uji[$element] as $k => $v)
                    {
                        foreach ($v as $k2=>$v2)
                        {
                            $hasil_uji[$element][$k][$k2] = $this->if_null_quote($v2);
                        }
                    }

                } 

            }
            // if($request->has('hot_2')) $hasil_uji['hot_2'] = json_decode($request->hot_2,true);
            // if($request->has('hot_3')) $hasil_uji['hot_3'] = json_decode($request->hot_3,true);
            // if($request->has('hot_4')) $hasil_uji['hot_4'] = json_decode($request->hot_4,true);
            // if($request->has('hot_5')) $hasil_uji['hot_5'] = json_decode($request->hot_5,true);
            // if($request->has('hot_6')) $hasil_uji['hot_6'] = json_decode($request->hot_6,true);
            // if($request->has('hot_7')) $hasil_uji['hot_7'] = json_decode($request->hot_7,true);
            // if($request->has('hot_8')) $hasil_uji['hot_8'] = json_decode($request->hot_8,true);
            // if($request->has('hot_9')) $hasil_uji['hot_9'] = json_decode($request->hot_9,true);

            $hasil_uji = json_encode($hasil_uji);
            $report_setting = json_encode($report_setting);

            DB::table("service_orders")
            ->where("id",$id)
            ->update([
                'metodeuji_id'=>$request->metodeuji_id,
                'standar_ukuran_id'=>$request->standar_ukuran_id,
                'cakupan'=>$request->cakupan,
                'tertelusur'=>$request->tertelusur,
                'referensi'=>$request->referensi,
                'calibrator'=>$request->calibrator,
                'suhu_ruang'=>$request->suhu_ruang,
                'lembab_ruang'=>$request->lembab_ruang,
                'remark_sertifikat'=>$request->remark_sertifikat,
                'tmp_report_id'=>$request->tmp_report_id,
                'hasil_uji'=>$hasil_uji,
                'report_setting'=>$report_setting
            ]);

            echo json_encode(array(true,"Data berhasil disimpan"));
        }
        else 
        {
            echo json_encode(array(false,$validation->messages()));
        }
    }

    private function if_null_quote($var)
    {
        if($var==null) return "";
        else return $var;
    }

    public function labin_simpan($service_request_item_id,$service_request_id)
    {
        DB::table("service_orders")
        ->insert([
            "laboratory_id"=>Auth::user()->laboratory_id,
            "service_request_id"=>$service_request_id,
            "service_request_item_id"=>$service_request_item_id,
            "lab_staff"=>Auth::id(),
            "staff_entry_datein"=>date("Y-m-d")
        ]);

        DB::table("service_request_items")
        ->where("id",$service_request_item_id)
        ->update(["stat_service_request_item"=>1]);

        return Redirect::route('service.stafindex');
    }

/*
LABORATORY OUT
===============
*/

    public function stafindex()
    {
        $userLabName = DB::table("master_laboratory")->where("id",Auth::user()->laboratory_id)->value("nama_lab");

        $labins = DB::table("service_request_items")
        ->leftJoin("service_requests","service_requests.id","=","service_request_items.service_request_id")
        ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
        ->where("stat_service_request_item",0)
        ->select(
            'service_request_items.id as service_request_item_id',
            'service_requests.id as service_request_id',
            'no_order',
            'tool_code',
            'service_requests.receipt_date',
            'stat_service_request_item'
            )
        ->orderBy('receipt_date','asc')
        ->get();

        return view('service.stafindex',compact('labins','userLabName'));
    }

    public function stafproses()
    {
        $userLabName = DB::table("master_laboratory")->where("id",Auth::user()->laboratory_id)->value("nama_lab");

        $labouts = DB::table("service_orders")
        ->leftJoin("service_request_items","service_request_items.id","=","service_orders.service_request_item_id")
        ->leftJoin("service_requests","service_requests.id","=","service_orders.service_request_id")
        ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
        ->leftJoin("users as a","a.id","=","service_orders.lab_staff")
        ->leftJoin("users as b","b.id","=","service_orders.lab_staff_out")
        ->whereIn("stat_service_order",[0,1])
        ->where("is_finish",0)
        ->select(
            'a.full_name',
            'b.full_name as full_name_out',
            'staff_entry_datein',
            'staff_entry_dateout',
            'no_order',
            'tool_code',
            'is_finish',
            'service_request_item_id',
            'stat_service_order',
            'service_orders.id'
            )
        ->orderBy('staff_entry_datein','desc')
        ->get();

        return view('service.stafproses',compact('labouts','userLabName'));
    }

    public function stafproses_finish($service_request_item_id)
    {
        DB::table("service_orders")
        ->where("laboratory_id",Auth::user()->laboratory_id)
        ->where("service_request_item_id",$service_request_item_id)
        ->update([
            "stat_service_order"=>1,
            "lab_staff_out"=>Auth::id(),
            "staff_entry_dateout"=>date("Y-m-d")
        ]);

        DB::table("service_request_items")
        ->where("id",$service_request_item_id)
        ->update([
            "stat_service_request_item"=>2
        ]);

        return Redirect::route('service.stafproses');
    }

    public function stafproses_send($service_order_id)
    {
        DB::table("service_orders")
        ->where("id",$service_order_id)
        ->update(["is_finish"=>1]);
        return Redirect::route('service.stafproses');
    }

    public function stafhistory()
    {
        $userLabName = DB::table("master_laboratory")->where("id",Auth::user()->laboratory_id)->value("nama_lab");

        $dlOrders = DB::table("service_orders")
        ->leftJoin("service_request_items","service_request_items.id","=","service_orders.service_request_item_id")
        ->leftJoin("service_requests","service_requests.id","=","service_orders.service_request_id")
        ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
        ->leftJoin("users as a","a.id","=","service_orders.lab_staff")
        ->leftJoin("users as b","b.id","=","service_orders.lab_staff_out")
        // ->whereIn("stat_service_order",[0,1])
        ->where("is_finish",1)
        ->select(
            'a.full_name',
            'b.full_name as full_name_out',
            'staff_entry_datein',
            'staff_entry_dateout',
            'no_order',
            'tool_code',
            'is_finish',
            'service_request_item_id',
            'stat_service_order',
            'service_orders.id'
            )
        ->orderBy('staff_entry_dateout','desc')
        ->get();

        return view('service.stafhistory',compact('dlOrders','userLabName'));
    }
/*
ORDER LABHEAD APPROVE LIST
==================
*/

    public function kaindex()
    {
        $userLabName = DB::table("master_laboratory")->where("id",Auth::user()->laboratory_id)->value("nama_lab");

        $data_list = DB::table("service_orders")
        ->leftJoin("service_request_items","service_request_items.id","=","service_orders.service_request_item_id")
        ->leftJoin("service_requests","service_requests.id","=","service_orders.service_request_id")
        ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
        ->leftJoin("users as a","a.id","=","service_orders.lab_staff")
        ->leftJoin("users as b","b.id","=","service_orders.lab_staff_out")
        ->leftJoin("users as c","c.id","=","service_orders.supervisor_staff")
        ->where("stat_service_order",1)
        ->select(
            'a.full_name',
            'b.full_name as full_name_out',
            'c.full_name as supervisor_name',
            'staff_entry_datein',
            'staff_entry_dateout',
            'supervisor_entry_date',
            'no_order',
            'tool_code',
            'is_finish',
            'stat_service_order',
            'service_orders.id'
            )
        ->orderBy('staff_entry_dateout','desc')
        ->get();

        return view('service.kaindex',compact('data_list','userLabName'));
    }

    public function kaproses($service_order_id)
    {
        DB::table("service_orders")
        ->where("id",$service_order_id)
        ->update([
            "stat_service_order"=>2,
            "supervisor_staff"=>Auth::id(),
            "supervisor_entry_date"=>date("Y-m-d")
        ]);

        return Redirect::route('service.kaindex');
    }

    public function kahistory()
    {
        $userLabName = DB::table("master_laboratory")->where("id",Auth::user()->laboratory_id)->value("nama_lab");

        $dlOrders = DB::table("service_orders")
        ->leftJoin("service_request_items","service_request_items.id","=","service_orders.service_request_item_id")
        ->leftJoin("service_requests","service_requests.id","=","service_orders.service_request_id")
        ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
        ->leftJoin("users as a","a.id","=","service_orders.lab_staff")
        ->leftJoin("users as b","b.id","=","service_orders.lab_staff_out")
        ->leftJoin("users as c","c.id","=","service_orders.supervisor_staff")
        ->where("stat_service_order","<>",1)
        ->select(
            'a.full_name',
            'b.full_name as full_name_out',
            'c.full_name as supervisor_name',
            'staff_entry_datein',
            'staff_entry_dateout',
            'supervisor_entry_date',
            'no_order',
            'tool_code',
            'is_finish',
            'stat_service_order',
            'service_orders.id'
            )
        ->orderBy('supervisor_entry_date','desc')
        ->get();

        return view('service.kahistory',compact('dlOrders','userLabName'));
    }

    public function kaditindex()
    {
        // $userLabName = DB::table("master_laboratory")->where("id",Auth::user()->laboratory_id)->value("nama_lab");
        $data_list = DB::table("service_orders")
        ->leftJoin("service_request_items","service_request_items.id","=","service_orders.service_request_item_id")
        ->leftJoin("service_requests","service_requests.id","=","service_orders.service_request_id")
        ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
        ->leftJoin("users as a","a.id","=","service_orders.lab_staff")
        ->leftJoin("users as b","b.id","=","service_orders.lab_staff_out")
        ->leftJoin("users as c","c.id","=","service_orders.supervisor_staff")
        ->leftJoin("users as d","d.id","=","service_orders.ditmet_staff")
        ->where("stat_service_order",2)
        ->select(
            'a.full_name',
            'b.full_name as full_name_out',
            'c.full_name as supervisor_name',
            'd.full_name as ditmet_name',
            'staff_entry_datein',
            'staff_entry_dateout',
            'supervisor_entry_date',
            'ditmet_entry_date',
            'no_sertifikat',
            'no_order',
            'tool_code',
            'is_finish',
            'stat_service_order',
            'service_orders.id'
            )
        ->orderBy('supervisor_entry_date','desc')
        ->get();

        return view('service.kaditindex',compact('data_list'));
    }

    public function kaditproses($service_order_id)
    {
        $no_sertifikat = DB::table("configs")->where("init","no_sertifikat")->value("param_value");

        $sertifikat = sprintf("%04d",$no_sertifikat).' / PKTN.4.7 / 01 / 2019';

        DB::table("service_orders")
        ->where("id",$service_order_id)
        ->update([
            "stat_service_order"=>3,
            "ditmet_staff"=>Auth::id(),
            "ditmet_entry_date"=>date("Y-m-d"),
            "no_sertifikat"=>$sertifikat
        ]);


        DB::table("configs")->where("init","no_sertifikat")->update(["param_value"=>($no_sertifikat+1)]);

        return Redirect::route('service.kaditindex');
    }


    public function kadithistory()
    {
        // $userLabName = DB::table("master_laboratory")->where("id",Auth::user()->laboratory_id)->value("nama_lab");
        $data_list = DB::table("service_orders")
        ->leftJoin("service_request_items","service_request_items.id","=","service_orders.service_request_item_id")
        ->leftJoin("service_requests","service_requests.id","=","service_orders.service_request_id")
        ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
        ->leftJoin("users as a","a.id","=","service_orders.lab_staff")
        ->leftJoin("users as b","b.id","=","service_orders.lab_staff_out")
        ->leftJoin("users as c","c.id","=","service_orders.supervisor_staff")
        ->leftJoin("users as d","d.id","=","service_orders.ditmet_staff")
        ->where("stat_service_order","<>",2)
        ->select(
            'a.full_name',
            'b.full_name as full_name_out',
            'c.full_name as supervisor_name',
            'd.full_name as ditmet_name',
            'staff_entry_datein',
            'staff_entry_dateout',
            'supervisor_entry_date',
            'ditmet_entry_date',
            'no_sertifikat',
            'no_order',
            'tool_code',
            'is_finish',
            'stat_service_order',
            'service_orders.id'
            )
        ->orderBy('supervisor_entry_date','desc')
        ->get();

        return view('service.kadithistory',compact('data_list'));
    }

// ================

    public function get_service_orders(Request $request)
    {
        $infos = array();

        if(intval($request->laboratory_id)>0)
        {
            $infos = DB::table('service_orders')
            ->leftJoin("service_request_items","service_request_items.id","=","service_orders.service_request_item_id")
            ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
            ->where('laboratory_id', $request->laboratory_id)
            ->where('stat_service_order', 0)
            ->select('tool_code', 'service_orders.id')
            ->orderBy('id')
            ->get();
        }

        echo json_encode($infos);
    }    

    public function get_tool_code_info(Request $request)
    {
        $infos = array();

        if(strlen($request->tool_code)>0)
        {
            $infos = DB::table('standards')
            ->where('tool_code', $request->tool_code)
            ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","standards.standard_measurement_type_id")
            ->leftJoin("standard_tool_types","standard_tool_types.id","=","standards.standard_tool_type_id")
            ->leftJoin("standard_detail_types","standard_detail_types.id","=","standards.standard_detail_type_id")
            ->leftJoin("msumber","msumber.id","=","standards.sumber_id")
            ->leftJoin("master_negara","master_negara.id","=","standards.made_in")
            ->select("standards.*","nama_negara","nama_sumber","standard_type","attribute_name","standard_detail_type_name")
            ->get();
        }

        echo json_encode($infos);
    }    

    public function get_service_order_outs(Request $request)
    {
        $infos = array();

        if(intval($request->laboratory_id)>0)
        {
            $infos = DB::table('service_orders')
            ->leftJoin("service_request_items","service_request_items.id","=","service_orders.service_request_item_id")
            ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
            ->where('laboratory_id', $request->laboratory_id)
            ->where('stat_service_order', 1)
            ->select('tool_code', 'service_orders.id')
            ->orderBy('id')
            ->get();
        }

        echo json_encode($infos);
    }

    public function get_service_order_out_finish(Request $request)
    {
        $infos = array();

        if(intval($request->laboratory_id)>0)
        {
            $infos = DB::table('service_orders')
            ->leftJoin("service_request_items","service_request_items.id","=","service_orders.service_request_item_id")
            ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
            ->where('laboratory_id', $request->laboratory_id)
            ->where('stat_service_order','<>', 0)
            ->where('is_finish', 0)
            ->select('tool_code', 'service_orders.id')
            ->orderBy('id')
            ->get();
        }

        echo json_encode($infos);
    }

    public function get_service_order_finish(Request $request)
    {
        $infos = array();

        if(intval($request->laboratory_id)>0)
        {
            $infos = DB::table('service_orders')
            ->leftJoin("service_request_items","service_request_items.id","=","service_orders.service_request_item_id")
            ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
            ->where('laboratory_id', $request->laboratory_id)
            ->where('stat_service_order', 2)
            ->select('tool_code', 'service_orders.id')
            ->orderBy('id')
            ->get();
        }

        echo json_encode($infos);
    }    

    public function sertifikat_pdf($id)
    {

        $data =  DB::table("service_orders")
        ->where('service_orders.id',$id)
        ->leftJoin("service_requests","service_orders.service_request_id","=","service_requests.id")
        ->leftJoin("service_request_items","service_requests.id","=","service_request_items.service_request_id")
        ->leftJoin("service_request_item_inspections","service_request_items.id","=","service_request_item_inspections.service_request_item_id")
        ->leftJoin("standards","standards.id","=","service_request_items.uml_standard_id")
        ->leftJoin("standard_tool_types","standard_tool_types.id","=","standards.standard_tool_type_id")
        ->leftJoin("profil_uml","profil_uml.id","=","standards.uml_id")
        ->leftJoin("master_laboratory","master_laboratory.id","=","service_orders.laboratory_id")
        // ->select("attribute_name")
        ->first();

        $metodeUji = DB::table("master_metodeuji")
        ->whereIn("id",explode(",", $data->metodeuji_id))
        ->get();

        $standarUkuran = DB::table("master_standar_ukuran")
        ->whereIn("id",explode(",", $data->standar_ukuran_id))
        ->get();

        $mtmp_report = DB::table("mtmp_report")
        ->whereIn("id",explode(",", $data->tmp_report_id))
        ->get();
        

        $file_name = str_replace(" / ", "", $data->no_sertifikat);
        $file_name = str_replace(" ", "", $file_name);
        $file_name = str_replace(".", "", $file_name);
        // $pdf = App::make('dompdf');
        // $pdf = App::make('dompdf.wrapper');
        $pdf = PDF::loadview('service.sertifikat_pdf',compact('data','metodeUji','standarUkuran','mtmp_report'));
        // $file_name = str_replace(" / ", "_", $data->no_sertifikat);

        // Storage::put('public/pdf/'.$file_name.'.pdf', $pdf->output());

        // return $pdf->download($file_name.'.pdf');
        $pdf->setPaper('A4', 'portrait');
        return $pdf->stream();
    }
}
