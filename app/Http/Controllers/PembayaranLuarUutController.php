<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ServiceRequest;
use App\ServiceRequestItem;
use App\ServiceRequestItemInspection;
use App\ServiceOrderUut;

use App\Uttp;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\MyClass\MyProjects;
use App\MasterServiceType;
use App\MasterDocTypes;
use App\MasterDocNumber;
use App\MasterInstalasi;
use App\MasterNegara;

use App\ServiceBooking;
use App\ServiceBookingItem;
use App\ServiceBookingItemInspection;
use App\ServiceBookingItemTTUPerlengkapan;

use App\Customer;

use App\UttpInspectionPrice;
use App\Holiday;

use App\ServiceRequestUutStaff;
use App\ServiceRequestUutInsituDoc;
use App\ServiceRequestUutInsituStaff;
use App\ServiceRequestUutItemTTUPerlengkapan;

use App\Mail\Invoice;
use App\Mail\Receipt;
use App\Mail\BookingConfirmation;
use Illuminate\Support\Facades\Mail;

use App\HistoryUut;

use App\Jobs\ProcessEmailJob;

//use App\MasterServiceTypeDocNumber;

use PDF;

class PembayaranLuarUutController extends Controller
{
    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        $this->ServiceRequestUttp = new ServiceRequest();
        $this->ServiceRequestUttpItem = new ServiceRequestItem();
     }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("pembayaranluaruut");

        $rows_penagihan_luar = ServiceRequest::with(['spuhDoc'])
            ->whereIn('status_id', [5,15,18,12,13])
            ->where('lokasi_pengujian', 'luar')
            //->where('payment_status_id', 5)
            ->where(function($query)
                {
                    $query->where(function($q1) {
                        $q1->where('payment_status_id', 5);
                            //->orWhereNull('payment_status_id');
                    });
                    
                })
            ->get();

        $rows_pembayaran_luar = ServiceRequest::with(['spuhDoc'])
            ->whereIn('status_id', [5,15,18,12,13])
            ->where('lokasi_pengujian', 'luar')
            //->where('payment_status_id', 5)
            ->where(function($query)
                {
                    $query->where(function($q1) {
                        $q1->where('payment_status_id', 6);
                            //->orWhereNull('payment_status_id');
                    });
                    
                })
            ->get();

        $rows_validasi_luar = ServiceRequest::with(['spuhDoc'])
            ->whereIn('status_id', [12,13,15,20])
            ->where('lokasi_pengujian', 'luar')
            //->where('payment_status_id', 7)
            ->where(function($query)
                {
                    $query->where(function($q1) {
                        $q1->where('payment_status_id', 7)
                            ->whereNull('payment_valid_date');
                            //->orWhereNull('payment_status_id');
                    });
                })
            ->get();

        
        return view('pembayaranluaruut.index', compact(
            'rows_penagihan_luar',
            'rows_pembayaran_luar',
            'rows_validasi_luar',
            'attribute'));
    }
}