<?php

namespace App\Http\Controllers;

use App\MasterUsers;
use App\MasterRoles;
use App\MasterLaboratory;
use App\MasterUml;
use App\MyClass\MyProjects;
use App\MasterPetugasUttp;
use App\MasterPetugasUut;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    private $MasterUsers;
    private $MasterRoles;
    private $MasterLaboratory;
    private $MasterUml;
    private $MyProjects;

    public function __construct() 
    {
        $this->MasterUsers = new MasterUsers();
        $this->MasterRoles = new MasterRoles();
        $this->MasterLaboratory = new MasterLaboratory();
        $this->MasterUml = new MasterUml();
        $this->MyProjects = new MyProjects();
        $this->PetugasUttp = new MasterPetugasUttp();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("user");
        if($attribute==null) return userlogout();
        
        $users = $this->MasterUsers->get();
        return view('user.index', compact('users','attribute'));
    }

    public function create($id=null) 
    {
        $attribute = $this->MyProjects->setup("user");
        if($attribute==null) return userlogout();

        $mUml = $this->MasterUml->where("id","<>",210)->pluck('uml_name','id');
        $mRole = $this->MasterRoles->pluck('role_name','id');
        $mLabUttp = $this->MasterLaboratory
            ->select(
            DB::raw("CONCAT(nama_lab,' (', upper(tipe), ')') AS nama_lab"),'id')
            ->where('is_active', true)
            ->where('tipe', 'uttp')
            ->pluck('nama_lab','id');
        $mLabSnsu = $this->MasterLaboratory
            ->select(
            DB::raw("CONCAT(nama_lab,' (', upper(tipe), ')') AS nama_lab"),'id')
            ->where('is_active', true)
            ->where('tipe', 'standard')
            ->pluck('nama_lab','id');
        $mPetugasUttp = $this->PetugasUttp
            ->where('is_active', true)
            ->where('flag_unit', 'uttp')
            ->pluck('nama', 'id');
        $mPetugasSnsu = $this->PetugasUttp
            ->where('is_active', true)
            ->where('flag_unit', 'snsu')
            ->pluck('nama', 'id');

        $row = null;
        if($id)
        {
            $row = $this->MasterUsers->whereId($id)->first();
        }

        return view('user.create',compact([
            'mRole','mLabUttp',"mLabSnsu", "mUml",'mPetugasUttp','mPetugasSnsu',
            'row','id','attribute'
        ]));
    }

    public function store(Request $request)
    {
        $response["status"] = false;

        $rules["full_name"] = ['required'];
        $rules["user_role"] = ['required'];
        $rules["laboratory_id"] = ['required_if:user_role,3'];
        $rules["uml_id"] = ['required_if:user_role,7'];
        $rules["username"] = ['required'];
        $rules["email"] = ['required','email','unique:users,email'];
        
        if($request->has("id"))
        {
            $rules["username"] = ['required',Rule::unique('users')->ignore($request->id)];
            $rules["email"] = ['required','email',Rule::unique('users')->ignore($request->id)];
        }


        $validation = Validator::make($request->all(),$rules,error_messages());
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->MasterUsers->whereId($id)->update($request->all());
            }
            else
            {
                $request["password"] = bcrypt("12345678");
                $request["created_at"] = date("Y-m-d H:i:s");
                $this->MasterUsers->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = true;
        switch ($request->action) 
        {
            case 'delete':
                $this->MasterUsers->whereId($request->id)->delete();
                break;
            case 'aktif':
                $this->MasterUsers->whereId($request->id)->update(["is_active"=>"t"]);
                break;
            case 'nonaktif':
                $this->MasterUsers->whereId($request->id)->update(["is_active"=>null]);
                break;
        }

        return response($response);
    }

}
