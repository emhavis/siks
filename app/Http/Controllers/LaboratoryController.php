<?php

namespace App\Http\Controllers;

use App\MasterLaboratory;
use App\MasterInstalasi;
use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LaboratoryController extends Controller
{
    private $MasterLaboratory;
    private $MasterInstalasi;
    private $MyProjects;

    public function __construct() 
    {
        $this->MasterLaboratory = new MasterLaboratory();
        $this->MasterInstalasi = new MasterInstalasi();
        $this->MyProjects = new MyProjects();
    }

    public function index() 
    {
        $attribute = $this->MyProjects->setup("laboratory");

        $laboratory = $this->MasterLaboratory->get();
        return view('laboratory.index', compact('laboratory','attribute'));
    }

    public function create($id=null) 
    {
        $attribute = $this->MyProjects->setup("laboratory");

        $row = null;

        if($id)
        {
            $row = $this->MasterLaboratory->whereId($id)->first();
        }

        return view('laboratory.create',compact('row','id','attribute'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["nama_lab"] = ['required'];
        $rules["quota_online"] = ['required', 'integer', 'min:0'];
        $rules["quota_offline"] = ['required', 'integer', 'min:0'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->MasterLaboratory->whereId($id)->update($request->all());
            }
            else
            {
                $request["is_active"] = "t";
                $request["created_at"] = date("Y-m-d H:i:s");
                $this->MasterLaboratory->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->MasterLaboratory->find($request->id);

        if(count($row->MasterUsers)>0)
        {
            $response["message"] = "Nama Laboratorium sudah terpakai di User List";
        }
        else
        {
            if($request->action=="delete")
            {
                $row->delete();
                $response["status"] = true;
            }
        }

        return response($response);
    }

    public function getbyid($id)
    {
        $row = $this->MasterLaboratory->find($id);
        $response["lab"] = $row;
        
        $rowsInstalasi = $this->MasterInstalasi->where("lab_id", $id)->get();
        $response["instalasi"] = $rowsInstalasi;

        return response($response);
    }
}
