<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use App\ServiceRequestUttp;
use App\Customer;
use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\UttpInsituCheckItem;
use App\ServiceRequestUttpInsituDocCheck;
use App\HistoryUttp;
use App\ServiceRequestUttpItem;
use App\ServiceRequestUttpItemInspection;
use App\MasterUttpType;
use App\Uttp;
use App\MasterServiceType;
use App\ServiceOrderUttps;
use App\ServiceRequestUttpItemTTUPerlengkapan;
use App\ServiceOrderUttpTTUPerlengkapan;
use App\MasterUttpUnit;
use App\ServiceOrderUttpInspections;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ServiceProcessLuarUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();

        $this->ServiceRequestUttp = new ServiceRequestUttp();
        $this->ServiceRequestUttpItem = new ServiceRequestUttpItem();
        $this->ServiceRequestUttpItemInspection = new ServiceRequestUttpItemInspection();
        
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $petugas_id = Auth::user()->petugas_uttp_id;
        if ($petugas_id == null) {
            $petugas_id = Auth::user()->petugas_uut_id;
        }
        
        $rows = ServiceRequestUttp::where('status_id', 12)
            ->where(function($query) {
                $query->whereIn('status_revisi_spt', [0,3])
                    ->orWhereNull('status_revisi_spt');
            })
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->where('service_request_uttp_insitu_staff.scheduled_id', $petugas_id)
            ->select('service_request_uttps.*')
            ->orderBy('received_date','desc')->get();

        return view('serviceprocessluaruttp.index',compact('rows','attribute'));
    }

    public function check($id) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        return view('serviceprocessluaruttp.check', compact(['request', 'requestor', 'doc', 'staffs',  'attribute']));
    }

    public function downloadBukti($docId)
    {
        $doc = ServiceRequestUttpInsituDoc::find($docId);
        
        return Storage::disk('public')->download($doc->path_bukti_pemeriksaan_awal, $doc->file_bukti_pemeriksaan_awal);
    }

    public function savecheck($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("firstcheckluaruttp");

        $serviceRequest = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        $rules = [];
        if ($request->get('submit_val') == 1 && $doc->path_bukti_pemeriksaan_awal == null) {
           // $rules['file_bukti_pemeriksaan_awal'] = ['required','mimes:pdf,jpg,jpeg,png'];
        } else {
           // $rules['file_bukti_pemeriksaan_awal'] = ['mimes:pdf,jpg,jpeg,png'];
        }
        

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();
        
        if ($validation->passes())
        {
            
            if ($request->get('submit_val') == 1)
            {
                $status = 13;
                $serviceRequest->update([
                    "status_id" => $status,
                    "payment_status_id"=>5,
                ]);

                $doc->update([
                    "checked_at" => date("Y-m-d H:i:s.u"),
                    "checked_by" => Auth::id(),
                ]);

                //$items = ServiceRequestUttpItem::where("request_id", $id)->get();
                // CREATE ORDERS
                $items = ServiceRequestUttpItem::where("request_id", $id)->orderBy('id', 'asc')->get();

                $serviceType = MasterServiceType::find($serviceRequest->service_type_id);
                //$no_order = $serviceType->last_order_no;
                $no_order = $serviceRequest->no_order;

                $prefix = $serviceType->prefix;

                $alat = count($items);
                $noorder_num = intval($no_order)+1;
                //$noorder = $prefix.'-'.date("y-").sprintf("%04d",$noorder_num)."-";
                $noorder = $no_order . '-';
                //$nokuitansi = $prefix.'-'.date("y-").sprintf("%04d",$noorder_num);
                $nokuitansi = $no_order;

                ServiceRequestUttp::whereId($id)
                ->update([
                    //"spuh_spt"=>$spuh_spt,
                    "status_id"=>$status,
                    //"no_order"=>$nokuitansi,
                ]);

                $no = 0;
                foreach($items as $item) {
                    $no = $no + 1;
                    $noorder_alat = $noorder.sprintf("%03d",$no);

                    $has_set = false;
                    $is_skhpt = false;
            
                    $only_pemeriksaan = false;
                    
                    if (count($item->inspections) == 1 && $item->inspections[0]->inspectionPrice->is_pemeriksaan == true) {
                        $is_skhpt = true;
                        $only_pemeriksaan = true;
                    }

                    ServiceOrderUttps::insert([
                        "location"              => $item->location,

                        "laboratory_id"         => $item->uttp->type->instalasi->lab_id,
                        "instalasi_id"          => $item->uttp->type->instalasi_id,
                        "service_request_id"    => $item->request_id,
                        "service_request_item_id"=> $item->id,
                        //"service_request_item_inspection_id"=>$inspection->id,
                        //"lab_staff"=>Auth::id(),
                        //"staff_entry_datein"=>date("Y-m-d")
                        'tool_type_id'			=> $item->uttp->type_id,
                        'tool_serial_no'		=> $item->uttp->serial_no,
                        'tool_brand'			=> $item->uttp->tool_brand,
                        'tool_model'			=> $item->uttp->tool_model,
                        'tool_type'				=> $item->uttp->tool_type,
                        'tool_capacity'			=> $item->uttp->tool_capacity,
                        'tool_capacity_unit'	=> $item->uttp->tool_capacity_unit,
                        'tool_factory'			=> $item->uttp->tool_factory,
                        'tool_factory_address'	=> $item->uttp->tool_factory_address,
                        'tool_made_in'			=> $item->uttp->tool_made_in,
                        'tool_made_in_id'		=> $item->uttp->tool_made_in_id,
                        'tool_owner_id'			=> $item->uttp->owner_id,
                        'tool_media'			=> $item->uttp->tool_media,
                        'tool_name'				=> $item->uttp->tool_name,
                        'tool_capacity_min'		=> $item->uttp->tool_capacity_min,

                        'location_lat'          => $item->uttp->location_lat,
                        'location_long'         => $item->uttp->location_long,
                        'location_alat'         => $item->location_alat,

                        'uttp_id'               => $item->uttp_id,

                        "stat_service_order"    => 1,
                        'staff_entry_datein'    => $doc->date_from,
                        "staff_entry_dateout"   => $doc->date_to,
                        "mulai_uji"             => $doc->date_from,
                        "selesai_uji"           => $doc->date_to,
                        "stat_warehouse"        => -1,
                        "has_set"               => $has_set,
                        "is_skhpt"              => $is_skhpt
                    ]);

                    ServiceRequestUttpItem::where("id", $item->id)
                    ->update([
                        "status_id"=>$status,
                        "no_order"=>$noorder_alat,
                        "order_at"=> date("Y-m-d H:i:s"),
                    ]);

                    $order = ServiceOrderUttps::where('service_request_id', $item->request_id)
                        ->where('service_request_item_id', $item->id)
                        ->first();

                    if ($serviceRequest->service_type_id == 4 || $serviceRequest->service_type_id == 5) {
                        foreach($item->perlengkapans as $perlengkapan) {
                            $ttuPerlengkapanModel = new ServiceOrderUttpTTUPerlengkapan();
                            $ttuPerlengkapan = [
                                'order_id' => $order->id,
                                'uttp_id' => $perlengkapan->uttp_id
                            ];
                            $ttuPerlengkapanModel->create($ttuPerlengkapan);
                        }
                    } elseif ($serviceRequest->service_type_id == 6 || $serviceRequest->service_type_id == 7) {
                        $inspectionItemsQ = DB::table('uttp_inspection_items')
                            //->where('uttp_type_id', $item->uttp->type_id)
                            //->where('template_id', $inspection->inspectionPrice->inspection_template_id)
                            ->where('template_id', $item->uttp->type->template_id)
                            ->orderBy('order_no', 'asc');
                        if ($only_pemeriksaan == true) {
                            $inspectionItemsQ = $inspectionItemsQ->where('is_pemeriksaan', true);
                        }

                        ServiceOrderUttpInspections::where("order_id", $order->id)->delete();

                        $inspectionItems = $inspectionItemsQ->get();
                        foreach ($inspectionItems as $inspectionItem) {
                            ServiceOrderUttpInspections::insert([
                                'order_id' => $order->id,
                                'inspection_item_id' => $inspectionItem->id,
                                'order_no' => $inspectionItem->order_no,
                            ]);
                        }

                        // prefix dan kndl
                        $serviceType = MasterServiceType::find($serviceRequest->service_type_id);
                        //$lastNo = $serviceType->last_no;
                        //$nextNo = intval($lastNo)+1;
                        $prefix = $serviceType->prefix;
                        $kndl = $serviceRequest->lokasi_pengujian == 'dalam' ? 'KN' : 'DL';

                        if ($order->cancel_at != null){
                            $kndl = 'KET';
                        }

                        // last no skhpt
                        $lastOrder = ServiceOrderUttps::where("id", "<>", $order->id)
                        ->whereNotNull("no_sertifikat_tipe_int")
                        ->where('no_sertifikat_tipe_year', date("Y"))
                        ->orderBy("no_sertifikat_tipe_int", "desc")
                        ->select("no_sertifikat_tipe_int")
                        ->first();

                        if ($lastOrder != null)
                        {
                            $lastNo = $lastOrder->no_sertifikat_tipe_int;
                            $nextNo = intval($lastNo)+1;
                        }
                        else 
                        {
                            $lastNo = 0;
                            $nextNo = 1;
                        }

                        $noSertifikatTipe = $prefix.'T.'.sprintf("%04d", $nextNo).'/PKTN.4.5/'.$kndl.'/'.date("m/Y");
            
                        $order->update([
                            "no_sertifikat_tipe" => $noSertifikatTipe,
                            "no_sertifikat_tipe_int" => $nextNo,
                            "no_sertifikat_tipe_year" => date("Y"),
                        ]);

                    }


                    $history = new HistoryUttp();
                    $history->request_status_id = $status; 
                    $history->request_id = $id;
                    $history->request_item_id = $item->id;
                    $history->order_id = $order->id;
                    $history->order_status_id = 1;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = $status;
                    $item->save();
                }
            }
            return Redirect::route('serviceprocessluaruttp');
        } else {
            return Redirect::route('serviceprocessluaruttp.check', $id);
        }
    }

    public function createbookinginspection($id)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttp");

        $item = $this->ServiceRequestUttpItem->find($id);
        if ($item->serviceRequest->service_type_id == 6 || $item->serviceRequest->service_type_id == 7) {
            /*
            $others_pemeriksaan = DB::table('uttp_inspection_prices')
                ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->leftJoin('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_request_uttp_item_inspections.request_item_id')
                ->leftJoin('service_order_uttps', 'service_order_uttps.service_request_item_id', '=', 'service_request_uttp_items.id')
                ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id)
                ->where('uttp_inspection_prices.is_pemeriksaan', true)
                ->where('service_request_uttp_items.uttp_id', $item->uttp_id)
                ->where('service_request_uttp_items.id', '<>', $item->id)
                ->get();
            */

            //dd([$item->serviceRequest->service_type_id, $item->uttp->type_id, $item->uttp_id, $item->id, $others_pemeriksaan]);

            //$ada_pemeriksaan = count($others_pemeriksaan) > 0;

            $prices = DB::table('uttp_inspection_prices')
                ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id)
                ->whereNull('uttp_inspection_prices.deleted_at')
                ->whereNull('uttp_inspection_price_types.deleted_at')
                ->select('uttp_inspection_prices.*')
                ->distinct();

            /*
            if ($ada_pemeriksaan) {
                $prices = $prices->where(function($query) {
                    $query->where('uttp_inspection_prices.is_pemeriksaan', false)
                          ->orWhereNull('uttp_inspection_prices.is_pemeriksaan');
                });
            } else {
                $prices = $prices->where('uttp_inspection_prices.is_pemeriksaan', true);
            }
            */

            $prices = $prices->get();
            //dd([$item->serviceRequest->service_type_id, $item->uttp->type_id, $prices]);
            
            //dd([$others_pemeriksaan, $ada_pemeriksaan, $item->serviceRequest->service_type_id, $item->uttp->type_id, $prices]);
            //dd($prices);
        } else {
            $prices = DB::table('uttp_inspection_prices')
                ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id) 
                ->whereNull('uttp_inspection_prices.deleted_at')
                ->whereNull('uttp_inspection_price_types.deleted_at')
                ->select('uttp_inspection_prices.*')
                ->distinct()
                ->get();
        }

        return view('serviceprocessluaruttp.create_booking_inspection', compact('item', 'prices', 'attribute'));
    }

    public function simpanbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttp");

        ServiceRequestUttpItemInspection::where('request_item_id', $id)->delete();
        
        foreach($request->get('quantity') as $index=>$value) {
            if ($value != null) {

                $dataInspection = new ServiceRequestUttpItemInspection();
                $dataInspection->request_item_id = $id;
                $dataInspection->quantity = $value;
                $dataInspection->price = $request->get('price')[$index];
                $dataInspection->inspection_price_id = $request->get('id')[$index];

                $dataInspection->status_id = 1;
                $dataInspection->save();
            }

        }

        $item = $this->ServiceRequestUttpItem->find($id);
        
        $itemAggregate = ServiceRequestUttpItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $id)
            ->first();

        $this->ServiceRequestUttpItem->where('id', $id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = $this->ServiceRequestUttpItem
            ->selectRaw('count(service_request_uttp_item_inspections.id) quantity, coalesce(sum(service_request_uttp_item_inspections.price * service_request_uttp_item_inspections.quantity), 0) subtotal')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        $svcRequest = $this->ServiceRequestUttp
            ->selectRaw("service_request_uttps.id, service_request_uttps.received_date, service_request_uttps.received_date 
                + max((case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end)) * interval '1 day' max_estimated_date")
            ->join('service_request_uttp_items', 'service_request_uttp_items.request_id', '=', 'service_request_uttps.id')
            //->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            //->join('uttp_inspection_prices', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            //->join('master_instalasi', 'uttp_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->join('uttps', 'uttps.id', '=', 'service_request_uttp_items.uttp_id')
            ->join('master_uttp_types', 'master_uttp_types.id', '=', 'uttps.type_id')
            ->join('master_instalasi', 'master_uttp_types.instalasi_id', '=', 'master_instalasi.id')
            ->groupBy('service_request_uttps.id', 'service_request_uttps.received_date')
            ->where('service_request_uttps.id', $item->serviceRequest->id)
            ->first();

        $itemCount = $this->ServiceRequestUttpItem->where("request_id", $item->serviceRequest->id)->count();
        $request = $this->ServiceRequestUttp->find($item->serviceRequest->id);
        
        // $no_order = $request->no_order;
        // $parts = explode("-",$no_order);
        // $parts[3] = sprintf("%03d",intval($itemCount));
        // $no_order = implode("-", $parts);

        $this->ServiceRequestUttp->where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal,
            'estimated_date' => $svcRequest ? $svcRequest->max_estimated_date : null,
            //'no_order' => $no_order,
        ]);

        return Redirect::route('serviceprocessluaruttp.check', $item->serviceRequest->id);
    }

    public function hapusbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $inspection = ServiceRequestUttpItemInspection::where('id', $id)->first();
        $inspection->delete();
        $item_id = $inspection->request_item_id;

        $item = $this->ServiceRequestUttpItem
            ->with(['serviceRequest'])
            ->find($item_id);
        
        $itemAggregate = ServiceRequestUttpItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $item->id)
            ->first();

        $this->ServiceRequestUttpItem->where('id', $item->id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = $this->ServiceRequestUttpItem
            ->selectRaw('count(service_request_uttp_item_inspections.id) quantity, coalesce(sum(service_request_uttp_item_inspections.price * service_request_uttp_item_inspections.quantity), 0) subtotal')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        $this->ServiceRequestUttp->where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal
        ]);

        return Redirect::route('serviceprocessluaruttp.check', $item->serviceRequest->id);
    }

    function newitem($id)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $uttp_types = MasterUttpType::orderBy('id')->pluck('uttp_type', 'id');

        return view('serviceprocessluaruttp.add_booking_item', 
            compact('request',
            'uttp_types', 
            'attribute'));
    }

    function savenewitem($id, Request $request)
    {
        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $uttp = Uttp::find($request->get('uttp_id'));

        $dataItem["request_id"] = $req->id;
        //$dataItem["quantity"] = $item->quantity;
        //$dataItem["subtotal"] = $item->est_subtotal;
        $dataItem["uttp_id"] = $uttp->id;

        //$dataItem["location"] = $request->location;
        $dataItem["location_prov_id"] = $req->inspection_prov_id;
        $dataItem["location_kabkot_id"] = $req->inspection_kabkot_id;

        /*
        $dataItem["file_application_letter"] = $item->file_application_letter;
        $dataItem["file_last_certificate"] = $item->file_last_certificate;
        $dataItem["file_manual_book"] = $item->file_manual_book;
        $dataItem["file_type_approval_certificate"] = $item->file_type_approval_certificate;
        $dataItem["file_calibration_manual"] = $item->file_calibration_manual;

        $dataItem["path_application_letter"] = $item->path_application_letter;
        $dataItem["path_last_certificate"] = $item->path_last_certificate;
        $dataItem["path_manual_book"] = $item->path_manual_book;
        $dataItem["path_type_approval_certificate"] = $item->path_type_approval_certificate;
        $dataItem["path_calibration_manual"] = $item->path_calibration_manual;

        $dataItem["reference_no"] = $item->reference_no;
        $dataItem["reference_date"] = $item->reference_date;
        */

        $dataItem["status_id"] = $req->status_id;

        $serviceItemID = ServiceRequestUttpItem::insertGetId($dataItem);

        return Redirect::route('serviceprocessluaruttp.check', $id);
    }

    function createitem($id, $uttp_type_id)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $type = MasterUttpType::find($uttp_type_id);

        $units = DB::table('master_uttp_units')
                ->where('uttp_type_id', $uttp_type_id)
                ->pluck('unit', 'unit');

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        $items = $request->items->map(function ($item, $key) {
            return [
                'label' => $item->uttp->type->uttp_type . ': ' .
                    $item->uttp->serial_no . ' (' . 
                    $item->uttp->tool_brand . ' / ' . 
                    $item->uttp->tool_model . ')',
                'id' => $item->id
            ];
        })->pluck('label', 'id');
        //dd([$items, $request->items->pluck('uttp.type.uttp_type', 'id')]);

        $kabkot = DB::table('master_kabupatenkota')->join('master_provinsi', 'master_provinsi.id', '=', 'master_kabupatenkota.provinsi_id')
            ->selectRaw("concat(master_kabupatenkota.nama, ', ', master_provinsi.nama) nama, master_kabupatenkota.id id")
            ->pluck('nama', 'id');

        return view('serviceprocessluaruttp.create_booking_item', compact(
            'request', 'items', 'type', 'units', 'negara', 'kabkot',
             'attribute'));
    }

    public function simpanitem($id, $uttp_type_id, Request $request) 
    {
        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
        ->find($id);
        
        $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();

        $uttp = Uttp::create([
            'owner_id' => $req->uttp_owner_id,
            'type_id' => $uttp_type_id,
            'serial_no' => $request->get('tool_serial_no'),
            'tool_brand' => $request->get('tool_brand'),
            'tool_model' => $request->get('tool_model'),
            'tool_capacity' => $request->get('tool_capacity'),
            'tool_factory' => $request->get('tool_factory'),
            'tool_factory_address' => $request->get('tool_factory_address'),
            'tool_made_in' => $negara->nama_negara,
            'tool_made_in_id' => $request->get('tool_made_in_id'),     
            'tool_capacity_unit' => $request->get('tool_capacity_unit'),
            'tool_media' => $request->get('tool_media'),
            'tool_capacity_min' => $request->get('tool_capacity_min'),

            'location' => $request->get('location_alat'),
            'location_lat' => $request->get('location_lat'),
            'location_long' => $request->get('location_long'),

        ]);

        if ($request->get('jenis') == 'sertifikat') {
            $dataItem["request_id"] = $req->id;
            $dataItem["uttp_id"] = $uttp->id;

            //$dataItem["location"] = $request->location;
            //$dataItem["location_prov_id"] = $req->inspection_prov_id;
            //$dataItem["location_kabkot_id"] = $req->inspection_kabkot_id;

            // lokasi alat
            $dataItem["location_alat"] = $request->get('location_alat');
            $dataItem["location_lat"] = $request->get('location_lat');
            $dataItem["location_long"] = $request->get('location_long');

            $dataItem["location"] = $request->get('location');
            $dataItem["location_kabkot_id"] = $request->get('location_kabkot_id');

            $kabkot = DB::table('master_kabupatenkota')->where('id', $request->get('location_kabkot_id'))->first();
            $dataItem["location_prov_id"] = $kabkot->provinsi_id;

            $dataItem["status_id"] = $req->status_id;

            $serviceItemID = ServiceRequestUttpItem::insertGetId($dataItem);
        } else {
            $item = ServiceRequestUttpItem::find($request->get('item_id'));

            $dataPerlengkapan["request_item_id"] =  $item->id;
            $dataPerlengkapan["uttp_id"] = $uttp->id;

            $perelengkapan = ServiceRequestUttpItemTTUPerlengkapan::insert($dataPerlengkapan);
        }

        return Redirect::route('serviceprocessluaruttp.check', $id);
    }

    public function edititem($id, $idItem) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $item = ServiceRequestUttpItem::find($idItem);

        $units = DB::table('master_uttp_units')
                    ->where('uttp_type_id', $item->uttp_type_id)
                    ->pluck('unit', 'unit')
                    ->prepend($item->uttp->tool_capacity_unit, $item->uttp->tool_capacity_unit);

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        $kabkot = DB::table('master_kabupatenkota')->join('master_provinsi', 'master_provinsi.id', '=', 'master_kabupatenkota.provinsi_id')
            ->selectRaw("concat(master_kabupatenkota.nama, ', ', master_provinsi.nama) nama, master_kabupatenkota.id id")
            ->pluck('nama', 'id');

        return view('serviceprocessluaruttp.edit_booking_item', 
            compact('request', 'item', 'units', 'negara', 'kabkot', 'attribute'));
    }

    public function simpanedititem($id, $idItem, Request $request) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $item = ServiceRequestUttpItem::find($idItem);

        $uttp = Uttp::find($item->uttp_id);

        $data['tool_capacity'] = $request->get("tool_capacity");
        $data['tool_capacity_min'] = $request->get("tool_capacity_min");
        $data['tool_capacity_unit'] = $request->get("tool_capacity_unit");

        $data['tool_brand'] = $request->get("tool_brand");
        $data['tool_model'] = $request->get("tool_model");
        $data['serial_no'] = $request->get("tool_serial_no");

        $data['tool_media'] = $request->get("tool_media");

        $data['tool_made_in_id'] = $request->get('tool_made_in_id');
        $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();
        $data['tool_made_in'] = $negara->nama_negara;

        $data['tool_factory'] = $request->get("tool_factory");
        $data['tool_factory_address'] = $request->get("tool_factory_address");

        $data['location'] = $request->get("location_alat");
        $data['location_lat'] = $request->get("location_lat");
        $data['location_long'] = $request->get("location_long");

        $uttp->update($data);

        $dataItem["location_alat"] = $request->get('location_alat');
        $dataItem["location_lat"] = $request->get('location_lat');
        $dataItem["location_long"] = $request->get('location_long');

        $dataItem["location"] = $request->get('location');
        $dataItem["location_kabkot_id"] = $request->get('location_kabkot_id');

        $kabkot = DB::table('master_kabupatenkota')->where('id', $request->get('location_kabkot_id'))->first();
        $dataItem["location_prov_id"] = $kabkot->provinsi_id;

        $item->update($dataItem);

        return Redirect::route('serviceprocessluaruttp.check', $id);
    }

    public function hapusitem(Request $request) 
    {
        $item = ServiceRequestUttpItem::find($request->get('id'));
        $request_id = $item->request_id;

        $itemCount = ServiceRequestUttpItem::where('uttp_id', $item->uttp_id)
            ->where('id', '<>', $request->get('id'))
            ->count();

        
        ServiceRequestUttpItemInspection::where('request_item_id', $item->id)->delete();

        $perlengkapans = ServiceRequestUttpItemTTUPerlengkapan::where('request_item_id', $item->id)->get();

        ServiceRequestUttpItemTTUPerlengkapan::where('request_item_id', $item->id)->delete();
        
        $item->delete();
        if ($itemCount == 0) {
            //Uttp::where('id', $item->uttp_id)->delete();
        }

        foreach($perlengkapans as $perlengkapan) {
            $itemPerlengkapanCount = ServiceRequestUttpItem::where('uttp_id', $perlengkapan->uttp_id)
                ->where('id', '<>', $request->get('id'))
                ->count();
            $ttuPerlengkapanCount = ServiceRequestUttpItemTTUPerlengkapan::where('uttp_id', $perlengkapan->uttp_id)
                ->where('request_item_id', '<>', $request->get('id'))
                ->count();

            if ($itemPerlengkapanCount == 0 && $ttuPerlengkapanCount == 0) {
                //Uttp::where('id', $perlengkapan->uttp_id)->delete();
            }
        }

        return Redirect::route('serviceprocessluaruttp.check', $request_id); 
    }

    public function getuttps(Request $request) {
        $owner_id = $request->get('owner_id');
        $uttp_type_id = $request->get('uttp_type_id');
        $uttps = Uttp::where('owner_id', $owner_id)->where('type_id', $uttp_type_id);

        if ($request->has('serial_no')) {
              $uttps = $uttps->where('serial_no', 'like', '%'.$request->get('serial_no').'%');
        }

        return response()->json($uttps->get());
    }

    public function perlengkapan($id) {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $item = ServiceRequestUttpItem::find($id);

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($item->serviceRequest->id);

        $perlengkapans = ServiceRequestUttpItemTTUPerlengkapan::where('request_item_id', $id)->get();

        return view('serviceprocessluaruttp.perlengkapan', 
            compact('request', 'item', 'perlengkapans', 'attribute'));
    }

    function newperlengkapan($id)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $item = ServiceRequestUttpItem::find($id);

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($item->serviceRequest->id);

        $uttp_types = MasterUttpType::orderBy('id')->get();
        $uttp_type_id = $uttp_types[0]->id;
        $uttp_types = $uttp_types->pluck('uttp_type', 'id');
        

        $units = DB::table('master_uttp_units')
                ->where('uttp_type_id', $uttp_type_id)
                ->pluck('unit', 'unit');

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        return view('serviceprocessluaruttp.create_perlengkapan', 
            compact('request', 'item', 'units',  'uttp_types', 'negara', 'attribute'));
    }

    public function simpanperlengkapan($id, Request $request) 
    {
        $item = ServiceRequestUttpItem::find($id);

        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
        ->find($item->serviceRequest->id);
        
        $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();

        $uttp = Uttp::create([
            'owner_id' => $req->uttp_owner_id,
            'type_id' => $request->get('uttp_type_id'),
            'serial_no' => $request->get('tool_serial_no'),
            'tool_brand' => $request->get('tool_brand'),
            'tool_model' => $request->get('tool_model'),
            'tool_capacity' => $request->get('tool_capacity'),
            'tool_factory' => $request->get('tool_factory'),
            'tool_factory_address' => $request->get('tool_factory_address'),
            'tool_made_in' => $negara->nama_negara,
            'tool_made_in_id' => $request->get('tool_made_in_id'),     
            'tool_capacity_unit' => $request->get('tool_capacity_unit'),
            'tool_media' => $request->get('tool_media'),
            'tool_capacity_min' => $request->get('tool_capacity_min'),
        ]);

        //$item = ServiceRequestUttpItem::find($request->get('item_id'));

        $dataPerlengkapan["request_item_id"] =  $item->id;
        $dataPerlengkapan["uttp_id"] = $uttp->id;

        $perelengkapan = ServiceRequestUttpItemTTUPerlengkapan::insert($dataPerlengkapan);

        return Redirect::route('serviceprocessluaruttp.perlengkapan', $id);
    }

    public function getunits(Request $request) {
        $uttp_type_id = $request->get('uttp_type_id');
        $units = MasterUttpUnit::where('uttp_type_id', $uttp_type_id);

        if ($request->has('unit')) {
              $units = $uttps->where('unit', 'like', '%'.$request->get('unit').'%');
        }

        return response()->json($units->get());
    }

    public function editperlengkapan($id, $idItem, $idPerlengkapan) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $perlengkapan = ServiceRequestUttpItemTTUPerlengkapan::find($idPerlengkapan);

        $item = ServiceRequestUttpItem::find($idItem);

        // $units = DB::table('master_uttp_units')
        //             ->where('uttp_type_id', $item->uttp_type_id)
        //             ->pluck('unit', 'unit')
        //             ->prepend($item->uttp->tool_capacity_unit, $item->uttp->tool_capacity_unit);
        $units = DB::table('master_uttp_units')
                    ->where('uttp_type_id', $perlengkapan->uttp->type_id)
                    ->pluck('unit', 'unit')
                    ->prepend($perlengkapan->uttp->tool_capacity_unit, $perlengkapan->uttp->tool_capacity_unit);

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        return view('serviceprocessluaruttp.edit_perlengkapan', 
            compact('request', 'item', 'perlengkapan', 'units', 'negara', 'attribute'));
    }

    public function simpaneditperlengkapan($id, $idItem, $idPerlengkapan, Request $request) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $item = ServiceRequestUttpItem::find($idItem);

        $perlengkapan = ServiceRequestUttpItemTTUPerlengkapan::find($idPerlengkapan);

        $uttp = Uttp::find($perlengkapan->uttp_id);

        $data['tool_capacity'] = $request->get("tool_capacity");
        $data['tool_capacity_min'] = $request->get("tool_capacity_min");
        $data['tool_capacity_unit'] = $request->get("tool_capacity_unit");

        $data['tool_brand'] = $request->get("tool_brand");
        $data['tool_model'] = $request->get("tool_model");
        $data['serial_no'] = $request->get("tool_serial_no");

        $data['tool_media'] = $request->get("tool_media");

        $data['tool_made_in_id'] = $request->get('tool_made_in_id');
        $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();
        $data['tool_made_in'] = $negara->nama_negara;

        $data['tool_factory'] = $request->get("tool_factory");
        $data['tool_factory_address'] = $request->get("tool_factory_address");

        $uttp->update($data);

        return Redirect::route('serviceprocessluaruttp.perlengkapan', $idItem);
    }

    public function hapusperlengkapan(Request $request) 
    {
        $perlengkapan = ServiceRequestUttpItemTTUPerlengkapan::find($request->get('id'));

        // Uttp::find($perlengkapan->uttp_id)->delete();
        $request_item_id = $perlengkapan->request_item_id;
        // $perlengkapan->delete();

        //$perlengkapans = ServiceRequestUttpItemTTUPerlengkapan::where('request_item_id', $perlengkapan->request_item_id)->get();

        //ServiceRequestUttpItemTTUPerlengkapan::where('request_item_id', $perlengkapan->request_item_id)->delete();

        $perlengkapan->delete();

        return Redirect::route('serviceprocessluaruttp.perlengkapan', $request_item_id); 
    }

    public function statusRevisiSPT(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Request tidak ditemukan';

        $req = null;
        if ($request->has('id')) {
            $req = ServiceRequestUttp::find($request->get('id'));
        } 

        if ($req != null) {
            $req->status_revisi_spt = $request->get('status_revisi_spt');
            if ($request->get('status_revisi_spt') == 1) {
                $doc = ServiceRequestUttpInsituDoc::find($req->spuh_doc_id);
                $doc->keterangan_revisi = $request->get('keterangan_revisi');
                $doc->update();
            }
            $req->update();

            $response["status"] = true;
            $response["messages"] = "Status revisi SPT telah diubah";
        }
        
        return response($response);
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        return view('serviceprocessluaruttp.cancel',compact(
            'request', 'requestor', 'doc',
            'attribute'
        ));
    }

    public function confirmcancel($id, Request $request)
    {
        
        $svcRequest = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
    

        $svcRequest->update([
            "cancel_at" => date('Y-m-d H:i:s'),
            "cancel_notes" => $request->get("cancel_notes"),
            "cancel_id" => Auth::id(),
            "status_id" => 16,
        ]);

        $items = ServiceRequestUttpItem::where('request_id', $id)->get();


        foreach($items as $item) {

            $item->status_id = 16;
            $item->save();

            $inspection = ServiceRequestUttpItemInspection::where('request_item_id', $item->id)
                ->update(['status_id' => 16]);

            $history = new HistoryUttp();
            $history->request_status_id = 16;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

        }
        

        return Redirect::route('serviceprocessluaruttp');
    }
}