<?php 

namespace App\Http\Controllers;

use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use Illuminate\Http\Request;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrders;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\ServiceRequestItem;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class TestedController extends Controller
{
    // protected $ServiceOrders;
    // protected $MasterMetodeUji;
    // protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;

    public function __construct()
    {
        // $this->ServiceOrders = new ServiceOrders();
        // $this->MasterMetodeUji = new MasterMetodeUji();
        // $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup();

        $rows = ServiceOrders::where("stat_service_order",1)
        ->orderBy('staff_entry_dateout','desc')
        ->get();

        return view('tested.index',compact('rows','attribute'));
    }

    public function proses($id)
    {
        ServiceOrders::whereId($id)
        ->where("stat_service_order",1)
        ->update([
            "stat_service_order"=>2,
            "supervisor_staff"=>Auth::id(),
            "supervisor_entry_date"=>date("Y-m-d")
        ]);
        return Redirect::route('tested');
    }

    public function CancelHasilUji($id)
    {
        $row = ServiceOrders::whereId($id)
        ->where("stat_service_order",1)
        ->whereNotNull("lab_staff_out")
        ->whereNotNull("staff_entry_dateout")
        ->first();

        if($row)
        {
            ServiceOrders::whereId($id)
            ->update([
                "stat_service_order"=>0,
                "lab_staff_out"=>null,
                "staff_entry_dateout"=>null
            ]);

            ServiceRequest::whereId($row->service_request_id)
            ->update([
                "stat_service_request"=>2
            ]);

            ServiceRequestItem::whereId($row->service_request_item_id)
            ->update([
                "stat_service_request_item"=>1
            ]);
        }

        return Redirect::route('tested');
    }

    public function history()
    {
        $attribute = $this->MyProjects->setup();
        if($attribute==null) return userlogout();
        
        $rows = ServiceOrders::where("stat_service_order","<>",1)
        ->orderBy('supervisor_entry_date','desc')
        ->get();

        return view('tested.history',compact('rows','userLabName','attribute'));
    }

    // 0001 / PKTN.4.7 / 02 / 2021
    // Nomor Sertifikat : {no urut: 0000. Reset pertahun} / PKTN.4.7{SNSU/BSML/UML:kode daerah} / {bulan: 0} / 2021
    // UML = UML.kodedaerah
    // SNSU =  PKTN.4.7
    // BSML1 = PKTN.4.8
    // BSML2 = PKTN.4.9
    // BSML3 = PKTN.4.10
    // BSML4 = PKTN.4.11    
    public function sertifikat($id)
    {
        $row =  ServiceOrders::find($id);
        
        if(!$row->hasil_uji) abort("404");

        $hot = json_decode($row->hasil_uji,true);
        $data['merk'] = $hot[1][1];
        $data['type'] = $hot[2][1];
        $data['serialnumber'] = $hot[3][1];
        $data['nominalrange'] = $hot[4][1];
        $data['kelas'] = $hot[5][1];
        $data['refmethod'] = $hot[7][1];
        $data['refstandard'] = $hot[8][5]." (Serial Number: ".$hot[8][12].")";
        $data['tertelusur'] = $hot[9][1];
        $data['kalibrator'] = $hot[7][15];
        $data['kalibrator2'] = $hot[7][17];
        $data['suhu'] = $hot[5][9];
        $data['lembab'] = $hot[5][11];
        $data['u_suhu'] = $hot[5][41];
        $data['u_lembab'] = $hot[5][42];

        $countUut = 0;
        for($incr=15;$incr<=33;$incr++)
        {
            if(floatval($hot[$incr][40])>0) $countUut++;
        }
        $data['countUut'] = $countUut;

        return view('service.sertifikat_pdf',compact(
            'row',
            'hot',
            'data'
        ));
    }

    public function KalkulasiInterpolasi($data,$input)
    {
        $prev_key = null;
        $next_key = null;

        foreach (array_keys($data)as $key) {
            // convert to timestamp for comparison
            if ($key < $input) {
                $prev_key = $key;
            } else {
                //  already found next key, move on
                if ($next_key == null) {
                    $next_key = $key;
                }
            }
        }

        $prev_value = $data[$prev_key];
        $next_value = $data[$next_key];

        $temp = ((($input) - ($prev_key)) * ($next_value - $prev_value) / (($next_key) - ($prev_key)));
        $y = $temp + $prev_value;

        return $y;
    }
    
    private function InterpolasiTermohygro($suhuStart,$suhuEnd,$termohygroStart,$termohygroEnd,$barometerStart,$barometerEnd)
    {
        $rows = MassaInterpolasiSuhu::get();
        $data = [];
        $u_suhu = [];
        foreach($rows as $row)
        {
            $data[$row->pembacaan] = $row->koreksi;
            $u_suhu[] = $row->u;
        }

        $indicationSuhu = ($suhuStart+$suhuEnd)/2;
        $koreksiSuhu = $this->KalkulasiInterpolasi($data,$indicationSuhu);
        $u_suhu[] = $koreksiSuhu;
        // $koreksiSuhu = -0.381176471;
        $actualSuhu = $indicationSuhu+$koreksiSuhu;

        $rows = MassaInterpolasiTermohygro::get();
        $data = [];
        $u_termohygro = [];
        foreach($rows as $row)
        {
            $data[$row->pembacaan] = $row->koreksi;
            $u_termohygro[] = $row->u;
        }        
        $indicationTermohygro = ($termohygroStart+$termohygroEnd)/2;
        $koreksiTermohygro = $this->KalkulasiInterpolasi($data,$indicationTermohygro);
        $u_termohygro[] = $koreksiTermohygro;
        // $koreksiTermohygro = -34.32946058;
        $actualTermohygro = $indicationTermohygro+$koreksiTermohygro;

        $rows = MassaInterpolasiBarometer::get();
        $data = [];
        foreach($rows as $row)
        {
            $data[$row->pembacaan] = $row->koreksi;
        }
        $indicationBarometer = ($barometerStart+$barometerEnd)/2;
        $koreksiBarometer = $this->KalkulasiInterpolasi($data,$indicationBarometer);
        // $koreksiBarometer = -1.519298246;
        $actualBarometer = $indicationBarometer+$koreksiBarometer;

        return [
            $actualSuhu,
            $actualTermohygro,
            $actualBarometer,
            $koreksiSuhu,
            $koreksiTermohygro,
            $koreksiBarometer,
            $u_suhu,
            $u_termohygro
        ];
    }

}
