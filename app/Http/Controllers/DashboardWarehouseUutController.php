<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\ServiceOrders;

class DashboardWarehouseUutController extends Controller
{
    private $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("warehousedashboarduut");

        $rows = ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
                ->whereHas('ServiceRequestItem', function($query) 
                {
                    $query->where("status_id",13);
                    $query->orWhere("status_id",14);
                })
                ->where('stat_warehouse', 0)
                ->orderBy('staff_entry_dateout','asc')
                ->get();
        
        return view('warehousedashboarduut.index',compact(['rows','attribute']));
    }    
}
