<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\Customer;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use PDF;

class ScheduleConfirmUttpController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("scheduleconfirmuttp");

        $rows = ServiceRequestUttp::where('status_id', 11)
            ->where('scheduled_test_id', Auth::id())
            ->orderBy('received_date','desc')->get();

        return view('scheduleconfirmuttp.index',compact('rows','attribute'));
    }

    public function schedule($id)
    {
        $attribute = $this->MyProjects->setup("schedulinguttp");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);
        $users = MasterUsers::pluck('full_name', 'id');
        //dd($request);

        return view('scheduleconfirmuttp.schedule', compact(['request', 'requestor', 'users', 'attribute']));
    }

    public function confirmschedule($id, Request $request)
    {
        $response["status"] = false;

        $requestEntity = ServiceRequestUttp::find($id);

        $requestEntity->update([
            'scheduled_test_confirmstaf_at' => date("Y-m-d")
        ]);

        $response["id"] = $requestEntity->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }
}
