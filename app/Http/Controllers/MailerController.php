<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\Mail\Registration;
use App\Mail\Activation;
use App\Mail\Forget;
use App\Mail\Generic;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use App\Customer;

class MailerController extends Controller
{
    
    public function __construct()
    {

    }

    private function checkToken($token) 
    {
        $tokenResult = DB::table('user_access_tokens')
                    ->where('token', $token)
                    ->where('expired_at', '>', date("Y-m-d H:i:s"))
                    ->first();

        return $tokenResult;
    }

    public function send(Request $request)
    {
        $response['status'] = false;
        $rules['token'] = ['required'];
        $rules['customer_id'] = ['required'];
        $rules['type'] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response['messages'] = $validation->messages();

        if ($validation->passes())
        {
            $token = $this->checkToken($request->get('token'));
            
            if ($token) {
                $customerModel = new Customer();
                $customer = $customerModel->find($request->get('customer_id'));
            
                if ($request->get('type') == 'registration') {
                    ProcessEmailJob::dispatch($customer->email, new Registration($customer, $request->get('url')))->onQueue('emails');
                    //Mail::to($customer->email)->send(new Registration($customer, $request->get('url')));
                }
                else if ($request->get('type') == 'activation') {
                    ProcessEmailJob::dispatch($customer->email, new Activation($customer, $request->get('url')))->onQueue('emails');
                    //Mail::to($customer->email)->send(new Activation($customer, $request->get('url')));
                }
                else if ($request->get('type') == 'forget') {
                    ProcessEmailJob::dispatch($customer->email, new Forget($customer, $request->get('url')))->onQueue('emails');
                    //Mail::to($customer->email)->send(new Forget($customer, $request->get('url')));
                }
                else {
                    ProcessEmailJob::dispatch($customer->email, new Generic($request->get('title'), $customer, $request->get('body')))->onQueue('emails');
                    //Mail::to($customer->email)->send(new Generic($request->get('title'), $customer, $request->get('body')));
                }
                
                $response['status'] = true;
                $response['messages'] = "Email berhasil dikirim";
            } else {
                $response['status'] = false;
                $response['messages'] = "Otentifikasi tidak dikenal";
            }
        }
        return response($response);
    }

    public function valid($token)
    {
        if ($token) {
            $order = ServiceOrderUttps::with([
                'ServiceRequest', 
                'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
                'MasterUsers', 'LabStaffOut',
                'persyaratanTeknis', 
                'inspections' => function ($query)
                {
                    $query->orderBy('id', 'asc');
                },
            ])->where('qrcode_token', $token)
            ->first();
    
            $blade = 'serviceuttp.valid';
    
            $view = true;
            return view($blade,compact('order','view')); 
        }

        abort(403);
    }

}
