<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\ArticleFile;
use App\MasterArticleType;

class ArticleFileController extends Controller
{
    private $MyProjects;
    private $Article;

    public function __construct()
    {
        $this->Article = new ArticleFile();
        $this->MasterArticleType = new MasterArticleType();
        $this->MyProjects = new MyProjects();
    }

    public function index(){
        // \DB::enableQueryLog();
        $data = $this->Article->all();

        $attribute = $this->MyProjects->setup("file");

        return View('file.index',compact('data','attribute'));
    }

    public function create(){
        $attribute = $this->MyProjects->setup("file");
        $types = $this->MasterArticleType->pluck('article_type','id');

        return View('file.create',compact(['attribute', 'types']));
    }

    public function store(Request $request)
    {
        $response["article"] = false;
        $rules["title"] = ['required'];
        $rules["file_name"] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);

            $data['title'] = $request->get('title');
            $data['featured'] = $request->get('featured') ? true : false;
            $data['published'] = $request->get('published') ? true : false;
            $data['priority_index'] = $request->get('priority_index');
            $data['type_id'] = $request->get('type_id');

            if($request->hasFile('file_name')){
                $file = $request->file('file_name');
                $data['file_name'] = $file->getClientOriginalName();

                $path = $file->store(
                    'files',
                    'public'
                );

                $data['path'] = $path;
            }
            //dd($data);
            $this->Article->create($data);
            
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return redirect()->route('file')->with($response);
    }

    public function edit($id)
    {
        $attribute = $this->MyProjects->setup("article");
        $row  = $this->Article->find($id);
        $types = $this->MasterArticleType->pluck('article_type','id');
        return view('file.edit', compact([
            'row','attribute','types',
        ]));
    }   

    public function update($id, Request $request)
    {
        $response["status"] = false;
        $response["article"] = false;
        $rules["title"] = ['required'];
        $rules["content"] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $this->Article->whereId($id)->update(
            [
                "title" => $request->get('title'),
                "content" => $request->get('content'),
                "published" => $request->get('published') ? true : false,
                "excerpt" => $request->get('excerpt'),
                "featured" => $request->get('featured') ? true : false,
                "article_type_id" => $request->get('article_type_id'),
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return redirect()->route('article')->with($response);
    }
    
    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->Article::whereId($id)->first();

        if($row)
        {
            $this->Article::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('article')->with($response);
    }

}