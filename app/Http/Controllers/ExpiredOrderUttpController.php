<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpsMirror;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Mail\ExpiredUttp;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use Maatwebsite\Excel\Facades\Excel;
use App\Excel\Exports\UttpExpiredExport;

use PDF;

class ExpiredOrderUttpController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("expiredorderuttp");

        $masa_berlaku = $request->has('masa_berlaku') ? $request->get('masa_berlaku') : 'Hampir Habis Masa Berlaku';

        $rows = ServiceOrderUttpsMirror::whereRaw('1=1')
        ->join('service_request_uttps','service_order_uttps_mirror.service_request_id','=','service_request_uttps.id')
        ->join('service_request_uttp_items','service_order_uttps_mirror.service_request_item_id','=','service_request_uttp_items.id')
        ->whereIn('service_order_uttps_mirror.stat_service_order',[3,4])
        ->whereIn('service_order_uttps_mirror.stat_sertifikat',[3])
        ->whereIn('service_request_uttps.service_type_id', [4,5])
        ->whereNotNull('service_order_uttps_mirror.no_sertifikat')
        ->select('service_order_uttps.*')
        ->addSelect(
            DB::raw('service_order_uttps_mirror.sertifikat_expired_at - current_date AS date_diff')
        );

        if ($masa_berlaku == 'Masih Berlaku') {
            $rows = $rows->whereRaw('service_order_uttps_mirror.sertifikat_expired_at - current_date >= 60');
        } elseif ($masa_berlaku == 'Hampir Habis Masa Berlaku') {
            $rows = $rows->whereRaw('service_order_uttps_mirror.sertifikat_expired_at - current_date > 0')
                ->whereRaw('service_order_uttps_mirror.sertifikat_expired_at - current_date < 60');
        } else {
            $rows = $rows->whereRaw('service_order_uttps_mirror.sertifikat_expired_at - current_date <= 0');
        }

        $rows = $rows->get();

        $kategori = [
            'Masih Berlaku' => 'Masih Berlaku', 
            'Hampir Habis Masa Berlaku' => 'Hampir Habis Masa Berlaku',
            'Habis Masa Berlaku' => 'Habis Masa Berlaku',
        ];
        
        return view('expiredorderuttp.index',compact('attribute', 'rows', 'kategori','masa_berlaku'));
    }

    public function notif() {
        $rows_60 = ServiceOrderUttpsMirror::whereRaw('1=1')
        ->join('service_request_uttps','service_order_uttps_mirror.service_request_id','=','service_request_uttps.id')
        ->join('service_request_uttp_items','service_order_uttps_mirror.service_request_item_id','=','service_request_uttp_items.id')
        ->whereIn('service_order_uttps_mirror.stat_service_order',[3,4])
        ->whereIn('service_order_uttps_mirror.stat_sertifikat',[3])
        ->whereIn('service_request_uttps.service_type_id', [4,5])
        ->whereNotNull('service_order_uttps_mirror.no_sertifikat')
        ->whereRaw('service_order_uttps_mirror.sertifikat_expired_at - current_date = 60')
        ->where(function ($query) {
            $query->whereNull('service_order_uttps_mirror.expired_notif1')
                  ->orWhere('service_order_uttps_mirror.expired_notif1', false);
        })
        ->select('service_order_uttps_mirror.*')
        ->get();
        
        foreach($rows_60 as $order) {
            //dd([$order,$order->id,ServiceOrderUttps::find($order->id)]);
            //$customerEmail = $order->ServiceRequest->requestor->email;
            // $customerEmail = 'uptp4.ditmet@gmail.com';
            $customerEmail = 'uptp4.ditmet@gmail.com';
            ProcessEmailJob::dispatch($customerEmail, new ExpiredUttp($order))->onQueue('emails');

            ServiceOrderUttpsMirror::find($order->id)->update([
                'expired_notif1' => true,
            ]);
        }


        $rows_30 = ServiceOrderUttpsMirror::whereRaw('1=1')
        ->join('service_request_uttps','service_order_uttps_mirror.service_request_id','=','service_request_uttps.id')
        ->join('service_request_uttp_items','service_order_uttps_mirror.service_request_item_id','=','service_request_uttp_items.id')
        ->whereIn('service_order_uttps_mirror.stat_service_order',[3,4])
        ->whereIn('service_order_uttps_mirror.stat_sertifikat',[3])
        ->whereIn('service_request_uttps.service_type_id', [4,5])
        ->whereNotNull('service_order_uttps_mirror.no_sertifikat')
        ->whereRaw('service_order_uttps_mirror.sertifikat_expired_at - current_date = 30')
        ->where('service_order_uttps_mirror.expired_notif1', true)
        ->where(function ($query) {
            $query->whereNull('service_order_uttps_mirror.expired_notif2')
                  ->orWhere('service_order_uttps_mirror.expired_notif2', false);
        })
        ->select('service_order_uttps_mirror.*')
        ->get();
        
        foreach($rows_30 as $order) {
            //dd([$order,$order->id,ServiceOrderUttps::find($order->id)]);
            //$customerEmail = $order->ServiceRequest->requestor->email;
            $customerEmail = 'uptp4.ditmet@gmail.com';
            ProcessEmailJob::dispatch($customerEmail, new ExpiredUttp($order))->onQueue('emails');

            ServiceOrderUttpsMirror::find($order->id)->update([
                'expired_notif2' => true,
            ]);
        }

        $rows_0 = ServiceOrderUttpsMirror::whereRaw('1=1')
        ->join('service_request_uttps','service_order_uttps_mirror.service_request_id','=','service_request_uttps.id')
        ->join('service_request_uttp_items','service_order_uttps_mirror.service_request_item_id','=','service_request_uttp_items.id')
        ->whereIn('service_order_uttps_mirror.stat_service_order',[3,4])
        ->whereIn('service_order_uttps_mirror.stat_sertifikat',[3])
        ->whereIn('service_request_uttps.service_type_id', [4,5])
        ->whereNotNull('service_order_uttps_mirror.no_sertifikat')
        ->whereRaw('service_order_uttps_mirror.sertifikat_expired_at - current_date = -1')
        ->where('service_order_uttps_mirror.expired_notif1', true)
        ->where('service_order_uttps_mirror.expired_notif2', true)
        ->where(function ($query) {
            $query->whereNull('service_order_uttps_mirror.expired_notif3')
                  ->orWhere('service_order_uttps_mirror.expired_notif3', false);
        })
        ->select('service_order_uttps_mirror.*')
        ->get();
        
        foreach($rows_0 as $order) {
            //dd([$order,$order->id,ServiceOrderUttps::find($order->id)]);
            //$customerEmail = $order->ServiceRequest->requestor->email;
            $customerEmail = 'uptp4.ditmet@gmail.com';
            ProcessEmailJob::dispatch($customerEmail, new ExpiredUttp($order))->onQueue('emails');

            ServiceOrderUttpsMirror::find($order->id)->update([
                'expired_notif3' => true,
            ]);
        }
    }

    public function export(Request $request) {
        
        $attribute = $this->MyProjects->setup("expiredorderuttp");

        $masa_berlaku = $request->has('masa_berlaku') ? $request->get('masa_berlaku') : 'Hampir Habis Masa Berlaku';

        $rows = ServiceOrderUttps::whereRaw('1=1')
        ->join('service_request_uttps','service_order_uttps.service_request_id','=','service_request_uttps.id')
        ->join('service_request_uttp_items','service_order_uttps.service_request_item_id','=','service_request_uttp_items.id')
        ->whereIn('service_order_uttps.stat_service_order',[3,4])
        ->whereIn('service_order_uttps.stat_sertifikat',[3])
        ->whereIn('service_request_uttps.service_type_id', [4,5])
        ->whereNotNull('service_order_uttps.no_sertifikat')
        ->select('service_order_uttps.*')
        ->addSelect(
            DB::raw('service_order_uttps.sertifikat_expired_at - current_date AS date_diff')
        );

        if ($masa_berlaku == 'Masih Berlaku') {
            $rows = $rows->whereRaw('service_order_uttps.sertifikat_expired_at - current_date >= 60');
        } elseif ($masa_berlaku == 'Hampir Habis Masa Berlaku') {
            $rows = $rows->whereRaw('service_order_uttps.sertifikat_expired_at - current_date > 0')
                ->whereRaw('service_order_uttps.sertifikat_expired_at - current_date < 60');
        } else {
            $rows = $rows->whereRaw('service_order_uttps.sertifikat_expired_at - current_date <= 0');
        }

        $rows = $rows->get();
        //dd($rows);
        //$rows = collect($rows)->map(function($x){ return (array) $x; })->toArray(); 

        return Excel::download(new UttpExpiredExport($rows),'Masa Berlaku UTTP.xlsx');
    }
}
