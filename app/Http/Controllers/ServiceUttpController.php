<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrderUttpTTUInspection;
use App\ServiceOrderUttpTTUInspectionItems;
use App\ServiceOrderUttpTTUInspectionSuhu;
use App\ServiceOrderUttpTTUInspectionBadanHitung;
use App\ServiceOrderUttpTTUPerlengkapan;
use App\ServiceOrderUttpEvaluasiTipe;

use App\Mail\Pending;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;

use App\MasterInstalasi;
use App\HistoryUttp;
use App\ServiceOrders;
// for clipping
// use App\Http\Libraries\PDFWatermark;
// use App\Http\Libraries\PDFWatermarker;
use LynX39\LaraPdfMerger\Facades\PdfMerger;
use setasign\Fpdi\Tcpdf\Fpdi;
use Illuminate\Support\Str;

class ServiceUttpController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("serviceuttp");

        $laboratory_id = Auth::user()->laboratory_id;
        //$instalasi_id = Auth::user()->instalasi_id;

        $instalasiList = MasterInstalasi::where('lab_id', $laboratory_id)->orderBy('id','asc')
            ->get();

        foreach($instalasiList as $instalasi) {
           $rows[$instalasi->id] = ServiceOrderUttps::whereIn("stat_service_order",[0,1,2,3,4])
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id",">=", 12);
            })
            ->whereHas('ServiceRequest', function($query) 
            {
                $query->where("lokasi_pengujian","=", 'dalam');
            })
            ->where("is_finish",0)
            ->where(function($query) {
                $query->where('stat_sertifikat', 0)
                      ->orWhereNull('stat_sertifikat');
            })
            //->where('lab_staff', Auth::id())
            ->where(function($query) {
                $query->whereNull('test_by_1')
                      ->orWhere('test_by_1', Auth::id())
                      ->orWhere('test_by_2', Auth::id());
            })
            ->whereNull('subkoordinator_notes')
            ->where('laboratory_id', $laboratory_id)
            ->where('instalasi_id', $instalasi->id)
            ->orderBy('staff_entry_datein','desc')
            ->get();
        }
        //dd($rows);

        return view('serviceuttp.index',compact('rows','attribute','instalasiList'));
    }

    public function test($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.test',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function savetest($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);
        $item = ServiceRequestUttpItem::find($order->service_request_item_id);
        $serviceRequest = ServiceRequestUttp::find($order->service_request_id);

        $has_set = false;
        $is_skhpt = false;

        $only_pemeriksaan = false;
        
        if (count($item->inspections) == 1 && $item->inspections[0]->inspectionPrice->is_pemeriksaan == true) {
            $is_skhpt = true;
            $only_pemeriksaan = true;
        }

        $order->update([
            "stat_service_order" => 1,
            "staff_entry_dateout" => date("Y-m-d"),
            "selesai_uji" => date("Y-m-d"),
            "stat_warehouse" => 0,
            "has_set" => $has_set,
            "is_skhpt" => $is_skhpt
        ]);

        //$inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        

        if ($serviceRequest->service_type_id == 4 || $serviceRequest->service_type_id == 5) {
            foreach($item->perlengkapans as $perlengkapan) {
                $ttuPerlengkapanModel = new ServiceOrderUttpTTUPerlengkapan();
                $ttuPerlengkapan = [
                    'order_id' => $id,
                    'uttp_id' => $perlengkapan->uttp_id
                ];
                $ttuPerlengkapanModel->create($ttuPerlengkapan);
            }
        } elseif ($serviceRequest->service_type_id == 6 || $serviceRequest->service_type_id == 7) {
            $inspectionItemsQ = DB::table('uttp_inspection_items')
                //->where('uttp_type_id', $item->uttp->type_id)
                //->where('template_id', $inspection->inspectionPrice->inspection_template_id)
                ->where('template_id', $item->uttp->type->template_id)
                ->orderBy('order_no', 'asc');
            if ($only_pemeriksaan == true) {
                $inspectionItemsQ = $inspectionItemsQ->where('is_pemeriksaan', true);
            }
                
            ServiceOrderUttpInspections::where("order_id", $order->id)->delete();

            $inspectionItems = $inspectionItemsQ->get();
            foreach ($inspectionItems as $inspectionItem) {
                ServiceOrderUttpInspections::insert([
                    'order_id' => $order->id,
                    'inspection_item_id' => $inspectionItem->id,
                    'order_no' => $inspectionItem->order_no,
                ]);
            }

            // prefix dan kndl
            $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
            //$lastNo = $serviceType->last_no;
            //$nextNo = intval($lastNo)+1;
            $prefix = $serviceType->prefix;
            $kndl = $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'KN' : 'DL';

            if ($order->cancel_at != null){
                $kndl = 'KET';
            }

            // last no skhpt
            $lastOrder = ServiceOrderUttps::where("id", "<>", $id)
                ->whereNotNull("no_sertifikat_tipe_int")
                ->where('no_sertifikat_tipe_year', date("Y"))
                ->orderBy("no_sertifikat_tipe_int", "desc")
                ->select("no_sertifikat_tipe_int")
                ->first();

            if ($lastOrder != null)
            {
                $lastNo = $lastOrder->no_sertifikat_tipe_int;
                $nextNo = intval($lastNo)+1;
            }
            else 
            {
                $lastNo = 0;
                $nextNo = 1;
            }

            //$kndl = 'KN';
            //$noSertifikat = $prefix.'.'.sprintf("%04d", $nextNo).'/PKTN.4.8/'.$kndl.'/'.date("m/Y");
            //$noService = $serviceType->prefix.'-'.date("y").'-'.sprintf("%04d", $nextNo);
            
            $noSertifikatTipe = $prefix.'T.'.sprintf("%04d", $nextNo).'/PKTN.4.5/'.$kndl.'/'.date("m/Y");
            
            $order->update([
                "no_sertifikat_tipe" => $noSertifikatTipe,
                "no_sertifikat_tipe_int" => $nextNo,
                "no_sertifikat_tipe_year" => date("Y"),
            ]);
        }
        
        /*
        $inspection->update([
            'status_id' => 13
        ]);

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_13 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>13]);
        }
        */
        $item->status_id = 13;
        $item->save();

        /*
        PINDAH ke KIRIM HASIL UJI
        $history = new HistoryUttp();
        $history->request_status_id = 13;
        $history->request_id = $item->request_id;
        $history->request_item_id = $item->id;
        $history->order_id = $order->id;
        $history->order_status_id = 1;
        $history->user_id = Auth::id();
        $history->save();
        /*

        ServiceRequestUttpItemInspection::where("request_item_id", $item->id)->update([
            "status_id"=>13,
        ]);

        $items_count = DB::table('service_request_uttp_items')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_id', $order->service_request_id)->get();

        if ($items_count[0]->count_13 == $items_count[0]->count_all) {
            ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>13]);
        }

        //return Redirect::route('serviceuttp');
        return Redirect::route('serviceuttp.result', $id);
    }

    public function savetestqr(Request $request)
    {
        $item = ServiceRequestUttpItem::where('no_order', $request->no_order)->first();
        $order = ServiceOrderUttps::where('service_request_item_id', $item->id)->first();

        if ($order->stat_service_order == 1 && $order->staff_entry_dateout != null) {
            return Redirect::route('serviceuttp.result', $order->id);
        }

        $order->update([
            "stat_service_order" => 1,
            "staff_entry_dateout" => date("Y-m-d"),
            "selesai_uji" => date("Y-m-d"),
            "stat_warehouse" => 0,
        ]);

        //$inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        $item = ServiceRequestUttpItem::find($order->service_request_item_id);
        $serviceRequest = ServiceRequestUttp::find($order->service_request_id);

        if ($serviceRequest->service_type_id == 4 || $serviceRequest->service_type_id == 5) {
            foreach($item->perlengkapans as $perlengkapan) {
                $ttuPerlengkapanModel = new ServiceOrderUttpTTUPerlengkapan();
                $ttuPerlengkapan = [
                    'order_id' => $id,
                    'uttp_id' => $perlengkapan->uttp_id
                ];
                $ttuPerlengkapanModel->create($ttuPerlengkapan);
            }
        } elseif ($serviceRequest->service_type_id == 6 || $serviceRequest->service_type_id == 7) {
            ServiceOrderUttpInspections::where("order_id", $order->id)->delete();
            
            $inspectionItems = DB::table('uttp_inspection_items')
                //->where('uttp_type_id', $item->uttp->type_id)
                //->where('template_id', $inspection->inspectionPrice->inspection_template_id)
                ->where('template_id', $item->uttp->type->template_id)
                ->orderBy('order_no', 'asc')->get();
            foreach ($inspectionItems as $inspectionItem) {
                ServiceOrderUttpInspections::insert([
                    'order_id' => $order->id,
                    'inspection_item_id' => $inspectionItem->id,
                    'order_no' => $inspectionItem->order_no,
                ]);
            }
            // prefix dan kndl
            $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
            //$lastNo = $serviceType->last_no;
            //$nextNo = intval($lastNo)+1;
            $prefix = $serviceType->prefix;
            $kndl = $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'KN' : 'DL';

            if ($order->cancel_at != null){
                $kndl = 'KET';
            }

            // last no skhpt
            $lastOrder = ServiceOrderUttps::where("id", "<>", $order->id)
                ->whereNotNull("no_sertifikat_tipe_int")
                ->where('no_sertifikat_tipe_year', date("Y"))
                ->orderBy("no_sertifikat_tipe_int", "desc")
                ->select("no_sertifikat_tipe_int")
                ->first();

            $lastNo = $lastOrder->no_sertifikat_tipe_int;
            $nextNo = intval($lastNo)+1;

            //$kndl = 'KN';
            //$noSertifikat = $prefix.'.'.sprintf("%04d", $nextNo).'/PKTN.4.8/'.$kndl.'/'.date("m/Y");
            //$noService = $serviceType->prefix.'-'.date("y").'-'.sprintf("%04d", $nextNo);
            
            $noSertifikatTipe = $prefix.'T.'.sprintf("%04d", $nextNo).'/PKTN.4.5/'.$kndl.'/'.date("m/Y");
            
            $order->update([
                "no_sertifikat_tipe" => $noSertifikatTipe,
                "no_sertifikat_tipe_int" => $nextNo,
                "no_sertifikat_tipe_year" => date("Y"),
            ]);
        }
        
        /*
        $inspection->update([
            'status_id' => 13
        ]);

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_13 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>13]);
        }
        */
        $item->status_id = 13;
        $item->save();

        /*
        $history = new HistoryUttp();
        $history->request_status_id = 13;
        $history->request_id = $item->request_id;
        $history->order_id = $order->id;
        $history->request_item_id = $item->id;
        $history->order_status_id = 1;
        $history->user_id = Auth::id();
        $history->save();
        */

        ServiceRequestUttpItemInspection::where("request_item_id", $item->id)->update([
            "status_id"=>13,
        ]);

        $items_count = DB::table('service_request_uttp_items')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_id', $order->service_request_id)->get();

        if ($items_count[0]->count_13 == $items_count[0]->count_all) {
            ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>13]);
        }

        //return Redirect::route('serviceuttp');
        return Redirect::route('serviceuttp.result', $order->id);
    }

    public function result($id)
    {
        $attribute = $this->MyProjects->setup("serviceuttp");

        $serviceOrder = ServiceOrderUttps::find($id);

        $users = MasterUsers::whereNotNull('petugas_uttp_id')
            ->whereHas('PetugasUttp', function($query) 
            {
                $query->where('flag_unit', 'uttp')
                    ->where('is_active', true);
            })
            ->pluck('full_name', 'id')->prepend('-- Pilih Pegawai --','');
        // $users = $users->prepend('-- Pilih Pegawai --', null);
        
        $user = MasterUsers::find(Auth::id());

        $oimls = DB::table("master_oimls")->pluck('oiml_name', 'id');
        $inspectionItems = ServiceOrderUttpInspections::where('order_id', $id)
            ->orderBy('order_no', 'asc')->orderBy('id', 'asc')->get();
            
        $sertifikat_expired_at = date('Y-m-d', 
            strtotime( $serviceOrder->staff_entry_dateout . ' + '. 
            ($serviceOrder->ServiceRequestItem->uttp->type->masa_berlaku_ttu == null ?  0 : 
            $serviceOrder->ServiceRequestItem->uttp->type->masa_berlaku_ttu) .
            ' year'));

        $ttu = ServiceOrderUttpTTUInspection::where('order_id', $id)->first();
        $ttuItems = ServiceOrderUttpTTUInspectionItems::where('order_id', $id)->get();
        //$ttuItems = DB::select("select id, panjang_sebenarnya::varchar from service_order_uttp_ttu_insp_items where order_id = 44");
        //dd($ttuItems);
        $ttuSuhu = ServiceOrderUttpTTUInspectionSuhu::where('order_id', $id)->get();
        $badanHitungs = ServiceOrderUttpTTUInspectionBadanHitung::where('order_id', $id)->get();
        $ttuPerlengkapans = ServiceOrderUttpTTUPerlengkapan::where('order_id', $id)->get();
        $tipe = ServiceOrderUttpEvaluasiTipe::where('order_id', $id)->first();

        $units = DB::table('master_uttp_units')
                    ->where('uttp_type_id', $serviceOrder->tool_type_id)
                    ->pluck('unit', 'unit')
                    ->prepend($serviceOrder->tool_capacity_unit, $serviceOrder->tool_capacity_unit);
        $units->prepend(' ', ' ');
        
        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        return view('serviceuttp.result',compact(
            'serviceOrder',
            'attribute',
            'users',
            'user',
            'oimls',
            'ttu',
            'ttuItems',
            'ttuSuhu',
            'ttuPerlengkapans',
            'badanHitungs',
            'inspectionItems',
            'sertifikat_expired_at',
            'tipe',
            'units',
            'negara',
        ));
    }

    public function resultupload($id, Request $request)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'ServiceRequestItem.uttp.type.oiml',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);


        $rules = [];
        if ($order->path_skhp == null) {
            $rules['file_skhp'] = ['required','mimes:pdf,jpg,jpeg,png','mimetypes:application/pdf,application/octet-stream,image/jpeg,image/png'];
        } else {
            $rules['file_skhp'] = ['mimes:pdf,jpg,jpeg,png','mimetypes:application/pdf,application/octet-stream,image/jpeg,image/png'];
        }
        //$rules['test_by_2'] = ['required'];
        //$rules['hasil_uji_memenuhi'] = ['required','string','min:1'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        //dd($order);

        if ($validation->passes())
        {
            $file = $request->file('file_skhp');
            
            if ($file != null) {
                $data['file_skhp'] = $file->getClientOriginalName();

                $path = $file->store(
                    'skhp',
                    'public'
                );

                $data['path_skhp'] = $path;
            }

            //$data['hasil_uji_memenuhi'] = $request->get("hasil_uji_memenuhi") == 'memenuhi';
            $data['test_by_1'] = Auth::id();
            $data['test_by_2'] = $request->get("test_by_2") > 0 ? $request->get("test_by_2") : null;
            //$data['persyaratan_teknis_id'] = $request->get("persyaratan_teknis_id");
            $data['stat_service_order'] = 1;
            $data['stat_sertifikat'] = $request->get("stat_sertifikat");
            if ($request->get("stat_sertifikat") == 1) {
                $data['draft_submit_at'] = date('Y-m-d H:i:s');
            }

            $data['tool_capacity'] = $request->get("tool_capacity");
            $data['tool_capacity_min'] = $request->get("tool_capacity_min");
            $data['tool_capacity_unit'] = $request->get("tool_capacity_unit");

            $data['tool_brand'] = $request->get("tool_brand");
            $data['tool_model'] = $request->get("tool_model");
            $data['tool_serial_no'] = $request->get("tool_serial_no");

            $data['tool_media'] = $request->get("tool_media");

            $data['tool_media_pengukuran'] = $request->get("tool_media_pengukuran");

            $data['tool_made_in_id'] = $request->get('tool_made_in_id');
            $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();
            $data['tool_made_in'] = $negara->nama_negara;


            $data['satuan_suhu'] = $request->get("satuan_suhu");
            $data['satuan_output'] = $request->get("satuan_output");
            $data['satuan_error'] = $request->get("satuan_error");

            $data['tool_factory'] = $request->get("tool_factory");
            $data['tool_factory_address'] = $request->get("tool_factory_address");


            ServiceRequestUttp::where('id', $order->service_request_id)->update([
                'label_sertifikat' => $request->get('label_sertifikat'),
                'addr_sertifikat' => $request->get('addr_sertifikat'),
            ]);

            if ($order->ServiceRequest->service_type_id == 4 || $order->ServiceRequest->service_type_id == 5) {
                
                if ($request->has("sertifikat_expired_at") && $request->input("sertifikat_expired_at") != null) {
                    $data['sertifikat_expired_at'] = date("Y-m-d", strtotime($request->input("sertifikat_expired_at")));
                } else {
                    $sertifikat_expired_at = date('Y-m-d', 
                        strtotime( $order->staff_entry_dateout . ' + '. 
                        ($order->ServiceRequestItem->uttp->type->masa_berlaku_ttu == null ?  0 : 
                        $order->ServiceRequestItem->uttp->type->masa_berlaku_ttu) .
                        ' year'));
                    $data['sertifikat_expired_at'] = $sertifikat_expired_at;
                }
                if (date("n", strtotime($request->input("sertifikat_expired_at"))) == '12') {
                    $exp = date("Y", strtotime($request->input("sertifikat_expired_at"))) . "-11-30";
                    $data['sertifikat_expired_at'] = date("Y-m-d", strtotime($exp)); 
                }

                $data['tanda_pegawai_berhak'] = $request->input("tanda_pegawai_berhak");
                $data['tanda_daerah'] = $request->input("tanda_daerah");
                $data['tanda_sah'] = $request->input("tanda_sah");

                $data['hasil_uji_memenuhi'] = $request->input('hasil_uji_memenuhi') == 'sah' ? true : false;

                $data['catatan_hasil_pemeriksaan'] = $request->get("catatan_hasil_pemeriksaan");

                if ($order->ServiceRequest->lokasi_pengujian == 'dalam') {
                    $data['location'] = $request->input("location");
                }

                ServiceOrderUttpTTUPerlengkapan::where("order_id", $id)->delete();

                if ($request->has('perlengkapan_id')) {
                    foreach($request->get('perlengkapan_id') as $index=>$value) {
                        ServiceOrderUttpTTUPerlengkapan::create([
                            "order_id" => $id,
                            "uttp_id" => $request->input("perlengkapan_uttp_id.".$index),
                            "keterangan" => $request->input("perlangkapan_keterangan.".$index),
                        ]);
                    }
                }

                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge') {
                    ServiceOrderUttpTTUInspectionBadanHitung::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('badanhitung_item_id')) {
                        foreach($request->get('badanhitung_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionBadanHitung::insert([
                                "order_id" => $id,
                                "type" => $request->input("type.".$index),
                                "serial_no" => $request->input("serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "tank_no" => $request->input("tank_no"),
                        "tag_no" => $request->input("tag_no"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "input_level" => $request->input("input_level.".$index),
                                "error_up" => $request->input("error_up.".$index),
                                "error_down" => $request->input("error_down.".$index),
                                "error_hysteresis" => $request->input("error_hysteresis.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' ||
                    $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus Media Air') {
                    ServiceOrderUttpTTUInspectionBadanHitung::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('badanhitung_item_id')) {
                        foreach($request->get('badanhitung_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionBadanHitung::insert([
                                "order_id" => $id,
                                "brand" => $request->input("brand.".$index),
                                "type" => $request->input("type.".$index),
                                "serial_no" => $request->input("serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "suhu_pengujian" => $request->input("suhu_pengujian"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "meter_factor" => $request->input("meter_factor.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Kadar Air') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "commodity" => $request->input("commodity.".$index),
                                "penunjukan" => $request->input("penunjukan.".$index),
                                "error" => $request->input("error.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBM') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "tool_brand" => $request->input("tool_brand.".$index),
                                "tool_type" => $request->input("tool_type.".$index),
                                "tool_serial_no" => $request->input("tool_serial_no.".$index),
                                "debit_max" => $request->input("debit_max.".$index),
                                "tool_media" => $request->input("tool_media.".$index),
                                "nozzle_count" => $request->input("nozzle_count.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "error_bkd" => $request->input("error_bkd.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                                "repeatability_bkd" => $request->input("repeatability_bkd.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Depth Tape') {
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "rentang_ukur" => $request->input("rentang_ukur.".$index),
                                "suhu_dasar" => $request->input("suhu_dasar.".$index),
                                "panjang_sebenarnya" => $request->input("panjang_sebenarnya.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'UTI Meter') {
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "rentang_ukur" => $request->input("rentang_ukur.".$index),
                                "suhu_dasar" => $request->input("suhu_dasar.".$index),
                                "panjang_sebenarnya" => $request->input("panjang_sebenarnya.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUInspectionSuhu::where("order_id", $id)->delete();

                    if ($request->has('suhu_id')) {
                        foreach($request->get('suhu_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionSuhu::insert([
                                "order_id" => $id,
                                "penunjukan" => $request->input("penunjukan.".$index),
                                "penunjukan_standar" => $request->input("penunjukan_standar.".$index),
                                "koreksi" => $request->input("koreksi.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice') {
                    ServiceOrderUttpTTUInspectionBadanHitung::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('badanhitung_item_id')) {
                        foreach($request->get('badanhitung_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionBadanHitung::insert([
                                "order_id" => $id,
                                "type" => $request->input("type.".$index),
                                "serial_no" => $request->input("serial_no.".$index),
                            ]);
                        }
                    }

                    /*
                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "tank_no" => $request->input("tank_no"),
                        "tag_no" => $request->input("tag_no"),
                    ]);
                    */
                    
                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "suhu_pengujian" => $request->input("suhu_pengujian"),
                        "ketidakpastian" => $request->input("ketidakpastian"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "d_inner" => $request->input("d_inner.".$index),
                                "d_outer" => $request->input("d_outer.".$index),
                                "e_inner" => $request->input("e_inner.".$index),
                                "e_outer" => $request->input("e_outer.".$index),
                                "alpha" => $request->input("alpha.".$index),
                                "roundness" => $request->input("roundness.".$index),
                                "flatness" => $request->input("flatness.".$index),
                                "roughness" => $request->input("roughness.".$index),
                                "eksentrisitas" => $request->input("eksentrisitas.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "input_pct" => $request->input("input_pct.".$index),
                                "input_level" => $request->input("input_level.".$index),
                                "actual" => $request->input("actual.".$index),
                                "output_up" => $request->input("output_up.".$index),
                                "output_down" => $request->input("output_down.".$index),
                                "error_up" => $request->input("error_up.".$index),
                                "error_down" => $request->input("error_down.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'EVC') {
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "error" => $request->input("error.".$index),
                                "error_bkd" => $request->input("error_bkd.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Orifice') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "line_bore_size" => $request->input("line_bore_size"),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "line_bore_size" => $request->input("line_bore_size"),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Tetap Silinder Tegak') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    
                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        //"jenis_atap" => $request->input("jenis_atap"),
                        "tinggi_tangki" => $request->input("tinggi_tangki"),
                        "diameter" => $request->input("diameter"),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "jaringan_listrik" => $request->input("jaringan_listrik"),
                        "konstanta" => $request->input("konstanta"),
                        "kelas" => $request->input("kelas"),
                        "kondisi_ruangan" => $request->input("kondisi_ruangan"),
                        "trapo_ukur" => $request->input("trapo_ukur"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "tegangan" => $request->input("tegangan.".$index),
                                "frekuensi" => $request->input("frekuensi.".$index),
                                "arus" => $request->input("arus.".$index),
                                "faktor_daya" => $request->input("faktor_daya.".$index),
                                "error" => $request->input("error.".$index),
                                "ketidaktetapan" => $request->input("ketidaktetapan.".$index),
                                "jenis_item" => $request->input("jenis_item.".$index),
                            ]);
                        }
                    }
                }
            } elseif ($order->ServiceRequest->service_type_id == 6 || $order->ServiceRequest->service_type_id == 7) {
                $data['daya_baca'] = $request->get("daya_baca");
                $data['kelas_massa_kendaraan'] = $request->get("kelas_massa_kendaraan");
                $data['kelas_keakurasian'] = $request->get("kelas_keakurasian");
                $data['interval_skala_verifikasi'] = $request->get("interval_skala_verifikasi");
                $data['konstanta'] = $request->get("konstanta");
                $data['kelas_single_axle_load'] = $request->get("kelas_single_axle_load");
                $data['kelas_single_group_load'] = $request->get("kelas_single_group_load");
                $data['metode_pengukuran'] = $request->get("metode_pengukuran");
                $data['sistem_jaringan'] = $request->get("sistem_jaringan");
                $data['kelas_temperatur'] = $request->get("kelas_temperatur");
                $data['rasio_q'] = $request->get("rasio_q");
                $data['diameter_nominal'] = $request->get("diameter_nominal");
                $data['kecepatan'] = $request->get("kecepatan");
                $data['volume_bersih'] = $request->get("volume_bersih");
                $data['diameter_tangki'] = $request->get("diameter_tangki");
                $data['tinggi_tangki'] = $request->get("tinggi_tangki");
                $data['jumlah_nozzle'] = $request->get("jumlah_nozzle");
                $data['jenis_pompa'] = $request->get("jenis_pompa");
                $data['meter_daya_baca'] = $request->get("meter_daya_baca");

                $data['dasar_pemeriksaan'] = $request->get("dasar_pemeriksaan");

                $data['catatan_hasil_pemeriksaan'] = $request->get("catatan_hasil_pemeriksaan");

                $data['mulai_uji'] = date("Y-m-d", strtotime($request->get('mulai_uji')));
            
                $inspectionItems = ServiceOrderUttpInspections::where('order_id', $id)
                    ->orderBy('id', 'asc')->get();  
            
                $memenuhi = true;
                $pemeriksaan_memenuhi = true;
                foreach ($inspectionItems as $inspectionItem) {
                    if ($request->has("is_accepted_".$inspectionItem->id)) {
                        $memenuhi = $memenuhi && !($request->get("is_accepted_".$inspectionItem->id) === "tidak");
                        if ($inspectionItem->inspectionItem->is_pemeriksaan == true) {
                            $pemeriksaan_memenuhi = $pemeriksaan_memenuhi  
                                && !($request->get("is_accepted_".$inspectionItem->id) === "tidak");
                        }

                        $val = $request->get("is_accepted_".$inspectionItem->id) === "ya" ? true : 
                            ($request->get("is_accepted_".$inspectionItem->id) === "tidak" ? false : null);
                        //print_r($val);
                        
                        ServiceOrderUttpInspections::find($inspectionItem->id)->update([
                            'is_accepted' => $val,
                        ]);
                        
                    }
                }

                $data['test_by_2_sertifikat'] = true;

                $data['hasil_uji_memenuhi'] = $memenuhi;
                $data['hasil_pemeriksaan_memenuhi'] = $pemeriksaan_memenuhi;
                $data['has_set'] = $request->has('has_set');
                
                if ($request->has('has_set')) {
                    $data['set_memenuhi'] = $request->input('set_memenuhi') == 'memenuhi' ? true : false;
                }

                $mustInpects = DB::table('uttp_inspection_prices')
                    ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                    ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
                    ->leftJoin('service_order_uttps', 'service_order_uttps.service_request_item_inspection_id', '=', 'service_request_uttp_item_inspections.id')
                    ->where('uttp_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
                    ->where('uttp_inspection_price_types.uttp_type_id', $order->ServiceRequestItem->uttp->type_id)
                    ->where(function($query) {
                        $query->whereNull('service_order_uttps.ujitipe_completed')
                              ->orWhere('service_order_uttps.ujitipe_completed', false);
                    })
                    ->selectRaw('uttp_inspection_prices.id as price_id, service_order_uttps.id as order_id')
                    ->get();
                //dd([$mustInpects, $order->ServiceRequest->service_type_id, $order->ServiceRequestItem->uttp->type_id]);
                $notOrderedYet = $mustInpects->filter(function ($value, $key) {
                    return $value->order_id == null;
                });

                //dd([$mustInpects->toQuery(), $notOrderedYet]);
                if (count($notOrderedYet) == 0) {
                    $ordersToUpdate = $mustInpects->map(function ($item, $key) {
                        return $item->order_id;
                    })->toArray();
                    ServiceOrderUttps::whereIn('id', $ordersToUpdate)->update([
                        'ujitipe_completed' => true,
                    ]);

                    ServiceOrderUttpEvaluasiTipe::where("order_id", $id)->delete();
                    ServiceOrderUttpEvaluasiTipe::insert([
                        "order_id" => $id,
                        "kelas_keakurasian" => $request->input("kelas_keakurasian"),
                        "daya_baca" => $request->input("daya_baca"),
                        "interval_skala_verifikasi" => $request->input("interval_skala_verifikasi"),
                        "konstanta" => $request->get("konstanta"),
                        "kelas_single_axle_load" => $request->get("kelas_single_axle_load"),
                        "kelas_single_group_load" => $request->get("kelas_single_group_load"),
                        "metode_pengukuran" => $request->get("metode_pengukuran"),
                        "sistem_jaringan" => $request->get("sistem_jaringan"),
                        "meter_daya_baca" => $request->input("meter_daya_baca"),
                    ]);
                }


            }

            $order->update($data);

            /*
            $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
            $inspection->update([
                'status_sertifikat' => 1
            ]);
            */

            //$this->checkAndUpdateFinishOrder($id);

            if ($order->ServiceRequest->lokasi_pengujian == 'luar') {
                return Redirect::route('serviceluaruttp');
            }

            // DIPINDAH SAAT SUBMIT/KIRIM HASIL UJI
            if ($request->get("stat_sertifikat") ==  1) {
                $history = new HistoryUttp();
                $history->request_status_id = 13;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                $history->order_id = $order->id;
                $history->order_status_id = 1;
                $history->user_id = Auth::id();
                $history->save();
            }

            return Redirect::route('serviceuttp');
        } else {
            return Redirect::route('serviceuttp.result', $id);
        }
    }

    public function approve($id)
    {
        $attribute = $this->MyProjects->setup("serviceuttp");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        return view('serviceuttp.approve',compact(
            'serviceOrder', 'attribute'
        ));
    }

    public function approvesubmit($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
        ])->find($id);
        $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
        $lastNo = $serviceType->last_no;
        $nextNo = intval($lastNo)+1;
        $noSertifikat = sprintf("%d", $nextNo).'/PKTN.4.5/KHP/KN/'.date("m/Y");
        $noService = $serviceType->prefix.'-'.sprintf("%05d", $nextNo);

        ServiceOrderUttps::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            "no_service" => $noService,
            "supervisor_staff" => Auth::id(),
            "supervisor_entry_date" => date("Y-m-d"),
            "stat_service_order"=>3
        ]);

        MasterServiceType::where("id",$id)->update(["last_no"=>$nextNo]);

        $this->checkAndUpdateFinishOrder($id);
        
        return Redirect::route('serviceuttp');
    }

    public function warehouse($id)
    {
        $order = ServiceOrderUttps::find($id);
        ServiceOrderUttps::whereId($id)->update(["stat_warehouse"=>1]);

        /*
        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        $inspection->update([
            'status_uttp' => 1
        ]);
        */

        $history = new HistoryUttp();
        $history->request_status_id = 13;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = $order->stat_sertifikat;
        $history->warehouse_status_id = 1;
        $history->user_id = Auth::id();
        $history->save();

        $this->checkAndUpdateFinishOrder($id);
        
        return Redirect::route('serviceuttp');
    }

    public function download($id)
    {
        $pdfMerger = PdfMerger::init();
        $order = ServiceOrderUttps::find($id);
        date_default_timezone_set("Asia/Singapore");

        if(
            (null != $order && isset($order->kabalai_date) && 
            $order->kabalai_date < date('Y-m-d',strtotime('19-09-2024')) || 
            null == $order->kabalai_date)
        ){
            return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
        }

        if ($order->no_sertifikat == null) {
            return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
        }

        if(
            $order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5
        ){
            $this->MakeClips('app/public/sample.pdf',$order->qrcode_token,$order->no_sertifikat,$order->id);
            $no = str_replace('/','_',$order->no_sertifikat);
            $no = "Lampiran-No.".$no.".pdf";

            return Storage::disk('public')->download("uttp-lampiran/".$no);
        }
        return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
    }

    public function preview($id)
    {
        $order = ServiceOrderUttps::find($id);
       
        // $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);


        $blade = 'serviceuttp.skhp_tipe_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_ttu_pdf';
        }

        $view = true;
        //dd($order->inspections);
        return view($blade,compact(
            'order', 'qrcode_generator', 'view'
        ));
    }

    public function previewTipe($id)
    {
        $order = ServiceOrderUttps::find($id);
       
        // $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);


        $blade = 'serviceuttp.skhpt_tipe_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_ttu_pdf';
        }

        $view = true;
        //dd($order->inspections);
        return view($blade,compact(
            'order', 'qrcode_generator', 'view'
        ));
    }

    public function print($id, $stream = 0)
    {
        $order = ServiceOrderUttps::with(['ttuPerlengkapans'])
            ->find($id);
        //dd($order);
        // $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);
        $file_name = 'SKHP '.($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $next_count = $order->preview_count + 1;
        $order->update([
            'preview_count' => $next_count
        ]);

        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);
       
        $blade = 'serviceuttp.skhp_tipe_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_ttu_pdf';

                if ($order->ServiceRequestItem->uttp->type->kelompok == 'CTMS Gas' || 
                $order->ServiceRequestItem->uttp->type->kelompok == 'CTMS BBM') {
                    $blade = 'serviceuttp.skhp_ttu_ctms_pdf';
                }
                elseif ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh') {
                    $blade = 'serviceuttp.skhp_ttu_kwh_pdf';
                }
        }

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));

        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }

    public function printTipe($id, $stream = 0)
    {
        $order = ServiceOrderUttps::find($id);

        if ($order->is_skhpt) {
            $next_count = $order->preview_count + 1;
            $order->update([
                'preview_count' => $next_count
            ]);
        }

        //$file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);
        $file_name = 'SKHPT '.($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhpt',
            'token' => $order->qrcode_skhpt_token,
        ]);
        
        $blade = 'serviceuttp.skhpt_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));
     
        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }

    public function previewSETipe($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $otherOrders = ServiceOrderUttps::whereHas(
            'ServiceRequestItem', function($query) use($order)
            {
                $query->where("uttp_id",$order->ServiceRequestItem->uttp_id);  
            })
            ->get();

        $qrcode_generator = route('documentuttp.valid', $order->qrcode_tipe_token);

        $blade = 'serviceuttp.set_tipe_pdf';

        $view = true;
        //dd($order);
        return view($blade,compact(
            'order', 'otherOrders', 'qrcode_generator', 'view'
        ));
    }

    public function printSETipe($id, $stream = 0)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $file_name = 'SET '.($order->no_surat_tipe ? $order->no_surat_tipe : $order->ServiceRequestItem->no_order);

        $otherOrders = ServiceOrderUttps::whereHas(
            'ServiceRequestItem', function($query) use($order)
            {
                $query->where("uttp_id",$order->ServiceRequestItem->uttp_id);  
            })
            ->get();
       
        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'set',
            'token' => $order->qrcode_tipe_token,
        ]);

        $blade = 'serviceuttp.set_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact(['order', 'otherOrders', 'qrcode_generator', 'view']));
       
        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }

    private function checkAndUpdateFinishOrder($id) 
    {
        $order = ServiceOrderUttps::find($id);

        /*
        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        if ($inspection->status_sertifikat == 1 && $inspection->status_uttp == 1) {
            $inspection->update(['status_id' => 14]); 
        }

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_14 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
        }
        */
        if ($order->stat_warehouse == 1 && $order->stat_sertifikat == 3) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestUttpItemInspection::where("request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUttp();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                $history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }

    public function pending($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.pending',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmpending($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $order->update([
            "pending_status" => 1,
            "pending_created" => date('Y-m-d H:i:s'),
            "pending_notes" => $request->get("pending_notes"),
            //"pending_estimated" => date("Y-m-d", strtotime($request->get("pending_estimated"))),
        ]);

        ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>15]);

        $history = new HistoryUttp();
        $history->request_status_id = 15;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        //$history->order_id = $order->id;
        $history->user_id = Auth::id();
        $history->save();

        $customerEmail = $order->ServiceRequest->requestor->email;
        //Mail::to($customerEmail)->send(new Pending($order));
        ProcessEmailJob::dispatch($customerEmail, new Pending($order))->onQueue('emails');

        return Redirect::route('serviceuttp');
    }

    public function continue($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.continue',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcontinue($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $order->update([
            "pending_status" => 0,
            "pending_ended" => date('Y-m-d H:i:s'),
        ]);

        ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>12]);

        $history = new HistoryUttp();
        $history->request_status_id = 12;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        //$history->order_id = $order->id;
        $history->user_id = Auth::id();
        $history->save();

        return Redirect::route('serviceuttp');
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.cancel',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcancel($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);
        $item = ServiceRequestUttpItem::find($order->service_request_item_id);
    

        $order->update([
            "cancel_at" => date('Y-m-d H:i:s'),
            "cancel_notes" => $request->get("cancel_notes"),
            "cancel_id" => Auth::id(),
            //"stat_service_order" => 4,
            //"stat_sertifikat" => 0,
            "stat_service_order" => 1,
            "stat_sertifikat" => 1,
            "status_id" => 16,
        ]);

        $inspection = ServiceRequestUttpItemInspection::where('request_item_id', $order->service_request_item_id)
            ->update(['status_id' => 16]);
        // ($order->service_request_item_inspection_id);
        //$inspection->update(['status_id' => 16]);

        $item->status_id = 16;
        $item->save();

        $history = new HistoryUttp();
        $history->request_status_id = 16;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = 1;
        $history->user_id = Auth::id();
        $history->save();

        return Redirect::route('serviceuttp');
    }

    //Watermark
    public function MakeClips($strfilepath,$qrcode_token,$no_sertifikat,$id_order)
    {
        $fileloc='';
        $no_sertifikat ='XXX';
        $pathtoQR = storage_path('app/public/tmp/qrcode.png');;
        $order = ServiceOrderUttps::with(['ServiceRequestItem'])->find($id_order);
        // dd($order->ServiceRequestItem);
        if($order->no_sertifikat !=null){
            $no_sertifikat = "Lampiran-No.".$order->no_sertifikat;
        }

        $pdf = new \setasign\Fpdi\Tcpdf\Fpdi();
        
        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);
        
        if(file_exists($pathtoQR)){ 
            $fs = new \Illuminate\Filesystem\Filesystem;
            $fs->cleanDirectory(storage_path('app/public/tmp')) ;
            $fs->cleanDirectory(storage_path('app/public/uttp-lampiran')) ;
        }
        $qrcode =  base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->merge("/public/assets/images/logo/icon-logo-kemendag-white.png", .3)
                                ->generate($qrcode_generator,storage_path('app/public/tmp/qrcode.png')));
        
        if($qrcode){
            $pathtoQR = storage_path('app/public/tmp/qrcode.png');
        }

        $fileloc = storage_path('app/public/'.$order->path_skhp);
        // dd($fileloc);
        if(file_exists($fileloc)){ 
            $pagecount = $pdf->setSourceFile($fileloc); 
        }else{ 
            die('File PDF Tidak ditemukan.!'); 
        } 
        // Add watermark image to PDF pages 
        for($i=1;$i<=$pagecount;$i++){ 
            $tplidx = $pdf->importPage($i);
            $specs = $pdf->getTemplateSize($tplidx);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->addPage($specs['height'] > $specs['width'] ? 'P' : 'L');
            $pdf->useTemplate($tplidx, null, null, $specs['width'], $specs['height'], true);
            
            /**  Header Lampiran
             * Setting text
            */
            $pdf->SetXY(5, 5);
            $_x_text = ($specs['width']- 60) - ($pdf->GetStringWidth($order->no_sertifikat, '', '',10)/2.8);
            $_y_text = $specs['height']- $specs['height'] +5+5 - 10;

            //setting for name of kabalai
            $_x_Hkab = ($specs['width'] -100 ) - ($pdf->GetStringWidth('Nomor', '', '',10)/2.8);
            $_y_Hkab = $specs['height']-90 - 10;

            // line2
            $_x_HL2kab = ($specs['width'] -100 ) - ($pdf->GetStringWidth('Pemilik', '', '',10)/2.8);
            $_y_HL2kab = $specs['height']-86 - 10;

            // line3
            $_x_HL3kab = ($specs['width'] -100 ) - ($pdf->GetStringWidth('Tempat', '', '',10)/2.8);
            $_y_HL3kab = $specs['height']-82 - 10;

            //setting for name of kabalai
            $_x_kab = ($specs['width'] -100 ) - ($pdf->GetStringWidth( 'Kabalai', '', '',10)/2.8);
            $_y_kab = $specs['height']-49 - 10;

            //Writing nip
            $_x_Nkab = ($specs['width'] -100 ) - ($pdf->GetStringWidth( 'Kabalai', '', '',10)/2.8);
            $_y_Nkab = $specs['height']-45 - 10;
            //QR
            $_x = ($specs['width']-70) - ($pdf->GetStringWidth($order->no_sertifikat, '', '', 10)/2.8);
            $_y = $specs['height'] -15 - 10;

            // if($i ==1){
            $pdf->SetFont('', '', 9);
            $pdf->SetXY($_x_text, $_y_text);
            $pdf->setAlpha(1);
            $pdf->Write(0,"Lampiran ".$order->no_sertifikat);
            // }
            /**
             * QR Code
             */
            if($i == 1){
                $b = "Bandung".',';
                $text = $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y'));
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_Hkab, $_y_Hkab -4);
                $pdf->setAlpha(1);
                $pdf->Write(0, $b);

                $b = "Bandung".',';
                $text = $order->kabalai_date ? format_long_date($order->kabalai_date) : format_long_date(date('d-m-Y'));
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_Hkab + 17, $_y_Hkab -4);
                $pdf->setAlpha(1);
                $pdf->Write(0, $text);

                //Setting header and name kabalai
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_Hkab, $_y_Hkab);
                $pdf->setAlpha(1);
                $pdf->Write(0,"Kepala Balai Pengujian");

                //Line 2
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_HL2kab, $_y_HL2kab);
                $pdf->setAlpha(1);
                $pdf->Write(0,"Alat Ukur, Alat Takar, Alat Timbang");

                //Line 3-1
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_HL3kab, $_y_HL3kab);
                $pdf->setAlpha(1);
                $pdf->Write(0,"dan Alat Perlengkapan");

                //writing NIP
                $pdf->SetFont('', '', 10);
                $pdf->SetXY($_x_Nkab, $_y_Nkab);
                $pdf->setAlpha(1);
                $pdf->Write(0,"NIP : ". $order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nip : '' );

                //Setting header and name kabalai
                $pdf->SetFont('', 'U', 10);
                $pdf->SetXY($_x_kab, $_y_kab);
                $pdf->setAlpha(1);
                $pdf->Write(0,$order->KaBalai != null && $order->KaBalai->PetugasUttp != null? $order->KaBalai->PetugasUttp->nama : 'Kepala Balai Pengujian UTTP');
                // QR
                $pdf->Image($pathtoQR, $_x-9.8,$_y-61.3, 25, 26, 'png'); 
                
            }
        } 
        
        // Output PDF with watermark 
        $no = str_replace('/','_',$no_sertifikat);
        if($order->cancel_at != null && $order->cancel_notes !=null){
            $down = $pdf->Output(storage_path('app/public/tmp/SBL-'.$no.'.pdf'),'F');
        }else{
            $down = $pdf->Output(storage_path('app/public/uttp-lampiran/'.$no.'.pdf'),'F');
        }
        // $down = $pdf->Output(storage_path('app/public/tmp/SERTIFIKAT-'.$no.'.pdf'),'F');
        if($down){
            // Storage::delete('app/public/tmp/SERTIFIKAT-'.$no.'.pdf');
        }
    }
}
