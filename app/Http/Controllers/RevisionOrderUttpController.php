<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\MyClass\MyProjects;
use App\RevisionUttp;

use App\MasterInstalasi;

class RevisionOrderUttpController extends Controller
{
    // protected $StandardInspectionPrice;
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("revisionorderuttp");

        $laboratory_id = Auth::user()->laboratory_id;
        $instalasi_id = Auth::user()->instalasi_id;

        /*
        $rows_processing = RevisionUttp::whereIn('status', [2])
            ->join('service_order_uttps', 'service_order_uttps.id', '=', 'certificate_revision_uttps.order_id')
            ->select('certificate_revision_uttps.*')
            ->where('service_order_uttps.laboratory_id', $laboratory_id)
            ->orderBy('certificate_revision_uttps.updated_at','desc')->get();
        $rows_processed = RevisionUttp::whereIn('status', [3])
            ->join('service_order_uttps', 'service_order_uttps.id', '=', 'certificate_revision_uttps.order_id')
            ->select('certificate_revision_uttps.*')
            ->where('service_order_uttps.laboratory_id', $laboratory_id)
            ->orderBy('certificate_revision_uttps.updated_at','desc')->get();
        */

        $instalasiList = MasterInstalasi::where('lab_id', $laboratory_id)->orderBy('id','asc')
            ->get();

        foreach($instalasiList as $instalasi) {

            $rows_processing[$instalasi->id] = RevisionUttp::whereIn('status', [2])
                ->join('service_order_uttps', 'service_order_uttps.id', '=', 'certificate_revision_uttps.order_id')
                ->select('certificate_revision_uttps.*')
                ->where('service_order_uttps.laboratory_id', $laboratory_id)
                ->where(function($query) {
                    $query->where('status_revision', 0)
                        ->orWhereNull('status_revision');
                })
                ->where('service_order_uttps.instalasi_id', $instalasi->id)
                ->orderBy('certificate_revision_uttps.updated_at','desc')->get();

        }

        //$rows_processed = RevisionUttp::whereIn('status', [3])
        //    ->orderBy('updated_at','desc')->get();

        return view('revisionorderuttp.index', compact('rows_processing', 'attribute', 'instalasiList'));
    }

    public function order($id)
    {
        $attribute = $this->MyProjects->setup("revisionorderuttp");

        $revision = RevisionUttp::find($id);

        return view('revisionorderuttp.edit', compact('attribute', 'revision'));
    }

    public function simpanorder($id)
    {
        $attribute = $this->MyProjects->setup("revisionorderuttp");
        
        $revision = RevisionUttp::find($id);
        $revisionOthers = RevisionUttp::where("order_id", $revision->order_id)
            ->orderBy("id", "desc")->first();

        if ($revisionOthers == null) {
            $next = 1;
        } else {
            $next = $revisionOthers->seri_revisi + 1;
        }

        $revision->update([
            "status" => 3,
            "staff_id" => Auth::id(),
            "staff_at" => date("Y-m-d H:i:s"),
            "seri_revisi" => $next,
            // "status_approval" => $revision->others == null ? 0 : null,
            // "status_revision" => $revision->others == null ? 3 : 1,
        ]);

        return Redirect::route('revisionorderuttp');
    }

}
