<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceRequestItemInspection;
use App\ServiceRequestItem;
use App\ServiceOrders;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\HistoryUut;
use App\MasterInstalasi;

class WarehouseHistoryUutController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("warehousehistoryuut");

        $rows = ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
                ->whereHas('ServiceRequestItem', function($query) 
                {
                    $query->whereIn("status_id",[13,14,16]);
                })
                ->where('stat_warehouse', 2)
                ->get();
        
        return view('warehousehistoryuut.index',compact(['rows','attribute']));
    }    

    public function print($id)
    {
        $row = ServiceOrders::find($id);
        return view('warehousehistoryuut.pdf',compact('row'));
    }
}
