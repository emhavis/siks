<?php

namespace App\Http\Controllers;

use App\MasterTemplate;
use App\MasterLaboratory;
use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TemplatesController extends Controller
{
    protected $MasterTemplate;
    protected $MasterLaboratory;
    protected $MyProjects;

    public function __construct()
    {
        $this->MasterTemplate = new MasterTemplate();
        $this->MasterLaboratory = new MasterLaboratory();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup('templates');
        if($attribute==null) return userlogout();
        
        $rows = $this->MasterTemplate->get();
        return view('templates.index', compact('rows','attribute'));
    }

    public function create()
    {
        $attribute = $this->MyProjects->setup('templates');
        if($attribute==null) return userlogout();
        
        $laboratories = $this->MasterLaboratory->dropdown();
        return view('templates.create',compact('laboratories','attribute'));
    }
    
    public function store(Request $request)
    {

    }
    
}
