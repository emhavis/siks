<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;

use App\ServiceRequest;
use App\MasterPetugasUttp;
use App\ServiceRequestUutInsituDoc;
use App\ServiceRequestUutInsituStaff;
use App\ServiceRequestUutStaff;


use App\Customer;
use App\MasterUsers;
use App\MasterServiceType;
use App\MasterSbm;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use PDF;

use App\HistoryUut;

use App\Mail\ScheduledInsituUut;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

class SchedulingUutController extends Controller
{
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    protected $MasterNegara;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("schedulinguut");

        $rows = ServiceRequest::where('status_id', 3)
            ->orderBy('received_date','desc')->get();

        return view('schedulinguut.index',compact('rows','attribute'));
    }

    public function schedule($id)
    {
        $attribute = $this->MyProjects->setup("schedulinguut");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts','inspectionKabkot','inspectionProv'])
            ->find($id);
        // dd($request->inspectionKabkot);
        $requestor = Customer::find($request->requestor_id);
        /*
        $users = MasterPetugasUttp::where('flag_unit', 'snsu')
            ->where('is_active', true)
            ->pluck('nama', 'id');
            */
        $rate = MasterSbm::where('provinsi_id', $request->inspection_prov_id)->first();
        if ($request->inspection_kabkot_id == env('KABKOT_DALAM')) {
            $request->spuh_rate = $rate->dalam;
        } else {
            $request->spuh_rate = $rate->luar;
        }
        
        $request->spuh_rate_id = $rate->id;

        $staffes = ServiceRequestUutStaff::where("request_id", $id)->get();

        return view('schedulinguut.schedule', compact(['request', 'requestor', 'staffes', 'attribute']));
    }

    public function confirmschedule($id, Request $request)
    {
        // dd([$id, $request]);
        $response["status"] = false;

        $requestEntity = ServiceRequest::find($id);

        $rules['scheduled_test_date_from'] = ['required','date'];
        $rules['scheduled_test_date_to'] = ['required','date','after:scheduled_test_date_from'];
        // $rules['spuh_spt'] = ['required'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $data = [];
            $keys = array_keys($rules);
            foreach($request->all() as $k=>$v)
            {
                if(in_array($k,$keys))
                {
                    $data[$k] = $request->get($k);
                }
            }
            
            $data["scheduled_test_date_from"] = date("Y-m-d", strtotime($data["scheduled_test_date_from"]));
            $data["scheduled_test_date_to"] = date("Y-m-d", strtotime($data["scheduled_test_date_to"]));

            $data["scheduled_test_id"] = Auth::id();

            $data["received_date"] = $data["scheduled_test_date_from"];
            $data["estimated_date"] = $data["scheduled_test_date_to"];

            $dt1 = new \DateTime($data['scheduled_test_date_from']);
            $dt2 = new \DateTime($data['scheduled_test_date_to']);
            $interval = $dt1->diff($dt2);
            $data["spuh_staff"] = ($request->has('scheduled_id_1') ? 1 : 0) + ($request->has('scheduled_id_2') ? 1 : 0);
            
            $data["spuh_rate"] = $request->get('spuh_rate');
            $data["spuh_rate_id"] = $request->get('spuh_rate_id');
            $data["spuh_price"] = ((int)$interval->format('%a') + 1) * $data["spuh_staff"] * $data["spuh_rate"];
            $data["spuh_inv_price"] = $data["spuh_price"];
            //$data["spuh_spt"] = $request->get('spuh_spt');
            
            $data["status_id"] = 4;
            // dd($request->all());
            $doc = ServiceRequestUutInsituDoc::find($requestEntity->spuh_doc_id);
            //dd($doc);

            $staffs[] = $request->get('scheduled_id_1');
            $staffs[] = $request->get('scheduled_id_2');

            if ($doc == null) 
            {
                $doc = ServiceRequestUutInsituDoc::create([
                    "request_id" => $id,
                    //"doc_no" => $data["spuh_spt"],
                    "rate" => $data["spuh_rate"],
                    "rate_id" => $data["spuh_rate_id"],
                    "price" => $data["spuh_price"],
                    "invoiced_price" => $data["spuh_price"],
                    "staffs" => implode(";",$staffs),
                    "date_from" => $data["scheduled_test_date_from"],
                    "date_to" => $data["scheduled_test_date_to"],
                    "days" => ((int)$interval->format('%a') + 1),

                    "is_siap_petugas" => null,
                    "keterangan_tidak_siap" => null,
                    "is_accepted" => null,
                    "keterangan_tidak_lengkap" => null,
                ]);
            } else {
                ServiceRequestUutInsituDoc::whereId($requestEntity->spuh_doc_id)
                    ->update([
                    "request_id" => $id,
                    //"doc_no" => $spuh_spt,
                    "rate" => $data["spuh_rate"],
                    "rate_id" => $data["spuh_rate_id"],
                    "price" => $data["spuh_price"],
                    "invoiced_price" => $data["spuh_price"],
                    "staffs" => implode(";",$staffs),
                    "date_from" => $data["scheduled_test_date_from"],
                    "date_to" => $data["scheduled_test_date_to"],
                    "days" => ((int)$interval->format('%a') + 1),

                    "is_siap_petugas" => null,
                    "keterangan_tidak_siap" => null,
                    "is_accepted" => null,
                    "keterangan_tidak_lengkap" => null,
                ]);
            }

            ServiceRequestUutStaff::where("request_id", $id)->delete();
            ServiceRequestUutInsituStaff::where("doc_id", $doc->id)->delete();

            ServiceRequestUutStaff::insert([
                [
                    "request_id" => $id,
                    "scheduled_id" => $request->get("scheduled_id_1"),
                ],
                [
                    "request_id" => $id,
                    "scheduled_id" => $request->get("scheduled_id_2"),
                ],
            ]);

            ServiceRequestUutInsituStaff::insert([
                [
                    "doc_id" => $doc->id,
                    "scheduled_id" => $request->get("scheduled_id_1"),
                ],
                [
                    "doc_id" => $doc->id,
                    "scheduled_id" => $request->get("scheduled_id_2"),
                ]
            ]);

            $data["spuh_doc_id"] = $doc->id;
            $requestEntity->update($data);

            if ($request->get('status_submit') == 1) 
            {
                $items = ServiceRequestItem::where("service_request_id", $id)->get();
                foreach($items as $item) {
                    $history = new HistoryUut();
                    $history->request_status_id = 4;
                    $history->request_id = $id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = 4;
                    $item->save();
                }

                $requestEntity->update([
                    "status_id" => 4
                ]);

                // $staffesEmail = ServiceRequestUutStaff::where('request_id', $id)->get();
                // foreach($staffesEmail as $staffEmail) {
                //     $petugas = $staffEmail->scheduledStaff;

                //     if ($petugas != null) {
                //         $petugasEmail = $petugas->email;
                //         //Mail::to($customerEmail)->send(new BookingConfirmation($requestEntity));
                //         ProcessEmailJob::dispatch($petugasEmail, new ScheduledInsituUut($requestEntity, $petugas))
                //             ->onQueue('emails');
                //     }
                // }
            }

            $response["id"] = $requestEntity->id;
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
            
        }

        return response($response);
    }

    public function getstaffes(Request $request)  
    {
        $from = date("Y-m-d", strtotime($request->get("from")));
        $to = date("Y-m-d", strtotime($request->get("to")));
        $search = $request->get('search');

        $rows = ServiceRequest::select('service_request_uut_insitu_staff.*')
            ->join('service_request_uut_insitu_doc', 'service_request_uut_insitu_doc.id', '=', 'service_requests.spuh_doc_id')
            ->join('service_request_uut_insitu_staff', 'service_request_uut_insitu_staff.doc_id', '=', 'service_requests.spuh_doc_id')
            ->whereRaw('not (service_request_uut_insitu_doc.date_from > ? or service_request_uut_insitu_doc.date_to < ?)', [$to, $from])
            ->whereIn('service_requests.status_id', [3,4,5,6,7,8,9,10,11,12,22])
            ->distinct()
            ->get();
            
        //dd([date("Y-m-d", strtotime($request->get("from"))), date("Y-m-d", strtotime($request->get("to"))), $rows]);

        $staffes = $rows->map(function ($item) {
            return $item->scheduled_id;
        })->unique()->values()->all();
        
        $staffes = array_filter($staffes, function($value) { 
            return !is_null($value) && $value !== ''; 
        });

        $users = MasterPetugasUttp::where('is_active', true)
            //->where('flag_unit', 'uttp')
            ->whereNotIn('id', $staffes);
       

        if ($search) {
            $users = $users->whereRaw('lower(nama) like ?', '%'.strtolower($search).'%');
        }

        $users = $users->get();
        
        $response["data"] = $users;
        $response["status"] = true;
        $response["messages"] = "Data berhasil diambil";

        return response($response);
    }

    public function print($id, $stream = 0, $docId = 0)
    {
        $attribute = $this->MyProjects->setup("schedulinguut");
        
        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        if ($docId == 0) {
            $doc = ServiceRequestUutInsituDoc::find($request->spuh_doc_id);
        } elseif ($docId == -1) { 
            $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis', 'revisi')->orderBy('id', 'desc')->first();
        } else {
            $doc = ServiceRequestUutInsituDoc::find($docId);
        }

        $staffes = ServiceRequestUutInsituStaff::where('doc_id', $doc->id)->orderBy('id', 'asc')->get();
        //dd($staffes);
        $doc_inisial = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis', 'inisial')->first();

        $file_name = 'Surat Tugas '.($request->no_order ? $request->no_order : 'XXXXXX');

        $qrcode_generator = route('documentuut.valid', [
            'jenis_sertifikat' => 'surat_tugas',
            'token' => $doc->token,
        ]);
       
        $blade = 'schedulinguut.surat_tugas_pdf';

        $view = false;
        
        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo'),
            'defaultPaperSize' => 'a4',
            'defaultMediaType' => 'print',
        ])
        ->loadview($blade,compact('request', 'requestor', 'doc', 'doc_inisial', 'staffes', 'qrcode_generator', 'view'));

        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("schedulinguut");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);
        //$users = MasterUsers::pluck('full_name', 'id');
        /*
        $users = MasterPetugasUttp::where('flag_unit', 'uttp')
            ->where('is_active', true)
            ->pluck('nama', 'id');
            */
        $rate = MasterSbm::where('provinsi_id', $request->inspection_prov_id)->first();

        if ($request->inspection_kabkot_id == env('KABKOT_DALAM')) {
            $request->spuh_rate = $rate->dalam;
        } else {
            $request->spuh_rate = $rate->luar;
        }
        
        $request->spuh_rate_id = $rate->id;

        //$staffes = ServiceRequestUttpStaff::where("request_id", $id)->get();

        $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffes = ServiceRequestUutInsituStaff::where("doc_id", $doc->id)->get();


        return view('schedulinguut.cancel', compact(['request', 'requestor', 'staffes', 'attribute']));
    }

    public function cancelsave($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("schedulinguut");

        $req = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);
        
        $req->cancel_at = date("Y-m-d");
        $req->cancel_notes = $request->get('cancel_notes');
        $req->status_id = 17;
        $req->update();

        $items = ServiceRequestItem::where("service_request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUut();
            $history->request_status_id = 17;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = 17;
            $item->save();
        }

        $response["id"] = $req->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }


    public function deleteRequest(Request $request){
        $ids = array();
        $reqModel = null != $request->get('order_id') && $request->get('order_id') > 0 ? ServiceRequest::with(['items'])->find($request->get('order_id')) : [];
        
        foreach($reqModel->items as $item){
            array_push($ids, $item->id);
        }

        $changeorder = ServiceRequest::find($request->get('order_id'))->update(['status_id' => 25]);
        if($changeorder){
            ServiceRequestItem::whereIn('id',$ids)->update(['status_id' => 25]);
        }

        return $changeorder ? response('ok') : response('no');
    }
}
