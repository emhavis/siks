<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Mail\CertificateDone;
use App\Mail\Cancel;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use Maatwebsite\Excel\Facades\Excel;
use App\Excel\Exports\EvaluasiOrderInsituUTTPExport;

use PDF;

class EvaluasiOrderInsituUttpController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("evaluasiorderinsituuttp");

        $start = null;
        $end = null;

        $rows = $this->query($request);

        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
       
            //$rows = $rows->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            //$rows = $rows->where('staff_entry_datein', '>=', $start);
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            //$rows = $rows->where('staff_entry_datein', '<=', $end);
        }


        $rows = $rows->get();

        return view('evaluasiorderinsituuttp.index',compact('attribute', 'rows', 'start', 'end'));
    }

    private function query(Request $request)
    {
        $rows = ServiceOrderUttps::whereRaw('1=1')
        ->join('service_request_uttps as sru','service_order_uttps.service_request_id','=','sru.id')
        ->join('service_request_uttp_items as srui','service_order_uttps.service_request_item_id','=','srui.id')
        //->join('service_request_uttp_item_inspections as sruii','srui.id','=','sruii.request_item_id')
        ->join('master_instalasi as mi','service_order_uttps.instalasi_id','=','mi.id')
        ->join('master_request_status as s', 'srui.status_id', '=', 's.id')
        ->join('master_service_types as t', 'sru.service_type_id', '=', 't.id')
        ->leftJoin('service_order_uttp_insitu_laporan as lap', 'service_order_uttps.id', '=', 'lap.service_order_id')
        ->leftJoin('users_customers as c', 'sru.requestor_id', '=', 'c.id')
        ->leftJoin('master_provinsi as mp', 'sru.inspection_prov_id', '=', 'mp.id')
        ->leftJoin('users as us', 'service_order_uttps.lab_staff', '=', 'us.id')
        ->leftJoin('users as us1', 'service_order_uttps.test_by_1', '=', 'us1.id')
        ->leftJoin('users as us2', 'service_order_uttps.test_by_2', '=', 'us2.id')
        ->whereIn("service_order_uttps.stat_service_order",[0,1,2,3,4])
        ->where('sru.lokasi_pengujian', 'luar')
        ->where('srui.status_id', 14)
        //->select('service_order_uttps.id')
        ->addSelect(
            DB::raw(
                "distinct  
                    srui.no_order, 
                    t.service_type,
                    sru.spuh_spt, 
                    
                    sru.label_sertifikat,
                    c.full_name requestor,

                    concat(service_order_uttps.tool_brand, '/',
                    service_order_uttps.tool_model, '/',
                    service_order_uttps.tool_type, ' (',
                    service_order_uttps.tool_serial_no, ')') alat,

                    mp.nama provinsi,
                    
                    concat_ws(' & ', us1.full_name,
                    us2.full_name) penguji,

                    lap.ringkasan,
                    lap.kendala_teknis,
                    lap.kendala_non_teknis, 
                    lap.metode_tindakan,
                    lap.saran_masukan
                "));
       

        return $rows;
    }

    public function fileExport(Request $request) 
    {
        // $request = new \Illuminate\Http\Request();
        $start = null;
        $end = null;

        $rows = $this->query($request);

        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
       
            //$rows = $rows->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            //$rows = $rows->where('staff_entry_datein', '>=', $start);
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            //$rows = $rows->where('staff_entry_datein', '<=', $end);
        }
        
        $rows = $rows->get();
        $this->data = $rows;
        //DD($rows->toSql());
        return Excel::download(new EvaluasiOrderInsituUTTPExport($this->data),'Evaluasi-Order-Insitu-UTTP.xlsx');
    } 

}
