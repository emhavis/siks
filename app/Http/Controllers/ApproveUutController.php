<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

//use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrders;
use App\ServiceOrderInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\ServiceRequestItemInspection;
use App\ServiceRequestItem;
use App\MasterUsers;
use App\MasterServiceType;
use App\MasterStandardType;
use App\HistoryUut;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Mail\CertificateDone_uut;
use App\Mail\CancelUut;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;
use PdfMerger;

use App\ServiceOrderUutInsituLaporan;

class ApproveUutController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("approveuut");

        $laboratory_id = Auth::user()->laboratory_id;

        if(Auth::user()->user_role == 15){
            $approval_before = 0;
        }elseif(Auth::user()->user_role == 16){
            $approval_before = 0;
        }elseif(Auth::user()->user_role == 22){
            $approval_before = 2;
        }
        // $approval_before = Auth::user()->user_role == 16 || 
        // Auth::user()->user_role == 21 ? 1 : 2;

        $rows = ServiceOrders::with([
            'ServiceRequest', 'ServiceRequestItem', 'ServiceRequestItem.uuts',
            'MasterUsers', 'LabStaffOut',
        ])
        // ->select("service_orders.*")//","master_laboratory.sla_day","service_request_items.order_at - date('d-m-Y')")
        ->selectRaw(
            "service_orders.*, service_request_items.order_at,(
                service_request_items.order_at + interval '1' day * (
                    ( 
                        case
                            WHEN service_request_items.sla_overide > 1 THEN service_request_items.sla_overide
                            ELSE master_laboratory.sla_day
                        END
                    )
                )
                ) as date_line,
            (master_laboratory.sla_day - date_part('day',now()) - date_part('day',date(service_request_items.order_at))) as sisa_hari")
        ->join('service_requests', 'service_requests.id', '=', 'service_orders.service_request_id')
        ->join('service_request_items', 'service_request_items.id', '=', 'service_orders.service_request_item_id')
        ->join('standard_uut', 'standard_uut.id', '=', 'service_request_items.uut_id')
        ->join('master_standard_types', 'master_standard_types.id', '=', 'standard_uut.type_id')
        ->join('master_laboratory', 'master_laboratory.id', '=', 'master_standard_types.lab_id');
        
        if(Auth::user()->user_role == 16){
            $rows = $rows->where("stat_sertifikat", 1);    
        }else{
            $rows = $rows->where("stat_sertifikat", $approval_before); 
        }
        // ->where("stat_sertifikat", $approval_before)
        $rows = $rows->where("is_finish",0)->whereNull("subkoordinator_notes")
            ->orWhereRaw("subkoordinator_notes= ''")
            ->orWhereRaw("subkoordinator_notes= ' '")
            ->orderBy('staff_entry_datein','ASC')
            ->get();

        /*
        if (Auth::user()->user_role == 3) {
            $rows = $rows->where('laboratory_id', $laboratory_id)
                ->whereIn("service_requests.service_type_id", [6,7]);
        }
        if (Auth::user()->user_role == 9) {
            $rows = $rows->whereIn("service_requests.service_type_id", [4,5]);
        }
        */

        $rows_kn = $rows->filter(function ($value, $key) {
            return $value->ServiceRequest->lokasi_pengujian == 'dalam';
        });
        $rows_dl = $rows->filter(function ($value, $key) {
            return $value->ServiceRequest->lokasi_pengujian == 'luar';
        });

        // $rows = $rows->get();
        return view('approveuut.index',compact('rows','rows_kn','rows_dl','attribute'));
    }

    public function approvekalab($id)
    {
        $attribute = $this->MyProjects->setup("approveuut");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        return view('approveuut.approve',compact(
            'serviceOrder', 'attribute'
        ));
    }

    public function approvesubmitkalab($id, Request $request)
    {
        $namaLampiran='';
        $path ='';
        $order = ServiceOrders::with([
            'ServiceRequest', 
        ])->find($id);
        if($request->file('file_lampiran_kalab')){
            $file = $request->file('file_lampiran_kalab');

            $namaLampiran = $file->getClientOriginalName();

            $path = $file->store(
                'lampiran',
                'public'
            );
        }
       
        ServiceOrders::whereId($id)->update([
            "file_lampiran_kalab" => $namaLampiran,
            "path_lampiran_kalab" => $path,
            "kalab_id" => Auth::id(),
            "kalab_date" => date("Y-m-d"),
            "stat_sertifikat"=>$request->get("is_approved") === "ya" ? 2 : null,
            "kalab_notes"=>$request->get("notes")
        ]);
        
        return Redirect::route('approveuut');
    }

    private function updateapprovekalab($id)
    {
        // approval 1 = kalab

        ServiceOrders::whereId($id)->update([
            "kalab_id" => Auth::id(),
            "kalab_date" => date("Y-m-d"),
            "stat_sertifikat"=> 2,
        ]);
        
        return Redirect::route('approveuut');
    }

    public function approvesubko($id)
    {
        //approve 2
        $attribute = $this->MyProjects->setup("approveuut");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $laporan = ServiceOrderUutInsituLaporan::where('service_order_id', $id)->first();

        return view('approveuut.approve',compact(
            'serviceOrder', 'attribute', 'laporan'
        ));
    }

    public function approvesubmitsubko($id, Request $request)
    {
        $namaLampiran ='';
        $path='';

        $order = ServiceOrders::with([
            'ServiceRequest', 
        ])->find($id);

        if($order->cancel_at !=null && $order->cancel_notes !=null){

            if($request->file('file_lampiran_kalab')){
                $file = $request->file('file_lampiran_kalab');

                $namaLampiran = $file->getClientOriginalName();

                $path = $file->store(
                    'lampiran',
                    'public'
                );
            }
        
            ServiceOrders::whereId($id)->update([
                "file_review_subkoordinator" => $namaLampiran,
                "path_review_subkoordinator" => $path,
                "subkoordinator_id" => Auth::id(),
                "subkoordinator_date" => date("Y-m-d"),
                "stat_sertifikat"=>$request->get("is_approved") === "ya" ? 2 : 0,
                "subkoordinator_notes"=>$request->get("notes")
            ]);

            $history = new HistoryUut();
            $history->request_status_id = 13;
            $history->request_id = $order->service_request_id;
            $history->request_item_id = $order->service_request_item_id;
            $history->order_id = $order->id;
            $history->order_status_id = $request->get("is_approved") === "ya" ? 2 : 0;
            $history->warehouse_status_id = $order->stat_warehouse;
            $history->user_id = Auth::id();
            $history->save();

        }else{

            if($request->file('file_lampiran_kalab')){
                $file = $request->file('file_lampiran_kalab');

                $namaLampiran = $file->getClientOriginalName();

                $path = $file->store(
                    'lampiran',
                    'public'
                );
            }
        
            ServiceOrders::whereId($id)->update([
                "file_review_subkoordinator" => $namaLampiran,
                "path_review_subkoordinator" => $path,
                "subkoordinator_id" => Auth::id(),
                "subkoordinator_date" => date("Y-m-d"),
                "stat_sertifikat"=>$request->get("is_approved") === "ya" ? 2 : 0,
                "subkoordinator_notes"=>$request->get("notes")
            ]);

            $history = new HistoryUut();
            $history->request_status_id = 13;
            $history->request_id = $order->service_request_id;
            $history->request_item_id = $order->service_request_item_id;
            $history->order_id = $order->id;
            $history->order_status_id = $request->get("is_approved") === "ya" ? 2 : 0;
            $history->warehouse_status_id = $order->stat_warehouse;
            $history->user_id = Auth::id();
            $history->save();
        }
        
        return Redirect::route('approveuut');
    }

    private function updateapprovesubko($id) {
        // approval 1 = subko
        ServiceOrders::whereId($id)->update([
            "subkoordinator_id" => Auth::id(),
            "subkoordinator_date" => date("Y-m-d"),
            "stat_sertifikat"=> 2,
        ]);

        $order = ServiceOrders::find($id);

        $history = new HistoryUut();
        $history->request_status_id = 13;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = 2;
        $history->warehouse_status_id = $order->stat_warehouse;
        $history->user_id = Auth::id();
        $history->save();
    }

    public function approve($id)
    {
        //aprove 3
        $attribute = $this->MyProjects->setup("approveuut");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $laporan = ServiceOrderUutInsituLaporan::where('service_order_id', $id)->first();

        return view('approveuut.approve',compact(
            'serviceOrder', 'attribute', 'laporan'
        ));
    }

    /* public function approvesubmit($id, Request $request)
    {
        // get Order
        $order = ServiceOrders::with([
            'ServiceRequest', 'ServiceRequest.Owner'
        ])->find($id);
        // find type
        $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);

        $noSertifikat = null;
        $noService = null;
        $file_review = null;
        $path_review = null;
        $noSertifikat = null;
        $noSertifikatTipe = null;
        $noService = null;
        
        // define a service type
        $stt = $order->ServiceRequest->service_type_id;
        // Make specification on the test result
        $turun_kelas = $order->hasil_uji_memenuhi;

        // define what type of user is
        if($stt =='1'){
            if($order->ServiceRequest->requestor_id == 1){ $stt = 'IN'; }else{ $stt = "EX";}
        }else{ $stt = "KAL"; }
        // check if approval true
        if ($request->get("is_approved") === "ya" ) {
            $lastNo = $serviceType->last_no;
            // $nextNo = intval($lastNo)+1;
            $prefix = $serviceType->prefix;
            $kndl = $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'KN' : 'DL';
            
            // $lastOrder = ServiceOrders::SelectRaw("
            //         id, SPLIT_PART(no_sertifikat,'/',array_length(regexp_split_to_array('no_sertifikat','/'), 1)) as lSlash
            //     ")
            //     ->where('stat_sertifikat',3)
            //     ->orderByRaw("SPLIT_PART(no_sertifikat,'/',array_length(regexp_split_to_array('no_sertifikat','/'), 1)) DESC")
            //     ->orderBy('id','DESC')
            //     ->limit('SELECT count(*) /4 FROM service_orders')
            //     ->first();
            
            // $lNumber = intval($lastOrder->lslash);
            // define last number that we had
            // $nextNo = $lNumber + 1;
            //========================================
            // Last No
            $lastOrder = ServiceOrders::where("id", "<>", $id)
                ->whereNotNull("no_sertifikat_int")
                ->where('no_sertifikat_year', date("Y"))
                ->orderBy("no_sertifikat_int", "desc")
                ->select("no_sertifikat_int")
                ->first();
            
            if ($lastOrder != null) 
            {
                $lastNo = $lastOrder->no_sertifikat_int;
                $nextNo = intval($lastNo)+1;
            } else {
                $lastNo = 0;
                $nextNo = 1;
            }

            if ($order->cancel_at != null){
                $kndl = 'KET';
            }
            
            $noSertifikat = sprintf("%04d", $nextNo).'/25/'.$stt.'/'.date("m/Y");
            $noService = $serviceType->prefix.'-'.date("y").'-'.sprintf("%04d", $nextNo);

        }else {
            if ($request->hasFile('file_review')) {
                $file = $request->file('file_review');
                $file_review = $file->getClientOriginalName();
                $path = $file->store(
                    'review_skhp',
                    'public'
                );
                $path_review = $path;
            }

        }

        $token = $this->hashing($noSertifikat, $noService);
        $skhpttoken = $this->hashing($noSertifikatTipe, $noService);
        $sertifikat_expired ='';

        if($order->sertifikat_expired_at == null){
            $tipe_id = $order->ServiceRequestItem->uuts->type_id;
            $standard = MasterStandardType::where('id',$tipe_id)->first();
            $sertifikat_expired ='';

            if($order->sertifikat_expired_at != null){
                $sertifikat_expired = $order->sertifikat_expired_at;
            }else{
                $sertifikat_expired =date("Y-m-d", strtotime(date('Y-m-d') .'+'.$standard->jangka_waktu." year"));
            }
        }else{
            $sertifikat_expired = $order->sertifikat_expired_at;
        }
        
        $s = ServiceOrders::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            "no_service" => $noService,
            "kabalai_id" => Auth::id(),
            "kabalai_date" => date("Y-m-d"),
            "stat_sertifikat"=>$request->get("is_approved") === "ya" ? 3 : 1,
            "kabalai_notes"=>$request->get("notes"),
            "file_review_kabalai"=>$request->get("is_approved") === "ya" ? null : $file_review,
            "path_review_kabalai"=>$request->get("is_approved") === "ya" ? null : $path_review,
            "stat_service_order"=>$request->get("is_approved") === "ya" ? 3 : 1,
            "qrcode_token"=>$token,
            "qrcode_skhpt_token" => $skhpttoken,
            "sertifikat_expired_at" => $sertifikat_expired,
        ]);
        // Update No
        $serviceType->update(["last_no" => $nextNo]);

        $history = new HistoryUut();
        $history->request_status_id = 13;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = $request->get("is_approved") === "ya" ? 3 : 0;
        $history->warehouse_status_id = $order->stat_warehouse;
        $history->user_id = Auth::id();
        $history->save();

        if ($order->ujitipe_completed == true) {
            $noSuratTipe = 'ET.'.sprintf("%04d", $nextNo).'/PKTN.4.8/'.date("m/Y");
            $mustInspects = DB::table('uut_inspection_prices')
                    ->join('uut_inspection_price_types', 'uut_inspection_price_types.inspection_price_id', '=', 'uut_inspection_prices.id')
                    ->leftJoin('service_request_item_inspections', 'service_request_item_inspections.inspection_price_id', '=', 'uut_inspection_prices.id')
                    ->leftJoin('service_orders', 'service_orders.service_request_item_inspection_id', '=', 'service_request_item_inspections.id')
                    ->where('uut_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
                    ->where('uut_inspection_price_types.uuts_type_id', $order->ServiceRequestItem->uut->type_id)
                    ->where(function($query) {
                        $query->where('service_orders.ujitipe_completed', true);
                    })
                    ->selectRaw('uut_inspection_prices.id as price_id, service_orders.id as order_id')
                    ->get();
            $ordersToUpdate = $mustInspects->map(function ($item, $key) {
                return $item->order_id;
            })->toArray();
            ServiceOrders::whereIn('id', $ordersToUpdate)->update([
                'no_surat_tipe' => $noSuratTipe
            ]);
        }

        if ($request->get("is_approved") === "ya" ) {
            $this->checkAndUpdateFinishOrder($id);

            $customerEmail = $order->ServiceRequest->requestor->email;

            if ($order->cancel_at == null) {
                //Mail::to($customerEmail)->send(new CertificateDone_uut($order));
                ProcessEmailJob::dispatch($customerEmail, new CertificateDone_uut($order))->onQueue('emails');
            } else {
                //Mail::to($customerEmail)->send(new Cancel($order));
                ProcessEmailJob::dispatch($customerEmail, new CancelUut($order))->onQueue('emails');
            }
             
        }
        
        return Redirect::route('approveuut');
    } **/

    public function approvesubmit($id, Request $request)
    {
        DB::transaction(function () use ($id, $request) {
            // Get Order
            $order = ServiceOrders::with([
                'ServiceRequest', 'ServiceRequest.Owner'
            ])->find($id);
            
            if (!$order) {
                throw new \Exception("Order not found");
            }
            
            // Find Service Type
            $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
            
            // Define Service Type and Prefix
            $stt = $order->ServiceRequest->service_type_id;
            if ($stt == '1') {
                $stt = $order->ServiceRequest->requestor_id == 1 ? 'IN' : 'EX';
            } else {
                $stt = 'KAL';
            }

            $kndl = $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'KN' : 'DL';
            if ($order->cancel_at != null) {
                $kndl = 'KET';
            }

            if ($request->get("is_approved") === "ya") {
                // Get Last Number with Lock
                $lastOrder = ServiceOrders::where("id", "<>", $id)
                    ->whereNotNull("no_sertifikat_int")
                    ->where("no_sertifikat_year", date("Y"))
                    ->orderBy("no_sertifikat_int", "desc")
                    ->select("no_sertifikat_int")
                    ->lockForUpdate()
                    ->first();
                
                $lastNo = $lastOrder ? $lastOrder->no_sertifikat_int : 0;
                $nextNo = $lastNo + 1;
                
                // Generate Certificate Number
                $noSertifikat = sprintf("%04d", $nextNo) . "/25/" . $stt . "/" . date("m/Y");
                $noService = $serviceType->prefix . "-" . date("y") . "-" . sprintf("%04d", $nextNo);

                // Update Order
                ServiceOrders::whereId($id)->update([
                    "no_sertifikat" => $noSertifikat,
                    "no_sertifikat_int" => $nextNo,
                    "no_sertifikat_year" => date("Y"),
                    "no_service" => $noService,
                    "kabalai_id" => Auth::id(),
                    "kabalai_date" => date("Y-m-d"),
                    "stat_sertifikat" => 3,
                    "kabalai_notes" => $request->get("notes"),
                    "stat_service_order" => 3,
                    "qrcode_token" => $this->hashing($noSertifikat, $noService),
                    "sertifikat_expired_at" => $this->getSertifikatExpiredDate($order)
                ]);
                
                // Update Last Number in Service Type
                $serviceType->update(["last_no" => $nextNo]);
            } else {
                // Handle File Review Upload
                if ($request->hasFile('file_review')) {
                    $file = $request->file('file_review');
                    $file_review = $file->getClientOriginalName();
                    $path_review = $file->store('review_skhp', 'public');
                    
                    ServiceOrders::whereId($id)->update([
                        "file_review_kabalai" => $file_review,
                        "path_review_kabalai" => $path_review,
                        "stat_sertifikat" => 1,
                        "stat_service_order" => 1,
                        "kabalai_notes" => $request->get("notes")
                    ]);
                }
            }
            
            // Save History
            HistoryUut::create([
                "request_status_id" => 13,
                "request_id" => $order->service_request_id,
                "request_item_id" => $order->service_request_item_id,
                "order_id" => $order->id,
                "order_status_id" => $request->get("is_approved") === "ya" ? 3 : 0,
                "warehouse_status_id" => $order->stat_warehouse,
                "user_id" => Auth::id()
            ]);
            
            // Check Completion and Send Email
            if ($request->get("is_approved") === "ya") {
                $this->checkAndUpdateFinishOrder($id);
                $customerEmail = $order->ServiceRequest->requestor->email;
                $emailJob = $order->cancel_at == null ? new CertificateDone_uut($order) : new CancelUut($order);
                ProcessEmailJob::dispatch($customerEmail, $emailJob)->onQueue('emails');
            }
        });
        
        return Redirect::route('approveuut');
    }

    private function getSertifikatExpiredDate($order)
    {
        if ($order->sertifikat_expired_at) {
            return $order->sertifikat_expired_at;
        }
        
        $tipe_id = $order->ServiceRequestItem->uuts->type_id;
        $standard = MasterStandardType::find($tipe_id);
        return date("Y-m-d", strtotime("+{$standard->jangka_waktu} year"));
    }
    private function hashing($username, $password) {
        $token = Hash::make($username.':'.$password);
        if (strpos($token, '/') !== false) {
            $token = $this->hashing($username, $password);
        } 
        return $token;
    }

    public function approvesubmitall(Request $request)
    {
        $ids = explode(",", $request->get('ids'));
        $orders = ServiceOrders::with([
            'ServiceRequest', 
        ])->whereIn('id', $ids)->get();

        foreach ($orders as $order) {
            if ($order->stat_sertifikat == null) {
                $this->updateapprovekalab($order->id);
                /* if ($order->ServiceRequest->service_type_id == 6 || $order->ServiceRequest->service_type_id == 7) {
                    // approval 1 == kalab
                    $this->updateapprovekalab($order->id);
                } elseif ($order->ServiceRequest->service_type_id == 4 || $order->ServiceRequest->service_type_id == 5) {
                    // approval 1 == subko
                    $this->updateapprovesubko($order->id);
                } */
            } elseif ($order->stat_sertifikat == 1 ||$order->stat_sertifikat == 0) {
                // approval 2 == subko --> mestinya tidak ada
                $this->updateapprovesubko($order->id);
            } elseif ($order->stat_sertifikat == 2) {
                // approval 3 == kabalai
                $this->updateapprove($order->id, $order);
            }
        }
 
        return Redirect::route('approveuut');
    }

    private function updateapprove($id, $order) {
        /* $noSertifikat = null;
        $noService = null;

        $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
        $lastNo = $serviceType->last_no;
        $nextNo = intval($lastNo)+1;
        $kndl = $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'KN' : 'DL';
        //$kndl = 'KN';
        $noSertifikat = sprintf("%d", $nextNo).'/PKTN.4.8/KHP/'.$kndl.'/'.date("m/Y");
        $noService = $serviceType->prefix.'-'.date("y").'-'.sprintf("%05d", $nextNo);

        ServiceOrders::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            "no_service" => $noService,
            "kabalai_id" => Auth::id(),
            "kabalai_date" => date("Y-m-d"),
            "stat_sertifikat"=>3,
            "stat_service_order"=>3
        ]);
        $history = new HistoryUut();
        $history->request_status_id = 13;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = 3;
        $history->warehouse_status_id = $order->stat_warehouse;
        $history->user_id = Auth::id();
        $history->save();

        MasterServiceType::where("id",$serviceType->id)->update(["last_no"=>$nextNo]);

        $inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
        dd($inspection);
        $inspection->update(['status_sertifikat' => 1]); **/

        $order = ServiceOrders::with([
            'ServiceRequest', 'ServiceRequest.Owner'
        ])->find($id);

        $noSertifikat = null;
        $noService = null;
        $noSertifikat = null;
        $noSertifikatTipe = null;
        $noService = null;
        
        $stt = $order->ServiceRequest->service_type_id;
        $turun_kelas = $order->hasil_uji_memenuhi;
        if($stt =='1'){
            if($order->ServiceRequest->requestor_id == 1){
                $stt = 'IN';
            }else{
                $stt = "EX";
            }
        }else{
            $stt = "KAL";
        }
        
        $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
        $lastNo = $serviceType->last_no;
        $nextNo = intval($lastNo)+1;
        $prefix = $serviceType->prefix;
        $kndl = $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'KN' : 'DL';

        if ($order->cancel_at != null){
            $kndl = 'KET';
        }
        //$kndl = 'KN';
        $noSertifikat = sprintf("%04d", $nextNo).'/25/'.$stt.'/'.date("m/Y");
        $noService = $serviceType->prefix.'-'.date("y").'-'.sprintf("%04d", $nextNo);
       

        $token = $this->hashing($noSertifikat, $noService);
        $skhpttoken = $this->hashing($noSertifikatTipe, $noService);
        $sertifikat_expired ='';

        if($order->sertifikat_expired_at == null){
            $tipe_id = $order->ServiceRequestItem->uuts->type_id;
            $standard = MasterStandardType::where('id',$tipe_id)->first();
            $sertifikat_expired ='';

            if($order->sertifikat_expired_at != null){
                $sertifikat_expired = $order->sertifikat_expired_at;
            }else{
                $sertifikat_expired =date("Y-m-d", strtotime(date('Y-m-d') .'+'.$standard->jangka_waktu." year"));
            }
        }else{
            $sertifikat_expired = $order->sertifikat_expired_at;
        }

        $s = ServiceOrders::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            "no_service" => $noService,
            "kabalai_id" => Auth::id(),
            "kabalai_date" => date("Y-m-d"),
            "stat_sertifikat"=> 3,
            "stat_service_order"=> 3,
            "qrcode_token"=>$token,
            "qrcode_skhpt_token" => $skhpttoken,
            "sertifikat_expired_at" => $sertifikat_expired,
        ]);

        $history = new HistoryUut();
        $history->request_status_id = 13;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = 3;
        $history->warehouse_status_id = $order->stat_warehouse;
        $history->user_id = Auth::id();
        $history->save();

        $this->checkAndUpdateFinishOrder($id);
    }

    public function download($id)
    {
        $order = ServiceOrders::find($id);

        return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
    }

    public function cancelation_download($id)
    {
        $order = ServiceOrders::find($id);

        return Storage::disk('public')->download($order->cancelation_file, $order->cancelation_file);
    }

    public function downloadLampiran($id)
    {
        $order = ServiceOrders::find($id);

        return Storage::disk('public')->download($order->path_lampiran, $order->file_lampiran);
    }

    public function preview($id)
    {
        $order = ServiceOrders::find($id);
        
         $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = route('documentuut.valid', ['jenis_sertifikat' => 'skhp', 'token' => $order->qrcode_token]);

        $blade ='serviceuut.skhp_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([

            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo'),
            'defaultPaperSize' => 'a4',
            'defaultMediaType' => 'print',
        ])
            ->loadview($blade, compact('order', 'qrcode_generator', 'view'));

        return $pdf->stream();
    }

    public function print($id)
    {
        $pdfMerger = PDFMerger::init();
        $order = ServiceOrders::find($id);
        
        $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = route('documentuut.valid', $order->qrcode_token);
        
        $blade ='serviceuut.skhp_tipe_pdf';
        $view = false;

        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo'),
            'defaultPaperSize' => 'a4',
            'defaultMediaType' => 'print',
        ])
            ->loadview($blade, compact('order', 'qrcode_generator', 'view'));
        $file = public_path('storage/images/file.pdf');

        if(! file_exists($file)){
            Storage::delete('public/tmp/file.pdf');
        }
        
        Storage::put('public/tmp/file.pdf', $pdf->output());
        $pdfMerger->addPDF(storage_path('app/public/tmp/file.pdf'),'all');
        if($order->path_lampiran !=null){
            $pdfMerger->addPDF(storage_path('app/public/'.$order->path_lampiran),'all');
            $pdfMerger->merge();

            Storage::delete('public/tmp/file.pdf');

            return $pdfMerger->save("SERTIFIKAT.pdf", "download");
        }

        return $pdf->stream();
    }

    private function checkAndUpdateFinishOrder($id) 
    {
        $order = ServiceOrders::find($id);

        /*
        $inspection = ServiceRequestItemInspection::find($order->service_request_item_inspection_id);
        if ($inspection->status_sertifikat == 1 && $inspection->status_uut == 1) {
            $inspection->update(['status_id' => 14]); 
        }

        $inspections_count = DB::table('service_request_item_inspections')
            ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_14 == $inspections_count[0]->count_all) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>14]);
        }
        */
        if(
            // $order->stat_warehouse ==2 
            (
                ($order->ServiceRequest->lokasi_pengujian == 'dalam' && $order->stat_warehouse == 2) 
                 ||
                 ($order->ServiceRequest->lokasi_pengujian == 'luar' 
                    && $order->stat_warehouse == -1
                    && $order->ServiceRequest->payment_valid_date != null)
            )
            && $order->stat_sertifikat ==3){
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestItemInspection::where("service_request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequest::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUut();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                //$history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }

    public function merge($files, $outputPath)
    {
        // this for usage
        /*
        $files = [Storage::disk('public')->path("tmp/file.pdf"), Storage::disk('public')->path('skhp/'.$order->path_skhp)];
        $this->merge($files,Storage::disk('local')->path('newtest.pdf'));
        */
        $fpdi = new FPDI;
        foreach ($files as $file) {
            $filename  = $file;
            $count = $fpdi->setSourceFile($filename);
            for ($i=1; $i<=$count; $i++) {
                $template   = $fpdi->importPage($i);
                $size       = $fpdi->getTemplateSize($template);
                $fpdi->AddPage($size['orientation'], array($size['width'], $size['height']));
                $fpdi->useTemplate($template);
            }
 
        }
        $fpdi->Output($outputPath, 'F');
    }

    public function downloadReview($type, $id)
    {
        $order = ServiceOrders::find($id);

        if ($type == 'subko') {
            return Storage::disk('public')->download($order->path_review_subkoordinator, $order->file_review_subkoordinator);
        } else {
            return Storage::disk('public')->download($order->path_review_kabalai, $order->file_review_kabalai);
        }
    }
}
