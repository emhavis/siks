<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceRequestItemInspection;
use App\ServiceRequestItem;
use App\ServiceOrders;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\HistoryUut;
use App\MasterInstalasi;

class WarehouseReceiveUutController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("warehousereceiveuut");

        $rows = ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
                ->whereHas('ServiceRequestItem', function($query) 
                {
                    $query->where("status_id",13);
                })
                ->where('stat_warehouse', 0)
                ->orderBy('staff_entry_datein','desc')
                ->get();
        
        return view('warehousereceiveuut.index',compact(['rows','attribute']));
    }    

    public function warehouse(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        if ($request->has('id')) {
            $order = ServiceOrders::find($request->id);
        } else {
            $order = ServiceOrders::whereHas('ServiceRequestItem', function ($query) use ($request) {
                    $query->where('no_order', $request->no_order);
                })->first();
        }

        if ($order != null) {
            //$order = ServiceOrderUttps::find($id);
            ServiceOrders::whereId($order->id)->update([
                "stat_warehouse"=>1,
                "warehouse_in_id" => Auth::id(),
                "warehouse_in_at" => date("Y-m-d H:i:s.u"),
            ]);

            /*
            $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
            $inspection->update([
                'status_uttp' => 1
            ]);
            */

            $history = new HistoryUut();
            $history->request_status_id = 13;
            $history->request_id = $order->service_request_id;
            $history->request_item_id = $order->service_request_item_id;
            $history->order_id = $order->id;
            $history->order_status_id = $order->stat_sertifikat;
            $history->warehouse_status_id = 1;
            $history->user_id = Auth::id();
            $history->save();

            //$this->checkAndUpdateFinishOrder($order->id);

            $response["status"] = true;
            $response["messages"] = "Alat telah diterima pemilik/pemohon";
        }
        
        return response($response);
    }

    private function checkAndUpdateFinishOrder($id) 
    {
        $order = ServiceOrders::find($id);

        if ($order->stat_warehouse == 2 && $order->stat_sertifikat == 3) {
            ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestItemInspection::where("service_request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequest::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUut();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                //$history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }
}
