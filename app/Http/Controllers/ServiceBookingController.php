<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Request as FacadeRequest;
// use App\Http\Repositories\UmlStandardRepository;

use App\ServiceBooking;
// use Redirect;
use Auth;
use Validator;

class ServiceBookingController extends Controller
{
    protected $ServiceBooking;

    public function __construct()
    {
        $this->ServiceBooking = new ServiceBooking();
    }

    public function getbyid($id)
    {
        $booking = $this->ServiceBooking->with([
            'Pic', 
            'items', 
            'items.Standard',
            'items.inspections',
            'items.inspections.Price'])->find($id);
        return response($booking);
    }  
}
?>