<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\ServiceOrderUttps;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class OrderLuarUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("orderluaruttp");

        // LABORATORY_ID === STANDARD_MEASUREMENT_TYPE_ID
        // ID HARUS SAMA
        $laboratory_id = Auth::user()->laboratory_id;
        $instalasi_id = Auth::user()->instalasi_id;
        
        $rows = ServiceRequestUttpItemInspection::whereNull("service_request_uttp_item_inspections.status_id")
        ->orWhere("service_request_uttp_item_inspections.status_id", 11)
        ->join('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_request_uttp_item_inspections.request_item_id')
        ->join('service_request_uttps', 'service_request_uttps.id', '=', 'service_request_uttp_items.request_id')
        ->with([
            'item',
            'item.serviceRequest' => function ($query)
            {
                $query->orderBy('id', 'asc');
            }
        ])
        ->whereHas('inspectionPrice.instalasi', function($query)use($laboratory_id, $instalasi_id)
        {
            $query->where("lab_id",$laboratory_id);
                //->where("id",$instalasi_id);
        })
        ->where('service_request_uttps.scheduled_test_id', Auth::id())
        ->orderBy('service_request_uttps.id', 'asc')
        ->orderBy('service_request_uttp_items.id', 'asc')
        ->orderBy('service_request_uttp_item_inspections.id', 'asc')
        ->select('service_request_uttp_item_inspections.*')
        ->get();

        return view('orderluaruttp.index',compact('rows','attribute','laboratory_id'));
    }    

    public function proses($id)
    {
        $inspection = ServiceRequestUttpItemInspection::with([
            'inspectionPrice', 
            'item',
            'item.serviceRequest'
        ])
            ->find($id);

        ServiceOrderUttps::insert([
            "laboratory_id"=>Auth::user()->laboratory_id, //===UML_STANDARD_ID
            "instalasi_id"=>$inspection->inspectionPrice->instalasi_id,
            "service_request_id"=>$inspection->item->request_id,
            "service_request_item_id"=>$inspection->request_item_id,
            "service_request_item_inspection_id"=>$inspection->id,
            "lab_staff"=>Auth::id(),
            "staff_entry_datein"=>date("Y-m-d")
        ]);

        $inspection->update(["status_id"=>12]);

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id >= 12 then 1 else 0 end) count_12, count(id) count_all')
            ->where('request_item_id', $inspection->request_item_id)->get();
        
        if ($inspections_count[0]->count_12 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::whereId($inspection->request_item_id)
                ->update(["status_id"=>12]); // PENGUJIAN 
        }

        $items_count = DB::table('service_request_uttp_items')
            ->selectRaw('sum(case when status_id >= 12 then 1 else 0 end) count_12, count(id) count_all')
            ->where('request_id', $inspection->item->request_id)->get();

        if ($items_count[0]->count_12 == $items_count[0]->count_all) {
            ServiceRequestUttp::whereId($inspection->item->request_id)
                ->update(["status_id"=>12]); // PENGUJIAN
        }
        /*
        ServiceRequestUttp::whereId($inspection->item->request_id)
        ->update(["status_id"=>6]); //ON PROSES

        ServiceRequestUttpItem::whereId($inspection->requet_item_id)
        ->update(["status_id"=>6]); //ON PROSES

        */
        

        return Redirect::route('orderuttp');
    }

    public function takeorder($id)
    {
        $attribute = $this->MyProjects->setup("takeorder");

        $request = ServiceRequestUttps::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        return view('requestuttp.edit_booking', compact('request', 'attribute'));
    }
}
