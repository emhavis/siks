<?php

namespace App\Http\Controllers;

// use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Request as FacadeRequest;
// use App\Http\Repositories\StandardRepository;
// use App\Http\Repositories\UmlRepository;
// use App\Http\Repositories\QrcodeRepository;

use App\StandardMeasurementType;
use App\MasterProfilUml;
use App\MasterSubUml;
use App\MasterUml;
use App\Qrcode;
use App\MyClass\MyProjects;

use Validator;
use Auth;
// use Redirect;
// use Session;
// use URL;
// use Log;
// use PDF;
// use Storage;

class QrcodeController extends Controller
{
 //    protected $standardRepo;
 //    protected $umlRepo;
	protected $MyProjects;
    protected $Qrcode;
    protected $MasterUml;
    protected $MasterSubUml;
    protected $MasterProfilUml;
    protected $StandardMeasurementType;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        $this->Qrcode = new Qrcode();
        $this->MasterUml = new MasterUml();
        $this->MasterSubUml = new MasterSubUml();
        $this->MasterProfilUml = new MasterProfilUml();
        $this->StandardMeasurementType = new StandardMeasurementType();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("qrcode");
        if($attribute==null) return userlogout();

        return view('qrcode.index',compact('attribute'));
    }

    public function getdata(Request $request)
    {
        $totalData = $this->Qrcode->get()->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];
        // $dir = $request->input('order.0.dir');

        $search = empty($request->input('search.value'))?null:$request->input('search.value');

        $posts = $this->Qrcode
        ->when($search, function($query, $search){
            return $query->where('tool_code', 'LIKE',"%{$search}%");
        })
        ->offset($start)
        ->limit($limit)
        ->get();

        $totalFiltered = $this->Qrcode
        ->when($search, function($query, $search){
            return $query->where('tool_code', 'LIKE',"%{$search}%");
        })
        ->count();

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $nestedData['uml_name'] = $post->masteruml->uml_name;
                $nestedData['uml_sub_name'] = $post->mastersubuml->uml_sub_name;
                $nestedData['tool_code'] = $post->tool_code;
                $nestedData['standard_type'] = $post->standardmeasurementtype->standard_type;
                $data[] = $nestedData;
            }
        }

        $json_data = 
        [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        ];

        return response($json_data);
    }

    public function create($id=null)
    {
        $attribute = $this->MyProjects->setup("qrcode");
        if($attribute==null) return userlogout();

        $standardMeasurementTypes = $this->StandardMeasurementType->dropdown();
        $umls = $this->MasterUml->dropdown();

        return view('qrcode.create', compact('standardMeasurementTypes','umls','attribute'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;

        $rules["uml_id"] = ['required'];
        $rules["uml_sub_id"] = ['required'];
        $rules["standard_measurement_type_id"] = ['required'];
        $rules["qrcode_list"] = ['required','numeric','min:1'];

        $validation = Validator::make($request->all(),$rules, error_messages());
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $uml_id = $request->uml_id;
            $kode_daerah = $this->MasterProfilUml
            ->whereHas('masteruml',function($query) use ($uml_id)
            {
                $query->where("id",$uml_id);
            })
            ->value("kode_daerah");

            if($kode_daerah==null)
            {
                $response["messages"] = [];
                $response["messages"]["uml_id"] = "UML tidak mempunyai Kode Daerah";
            }
            else
            {
                $uml_sub = $this->MasterSubUml
                ->where("uml_sub.id",$request->uml_sub_id)
                ->value("no_sub_uml");

                $like_tool_code = $kode_daerah.'.'.$uml_sub.'.'.sprintf("%02d",$request->standard_measurement_type_id).'.%';

                $count_code = $this->Qrcode
                ->where('tool_code','like',$like_tool_code)
                ->count();

                if($count_code>0)
                {
                    $last_code = $this->Qrcode
                    ->where('tool_code',"like",$like_tool_code)
                    ->orderBy('id',"desc")
                    ->first();

                    $start_code = intval(substr($last_code->tool_code, -4, 4))+1;
                }
                else
                {
                    $start_code = 1;
                }

                $ii=intval($request->qrcode_list);

                $indata = array();

                for($i=0;$i<$ii;$i++)
                {
                    $indata[] = array(
                        "tool_code"=>$kode_daerah.'.'.$uml_sub.'.'.sprintf("%02d",$request->standard_measurement_type_id).'.'.sprintf("%04d",$start_code),
                        "uml_id"=>$request->uml_id,
                        "uml_sub_id"=>$request->uml_sub_id,
                        "standard_measurement_type_id"=>$request->standard_measurement_type_id
                    );
                    $start_code++;
                }

                $this->Qrcode->insert($indata);

                $response["status"] = true;
                $response["messages"] = "Data berhasil disimpan";
            }
        }

        return response($response);
    }

    public function show()
    {
        $attribute = $this->MyProjects->setup();
        if($attribute==null) return userlogout();
        
        $standardMeasurementTypes = $this->StandardMeasurementType->dropdown();
        $umls = $this->MasterUml->dropdown();

        return view('qrcode.show', compact(
          'standardMeasurementTypes',
          'attribute',
          'umls'));
    }

    public function dropdown(Request $request)
    {
        $qrcodefordropdown = $this->Qrcode
        ->where('uml_id',$request->uml_id)
        ->where('uml_sub_id',$request->uml_sub_id)
        ->where('standard_measurement_type_id',$request->standard_measurement_type_id)
        ->pluck('tool_code','id');

        echo json_encode(array(true,$qrcodefordropdown));
    }

    public function prints(Request $request)
    {
        $qrcode_list =  $this->Qrcode
        ->where('uml_id',$request->uml_id)
        ->where('uml_sub_id',$request->uml_sub_id)
        ->where('standard_measurement_type_id',$request->standard_measurement_type_id)
        ->whereBetween('id', [$request->from_qrcode, $request->to_qrcode])
        ->pluck('tool_code');

        echo json_encode($qrcode_list);

        // $customPaper = array(0,0,200,1000);
        // $pdf = PDF::loadview('qrcode.qrcode_pdf',compact('qrcode_list'))->setPaper($customPaper, 'portrait');
        // $pdf = PDF::loadview('qrcode.qrcode_pdf',compact('qrcode_list'));

        // Storage::put('public/pdf/'.$file_name.'.pdf', $pdf->output());

        // return $pdf->download($file_name.'.pdf');
    }

    public function qrcodeinfoByUml(Request $request)
    {
        $umlstandard = $this->qrcode
        ->leftJoin("uml","uml.id","=","qrcodes.uml_id")
        ->leftJoin("uml_sub","uml_sub.id","=","qrcodes.uml_sub_id")
        ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","qrcodes.standard_measurement_type_id")

        ->where('tool_code',$request->tool_code)
        ->where('qrcodes.uml_id',$request->uml_id)
        ->select("qrcodes.*","uml_name","uml_sub_name","standard_type")
        ->get();

        if(count($umlstandard)>0)
        {
            echo json_encode($umlstandard);
        }
        else
        {
            echo json_encode(array(false,array("tool_code2"=>"QR CODE tidak ditemukan")));
        }
    }

    public function info(Request $request)
    {
        $umlstandard = $this->Qrcode
        ->leftJoin("uml","uml.id","=","qrcodes.uml_id")
        ->leftJoin("uml_sub","uml_sub.id","=","qrcodes.uml_sub_id")
        ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","qrcodes.standard_measurement_type_id")

        ->where('tool_code',$request->tool_code)
        ->select("qrcodes.*","uml_name","uml_sub_name","standard_type")
        ->get();
 
        return response($umlstandard);
    }

}
