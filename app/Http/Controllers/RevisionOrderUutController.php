<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\MyClass\MyProjects;
use App\RevisionUut;

class RevisionOrderUutController extends Controller
{
    // protected $StandardInspectionPrice;
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("revisionorderuut");

        $rows_processing = RevisionUut::whereIn('status', [2])
            ->orderBy('updated_at','desc')->get();
        $rows_processed = RevisionUut::whereIn('status', [3])
            ->orderBy('updated_at','desc')->get();

        return view('revisionorderuut.index', compact('rows_processing', 'rows_processed', 'attribute'));
    }

    public function order($id)
    {
        $attribute = $this->MyProjects->setup("revisionorderuttp");

        $revision = RevisionUttp::find($id);

        return view('revisionorderuttp.edit', compact('attribute', 'revision'));
    }

    public function simpanorder($id)
    {
        $attribute = $this->MyProjects->setup("revisionorderuttp");
        
        $revision = RevisionUttp::find($id);
        $revisionOthers = RevisionUttp::where("order_id", $revision->order_id)
            ->orderBy("id", "desc")->first();

        if ($revisionOthers == null) {
            $next = 1;
        } else {
            $next = $revisionOthers->seri_revisi + 1;
        }

        $revision->update([
            "status" => 3,
            "staff_id" => Auth::id(),
            "staff_at" => date("Y-m-d H:i:s"),
            "seri_revisi" => $next,
            "status_approval" => $revision->others == null ? 0 : null,
            "status_revision" => $revision->others == null ? 3 : 1,
        ]);

        return Redirect::route('revisionorderuttp');
    }

}
