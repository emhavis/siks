<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\ServiceOrderUttps;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\HistoryUttp;
use App\MasterInstalasi;

class CustomerController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("customer");

        $rows = \DB::table('users_customers')
                ->get();
        
        return view('customers.index',compact(['rows','attribute']));
    }    

    public function getdata(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Data tidak ditemukan';

        if ($request->has('id')) {
            $row = \DB::table('users_customers')
                ->select('full_name', 'kantor', 'email', 'phone', 'is_active')
                ->where('id', '=', $request->get('id'))
                ->first();

            $response["status"] = true;
            $response["messages"] = 'Data ditemukan';
            $response["data"] = $row;
        }

        return response($response);
    }
}
