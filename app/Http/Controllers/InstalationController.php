<?php

namespace App\Http\Controllers;

use App\MasterLaboratory;
use App\MasterInstalasi;
use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\DB;

class InstalationController extends Controller
{
    private $MasterLaboratory;
    private $MasterInstalasi;
    private $MyProjects;

    public function __construct() 
    {
        $this->MasterLaboratory = new MasterLaboratory();
        $this->MasterInstalasi = new MasterInstalasi();
        $this->MyProjects = new MyProjects();
    }

    public function index() 
    {
        $attribute = $this->MyProjects->setup("instalasi");

        $instalasi = $this->MasterInstalasi->get();
        return view('instalasi.index', compact('instalasi','attribute'));
    }

    public function create($id=null) 
    {
        $attribute = $this->MyProjects->setup("instalasi");

        $row = null;
        $kajiUlangs = [];

        if($id)
        {
            $row = $this->MasterInstalasi->whereId($id)->first();
            $kajiUlangs = DB::table('master_instalasi_kajiulang')
                ->join('master_service_types', 'master_service_types.id', '=', 'master_instalasi_kajiulang.service_type_id')
                ->select('master_instalasi_kajiulang.*', 'master_service_types.service_type')
                ->where('instalasi_id', $id)
                ->orderBy('service_type_id')
                ->get();
        }

        $laboratories = $this->MasterLaboratory->pluck('nama_lab', 'id');

        return view('instalasi.create',compact([
            'row','id','attribute','laboratories','kajiUlangs',
        ]));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["nama_instalasi"] = ['required'];
        $rules["lab_id"] = ['required'];
        $rules["quota_online"] = ['required', 'integer', 'min:0'];
        $rules["quota_offline"] = ['required', 'integer', 'min:0'];
        $rules["sla_day"] = ['required', 'integer', 'min:0'];
        $rules["sla_tipe_day"] = ['required', 'integer', 'min:0'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->MasterInstalasi->whereId($id)->update($request->all());
            }
            else
            {
                $request["is_active"] = "t";
                $request["created_at"] = date("Y-m-d H:i:s");
                $this->MasterInstalasi->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->MasterLaboratory->find($request->id);

        if(count($row->MasterUsers)>0)
        {
            $response["message"] = "Nama Laboratorium sudah terpakai di User List";
        }
        else
        {
            if($request->action=="delete")
            {
                $row->delete();
                $response["status"] = true;
            }
        }

        return response($response);
    }

    public function getbyid($id)
    {
        $row = $this->MasterLaboratory->find($id);
        $response["lab"] = $row;
        
        $rowsInstalasi = $this->MasterInstalasi->where("lab_id", $id)->get();
        $response["instalasi"] = $rowsInstalasi;

        return response($response);
    }

    public function saveKajiUlang(Request $request)
    {
        $response["status"] = false;
        $rules["instalasi_id"] = ['required'];
        $rules["service_type_id"] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                DB::table('master_instalasi_kajiulang')
                    ->where('id', $request->get('id'))
                    ->where('service_type_id', $request->get('service_type_id'))
                    ->where('instalasi_id', $request->get('instalasi_id'))
                    ->update(['kaji_ulang' => $request->get('kaji_ulang')]);
            }
            else
            {
                dd($request);
                //$request["is_active"] = "t";
                //$request["created_at"] = date("Y-m-d H:i:s");
                //$this->MasterInstalasi->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }
}
