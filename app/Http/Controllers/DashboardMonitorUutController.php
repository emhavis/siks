<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DashboardMonitorUutController extends Controller
{
    private $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("dashboardmonitoruut");
        if($attribute==null) return userlogout();

        $start = null;
        $end = null;
        
        $q_sum_all = DB::table('service_orders')
            ->join('service_request_items', 'service_orders.service_request_item_id', '=', 'service_request_items.id')
            ->join('service_requests', 'service_request_items.service_request_id', '=', 'service_requests.id')
            ->join('standard_uut', 'service_request_items.uut_id', '=', 'standard_uut.id')
            ->join('master_standard_types', 'standard_uut.type_id', '=', 'master_standard_types.id')
            ->join('master_laboratory', 'master_standard_types.lab_id', '=', 'master_laboratory.id')
            ->selectRaw("master_laboratory.nama_lab,
                count(distinct service_request_items.id) as jumlah,
                master_standard_types.lab_id as lab_id,
                sum(case when service_requests.status_id = 14 then 1 else 0 end) as jumlah_selesai,
                sum(case when service_requests.status_id not in (14, 16) then 1 else 0 end) as jumlah_proses,
                sum(case when service_requests.status_id = 14 and 
                DATE_PART('day', service_orders.kabalai_date::timestamp - service_requests.spuh_billing_date::timestamp) 
                    <= (
                        case 
                            when service_requests.service_type_id in (1,2) 
                                then master_laboratory.sla_day else master_laboratory.sla_day end
                        ) then 1 else 0 end) as jumlah_sesuai_sla,
            sum(case when service_requests.status_id = 14 and 
                DATE_PART('day', service_orders.kabalai_date::timestamp - service_requests.spuh_billing_date::timestamp) 
                    > (case when service_requests.service_type_id in (1,2) then master_laboratory.sla_day else master_laboratory.sla_day end) then 1 else 0 end) as jumlah_lebih_sla
            ")
            ->groupBy('master_laboratory.nama_lab')
            ->groupBy('master_standard_types.lab_id');

        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
        
            $q_sum_all = $q_sum_all->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            $q_sum_all = $q_sum_all->where('staff_entry_datein', '>=', $start);
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            $q_sum_all = $q_sum_all->where('staff_entry_datein', '<=', $end);
        }
            
        $q_sum = clone $q_sum_all;
        $summaries = $q_sum->get();
        $lab = $q_sum->pluck('nama_lab');

        $q_sum_v = clone $q_sum_all;
        $q_sum_v = $q_sum_v->where('service_requests.service_type_id', 1);
        $summaries_v = $q_sum_v->get();
        $lab_v = $q_sum_v->pluck('nama_lab');

        $q_sum_k = clone $q_sum_all;
        $q_sum_k = $q_sum_k->where('service_requests.service_type_id', 2);
        $summaries_k = $q_sum_k->get();
        $lab_k = $q_sum_k->pluck('nama_lab');
        
        return view('dashboardmonitoruut.home',compact([
            'attribute', 
            'summaries', 
            'lab',
            'summaries_v', 
            'lab_v',
            'summaries_k', 
            'lab_k',
            'start', 'end',
        ]));
    }

    public function details(Request $request) {
        $q_sum_all = DB::table('service_orders')
            ->join('service_request_items', 'service_orders.service_request_item_id', '=', 'service_request_items.id')
            ->join('service_requests', 'service_orders.service_request_id', '=', 'service_requests.id')
            ->join('standard_uut', 'service_orders.uut_id', '=', 'standard_uut.id')
            ->join('master_standard_types', 'standard_uut.type_id', '=', 'master_standard_types.id')
            ->join('master_laboratory', 'master_standard_types.lab_id', '=', 'master_laboratory.id')
            ->selectRaw('service_request_items.no_order, 
                service_requests.no_register,
                service_requests.label_sertifikat, 
                master_standard_types.uut_type as jenis,
                service_requests.jenis_layanan,
                master_laboratory.nama_lab,
                service_requests.received_date,
                service_orders.staff_entry_dateout
                ');

        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
        
            $q_sum_all = $q_sum_all->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            $q_sum_all = $q_sum_all->where('staff_entry_datein', '>=', $start);
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            $q_sum_all = $q_sum_all->where('staff_entry_datein', '<=', $end);
        }

        if ($request->get('lab') != null) {
            $q_sum_all = $q_sum_all->where('master_standard_types.lab_id', '=', $request->get('lab'));
        }
        if ($request->get('type') != null) {
            if ($request->get('type') == 'ttu') {
                $q_sum_all->whereIn('service_requests.service_type_id', [1]);
            } elseif ($request->get('type') == 'tipe') {
                $q_sum_all->whereIn('service_requests.service_type_id', [2]);
            } 
        }
        if ($request->get('sla') != null) {
            if ($request->get('sla') == 'selesai') {
                $q_sum_all->where('service_requests.status_id', 14);
            } elseif ($request->get('sla') == 'proses') {
                $q_sum_all->whereNotIn('service_requests.status_id', [14,16]);
            } elseif ($request->get('sla') == 'sesuai') {
                $q_sum_all->where('service_requests.status_id', 14)
                    ->whereRaw("DATE_PART('day', service_orders.kabalai_date::timestamp - service_requests.spuh_billing_date::timestamp) <= master_laboratory.sla_day");
            } elseif ($request->get('sla') == 'lebih') {
                $q_sum_all->where('service_requests.status_id', 14)
                    ->whereRaw("DATE_PART('day', service_orders.kabalai_date::timestamp - service_requests.spuh_billing_date::timestamp) > master_laboratory.sla_day");
            } 

        }
        return response($q_sum_all->get());

    }
}
