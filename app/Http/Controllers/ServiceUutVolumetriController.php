<?php 
namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\AppResponse;

use App\ServiceRequestItem;
use App\MasterTemplate;
use App\ServiceOrders;
use App\MasterMetodeUji;
use App\MasterStudentTable;
use App\ServiceRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ServiceUutVolumetriController extends Controller
{
    use AppResponse;

    public function index()
    {
        $attribute = $this->setup("service");
        $rows = ServiceOrders::whereIn("stat_service_order",[0,1])
        ->where("is_finish",0)
        ->where("laboratory_id",Auth::user()->laboratory_id)
        ->orderBy('staff_entry_datein','desc')
        ->get();

        return view('service.index',compact('rows','attribute'));
    }

    public function recount(Request $request)
    {
        $id = $request->id;
        $row = ServiceOrders::find($id);
        // $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        // $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($request->get("hotdata"),true);

        // // User Take Order
        $hot[3][1] = $row->MasterUsers->full_name;
        $hot[1][1] = date("d - F - Y");

        //Tanggal Bayar Order
        $orderPaymentDate =  date("d - F - Y",strtotime($row->ServiceRequest->payment_date)); 
        $hot[2][1] = $row->ServiceRequest->no_order."/".$orderPaymentDate;

        $C16 = $hot[8][1];
        $C17 = $hot[9][1];
        $C18 = $hot[10][1];
        $C19 = $hot[11][1];
        $C20 = $hot[12][1];
        $C21 = $hot[13][1];
        $C24 = $hot[16][1];
        $C25 = $hot[17][1];
        $D21 = $hot[13][4];
        $D35 = $hot[29][3];
        $D36 = $hot[30][3];
        $hot[1][12] = "X";
        $hot[2][12] = "X";
        $hot[3][12] = "X";
        $hot[4][12] = "X";
        $hot[5][12] = "X";
        $hot[6][12] = "X";
        $hot[7][12] = "X";
        $hot[8][12] = "X";
        if(strlen($C16)>0) $hot[1][12] = "V";
        if(strlen($C17)>0) $hot[2][12] = "V";
        if(strlen($C20)>0) $hot[4][12] = "V";
        if(strlen($C21)>0) $hot[6][12] = "V";
        if(strlen($C19)>0) $hot[7][12] = "V";

        if(strlen($C25)>0 && strlen($D35)>0 && strlen($D36)>0)
        {
            $D37 = $hot[31][3] = ($D36-$D35)/$C25;
            $C38 = $hot[32][3];
            $D38 = $M35 = $this->LabuV20($C38);
            $hot[33][3] = $this->FNum($this->LabuV20($C38));
            $D39 = $hot[34][3];
            $D40 = ($D38/$D37)*(1+$D21*(28-$D39));
            $hot[35][3] = $this->FNum($D40);
            $hot[29][12] = $this->FNum($M35);
            $M36 = $hot[30][12];
            $M37 = 3*$M35/($M36);
            $hot[31][12] = $this->FNum($M37);
            $M38 = $C24*1000;
            $hot[32][12] = $this->FNum($M38);
        }

        return response($hot);
    }

    public function Resume($id,$tipe=null)
    {
        $row = ServiceOrders::find($id);

        $hot = json_decode($row->hasil_uji,true);
        $cerapan = json_decode($row->cerapan,true);
        $cerapan2 = json_decode($row->cerapandua,true);
        $cerapan3 = json_decode($row->cerapantiga,true);
        $uncertainty = json_decode($row->uncertainty,true);
        $uncertainty2 = json_decode($row->uncertaintydua,true);
        $uncertainty3 = json_decode($row->uncertaintytiga,true);

        if($row->tmp_report_id==="1")
        {
            $data = $this->ThermohygroLabu("uncertaintyTemperatur");
            $temperaturSuhuRuang = $this->KalkulasiInterpolasi($data,$cerapan2[22][15]);
            $data = $this->ThermohygroLabu("uncertaintyThermohygro");
            $kelembabanRuang = $this->KalkulasiInterpolasi($data,$cerapan2[22][17]);
            $data = $this->BarometerLabu("uncertainty");
            $tekananRuang = $this->KalkulasiInterpolasi($data,$cerapan2[22][13]);

            if($tipe==2)
            {
                return view('service.resumelabudua',compact("hot","cerapan2","uncertainty2", "temperaturSuhuRuang", "kelembabanRuang", "tekananRuang"));
            }
            return view('service.resumelabusatu',compact("hot","cerapan","uncertainty", "temperaturSuhuRuang", "kelembabanRuang", "tekananRuang"));
        }
        else if($row->tmp_report_id==="3")
        {
            $data = $this->ThermohygroGelas("uncertaintyTemperatur");
            $temperaturSuhuRuang = $this->KalkulasiInterpolasi($data,$cerapan2[22][15]);
            $data = $this->ThermohygroGelas("uncertaintyThermohygro");
            $kelembabanRuang = $this->KalkulasiInterpolasi($data,$cerapan2[22][17]);
            $data = $this->BarometerGelas("uncertainty");
            $tekananRuang = $this->KalkulasiInterpolasi($data,$cerapan2[22][13]);

            return view('service.resumegelassatu',compact("hot","cerapan","uncertainty","cerapan2","uncertainty2","cerapan3","uncertainty3", "temperaturSuhuRuang", "kelembabanRuang", "tekananRuang"));
        }

        $VS1 = $this->FNum($cerapan[39][26],$uncertainty["AngkaPenting"]);
        $VS2 = $this->FNum($cerapan[39][24],$uncertainty["AngkaPenting"]);
        $VS3 = $this->FNum($cerapan[39][22],$uncertainty["AngkaPenting"]);
        $VS4 = $this->FNum($cerapan[39][20],$uncertainty["AngkaPenting"]);

        return view('service.resumevolumetri',compact("hot","cerapan","uncertainty","VS1","VS2","VS3","VS4"));
    }

    public function Uncertainty($id,$tipe=null)
    {
        $row = ServiceOrders::find($id);
        $hot = json_decode($row->uncertainty,true);
        $hot2 = json_decode($row->uncertaintydua,true);
        $hot3 = json_decode($row->uncertaintytiga,true);

        if($row->tmp_report_id==="1")
        {
            if($tipe==2)
            {
                return view('service.uncertaintylabudua', compact("hot","hot2","row"));
            }
            return view('service.uncertaintylabusatu', compact("hot","row"));
        }
        else if($row->tmp_report_id==="3")
        {
            if($tipe==2)
            {
                return view('service.uncertaintygelasdua', compact("hot","hot2","row"));
            }
            else if($tipe==3)
            {
                return view('service.uncertaintygelastiga', compact("hot","hot3","row"));
            }
            return view('service.uncertaintygelassatu', compact("hot","row"));
        }

        return view('service.uncertaintyvolumetri', compact("hot","row"));
    }

    public function Cerapan($id,$tipe=null)
    {
        $row = ServiceOrders::find($id);

        if($row->tmp_report_id==="1")
        {
            if($tipe && $tipe==2)
            {
                return $this->CerapanLabuUkurDua($id);
            }
            return $this->CerapanLabuUkur($id);
        }
        else if($row->tmp_report_id==="3")
        {
            if($tipe && $tipe==2)
            {
                return $this->CerapanGelasUkurDua($id);
            }
            else if($tipe && $tipe==3)
            {
                return $this->CerapanGelasUkurTiga($id);
            }

            return $this->CerapanGelasUkur($id);

        }

        $hot = json_decode($row->hasil_uji,true);
        $inputData = $hot;

        // BACA MASA KOSONG
        $H21 = $hot[24][1];
        $H22 = $hot[25][1];
        $H23 = $hot[26][1];
        $massaKosongStat = false;
        if(strlen($H21)>0 && strlen($H22)>0 && strlen($H23)>0)
        {
            $massaKosongStat = true;

            $massaKosongAvg = ($H21+$H22+$H23)/3;
            $hot[27][2] = $H24 = $this->FNum($massaKosongAvg);

            $data = $this->TimbanganElektronik();
            $K21 = $this->KalkulasiInterpolasi($data,$H21);
            $K22 = $this->KalkulasiInterpolasi($data,$H22);
            $K23= $this->KalkulasiInterpolasi($data,$H23);
            $hot[24][4] = $this->FNum($K21);
            $hot[25][4] = $this->FNum($K22);
            $hot[26][4] = $this->FNum($K23);
            $massaKosongKoreksi = ($K21+$K22+$K23)/3;
            $hot[27][4] = $K24 = $this->FNum($massaKosongKoreksi);
        }

        // BACA MASA ISI
        $M21 = $hot[24][3];
        $M22 = $hot[25][3];
        $M23 = $hot[26][3];
        if(strlen($M21)>0 && strlen($M22)>0 && strlen($M23)>0)
        {
            $rataRata = ($M21+$M22+$M23)/3;
            $hot[27][6] = $M24 = $this->FNum($rataRata);

            $data = $this->TimbanganElektronik();
            $O21 = $this->KalkulasiInterpolasi($data,$M21);
            $O22 = $this->KalkulasiInterpolasi($data,$M22);
            $O23 = $this->KalkulasiInterpolasi($data,$M23);
            $hot[24][8] = $this->FNum($O21);
            $hot[25][8] = $this->FNum($O22);
            $hot[26][8] = $this->FNum($O23);
            $rataRata = ($O21+$O22+$O23)/3;
            $hot[27][8] = $O24 = $this->FNum($rataRata);

            if($massaKosongStat)
            {
                $F28 = ($M21+$O21)-($H21+$K21);
                $F29 = ($M22+$O22)-($H22+$K22);
                $F30 = ($M23+$O23)-($H23+$K23);
                $hot[32][1] = $this->FNum($F28);
                $hot[33][1] = $this->FNum($F29);
                $hot[34][1] = $this->FNum($F30);

                $rataRata = ($F28+$F29+$F30)/3;
                $hot[35][1] = $F31 = $this->FNum($rataRata);
            }
        }

        // BACA TEMPERATUR
        $temperaturStat = false;
        $P21 = $hot[24][5];
        $P22 = $hot[25][5];
        $P23 = $hot[26][5];
        $R21 = $hot[24][7];
        $R22 = $hot[25][7];
        $R23 = $hot[26][7];
        if(strlen($P21)>0 && strlen($P22)>0 && strlen($P23)>0 && strlen($R21)>0 && strlen($R22)>0 && strlen($R23)>0)
        {
            $temperaturStat = true;

            $lapAtas = ($P21+$P22+$P23)/3;
            $hot[27][10] = $P24 = $this->FNum($lapAtas);

            $lapBawah = ($R21+$R22+$R23)/3;
            $hot[27][12] = $R24 = $this->FNum($lapBawah);

            $data = $this->TimbanganDigital();
            $hot[24][14] = $T21 = $this->KalkulasiInterpolasi($data,($P21+$R21)/2);
            $hot[25][14] = $T22 = $this->KalkulasiInterpolasi($data,($P22+$R22)/2);
            $hot[26][14] = $T23 = $this->KalkulasiInterpolasi($data,($P23+$R23)/2);
            $rataRata = ($T21+$T22+$T23)/3;
            $hot[27][14] = $T24 = $this->FNum($rataRata);
        }

        // BACA BAROMETER
        $U21 = $hot[24][9];
        $U22 = $hot[25][9];
        $U23 = $hot[26][9];
        if(strlen($U21)>0 && strlen($U22)>0 && strlen($U23)>0)
        {
            $rataRata = ($U21+$U22+$U23)/3;
            $hot[27][16] = $U24 = $this->FNum($rataRata);

            $data = $this->Barometer();
            // dd($data);
            $V21 = $this->KalkulasiInterpolasi($data,$U21);
            $V22 = $this->KalkulasiInterpolasi($data,$U22);
            $V23 = $this->KalkulasiInterpolasi($data,$U23);

            $data = $this->Barometer("uncertainty");
            $hot[99]["AD16"] = $this->KalkulasiInterpolasi($data,$U24);

            $hot[24][18] = $this->FNum($V21);
            $hot[25][18] = $this->FNum($V22);
            $hot[26][18] = $this->FNum($V23);
            $koreksi = ($V21+$V22+$V23)/3;
            $hot[27][18] = $V24 = $this->FNum($koreksi);
        }

        // BACA THERMOMETER
        $W21 = $hot[24][11];
        $W22 = $hot[25][11];
        $W23 = $hot[26][11];
        if( strlen($W21)>0 && strlen($W22)>0 && strlen($W23)>0)
        {
            $rataRata = ($W21+$W22+$W23)/3;
            $hot[27][20] = $W24 = $this->FNum($rataRata);

            $data = $this->Thermometer();
            $X21 = $this->KalkulasiInterpolasi($data,$W21);
            $X22 = $this->KalkulasiInterpolasi($data,$W22);
            $X23 = $this->KalkulasiInterpolasi($data,$W23);

            $data = $this->Thermometer("uncertainty");
            $hot[99]["AD12"] = $this->KalkulasiInterpolasi($data,$W24);

            $hot[24][22] = $this->FNum($X21);
            $hot[25][22] = $this->FNum($X22);
            $hot[26][22] = $this->FNum($X23);
            $koreksi = ($X21+$X22+$X23)/3;
            $hot[27][22] = $X24 = $this->FNum($koreksi);
        }

        // BACA HYGROMETER
        $Y21 = $hot[24][13];
        $Y22 = $hot[25][13];
        $Y23 = $hot[26][13];
        if(strlen($Y21)>0 && strlen($Y22)>0 && strlen($Y23)>0)
        {
            $rataRata = ($Y21+$Y22+$Y23)/3;
            $hot[27][24] = $Y24 = $this->FNum($rataRata);

            $data = $this->Hygrometer();
            $Z21 = $this->KalkulasiInterpolasi($data,$Y21);
            $Z22 = $this->KalkulasiInterpolasi($data,$Y22);
            $Z23 = $this->KalkulasiInterpolasi($data,$Y23);

            $data = $this->Hygrometer("uncertainty");
            $hot[99]["AD14"] = $this->KalkulasiInterpolasi($data,$Y24);

            $hot[24][26] = $this->FNum($Z21);
            $hot[25][26] = $this->FNum($Z22);
            $hot[26][26] = $this->FNum($Z23);
            $koreksi = ($Z21+$Z22+$Z23)/3;
            $hot[27][26] = $Z24 = $this->FNum($koreksi);
        }

        if($temperaturStat)
        {
            $J28 = 0.99997495*(1-(pow((($P21+$T21)+-3.983035),2)*(($P21+$T21)+301.797))/(522528.9*(($P21+$T21)+69.34881)));
            $J29 = 0.99997495*(1-(pow((($P22+$T22)+-3.983035),2)*(($P22+$T22)+301.797))/(522528.9*(($P22+$T22)+69.34881)));
            $J30 = 0.99997495*(1-(pow((($P23+$T23)+-3.983035),2)*(($P23+$T23)+301.797))/(522528.9*(($P23+$T23)+69.34881)));
            $hot[32][3] = $this->FNum($J28);
            $hot[33][3] = $this->FNum($J29);
            $hot[34][3] = $this->FNum($J30);

            $rataRata = ($J28+$J29+$J30)/3;
            $hot[35][3] = $J31 = $this->FNum($rataRata);

            $N28 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N29 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N30 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $RhoUdaraE19 = $hot[32][6] = $this->FNum($N28);
            $RhoUdaraE20 = $hot[33][6] = $this->FNum($N29);
            $RhoUdaraE21 = $hot[34][6] = $this->FNum($N30);

            $rataRata = ($N28+$N29+$N30)/3;
            $hot[35][6] = $N31 = $this->FNum($rataRata);

            $J13 = 8; // NOT INPUT DATA: ρ AT : 8
            $U11 = $hot[13][4];
            $P28 = (($F28)*pow(($J28-$N28),-1)*(1-($N28/$J13))*(1-($U11*(($P21+$T21)-20))));
            $P29 = (($F29)*pow(($J29-$N29),-1)*(1-($N29/$J13))*(1-($U11*(($P22+$T22)-20))));
            $P30 = (($F30)*pow(($J30-$N30),-1)*(1-($N30/$J13))*(1-($U11*(($P23+$T23)-20))));
            $hot[32][9] = $this->FNum($P28);
            $hot[33][9] = $this->FNum($P29);
            $hot[34][9] = $this->FNum($P30);

            $rataRata = ($P28+$P29+$P30)/3;
            $hot[35][9] = $P31 = $this->FNum($rataRata);

            $A28 = $hot[12][1];
            $U28 = $P28-($A28*1000);
            $U29 = $P29-($A28*1000);
            $U30 = $P30-($A28*1000);
            $hot[32][11] = $this->FNum($U28);
            $hot[33][11] = $this->FNum($U29);
            $hot[34][11] = $this->FNum($U30);
            $rataRata = ($U28+$U29+$U30)/3;
            $hot[35][11] = $U31 = $this->FNum($rataRata);

            // UNCERTAINTY
            // 1. Repeatability pengukuran Vol pd Meniscus
            $uncert["K5"] = (max([$P28,$P29,$P30])-min([$P28,$P29,$P30]))/(2*sqrt(3));
            // dd(max([$P28,$P29,$P30]),min([$P28,$P29,$P30]));
            $uncert["Q5"] = 200;
            $uncert["S5"] = 1;
            $uncert["U5"] = $uncert["K5"]*$uncert["S5"];
            $uncert["W5"] = pow($uncert["U5"],2);
            $uncert["Y5"] = pow($uncert["U5"],4)/$uncert["Q5"];

            // 2. Massa
            $M10 = $this->TimbanganElektronik("dayaBaca");
            $M14 = $this->TimbanganElektronik("maxU95"); //0.3;
            // a. ketidakpastian akibat penunjukkan timbangan dari sertifikat
            $uncert["K7"] = sqrt((pow($M14/2,2))+pow($M10/sqrt(3),2));
            // b. ketidakpastian akibat massa isi
            $uncert["K8"] = sqrt(pow($M14/2,2)+pow($M10/sqrt(3),2));
            // c. ketidakpastian akibat drift dari timbangan
            $max = $this->TimbanganElektronik("maxU95");
            $uncert["K9"] = 0.1*($max)/2;
            $uncert["N6"] = pow(pow($uncert["K7"],2)+pow($uncert["K8"],2)+pow($uncert["K9"],2),0.5);
            $uncert["Q6"] = 200;
            $uncert["S6"] = (1/($J31-$N31))*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U6"] = $uncert["N6"]*$uncert["S6"];
            $uncert["W6"] = pow($uncert["U6"],2);
            $uncert["Y6"] = pow($uncert["U6"],4)/$uncert["Q6"];

            // 3. Temperatur (Air dan Instrumen)
            // A. Ketidakpastian akibat Instrumen
            $uncert["K11"] = (ABS(($W24+$X24)-($P24+$T24)))/SQRT(12);
            // a. ketidakpastian thermometer standar dari sertifikat thermometer
            $J16 = $this->TimbanganDigital("maxU95");
            $uncert["K13"] = $J16/2;
            // b. ketidakpastian akibat drift dari thermometer
            $max = $this->Drift("max");
            $min = $this->Drift();
            $uncert["K14"] = ($max-$min)/sqrt(12);
            // c. ketidakpastian akibat resolusi thermometer standar
            $dayaBaca = $this->TimbanganDigital("dayaBaca");
            $uncert["K15"] = $dayaBaca/sqrt(12);
            // d. ketidakpastian akibat variansi temperatur saat pengujian
            $intP21 = $P21+$T21;
            $intP22 = $P22+$T22;
            $intP23 = $P23+$T23;
            $intR21 = $R21+$T21;
            $intR22 = $R22+$T22;
            $intR23 = $R23+$T23;
            $max = max([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $min = min([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $uncert["K16"] = ($max-$min)/sqrt(12);

            // B. Ketidakpastian akibat temp air
            $uncert["K12"] = sqrt(pow($uncert["K13"],2)+pow($uncert["K14"],2)+pow($uncert["K15"],2)+pow($uncert["K16"],2));
            $uncert["N10"] = sqrt(pow($uncert["K11"],2)+pow($uncert["K12"],2));
            $uncert["Q10"] = 60;
            $uncert["S10"] = $F31*(1/($J31-$N31))*(1-($N31/$J13))*(-$U11);
            $uncert["U10"] = $uncert["N10"]*$uncert["S10"];
            $uncert["W10"] = pow($uncert["U10"],2);
            $uncert["Y10"] = pow($uncert["U10"],4)/$uncert["Q10"];

            // 4. Densitas Air
            // a. ketidakpastian dari persamaan tanaka
            $uncert["K18"] = 4.5*pow(10,-7);
            // b. ketidakpastian akibat temperatur air
            $rhoAirC3 = ((-0.1176*pow(($P24+$T24),2))+(15.846*($P24+$T24))-62.677)*pow(10,-6);
            $uncert["K19"] = $uncert["K12"]*$rhoAirC3*$J31;
            // c. ketidakpastian akibat kemurnian air
            $uncert["K20"] = 10*pow(10,-6);
            $uncert["N18"] = sqrt(pow($uncert["K18"],2)+pow($uncert["K19"],2)+pow($uncert["K20"],2));
            $uncert["Q17"] = 60;
            $uncert["S17"] = (-$F31)*pow((1/($J31-$N31)),2)*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U17"] = $uncert["N18"]*$uncert["S17"];
            $uncert["W17"] = pow($uncert["U17"],2);
            $uncert["Y17"] = pow($uncert["U17"],4)/$uncert["Q17"];

            // 5. Densitas Udara
            $rhoUdaraD19 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*exp(0.061*($W21+$X21)))/(($W21+$X21)+273.15);
            $rhoUdaraD20 = ((0.34848*($U22+$V22))-(0.009*($Y22+$Z22))*exp(0.061*($W22+$X22)))/(($W22+$X22)+273.15);
            $rhoUdaraD21 = ((0.34848*($U23+$V23))-(0.009*($Y23+$Z23))*exp(0.061*($W23+$X23)))/(($W23+$X23)+273.15);
            $rataRata = ($rhoUdaraD19+$rhoUdaraD20+$rhoUdaraD21)/3;
            $rhoUdaraE23 = $rataRata/1000;

            $rhoUdaraE27 = 2.4*pow(10,-4);
            $rhoUdaraG27 = 1;
            $rhoUdaraI27 = $rhoUdaraE27*$rhoUdaraG27;
            $rhoUdaraJ27 = pow($rhoUdaraI27,2);

            $max = $this->Barometer("tekananMax");
            $rhoUdaraE28 = $max*100/2;
            $rhoUdaraG28 = 1*pow(10,-5);
            $rhoUdaraI28 = $rhoUdaraE28*$rhoUdaraG28;
            $rhoUdaraJ28 = pow($rhoUdaraI28,2);

            $dataMax = $this->Thermohygro();
            $rhoUdaraE29 = max($dataMax)/2;
            $rhoUdaraG29 = -4*pow(10,-3);
            $rhoUdaraI29 = $rhoUdaraE29*$rhoUdaraG29;
            $rhoUdaraJ29 = pow($rhoUdaraI29,2);

            $dataMax = $this->Thermohygro("persen");
            $rhoUdaraE30 = max($dataMax)/(2*100);
            $rhoUdaraG30 = -9*pow(10,-3);
            $rhoUdaraI30 = $rhoUdaraE30*$rhoUdaraG30;
            $rhoUdaraJ30 = pow($rhoUdaraI30,2);
            $rhoUdaraJ32 = $rhoUdaraE23*sqrt($rhoUdaraJ27+$rhoUdaraJ28+$rhoUdaraJ29+$rhoUdaraJ30);

            $uncert["K21"] = $rhoUdaraJ32;
            $uncert["Q21"] = 60;
            $uncert["S21"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*((1-($N31/$J13))*(1/($J31-$N31))-(1/$J13));
            $uncert["U21"] = $uncert["K21"]*$uncert["S21"];
            $uncert["W21"] = pow($uncert["U21"],2);
            $uncert["Y21"] = pow($uncert["U21"],4)/$uncert["Q21"];

            // 6. Densitas Anak Timbangan
            $uncert["K22"] = 0.14/SQRT(3); //sqrt($uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]);
            $uncert["Q22"] = 60;
            $uncert["S22"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*($N31/pow($J13,2));
            $uncert["U22"] = $uncert["K22"]*$uncert["S22"];
            $uncert["W22"] = pow($uncert["U22"],2);
            $uncert["Y22"] = pow($uncert["U22"],4)/$uncert["Q22"];

            // 7. Koefisien Muai Ruang
            $uncert["K23"] = (0.1*$U11)/sqrt(3);
            $uncert["Q23"] = 60;
            $uncert["S23"] = ($F31)*(1/($J31-$N31))*(1-($N31/$J13))*(-(($P24)+($T24)-20));
            $uncert["U23"] = $uncert["K23"]*$uncert["S23"];
            $uncert["W23"] = pow($uncert["U23"],2);
            $uncert["Y23"] = pow($uncert["U23"],4)/$uncert["Q23"];

            // 8. Pembacaan Meniskus
            $C25 = $hot[17][1];
            // Pembagian skala 10
            // $F26 = $hot[13][10];
            $F26 = $hot[19][1];
            $uncert["K24"] = ($C25)/(sqrt(3)*$F26);
            $uncert["Q24"] = 200;
            $uncert["S24"] = 1;
            $uncert["U24"] = $uncert["K24"]*$uncert["S24"];
            $uncert["W24"] = pow($uncert["U24"],2);
            $uncert["Y24"] = pow($uncert["U24"],4)/$uncert["Q24"];

            $uncert["U25"] = $uncert["U5"]+$uncert["U6"]+$uncert["U10"]+$uncert["U17"]+$uncert["U21"]+$uncert["U22"]+$uncert["U23"]+$uncert["U24"];
            $uncert["W25"] = $uncert["W5"]+$uncert["W6"]+$uncert["W10"]+$uncert["W17"]+$uncert["W21"]+$uncert["W22"]+$uncert["W23"]+$uncert["W24"];
            $uncert["Y25"] = $uncert["Y5"]+$uncert["Y6"]+$uncert["Y10"]+$uncert["Y17"]+$uncert["Y21"]+$uncert["Y22"]+$uncert["Y23"]+$uncert["Y24"];

            $uncert["U27"] = pow($uncert["W25"],0.5);
            $uncert["U28"] = pow($uncert["U27"],4)/$uncert["Y25"];

            $df = intval($uncert["U28"])>200?200:intval($uncert["U28"]);
            $TINV = MasterStudentTable::where("pr","0.05")
            ->where("k",2)
            ->where("df",$df)->first();
            $uncert["U29"] = $TINV->value;
            // $uncert["U29"] = 1.96; // TINV(0.05;U28); FORMULA ?

            $uncert["U30"] = ($uncert["U29"]*$uncert["U27"])/1000;
            $decimal = explode(".",$uncert["U30"]);
            $arrs = str_split($decimal[1]);
            $i=0;
            foreach($arrs as $arr)
            {
                $i++;
                if($arr!=="0") break;
            }
            $uncert["U31"] = $this->FNum($uncert["U30"],$i+1);
            $uncert["AngkaPenting"] = $i+1;

            $W28 = $uncert["U28"];
            $X28 = $uncert["U29"];
            $Y28 = $uncert["U31"];
            $hot[32][13] = $this->FNum($W28);
            $hot[32][15] = $this->FNum($X28);
            $hot[32][17] = $this->FNum($Y28);

            // // UNCERTAINTY END

            $J34 = $hot[29][3];
            $J35 = $hot[30][3];
            $J38 = $hot[34][3];
            if(strlen($J34)>0 && strlen($J35)>0 && strlen($J38)>0)
            {
                $hot[40][4] = $J36 = ($J35-$J34)/$C25;
                $C38 = "100 mL ID 2";
                $labu = $this->Labu($C38);
                $hot[41][4] = $J37 = $labu;
                $J39 = ($J37/$J36)*(1+$U11*(28-$J38));

                $hot[38][12] = $J37;
                $hot[40][12] = 3.371; //FORMULA ?
                $S13 = $hot[16][1];
                $hot[41][12] = $S13*1000;
                $X34 = $P31/1000;
                $hot[39][22] = $this->FNum($X34);
                $W33 = 28; //$hot[38][20];
                $X33 = 20; //$hot[38][22];
                $Y33 = 15.6; //$hot[38][24];
                $Z33 = 15; //$hot[38][26];
                $W34 = $X34*(1+($U11*($W33-$X33)));
                $Y34 = $X34*(1+($U11*($Y33-$X33)));
                $Z34 = $X34*(1+($U11*($Z33-$X33)));

                $hot[39][20] = $this->FNum($W34);
                $hot[39][24] = $this->FNum($Y34);
                $hot[39][26] = $this->FNum($Z34);

                $W35 = ($J37/$J36)*(1+$U11*($W33-$J38));
                $X35 = ($J37/$J36)*(1+$U11*($X33-$J38));
                $Y35 = ($J37/$J36)*(1+$U11*($Y33-$J38));
                $Z35 = ($J37/$J36)*(1+$U11*($Z33-$J38));

                $hot[40][20] = $this->FNum($W35);
                $hot[40][22] = $this->FNum($X35);
                $hot[40][24] = $this->FNum($Y35);
                $hot[40][26] = $this->FNum($Z35);

                $hot[41][19] = $X28;
                $hot[41][20] = $W36 = $Y28;
                $V15 = $hot[13][10];
                $hot[42][20] = $W37 = $V15;
            }

        }

        ServiceOrders::whereId($id)->update([
            "cerapan"=>json_encode($hot),
            "uncertainty"=>json_encode($uncert)
        ]);

        return view('service.cerapanvolumetri',compact('hot','inputData','row'));

        // return response($hot);
    }

    public function recountlabuukur(Request $request)
    {
        $id = $request->id;
        $row = ServiceOrders::find($id);
        $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($request->get("hotdata"),true);

        // // User Take Order
        $hot[3][1] = $row->MasterUsers->full_name;
        $hot[1][1] = date("d - F - Y");

        $orderPaymentDate =  date("d - F - Y",strtotime($rowServiceRequest->payment_date));  // 25 Januari 2021	Tanggal bayar order
        $hot[2][1] = $rowServiceRequest->no_order."/".$orderPaymentDate;

        return response($hot);
    }

    private function CerapanLabuUkur($id)
    {
        $row = ServiceOrders::find($id);
        $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($row->hasil_uji,true);
        $inputData = $hot;
        // // User Take Order
        // $orderPaymentDate =  date("d - F - Y",strtotime($rowServiceRequest->payment_date));  // 25 Januari 2021	Tanggal bayar order
        // $hot[1][15] = $rowServiceRequest->no_order."/".$orderPaymentDate;

        $inputStandard = $this->TimbanganElektronikLabu("standard");
        $inputStandard["maxU95"] = $this->TimbanganElektronikLabu("maxU95");
        $inputStandardThermometer = $this->TimbanganDigitalLabu("standard");
        $inputStandardThermometer["maxU95"] = $this->TimbanganDigitalLabu("maxU95");

        // BACA MASA KOSONG
        $data = $this->ImbuhLabu();
        $H21 = $hot[19][1] = $hot[19][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[19][3]]);
        $H22 = $hot[20][1] = $hot[20][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[20][3]]);
        $H23 = $hot[21][1] = $hot[21][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[21][3]]);
        $massaKosongStat = false;
        if(strlen($H21)>0 && strlen($H22)>0 && strlen($H23)>0)
        {
            $massaKosongStat = true;

            $massaKosongAvg = ($H21+$H22+$H23)/3;
            $hot[22][1] = $H24 = $this->FNum($massaKosongAvg);

            $data = $this->TimbanganElektronikLabu();
            $K21 = $this->KalkulasiInterpolasi($data,$H21);
            $K22 = $this->KalkulasiInterpolasi($data,$H22);
            $K23= $this->KalkulasiInterpolasi($data,$H23);
            $hot[19][4] = $this->FNum($K21);
            $hot[20][4] = $this->FNum($K22);
            $hot[21][4] = $this->FNum($K23);
            $massaKosongKoreksi = ($K21+$K22+$K23)/3;
            $hot[22][4] = $K24 = $this->FNum($massaKosongKoreksi);
        }

        // BACA MASA ISI
        $data = $this->imbuhGelas();
        $M21 = $hot[19][5] = $hot[19][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[19][7]]);
        $M22 = $hot[20][5] = $hot[20][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[20][7]]);
        $M23 = $hot[21][5] = $hot[21][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[21][7]]);
        if(strlen($M21)>0 && strlen($M22)>0 && strlen($M23)>0)
        {
            $rataRata = ($M21+$M22+$M23)/3;
            $hot[22][5] = $M24 = $this->FNum($rataRata);

            $data = $this->TimbanganElektronikLabu();
            $O21 = $this->KalkulasiInterpolasi($data,$M21);
            $O22 = $this->KalkulasiInterpolasi($data,$M22);
            $O23 = $this->KalkulasiInterpolasi($data,$M23);
            $hot[19][8] = $this->FNum($O21);
            $hot[20][8] = $this->FNum($O22);
            $hot[21][8] = $this->FNum($O23);
            $rataRata = ($O21+$O22+$O23)/3;
            $hot[22][8] = $O24 = $this->FNum($rataRata);

            if($massaKosongStat)
            {
                $F28 = ($M21+$O21)-($H21+$K21);
                $F29 = ($M22+$O22)-($H22+$K22);
                $F30 = ($M23+$O23)-($H23+$K23);
                $hot[32][1] = $this->FNum($F28);
                $hot[33][1] = $this->FNum($F29);
                $hot[34][1] = $this->FNum($F30);

                $rataRata = ($F28+$F29+$F30)/3;
                $hot[35][1] = $F31 = $this->FNum($rataRata);
            }
        }

        // BACA TEMPERATUR
        $temperaturStat = false;
        $P21 = $hot[19][9];
        $P22 = $hot[20][9];
        $P23 = $hot[21][9];
        $R21 = $hot[19][11];
        $R22 = $hot[20][11];
        $R23 = $hot[21][11];
        if(strlen($P21)>0 && strlen($P22)>0 && strlen($P23)>0 && strlen($R21)>0 && strlen($R22)>0 && strlen($R23)>0)
        {
            $temperaturStat = true;

            $lapAtas = ($P21+$P22+$P23)/3;
            $hot[22][9] = $P24 = $this->FNum($lapAtas);

            $lapBawah = ($R21+$R22+$R23)/3;
            $hot[22][11] = $R24 = $this->FNum($lapBawah);

            $data = $this->ThermometerDigitalLabu("koreksi");
            $hot[19][14] = $T21 = $this->KalkulasiInterpolasi($data,($P21+$R21)/2);
            $hot[20][14] = $T22 = $this->KalkulasiInterpolasi($data,($P22+$R22)/2);
            $hot[21][14] = $T23 = $this->KalkulasiInterpolasi($data,($P23+$R23)/2);
            $rataRata = ($T21+$T22+$T23)/3;
            $hot[22][14] = $T24 = $this->FNum($rataRata);
        }

        // BACA BAROMETER
        $U21 = $hot[19][13];
        $U22 = $hot[20][13];
        $U23 = $hot[21][13];
        if(strlen($U21)>0 && strlen($U22)>0 && strlen($U23)>0)
        {
            $rataRata = ($U21+$U22+$U23)/3;
            $hot[22][13] = $U24 = $this->FNum($rataRata);

            $data = $this->BarometerLabu();
            // dd($data);
            $V21 = $this->KalkulasiInterpolasi($data,$U21);
            $V22 = $this->KalkulasiInterpolasi($data,$U22);
            $V23 = $this->KalkulasiInterpolasi($data,$U23);

            $hot[19][18] = $this->FNum($V21);
            $hot[20][18] = $this->FNum($V22);
            $hot[21][18] = $this->FNum($V23);
            $koreksi = ($V21+$V22+$V23)/3;
            $hot[22][18] = $V24 = $this->FNum($koreksi);
        }

        // BACA THERMOMETER
        $W21 = $hot[19][15];
        $W22 = $hot[20][15];
        $W23 = $hot[21][15];

        if( strlen($W21)>0 && strlen($W22)>0 && strlen($W23)>0)
        {
            $rataRata = ($W21+$W22+$W23)/3;

            $hot[22][15] = $W24 = $this->FNum($rataRata);

            $data = $this->ThermohygroLabu("temperatur");
            $X21 = $this->KalkulasiInterpolasi($data,$W21);
            $X22 = $this->KalkulasiInterpolasi($data,$W22);
            $X23 = $this->KalkulasiInterpolasi($data,$W23);

            $hot[19][22] = $this->FNum($X21);
            $hot[20][22] = $this->FNum($X22);
            $hot[21][22] = $this->FNum($X23);
            $koreksi = ($X21+$X22+$X23)/3;
            $hot[22][22] = $X24 = $this->FNum($koreksi);
        }

        // BACA HYGROMETER
        $Y21 = $hot[19][17];
        $Y22 = $hot[20][17];
        $Y23 = $hot[21][17];
        if(strlen($Y21)>0 && strlen($Y22)>0 && strlen($Y23)>0)
        {
            $rataRata = ($Y21+$Y22+$Y23)/3;
            $hot[22][17] = $Y24 = $this->FNum($rataRata);

            $data = $this->ThermohygroLabu("thermohygro");
            $Z21 = $this->KalkulasiInterpolasi($data,$Y21);
            $Z22 = $this->KalkulasiInterpolasi($data,$Y22);
            $Z23 = $this->KalkulasiInterpolasi($data,$Y23);

            // $data = $this->Hygrometer("uncertainty");
            // $hot[99]["AD14"] = $this->KalkulasiInterpolasi($data,$Y24);

            $hot[19][26] = $this->FNum($Z21);
            $hot[20][26] = $this->FNum($Z22);
            $hot[21][26] = $this->FNum($Z23);
            $koreksi = ($Z21+$Z22+$Z23)/3;
            $hot[22][26] = $Z24 = $this->FNum($koreksi);
        }

        if($temperaturStat)
        {
            $J28 = 0.99997495*(1-(pow((($P21+$T21)+-3.983035),2)*(($P21+$T21)+301.797))/(522528.9*(($P21+$T21)+69.34881)));
            $J29 = 0.99997495*(1-(pow((($P22+$T22)+-3.983035),2)*(($P22+$T22)+301.797))/(522528.9*(($P22+$T22)+69.34881)));
            $J30 = 0.99997495*(1-(pow((($P23+$T23)+-3.983035),2)*(($P23+$T23)+301.797))/(522528.9*(($P23+$T23)+69.34881)));
            $hot[32][3] = $this->FNum($J28);
            $hot[33][3] = $this->FNum($J29);
            $hot[34][3] = $this->FNum($J30);

            $rataRata = ($J28+$J29+$J30)/3;
            $hot[35][3] = $J31 = $this->FNum($rataRata);

            $N28 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N29 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N30 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $RhoUdaraE19 = $hot[32][5] = $this->FNum($N28);
            $RhoUdaraE20 = $hot[33][5] = $this->FNum($N29);
            $RhoUdaraE21 = $hot[34][5] = $this->FNum($N30);

            $rataRata = ($N28+$N29+$N30)/3;
            $hot[35][5] = $N31 = $this->FNum($rataRata);

            $J13 = 8; // NOT INPUT DATA: ρ AT : 8
            $U11 = $hot[12][4];
            $P28 = (($F28)*pow(($J28-$N28),-1)*(1-($N28/$J13))*(1-($U11*(($P21+$T21)-20))));
            $P29 = (($F29)*pow(($J29-$N29),-1)*(1-($N29/$J13))*(1-($U11*(($P22+$T22)-20))));
            $P30 = (($F30)*pow(($J30-$N30),-1)*(1-($N30/$J13))*(1-($U11*(($P23+$T23)-20))));
            $hot[32][7] = $this->FNum($P28);
            $hot[33][7] = $this->FNum($P29);
            $hot[34][7] = $this->FNum($P30);

            $rataRata = ($P28+$P29+$P30)/3;
            $hot[35][7] = $P31 = $this->FNum($rataRata);

            $A28 = $hot[11][1];
            $U28 = $P28-($A28);
            $U29 = $P29-($A28);
            $U30 = $P30-($A28);
            $hot[32][11] = $this->FNum($U28);
            $hot[33][11] = $this->FNum($U29);
            $hot[34][11] = $this->FNum($U30);
            $rataRata = ($U28+$U29+$U30)/3;
            $hot[35][11] = $U31 = $this->FNum($rataRata);

            // UNCERTAINTY
            // 1. Repeatability pengukuran Vol pd Meniscus
            $uncert["K5"] = (max([$P28,$P29,$P30])-min([$P28,$P29,$P30]))/(2*sqrt(3));
            $uncert["Q5"] = 200;
            $uncert["S5"] = 1;
            $uncert["U5"] = $uncert["K5"]*$uncert["S5"];
            $uncert["W5"] = pow($uncert["U5"],2);
            $uncert["Y5"] = pow($uncert["U5"],4)/$uncert["Q5"];

            // 2. Massa
            $M10 = $inputStandard["dayaBaca"];
            $M14 = $inputStandard["maxU95"];
            // dd($M10,$M14);
            // a. ketidakpastian akibat penunjukkan timbangan dari sertifikat
            // pow($M14/2,2)+pow($M10/SQRT(3),2)
            $uncert["K7"] = sqrt(pow($M14/2,2)+pow($M10/SQRT(3),2));
            // b. ketidakpastian akibat massa isi
            $uncert["K8"] = sqrt(pow($M14/2,2)+pow($M10/SQRT(3),2));
            // c. ketidakpastian akibat drift dari timbangan
            // $max = 0; // Diambil yang terakhir
            // $min = 0; // Diambil yang terakhir
            $max = $inputStandard["maxU95"];
            $uncert["K9"] = 0.1*($max)/2;

            $data = $this->ImbuhLabu("kp");
            $imbuhkp1 = $data[$inputData[19][3]];
            $imbuhkp2 = $data[$inputData[20][3]];
            $imbuhkp3 = $data[$inputData[21][3]];
            $imbuhkp4 = $data[$inputData[19][7]];
            $imbuhkp5 = $data[$inputData[20][7]];
            $imbuhkp6 = $data[$inputData[21][7]];

            $uncert["K10"] = max([$imbuhkp1,$imbuhkp2,$imbuhkp3,$imbuhkp4,$imbuhkp5,$imbuhkp6])/2;
            $uncert["N6"] = pow(pow($uncert["K7"],2)+pow($uncert["K8"],2)+pow($uncert["K9"],2),0.5);
            $uncert["Q6"] = 200;
            $uncert["S6"] = (1/($J31-$N31))*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U6"] = $uncert["N6"]*$uncert["S6"];
            $uncert["W6"] = pow($uncert["U6"],2);
            $uncert["Y6"] = pow($uncert["U6"],4)/$uncert["Q6"];

            // 3. Temperatur (Air dan Instrumen)
            // A. Ketidakpastian akibat Instrumen
            $uncert["K11"] = (ABS($W24+$X24)-($P24+$T24))/SQRT(12);
            // a. ketidakpastian thermometer standar dari sertifikat thermometer
            $J16 = $inputStandardThermometer["maxU95"];
            $uncert["K13"] = $J16/2;
            // b. ketidakpastian akibat drift dari thermometer
            $uncert["K14"] = $this->DriftLabu();
            // c. ketidakpastian akibat resolusi thermometer standar
            $dayaBaca = $inputStandardThermometer["dayaBaca"];
            $uncert["K15"] = $dayaBaca/sqrt(12);
            // dd($uncert["K15"]);
            // d. ketidakpastian akibat variansi temperatur saat pengujian
            $intP21 = $P21+$T21;
            $intP22 = $P22+$T22;
            $intP23 = $P23+$T23;
            $intR21 = $R21+$T21;
            $intR22 = $R22+$T22;
            $intR23 = $R23+$T23;
            $max = max([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $min = min([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $uncert["K16"] = ($max-$min)/sqrt(12);

            // B. Ketidakpastian akibat temp air
            $uncert["K12"] = sqrt(pow($uncert["K13"],2)+pow($uncert["K14"],2)+pow($uncert["K15"],2)+pow($uncert["K16"],2));
            $uncert["N10"] = pow((pow($uncert["K11"],2)+pow($uncert["K12"],2)),0.5);
            $uncert["Q10"] = 60;
            $uncert["S10"] = $F31*(1/($J31-$N31))*(1-($N31/$J13))*(-$U11);
            $uncert["U10"] = $uncert["N10"]*$uncert["S10"];
            $uncert["W10"] = pow($uncert["U10"],2);
            $uncert["Y10"] = pow($uncert["U10"],4)/$uncert["Q10"];

            // 4. Densitas Air
            // a. ketidakpastian dari persamaan tanaka
            $uncert["K18"] = 4.5*pow(10,-7);
            // b. ketidakpastian akibat temperatur air
            $rhoAirC3 = ((-0.1176*pow(($P24+$T24),2))+(15.846*($P24+$T24))-62.677)*pow(10,-6);
            $uncert["K19"] = $uncert["K12"]*$rhoAirC3*$J31;
            // c. ketidakpastian akibat kemurnian air
            $uncert["K20"] = 5*pow(10,-6);
            $uncert["N18"] = sqrt(pow($uncert["K18"],2)+pow($uncert["K19"],2)+pow($uncert["K20"],2));
            $uncert["Q17"] = 60;
            $uncert["S17"] = (-$F31)*pow((1/($J31-$N31)),2)*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U17"] = $uncert["N18"]*$uncert["S17"];
            $uncert["W17"] = pow($uncert["U17"],2);
            $uncert["Y17"] = pow($uncert["U17"],4)/$uncert["Q17"];

            // 5. Densitas Udara
            $rhoUdaraD19 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*exp(0.061*($W21+$X21)))/(($W21+$X21)+273.15);
            $rhoUdaraD20 = ((0.34848*($U22+$V22))-(0.009*($Y22+$Z22))*exp(0.061*($W22+$X22)))/(($W22+$X22)+273.15);
            $rhoUdaraD21 = ((0.34848*($U23+$V23))-(0.009*($Y23+$Z23))*exp(0.061*($W23+$X23)))/(($W23+$X23)+273.15);
            $rataRata = ($rhoUdaraD19+$rhoUdaraD20+$rhoUdaraD21)/3;
            $rhoUdaraE23 = $rataRata/1000;

            $rhoUdaraE27 = 2.4*pow(10,-4);
            $rhoUdaraG27 = 1;
            $rhoUdaraI27 = $rhoUdaraE27*$rhoUdaraG27;
            $rhoUdaraJ27 = pow($rhoUdaraI27,2);

            $max = $this->Barometer("tekananMax");
            $rhoUdaraE28 = $max*100/2;
            $rhoUdaraG28 = 1*pow(10,-5);
            $rhoUdaraI28 = $rhoUdaraE28*$rhoUdaraG28;
            $rhoUdaraJ28 = pow($rhoUdaraI28,2);

            $dataMax = $this->ThermohygroLabu("uncertaintyCelcius");
            $rhoUdaraE29 = $dataMax/2;
            $rhoUdaraG29 = -4*pow(10,-3);
            $rhoUdaraI29 = $rhoUdaraE29*$rhoUdaraG29;
            $rhoUdaraJ29 = pow($rhoUdaraI29,2);

            $dataMax = $this->Thermohygro("persen");
            $rhoUdaraE30 = max($dataMax)/(2*100);
            $rhoUdaraG30 = -9*pow(10,-3);
            $rhoUdaraI30 = $rhoUdaraE30*$rhoUdaraG30;
            $rhoUdaraJ30 = pow($rhoUdaraI30,2);
            $rhoUdaraJ32 = $rhoUdaraE23*sqrt($rhoUdaraJ27+$rhoUdaraJ28+$rhoUdaraJ29+$rhoUdaraJ30);

            $uncert["K21"] = $rhoUdaraJ32;
            $uncert["Q21"] = 60;
            $uncert["S21"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*((1-($N31/$J13))*(1/($J31-$N31))-(1/$J13));
            $uncert["U21"] = $uncert["K21"]*$uncert["S21"];
            $uncert["W21"] = pow($uncert["U21"],2);
            $uncert["Y21"] = pow($uncert["U21"],4)/$uncert["Q21"];

            // 6. Densitas Anak Timbangan
            $uncert["K22"] = 0.14/SQRT(3); //sqrt($uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]);
            $uncert["Q22"] = 60;
            $uncert["S22"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*($N31/pow($J13,2));
            $uncert["U22"] = $uncert["K22"]*$uncert["S22"];
            $uncert["W22"] = pow($uncert["U22"],2);
            $uncert["Y22"] = pow($uncert["U22"],4)/$uncert["Q22"];

            // 7. Koefisien Muai Ruang
            $uncert["K23"] = (0.1*$U11)/sqrt(3);
            $uncert["Q23"] = 60;
            $uncert["S23"] = ($F31)*(1/($J31-$N31))*(1-($N31/$J13))*(-(($P24)+($T24)-20));
            $uncert["U23"] = $uncert["K23"]*$uncert["S23"];
            $uncert["W23"] = pow($uncert["U23"],2);
            $uncert["Y23"] = pow($uncert["U23"],4)/$uncert["Q23"];

            // 8. Pembacaan Meniskus
            $C25 = $hot[11][1];
            // Pembagian skala 10
            // $F26 = $hot[13][10];
            // $F26 = 1;//$hot[19][1];
            $F26 = 40000;//$hot[19][1];
            $uncert["K24"] = ($C25)/(sqrt(3)*$F26);
            $uncert["Q24"] = 200;
            $uncert["S24"] = 1;
            $uncert["U24"] = $uncert["K24"]*$uncert["S24"];
            $uncert["W24"] = pow($uncert["U24"],2);
            $uncert["Y24"] = pow($uncert["U24"],4)/$uncert["Q24"];

            $uncert["U25"] = $uncert["U5"]+$uncert["U6"]+$uncert["U10"]+$uncert["U17"]+$uncert["U21"]+$uncert["U22"]+$uncert["U23"]+$uncert["U24"];
            $uncert["W25"] = $uncert["W5"]+$uncert["W6"]+$uncert["W10"]+$uncert["W17"]+$uncert["W21"]+$uncert["W22"]+$uncert["W23"]+$uncert["W24"];
            $uncert["Y25"] = $uncert["Y5"]+$uncert["Y6"]+$uncert["Y10"]+$uncert["Y17"]+$uncert["Y21"]+$uncert["Y22"]+$uncert["Y23"]+$uncert["Y24"];

            $uncert["U27"] = pow($uncert["W25"],0.5);
            $uncert["U28"] = pow($uncert["U27"],4)/$uncert["Y25"];

            $df = intval($uncert["U28"])>200?200:intval($uncert["U28"]);
            $TINV = MasterStudentTable::where("pr","0.05")
            ->where("k",2)
            ->where("df",$df)->first();
            $uncert["U29"] = $TINV->value;
            // $uncert["U29"] = 1.96; // TINV(0.05;U28); FORMULA ?

            $uncert["U30"] = ($uncert["U29"]*$uncert["U27"]);
            $decimal = explode(".",$uncert["U30"]);
            $arrs = str_split($decimal[1]);
            $i=0;
            foreach($arrs as $arr)
            {
                $i++;
                if($arr!=="0") break;
            }
            $uncert["U31"] = $this->FNum($uncert["U30"],$i+1);
            $uncert["AngkaPenting"] = $i+1;

            $W28 = $uncert["U28"];
            $X28 = $uncert["U29"];
            $Y28 = $uncert["U31"];
            $hot[32][13] = $this->FNum($W28);
            $hot[32][15] = $this->FNum($X28);
            $hot[32][17] = $this->FNum($Y28);

            // // // UNCERTAINTY END

            $X34 = $P31;
            $hot[39][22] = $this->FNum($X34);
            $W33 = 28;
            $X33 = 20;
            $Y33 = 15.6;
            $Z33 = 15;
            $W34 = $X34*(1+($U11*($W33-$X33)));
            $Y34 = $X34*(1+($U11*($Y33-$X33)));
            $Z34 = $X34*(1+($U11*($Z33-$X33)));

            $hot[39][20] = $this->FNum($W34);
            $hot[39][24] = $this->FNum($Y34);
            $hot[39][26] = $this->FNum($Z34);

            $hot[41][19] = $X28;
            $hot[41][20] = $W36 = $Y28;
            $V15 = $hot[13][10];
            $hot[42][20] = $W37 = $V15;

        }

        ServiceOrders::whereId($id)->update([
            "cerapan"=>json_encode($hot),
            "uncertainty"=>json_encode($uncert)
        ]);

        return view('service.cerapanlabusatu',compact('hot','inputData',"row","inputStandard"));

    }

    private function CerapanLabuUkurDua($id)
    {
        $row = ServiceOrders::find($id);
        $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($row->hasil_uji,true);
        $inputData = $hot;
        // // User Take Order
        // $orderPaymentDate =  date("d - F - Y",strtotime($rowServiceRequest->payment_date));  // 25 Januari 2021	Tanggal bayar order
        // $hot[1][15] = $rowServiceRequest->no_order."/".$orderPaymentDate;

        $inputStandard = $this->TimbanganElektronikLabu("standard");
        $inputStandard["maxU95"] = $this->TimbanganElektronikLabu("maxU95");
        $inputStandardThermometer = $this->TimbanganDigitalLabu("standard");
        $inputStandardThermometer["maxU95"] = $this->TimbanganDigitalLabu("maxU95");

        // BACA MASA KOSONG
        $data = $this->ImbuhLabu();
        $H21 = $hot[19][1] = $hot[26][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[26][3]]);
        $H22 = $hot[20][1] = $hot[27][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[27][3]]);
        $H23 = $hot[21][1] = $hot[28][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[28][3]]);
        $massaKosongStat = false;
        if(strlen($H21)>0 && strlen($H22)>0 && strlen($H23)>0)
        {
            $massaKosongStat = true;

            $massaKosongAvg = ($H21+$H22+$H23)/3;
            $hot[22][1] = $H24 = $this->FNum($massaKosongAvg);

            $data = $this->TimbanganElektronikLabu();
            $K21 = $this->KalkulasiInterpolasi($data,$H21);
            $K22 = $this->KalkulasiInterpolasi($data,$H22);
            $K23= $this->KalkulasiInterpolasi($data,$H23);
            $hot[19][4] = $this->FNum($K21);
            $hot[20][4] = $this->FNum($K22);
            $hot[21][4] = $this->FNum($K23);
            $massaKosongKoreksi = ($K21+$K22+$K23)/3;
            $hot[22][4] = $K24 = $this->FNum($massaKosongKoreksi);
        }

        // BACA MASA ISI
        $data = $this->imbuhGelas();
        $M21 = $hot[19][5] = $hot[26][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[26][7]]);
        $M22 = $hot[20][5] = $hot[27][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[27][7]]);
        $M23 = $hot[21][5] = $hot[28][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[28][7]]);
        if(strlen($M21)>0 && strlen($M22)>0 && strlen($M23)>0)
        {
            $rataRata = ($M21+$M22+$M23)/3;
            $hot[22][5] = $M24 = $this->FNum($rataRata);

            $data = $this->TimbanganElektronikLabu();
            $O21 = $this->KalkulasiInterpolasi($data,$M21);
            $O22 = $this->KalkulasiInterpolasi($data,$M22);
            $O23 = $this->KalkulasiInterpolasi($data,$M23);
            $hot[19][8] = $this->FNum($O21);
            $hot[20][8] = $this->FNum($O22);
            $hot[21][8] = $this->FNum($O23);
            $rataRata = ($O21+$O22+$O23)/3;
            $hot[22][8] = $O24 = $this->FNum($rataRata);

            if($massaKosongStat)
            {
                $F28 = ($M21+$O21)-($H21+$K21);
                $F29 = ($M22+$O22)-($H22+$K22);
                $F30 = ($M23+$O23)-($H23+$K23);
                $hot[32][1] = $this->FNum($F28);
                $hot[33][1] = $this->FNum($F29);
                $hot[34][1] = $this->FNum($F30);

                $rataRata = ($F28+$F29+$F30)/3;
                $hot[35][1] = $F31 = $this->FNum($rataRata);
            }
        }

        // BACA TEMPERATUR
        $temperaturStat = false;
        $P21 = $hot[26][9];
        $P22 = $hot[27][9];
        $P23 = $hot[28][9];
        $R21 = $hot[26][11];
        $R22 = $hot[27][11];
        $R23 = $hot[28][11];
        if(strlen($P21)>0 && strlen($P22)>0 && strlen($P23)>0 && strlen($R21)>0 && strlen($R22)>0 && strlen($R23)>0)
        {
            $temperaturStat = true;

            $lapAtas = ($P21+$P22+$P23)/3;
            $hot[22][9] = $P24 = $this->FNum($lapAtas);

            $lapBawah = ($R21+$R22+$R23)/3;
            $hot[22][11] = $R24 = $this->FNum($lapBawah);

            $data = $this->ThermometerDigitalLabu("koreksi");
            $hot[19][14] = $T21 = $this->KalkulasiInterpolasi($data,($P21+$R21)/2);
            $hot[20][14] = $T22 = $this->KalkulasiInterpolasi($data,($P22+$R22)/2);
            $hot[21][14] = $T23 = $this->KalkulasiInterpolasi($data,($P23+$R23)/2);
            $rataRata = ($T21+$T22+$T23)/3;
            $hot[22][14] = $T24 = $this->FNum($rataRata);
        }

        // BACA BAROMETER
        $U21 = $hot[26][13];
        $U22 = $hot[27][13];
        $U23 = $hot[28][13];
        if(strlen($U21)>0 && strlen($U22)>0 && strlen($U23)>0)
        {
            $rataRata = ($U21+$U22+$U23)/3;
            $hot[22][13] = $U24 = $this->FNum($rataRata);

            $data = $this->BarometerLabu();
            // dd($data);
            $V21 = $this->KalkulasiInterpolasi($data,$U21);
            $V22 = $this->KalkulasiInterpolasi($data,$U22);
            $V23 = $this->KalkulasiInterpolasi($data,$U23);

            $hot[19][18] = $this->FNum($V21);
            $hot[20][18] = $this->FNum($V22);
            $hot[21][18] = $this->FNum($V23);
            $koreksi = ($V21+$V22+$V23)/3;
            $hot[22][18] = $V24 = $this->FNum($koreksi);
        }

        // BACA THERMOMETER
        $W21 = $hot[26][15];
        $W22 = $hot[27][15];
        $W23 = $hot[28][15];

        if( strlen($W21)>0 && strlen($W22)>0 && strlen($W23)>0)
        {
            $rataRata = ($W21+$W22+$W23)/3;

            $hot[22][15] = $W24 = $this->FNum($rataRata);

            $data = $this->ThermohygroLabu("temperatur");
            $X21 = $this->KalkulasiInterpolasi($data,$W21);
            $X22 = $this->KalkulasiInterpolasi($data,$W22);
            $X23 = $this->KalkulasiInterpolasi($data,$W23);

            // $data = $this->Thermometer("uncertainty");
            // $hot[99]["AD12"] = $this->KalkulasiInterpolasi($data,$W24);

            $hot[19][22] = $this->FNum($X21);
            $hot[20][22] = $this->FNum($X22);
            $hot[21][22] = $this->FNum($X23);
            $koreksi = ($X21+$X22+$X23)/3;
            $hot[22][22] = $X24 = $this->FNum($koreksi);
        }

        // BACA HYGROMETER
        $Y21 = $hot[26][17];
        $Y22 = $hot[27][17];
        $Y23 = $hot[28][17];
        if(strlen($Y21)>0 && strlen($Y22)>0 && strlen($Y23)>0)
        {
            $rataRata = ($Y21+$Y22+$Y23)/3;
            $hot[22][17] = $Y24 = $this->FNum($rataRata);

            $data = $this->ThermohygroLabu("thermohygro");
            $Z21 = $this->KalkulasiInterpolasi($data,$Y21);
            $Z22 = $this->KalkulasiInterpolasi($data,$Y22);
            $Z23 = $this->KalkulasiInterpolasi($data,$Y23);

            // $data = $this->Hygrometer("uncertainty");
            // $hot[99]["AD14"] = $this->KalkulasiInterpolasi($data,$Y24);

            $hot[19][26] = $this->FNum($Z21);
            $hot[20][26] = $this->FNum($Z22);
            $hot[21][26] = $this->FNum($Z23);
            $koreksi = ($Z21+$Z22+$Z23)/3;
            $hot[22][26] = $Z24 = $this->FNum($koreksi);
        }

        if($temperaturStat)
        {
            $J28 = 0.99997495*(1-(pow((($P21+$T21)+-3.983035),2)*(($P21+$T21)+301.797))/(522528.9*(($P21+$T21)+69.34881)));
            $J29 = 0.99997495*(1-(pow((($P22+$T22)+-3.983035),2)*(($P22+$T22)+301.797))/(522528.9*(($P22+$T22)+69.34881)));
            $J30 = 0.99997495*(1-(pow((($P23+$T23)+-3.983035),2)*(($P23+$T23)+301.797))/(522528.9*(($P23+$T23)+69.34881)));
            $hot[32][3] = $this->FNum($J28);
            $hot[33][3] = $this->FNum($J29);
            $hot[34][3] = $this->FNum($J30);

            $rataRata = ($J28+$J29+$J30)/3;
            $hot[35][3] = $J31 = $this->FNum($rataRata);

            $N28 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N29 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N30 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $RhoUdaraE19 = $hot[32][5] = $this->FNum($N28);
            $RhoUdaraE20 = $hot[33][5] = $this->FNum($N29);
            $RhoUdaraE21 = $hot[34][5] = $this->FNum($N30);

            $rataRata = ($N28+$N29+$N30)/3;
            $hot[35][5] = $N31 = $this->FNum($rataRata);

            $J13 = 8; // NOT INPUT DATA: ρ AT : 8
            $U11 = $hot[12][4];
            $P28 = (($F28)*pow(($J28-$N28),-1)*(1-($N28/$J13))*(1-($U11*(($P21+$T21)-20))));
            $P29 = (($F29)*pow(($J29-$N29),-1)*(1-($N29/$J13))*(1-($U11*(($P22+$T22)-20))));
            $P30 = (($F30)*pow(($J30-$N30),-1)*(1-($N30/$J13))*(1-($U11*(($P23+$T23)-20))));
            $hot[32][7] = $this->FNum($P28);
            $hot[33][7] = $this->FNum($P29);
            $hot[34][7] = $this->FNum($P30);

            $rataRata = ($P28+$P29+$P30)/3;
            $hot[35][7] = $P31 = $this->FNum($rataRata);

            $A28 = $hot[11][1];
            $U28 = $P28-($A28);
            $U29 = $P29-($A28);
            $U30 = $P30-($A28);
            $hot[32][11] = $this->FNum($U28);
            $hot[33][11] = $this->FNum($U29);
            $hot[34][11] = $this->FNum($U30);
            $rataRata = ($U28+$U29+$U30)/3;
            $hot[35][11] = $U31 = $this->FNum($rataRata);

            // UNCERTAINTY
            // 1. Repeatability pengukuran Vol pd Meniscus
            $uncert["K5"] = (max([$P28,$P29,$P30])-min([$P28,$P29,$P30]))/(2*sqrt(3));
            $uncert["Q5"] = 200;
            $uncert["S5"] = 1;
            $uncert["U5"] = $uncert["K5"]*$uncert["S5"];
            $uncert["W5"] = pow($uncert["U5"],2);
            $uncert["Y5"] = pow($uncert["U5"],4)/$uncert["Q5"];

            // 2. Massa
            $M10 = $inputStandard["dayaBaca"];
            $M14 = $inputStandard["maxU95"];
            // dd($M10,$M14);
            // a. ketidakpastian akibat penunjukkan timbangan dari sertifikat
            // pow($M14/2,2)+pow($M10/SQRT(3),2)
            $uncert["K7"] = sqrt(pow($M14/2,2)+pow($M10/SQRT(3),2));
            // b. ketidakpastian akibat massa isi
            $uncert["K8"] = sqrt(pow($M14/2,2)+pow($M10/SQRT(3),2));
            // c. ketidakpastian akibat drift dari timbangan
            // $max = 0; // Diambil yang terakhir
            // $min = 0; // Diambil yang terakhir
            $max = $inputStandard["maxU95"];
            $uncert["K9"] = 0.1*($max)/2;

            $data = $this->ImbuhLabu("kp");
            $imbuhkp1 = $data[$inputData[19][3]];
            $imbuhkp2 = $data[$inputData[20][3]];
            $imbuhkp3 = $data[$inputData[21][3]];
            $imbuhkp4 = $data[$inputData[19][7]];
            $imbuhkp5 = $data[$inputData[20][7]];
            $imbuhkp6 = $data[$inputData[21][7]];

            $uncert["K10"] = max([$imbuhkp1,$imbuhkp2,$imbuhkp3,$imbuhkp4,$imbuhkp5,$imbuhkp6])/2;
            $uncert["N6"] = pow(pow($uncert["K7"],2)+pow($uncert["K8"],2)+pow($uncert["K9"],2),0.5);
            $uncert["Q6"] = 200;
            $uncert["S6"] = (1/($J31-$N31))*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U6"] = $uncert["N6"]*$uncert["S6"];
            $uncert["W6"] = pow($uncert["U6"],2);
            $uncert["Y6"] = pow($uncert["U6"],4)/$uncert["Q6"];

            // 3. Temperatur (Air dan Instrumen)
            // A. Ketidakpastian akibat Instrumen
            $uncert["K11"] = (ABS($W24+$X24)-($P24+$T24))/SQRT(12);
            // a. ketidakpastian thermometer standar dari sertifikat thermometer
            $J16 = $inputStandardThermometer["maxU95"];
            $uncert["K13"] = $J16/2;
            // b. ketidakpastian akibat drift dari thermometer
            $uncert["K14"] = $this->DriftLabu();
            // c. ketidakpastian akibat resolusi thermometer standar
            $dayaBaca = $inputStandardThermometer["dayaBaca"];
            $uncert["K15"] = $dayaBaca/sqrt(12);
            // dd($uncert["K15"]);
            // d. ketidakpastian akibat variansi temperatur saat pengujian
            $intP21 = $P21+$T21;
            $intP22 = $P22+$T22;
            $intP23 = $P23+$T23;
            $intR21 = $R21+$T21;
            $intR22 = $R22+$T22;
            $intR23 = $R23+$T23;
            $max = max([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $min = min([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $uncert["K16"] = ($max-$min)/sqrt(12);

            // B. Ketidakpastian akibat temp air
            $uncert["K12"] = sqrt(pow($uncert["K13"],2)+pow($uncert["K14"],2)+pow($uncert["K15"],2)+pow($uncert["K16"],2));
            $uncert["N10"] = pow((pow($uncert["K11"],2)+pow($uncert["K12"],2)),0.5);
            $uncert["Q10"] = 60;
            $uncert["S10"] = $F31*(1/($J31-$N31))*(1-($N31/$J13))*(-$U11);
            $uncert["U10"] = $uncert["N10"]*$uncert["S10"];
            $uncert["W10"] = pow($uncert["U10"],2);
            $uncert["Y10"] = pow($uncert["U10"],4)/$uncert["Q10"];

            // 4. Densitas Air
            // a. ketidakpastian dari persamaan tanaka
            $uncert["K18"] = 4.5*pow(10,-7);
            // b. ketidakpastian akibat temperatur air
            $rhoAirC3 = ((-0.1176*pow(($P24+$T24),2))+(15.846*($P24+$T24))-62.677)*pow(10,-6);
            $uncert["K19"] = $uncert["K12"]*$rhoAirC3*$J31;
            // c. ketidakpastian akibat kemurnian air
            $uncert["K20"] = 5*pow(10,-6);
            $uncert["N18"] = sqrt(pow($uncert["K18"],2)+pow($uncert["K19"],2)+pow($uncert["K20"],2));
            $uncert["Q17"] = 60;
            $uncert["S17"] = (-$F31)*pow((1/($J31-$N31)),2)*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U17"] = $uncert["N18"]*$uncert["S17"];
            $uncert["W17"] = pow($uncert["U17"],2);
            $uncert["Y17"] = pow($uncert["U17"],4)/$uncert["Q17"];

            // 5. Densitas Udara
            $rhoUdaraD19 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*exp(0.061*($W21+$X21)))/(($W21+$X21)+273.15);
            $rhoUdaraD20 = ((0.34848*($U22+$V22))-(0.009*($Y22+$Z22))*exp(0.061*($W22+$X22)))/(($W22+$X22)+273.15);
            $rhoUdaraD21 = ((0.34848*($U23+$V23))-(0.009*($Y23+$Z23))*exp(0.061*($W23+$X23)))/(($W23+$X23)+273.15);
            $rataRata = ($rhoUdaraD19+$rhoUdaraD20+$rhoUdaraD21)/3;
            $rhoUdaraE23 = $rataRata/1000;

            $rhoUdaraE27 = 2.4*pow(10,-4);
            $rhoUdaraG27 = 1;
            $rhoUdaraI27 = $rhoUdaraE27*$rhoUdaraG27;
            $rhoUdaraJ27 = pow($rhoUdaraI27,2);

            $max = $this->Barometer("tekananMax");
            $rhoUdaraE28 = $max*100/2;
            $rhoUdaraG28 = 1*pow(10,-5);
            $rhoUdaraI28 = $rhoUdaraE28*$rhoUdaraG28;
            $rhoUdaraJ28 = pow($rhoUdaraI28,2);

            $dataMax = $this->ThermohygroLabu("uncertaintyCelcius");
            $rhoUdaraE29 = $dataMax/2;
            $rhoUdaraG29 = -4*pow(10,-3);
            $rhoUdaraI29 = $rhoUdaraE29*$rhoUdaraG29;
            $rhoUdaraJ29 = pow($rhoUdaraI29,2);

            $dataMax = $this->Thermohygro("persen");
            $rhoUdaraE30 = max($dataMax)/(2*100);
            $rhoUdaraG30 = -9*pow(10,-3);
            $rhoUdaraI30 = $rhoUdaraE30*$rhoUdaraG30;
            $rhoUdaraJ30 = pow($rhoUdaraI30,2);
            $rhoUdaraJ32 = $rhoUdaraE23*sqrt($rhoUdaraJ27+$rhoUdaraJ28+$rhoUdaraJ29+$rhoUdaraJ30);

            $uncert["K21"] = $rhoUdaraJ32;
            $uncert["Q21"] = 60;
            $uncert["S21"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*((1-($N31/$J13))*(1/($J31-$N31))-(1/$J13));
            $uncert["U21"] = $uncert["K21"]*$uncert["S21"];
            $uncert["W21"] = pow($uncert["U21"],2);
            $uncert["Y21"] = pow($uncert["U21"],4)/$uncert["Q21"];

            // 6. Densitas Anak Timbangan
            $uncert["K22"] = 0.14/SQRT(3); //sqrt($uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]);
            $uncert["Q22"] = 60;
            $uncert["S22"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*($N31/pow($J13,2));
            $uncert["U22"] = $uncert["K22"]*$uncert["S22"];
            $uncert["W22"] = pow($uncert["U22"],2);
            $uncert["Y22"] = pow($uncert["U22"],4)/$uncert["Q22"];

            // 7. Koefisien Muai Ruang
            $uncert["K23"] = (0.1*$U11)/sqrt(3);
            $uncert["Q23"] = 60;
            $uncert["S23"] = ($F31)*(1/($J31-$N31))*(1-($N31/$J13))*(-(($P24)+($T24)-20));
            $uncert["U23"] = $uncert["K23"]*$uncert["S23"];
            $uncert["W23"] = pow($uncert["U23"],2);
            $uncert["Y23"] = pow($uncert["U23"],4)/$uncert["Q23"];

            // 8. Pembacaan Meniskus
            $C25 = $hot[11][1];
            // Pembagian skala 10
            // $F26 = $hot[13][10];
            // $F26 = 1;//$hot[19][1];
            $F26 = 40000;//$hot[19][1];
            $uncert["K24"] = ($C25)/(sqrt(3)*$F26);
            $uncert["Q24"] = 200;
            $uncert["S24"] = 1;
            $uncert["U24"] = $uncert["K24"]*$uncert["S24"];
            $uncert["W24"] = pow($uncert["U24"],2);
            $uncert["Y24"] = pow($uncert["U24"],4)/$uncert["Q24"];

            $uncert["U25"] = $uncert["U5"]+$uncert["U6"]+$uncert["U10"]+$uncert["U17"]+$uncert["U21"]+$uncert["U22"]+$uncert["U23"]+$uncert["U24"];
            $uncert["W25"] = $uncert["W5"]+$uncert["W6"]+$uncert["W10"]+$uncert["W17"]+$uncert["W21"]+$uncert["W22"]+$uncert["W23"]+$uncert["W24"];
            $uncert["Y25"] = $uncert["Y5"]+$uncert["Y6"]+$uncert["Y10"]+$uncert["Y17"]+$uncert["Y21"]+$uncert["Y22"]+$uncert["Y23"]+$uncert["Y24"];

            $uncert["U27"] = pow($uncert["W25"],0.5);
            $uncert["U28"] = pow($uncert["U27"],4)/$uncert["Y25"];

            $df = intval($uncert["U28"])>200?200:intval($uncert["U28"]);
            $TINV = MasterStudentTable::where("pr","0.05")
            ->where("k",2)
            ->where("df",$df)->first();
            $uncert["U29"] = $TINV->value;
            // $uncert["U29"] = 1.96; // TINV(0.05;U28); FORMULA ?

            $uncert["U30"] = ($uncert["U29"]*$uncert["U27"]);
            $decimal = explode(".",$uncert["U30"]);
            $arrs = str_split($decimal[1]);
            $i=0;
            foreach($arrs as $arr)
            {
                $i++;
                if($arr!=="0") break;
            }
            $uncert["U31"] = $this->FNum($uncert["U30"],$i+1);
            $uncert["AngkaPenting"] = $i+1;

            $W28 = $uncert["U28"];
            $X28 = $uncert["U29"];
            $Y28 = $uncert["U31"];
            $hot[32][13] = $this->FNum($W28);
            $hot[32][15] = $this->FNum($X28);
            $hot[32][17] = $this->FNum($Y28);

            // // // UNCERTAINTY END

            $X34 = $P31;
            $hot[39][22] = $this->FNum($X34);
            $W33 = 28;
            $X33 = 20;
            $Y33 = 15.6;
            $Z33 = 15;
            $W34 = $X34*(1+($U11*($W33-$X33)));
            $Y34 = $X34*(1+($U11*($Y33-$X33)));
            $Z34 = $X34*(1+($U11*($Z33-$X33)));

            $hot[39][20] = $this->FNum($W34);
            $hot[39][24] = $this->FNum($Y34);
            $hot[39][26] = $this->FNum($Z34);

            $hot[41][19] = $X28;
            $hot[41][20] = $W36 = $Y28;
            $V15 = $hot[13][10];
            $hot[42][20] = $W37 = $V15;

        }

        ServiceOrders::whereId($id)->update([
            "cerapandua"=>json_encode($hot),
            "uncertaintydua"=>json_encode($uncert)
        ]);

        return view('service.cerapanlabudua',compact('hot','inputData',"row","inputStandard"));

    }

    public function recountgelasukur(Request $request)
    {
        $id = $request->id;
        $row = ServiceOrders::find($id);
        // $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        // $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($request->get("hotdata"),true);

        // // User Take Order
        $hot[3][1] = $row->MasterUsers->full_name;
        $hot[1][1] = date("d - F - Y");

        $orderPaymentDate =  date("d - F - Y",strtotime($row->ServiceRequest->payment_date));  // 25 Januari 2021	Tanggal bayar order
        $hot[2][1] = $row->ServiceRequest->no_order."/".$orderPaymentDate;

        $C16 = $hot[8][1];
        $C17 = $hot[9][1];
        $C18 = $hot[10][1];
        $C19 = $hot[11][1];
        $C20 = $hot[12][1];
        $C21 = $hot[13][1];
        $C24 = $hot[16][1];
        $C25 = $hot[17][1];
        $D21 = $hot[13][4];
        $D35 = $hot[29][3];
        $D36 = $hot[30][3];
        $hot[1][12] = "X";
        $hot[2][12] = "X";
        $hot[3][12] = "X";
        $hot[4][12] = "X";
        $hot[5][12] = "X";
        $hot[6][12] = "X";
        $hot[7][12] = "X";
        $hot[8][12] = "X";
        if(strlen($C16)>0) $hot[1][12] = "V";
        if(strlen($C17)>0) $hot[2][12] = "V";
        if(strlen($C20)>0) $hot[4][12] = "V";
        if(strlen($C21)>0) $hot[6][12] = "V";
        if(strlen($C19)>0) $hot[7][12] = "V";

        if(strlen($C25)>0 && strlen($D35)>0 && strlen($D36)>0)
        {
            $D37 = $hot[31][3] = ($D36-$D35)/$C25;
            $C38 = $hot[32][3];
            $D38 = $M35 = $this->LabuV20($C38);
            $hot[33][3] = $this->FNum($this->LabuV20($C38));
            $D39 = $hot[34][3];
            $D40 = ($D38/$D37)*(1+$D21*(28-$D39));
            $hot[35][3] = $this->FNum($D40);
            $hot[29][12] = $this->FNum($M35);
            $M36 = $hot[30][12];
            $M37 = 3*$M35/($M36);
            $hot[31][12] = $this->FNum($M37);
            $M38 = $C24*1000;
            $hot[32][12] = $this->FNum($M38);
        }

        return response($hot);
    }

    private function CerapanGelasUkur($id)
    {
        $row = ServiceOrders::find($id);
        $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($row->hasil_uji,true);
        $inputData = $hot;

        // $orderPaymentDate =  date("d - F - Y",strtotime($rowServiceRequest->payment_date));  // 25 Januari 2021	Tanggal bayar order
        // $hot[1][15] = $rowServiceRequest->no_order."/".$orderPaymentDate;

        $inputStandard = $this->TimbanganElektronikGelas("standard");
        $inputStandard["maxU95"] = $this->TimbanganElektronikGelas("maxU95");
        $inputStandardThermometer = $this->TimbanganDigitalGelas("standard");
        $inputStandardThermometer["maxU95"] = $this->TimbanganDigitalGelas("maxU95");

        // BACA MASA KOSONG
        $data = $this->ImbuhGelas();
        $H21 = $hot[20][1] = $hot[20][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[20][3]]);
        $H22 = $hot[21][1] = $hot[21][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[21][3]]);
        $H23 = $hot[22][1] = $hot[22][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[22][3]]);
        $massaKosongStat = false;
        if(strlen($H21)>0 && strlen($H22)>0 && strlen($H23)>0)
        {
            $massaKosongStat = true;

            $massaKosongAvg = ($H21+$H22+$H23)/3;
            $hot[23][1] = $H24 = $this->FNum($massaKosongAvg);

            $data = $this->TimbanganElektronikGelas();
            $K21 = $this->KalkulasiInterpolasi($data,$H21);
            $K22 = $this->KalkulasiInterpolasi($data,$H22);
            $K23= $this->KalkulasiInterpolasi($data,$H23);
            $hot[20][4] = $this->FNum($K21);
            $hot[21][4] = $this->FNum($K22);
            $hot[22][4] = $this->FNum($K23);
            $massaKosongKoreksi = ($K21+$K22+$K23)/3;
            $hot[23][4] = $K24 = $this->FNum($massaKosongKoreksi);
        }

        // BACA MASA ISI
        $data = $this->imbuhGelas();
        $M21 = $hot[20][5] = $hot[20][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[20][7]]);
        $M22 = $hot[21][5] = $hot[21][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[21][7]]);
        $M23 = $hot[22][5] = $hot[22][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[22][7]]);
        if(strlen($M21)>0 && strlen($M22)>0 && strlen($M23)>0)
        {
            $rataRata = ($M21+$M22+$M23)/3;
            $hot[23][5] = $M24 = $this->FNum($rataRata);

            $data = $this->TimbanganElektronikGelas();
            $O21 = $this->KalkulasiInterpolasi($data,$M21);
            $O22 = $this->KalkulasiInterpolasi($data,$M22);
            $O23 = $this->KalkulasiInterpolasi($data,$M23);
            $hot[20][8] = $this->FNum($O21);
            $hot[21][8] = $this->FNum($O22);
            $hot[22][8] = $this->FNum($O23);
            $rataRata = ($O21+$O22+$O23)/3;
            $hot[23][8] = $O24 = $this->FNum($rataRata);

            if($massaKosongStat)
            {
                $F28 = ($M21+$O21)-($H21+$K21);
                $F29 = ($M22+$O22)-($H22+$K22);
                $F30 = ($M23+$O23)-($H23+$K23);
                $hot[32][1] = $this->FNum($F28);
                $hot[33][1] = $this->FNum($F29);
                $hot[34][1] = $this->FNum($F30);

                $rataRata = ($F28+$F29+$F30)/3;
                $hot[35][1] = $F31 = $this->FNum($rataRata);
            }
        }

        // BACA TEMPERATUR
        $temperaturStat = false;
        $P21 = $hot[20][9];
        $P22 = $hot[21][9];
        $P23 = $hot[22][9];
        $R21 = $hot[20][11];
        $R22 = $hot[21][11];
        $R23 = $hot[22][11];
        if(strlen($P21)>0 && strlen($P22)>0 && strlen($P23)>0 && strlen($R21)>0 && strlen($R22)>0 && strlen($R23)>0)
        {
            $temperaturStat = true;

            $lapAtas = ($P21+$P22+$P23)/3;
            $hot[23][9] = $P24 = $this->FNum($lapAtas);

            $lapBawah = ($R21+$R22+$R23)/3;
            $hot[23][11] = $R24 = $this->FNum($lapBawah);

            $data = $this->TimbanganDigitalGelas();
            $hot[20][14] = $T21 = $this->KalkulasiInterpolasi($data,($P21+$R21)/2);
            $hot[21][14] = $T22 = $this->KalkulasiInterpolasi($data,($P22+$R22)/2);
            $hot[22][14] = $T23 = $this->KalkulasiInterpolasi($data,($P23+$R23)/2);
            $rataRata = ($T21+$T22+$T23)/3;
            $hot[23][14] = $T24 = $this->FNum($rataRata);
        }

        // BACA BAROMETER
        $U21 = $hot[20][13];
        $U22 = $hot[21][13];
        $U23 = $hot[22][13];
        if(strlen($U21)>0 && strlen($U22)>0 && strlen($U23)>0)
        {
            $rataRata = ($U21+$U22+$U23)/3;
            $hot[23][13] = $U24 = $this->FNum($rataRata);

            $data = $this->BarometerGelas();
            // dd($data);
            $V21 = $this->KalkulasiInterpolasi($data,$U21);
            $V22 = $this->KalkulasiInterpolasi($data,$U22);
            $V23 = $this->KalkulasiInterpolasi($data,$U23);

            $hot[20][18] = $this->FNum($V21);
            $hot[21][18] = $this->FNum($V22);
            $hot[22][18] = $this->FNum($V23);
            $koreksi = ($V21+$V22+$V23)/3;
            $hot[23][18] = $V24 = $this->FNum($koreksi);
        }

        // BACA THERMOMETER
        $W21 = $hot[20][15];
        $W22 = $hot[21][15];
        $W23 = $hot[22][15];
        if( strlen($W21)>0 && strlen($W22)>0 && strlen($W23)>0)
        {
            $rataRata = ($W21+$W22+$W23)/3;
            $hot[23][15] = $W24 = $this->FNum($rataRata);

            $data = $this->ThermohygroGelas("temperatur");
            $X21 = $this->KalkulasiInterpolasi($data,$W21);
            $X22 = $this->KalkulasiInterpolasi($data,$W22);
            $X23 = $this->KalkulasiInterpolasi($data,$W23);

            $hot[20][22] = $this->FNum($X21);
            $hot[21][22] = $this->FNum($X22);
            $hot[22][22] = $this->FNum($X23);
            $koreksi = ($X21+$X22+$X23)/3;
            $hot[23][22] = $X24 = $this->FNum($koreksi);
        }

        // BACA HYGROMETER
        $Y21 = $hot[20][17];
        $Y22 = $hot[21][17];
        $Y23 = $hot[22][17];
        if(strlen($Y21)>0 && strlen($Y22)>0 && strlen($Y23)>0)
        {
            $rataRata = ($Y21+$Y22+$Y23)/3;
            $hot[23][17] = $Y24 = $this->FNum($rataRata);

            $data = $this->ThermohygroGelas("thermohygro");
            $Z21 = $this->KalkulasiInterpolasi($data,$Y21);
            $Z22 = $this->KalkulasiInterpolasi($data,$Y22);
            $Z23 = $this->KalkulasiInterpolasi($data,$Y23);

            $hot[20][26] = $this->FNum($Z21);
            $hot[21][26] = $this->FNum($Z22);
            $hot[22][26] = $this->FNum($Z23);
            $koreksi = ($Z21+$Z22+$Z23)/3;
            $hot[23][26] = $Z24 = $this->FNum($koreksi);
        }

        if($temperaturStat)
        {
            $J28 = 0.99997495*(1-(pow((($P21+$T21)+-3.983035),2)*(($P21+$T21)+301.797))/(522528.9*(($P21+$T21)+69.34881)));
            $J29 = 0.99997495*(1-(pow((($P22+$T22)+-3.983035),2)*(($P22+$T22)+301.797))/(522528.9*(($P22+$T22)+69.34881)));
            $J30 = 0.99997495*(1-(pow((($P23+$T23)+-3.983035),2)*(($P23+$T23)+301.797))/(522528.9*(($P23+$T23)+69.34881)));
            $hot[32][3] = $this->FNum($J28);
            $hot[33][3] = $this->FNum($J29);
            $hot[34][3] = $this->FNum($J30);

            $rataRata = ($J28+$J29+$J30)/3;
            $hot[35][3] = $J31 = $this->FNum($rataRata);

            $N28 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N29 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N30 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $RhoUdaraE19 = $hot[32][6] = $this->FNum($N28);
            $RhoUdaraE20 = $hot[33][6] = $this->FNum($N29);
            $RhoUdaraE21 = $hot[34][6] = $this->FNum($N30);

            $rataRata = ($N28+$N29+$N30)/3;
            $hot[35][6] = $N31 = $this->FNum($rataRata);

            $J13 = 7.95; // NOT INPUT DATA: ρ AT : 8
            $U11 = $hot[12][4];
            $P28 = (($F28)*pow(($J28-$N28),-1)*(1-($N28/$J13))*(1-($U11*(($P21+$T21)-20))));
            $P29 = (($F29)*pow(($J29-$N29),-1)*(1-($N29/$J13))*(1-($U11*(($P22+$T22)-20))));
            $P30 = (($F30)*pow(($J30-$N30),-1)*(1-($N30/$J13))*(1-($U11*(($P23+$T23)-20))));
            $hot[32][9] = $this->FNum($P28);
            $hot[33][9] = $this->FNum($P29);
            $hot[34][9] = $this->FNum($P30);

            $rataRata = ($P28+$P29+$P30)/3;
            $hot[35][9] = $P31 = $this->FNum($rataRata);

            $A28 = $hot[16][13];
            $U28 = $P28-$A28;
            $U29 = $P29-$A28;
            $U30 = $P30-$A28;
            $hot[32][11] = $this->FNum($U28);
            $hot[33][11] = $this->FNum($U29);
            $hot[34][11] = $this->FNum($U30);
            $rataRata = ($U28+$U29+$U30)/3;
            $hot[35][11] = $U31 = $this->FNum($rataRata);

            // UNCERTAINTY
            // 1. Repeatability pengukuran Vol pd Meniscus
            $uncert["K5"] = (max([$P28,$P29,$P30])-min([$P28,$P29,$P30]))/(2*sqrt(3));
            $uncert["Q5"] = 200;
            $uncert["S5"] = 1;
            $uncert["U5"] = $uncert["K5"]*$uncert["S5"];
            $uncert["W5"] = pow($uncert["U5"],2);
            $uncert["Y5"] = pow($uncert["U5"],4)/$uncert["Q5"];

            // 2. Massa
            $M10 = $inputStandard["dayaBaca"];
            $M14 = $inputStandard["maxU95"]; //0.3
            // dd($M10,$M14);
            // a. ketidakpastian akibat penunjukkan timbangan dari sertifikat
            $uncert["K7"] = sqrt((pow($M14/2,2))+pow($M10/sqrt(3),2));
            // b. ketidakpastian akibat massa isi
            $uncert["K8"] = sqrt(pow($M14/2,2)+pow($M10/sqrt(3),2));
            // c. ketidakpastian akibat drift dari timbangan
            $max = $inputStandard["maxU95"];
            $uncert["K9"] = 0.1*($max)/2;
            $data = $this->imbuhGelas("kp");
            $imbuhkp1 = $data[$inputData[20][3]];
            $imbuhkp2 = $data[$inputData[21][3]];
            $imbuhkp3 = $data[$inputData[22][3]];
            $imbuhkp4 = $data[$inputData[20][7]];
            $imbuhkp5 = $data[$inputData[21][7]];
            $imbuhkp6 = $data[$inputData[22][7]];

            $uncert["K10"] = max([$imbuhkp1,$imbuhkp2,$imbuhkp3,$imbuhkp4,$imbuhkp5,$imbuhkp6])/2;
            $uncert["N6"] = pow(pow($uncert["K7"],2)+pow($uncert["K8"],2)+pow($uncert["K9"],2),0.5);
            $uncert["Q6"] = 200;
            $uncert["S6"] = (1/($J31-$N31))*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U6"] = $uncert["N6"]*$uncert["S6"];
            $uncert["W6"] = pow($uncert["U6"],2);
            $uncert["Y6"] = pow($uncert["U6"],4)/$uncert["Q6"];

            // 3. Temperatur (Air dan Instrumen)
            // A. Ketidakpastian akibat Instrumen
            $uncert["K11"] = (ABS($W24+$X24)-($P24+$T24))/SQRT(12);
            // a. ketidakpastian thermometer standar dari sertifikat thermometer
            $J16 = $inputStandardThermometer["maxU95"];
            $uncert["K13"] = $J16/2;
            // b. ketidakpastian akibat drift dari thermometer
            $max = $this->Drift("max");
            $min = $this->Drift();
            $uncert["K14"] = ($max-$min)/sqrt(12);
            // c. ketidakpastian akibat resolusi thermometer standar
            $dayaBaca = $inputStandardThermometer["dayaBaca"];
            $uncert["K15"] = $dayaBaca/sqrt(12);
            // d. ketidakpastian akibat variansi temperatur saat pengujian
            $intP21 = $P21+$T21;
            $intP22 = $P22+$T22;
            $intP23 = $P23+$T23;
            $intR21 = $R21+$T21;
            $intR22 = $R22+$T22;
            $intR23 = $R23+$T23;
            $max = max([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $min = min([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $uncert["K16"] = ($max-$min)/sqrt(12);

            // B. Ketidakpastian akibat temp air
            // $uncert["K12"] = sqrt(pow($uncert["K13"],2)+pow($uncert["K14"],2)+pow($uncert["K15"],2)+pow($uncert["K16"],2));
            $uncert["K12"] = sqrt(pow($uncert["K13"],2)+pow($uncert["K14"],2)+pow($uncert["K15"],2)+pow($uncert["K16"],2));
            $uncert["N10"] = SQRT(pow($uncert["K11"],2)+pow($uncert["K12"],2));
            $uncert["Q10"] = 60;
            $uncert["S10"] = $F31*(1/($J31-$N31))*(1-($N31/$J13))*(-$U11);
            $uncert["U10"] = $uncert["N10"]*$uncert["S10"];
            $uncert["W10"] = pow($uncert["U10"],2);
            $uncert["Y10"] = pow($uncert["U10"],4)/$uncert["Q10"];

            // 4. Densitas Air
            // a. ketidakpastian dari persamaan tanaka
            $uncert["K18"] = 4.5*pow(10,-7);
            // b. ketidakpastian akibat temperatur air
            $rhoAirC3 = ((-0.1176*pow(($P24+$T24),2))+(15.846*($P24+$T24))-62.677)*pow(10,-6);
            $uncert["K19"] = $uncert["K12"]*$rhoAirC3*$J31;
            // c. ketidakpastian akibat kemurnian air
            $uncert["K20"] = 5*pow(10,-6);
            $uncert["N18"] = sqrt(pow($uncert["K18"],2)+pow($uncert["K19"],2)+pow($uncert["K20"],2));
            $uncert["Q17"] = 60;
            $uncert["S17"] = (-$F31)*pow((1/($J31-$N31)),2)*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U17"] = $uncert["N18"]*$uncert["S17"];
            $uncert["W17"] = pow($uncert["U17"],2);
            $uncert["Y17"] = pow($uncert["U17"],4)/$uncert["Q17"];

            // 5. Densitas Udara
            $rhoUdaraD19 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*exp(0.061*($W21+$X21)))/(($W21+$X21)+273.15);
            $rhoUdaraD20 = ((0.34848*($U22+$V22))-(0.009*($Y22+$Z22))*exp(0.061*($W22+$X22)))/(($W22+$X22)+273.15);
            $rhoUdaraD21 = ((0.34848*($U23+$V23))-(0.009*($Y23+$Z23))*exp(0.061*($W23+$X23)))/(($W23+$X23)+273.15);
            $rataRata = ($rhoUdaraD19+$rhoUdaraD20+$rhoUdaraD21)/3;
            $rhoUdaraE23 = $rataRata/1000;

            $rhoUdaraE27 = 2.4*pow(10,-4);
            $rhoUdaraG27 = 1;
            $rhoUdaraI27 = $rhoUdaraE27*$rhoUdaraG27;
            $rhoUdaraJ27 = pow($rhoUdaraI27,2);

            $max = $this->BarometerGelas("maxU95");
            $rhoUdaraE28 = $max*100/2;
            $rhoUdaraG28 = 1*pow(10,-5);
            $rhoUdaraI28 = $rhoUdaraE28*$rhoUdaraG28;
            $rhoUdaraJ28 = pow($rhoUdaraI28,2);

            $dataMax = $this->ThermohygroGelas("uncertaintyCelcius");
            $rhoUdaraE29 = $dataMax/2;
            $rhoUdaraG29 = -4*pow(10,-3);
            $rhoUdaraI29 = $rhoUdaraE29*$rhoUdaraG29;
            $rhoUdaraJ29 = pow($rhoUdaraI29,2);

            $dataMax = $this->ThermohygroGelas("uncertaintyPersen");
            $rhoUdaraE30 = $dataMax/(2*100);
            $rhoUdaraG30 = -9*pow(10,-3);
            $rhoUdaraI30 = $rhoUdaraE30*$rhoUdaraG30;
            $rhoUdaraJ30 = pow($rhoUdaraI30,2);
            $rhoUdaraJ32 = $rhoUdaraE23*sqrt($rhoUdaraJ27+$rhoUdaraJ28+$rhoUdaraJ29+$rhoUdaraJ30);

            $uncert["K21"] = $rhoUdaraJ32;
            $uncert["Q21"] = 60;
            $uncert["S21"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*((1-($N31/$J13))*(1/($J31-$N31))-(1/$J13));
            $uncert["U21"] = $uncert["K21"]*$uncert["S21"];
            $uncert["W21"] = pow($uncert["U21"],2);
            $uncert["Y21"] = pow($uncert["U21"],4)/$uncert["Q21"];

            // 6. Densitas Anak Timbangan
            $uncert["K22"] = 0.14/SQRT(3); //sqrt($uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]);
            $uncert["Q22"] = 60;
            $uncert["S22"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*($N31/pow($J13,2));
            $uncert["U22"] = $uncert["K22"]*$uncert["S22"];
            $uncert["W22"] = pow($uncert["U22"],2);
            $uncert["Y22"] = pow($uncert["U22"],4)/$uncert["Q22"];

            // 7. Koefisien Muai Ruang
            $uncert["K23"] = (0.1*$U11)/sqrt(3);
            $uncert["Q23"] = 60;
            $uncert["S23"] = ($F31)*(1/($J31-$N31))*(1-($N31/$J13))*(-(($P24)+($T24)-20));
            $uncert["U23"] = $uncert["K23"]*$uncert["S23"];
            $uncert["W23"] = pow($uncert["U23"],2);
            $uncert["Y23"] = pow($uncert["U23"],4)/$uncert["Q23"];

            // 8. Pembacaan Meniskus
            $C25 = $hot[16][13];
            // Pembagian skala 10
            // $F26 = $hot[13][10];
            // $F26 = 1;//$hot[19][1];
            $F26 = 40000;//$hot[19][1];
            $uncert["K24"] = ($C25)/(sqrt(3)*$F26);
            // $uncert["K24"] = (100)/(40000*SQRT(3));
            $uncert["Q24"] = 200;
            $uncert["S24"] = 1;
            $uncert["U24"] = $uncert["K24"]*$uncert["S24"];
            $uncert["W24"] = pow($uncert["U24"],2);
            $uncert["Y24"] = pow($uncert["U24"],4)/$uncert["Q24"];

            $uncert["U25"] = $uncert["U5"]+$uncert["U6"]+$uncert["U10"]+$uncert["U17"]+$uncert["U21"]+$uncert["U22"]+$uncert["U23"]+$uncert["U24"];
            $uncert["W25"] = $uncert["W5"]+$uncert["W6"]+$uncert["W10"]+$uncert["W17"]+$uncert["W21"]+$uncert["W22"]+$uncert["W23"]+$uncert["W24"];
            $uncert["Y25"] = $uncert["Y5"]+$uncert["Y6"]+$uncert["Y10"]+$uncert["Y17"]+$uncert["Y21"]+$uncert["Y22"]+$uncert["Y23"]+$uncert["Y24"];

            $uncert["U27"] = pow($uncert["W25"],0.5);
            $uncert["U28"] = pow($uncert["U27"],4)/$uncert["Y25"];

            $df = intval($uncert["U28"])>200?200:intval($uncert["U28"]);
            $TINV = MasterStudentTable::where("pr","0.05")
            ->where("k",2)
            ->where("df",$df)->first();
            $uncert["U29"] = $TINV->value;
            // $uncert["U29"] = 1.96; // TINV(0.05;U28); FORMULA ?
            // =U30*U28
            $uncert["U30"] = ($uncert["U29"]*$uncert["U27"]);
            $decimal = explode(".",$uncert["U30"]);
            $arrs = str_split($decimal[1]);
            $i=0;
            foreach($arrs as $arr)
            {
                $i++;
                if($arr!=="0") break;
            }
            $uncert["U31"] = $this->FNum($uncert["U30"],$i+1);
            $uncert["AngkaPenting"] = $i+1;

            $W28 = $uncert["U28"];
            $X28 = $uncert["U29"];
            $Y28 = $uncert["U31"];
            $hot[32][13] = $this->FNum($W28);
            $hot[32][15] = $this->FNum($X28);
            $hot[32][17] = $this->FNum($Y28);

            // UNCERTAINTY END

            $X34 = $P31;
            $hot[39][22] = $this->FNum($X34);
            $W33 = 28; //$hot[38][20];
            $X33 = 20; //$hot[38][22];
            $Y33 = 15.6; //$hot[38][24];
            $Z33 = 15; //$hot[38][26];
            $W34 = $X34*(1+($U11*($W33-$X33)));
            $Y34 = $X34*(1+($U11*($Y33-$X33)));
            $Z34 = $X34*(1+($U11*($Z33-$X33)));

            $hot[39][20] = $this->FNum($W34);
            $hot[39][24] = $this->FNum($Y34);
            $hot[39][26] = $this->FNum($Z34);

            $hot[41][19] = $X28;
            $hot[41][20] = $W36 = $Y28;
            $V15 = $hot[13][10];
            $hot[42][20] = $W37 = $V15;
        }

        ServiceOrders::whereId($id)->update([
            "cerapan"=>json_encode($hot),
            "uncertainty"=>json_encode($uncert)
        ]);

        return view('service.cerapangelassatu',compact('hot','row','inputData',"inputStandard","inputStandardThermometer"));
    }

    private function CerapanGelasUkurDua($id)
    {
        $row = ServiceOrders::find($id);
        $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($row->hasil_uji,true);
        $inputData = $hot;

        // $orderPaymentDate =  date("d - F - Y",strtotime($rowServiceRequest->payment_date));  // 25 Januari 2021	Tanggal bayar order
        // $hot[1][15] = $rowServiceRequest->no_order."/".$orderPaymentDate;

        $inputStandard = $this->TimbanganElektronikGelas("standard");
        $inputStandard["maxU95"] = $this->TimbanganElektronikGelas("maxU95");
        $inputStandardThermometer = $this->TimbanganDigitalGelas("standard");
        $inputStandardThermometer["maxU95"] = $this->TimbanganDigitalGelas("maxU95");

        // BACA MASA KOSONG
        $data = $this->ImbuhGelas();
        $H21 = $hot[20][1] = $hot[28][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[28][3]]);
        $H22 = $hot[21][1] = $hot[29][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[29][3]]);
        $H23 = $hot[22][1] = $hot[30][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[30][3]]);
        $massaKosongStat = false;
        if(strlen($H21)>0 && strlen($H22)>0 && strlen($H23)>0)
        {
            $massaKosongStat = true;

            $massaKosongAvg = ($H21+$H22+$H23)/3;
            $hot[23][1] = $H24 = $this->FNum($massaKosongAvg);

            $data = $this->TimbanganElektronikGelas();
            $K21 = $this->KalkulasiInterpolasi($data,$H21);
            $K22 = $this->KalkulasiInterpolasi($data,$H22);
            $K23= $this->KalkulasiInterpolasi($data,$H23);
            $hot[20][4] = $this->FNum($K21);
            $hot[21][4] = $this->FNum($K22);
            $hot[22][4] = $this->FNum($K23);
            $massaKosongKoreksi = ($K21+$K22+$K23)/3;
            $hot[23][4] = $K24 = $this->FNum($massaKosongKoreksi);
        }

        // BACA MASA ISI
        $data = $this->imbuhGelas();
        $M21 = $hot[20][5] = $hot[28][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[28][7]]);
        $M22 = $hot[21][5] = $hot[29][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[29][7]]);
        $M23 = $hot[22][5] = $hot[30][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[30][7]]);
        if(strlen($M21)>0 && strlen($M22)>0 && strlen($M23)>0)
        {
            $rataRata = ($M21+$M22+$M23)/3;
            $hot[23][5] = $M24 = $this->FNum($rataRata);

            $data = $this->TimbanganElektronikGelas();
            $O21 = $this->KalkulasiInterpolasi($data,$M21);
            $O22 = $this->KalkulasiInterpolasi($data,$M22);
            $O23 = $this->KalkulasiInterpolasi($data,$M23);
            $hot[20][8] = $this->FNum($O21);
            $hot[21][8] = $this->FNum($O22);
            $hot[22][8] = $this->FNum($O23);
            $rataRata = ($O21+$O22+$O23)/3;
            $hot[23][8] = $O24 = $this->FNum($rataRata);

            if($massaKosongStat)
            {
                $F28 = ($M21+$O21)-($H21+$K21);
                $F29 = ($M22+$O22)-($H22+$K22);
                $F30 = ($M23+$O23)-($H23+$K23);
                $hot[32][1] = $this->FNum($F28);
                $hot[33][1] = $this->FNum($F29);
                $hot[34][1] = $this->FNum($F30);

                $rataRata = ($F28+$F29+$F30)/3;
                $hot[35][1] = $F31 = $this->FNum($rataRata);
            }
        }

        // BACA TEMPERATUR
        $temperaturStat = false;
        $P21 = $hot[20][9] = $hot[28][9];
        $P22 = $hot[21][9] = $hot[29][9];
        $P23 = $hot[22][9] = $hot[30][9];
        $R21 = $hot[20][11] = $hot[28][11];
        $R22 = $hot[21][11] = $hot[29][11];
        $R23 = $hot[22][11] = $hot[30][11];
        if(strlen($P21)>0 && strlen($P22)>0 && strlen($P23)>0 && strlen($R21)>0 && strlen($R22)>0 && strlen($R23)>0)
        {
            $temperaturStat = true;

            $lapAtas = ($P21+$P22+$P23)/3;
            $hot[23][9] = $P24 = $this->FNum($lapAtas);

            $lapBawah = ($R21+$R22+$R23)/3;
            $hot[23][11] = $R24 = $this->FNum($lapBawah);

            $data = $this->TimbanganDigitalGelas();
            $hot[20][14] = $T21 = $this->KalkulasiInterpolasi($data,($P21+$R21)/2);
            $hot[21][14] = $T22 = $this->KalkulasiInterpolasi($data,($P22+$R22)/2);
            $hot[22][14] = $T23 = $this->KalkulasiInterpolasi($data,($P23+$R23)/2);
            $rataRata = ($T21+$T22+$T23)/3;
            $hot[23][14] = $T24 = $this->FNum($rataRata);
        }

        // BACA BAROMETER
        $U21 = $hot[20][13] = $hot[28][13];
        $U22 = $hot[21][13] = $hot[29][13];
        $U23 = $hot[22][13] = $hot[30][13];
        if(strlen($U21)>0 && strlen($U22)>0 && strlen($U23)>0)
        {
            $rataRata = ($U21+$U22+$U23)/3;
            $hot[23][13] = $U24 = $this->FNum($rataRata);

            $data = $this->BarometerGelas();
            // dd($data);
            $V21 = $this->KalkulasiInterpolasi($data,$U21);
            $V22 = $this->KalkulasiInterpolasi($data,$U22);
            $V23 = $this->KalkulasiInterpolasi($data,$U23);

            $hot[20][18] = $this->FNum($V21);
            $hot[21][18] = $this->FNum($V22);
            $hot[22][18] = $this->FNum($V23);
            $koreksi = ($V21+$V22+$V23)/3;
            $hot[23][18] = $V24 = $this->FNum($koreksi);
        }

        // BACA THERMOMETER
        $W21 = $hot[20][15] = $hot[28][15];
        $W22 = $hot[21][15] = $hot[29][15];
        $W23 = $hot[22][15] = $hot[30][15];
        if( strlen($W21)>0 && strlen($W22)>0 && strlen($W23)>0)
        {
            $rataRata = ($W21+$W22+$W23)/3;
            $hot[23][15] = $W24 = $this->FNum($rataRata);

            $data = $this->ThermohygroGelas("temperatur");
            $X21 = $this->KalkulasiInterpolasi($data,$W21);
            $X22 = $this->KalkulasiInterpolasi($data,$W22);
            $X23 = $this->KalkulasiInterpolasi($data,$W23);

            $hot[20][22] = $this->FNum($X21);
            $hot[21][22] = $this->FNum($X22);
            $hot[22][22] = $this->FNum($X23);
            $koreksi = ($X21+$X22+$X23)/3;
            $hot[23][22] = $X24 = $this->FNum($koreksi);
        }

        // BACA HYGROMETER
        $Y21 = $hot[20][17] = $hot[28][17];
        $Y22 = $hot[21][17] = $hot[29][17];
        $Y23 = $hot[22][17] = $hot[30][17];
        if(strlen($Y21)>0 && strlen($Y22)>0 && strlen($Y23)>0)
        {
            $rataRata = ($Y21+$Y22+$Y23)/3;
            $hot[23][17] = $Y24 = $this->FNum($rataRata);

            $data = $this->ThermohygroGelas("thermohygro");
            $Z21 = $this->KalkulasiInterpolasi($data,$Y21);
            $Z22 = $this->KalkulasiInterpolasi($data,$Y22);
            $Z23 = $this->KalkulasiInterpolasi($data,$Y23);

            $hot[20][26] = $this->FNum($Z21);
            $hot[21][26] = $this->FNum($Z22);
            $hot[22][26] = $this->FNum($Z23);
            $koreksi = ($Z21+$Z22+$Z23)/3;
            $hot[23][26] = $Z24 = $this->FNum($koreksi);
        }

        if($temperaturStat)
        {
            $J28 = 0.99997495*(1-(pow((($P21+$T21)+-3.983035),2)*(($P21+$T21)+301.797))/(522528.9*(($P21+$T21)+69.34881)));
            $J29 = 0.99997495*(1-(pow((($P22+$T22)+-3.983035),2)*(($P22+$T22)+301.797))/(522528.9*(($P22+$T22)+69.34881)));
            $J30 = 0.99997495*(1-(pow((($P23+$T23)+-3.983035),2)*(($P23+$T23)+301.797))/(522528.9*(($P23+$T23)+69.34881)));
            $hot[32][3] = $this->FNum($J28);
            $hot[33][3] = $this->FNum($J29);
            $hot[34][3] = $this->FNum($J30);

            $rataRata = ($J28+$J29+$J30)/3;
            $hot[35][3] = $J31 = $this->FNum($rataRata);

            $N28 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N29 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N30 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $RhoUdaraE19 = $hot[32][6] = $this->FNum($N28);
            $RhoUdaraE20 = $hot[33][6] = $this->FNum($N29);
            $RhoUdaraE21 = $hot[34][6] = $this->FNum($N30);

            $rataRata = ($N28+$N29+$N30)/3;
            $hot[35][6] = $N31 = $this->FNum($rataRata);

            $J13 = 7.95; // NOT INPUT DATA: ρ AT : 8
            $U11 = $hot[12][4];
            $P28 = (($F28)*pow(($J28-$N28),-1)*(1-($N28/$J13))*(1-($U11*(($P21+$T21)-20))));
            $P29 = (($F29)*pow(($J29-$N29),-1)*(1-($N29/$J13))*(1-($U11*(($P22+$T22)-20))));
            $P30 = (($F30)*pow(($J30-$N30),-1)*(1-($N30/$J13))*(1-($U11*(($P23+$T23)-20))));
            $hot[32][9] = $this->FNum($P28);
            $hot[33][9] = $this->FNum($P29);
            $hot[34][9] = $this->FNum($P30);

            $rataRata = ($P28+$P29+$P30)/3;
            $hot[35][9] = $P31 = $this->FNum($rataRata);

            $A28 = $hot[24][13]; //Volume titik uji
            $U28 = $P28-$A28;
            $U29 = $P29-$A28;
            $U30 = $P30-$A28;
            $hot[32][11] = $this->FNum($U28);
            $hot[33][11] = $this->FNum($U29);
            $hot[34][11] = $this->FNum($U30);
            $rataRata = ($U28+$U29+$U30)/3;
            $hot[35][11] = $U31 = $this->FNum($rataRata);

            // UNCERTAINTY
            // 1. Repeatability pengukuran Vol pd Meniscus
            $uncert["K5"] = (max([$P28,$P29,$P30])-min([$P28,$P29,$P30]))/(2*sqrt(3));
            $uncert["Q5"] = 200;
            $uncert["S5"] = 1;
            $uncert["U5"] = $uncert["K5"]*$uncert["S5"];
            $uncert["W5"] = pow($uncert["U5"],2);
            $uncert["Y5"] = pow($uncert["U5"],4)/$uncert["Q5"];

            // 2. Massa
            $M10 = $inputStandard["dayaBaca"];
            $M14 = $inputStandard["maxU95"]; //0.3
            // dd($M10,$M14);
            // a. ketidakpastian akibat penunjukkan timbangan dari sertifikat
            $uncert["K7"] = sqrt((pow($M14/2,2))+pow($M10/sqrt(3),2));
            // b. ketidakpastian akibat massa isi
            $uncert["K8"] = sqrt(pow($M14/2,2)+pow($M10/sqrt(3),2));
            // c. ketidakpastian akibat drift dari timbangan
            $max = $inputStandard["maxU95"];
            $uncert["K9"] = 0.1*($max)/2;
            $data = $this->imbuhGelas("kp");
            $imbuhkp1 = $data[$inputData[20][3]];
            $imbuhkp2 = $data[$inputData[21][3]];
            $imbuhkp3 = $data[$inputData[22][3]];
            $imbuhkp4 = $data[$inputData[20][7]];
            $imbuhkp5 = $data[$inputData[21][7]];
            $imbuhkp6 = $data[$inputData[22][7]];

            $uncert["K10"] = max([$imbuhkp1,$imbuhkp2,$imbuhkp3,$imbuhkp4,$imbuhkp5,$imbuhkp6])/2;
            $uncert["N6"] = pow(pow($uncert["K7"],2)+pow($uncert["K8"],2)+pow($uncert["K9"],2),0.5);
            $uncert["Q6"] = 200;
            $uncert["S6"] = (1/($J31-$N31))*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U6"] = $uncert["N6"]*$uncert["S6"];
            $uncert["W6"] = pow($uncert["U6"],2);
            $uncert["Y6"] = pow($uncert["U6"],4)/$uncert["Q6"];

            // 3. Temperatur (Air dan Instrumen)
            // A. Ketidakpastian akibat Instrumen
            $uncert["K11"] = (ABS($W24+$X24)-($P24+$T24))/SQRT(12);
            // a. ketidakpastian thermometer standar dari sertifikat thermometer
            $J16 = $inputStandardThermometer["maxU95"];
            $uncert["K13"] = $J16/2;
            // b. ketidakpastian akibat drift dari thermometer
            $max = $this->Drift("max");
            $min = $this->Drift();
            $uncert["K14"] = ($max-$min)/sqrt(12);
            // c. ketidakpastian akibat resolusi thermometer standar
            $dayaBaca = $inputStandardThermometer["dayaBaca"];
            $uncert["K15"] = $dayaBaca/sqrt(12);
            // d. ketidakpastian akibat variansi temperatur saat pengujian
            $intP21 = $P21+$T21;
            $intP22 = $P22+$T22;
            $intP23 = $P23+$T23;
            $intR21 = $R21+$T21;
            $intR22 = $R22+$T22;
            $intR23 = $R23+$T23;
            $max = max([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $min = min([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $uncert["K16"] = ($max-$min)/sqrt(12);

            // B. Ketidakpastian akibat temp air
            // $uncert["K12"] = sqrt(pow($uncert["K13"],2)+pow($uncert["K14"],2)+pow($uncert["K15"],2)+pow($uncert["K16"],2));
            $uncert["K12"] = sqrt(pow($uncert["K13"],2)+pow($uncert["K14"],2)+pow($uncert["K15"],2)+pow($uncert["K16"],2));
            $uncert["N10"] = SQRT(pow($uncert["K11"],2)+pow($uncert["K12"],2));
            $uncert["Q10"] = 60;
            $uncert["S10"] = $F31*(1/($J31-$N31))*(1-($N31/$J13))*(-$U11);
            $uncert["U10"] = $uncert["N10"]*$uncert["S10"];
            $uncert["W10"] = pow($uncert["U10"],2);
            $uncert["Y10"] = pow($uncert["U10"],4)/$uncert["Q10"];

            // 4. Densitas Air
            // a. ketidakpastian dari persamaan tanaka
            $uncert["K18"] = 4.5*pow(10,-7);
            // b. ketidakpastian akibat temperatur air
            $rhoAirC3 = ((-0.1176*pow(($P24+$T24),2))+(15.846*($P24+$T24))-62.677)*pow(10,-6);
            $uncert["K19"] = $uncert["K12"]*$rhoAirC3*$J31;
            // c. ketidakpastian akibat kemurnian air
            $uncert["K20"] = 5*pow(10,-6);
            $uncert["N18"] = sqrt(pow($uncert["K18"],2)+pow($uncert["K19"],2)+pow($uncert["K20"],2));
            $uncert["Q17"] = 60;
            $uncert["S17"] = (-$F31)*pow((1/($J31-$N31)),2)*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U17"] = $uncert["N18"]*$uncert["S17"];
            $uncert["W17"] = pow($uncert["U17"],2);
            $uncert["Y17"] = pow($uncert["U17"],4)/$uncert["Q17"];

            // 5. Densitas Udara
            $rhoUdaraD19 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*exp(0.061*($W21+$X21)))/(($W21+$X21)+273.15);
            $rhoUdaraD20 = ((0.34848*($U22+$V22))-(0.009*($Y22+$Z22))*exp(0.061*($W22+$X22)))/(($W22+$X22)+273.15);
            $rhoUdaraD21 = ((0.34848*($U23+$V23))-(0.009*($Y23+$Z23))*exp(0.061*($W23+$X23)))/(($W23+$X23)+273.15);
            $rataRata = ($rhoUdaraD19+$rhoUdaraD20+$rhoUdaraD21)/3;
            $rhoUdaraE23 = $rataRata/1000;

            $rhoUdaraE27 = 2.4*pow(10,-4);
            $rhoUdaraG27 = 1;
            $rhoUdaraI27 = $rhoUdaraE27*$rhoUdaraG27;
            $rhoUdaraJ27 = pow($rhoUdaraI27,2);

            $max = $this->BarometerGelas("maxU95");
            $rhoUdaraE28 = $max*100/2;
            $rhoUdaraG28 = 1*pow(10,-5);
            $rhoUdaraI28 = $rhoUdaraE28*$rhoUdaraG28;
            $rhoUdaraJ28 = pow($rhoUdaraI28,2);

            $dataMax = $this->ThermohygroGelas("uncertaintyCelcius");
            $rhoUdaraE29 = $dataMax/2;
            $rhoUdaraG29 = -4*pow(10,-3);
            $rhoUdaraI29 = $rhoUdaraE29*$rhoUdaraG29;
            $rhoUdaraJ29 = pow($rhoUdaraI29,2);

            $dataMax = $this->ThermohygroGelas("uncertaintyPersen");
            $rhoUdaraE30 = $dataMax/(2*100);
            $rhoUdaraG30 = -9*pow(10,-3);
            $rhoUdaraI30 = $rhoUdaraE30*$rhoUdaraG30;
            $rhoUdaraJ30 = pow($rhoUdaraI30,2);
            $rhoUdaraJ32 = $rhoUdaraE23*sqrt($rhoUdaraJ27+$rhoUdaraJ28+$rhoUdaraJ29+$rhoUdaraJ30);

            $uncert["K21"] = $rhoUdaraJ32;
            $uncert["Q21"] = 60;
            $uncert["S21"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*((1-($N31/$J13))*(1/($J31-$N31))-(1/$J13));
            $uncert["U21"] = $uncert["K21"]*$uncert["S21"];
            $uncert["W21"] = pow($uncert["U21"],2);
            $uncert["Y21"] = pow($uncert["U21"],4)/$uncert["Q21"];

            // 6. Densitas Anak Timbangan
            $uncert["K22"] = 0.14/SQRT(3); //sqrt($uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]);
            $uncert["Q22"] = 60;
            $uncert["S22"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*($N31/pow($J13,2));
            $uncert["U22"] = $uncert["K22"]*$uncert["S22"];
            $uncert["W22"] = pow($uncert["U22"],2);
            $uncert["Y22"] = pow($uncert["U22"],4)/$uncert["Q22"];

            // 7. Koefisien Muai Ruang
            $uncert["K23"] = (0.1*$U11)/sqrt(3);
            $uncert["Q23"] = 60;
            $uncert["S23"] = ($F31)*(1/($J31-$N31))*(1-($N31/$J13))*(-(($P24)+($T24)-20));
            $uncert["U23"] = $uncert["K23"]*$uncert["S23"];
            $uncert["W23"] = pow($uncert["U23"],2);
            $uncert["Y23"] = pow($uncert["U23"],4)/$uncert["Q23"];

            // 8. Pembacaan Meniskus
            $C25 = $hot[24][13];
            // Pembagian skala 10
            // $F26 = $hot[13][10];
            // $F26 = 1;//$hot[19][1];
            $F26 = 40000;//$hot[19][1];
            $uncert["K24"] = ($C25)/(sqrt(3)*$F26);
            // $uncert["K24"] = (100)/(40000*SQRT(3));
            $uncert["Q24"] = 200;
            $uncert["S24"] = 1;
            $uncert["U24"] = $uncert["K24"]*$uncert["S24"];
            $uncert["W24"] = pow($uncert["U24"],2);
            $uncert["Y24"] = pow($uncert["U24"],4)/$uncert["Q24"];

            $uncert["U25"] = $uncert["U5"]+$uncert["U6"]+$uncert["U10"]+$uncert["U17"]+$uncert["U21"]+$uncert["U22"]+$uncert["U23"]+$uncert["U24"];
            $uncert["W25"] = $uncert["W5"]+$uncert["W6"]+$uncert["W10"]+$uncert["W17"]+$uncert["W21"]+$uncert["W22"]+$uncert["W23"]+$uncert["W24"];
            $uncert["Y25"] = $uncert["Y5"]+$uncert["Y6"]+$uncert["Y10"]+$uncert["Y17"]+$uncert["Y21"]+$uncert["Y22"]+$uncert["Y23"]+$uncert["Y24"];

            $uncert["U27"] = pow($uncert["W25"],0.5);
            $uncert["U28"] = pow($uncert["U27"],4)/$uncert["Y25"];

            $df = intval($uncert["U28"])>200?200:intval($uncert["U28"]);
            $TINV = MasterStudentTable::where("pr","0.05")
            ->where("k",2)
            ->where("df",$df)->first();
            $uncert["U29"] = $TINV->value;
            // $uncert["U29"] = 1.96; // TINV(0.05;U28); FORMULA ?
            // =U30*U28
            $uncert["U30"] = ($uncert["U29"]*$uncert["U27"]);
            $decimal = explode(".",$uncert["U30"]);
            $arrs = str_split($decimal[1]);
            $i=0;
            foreach($arrs as $arr)
            {
                $i++;
                if($arr!=="0") break;
            }
            $uncert["U31"] = $this->FNum($uncert["U30"],$i+1);
            $uncert["AngkaPenting"] = $i+1;

            $W28 = $uncert["U28"];
            $X28 = $uncert["U29"];
            $Y28 = $uncert["U31"];
            $hot[32][13] = $this->FNum($W28);
            $hot[32][15] = $this->FNum($X28);
            $hot[32][17] = $this->FNum($Y28);

            // UNCERTAINTY END

            $X34 = $P31;
            $hot[39][22] = $this->FNum($X34);
            $W33 = 28;
            $X33 = 20;
            $Y33 = 15.6;
            $Z33 = 15;
            $W34 = $X34*(1+($U11*($W33-$X33)));
            $Y34 = $X34*(1+($U11*($Y33-$X33)));
            $Z34 = $X34*(1+($U11*($Z33-$X33)));

            $hot[39][20] = $this->FNum($W34);
            $hot[39][24] = $this->FNum($Y34);
            $hot[39][26] = $this->FNum($Z34);

            $hot[41][19] = $X28;
            $hot[41][20] = $W36 = $Y28;
            $V15 = $hot[13][10];
            $hot[42][20] = $W37 = $V15;
        }

        ServiceOrders::whereId($id)->update([
            "cerapandua"=>json_encode($hot),
            "uncertaintydua"=>json_encode($uncert)
        ]);

        return view('service.cerapangelasdua',compact('hot','row','inputData',"inputStandard","inputStandardThermometer"));
    }

    private function CerapanGelasUkurTiga($id)
    {
        $row = ServiceOrders::find($id);
        $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($row->hasil_uji,true);
        $inputData = $hot;

        // $orderPaymentDate =  date("d - F - Y",strtotime($rowServiceRequest->payment_date));  // 25 Januari 2021	Tanggal bayar order
        // $hot[1][15] = $rowServiceRequest->no_order."/".$orderPaymentDate;

        $inputStandard = $this->TimbanganElektronikGelas("standard");
        $inputStandard["maxU95"] = $this->TimbanganElektronikGelas("maxU95");
        $inputStandardThermometer = $this->TimbanganDigitalGelas("standard");
        $inputStandardThermometer["maxU95"] = $this->TimbanganDigitalGelas("maxU95");

        // BACA MASA KOSONG
        $data = $this->ImbuhGelas();
        $H21 = $hot[20][1] = $hot[36][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[36][3]]);
        $H22 = $hot[21][1] = $hot[37][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[37][3]]);
        $H23 = $hot[22][1] = $hot[38][1]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[38][3]]);
        $massaKosongStat = false;
        if(strlen($H21)>0 && strlen($H22)>0 && strlen($H23)>0)
        {
            $massaKosongStat = true;

            $massaKosongAvg = ($H21+$H22+$H23)/3;
            $hot[23][1] = $H24 = $this->FNum($massaKosongAvg);

            $data = $this->TimbanganElektronikGelas();
            $K21 = $this->KalkulasiInterpolasi($data,$H21);
            $K22 = $this->KalkulasiInterpolasi($data,$H22);
            $K23= $this->KalkulasiInterpolasi($data,$H23);
            $hot[20][4] = $this->FNum($K21);
            $hot[21][4] = $this->FNum($K22);
            $hot[22][4] = $this->FNum($K23);
            $massaKosongKoreksi = ($K21+$K22+$K23)/3;
            $hot[23][4] = $K24 = $this->FNum($massaKosongKoreksi);
        }

        // BACA MASA ISI
        $data = $this->imbuhGelas();
        $M21 = $hot[20][5] = $hot[36][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[36][7]]);
        $M22 = $hot[21][5] = $hot[37][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[37][7]]);
        $M23 = $hot[22][5] = $hot[38][5]+(0.5*$inputStandard["dayaBaca"])-($data[$hot[38][7]]);
        if(strlen($M21)>0 && strlen($M22)>0 && strlen($M23)>0)
        {
            $rataRata = ($M21+$M22+$M23)/3;
            $hot[23][5] = $M24 = $this->FNum($rataRata);

            $data = $this->TimbanganElektronikGelas();
            $O21 = $this->KalkulasiInterpolasi($data,$M21);
            $O22 = $this->KalkulasiInterpolasi($data,$M22);
            $O23 = $this->KalkulasiInterpolasi($data,$M23);
            $hot[20][8] = $this->FNum($O21);
            $hot[21][8] = $this->FNum($O22);
            $hot[22][8] = $this->FNum($O23);
            $rataRata = ($O21+$O22+$O23)/3;
            $hot[23][8] = $O24 = $this->FNum($rataRata);

            if($massaKosongStat)
            {
                $F28 = ($M21+$O21)-($H21+$K21);
                $F29 = ($M22+$O22)-($H22+$K22);
                $F30 = ($M23+$O23)-($H23+$K23);
                $hot[32][1] = $this->FNum($F28);
                $hot[33][1] = $this->FNum($F29);
                $hot[34][1] = $this->FNum($F30);

                $rataRata = ($F28+$F29+$F30)/3;
                $hot[35][1] = $F31 = $this->FNum($rataRata);
            }
        }

        // BACA TEMPERATUR
        $temperaturStat = false;
        $P21 = $hot[20][9] = $hot[36][9];
        $P22 = $hot[21][9] = $hot[37][9];
        $P23 = $hot[22][9] = $hot[38][9];
        $R21 = $hot[20][11] = $hot[36][11];
        $R22 = $hot[21][11] = $hot[37][11];
        $R23 = $hot[22][11] = $hot[38][11];
        if(strlen($P21)>0 && strlen($P22)>0 && strlen($P23)>0 && strlen($R21)>0 && strlen($R22)>0 && strlen($R23)>0)
        {
            $temperaturStat = true;

            $lapAtas = ($P21+$P22+$P23)/3;
            $hot[23][9] = $P24 = $this->FNum($lapAtas);

            $lapBawah = ($R21+$R22+$R23)/3;
            $hot[23][11] = $R24 = $this->FNum($lapBawah);

            $data = $this->TimbanganDigitalGelas();
            $hot[20][14] = $T21 = $this->KalkulasiInterpolasi($data,($P21+$R21)/2);
            $hot[21][14] = $T22 = $this->KalkulasiInterpolasi($data,($P22+$R22)/2);
            $hot[22][14] = $T23 = $this->KalkulasiInterpolasi($data,($P23+$R23)/2);
            $rataRata = ($T21+$T22+$T23)/3;
            $hot[23][14] = $T24 = $this->FNum($rataRata);
        }

        // BACA BAROMETER
        $U21 = $hot[20][13] = $hot[36][13];
        $U22 = $hot[21][13] = $hot[37][13];
        $U23 = $hot[22][13] = $hot[38][13];
        if(strlen($U21)>0 && strlen($U22)>0 && strlen($U23)>0)
        {
            $rataRata = ($U21+$U22+$U23)/3;
            $hot[23][13] = $U24 = $this->FNum($rataRata);

            $data = $this->BarometerGelas();
            // dd($data);
            $V21 = $this->KalkulasiInterpolasi($data,$U21);
            $V22 = $this->KalkulasiInterpolasi($data,$U22);
            $V23 = $this->KalkulasiInterpolasi($data,$U23);

            $hot[20][18] = $this->FNum($V21);
            $hot[21][18] = $this->FNum($V22);
            $hot[22][18] = $this->FNum($V23);
            $koreksi = ($V21+$V22+$V23)/3;
            $hot[23][18] = $V24 = $this->FNum($koreksi);
        }

        // BACA THERMOMETER
        $W21 = $hot[20][15] = $hot[36][15];
        $W22 = $hot[21][15] = $hot[37][15];
        $W23 = $hot[22][15] = $hot[38][15];
        if( strlen($W21)>0 && strlen($W22)>0 && strlen($W23)>0)
        {
            $rataRata = ($W21+$W22+$W23)/3;
            $hot[23][15] = $W24 = $this->FNum($rataRata);

            $data = $this->ThermohygroGelas("temperatur");
            $X21 = $this->KalkulasiInterpolasi($data,$W21);
            $X22 = $this->KalkulasiInterpolasi($data,$W22);
            $X23 = $this->KalkulasiInterpolasi($data,$W23);

            $hot[20][22] = $this->FNum($X21);
            $hot[21][22] = $this->FNum($X22);
            $hot[22][22] = $this->FNum($X23);
            $koreksi = ($X21+$X22+$X23)/3;
            $hot[23][22] = $X24 = $this->FNum($koreksi);
        }

        // BACA HYGROMETER
        $Y21 = $hot[20][17] = $hot[36][17];
        $Y22 = $hot[21][17] = $hot[37][17];
        $Y23 = $hot[22][17] = $hot[38][17];
        if(strlen($Y21)>0 && strlen($Y22)>0 && strlen($Y23)>0)
        {
            $rataRata = ($Y21+$Y22+$Y23)/3;
            $hot[23][17] = $Y24 = $this->FNum($rataRata);

            $data = $this->ThermohygroGelas("thermohygro");
            $Z21 = $this->KalkulasiInterpolasi($data,$Y21);
            $Z22 = $this->KalkulasiInterpolasi($data,$Y22);
            $Z23 = $this->KalkulasiInterpolasi($data,$Y23);

            $hot[20][26] = $this->FNum($Z21);
            $hot[21][26] = $this->FNum($Z22);
            $hot[22][26] = $this->FNum($Z23);
            $koreksi = ($Z21+$Z22+$Z23)/3;
            $hot[23][26] = $Z24 = $this->FNum($koreksi);
        }

        if($temperaturStat)
        {
            $J28 = 0.99997495*(1-(pow((($P21+$T21)+-3.983035),2)*(($P21+$T21)+301.797))/(522528.9*(($P21+$T21)+69.34881)));
            $J29 = 0.99997495*(1-(pow((($P22+$T22)+-3.983035),2)*(($P22+$T22)+301.797))/(522528.9*(($P22+$T22)+69.34881)));
            $J30 = 0.99997495*(1-(pow((($P23+$T23)+-3.983035),2)*(($P23+$T23)+301.797))/(522528.9*(($P23+$T23)+69.34881)));
            $hot[32][3] = $this->FNum($J28);
            $hot[33][3] = $this->FNum($J29);
            $hot[34][3] = $this->FNum($J30);

            $rataRata = ($J28+$J29+$J30)/3;
            $hot[35][3] = $J31 = $this->FNum($rataRata);

            $N28 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N29 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $N30 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*EXP(0.061*($W21+$X21)))/(($W21+$X21)+273.15)/1000;
            $RhoUdaraE19 = $hot[32][6] = $this->FNum($N28);
            $RhoUdaraE20 = $hot[33][6] = $this->FNum($N29);
            $RhoUdaraE21 = $hot[34][6] = $this->FNum($N30);

            $rataRata = ($N28+$N29+$N30)/3;
            $hot[35][6] = $N31 = $this->FNum($rataRata);

            $J13 = 7.95; // NOT INPUT DATA: ρ AT : 8
            $U11 = $hot[12][4];
            $P28 = (($F28)*pow(($J28-$N28),-1)*(1-($N28/$J13))*(1-($U11*(($P21+$T21)-20))));
            $P29 = (($F29)*pow(($J29-$N29),-1)*(1-($N29/$J13))*(1-($U11*(($P22+$T22)-20))));
            $P30 = (($F30)*pow(($J30-$N30),-1)*(1-($N30/$J13))*(1-($U11*(($P23+$T23)-20))));
            $hot[32][9] = $this->FNum($P28);
            $hot[33][9] = $this->FNum($P29);
            $hot[34][9] = $this->FNum($P30);

            $rataRata = ($P28+$P29+$P30)/3;
            $hot[35][9] = $P31 = $this->FNum($rataRata);

            $A28 = $hot[32][13]; //Volume titik uji
            $U28 = $P28-$A28;
            $U29 = $P29-$A28;
            $U30 = $P30-$A28;
            $hot[32][11] = $this->FNum($U28);
            $hot[33][11] = $this->FNum($U29);
            $hot[34][11] = $this->FNum($U30);
            $rataRata = ($U28+$U29+$U30)/3;
            $hot[35][11] = $U31 = $this->FNum($rataRata);

            // UNCERTAINTY
            // 1. Repeatability pengukuran Vol pd Meniscus
            $uncert["K5"] = (max([$P28,$P29,$P30])-min([$P28,$P29,$P30]))/(2*sqrt(3));
            $uncert["Q5"] = 200;
            $uncert["S5"] = 1;
            $uncert["U5"] = $uncert["K5"]*$uncert["S5"];
            $uncert["W5"] = pow($uncert["U5"],2);
            $uncert["Y5"] = pow($uncert["U5"],4)/$uncert["Q5"];

            // 2. Massa
            $M10 = $inputStandard["dayaBaca"];
            $M14 = $inputStandard["maxU95"]; //0.3
            // dd($M10,$M14);
            // a. ketidakpastian akibat penunjukkan timbangan dari sertifikat
            $uncert["K7"] = sqrt((pow($M14/2,2))+pow($M10/sqrt(3),2));
            // b. ketidakpastian akibat massa isi
            $uncert["K8"] = sqrt(pow($M14/2,2)+pow($M10/sqrt(3),2));
            // c. ketidakpastian akibat drift dari timbangan
            $max = $inputStandard["maxU95"];
            $uncert["K9"] = 0.1*($max)/2;
            $data = $this->imbuhGelas("kp");
            $imbuhkp1 = $data[$inputData[20][3]];
            $imbuhkp2 = $data[$inputData[21][3]];
            $imbuhkp3 = $data[$inputData[22][3]];
            $imbuhkp4 = $data[$inputData[20][7]];
            $imbuhkp5 = $data[$inputData[21][7]];
            $imbuhkp6 = $data[$inputData[22][7]];

            $uncert["K10"] = max([$imbuhkp1,$imbuhkp2,$imbuhkp3,$imbuhkp4,$imbuhkp5,$imbuhkp6])/2;
            $uncert["N6"] = pow(pow($uncert["K7"],2)+pow($uncert["K8"],2)+pow($uncert["K9"],2),0.5);
            $uncert["Q6"] = 200;
            $uncert["S6"] = (1/($J31-$N31))*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U6"] = $uncert["N6"]*$uncert["S6"];
            $uncert["W6"] = pow($uncert["U6"],2);
            $uncert["Y6"] = pow($uncert["U6"],4)/$uncert["Q6"];

            // 3. Temperatur (Air dan Instrumen)
            // A. Ketidakpastian akibat Instrumen
            $uncert["K11"] = (ABS($W24+$X24)-($P24+$T24))/SQRT(12);
            // a. ketidakpastian thermometer standar dari sertifikat thermometer
            $J16 = $inputStandardThermometer["maxU95"];
            $uncert["K13"] = $J16/2;
            // b. ketidakpastian akibat drift dari thermometer
            $max = $this->Drift("max");
            $min = $this->Drift();
            $uncert["K14"] = ($max-$min)/sqrt(12);
            // c. ketidakpastian akibat resolusi thermometer standar
            $dayaBaca = $inputStandardThermometer["dayaBaca"];
            $uncert["K15"] = $dayaBaca/sqrt(12);
            // d. ketidakpastian akibat variansi temperatur saat pengujian
            $intP21 = $P21+$T21;
            $intP22 = $P22+$T22;
            $intP23 = $P23+$T23;
            $intR21 = $R21+$T21;
            $intR22 = $R22+$T22;
            $intR23 = $R23+$T23;
            $max = max([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $min = min([$intP21,$intP22,$intP23,$intR21,$intR22,$intR23]);
            $uncert["K16"] = ($max-$min)/sqrt(12);

            // B. Ketidakpastian akibat temp air
            // $uncert["K12"] = sqrt(pow($uncert["K13"],2)+pow($uncert["K14"],2)+pow($uncert["K15"],2)+pow($uncert["K16"],2));
            $uncert["K12"] = sqrt(pow($uncert["K13"],2)+pow($uncert["K14"],2)+pow($uncert["K15"],2)+pow($uncert["K16"],2));
            $uncert["N10"] = SQRT(pow($uncert["K11"],2)+pow($uncert["K12"],2));
            $uncert["Q10"] = 60;
            $uncert["S10"] = $F31*(1/($J31-$N31))*(1-($N31/$J13))*(-$U11);
            $uncert["U10"] = $uncert["N10"]*$uncert["S10"];
            $uncert["W10"] = pow($uncert["U10"],2);
            $uncert["Y10"] = pow($uncert["U10"],4)/$uncert["Q10"];

            // 4. Densitas Air
            // a. ketidakpastian dari persamaan tanaka
            $uncert["K18"] = 4.5*pow(10,-7);
            // b. ketidakpastian akibat temperatur air
            $rhoAirC3 = ((-0.1176*pow(($P24+$T24),2))+(15.846*($P24+$T24))-62.677)*pow(10,-6);
            $uncert["K19"] = $uncert["K12"]*$rhoAirC3*$J31;
            // c. ketidakpastian akibat kemurnian air
            $uncert["K20"] = 5*pow(10,-6);
            $uncert["N18"] = sqrt(pow($uncert["K18"],2)+pow($uncert["K19"],2)+pow($uncert["K20"],2));
            $uncert["Q17"] = 60;
            $uncert["S17"] = (-$F31)*pow((1/($J31-$N31)),2)*(1-($N31/$J13))*(1-($U11*((($P24+$T24)/2)-20)));
            $uncert["U17"] = $uncert["N18"]*$uncert["S17"];
            $uncert["W17"] = pow($uncert["U17"],2);
            $uncert["Y17"] = pow($uncert["U17"],4)/$uncert["Q17"];

            // 5. Densitas Udara
            $rhoUdaraD19 = ((0.34848*($U21+$V21))-(0.009*($Y21+$Z21))*exp(0.061*($W21+$X21)))/(($W21+$X21)+273.15);
            $rhoUdaraD20 = ((0.34848*($U22+$V22))-(0.009*($Y22+$Z22))*exp(0.061*($W22+$X22)))/(($W22+$X22)+273.15);
            $rhoUdaraD21 = ((0.34848*($U23+$V23))-(0.009*($Y23+$Z23))*exp(0.061*($W23+$X23)))/(($W23+$X23)+273.15);
            $rataRata = ($rhoUdaraD19+$rhoUdaraD20+$rhoUdaraD21)/3;
            $rhoUdaraE23 = $rataRata/1000;

            $rhoUdaraE27 = 2.4*pow(10,-4);
            $rhoUdaraG27 = 1;
            $rhoUdaraI27 = $rhoUdaraE27*$rhoUdaraG27;
            $rhoUdaraJ27 = pow($rhoUdaraI27,2);

            $max = $this->BarometerGelas("maxU95");
            $rhoUdaraE28 = $max*100/2;
            $rhoUdaraG28 = 1*pow(10,-5);
            $rhoUdaraI28 = $rhoUdaraE28*$rhoUdaraG28;
            $rhoUdaraJ28 = pow($rhoUdaraI28,2);

            $dataMax = $this->ThermohygroGelas("uncertaintyCelcius");
            $rhoUdaraE29 = $dataMax/2;
            $rhoUdaraG29 = -4*pow(10,-3);
            $rhoUdaraI29 = $rhoUdaraE29*$rhoUdaraG29;
            $rhoUdaraJ29 = pow($rhoUdaraI29,2);

            $dataMax = $this->ThermohygroGelas("uncertaintyPersen");
            $rhoUdaraE30 = $dataMax/(2*100);
            $rhoUdaraG30 = -9*pow(10,-3);
            $rhoUdaraI30 = $rhoUdaraE30*$rhoUdaraG30;
            $rhoUdaraJ30 = pow($rhoUdaraI30,2);
            $rhoUdaraJ32 = $rhoUdaraE23*sqrt($rhoUdaraJ27+$rhoUdaraJ28+$rhoUdaraJ29+$rhoUdaraJ30);

            $uncert["K21"] = $rhoUdaraJ32;
            $uncert["Q21"] = 60;
            $uncert["S21"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*((1-($N31/$J13))*(1/($J31-$N31))-(1/$J13));
            $uncert["U21"] = $uncert["K21"]*$uncert["S21"];
            $uncert["W21"] = pow($uncert["U21"],2);
            $uncert["Y21"] = pow($uncert["U21"],4)/$uncert["Q21"];

            // 6. Densitas Anak Timbangan
            $uncert["K22"] = 0.14/SQRT(3); //sqrt($uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]+$uncert["AL23"]);
            $uncert["Q22"] = 60;
            $uncert["S22"] = ($F31)*(1/($J31-$N31))*(1-($U11*($P24-20)))*($N31/pow($J13,2));
            $uncert["U22"] = $uncert["K22"]*$uncert["S22"];
            $uncert["W22"] = pow($uncert["U22"],2);
            $uncert["Y22"] = pow($uncert["U22"],4)/$uncert["Q22"];

            // 7. Koefisien Muai Ruang
            $uncert["K23"] = (0.1*$U11)/sqrt(3);
            $uncert["Q23"] = 60;
            $uncert["S23"] = ($F31)*(1/($J31-$N31))*(1-($N31/$J13))*(-(($P24)+($T24)-20));
            $uncert["U23"] = $uncert["K23"]*$uncert["S23"];
            $uncert["W23"] = pow($uncert["U23"],2);
            $uncert["Y23"] = pow($uncert["U23"],4)/$uncert["Q23"];

            // 8. Pembacaan Meniskus
            $C25 = $hot[32][13];
            // Pembagian skala 10
            // $F26 = $hot[13][10];
            // $F26 = 1;//$hot[19][1];
            $F26 = 40000;//$hot[19][1];
            $uncert["K24"] = ($C25)/(sqrt(3)*$F26);
            // $uncert["K24"] = (100)/(40000*SQRT(3));
            $uncert["Q24"] = 200;
            $uncert["S24"] = 1;
            $uncert["U24"] = $uncert["K24"]*$uncert["S24"];
            $uncert["W24"] = pow($uncert["U24"],2);
            $uncert["Y24"] = pow($uncert["U24"],4)/$uncert["Q24"];

            $uncert["U25"] = $uncert["U5"]+$uncert["U6"]+$uncert["U10"]+$uncert["U17"]+$uncert["U21"]+$uncert["U22"]+$uncert["U23"]+$uncert["U24"];
            $uncert["W25"] = $uncert["W5"]+$uncert["W6"]+$uncert["W10"]+$uncert["W17"]+$uncert["W21"]+$uncert["W22"]+$uncert["W23"]+$uncert["W24"];
            $uncert["Y25"] = $uncert["Y5"]+$uncert["Y6"]+$uncert["Y10"]+$uncert["Y17"]+$uncert["Y21"]+$uncert["Y22"]+$uncert["Y23"]+$uncert["Y24"];

            $uncert["U27"] = pow($uncert["W25"],0.5);
            $uncert["U28"] = pow($uncert["U27"],4)/$uncert["Y25"];

            $df = intval($uncert["U28"])>200?200:intval($uncert["U28"]);
            $TINV = MasterStudentTable::where("pr","0.05")
            ->where("k",2)
            ->where("df",$df)->first();
            $uncert["U29"] = $TINV->value;
            // $uncert["U29"] = 1.96; // TINV(0.05;U28); FORMULA ?
            // =U30*U28
            $uncert["U30"] = ($uncert["U29"]*$uncert["U27"]);
            $decimal = explode(".",$uncert["U30"]);
            $arrs = str_split($decimal[1]);
            $i=0;
            foreach($arrs as $arr)
            {
                $i++;
                if($arr!=="0") break;
            }
            $uncert["U31"] = $this->FNum($uncert["U30"],$i+1);
            $uncert["AngkaPenting"] = $i+1;

            $W28 = $uncert["U28"];
            $X28 = $uncert["U29"];
            $Y28 = $uncert["U31"];
            $hot[32][13] = $this->FNum($W28);
            $hot[32][15] = $this->FNum($X28);
            $hot[32][17] = $this->FNum($Y28);

            // UNCERTAINTY END

            $X34 = $P31;
            $hot[39][22] = $this->FNum($X34);
            $W33 = 28; //$hot[38][20];
            $X33 = 20; //$hot[38][22];
            $Y33 = 15.6; //$hot[38][24];
            $Z33 = 15; //$hot[38][26];
            $W34 = $X34*(1+($U11*($W33-$X33)));
            $Y34 = $X34*(1+($U11*($Y33-$X33)));
            $Z34 = $X34*(1+($U11*($Z33-$X33)));

            $hot[39][20] = $this->FNum($W34);
            $hot[39][24] = $this->FNum($Y34);
            $hot[39][26] = $this->FNum($Z34);

            $hot[41][19] = $X28;
            $hot[41][20] = $W36 = $Y28;
            $V15 = $hot[13][10];
            $hot[42][20] = $W37 = $V15;
        }

        ServiceOrders::whereId($id)->update([
            "cerapantiga"=>json_encode($hot),
            "uncertaintytiga"=>json_encode($uncert)
        ]);

        return view('service.cerapangelastiga',compact('hot','row','inputData',"inputStandard","inputStandardThermometer"));
    }

    public function result($id)
    {
        $attribute = $this->setup("service");

        $serviceOrders = ServiceOrders::where("service_request_item_id",$id)
        // SEMENTARA
        // ->where("stat_service_order",0)
        // ->where("is_finish",0)
        ->first();
        // SEMENTARA
        // if(!$serviceOrders) abort("404");
        // dd($serviceOrders);
        $tmp_report_id = $serviceOrders->tmp_report_id?$serviceOrders->tmp_report_id:0;
        $hasil_uji = $serviceOrders->hasil_uji?$serviceOrders->hasil_uji:"[]";

        $mMetodeUji = MasterMetodeUji::orderBy('id')->pluck('nama_metode', 'id');

        $template_format_item = MasterTemplate::where("laboratory_id",Auth::user()->laboratory_id)
        ->pluck('title_tmp', 'id');

        $template_rows = MasterTemplate::where("laboratory_id",Auth::user()->laboratory_id)
        ->select("header_tmp","column_tmp","id")->get();

        $template_header = $template_setting = array();
        foreach ($template_rows as $row)
        {
            if($row->header_tmp!=NULL) $template_header[$row->id] = json_decode($row->header_tmp,true);
            if($row->column_tmp!=NULL) $template_setting[$row->id] = json_decode($row->column_tmp,true);
        }

        $template_header = json_encode($template_header);
        $template_setting = json_encode($template_setting);

        return view('service.resultvolumetri',compact(
            'mMetodeUji',
            'hasil_uji',
            'tmp_report_id',
            'serviceOrders',
            'template_format_item',
            'template_header',
            'template_setting',
            'attribute'
        ));
    }

    public function finish($id)
    {
        $row = ServiceOrders::whereId($id)
        ->whereNotNull("hasil_uji")
        ->whereNotNull("tmp_report_id")->first();

        if($row)
        {
            ServiceOrders::whereId($id)
            ->update([
                "stat_service_order"=>1,
                "lab_staff_out"=>Auth::id(),
                "staff_entry_dateout"=>date("Y-m-d")
            ]);

            ServiceRequest::whereId($row->service_request_id)
            ->update([
                "stat_service_request"=>3
            ]);

            ServiceRequestItem::whereId($row->service_request_item_id)
            ->update([
                "stat_service_request_item"=>2
            ]);
        }
        return Redirect::route('service');
    }

    public function warehouse($id)
    {
        ServiceOrders::whereId($id)->update(["is_finish"=>1]);
        return Redirect::route('service');
    }

    public function history()
    {
        $attribute = $this->setup();

        $rows = ServiceOrders::where("is_finish",1)
        ->orderBy('staff_entry_dateout','desc')
        ->get();

        return view('service.history',compact('rows','attribute'));
    }

    private function FNum($num,$dec=4){ return number_format($num,$dec,".",""); }

    // public function KalkulasiInterpolasi($data,$input)
    // {
    //     $prev_key = null;
    //     $next_key = null;

    //     foreach (array_keys($data) as $key) {
    //         // convert to timestamp for comparison
    //         if ($key < $input) {
    //             $prev_key = $key;
    //         } else {
    //             //  already found next key, move on
    //             if ($next_key == null) {
    //                 $next_key = $key;
    //             }
    //         }
    //     }

    //     $prev_value = $data[$prev_key];
    //     $next_value = $data[$next_key];

    //     $temp = ((($input) - ($prev_key)) * ($next_value - $prev_value) / (($next_key) - ($prev_key)));
    //     $y = $temp + $prev_value;

    //     return $y;
    // }

    private function TimbanganElektronik($mode="koreksi")
    {
        $merk =  "Mettler Toledo";
        $model = "BBK 422-35 LA";
        $seri = "2876247";
        $kapasitas = 35100;
        $dayaBaca = 0.1;
        $kalibrasi = "Feb 2021";
        $kelas = "I";

        // Timbangan Elektronik
        // Pembacaan  (g)	Koreksi (g)	U95 (g)
        $pembacaan = [
            [10,-0.05,0.30],
            [50,-0.05,0.30],
            [500,-0.05,0.30],
            [1000,-0.05,0.30],
            [5000,-0.15,0.30],
            [10000,-0.20,0.30],
            [20000,-0.40,0.30],
            [25000,-0.50,0.30],
            [30000,-0.70,0.30],
            [35000,-0.90,0.30],
        ];

        if($mode=="dayaBaca")
        {
            return $dayaBaca;
        }
        if($mode=="maxU95")
        {
            return max([0.30,0.30,0.30,0.30,0.30,0.30,0.30,0.30,0.30,0.30]);
        }

        return [
            "10"=>-0.05,
            "50"=>-0.05,
            "500"=>-0.05,
            "1000"=>-0.05,
            "5000"=>-0.15,
            "10000"=>-0.20,
            "20000"=>-0.40,
            "25000"=>-0.50,
            "30000"=>-0.70,
            "35000"=>-0.90,
        ];
    }

    private function TimbanganElektronikGelas($mode="koreksi")
    {
        if($mode==="standard")
        {
            return [
            "merk"=>"Mettler Toledo",
            "model"=>"PG 6002-S",
            "seri"=>"1121273179",
            "kapasitas"=>6100,
            "dayaBaca"=>0.01,
            "kalibrasi"=>"Feb 2021",
            "kelas"=>"II",
            ];
        }

        $dayaBaca = 0.01;

        // Timbangan Elektronik
        // Pembacaan  (g)	Koreksi (g)	U95 (g)
        $pembacaan = [
            [1,-0.015,0.040],
            [10,-0.015,0.040],
            [100,-0.025,0.040],
            [500,-0.010,0.040],
            [1000,-0.010,0.040],
            [2000,0.000,0.040],
            [3000,0.005,0.040],
            [4000,0.005,0.040],
            [5000,-0.010,0.040],
            [6100,0.000,0.040],
        ];

        if($mode=="dayaBaca")
        {
            return $dayaBaca;
        }
        if($mode=="maxU95")
        {
            return max([0.040,0.040,0.040,0.040,0.040,0.040,0.040,0.040,0.040,0.040]);
        }

        return [
            "1"=>-0.015,
            "10"=>-0.015,
            "100"=>-0.025,
            "500"=>-0.010,
            "1000"=>-0.010,
            "2000"=>0.000,
            "3000"=>0.005,
            "4000"=>0.005,
            "5000"=>-0.010,
            "6100"=>0.000,
        ];
    }

    private function TimbanganElektronikLabu($mode="koreksi")
    {
        if($mode==="standard")
        {
            return [
                "merk"=>  "Mettler Toledo/-",
                "model"=> "PG 6002-S",
                "seri"=> "1121273179",
                "kapasitas"=> 6100,
                "dayaBaca"=> 0.01,
                "kalibrasi"=> "Feb 2021",
                "kelas"=> "II",
            ];
        }

        // Timbangan Elektronik
        // Pembacaan  (g)	Koreksi (g)	U95 (g)
        $pembacaan = [
            [1,-0.015,0.040],
            [10,-0.015,0.040],
            [100,-0.025,0.040],
            [500,-0.010,0.040],
            [1000,-0.010,0.040],
            [2000,0.000,0.040],
            [3000,0.005,0.040],
            [4000,0.005,0.040],
            [5000,-0.010,0.040],
            [6100,0.000,0.040],
        ];

        if($mode=="maxU95")
        {
            return max([0.040,0.040,0.040,0.040,0.040,0.040,0.040,0.040,0.040,0.040]);
        }

        return [
            "1"=>-0.015,
            "10"=>-0.015,
            "100"=>-0.025,
            "500"=>-0.010,
            "1000"=>-0.010,
            "2000"=>0.000,
            "3000"=>0.005,
            "4000"=>0.005,
            "5000"=>-0.010,
            "6100"=>0.000,
        ];
    }

    private function TimbanganDigital($mode="abc")
    {
        $merek = "Fluke";
        $model = "5211/K/";
        $seri = "22450303";
        $dayaBaca = 0.1;
        $kalibrasi = "Feb 2021";

        // Thermometer Digital sensor 1
        // Pembacaan (°C)	Koreksi (°C)	U95 (°C)

        $pembacaan = [
            [0.6,-0.63,0.13],
            [10.3,-0.32,0.12],
            [15.2,-0.22,0.12],
            [20.0,-0.04,0.13],
            [25.0,0.04,0.13],
            [29.8,0.20,0.12],
            [44.6,0.41,0.12],
            [44.6,0.41,0.12],
        ];

        if($mode==="dayaBaca")
        {
            return $dayaBaca;
        }

        if($mode=="maxU95")
        {
            return max([0.13,0.13,0.12,0.12,0.12,0.1]);
        }

        return [
            "0.6"=>-0.63,
            "10.3"=>-0.32,
            "15.2"=>-0.22,
            "20.0"=>-0.04,
            "25.0"=>0.04,
            "29.8"=>0.20,
            "44.6"=>0.41,
            "44.6"=>0.41,
        ];
    }

    private function TimbanganDigitalLabu($mode="abc")
    {
        if($mode==="standard")
        {
            return [
            "merek"=>"Fluke",
            "model"=>"5211/K/",
            "seri"=>"22450303",
            "dayaBaca"=>0.1,
            "kalibrasi"=>"Feb 2021",
            ];
        }
            
        $pembacaan = [
            [10.4,-0.40,0.13],
            [15.3,-0.20,0.13],
            [20.3,-0.22,0.12],
            [25.0,0.04,0.12],
            [30.0,0.07,0.12],
            [44.6,0.45,0.12],
        ];

        if($mode=="maxU95")
        {
            return max([0.13,0.13,0.12,0.12,0.12,0.12]);
        }

        return [
            "10.4"=>-0.40,
            "15.3"=>-0.20,
            "20.3"=>-0.22,
            "25.0"=>0.04,
            "30.0"=>0.07,
            "44.6"=>0.45,
        ];
    }

    private function TimbanganDigitalGelas($mode="abc")
    {
        if($mode==="standard")
        {
            return [
            "merek"=>"Fluke",
            "model"=>"52 II",
            "seri"=>"22450303 / T1",
            "dayaBaca"=>0.1,
            "kalibrasi"=>"Feb 2021",
            ];
        }
        $dayaBaca = 0.1;

        // Thermometer Digital sensor 1
        // Pembacaan (°C)	Koreksi (°C)	U95 (°C)

        $pembacaan = [
            [10.4,-0.40,0.13],
            [15.3,-0.20,0.13],
            [20.3,-0.22,0.12],
            [25.0,0.04,0.12],
            [30.0,0.07,0.12],
            [44.6,0.45,0.12],
        ];

        if($mode==="dayaBaca")
        {
            return $dayaBaca;
        }

        if($mode=="maxU95")
        {
            return max([0.13,0.13,0.12,0.12,0.12,0.12]);
        }

        return [
            "10.4"=>-0.40,
            "15.3"=>-0.20,
            "20.3"=>-0.22,
            "25.0"=>0.04,
            "30.0"=>0.07,
            "44.6"=>0.45,
        ];
    }

    private function Barometer($mode="abc")
    {
        $merek = "LUTRON";
        $model = "MHB-382SD";
        $seri = "AI.35563";
        $daya_baca = "-/ 0.1";
        $kalibrasi = "14 Agustus 2020";

        // Barometer
        // pembacaan(hPa)	Actual (hPa)	Koreksi (hPa)	U95hPa
        $pembacaan = [
            [900,898.7,-1.30,0.3],
            [920,918.7,-1.30,0.3],
            [940,938.6,-1.40,0.3],
            [960,958.6,-1.40,0.3],
            [980,978.6,-1.40,0.3],
            [990,988.5,-1.50,0.3],
            [1000,998.5,-1.50,0.3],
            [1010,1008.5,-1.50,0.3],
            [1020,1018.5,-1.50,0.3],
            [1030,1028.4,-1.60,0.3],
        ];

        if($mode==="uncertainty")
        {
            return [
                "900"=>0.3,
                "920"=>0.3,
                "940"=>0.3,
                "960"=>0.3,
                "980"=>0.3,
                "990"=>0.3,
                "1000"=>0.3,
                "1010"=>0.3,
                "1020"=>0.3,
                "1030"=>0.3,
            ];
        }
        else if($mode==="tekananMax")
        {
            return max([0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3]);
        }

        return [
            "900"=>-1.30,
            "920"=>-1.30,
            "940"=>-1.40,
            "960"=>-1.40,
            "980"=>-1.40,
            "990"=>-1.50,
            "1000"=>-1.50,
            "1010"=>-1.50,
            "1020"=>-1.50,
            "1030"=>-1.60,
        ];
    }

    private function BarometerLabu($mode="abc")
    {
        $merek = "LUTRON";
        $model = "MHB-382SD";
        $seri = "AI.35563";
        $daya_baca = "-/ 0.1";
        $kalibrasi = "14 Agustus 2020";

        // Barometer
        // pembacaan(hPa)	Actual (hPa)	Koreksi (hPa)	U95hPa
        $pembacaan = [
            [900,898.7,-1.30,0.3],
            [920,918.7,-1.30,0.3],
            [940,938.6,-1.40,0.3],
            [960,958.6,-1.40,0.3],
            [980,978.6,-1.40,0.3],
            [990,988.5,-1.50,0.3],
            [1000,998.5,-1.50,0.3],
            [1010,1008.5,-1.50,0.3],
            [1020,1018.5,-1.50,0.3],
            [1030,1028.4,-1.60,0.3],
        ];

        if($mode==="uncertainty")
        {
            return [
                "900"=>0.3,
                "920"=>0.3,
                "940"=>0.3,
                "960"=>0.3,
                "980"=>0.3,
                "990"=>0.3,
                "1000"=>0.3,
                "1010"=>0.3,
                "1020"=>0.3,
                "1030"=>0.3,
            ];
        }
        else if($mode==="tekananMax")
        {
            return max([0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3]);
        }

        return [
            "900"=>-1.30,
            "920"=>-1.30,
            "940"=>-1.40,
            "960"=>-1.40,
            "980"=>-1.40,
            "990"=>-1.50,
            "1000"=>-1.50,
            "1010"=>-1.50,
            "1020"=>-1.50,
            "1030"=>-1.60,
        ];
    }

    private function BarometerGelas($mode="abc")
    {
        $merek = "LUTRON/Taiwan";
        $model = "MHB-382SD";
        $seri = "AI.35558";
        $dayaBaca = "900-1040hPa";
        $kalibrasi = "14 Agustus 2020";

        // Barometer
        // pembacaan(hPa)	Actual (hPa)	Koreksi (hPa)	U95hPa
        $pembacaan = [
            [900,898.7,-1.30,0.3],
            [920,918.7,-1.30,0.3],
            [940,938.6,-1.40,0.3],
            [960,958.6,-1.40,0.3],
            [980,978.6,-1.40,0.3],
            [990,988.5,-1.50,0.3],
            [1000,998.5,-1.50,0.3],
            [1010,1008.5,-1.50,0.3],
            [1020,1018.5,-1.50,0.3],
            [1030,1028.4,-1.60,0.3],
        ];

        if($mode==="uncertainty")
        {
            return [
                "900"=>0.3,
                "920"=>0.3,
                "940"=>0.3,
                "960"=>0.3,
                "980"=>0.3,
                "990"=>0.3,
                "1000"=>0.3,
                "1010"=>0.3,
                "1020"=>0.3,
                "1030"=>0.3,
            ];
        }
        else if($mode==="maxU95")
        {
            return max([0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3]);
        }

        return [
            "900"=>-1.30,
            "920"=>-1.30,
            "940"=>-1.40,
            "960"=>-1.40,
            "980"=>-1.40,
            "990"=>-1.50,
            "1000"=>-1.50,
            "1010"=>-1.50,
            "1020"=>-1.50,
            "1030"=>-1.60,
        ];
    }

    private function ThermometerDigitalLabu($mode="abc")
    {
        $merk = "Fluke";
        $model = "5211/K/";
        $seri = "22450303";
        $daya_baca = 0.1;
        $kalibrasi = "Feb 2021";

        // Thermometer Digital sensor 1
        // Pembacaan (°C)	Koreksi (°C)	U95 (°C)
        $pembacaan = [
            [10.4,-0.40,0.13],
            [15.3,-0.20,0.13],
            [20.3,-0.22,0.12],
            [25.0,0.04,0.12],
            [30.0,0.07,0.12],
            [44.6,0.45,0.12],
        ];

        if($mode=="koreksi")
        {
            return [
                "10.4"=>-0.40,
                "15.3"=>-0.20,
                "20.3"=>-0.22,
                "25.0"=>0.04,
                "30.0"=>0.07,
                "44.6"=>0.45,
            ];
        }

        return [
            "10.4"=>0.13,
            "15.3"=>0.13,
            "20.3"=>0.12,
            "25.0"=>0.12,
            "30.0"=>0.12,
            "44.6"=>0.12,
        ];
    }

    private function Thermometer($mode="abc")
    {
        if($mode=="uncertainty")
        {
            return [
                "0.0"=>0.00,
                "0.0"=>0.00,
                "0.0"=>0.00,
                "18.3"=>0.87,
                "25.1"=>1.10,
                "34.8"=>0.87,
            ];
        }

        // Pembacaan (%)	Koreksi (%)
        return [
            "0.0"=>0.000,
            "0.0"=>0.000,
            "0.0"=>0.000,
            "18.3"=>0.01,
            "25.1"=>-0.2,
            "34.8"=>0.97,
        ];
    }

    private function ThermohygroLabu($mode="abc")
    {
        if($mode==="temperatur")
        {
            return [
                "18.5"=>-0.12,
                "25.9"=>-0.31,
                "35.0"=>-0.17,
            ];
        }

        if($mode==="uncertaintyTemperatur")
        {
            return [
                "18.5"=>0.87,
                "25.9"=>0.87,
                "35.0"=>0.87,
            ];
        }

        if($mode==="uncertaintyThermohygro")
        {
            return [
                "52.70"=>3.60,
                "58.40"=>3.60,
                "69.10"=>3.60,
            ];
        }

        if($mode==="thermohygro")
        {
            return [
                "52.70"=>-3.20,
                "58.40"=>-2.40,
                "69.10"=>-0.40,
            ];
        }

        if($mode==="uncertaintyCelcius")
        {
            return max([0.87,0.87,0.87]);
        }

        if($mode==="uncertaintyPersen")
        {
            return max([3.60,3.60,3.60]);
        }

    }

    private function Hygrometer($mode="abc")
    {
        // Pembacaan (%)	Koreksi (%)
        if($mode=="uncertainty")
        {
            return [
                "0.00"=>-0.00,
                "0.00"=>0.00,
                "0.00"=>0.00,
                "48.80"=>-3.60,
                "54.80"=>3.60,
                "67.00"=>3.60,
                "67.00"=>3.60,
            ];
        }

        return [
            "0.00"=>-0.10,
            "0.00"=>0.60,
            "0.00"=>1.40,
            "48.80"=>-0.10,
            "54.80"=>0.60,
            "67.00"=>1.40,
            "67.00"=>1.40,
        ];
    }

    private function HygrometerLabu($mode="abc")
    {
        // Pembacaan (%)	Koreksi (%)

        if($mode=="uncertainty")
        {
            return [
                "52.70"=>3.60,
                "58.40"=>3.60,
                "69.10"=>3.60,
                "0.00"=>0.00,
                "0.00"=>0.00,
                "0.00"=>0.00,
                "0.00"=>0.00,
            ];
        }

        return [
            "52.70"=>-3.20,
            "58.40"=>-2.40,
            "69.10"=>-0.40,
        ];
    }

    private function Thermohygro($mode="abc")
    {
        if($mode=="persen")
        {
            return [
                3.60,
                3.60,
                3.60,
            ];
        }

        // Ketidakpastian (oC)
        return [
            0.87,
            1.10,
            0.87,
        ];
    }

    private function ThermohygroGelas($mode)
    {
        // 18.5	-0.12	52.70	-3.2	0.87	3.60
        // 25.9	-0.31	58.40	-2.4	0.87	3.60
        // 35.0	-0.17	69.10	-0.4	0.87	3.60

        if($mode==="temperatur")
        {
            return [
                "18.5"=>-0.12,
                "25.9"=>-0.31,
                "35.0"=>-0.17,
            ];
        }

        if($mode==="uncertaintyTemperatur")
        {
            return [
                "18.5"=>0.87,
                "25.9"=>0.87,
                "35.0"=>0.87,
            ];
        }

        if($mode==="thermohygro")
        {
            return [
                "52.70"=>-3.2,
                "58.40"=>-2.4,
                "69.10"=>-0.4,
            ];
        }

        if($mode==="uncertaintyThermohygro")
        {
            return [
                "52.70"=>3.6,
                "58.40"=>3.6,
                "69.10"=>3.6,
            ];
        }

        if($mode==="uncertaintyCelcius")
        {
            return max([0.87,0.87,0.87]);
        }

        if($mode==="uncertaintyPersen")
        {
            return max([3.60,3.60,3.60]);
        }
    }

    private function Drift($mode="min")
    {
        $a1 = -0.1; // Koreksi 25 - 2017
        $a2 = -0.02; // Koreksi 25 - 2019
        $a3 = -0.05; // Koreksi 25 - 2020
        $a4 = 0.04; // Koreksi 25 - 2021
        $a5 = 0; // Koreksi 25 kg-2020
        $a6 = -0.04; // desember 2021 maintenance
        $a7 = 0.02; // Koreksi 25 kg-2021

        if($mode=="max") return max([$a1,$a2,$a3,$a4,$a5,$a6,$a7]);
        return min([$a1,$a2,$a3,$a4,$a5,$a6,$a7]);
    }

    private function DriftLabu()
    {
        $a1 = -0.1; //Koreksi 25 - 2017
        $a2 = -0.02; //Koreksi 25 - 2019
        $a3 = -0.05; //Koreksi 25 - 2020
        $a4 = 0.04; //Koreksi 25 - 2021

        return (max([$a1,$a2,$a3,$a4])-min([$a1,$a2,$a3,$a4]))/sqrt(12);
    }

    private function Labu($data)
    {
        $suhuTerkoreksi = 18.088;

        // Labu	Suhu terkoreksi	18.088
        // V20 Labu		Vuji

        $labu =
        [
            [49.785,"50 mL ID 1",49.78405763],
            [99.805,"100 mL ID 2",99.80311081],
            [199.66,"200 mL ID 3",199.6562207],
            [249.514,"250 mL ID 4",249.509277],
            [499.529,"500 mL ID 5",499.5195445],
            [499.314,"500 mL ID 6",499.3045486],
            [499.19,"500 mL ID 7",499.1805509],
            [998.618,"1000 mL ID 8",998.5990974],
            [999.033,"1000 mL ID 9",999.0140895],
            [1998.3,"2000 mL ID 10",1998.262175],
            [1998.61,"2000 mL ID 11",1998.572169],
            [4996.41,"5000 mL ID 12",4996.315424],
            [4997.2,"5000 mL ID 13",4997.105409],
        ];

        foreach($labu as $k=>$v)
        {
            if(in_array($data,$labu)) return end($labu);
        }
        return 0;

    }

    private function LabuV20($data)
    {
        // Labu	Suhu terkoreksi	18.088
        // V20 Labu		Vuji
        $pembacaan = [
            [49.785, "50 mL ID 1", 49.78405763],
            [99.805, "100 mL ID 2", 99.80311081],
            [199.66, "200 mL ID 3", 199.6562207],
            [249.514, "250 mL ID 4", 249.509277],
            [499.529, "500 mL ID 5", 499.5195445],
            [499.314, "500 mL ID 6", 499.3045486],
            [499.19, "500 mL ID 7", 499.1805509],
            [998.618, "1000 mL ID 8", 998.5990974],
            [999.033, "1000 mL ID 9", 999.0140895],
            [1998.3, "2000 mL ID 10", 1998.262175],
            [1998.61, "2000 mL ID 11", 1998.572169],
            [4996.41, "5000 mL ID 12", 4996.315424],
            [4997.2, "5000 mL ID 13", 4997.105409],
        ];

        foreach($pembacaan as $k=>$v)
        {
            if(in_array($data,$v))
            {
                return end($v);
            }
        }

        return 0;
    }

    private function ImbuhLabu($mode="abc")
    {
        $data = [
        [0,0,0],
        [1,0.0010019,0.0000012],
        [2,0.0020021,0.0000012],
        [3,0.0030040,0.0000017],
        [4,0.0040040,0.0000017],
        [5,0.0050031,0.0000013],
        [6,0.0060050,0.0000018],
        [7,0.0070052,0.0000018],
        [8,0.0080071,0.0000021],
        [9,0.0090071,0.0000021],
        [10,0.0100026,0.0000014],
        [11,0.0110045,0.0000018],
        [12,0.0120047,0.0000018],
        [13,0.0130066,0.0000022],
        [14,0.0140066,0.0000022],
        [15,0.0150057,0.0000019],
        ];

        if($mode==="kp")
        {
            return [
                "0"=>0,
                "1"=>0.0000012,
                "2"=>0.0000012,
                "3"=>0.0000017,
                "4"=>0.0000017,
                "5"=>0.0000013,
                "6"=>0.0000018,
                "7"=>0.0000018,
                "8"=>0.0000021,
                "9"=>0.0000021,
                "10"=>0.0000014,
                "11"=>0.0000018,
                "12"=>0.0000018,
                "13"=>0.0000022,
                "14"=>0.0000022,
                "15"=>0.0000019,
            ];
        }

        return [
            "0"=>0,
            "1"=>0.0010019,
            "2"=>0.0020021,
            "3"=>0.0030040,
            "4"=>0.0040040,
            "5"=>0.0050031,
            "6"=>0.0060050,
            "7"=>0.0070052,
            "8"=>0.0080071,
            "9"=>0.0090071,
            "10"=>0.0100026,
            "11"=>0.0110045,
            "12"=>0.0120047,
            "13"=>0.0130066,
            "14"=>0.0140066,
            "15"=>0.0150057,
        ];
    }
    private function ImbuhGelas($mode="abc")
    {
        // Merek / buatan		: Sartorius /Germany
        // Model / tipe		: YCS011-652-02
        // No. seri		22429858
        // Kapasitas/daya baca		1 mg- 10 mg
        // Kalibrasi 		:  Maret 2021
        // Kelas		: E2
        // AT Imbuh

        // Nominal (g)	Massa Konv (g)	U95 (g)	Kombinasi	Nilai Kombinasi	KP
        $data = [
            [0,0,0],
            [1,0.0010019,0.0000012],
            [2,0.0020021,0.0000012],
            [3,0.0030040,0.0000017],
            [4,0.0040040,0.0000017],
            [5,0.0050031,0.0000013],
            [6,0.0060050,0.0000018],
            [7,0.0070052,0.0000018],
            [8,0.0080071,0.0000021],
            [9,0.0090071,0.0000021],
            [10,0.0100026,0.0000014],
            [11,0.0110045,0.0000018],
            [12,0.0120047,0.0000018],
            [13,0.0130066,0.0000022],
            [14,0.0140066,0.0000022],
            [15,0.0150057,0.0000019],
        ];

        if($mode==="kp")
        {
            return [
                "0"=>0,
                "1"=>0.0000012,
                "2"=>0.0000012,
                "3"=>0.0000017,
                "4"=>0.0000017,
                "5"=>0.0000013,
                "6"=>0.0000018,
                "7"=>0.0000018,
                "8"=>0.0000021,
                "9"=>0.0000021,
                "10"=>0.0000014,
                "11"=>0.0000018,
                "12"=>0.0000018,
                "13"=>0.0000022,
                "14"=>0.0000022,
                "15"=>0.0000019,
            ];
        }

        return [
            "0"=>0,
            "1"=>0.0010019,
            "2"=>0.0020021,
            "3"=>0.0030040,
            "4"=>0.0040040,
            "5"=>0.0050031,
            "6"=>0.0060050,
            "7"=>0.0070052,
            "8"=>0.0080071,
            "9"=>0.0090071,
            "10"=>0.0100026,
            "11"=>0.0110045,
            "12"=>0.0120047,
            "13"=>0.0130066,
            "14"=>0.0140066,
            "15"=>0.0150057,
        ];

    }

}
