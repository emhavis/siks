<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{

    private $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("home");
        if($attribute==null) return userlogout();

        if (Auth::user()->user_role=="8" ) {
            return Redirect::route('dashboardmonitoruttp');
        } else if (Auth::user()->user_role=="3" || Auth::user()->user_role=="9" || Auth::user()->user_role=="10") {
            return Redirect::route('dashboardmonitoruttp');
        } else if (Auth::user()->user_role=="15" || Auth::user()->user_role=="16" || Auth::user()->user_role=="22") {
            return Redirect::route('dashboardmonitoruut');
        } else if (Auth::user()->user_role=="14") {
            return Redirect::route('dashboardwarehouseuttp');
        } else if (Auth::user()->user_role=="26") {
            return Redirect::route('dashboardwarehouseuut');
        }

        return view('home',compact('attribute'));
    }
}
