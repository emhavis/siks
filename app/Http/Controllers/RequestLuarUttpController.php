<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItem;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpStaff;
use App\ServiceOrderUttps;
use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\MasterServiceType;

use App\MasterPetugasUttp;
use App\Customer;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

use App\Mail\Invoice;
use App\Mail\InvoiceTUHP;
use App\Mail\Receipt;
use App\Mail\ReceiptTUHP;
use Illuminate\Support\Facades\Mail;

use App\HistoryUttp;

use App\Jobs\ProcessEmailJob;

use PDF;

class RequestLuarUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();

        $this->ServiceRequestUttp = new ServiceRequestUttp();
        $this->ServiceRequestUttpItem = new ServiceRequestUttpItem();
        $this->ServiceRequestUttpItemInspection = new ServiceRequestUttpItemInspection();
        $this->ServiceOrderUttp = new ServiceOrderUttps();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("requestluaruttp");

        $petugas_id = Auth::user()->petugas_uttp_id;

        $rows_berangkat = ServiceRequestUttp::whereIn('status_id', [11, 15])
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->where('service_request_uttp_insitu_staff.scheduled_id', $petugas_id)
            ->whereNull('payment_status_id')
            ->select('service_request_uttps.*')
            ->get();

        $rows_selesai = ServiceRequestUttp::where('status_id', 12)
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->where('service_request_uttp_insitu_staff.scheduled_id', $petugas_id)
            ->select('service_request_uttps.*')
            ->get();
        
        /*
        $rows_penagihan = ServiceRequestUttp::whereIn('status_id', [5,15,18])
            ->where('lokasi_pengujian', 'luar')
            //->where('payment_status_id', 5)
            ->where(function($query)
                {
                    $query->where('payment_status_id', 5)
                    ->orWhereNull('payment_status_id');
                })
            ->get();

        $rows_validasi = ServiceRequestUttp::whereIn('status_id', [7,15,20])
            ->where('lokasi_pengujian', 'luar')
            //->where('payment_status_id', 7)
            ->where(function($query)
                {
                    $query->where('payment_status_id', 7)
                    ->orWhereNull('payment_status_id');
                })
            ->get();
        */

        $rows_penagihan = [];
        $rows_validasi = [];

        return view('requestluaruttp.index',compact([
            'rows_berangkat',
            'rows_selesai',
            'rows_penagihan',
            'rows_validasi',
            'attribute']));
    }

    public function proses($id)
    {
        $attribute = $this->MyProjects->setup("requestluaruttp");

        $request = ServiceRequestUttp::find($id);
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();

        return view('requestluaruttp.proses',compact(
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function saveproses($id, Request $request)
    {
        $serviceRequest = ServiceRequestUttp::find($id);

        $serviceRequest->status_id = 12;
        $serviceRequest->save();

        $items = ServiceRequestUttpItem::where("request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUttp();
            $history->request_status_id = 12;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = 12;
            $item->save();
        }

        /*
        if ($request->hasFile('spuh_file')) {
            $file = $request->file('spuh_file');
            $file_spuh = $file->getClientOriginalName();
            $path = $file->store(
                'spuh_dl',
                'public'
            );
            $path_spuh = $path;

            ServiceRequestUttpInsituDoc::where("id", $serviceRequest->spuh_doc_id)
            ->update([
                "spuh_file" => $file_spuh,
                "spuh_path" => $path_spuh,
            ]);
        }
        */

        return Redirect::route('requestluaruttp');

    }

    public function pending($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $request = ServiceRequestUttp::find($id);
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();

        return view('requestluaruttp.pending',compact(
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function savepending($id, Request $request)
    {
        $serviceRequest = ServiceRequestUttp::find($id);

        $serviceRequest->status_id = 15;
        $serviceRequest->pending_status = 1;
        $serviceRequest->pending_created = date('Y-m-d H:i:s');
        $serviceRequest->pending_notes = $request->get("pending_notes");
        $serviceRequest->save();

        $items = ServiceRequestUttpItem::where("request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUttp();
            $history->request_status_id = 15;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = 15;
            $item->save();
        }

        $response["id"] = $serviceRequest->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    public function continue($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $request = ServiceRequestUttp::find($id);
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();

        $users = MasterPetugasUttp::pluck('nama', 'id');

        return view('requestluaruttp.continue',compact(
            'request',
            'staffes',
            'users',
            'attribute'
        ));
    }

    public function savecontinue($id, Request $request)
    {
        $serviceRequest = ServiceRequestUttp::find($id);

        //$serviceRequest->status_id = 11;
        $serviceRequest->pending_status = 0;
        $serviceRequest->pending_ended = date('Y-m-d H:i:s');

        $serviceRequest->spuh_spt = $request->get('spuh_spt');
        $serviceRequest->spuh_staff = count($request->get('scheduled_id'));

        $serviceRequest->received_date = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $serviceRequest->estimated_date = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));

        $serviceRequest->scheduled_test_date_from = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $serviceRequest->scheduled_test_date_to = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));

        $dt1 = new \DateTime($serviceRequest->received_date);
        $dt2 = new \DateTime($serviceRequest->estimated_date);
        $interval = $dt1->diff($dt2);
        $total_price = ((int)$interval->format('%a') + 1) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        $serviceRequest->spuh_price = ((int)$interval->format('%a') + 1) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        $serviceRequest->save();

        $d1 = $request->get('scheduled_test_date_from');

        $last_doc = ServiceRequestUttpInsituDoc::where('request_id', $id)
            ->latest()
            ->first();
        
        $invoiced = null;
        $historyStatus = $serviceRequest->status_id;
        if ($serviceRequest->spuh_price > $last_doc->price) {
            //$serviceRequest->status_id = 6;
            $serviceRequest->spuh_inv_price = $serviceRequest->spuh_price - $last_doc->price;
            $invoiced = $serviceRequest->spuh_inv_price;
            $serviceRequest->spuh_payment_date = null;
            $serviceRequest->payment_status_id = 5;
        } else {
            $serviceRequest->status_id = 11;
        }

        $doc = ServiceRequestUttpInsituDoc::create([
            "request_id" => $id,
            "doc_no" => $request->get('spuh_spt'),
            "rate" => $serviceRequest->spuh_rate,
            "rate_id" => $serviceRequest->spuh_rate_id,
            "price" => $serviceRequest->spuh_price,
            "invoiced_price" => $invoiced ? $invoiced : null,
            "staffs" => implode(";",$request->get("scheduled_id")),
            "date_from" => $serviceRequest->received_date,
            "date_to" => $serviceRequest->estimated_date,
            "days" => ((int)$interval->format('%a') + 1),
        ]);

        ServiceRequestUttpStaff::where("request_id", $id)->delete();
        foreach($request->get('scheduled_id') as $index=>$value) {
            
            ServiceRequestUttpStaff::insert([
                "request_id" => $id,
                "scheduled_id" => $request->input("scheduled_id.".$index),
            ]);

            ServiceRequestUttpInsituStaff::insert([
                "doc_id" => $doc->id,
                "scheduled_id" => $request->input("scheduled_id.".$index),
            ]);
        }

        $serviceRequest->spuh_doc_id = $doc->id;
        
        $serviceRequest->save();

        $items = ServiceRequestUttpItem::where("request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUttp();
            $history->request_status_id = $historyStatus;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = $historyStatus;
            $item->save();
        }

        $response["id"] = $serviceRequest->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $request = ServiceRequestUttp::find($id);
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();

        return view('requestluaruttp.cancel',compact(
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function savecancel($id, Request $request)
    {
        $serviceRequest = ServiceRequestUttp::find($id);

        $serviceRequest->status_id = 16;
        $serviceRequest->cancel_at = date('Y-m-d H:i:s');
        $serviceRequest->cancel_notes = $request->get("cancel_notes");
        $serviceRequest->save();

        $items = ServiceRequestUttpItem::where("request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUttp();
            $history->request_status_id = 16;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = 16;
            $item->save();
        }

        $response["id"] = $serviceRequest->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    public function selesai($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $request = ServiceRequestUttp::find($id);
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();

        return view('requestluaruttp.selesai',compact(
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function saveselesai($id, Request $request)
    {
        $serviceRequest = ServiceRequestUttp::find($id);

        $totalInspectionConfirm = $serviceRequest->items->reduce(function ($carry, $item) {
            return $carry + $item->inspections->count();
        });

        /*
        if ($totalInspectionConfirm == 0 ) {
            $response["id"] = $serviceRequest->id;
            $response["status"] = false;
            $response["messages"] = "Item pengujian belum diisi";

            return response($response);
        }
        */

        if ($serviceRequest->billing_code == null && $serviceRequest->payment_code == null) {
            $serviceRequest->payment_status_id = 5;
        }

        $no_order = $serviceRequest->no_order;

        //$parts_no_order = explode("-", $no_order);
        //$inspection = ServiceRequestUttpItemInspection::join('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_request_uttp_item_inspections.request_item_id')
        //    ->where('service_request_uttp_items.request_id', $id)->count();
        //$parts_no_order[3] = sprintf("%03d",$alat);

        $nextStatus = 13;

        $items = ServiceRequestUttpItem::where('request_id', $id)->get();
        foreach($items as $item) {
            ServiceOrderUttps::where('service_request_item_id', $item->id)
                ->update([
                    //"laboratory_id"=>$inspection->inspectionPrice->instalasi->lab_id, //===UML_STANDARD_ID
                    //"instalasi_id"=>$inspection->inspectionPrice->instalasi_id,
                    //"service_request_id"=>$id,
                    //"service_request_item_id"=>$item->id,
                    //"service_request_item_inspection_id"=>$inspection->id,
                    "lab_staff"=>$serviceRequest->scheduled_test_id,
                    "staff_entry_datein"=>date("Y-m-d", strtotime($request->get("scheduled_test_date_from"))),
                    "staff_entry_dateout"=>date("Y-m-d", strtotime($request->get("scheduled_test_date_to"))),
                    "stat_service_order"=>1,
                    "stat_warehouse"=>-1,
                ]);

            
        }

        $dt1 = new \DateTime($request->get('scheduled_test_date_from'));
        $dt2 = new \DateTime($request->get('scheduled_test_date_to'));
        $interval = $dt1->diff($dt2);

        $total_price = ((int)$interval->format('%a') + 1) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        $serviceRequest->spuh_add_price = $total_price;
        //$serviceRequest->spuh_add_price = ((int)$interval->format('%a')) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        if ($serviceRequest->spuh_add_price > $serviceRequest->spuh_price) {
            // ada tambahan spuh
            $serviceRequest->spuh_add_price = $serviceRequest->spuh_add_price - $serviceRequest->spuh_price;
            $serviceRequest->save();
            
            if ($totalInspectionConfirm > 0) {
                // pnbp sudah ditagihkan
                // hanya perlu menagihkan tambahan spuh
                $nextStatus = 18;                
            } else {
                // pnbp belum ditagihkan
                // perlu kembali ke frontdesk utk input pnbp
            }

        } elseif ($totalInspectionConfirm > 0) { 
            // tidak ada tambahan spuh
            // pnbp sudah ditagihkan
            // tidak perlu invoice 
            // dianggap pengujian sudah selesai
            $serviceRequest->total_inspection_confirm = $totalInspectionConfirm;
            $serviceRequest->billing_to_date == null;
            $serviceRequest->save();

            $nextStatus = 13;
        }else {
            // kalau bolong
            $nextStatus = 13;
        }

        $doc = ServiceRequestUttpInsituDoc::find($serviceRequest->spuh_doc_id);
        $doc->act_date_from = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $doc->act_date_to = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));
        $doc->act_days = ((int)$interval->format('%a') + 1);
        $doc->act_price = $total_price;
        $doc->invoiced_price = $doc->act_price - $doc->price;
        $doc->save();

        // STATUS
        $serviceRequest->status_id = $nextStatus;
        $serviceRequest->received_date = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $serviceRequest->estimated_date = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));

        $serviceRequest->save();

        ServiceRequestUttpItem::where('request_id', $id)->update([
            'status_id' => $nextStatus
        ]);

        ServiceRequestUttpItemInspection::where('request_item_id', $id)->update([
            'status_id' => $nextStatus,
            'status_uttp' => 1,
        ]);

        $items = ServiceRequestUttpItem::where("request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUttp();
            $history->request_status_id = $nextStatus;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = $nextStatus;
            $item->save();
        }

        $response["id"] = $serviceRequest->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    public function createbookinginspection($id)
    {
        $attribute = $this->MyProjects->setup("requestluaruttp");

        $item = ServiceRequestUttpItem::with(['serviceRequest'])
            //->selectRaw('id, request_id, quantity, cast(subtotal as integer) as subtotal, uttp_id')
            ->find($id);
    
        /*
        $prices = DB::table('uttp_inspection_prices')
            ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
            ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id) 
            ->pluck('uttp_inspection_prices.inspection_type','uttp_inspection_prices.id');
            */
        //dd([$item->serviceRequest->service_type_id, $item->uttp->type_id, $prices]);

        //array_unshift($prices , [0 => "Pilih jenis pemeriksaan/pengujian"]);
        //$prices->prepend("-- Pilih jenis pemeriksaan/pengujian --", 0);
        //dd($prices);

        $prices = DB::table('uttp_inspection_prices')
                ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id)
                ->whereNull('uttp_inspection_prices.deleted_at')
                ->whereNull('uttp_inspection_price_types.deleted_at')
                ->select('uttp_inspection_prices.*')
                ->distinct();
        $prices = $prices->get();
        //dd([$prices, $item->serviceRequest->service_type_id, $item->uttp->type_id]);

        return view('requestluaruttp.create_booking_inspection', compact('item', 'prices', 'attribute'));
    }

    public function simpanbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestluaruttp");

        ServiceRequestUttpItemInspection::where('request_item_id', $id)->delete();
        
        foreach($request->get('quantity') as $index=>$value) {
            if ($value != null) {

                $dataInspection = new ServiceRequestUttpItemInspection();
                $dataInspection->request_item_id = $id;
                $dataInspection->quantity = $value;
                $dataInspection->price = $request->get('price')[$index];
                $dataInspection->inspection_price_id = $request->get('id')[$index];

                $dataInspection->status_id = 1;
                $dataInspection->save();
            }

        }

        $item = $this->ServiceRequestUttpItem->find($id);
        
        $itemAggregate = ServiceRequestUttpItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $id)
            ->first();

        $this->ServiceRequestUttpItem->where('id', $id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = $this->ServiceRequestUttpItem
            ->selectRaw('count(service_request_uttp_item_inspections.id) quantity, coalesce(sum(service_request_uttp_item_inspections.price * service_request_uttp_item_inspections.quantity), 0) subtotal')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        $svcRequest = $this->ServiceRequestUttp
            ->selectRaw("service_request_uttps.id, service_request_uttps.received_date, service_request_uttps.received_date 
                + max((case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end)) * interval '1 day' max_estimated_date")
            ->join('service_request_uttp_items', 'service_request_uttp_items.request_id', '=', 'service_request_uttps.id')
            //->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            //->join('uttp_inspection_prices', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            //->join('master_instalasi', 'uttp_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->join('uttps', 'uttps.id', '=', 'service_request_uttp_items.uttp_id')
            ->join('master_uttp_types', 'master_uttp_types.id', '=', 'uttps.type_id')
            ->join('master_instalasi', 'master_uttp_types.instalasi_id', '=', 'master_instalasi.id')
            ->groupBy('service_request_uttps.id', 'service_request_uttps.received_date')
            ->where('service_request_uttps.id', $item->serviceRequest->id)
            ->first();

        $req = $this->ServiceRequestUttp->find($item->serviceRequest->id);
        /*
        $itemCount = $this->ServiceRequestUttpItem->where("request_id", $item->serviceRequest->id)->count();
        $request = $this->ServiceRequestUttp->find($item->serviceRequest->id);
        $no_order = $request->no_order;
        $parts = explode("-",$no_order);
        $parts[3] = sprintf("%03d",intval($itemCount));
        $no_order = implode("-", $parts);
        */

        $origin = $reqAggregate->subtotal;
        $total = $reqAggregate->subtotal;
        if ($req->persen_potongan != null) {
            $total = $reqAggregate->subtotal * (int)$req->persen_potongan / 100;
        }
        
        $this->ServiceRequestUttp->where('id', $item->serviceRequest->id)->update([
            'total_price' => $total,
            'origin_price' => $reqAggregate->subtotal,
            'estimated_date' => $svcRequest ? $svcRequest->max_estimated_date : null,
            //'no_order' => $no_order,
            
        ]);
        //dd([$req->persen_potongan, $origin, $total,$reqAggregate->subtotal]);

        //return Redirect::route('requestluaruttp.selesai', $item->serviceRequest->id);
        //return Redirect::route('requestluaruttp', ['type' => 'dl']);
        return Redirect::route('requestluaruttp.editpnbp', $item->serviceRequest->id);
    }

    public function simpanbookinginspection_old($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestluaruttp");

        $item = ServiceRequestUttpItem::with(['serviceRequest'])
            ->find($id);
        $price = DB::table('uttp_inspection_prices')
            ->where('id', $request->get('inspection_price_id'))
            ->first();

        $harga = $price->price;
        if ($price->has_range) {
            $range = DB::table('uttp_inspection_price_ranges')
                ->where('inspection_price_id', $request->get('inspection_price_id'))
                ->where('min_range', '<=', $request->get('quantity'))
                ->whereRaw('coalesce(max_range, min_range ^ 2) > ?', $request->get('quantity'))
                ->first();
            $harga = $range->price;
        }

        $dataInspection["request_item_id"] = $id;
        $dataInspection["quantity"] = $request->get('quantity');
        $dataInspection["price"] = $harga;
        $dataInspection["inspection_price_id"] = $request->get('inspection_price_id');

        $dataInspection["status_id"] = 1;

        $serviceInspectionID = ServiceRequestUttpItemInspection::insertGetId($dataInspection);

        $itemAggregate = ServiceRequestUttpItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $id)
            ->first();

        ServiceRequestUttpItem::where('id', $id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = ServiceRequestUttpItem::selectRaw('count(service_request_uttp_item_inspections.id) quantity, coalesce(sum(service_request_uttp_item_inspections.price * service_request_uttp_item_inspections.quantity), 0) subtotal')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        $svcRequest = ServiceRequestUttp::selectRaw("service_request_uttps.id, service_request_uttps.received_date, service_request_uttps.received_date 
                + max((case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end)) * interval '1 day' max_estimated_date")
            ->join('service_request_uttp_items', 'service_request_uttp_items.request_id', '=', 'service_request_uttps.id')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->join('uttp_inspection_prices', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->join('master_instalasi', 'uttp_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->groupBy('service_request_uttps.id', 'service_request_uttps.received_date')
            ->where('service_request_uttps.id', $item->serviceRequest->id)
            ->first();

        $itemCount = ServiceRequestUttpItem::where("request_id", $item->serviceRequest->id)->count();
        $request = ServiceRequestUttp::find($item->serviceRequest->id);
        $no_order = $request->no_order;
        $parts = explode("-",$no_order);
        $parts[3] = sprintf("%03d",intval($itemCount));
        $no_order = implode("-", $parts);

        ServiceRequestUttp::where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal,
            'estimated_date' => $svcRequest->max_estimated_date,
            'no_order' => $no_order,
        ]);

        return Redirect::route('requestluaruttp.selesai', $item->serviceRequest->id);
    }

    public function hapusbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $inspection = ServiceRequestUttpItemInspection::where('id', $id)->first();
        $inspection->delete();
        $item_id = $inspection->request_item_id;

        $item = ServiceRequestUttpItem::with(['serviceRequest'])
            ->find($item_id);
        
        $itemAggregate = ServiceRequestUttpItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $item->id)
            ->first();

        ServiceRequestUttpItem::where('id', $item->id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = ServiceRequestUttpItem::selectRaw('count(service_request_uttp_item_inspections.id) quantity, coalesce(sum(service_request_uttp_item_inspections.price * service_request_uttp_item_inspections.quantity), 0) subtotal')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        ServiceRequestUttp::where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal
        ]);

        return Redirect::route('requestluaruttp.selesai', $item->serviceRequest->id);
    }

    public function pdf($id)
    {
        $row = ServiceRequestUttp::find($id);

        if ($row->lokasi_pengujian == 'luar') {
            if ($row->spuh_no == null) {
                $no_parts = explode("-", $row->no_register);
                $row->spuh_no = $no_parts[0].'-'.sprintf("%04d",$no_parts[1]).'/SPUH/'.date("m/Y");
            }
            if ($row->spuh_billing_date == null) {
                $row->spuh_billing_date = date("Y-m-d");
            }
        }
        
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUttpInsituDoc::where('request_id', $id)->get();

        return view('requestluaruttp.pdf',compact(['row', 'staffes', 'docs']));
    }

    public function pdftuhp($id)
    {
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUttpInsituDoc::where('request_id', $id)->where('jenis', 'inisial')->get();

        $row = ServiceRequestUttp::find($id);
        return view('requestluaruttp.pdf_tuhp',compact(['row','staffes','docs']));
    }


    public function pdftuhp_old($id)
    {
        $row = ServiceRequestUttp::find($id);

        if ($row->lokasi_pengujian == 'luar') {
            if ($row->spuh_no == null) {
                $no_parts = explode("-", $row->no_register);
                $row->spuh_no = $no_parts[0].'-'.sprintf("%04d",$no_parts[1]).'/SPUH/'.date("m/Y");
            }
            if ($row->spuh_billing_date == null) {
                $row->spuh_billing_date = date("Y-m-d");
            }
        }
        
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUttpInsituDoc::where('request_id', $id)->get();

        return view('requestluaruttp.pdftuhp',compact(['row', 'staffes', 'docs']));
    }

    public function payment($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $serviceRequest = ServiceRequestUttp::find($id);
        if((!isset($serviceRequest->persen_potongan)) && $serviceRequest->persen_potongan <= 0){
            $origin = explode(',', $serviceRequest->total_price);
            $origin = str_replace('.','',$origin[0]);

            $serviceRequest->update([
                "origin_price" => $origin
            ]);
        }
        $doc = ServiceRequestUttpInsituDoc::find($serviceRequest->spuh_doc_id);

        return view('requestluaruttp.payment',compact([
            'id','serviceRequest', 'doc',"attribute"
        ]));
    }

    public function paymentsave(Request $request)
    {
        $rules = [];
        $serviceRequest = ServiceRequestUttp::find($request->id);
        
        if ($serviceRequest->total_price > 0) {
            $rules = [
                //"payment_date"=>'required',
                //"payment_code"=>'required',
                "billing_code"=>'required',
                "billing_to_date"=>'required',
            ];
        }

        $validation = Validator::make($request->all(),$rules);

        if ($validation->passes())
        {
            $origin = explode(',', $request->total_price);
            $origin = str_replace('.','',$origin[0]);
        
            ServiceRequestUttp::whereId($request->id)
            ->update([
                "origin_price" => $origin,
                "billing_code"=>$request->billing_code,
                "billing_to_date"=>date("Y-m-d", strtotime($request->billing_to_date)),
                "payment_status_id"=> 6,
            ]);

            $items = ServiceRequestUttpItem::where("request_id", $request->id)->orderBy('id', 'asc')->get();

            foreach($items as $item) {
                $history = new HistoryUttp();
                $history->request_status_id = $serviceRequest->status_id;
                $history->request_id = $serviceRequest->id;
                $history->request_item_id = $item->id;
                $history->payment_status_id = 6;
                $history->user_id = Auth::id();
                $history->save();
            }

            //$this->updatepaymentstatus($request);
            $customerEmail = $serviceRequest->requestor->email;
            //Mail::to($customerEmail)->send(new Invoice($svcRequest));
            ProcessEmailJob::dispatch($customerEmail, new Invoice($serviceRequest))->onQueue('emails');


            return response([true,"success"]);
        }
        else
        {
            return response([false,$validation->messages()]);
        }
    }

    public function paymenttuhp($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $serviceRequest = ServiceRequestUttp::find($id);
        $doc = ServiceRequestUttpInsituDoc::find($serviceRequest->spuh_doc_id);

        if ($serviceRequest->spuh_no == null) {
            $no_parts = explode("-", $serviceRequest->no_register);
            $serviceRequest->spuh_no = $no_parts[0].'-'.sprintf("%04d",$no_parts[1]).'/SPUH/'.date("m/Y");
        }
        if ($serviceRequest->spuh_billing_date == null) {
            $serviceRequest->spuh_billing_date = date("Y-m-d");
        }

        return view('requestluaruttp.paymenttuhp',compact([
            'id','serviceRequest', 'doc',"attribute"
        ]));
    }

    public function paymenttuhpsave(Request $request)
    {
        $rules = [];
        $serviceRequest = ServiceRequestUttp::find($request->id);
        
        $doc = ServiceRequestUttpInsituDoc::find($serviceRequest->spuh_doc_id);

        if ($serviceRequest->lokasi_pengujian == 'luar'
            && ($doc->invoiced_price > 0)) {
            ServiceRequestUttp::whereId($request->id)
            ->update([
                "spuh_no" => $request->spuh_no,
                "spuh_billing_date"=>date("Y-m-d"),
                "spuh_payment_date" => null,
                "spuh_payment_status_id" => 6,
            ]);

            $items = ServiceRequestUttpItem::where("request_id", $request->id)->orderBy('id', 'asc')->get();

            foreach($items as $item) {
                $history = new HistoryUttp();
                $history->request_status_id = $serviceRequest->status_id;
                $history->request_id = $request->id;
                $history->request_item_id = $item->id;
                $history->spuh_payment_status_id = 6;
                $history->user_id = Auth::id();
                $history->save();
            }

        }

        if ($doc->payment_date == null) {
            $doc->billing_date = date("Y-m-d");
        }
        if ($doc->act_price != null && $doc->act_price > $doc->price && $doc->act_payment_date == null) {
            $doc->act_billing_date = date("Y-m-d");
        }
        $doc->billing_date = date("Y-m-d");
        $doc->save();

        //$this->updatepaymentstatus($request);

        $customerEmail = $serviceRequest->requestor->email;
        //Mail::to($customerEmail)->send(new Invoice($svcRequest));
        ProcessEmailJob::dispatch($customerEmail, new InvoiceTUHP($serviceRequest, $doc))->onQueue('emails');


        return response([true,"success"]);
   
    }

    private function updatepaymentstatus(Request $request)
    {
        $serviceRequest = ServiceRequestUttp::find($request->id);

        $needPNBP = false;
        $donePNBP = false;
        if ($serviceRequest->total_price > 0) {
            $needPNBP = true;
        }
        if ($needPNBP && $serviceRequest->billing_to_date != null) {
            $donePNBP = true;
        }

        $needTUHP = false;
        $doneTUHP = false;
        $doc = ServiceRequestUttpInsituDoc::find($serviceRequest->spuh_doc_id);
        if ($serviceRequest->lokasi_pengujian == 'luar' && $doc->invoiced_price > 0) {
            $needTUHP = true;
        }
        if ($needTUHP && $serviceRequest->spuh_billing_date != null) {
            $doneTUHP = true;
        }

        $nextStatus = 6;
        //dd([$donePNBP, $needPNBP , $doneTUHP , $needTUHP]);
        if ((!$needPNBP || ($donePNBP && $needPNBP)) && ((!$needTUHP || ($doneTUHP && $needTUHP)))) {
            if ($serviceRequest->status_id == 5) {
                ServiceRequestUttp::whereId($request->id)
                    ->update([
                        "status_id"=> $nextStatus,
                    ]);
                ServiceRequestUttpItem::where("request_id", $request->id)
                    ->update([
                        "status_id"=> $nextStatus,
                    ]);
                $listItems = ServiceRequestUttpItem::where("request_id", $request->id)->pluck('id');
                ServiceRequestUttpItemInspection::whereIn("request_item_id", $listItems)->update([
                    "status_id"=> $nextStatus,
                ]);
            } else {
                $nextStatus = 19;
                ServiceRequestUttp::whereId($request->id)
                    ->update([
                        "payment_status_id"=>6,
                        "status_id"=> $nextStatus,
                    ]);
                ServiceRequestUttpItem::where("request_id", $request->id)
                    ->update([
                        "status_id"=> $nextStatus,
                    ]);
                $listItems = ServiceRequestUttpItem::where("request_id", $request->id)->pluck('id');
                ServiceRequestUttpItemInspection::whereIn("request_item_id", $listItems)->update([
                    "status_id"=> $nextStatus,
                ]);
            }

            $items = ServiceRequestUttpItem::where("request_id", $serviceRequest->id)->get();
            foreach($items as $item) {
                $history = new HistoryUttp();
                $history->request_status_id = $nextStatus;
                $history->request_id = $serviceRequest->id;
                $history->request_item_id = $item->id;
                $history->user_id = Auth::id();
                $history->save();

                $item->status_id = $nextStatus;
                $item->save();
            }

            $svcRequest = ServiceRequestUttp::find($request->id);

            $customerEmail = $svcRequest->requestor->email;
            //Mail::to($customerEmail)->send(new Invoice($svcRequest));
            ProcessEmailJob::dispatch($customerEmail, new Invoice($svcRequest))->onQueue('emails');
        }

    }

    public function valid($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $request = ServiceRequestUttp::find($id);
        return view('requestluaruttp.valid',compact('id',"request","attribute"));
    }

    public function validsave(Request $request)
    {
        $req = ServiceRequestUttp::find($request->id);

        ServiceRequestUttp::whereId($request->id)
            ->update([
                "payment_valid_date" => date("Y-m-d"),
                "status_id" => 13,
            ]);

        ServiceRequestUttpItem::where('request_id', $request->id)->update(['status_id'=>13]);

        $items = ServiceRequestUttpItem::where("request_id", $request->id)->get();
        foreach($items as $item) {
            ServiceRequestUttpItemInspection::where("request_item_id", $item->id)->update([
                "status_id"=>13,
            ]);

            $order = ServiceOrderUttps::where('service_request_item_id', $item->id)->first();

            $history = new HistoryUttp();
            $history->request_status_id = $req->status_id;
            $history->request_id = $req->id;
            $history->request_item_id = $item->id;
            $history->payment_status_id = 8;
            $history->order_id = $order->id;
            $history->order_status_id = $order->stat_service_order;
            $history->user_id = Auth::id();
            $history->save();

        }

        //$this->allvalid($request);

        $customerEmail = $req->requestor->email;
        //Mail::to($customerEmail)->send(new Receipt($svcRequest));
        ProcessEmailJob::dispatch($customerEmail, new Receipt($req))->onQueue('emails');

        return response([true,"success"]);
    }

    public function novalidsave(Request $request)
    {
        ServiceRequestUttp::whereId($request->id)
        ->update([
            "payment_status_id"=>6,
        ]);

        return response([true,"success"]);
    }

    public function validtuhp($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $request = ServiceRequestUttp::find($id);
        return view('requestluaruttp.validtuhp',compact('id',"request","attribute"));
    }

    public function validtuhpsave(Request $request)
    {
        $req = ServiceRequestUttp::find($request->id);

        $doc = ServiceRequestUttpInsituDoc::find($req->spuh_doc_id);

        $spt_revisi = $req->status_revisi_spt;
        if ($doc->jenis == 'revisi') {
            $spt_revisi = 3;     
        }
        $doc->update([
            'valid_bukti_bayar' => date("Y-m-d"),
        ]);

        ServiceRequestUttp::whereId($request->id)
        ->update([
            "spuh_payment_valid_date" => date("Y-m-d"),
            'spuh_payment_status_id' => 8,
            'status_revisi_spt' => $spt_revisi,
        ]);

        

        //$this->allvalid($request);
        $items = ServiceRequestUttpItem::where("request_id", $req->id)->get();
        foreach($items as $item) {
            $history = new HistoryUttp();
            $history->request_status_id = $req->status_id;
            $history->request_id = $req->id;
            $history->request_item_id = $item->id;
            $history->spuh_payment_status_id = 8;
            $history->user_id = Auth::id();
            $history->save();

        }

        $customerEmail = $req->requestor->email;
        //Mail::to($customerEmail)->send(new Receipt($svcRequest));
        ProcessEmailJob::dispatch($customerEmail, new ReceiptTUHP($req, $doc))->onQueue('emails');

        return response([true,"success"]);
    }

    public function novalidtuhpsave(Request $request)
    {
        ServiceRequestUttp::whereId($request->id)
        ->update([
            "spuh_payment_status_id"=>6,
            "spuh_payment_date"=>null,
        ]);

        return response([true,"success"]);
    }

    private function allvalid(Request $request)
    {
        $req = ServiceRequestUttp::find($request->id);

        $needValidPNBP = $req->total_price > 0 && $req->payment_date != null && $req->payment_valid_date == null;
        $validPNBP = true;
        if ($needValidPNBP) {
            $validPNBP = $req->payment_valid_date != null;
        }
        $needValidTUHP = $req->spuhDoc->invoiced_price > 0 && $req->spuh_payment_date != null && $req->spuh_payment_valid_date == null;
        $validTUHP = true;
        if ($needValidTUHP) {
            $validTUHP = $req->spuh_payment_valid_date != null;
        }

        if ($validPNBP 
            && $validTUHP) {

            if ($req->status_id == 7) {
                ServiceRequestUttp::whereId($request->id)
                ->update([
                    "status_id" => 11,
                    "payment_status_id" => null,
                ]);
                $listItems = ServiceRequestUttpItem::where("request_id", $request->id)->pluck('id');
                ServiceRequestUttpItemInspection::whereIn("request_item_id", $listItems)->update([
                    "status_id"=> 11,
                ]);

                $items = ServiceRequestUttpItem::where("request_id", $request->id)
                    ->orderBy('id', 'asc')
                    ->get();

                $serviceType = MasterServiceType::find($req->service_type_id);

                $no_order = $serviceType->last_order_no;
                $prefix = $serviceType->prefix;
        
                $alat = count($items);
                $noorder_num = intval($no_order)+1;
                $noorder = $prefix.'-'.date("y-").sprintf("%04d",$noorder_num)."-";

                $no = 0;
                foreach($items as $item) {
                    
                    $no = $no + 1;
                    $noorder_alat = $noorder.sprintf("%03d",$no);

                    ServiceOrderUttps::insert([
                        "laboratory_id"         => $item->uttp->type->instalasi->lab_id,
                        "instalasi_id"          => $item->uttp->type->instalasi_id,
                        "service_request_id"    => $item->request_id,
                        "service_request_item_id"=> $item->id,
                        //"service_request_item_inspection_id"=>$inspection->id,
                        //"lab_staff"=>Auth::id(),
                        //"staff_entry_datein"=>date("Y-m-d")
                        'tool_type_id'			=> $item->uttp->type_id,
                        'tool_serial_no'		=> $item->uttp->serial_no,
                        'tool_brand'			=> $item->uttp->tool_brand,
                        'tool_model'			=> $item->uttp->tool_model,
                        'tool_type'				=> $item->uttp->tool_type,
                        'tool_capacity'			=> $item->uttp->tool_capacity,
                        'tool_capacity_unit'	=> $item->uttp->tool_capacity_unit,
                        'tool_factory'			=> $item->uttp->tool_factory,
                        'tool_factory_address'	=> $item->uttp->tool_factory_address,
                        'tool_made_in'			=> $item->uttp->tool_made_in,
                        'tool_made_in_id'		=> $item->uttp->tool_made_in_id,
                        'tool_owner_id'			=> $item->uttp->owner_id,
                        'tool_media'			=> $item->uttp->tool_media,
                        'tool_name'				=> $item->uttp->tool_name,
                        'tool_capacity_min'		=> $item->uttp->tool_capacity_min,

                        'uttp_id'               => $item->uttp_id,
                    ]);

                    ServiceRequestUttpItem::where("id", $item->id)
                    ->update([
                        "status_id"=>11,
                        "no_order"=>$noorder_alat,
                        "order_at"=> $req->lokasi_pengujian == 'dalam' ? date("Y-m-d H:i:s") : null,
                    ]);

                    $history = new HistoryUttp();
                    $history->request_status_id = 11;
                    $history->request_id = $request->id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();
                }

                $serviceType->last_order_no = $noorder_num;
                $serviceType->save();
            }
            if ($req->status_id == 13) {
                ServiceRequestUttp::whereId($request->id)
                ->update([
                    "payment_status_id"=>13,
                ]);

                $items = ServiceRequestUttpItem::where("request_id", $request->id)->get();
                foreach($items as $item) {
                    $history = new HistoryUttp();
                    $history->request_status_id = 13;
                    $history->request_id = $request->id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = 13;
                    $item->save();
                }

            }
            if ($req->status_id == 15) {
                ServiceRequestUttp::whereId($request->id)
                ->update([
                    "payment_status_id"=>null,
                    "status_id" => 11
                ]);

                $items = ServiceRequestUttpItem::where("request_id", $request->id)->get();
                foreach($items as $item) {
                    $history = new HistoryUttp();
                    $history->request_status_id = 11;
                    $history->request_id = $request->id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = 11;
                    $item->save();
                }

            }
            if ($req->status_id == 20) {
                ServiceRequestUttp::whereId($request->id)
                ->update([
                    "status_id"=>13,
                    "payment_status_id"=>13,
                ]);

                ServiceRequestUttpItem::where("request_id", $request->id)
                    ->update([
                        "status_id"=>13,
                    ]);

                $listItems = ServiceRequestUttpItem::where("request_id", $request->id)->pluck('id');
                ServiceRequestUttpItemInspection::whereIn("request_item_id", $listItems)->update([
                    "status_id"=> 13,
                ]);

                $items = ServiceRequestUttpItem::where("request_id", $request->id)->get();
                foreach($items as $item) {
                    $history = new HistoryUttp();
                    $history->request_status_id = 13;
                    $history->request_id = $request->id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = 13;
                    $item->save();
                }

            }

            $svcRequest = ServiceRequestUttp::find($request->id);

            $customerEmail = $svcRequest->requestor->email;
            //Mail::to($customerEmail)->send(new Receipt($svcRequest));
            ProcessEmailJob::dispatch($customerEmail, new Receipt($svcRequest))->onQueue('emails');
        }
    }

    public function extend($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $request = ServiceRequestUttp::find($id);
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();

        return view('requestluaruttp.extend',compact(
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function saveextend($id, Request $request)
    {
        $serviceRequest = ServiceRequestUttp::find($id);

        $serviceRequest->scheduled_test_date_from = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $serviceRequest->scheduled_test_date_to = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));

        $serviceRequest->status_id = 18; // penagihan tambahan

        $serviceRequest->save();

        $dt1 = new \DateTime($request->get('scheduled_test_date_from'));
        $dt2 = new \DateTime($request->get('scheduled_test_date_to'));
        $interval = $dt1->diff($dt2);

        $total_price = ((int)$interval->format('%a') + 1) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        $serviceRequest->spuh_add_price = $total_price;
        //$serviceRequest->spuh_add_price = ((int)$interval->format('%a')) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        if ($serviceRequest->spuh_add_price > $serviceRequest->spuh_price) {
            $serviceRequest->spuh_add_price = $serviceRequest->spuh_add_price - $serviceRequest->spuh_price;
            $serviceRequest->save();
        }

        $doc = ServiceRequestUttpInsituDoc::find($serviceRequest->spuh_doc_id);
        $doc->act_date_from = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $doc->act_date_to = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));
        $doc->act_days = ((int)$interval->format('%a') + 1);
        $doc->act_price = $total_price;
        $doc->save();

        $response["id"] = $serviceRequest->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    public function editpnbp($id)
    {
        $attribute = $this->MyProjects->setup("requestluaruttp");
        $request = ServiceRequestUttp::find($id);
        
        $total = explode(',', number_format($request->total_price, 2, ",", "."));
        $total = str_replace('.','',$total[0]);
        $origin = $total;
        if ($request->persen_potongan != null) {
            $origin = $total * 100 / $request->persen_potongan;
        }
        ServiceRequestUttp::whereId($id)->whereNull("origin_price")
        ->update([
            "origin_price" => $origin,
        ]);

        $doc = ServiceRequestUttpInsituDoc::find($request->spuh_doc_id);

        $requestor = Customer::find($request->requestor_id);

        return view('requestluaruttp.edit_pnbp',compact([
            'id','request', 'doc',
            'requestor',
            "attribute"
        ]));
    }

    public function setPotongan($id, $disc, Request $r)
    {
        //dd($r);
        $total =explode(',',$r->get('total'));
        $origin = explode(',',$r->get('origin_price'));
        $total = str_replace('.','',$total[0]); 
        $origin = str_replace('.','',$origin[0]);
        if ($r->has('disc') && $r->get('disc') != null) {
            $total = $origin * (int)$r->get('disc') / 100;
        }
        //dd([$origin, $total,$r]);
        $st = ServiceRequestUttp::whereId($id)
        ->update([
            "persen_potongan"=>$r->get('disc'),
            "total_price" => $total,
            "origin_price" => $origin,
        ]);

        if($st){
            return response([true,"success"]); }
        else{return response([false,"error"]);}
    }
}