<?php

namespace App\Http\Controllers;

use App\MasterLaboratory;
use App\ServiceOrders;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

use Illuminate\Support\Facades\DB;
use PDF;

class TermController extends Controller
{
    public function __construct() 
    {
        
    }

    public function index()
    {
    }

    public function term($idlab)
    {
        // $token = $this->checkToken($token);
        // if ($token) {
            $row = MasterLaboratory::find($idlab);
            $term = $row->kaji_ulang;
            
            return view('requestuut.kajiulang_pdf',
                compact('term'));
        // }
        // abort(403);
    }
    public function downloadPDF($id) {        
        $row = ServiceOrders::with(['MasterLaboratory','ServiceRequestItem'])->find($id);
        $term = $row;
        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'defaultMediaType' => 'print',
            'preferCSSPageSize' => true,
            'marginTop' => 12,
            'marginBottom' => 0.2,
            'marginLeft' => 0.2,
            'marginRight' => 0.2
        ])
        ->setPaper([20, 10, 99.21, 198.43], 'landscape')
        ->loadview('requestuut.kajiulang_pdf',compact('row'));
        return $pdf->download('Kajiulang-'.$row->ServiceRequestItem->no_order.'.pdf');
    }
}
