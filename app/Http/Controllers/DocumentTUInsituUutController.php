<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use App\ServiceRequest;
use App\Customer;
use App\ServiceRequestUutInsituDoc;
use App\ServiceRequestUutInsituStaff;
use App\UttpInsituCheckItem;
use App\ServiceRequestUutInsituDocCheck;
use App\HistoryUut;
use App\ServiceRequestItem;
use App\MasterServiceType;
use App\ServiceOrders;
use App\MasterUsers;
use App\ServiceRequestUutStaff;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DocumentTUInsituUutController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("doctuinsituuut");

        $rows = ServiceRequest::whereIn('status_id', [11,12,13,14,19,20,22])
            ->whereNotNull('spuh_doc_id')
            ->whereNotNull('spuh_spt')
            ->select('service_requests.*')
            ->orderBy('received_date','desc')->get();

        $rows_status = ServiceRequest::whereIn('status_id', [11,12,13,14,19,20,22])
            ->whereNotNull('spuh_doc_id')
            ->whereNotNull('spuh_spt')
            ->where('status_revisi_spt', 1)
            ->select('service_requests.*')
            ->orderBy('received_date','desc')->get();

        $rows_bukti = ServiceRequest::whereIn('status_id', [11,12,13,14,19,20,22])
            ->whereNotNull('spuh_doc_id')
            ->whereNotNull('spuh_spt')
            ->where('status_revisi_spt', 2)
            ->select('service_requests.*')
            ->orderBy('received_date','desc')->get();

        return view('doctuinsituuut.index',compact('rows','rows_status','rows_bukti','attribute'));
    }

    public function list($id) 
    {
        $attribute = $this->MyProjects->setup("doctuinsituuut");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();
        $docs = ServiceRequestUutInsituDoc::with(['listOfStaffs'])
            ->where('request_id', $id)->orderBy('id', 'desc')->get();
            
        $staffs = ServiceRequestUutInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        return view('doctuinsituuut.list', compact(['request', 'requestor', 'doc', 'docs', 'staffs',  'attribute']));
    }

    public function create($id) 
    {
        $attribute = $this->MyProjects->setup("doctuinsituuttp");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis','inisial')->orderBy('id', 'desc')->first();
        
        $kabalais = MasterUsers::where("user_role", 22) //kabalai
            ->whereHas('PetugasUut', function($query) {
                $query->where('flag_unit', 'snsu')->where('is_active', 1);
            })
            ->pluck('full_name', 'id');

        $staffes = ServiceRequestUutStaff::where("request_id", $id)->get();

        return view('doctuinsituuut.create', compact(['request', 'staffes', 'requestor', 'doc',  'kabalais', 'attribute']));
    }

    public function store($id, Request $request) 
    {
        $attribute = $this->MyProjects->setup("doctuinsituuut");

        $req = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);
        $requestor = Customer::find($req->requestor_id);

        $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis','inisial')->orderBy('id', 'desc')->first();
        
        $kabalais = MasterUsers::where("user_role", 22) //kabalai
            ->whereHas('PetugasUut', function($query) {
                $query->where('flag_unit', 'snsu')->where('is_active', 1);
            })
            ->pluck('full_name', 'id');

        //$rules['file_spt'] = ['required','mimes:pdf,jpg,jpeg,png'];
        $rules['doc_no'] = ['required'];
        $rules['accepted_date'] = ['required'];
        $rules['accepted_by_id'] = ['required'];
        $rules['date_from'] = ['required'];
        $rules['date_to'] = ['required'];
        $rules['scheduled_id_1'] = ['required'];
        //$rules['scheduled_id_2'] = ['required'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $file_spt = $request->file('file_spt');
            
            $data['file_spt'] = null;
            $data['path_spt'] = null;
            if ($file_spt != null) {
                $data['file_spt'] = $file_spt->getClientOriginalName();

                $path = $file_spt->store(
                    'skhp',
                    'public'
                );

                $data['path_spt'] = $path;
            }
            
            $file_spuh = $request->file('file_spuh');
            
            $data['file_spuh'] = null;
            $data['path_spuh'] = null;
            if ($file_spuh != null) {
                $data['file_spuh'] = $file_spuh->getClientOriginalName();

                $path = $file_spuh->store(
                    'skhp',
                    'public'
                );

                $data['path_spuh'] = $path;
            }

            $file_bukti_bayar = $request->file('file_bukti_bayar');

            $data['file_bukti_bayar'] = null;
            $data['path_bukti_bayar'] = null;
            if ($file_bukti_bayar != null) {
                $data['file_bukti_bayar'] = $file_bukti_bayar->getClientOriginalName();

                $path = $file_bukti_bayar->store(
                    'skhp',
                    'public'
                );

                $data['path_bukti_bayar'] = $path;
            }

            $data["accepted_date"] = date("Y-m-d", strtotime($request->get("accepted_date")));

            $data["date_from"] = date("Y-m-d", strtotime($request->get("date_from")));
            $data["date_to"] = date("Y-m-d", strtotime($request->get("date_to")));

            $dt1 = new \DateTime($data['date_from']);
            $dt2 = new \DateTime($data['date_to']);
            $interval = $dt1->diff($dt2);

            $staffs[] = $request->get('scheduled_id_1');
            $staffs[] = $request->get('scheduled_id_2');

            //dd($data);

            $doc = ServiceRequestUutInsituDoc::create([
                "request_id" => $id,
                "doc_no"=>$request->get('doc_no'),
                "spuh_year"=>date("Y"),
                //"doc_no" => $request->get('doc_no'),
                //"rate" => $data["spuh_rate"],
                //"rate_id" => $data["spuh_rate_id"],
                //"price" => $data["spuh_price"],
                //"invoiced_price" => $data["spuh_price"],
                "staffs" => implode(";",$staffs),
                "date_from" => $data["date_from"],
                "date_to" => $data["date_to"],
                "days" => ((int)$interval->format('%a') + 1),
                "accepted_date" => $data['accepted_date'],
                "accepted_by_id" => $request->get('accepted_by_id'),

                "file_spt" => $data["file_spt"],
                "path_spt" => $data["path_spt"],
                "file_spuh" => $data["file_spuh"],
                "path_spuh" => $data["path_spuh"],
                "file_bukti_bayar" => $data["file_bukti_bayar"],
                "path_bukti_bayar" => $data["path_bukti_bayar"],
            ]);

            ServiceRequestUutStaff::where("request_id", $id)->delete();
            
            ServiceRequestUutStaff::insert([
                "request_id" => $id,
                "scheduled_id" => $request->get("scheduled_id_1"),
            ]);
            ServiceRequestUutStaff::insert([
                "request_id" => $id,
                "scheduled_id" => $request->get("scheduled_id_2"),
            ]);

            ServiceRequestUutInsituStaff::insert([
                "doc_id" => $doc->id,
                "scheduled_id" => $request->get("scheduled_id_1"),
            ]);
            ServiceRequestUutInsituStaff::insert([
                "doc_id" => $doc->id,
                "scheduled_id" => $request->get("scheduled_id_2"),
            ]);

            $status_revisi_spt = 2;
            if ($file_spuh != null && $file_bukti_bayar != null) {
                $status_revisi_spt = 3;
            }
            $req->update([
                //"spuh_doc_id" => $doc->id,
                "status_revisi_spt" => $status_revisi_spt
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";

            return redirect()->route('doctuinsituuut.list', $id)->with('response', $response);

        }

        $response["status"] = false;

        //dd($response);

        return redirect()->route('doctuinsituuut.create', $id)->with('response', $response);
    }

    public function download($id, String $tipe)
    {
        $doc = ServiceRequestUutInsituDoc::find($id);

        $path = 'path_' . $tipe;
        $file = 'file_' . $tipe;

        return Storage::disk('public')->download($doc[$path], $doc[$file]);
    }

    public function edit($id) 
    {
        $attribute = $this->MyProjects->setup("doctuinsituuttp");

        $doc = ServiceRequestUutInsituDoc::find($id);

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($doc->request_id);
        $requestor = Customer::find($request->requestor_id);

        $kabalais = MasterUsers::where("user_role", 22) //kabalai
            ->whereHas('PetugasUut', function($query) {
                $query->where('flag_unit', 'snsu')->where('is_active', 1);
            })
            ->pluck('full_name', 'id');

        $staffes = ServiceRequestUutStaff::where("request_id", $doc->request_id)->get();

        return view('doctuinsituuut.edit', compact(['request', 'staffes', 'requestor', 'doc',  'kabalais', 'attribute']));
    }

    public function update($id, Request $request) 
    {
        $attribute = $this->MyProjects->setup("doctuinsituuut");

        $doc = ServiceRequestUutInsituDoc::find($id);


        $req = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($doc->request_id);
        $requestor = Customer::find($req->requestor_id);

    
        $kabalais = MasterUsers::where("user_role", 22) //kabalai
            ->whereHas('PetugasUut', function($query) {
                $query->where('flag_unit', 'snsu')->where('is_active', 1);
            })
            ->pluck('full_name', 'id');

        //$rules['file_spt'] = ['required','mimes:pdf,jpg,jpeg,png'];
        
        $rules['doc_no'] = ['required'];
        $rules['accepted_date'] = ['required'];
        $rules['accepted_by_id'] = ['required'];
        $rules['date_from'] = ['required'];
        $rules['date_to'] = ['required'];
        $rules['scheduled_id_1'] = ['required'];
        $rules['scheduled_id_2'] = ['required'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $file_spt = $request->file('file_spt');
            
            //$data['file_spt'] = null;
            //$data['path_spt'] = null;
            if ($file_spt != null) {
                $data['file_spt'] = $file_spt->getClientOriginalName();

                $path = $file_spt->store(
                    'skhp',
                    'public'
                );

                $data['path_spt'] = $path;
            }
            
            $file_spuh = $request->file('file_spuh');
            //$data['file_spuh'] = null;
            //$data['path_spuh'] = null;
            if ($file_spuh != null) {
                $data['file_spuh'] = $file_spuh->getClientOriginalName();

                $path = $file_spuh->store(
                    'skhp',
                    'public'
                );

                $data['path_spuh'] = $path;
            }

            $file_bukti_bayar = $request->file('file_bukti_bayar');

            //$data['file_bukti_bayar'] = null;
            //$data['path_bukti_bayar'] = null;
            if ($file_bukti_bayar != null) {
                $data['file_bukti_bayar'] = $file_bukti_bayar->getClientOriginalName();

                $path = $file_bukti_bayar->store(
                    'skhp',
                    'public'
                );

                $data['path_bukti_bayar'] = $path;
            }

            $data["accepted_date"] = date("Y-m-d", strtotime($request->get("accepted_date")));

            $data["date_from"] = date("Y-m-d", strtotime($request->get("date_from")));
            $data["date_to"] = date("Y-m-d", strtotime($request->get("date_to")));

            $dt1 = new \DateTime($data['date_from']);
            $dt2 = new \DateTime($data['date_to']);
            $interval = $dt1->diff($dt2);

            $staffs[] = $request->get('scheduled_id_1');
            $staffs[] = $request->get('scheduled_id_2');

            //dd($data);

            $data["request_id"] = $req->id;
            $data["doc_no"] = $request->get('doc_no');
            $data["spuh_year"] = date("Y");
            $data["staffs"] = implode(";",$staffs);
            $data["days"] = ((int)$interval->format('%a') + 1);
            $data["accepted_by_id"] = $request->get('accepted_by_id');

            $doc->update($data);

            ServiceRequestUutStaff::where("request_id", $req->id)->delete();
            ServiceRequestUutStaff::insert([
                "request_id" => $req->id,
                "scheduled_id" => $request->get("scheduled_id_1"),
            ]);
            if ($request->has("scheduled_id_2")) {
                ServiceRequestUutStaff::insert([
                    "request_id" => $req->id,
                    "scheduled_id" => $request->get("scheduled_id_2"),
                ]);
            }

            ServiceRequestUutInsituStaff::where("doc_id", $doc->id)->delete();
            ServiceRequestUutInsituStaff::insert([
                "doc_id" => $doc->id,
                "scheduled_id" => $request->get("scheduled_id_1"),
            ]);
            if ($request->has("scheduled_id_2")) {
                ServiceRequestUutInsituStaff::insert([
                    "doc_id" => $doc->id,
                    "scheduled_id" => $request->get("scheduled_id_2"),
                ]);
            }

            //update
            $doc = ServiceRequestUutInsituDoc::find($id);
            if ($doc->file_spuh != null && $doc->file_bukti_bayar != null) {
                $req->update([
                    "status_revisi_spt" => 3
                ]);
            }


            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";

            return redirect()->route('doctuinsituuut.list', $req->id)->with('response', $response);

        }

        $response["status"] = false;

        return redirect()->route('doctuinsituuut.edit', $id)->with('response', $response);
    }
}