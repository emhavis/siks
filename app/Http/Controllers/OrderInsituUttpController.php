<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\ServiceOrderUttps;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\Customer;
use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\HistoryUttp;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class OrderInsituUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("orderinsituuttp");

        $petugas_id = Auth::user()->petugas_uttp_id;

        /*
        $rows = ServiceRequestUttp::where('status_id', 11)
            ->leftJoin('service_request_uttp_insitu_doc', 'service_request_uttp_insitu_doc.id', '=', 'service_request_uttps.spuh_doc_id')
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->where('service_request_uttp_insitu_staff.scheduled_id', $petugas_id)
            ->select('service_request_uttps.*', 'service_request_uttp_insitu_doc.doc_no')
            ->orderBy('received_date','desc')->get();
            */

        $rows = ServiceOrderUttps::join('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_order_uttps.service_request_item_id' )
            ->join('service_request_uttps', 'service_request_uttps.id', '=', 'service_order_uttps.service_request_id' )
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id", 11);
            })
            ->leftJoin('service_request_uttp_insitu_doc', 'service_request_uttp_insitu_doc.id', '=', 'service_request_uttps.spuh_doc_id')
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->where('service_request_uttp_insitu_staff.scheduled_id', $petugas_id)
            ->orderBy('service_request_uttp_items.order_at', 'asc')
            ->orderBy('service_request_uttp_items.received_at', 'asc')
            ->orderBy('service_request_uttp_items.delivered_at', 'asc')
            ->with([
                'ServiceRequestItem' => function ($query)
                {
                    $query
                        ->orderBy('order_at', 'asc')
                        ->orderBy('received_at', 'asc')
                        ->orderBy('delivered_at', 'asc');
                }
            ])
            ->select('service_order_uttps.*')
            ->get();
            //dd($rows);

        return view('orderinsituuttp.index',compact('rows','attribute'));
    }    

    public function order($id) 
    {
        $attribute = $this->MyProjects->setup("firstcheckluaruttp");

        $order = ServiceOrderUttps::find($id);

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($order->service_request_id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $request->id)->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        return view('orderinsituuttp.order', compact(['order', 'request', 'requestor', 'doc', 'staffs',  'attribute']));
    }

    public function saveorder($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $req = ServiceRequestUttp::find($order->service_request_id); 
        
        $order->lab_staff = Auth::id();
        $order->staff_entry_datein = $req->spuhDoc->date_from;
        $order->staff_entry_dateout = $req->spuhDoc->date_to;
        $order->mulai_uji = $req->spuhDoc->date_from;
        $order->selesai_uji = $req->spuhDoc->date_to;

        $order->stat_service_order = 0;
        $order->save();

        $status = 13;

        $item = ServiceRequestUttpItem::find($order->service_request_item_id);
        $item->status_id = $status;
        $item->save();

        $history = new HistoryUttp();
        $history->request_status_id = $status;
        $history->request_id = $item->request_id;
        $history->request_item_id = $item->id;
        $history->user_id = Auth::id();
        $history->save();

        ServiceRequestUttpItemInspection::where("request_item_id", $item->id)->update([
            "status_id"=>$status,
        ]);

        $items_count = DB::table('service_request_uttp_items')
            ->selectRaw('sum(case when status_id = '. $status .' then 1 else 0 end) count_x, count(id) count_all')
            ->where('request_id', $item->request_id)->get();

        if ($items_count[0]->count_x == $items_count[0]->count_all) {
            ServiceRequestUttp::find($item->request_id)->update(['status_id'=>$status]);
        }

        return Redirect::route('orderinsituuttp');
    }
}
