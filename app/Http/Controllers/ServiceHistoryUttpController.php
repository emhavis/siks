<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrderUttpTTUInspection;
use App\ServiceOrderUttpTTUInspectionItems;
use App\ServiceOrderUttpTTUInspectionBadanHitung;
use App\ServiceOrderUttpTTUPerlengkapan;
use App\ServiceOrderUttpEvaluasiTipe;

use App\Mail\Pending;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;

use App\MasterInstalasi;
use App\HistoryUttp;
use App\ServiceOrders;

class ServiceHistoryUttpController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("servicehistoryuttp");

        $laboratory_id = Auth::user()->laboratory_id;
        //$instalasi_id = Auth::user()->instalasi_id;

        $instalasiList = MasterInstalasi::where('lab_id', $laboratory_id)->orderBy('id','asc')
            ->get();

        foreach($instalasiList as $instalasi) {
           $rows[$instalasi->id] = ServiceOrderUttps::whereIn("stat_service_order",[0,1,2,3,4])
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->whereIn("status_id", [13,14,16]);
            })
            ->whereHas('ServiceRequest', function($query) 
            {
                $query->where("lokasi_pengujian","=", 'dalam');
            })
            ->where('stat_sertifikat', '>=', 1)
            //->where('lab_staff', Auth::id())
            //->where('test_by_1', Auth::id())
            ->where(function($query) {
                $query->where(function($q1) {
                    $q1->where('test_by_1', Auth::id())
                        ->orWhere('test_by_2', Auth::id());
                    })
                    ->orWhere(function($q2) {
                        $q2->whereNotNull('cancel_at')
                            ->where('cancel_id', Auth::id());
                    });
            })
            ->where('laboratory_id', $laboratory_id)
            ->where('instalasi_id', $instalasi->id)
            ->orderBy('staff_entry_datein','desc')
            ->get();
            ///dd([Auth::id(), $rows[$instalasi->id]]);
        }

        return view('servicehistoryuttp.index',compact('rows','attribute','instalasiList'));
    }

    public function warehouse(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        if ($request->has('id')) {
            $order = ServiceOrderUttps::find($request->id);
        } else {
            $order = ServiceOrderUttps::where("no_order", $request->no_order)->first();
        }

        if ($order != null) {
            //$order = ServiceOrderUttps::find($id);
            ServiceOrderUttps::whereId($order->id)->update(["stat_warehouse"=>1]);

            /*
            $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
            $inspection->update([
                'status_uttp' => 1
            ]);
            */

            $history = new HistoryUttp();
            $history->request_status_id = 13;
            $history->request_id = $order->service_request_id;
            $history->request_item_id = $order->service_request_item_id;
            $history->order_id = $order->id;
            $history->order_status_id = $order->stat_sertifikat;
            $history->warehouse_status_id = 1;
            $history->user_id = Auth::id();
            $history->save();

            //$this->checkAndUpdateFinishOrder($order->id);

            $response["status"] = true;
            $response["messages"] = "Alat telah dikirim ke gudang";

            
        }

        return response($response);
    }

    private function checkAndUpdateFinishOrder($id) 
    {
        $order = ServiceOrderUttps::find($id);

        /*
        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        if ($inspection->status_sertifikat == 1 && $inspection->status_uttp == 1) {
            $inspection->update(['status_id' => 14]); 
        }

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_14 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
        }
        */
        if ($order->stat_warehouse == 1 && $order->stat_sertifikat == 3) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestUttpItemInspection::where("request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUttp();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                $history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }

    

}
