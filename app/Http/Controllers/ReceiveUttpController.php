<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\ServiceOrderUttps;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\HistoryUttp;
use App\MasterInstalasi;

class ReceiveUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("receiveuttp");

        $laboratory_id = Auth::user()->laboratory_id;
        // $instalasi_id = Auth::user()->instalasi_id;
        
        $instalasiList = MasterInstalasi::where('lab_id', $laboratory_id)->orderBy('id','asc')
            ->get();
        
        foreach($instalasiList as $instalasi) {
            $rows[$instalasi->id] = ServiceOrderUttps::whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id", 9);
            })
            ->whereHas('instalasi', function($query)use($laboratory_id, $instalasi)
            {
                $query->where("lab_id",$laboratory_id)
                    ->where("id",$instalasi->id);
            })
            ->with([
                'ServiceRequestItem' => function ($query)
                {
                    $query->orderBy('delivered_at', 'asc');
                }
            ])
            ->get();
        }
        //dd($rows);

        return view('receiveuttp.index',compact('rows','attribute','laboratory_id','instalasiList'));
    }    

    public function proses(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        if ($request->has('id')) {
            $item = ServiceRequestUttpItem::where("id", $request->id)->first();
        } else {
            $item = ServiceRequestUttpItem::where("no_order", $request->no_order)->first();
        }

        if ($item != null) {
            //$item = ServiceRequestUttpItem::where("id", $request->id)->first();
            $item->status_id = 10;
            $item->received_at = date("Y-m-d H:i:s");
            $item->save();
            
            $history = new HistoryUttp();
            $history->request_status_id = 10;
            $history->request_id = $item->request_id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            ServiceRequestUttpItemInspection::where("request_item_id", $item->id)->update([
                "status_id"=>10,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 10 then 1 else 0 end) count_10, count(id) count_all')
                ->where('request_id', $item->request_id)->get();

            if ($items_count[0]->count_10 == $items_count[0]->count_all) {
                ServiceRequestUttp::find($item->request_id)->update(['status_id'=>10]);
            }

            $response["status"] = true;
            $response["messages"] = "Alat telah diterima";
        }
        

        return response($response);
    }

    /*
    public function proses($id)
    {
        $inspection = ServiceRequestUttpItemInspection::with([
            'inspectionPrice', 
            'item',
            'item.serviceRequest'
        ])
            ->find($id);

        $inspection->update(["status_id"=>10]);

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id >= 10 then 1 else 0 end) count_10, count(id) count_all')
            ->where('request_item_id', $inspection->request_item_id)->get();
        
        if ($inspections_count[0]->count_10 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::whereId($inspection->request_item_id)
                ->update(["status_id"=>10]); // TERIMA ALAT 
        }

        $items_count = DB::table('service_request_uttp_items')
            ->selectRaw('sum(case when status_id >= 10 then 1 else 0 end) count_10, count(id) count_all')
            ->where('request_id', $inspection->item->request_id)->get();

        if ($items_count[0]->count_10 == $items_count[0]->count_all) {
            ServiceRequestUttp::whereId($inspection->item->request_id)
                ->update(["status_id"=>10]); // TERIMA ALAT
        }

        return Redirect::route('receiveuttp');
    }
    */

    public function takeorder($id)
    {
        $attribute = $this->MyProjects->setup("takeorder");

        $request = ServiceRequestUttps::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        return view('requestuttp.edit_booking', compact('request', 'attribute'));
    }
}
