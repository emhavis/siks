<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Uttp;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\MyClass\MyProjects;
use App\MasterServiceType;
use App\MasterDocTypes;
use App\MasterDocNumber;
use App\MasterInstalasi;

use App\ServiceBooking;

use App\Customer;
use App\HistoryUut;
use App\UttpInspectionPrice;
use App\Holiday;
use App\Mail\BookingConfirmation;
use App\ServiceRequestUttpStaff;
use App\ServiceRequestUttpInsituDoc;

use App\Mail\Invoice;
use App\Mail\Invoice_uut;
use App\Mail\Receipt;
use App\Mail\Receipt_uut;
use App\Mail\BookingConfirmation_uut;
use App\MasterLaboratory;
use App\ServiceOrders;
use App\ServiceRequest;
use App\ServiceRequestItem;
use App\ServiceRequestItemInspection;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUutItemTTUPerlengkapan;
use App\ServiceType;
use App\Standard;
use App\StandardInspectionPrice;
use App\User;
use App\UserCustomer;
use App\Uut;
use App\UutOwner;
use Illuminate\Support\Facades\Mail;
use App\UserUutOwner;

use App\Jobs\ProcessEmailJob;

use PDF;

class RequestUutController extends Controller
{
    // protected $StandardInspectionPrice;
    protected $MyProjects;
    protected $MasterDocTypes;
    protected $MasterServiceType;
    protected $ServiceRequestItem;
    protected $MasterLaboratory;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        $this->MasterServiceType = new MasterServiceType();
        // all the requirement here
        $this->ServiceBooking = new ServiceBooking();
        $this->ServiceRequest = new ServiceRequest();
        $this->ServiceRequestItem = new ServiceRequestItem();
        $this->ServiceRequestItemInspection = new ServiceRequestItemInspection();
        $this->ServiceOrder = new ServiceOrders();
        $this->ServiceRequesItem = new ServiceRequestItem();
        $this->MasterLaboratory = new MasterLaboratory();

        $this->Customer = new Customer();

        $this->MasterInstalasi = new MasterInstalasi();
        $this->Holiday = new Holiday();
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuut");

        /*
        $id_arr = [1,2,3,4];
        if ($request->has("status_id")) {
            $ids = $request->get("status_id");
            $id_arr = explode(",",$ids);
        } 
        */
        if($request->get('type') == 'kn' || empty($request->get('type'))){
            $rows_pendaftaran = ServiceRequest::where('status_id', 1)
                ->orderBy('received_date','desc')->get();

            $rows_penagihan = ServiceRequest::whereIn('status_id', [5,6])
                ->where('lokasi_pengujian', 'dalam')
                ->orderBy('received_date','desc')->get();

            $rows_validasi = ServiceRequest::where('status_id', 7)
                ->where('lokasi_pengujian', 'dalam')
                ->orderBy('received_date','desc')->get();

            /* $rows_kirim = ServiceRequest::where('status_id', 8)
                ->orderBy('received_date','desc')->get(); */

            $rows_kirim = $rows_done = ServiceOrders::whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id", 8);
            })->with([
                'serviceRequest' => function ($query)
                {
                    $query->where('status_id', 8)
                        ->orderBy('received_date', 'desc');
                }
            ])
                ->orderBy('id','asc')->get();

            // $rows_process = ServiceRequestItem::with([
            //     'serviceRequest' => function ($query)
            //     {
            //         $query->orderBy('received_date', 'desc');
            //     }
            // ])->whereIn('status_id', [9,10,11,12,13,15])->get();

            // ::whereIn('status_id', [9,10,11,12,13,15])
            //     ->orderBy('received_date','desc')->get();
                // dd([$rows_process[0]->no_order,$rows_process[0]->serviceRequestItem[0]->no_order]);
            
            // $rows_done = ServiceRequest::whereIn('status_id', [14,16])
            //     ->orderBy('received_date','desc')->get();

            // $rows_done = ServiceOrders::whereHas('ServiceRequestItem', function($query) 
            //     {
            //         $query->whereIn("status_id", [14,16]);
            //     })
            // ->orderBy('staff_entry_datein','desc')->get();

            $bookings = $this->ServiceBooking
                ->whereIn('service_type_id', [1,2])
                ->whereNotNull('booking_no')
                ->where('requested', false)
                ->where('lokasi_pengujian', 'dalam')
                ->where('est_arrival_date', date('Y-m-d'))
                ->orderBy('id')
                ->get();
            // $bookings_all = $this->ServiceBooking
            //     ->whereIn('service_type_id', [1,2])
            //     ->whereNotNull('booking_no')
            //     ->whereNotNull('uut_owner_id')
            //     // ->where('requested', false)
            //     ->where('lokasi_pengujian', 'dalam')
            //     ->orderBy('id')
            //     ->get();
                // dd($bookings_all);
            //$statuses = DB::table('master_request_status')->orderBy('id','asc')->pluck('status', 'id');
        
            return view('requestuut.index', [
                'rows_pendaftaran' => $rows_pendaftaran,
                'rows_penagihan' => $rows_penagihan,
                'rows_validasi' => $rows_validasi,
                'rows_kirim' => $rows_kirim,
                'rows_process' => [], 
                'rows_done' => [], 
                'bookings' =>$bookings, 
                'bookings_all' => [],
                'attribute' =>  $attribute]);
        } elseif ($request->get('type') =='dl'){
            $bookings_luar = $this->ServiceBooking
                ->whereIn('service_type_id', [1,2])
                ->whereNotNull('booking_no')
                ->where('requested', false)
                ->where('lokasi_pengujian', 'luar')
                // ->where('est_arrival_date', date('Y-m-d'))
                ->orderBy('id')
                ->get();
            
                /*
            $rows_item_luar = ServiceRequest::where('status_id', 13)
                ->where('lokasi_pengujian', 'luar')
                //->where('payment_status_id', 5)
                ->where('total_inspection_confirm', 0)
                ->where(function($query)
                    {
                        $query->where('payment_status_id', 5)
                        ->orWhereNull('payment_status_id');
                    })
                ->get();
                */
        
                /*
            $rows_penagihan_luar = ServiceRequest::with(['spuhDoc'])
                ->whereIn('status_id', [5,15,18,12,13])
                ->where('lokasi_pengujian', 'luar')
                //->where('payment_status_id', 5)
                ->where(function($query)
                    {
                        $query->where(function($q1) {
                            $q1->where('payment_status_id', 5);
                                //->orWhereNull('payment_status_id');
                        })->orWhere(function($q2) {
                            $q2->where('spuh_payment_status_id', 5);
                                //->orWhereNull('spuh_payment_status_id');
                        });
                        
                    })
                ->get();

            $rows_validasi_luar = ServiceRequest::with(['spuhDoc'])
                ->whereIn('status_id', [12,13,15,20])
                ->where('lokasi_pengujian', 'luar')
                //->where('payment_status_id', 7)
                ->where(function($query)
                    {
                        $query->where(function($q1) {
                            $q1->where('payment_status_id', 7)
                                ->whereNull('payment_valid_date');
                                //->orWhereNull('payment_status_id');
                        })->orWhere(function($q2) {
                            $q2->where('spuh_payment_status_id', 7)
                                ->whereNull('spuh_payment_valid_date');
                                //->orWhereNull('spuh_payment_status_id');
                        });
                    })
                ->get();
                */

            $rows_pendaftaran_luar = ServiceRequest::where('status_id', 1)
                ->where('lokasi_pengujian', 'luar')
                ->orderBy('received_date','desc')->get();

            return view('dinasluaruut.index', compact(
                'rows_pendaftaran_luar',
                //'rows_item_luar',
                //'rows_penagihan_luar',
                //'rows_validasi_luar',
                'bookings_luar',
                'attribute'));
        }
    }

    public function index_kirim(Request $req){
        $rows_kirim = $rows_done = ServiceOrders::whereHas('ServiceRequestItem', function($query) 
        {
            $query->where("status_id", 8);
        })->with([
            'serviceRequest' => function ($query)
            {
                $query->where('status_id', 8)
                    ->orderBy('received_date', 'desc');
            }
        ])
            ->orderBy('id','asc')->get();
            
        return $rows_kirim;
    }

    public function index_penagihan(){
        $rows_penagihan = ServiceRequest::whereIn('status_id', [5,6])
            ->where('lokasi_pengujian', 'dalam')
            ->orderBy('received_date','desc')->get();

        return $rows_penagihan;
    }

    public function index_validasi(){
        $rows_validasi = ServiceRequest::where('status_id', 7)
            ->where('lokasi_pengujian', 'dalam')
            ->orderBy('received_date','desc')->get();
        return $rows_validasi;
    }

    public function index_process(Request $request){
        // Page Length
        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;
        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';

        $qparams = '';
        if($request->has('search')){
            $sv =strtolower($request->get('search')['value']);
            $qparams = $sv;
        }

        $rows = ServiceRequestItem::with(['uuts',
            'serviceRequest' => function ($query) use($qparams)
            {
                $query->whereRaw("lower(no_register) like '%".$qparams."%'");
                $query->orderBy('received_date', 'desc');
            },'serviceRequest.requestor','status'
        ])
        ->join('service_requests', 'service_requests.id','=','service_request_items.service_request_id')
        ->select('service_request_items.*')
        ->whereIn('service_request_items.status_id', [9,10,11,12,13,15]);
        if($qparams != '' || null != $qparams){
            $rows->whereRaw("lower(service_request_items.no_order) like '%".$qparams."%'")
            ->orWhereRaw("lower(service_requests.no_register) like '%".$qparams."%'");
        }

        $query = $rows->orderBy('service_request_items.id',$orderBy);
        $recordsFiltered = $recordsTotal = $query->count();
        $data = $rows->skip($skip)->take($pageLength)->get();
        
        return response()->json(["draw"=> $request->draw, "recordsTotal"=> $recordsTotal, "recordsFiltered" => $recordsFiltered, 'data' => $data], 200);

    }

    public function index_done(Request $request){
        // Page Length
        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;
        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';

        $qparams = '';
        if($request->has('search')){
            $sv =strtolower($request->get('search')['value']);
            $qparams = $sv;
        }

        $callback = function($query) use ($qparams){
            if($qparams != '' || null != $qparams){
                $query->whereRaw("lower(no_order) like '%".$qparams."%'");    
            }
            $query->whereIn('status_id', [14,16]);
        };

        $rows = ServiceOrders::with('ServiceRequestItem') ->whereHas('ServiceRequestItem', $callback)
                            ->with('ServiceRequest')->with('ServiceRequest.requestor')->with('ServiceRequest.status');
        
        $query = $rows->orderBy('id',$orderBy);
        $recordsFiltered = $recordsTotal = $query->count();
        $data = $rows->skip($skip)->take($pageLength)->get();
        
        return response()->json(["draw"=> $request->draw, "recordsTotal"=> $recordsTotal, "recordsFiltered" => $recordsFiltered, 'data' => $data], 200);
    }

    public function index_bookigs(Request $request){
        $bookings = $this->ServiceBooking
            ->whereIn('service_type_id', [1,2])
            ->whereNotNull('booking_no')
            ->where('requested', false)
            ->where('lokasi_pengujian', 'dalam')
            ->where('est_arrival_date', date('Y-m-d'))
            ->orderBy('id')
            ->get();

        return $bookings;
    }

    public function index_all(Request $request){
        // Page Length
        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;
        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';

        $search = $request->get('search')['value'] ? $request->get('search')['value'] : '';

        $fc = function($query) use ($search){
            if('' != $search || null != $search){
                $query->whereRaw('1=1')
                    ->whereRaw("lower(full_name) like '%".$search."%'");
            }
        };
        
        $rows = $this->ServiceBooking::whereHas('Pic',$fc )
                ->with(['Pic' => $fc])
            ->whereIn('service_type_id', [1,2])
            ->whereNotNull('booking_no')
            ->whereNotNull('uut_owner_id')
            ->whereNotNull('pic_id')
            ->where('lokasi_pengujian', 'dalam');
            if('' != $search || null != $search){
                $rows->whereRaw("lower(label_sertifikat) like '%".$search."%'");
            }

        $query = $rows->orderBy('id',$orderBy);
        $recordsFiltered = $recordsTotal = $query->count();
        $data = $rows->skip($skip)->take($pageLength)->get();
        
        return response()->json(["draw"=> $request->draw, "recordsTotal"=> $recordsTotal, "recordsFiltered" => $recordsFiltered, 'data' => $data], 200);
    }

    public function create()
    {
        $attribute = $this->MyProjects->setup("requestuut");
        $idTypes = $this->MasterDocTypes->dropdown();

        return view('requestuut.create', compact('attribute', 'idTypes'));
    }

    public function createbooking()
    {
        $attribute = $this->MyProjects->setup("requestuut");

        $bookings = $this->ServiceBooking
            ->whereIn('service_type_id', [4,5,6,7])
            ->whereNotNull('booking_no')
            ->where('requested', false)
            ->orderBy('id')->pluck('booking_no', 'id');
        return view('requestuut.create_booking', compact('bookings','attribute'));
    }

    public function simpan(Request $request)
    {
        $response["status"] = false;

        $rules['booking_id'] = ['required'];
        $rules['pic_name'] = ['required'];
        $rules['pic_phone_no'] = ['required'];
        $rules['pic_email'] = ['required',"email"];
        $rules['id_type_id'] = ['required'];
        $rules['pic_id_no'] = ['required'];
        $rules['jenis_layanan'] = ['required'];

        if($request->for_sertifikat=="1")
        {
            $rules['label_sertifikat'] = ['required'];
            $rules['addr_sertifikat'] = ['required'];
        }

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        $no_register = MasterDocNumber::where("init","no_register_uut")->value("param_value");
        $no_order = MasterDocNumber::where("init","no_order_uut")->value("param_value");

        if ($validation->passes())
        {
            $alat = 0;
            $noreg_num = intval($no_register)+1;
            $noreg = sprintf("%04d",$noreg_num).date("-m-Y");


            $data = [];
            $keys = array_keys($rules);
            foreach($request->all() as $k=>$v)
            {
                if(in_array($k,$keys))
                {
                    $data[$k] = $request->get($k);
                }
            }

            $noorder_num = intval($no_order)+1;
            $noorder = date("y-").$booking->service_type_id.'-'.sprintf("%04d",$noorder_num)."-";

            $data["no_register"] = $noreg;
            $data["created_by"] = Auth::id();
            $data["pic_id"] = Auth::id();

            $data["status_id"] = 1;

            $data["received_date"] = date("Y-m-d");
            $data["estimated_date"] = date("Y-m-d");

            $serviceRequestID = ServiceRequest::insertGetId($data);

            $noorder = $noorder.sprintf("%03d",$alat);

            ServiceRequest::whereId($serviceRequestID)->update(["no_order"=>$noorder]);

            MasterDocNumber::where("init","no_register_uut")->update(["param_value"=>$noreg_num]);

            MasterDocNumber::where("init","no_order_uut")->update(["param_value"=>$noorder_num]);

            $response["id"] = $serviceRequestID;
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";

        }

        return response($response);
    }

    public function simpanbooking(Request $request)
    {
        $response["status"] = false;
        $rules['booking_id'] = ['required'];
        // $rules['received_date'] = ['required','date'];
        // $rules['estimate_date'] = ['required','date','after:received_date'];


        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();
        if ($validation->passes())
        {

            $data["booking_id"] = $request->get("booking_id");
            $booking = $this->ServiceBooking->find($data["booking_id"]);
            
            $serviceType = MasterServiceType::find($booking->service_type_id);

            $no_register = $serviceType->last_register_no;
            // $no_order = $serviceType->last_order_no;
            $prefix = $serviceType->prefix;

            $alat = 0;
            $noreg_num = intval($no_register)+1;
            $noreg = $prefix."-".sprintf("%04d",$noreg_num).date("-m-Y");
            $data = [];
            $keys = array_keys($rules);
            foreach($request->all() as $k=>$v)
            {
                if(in_array($k,$keys))
                {
                    $data[$k] = $request->get($k);
                }
            }

            // $noorder_num = intval($no_order)+1;
            // $noorder = $prefix.'-'.date("y-").sprintf("%04d",$noorder_num)."-";
            
            $data["requestor_id"] = $booking->Pic->id;
            
            if ($booking->lokasi_pengujian == 'dalam') {
                $data["received_date"] = date("Y-m-d");
                $data["estimated_date"] = date("Y-m-d");
            } else {
                $data["scheduled_test_date_from"] = $booking->est_schedule_date_from;
                $data["scheduled_test_date_to"] = $booking->est_schedule_date_to;

                $data["received_date"] = $data["scheduled_test_date_from"];
                $data["estimated_date"] = $data["scheduled_test_date_to"];
            }

            $received_date = date("Y-m-d");
            $estimated_date = date("Y-m-d");

            //$data["total_price"] = floatval(str_replace(",", "", $data["total"]));
            $data["no_register"] = $noreg;
            $data["no_order"] = $booking->lokasi_pengujian == 'dalam' ? $noreg : null;
            $data["created_by"] = Auth::id();
            $data["pic_id_no"] = Auth::id();

            $data["booking_no"] = $booking->booking_no;
            $data["label_sertifikat"] = $booking->label_sertifikat;
            $data["addr_sertifikat"] = $booking->addr_sertifikat;
            $data["for_sertifikat"] = $booking->for_sertifikat;
            // $data["uttp_owner_id"] = $booking->uttp_owner_id;
            $data["uut_owner_id"] = $booking->uut_owner_id;

            $data["total_price"] = $booking->est_total_price;
            $data["jenis_layanan"] = $booking->jenis_layanan;
            
            $data["service_type_id"] = $booking->service_type_id;
            $data["lokasi_pengujian"] = $booking->lokasi_pengujian;

            

            $data["inspection_loc"] = $booking->inspection_loc;
            $data["inspection_prov_id"] = $booking->inspection_prov_id;
            $data["inspection_kabkot_id"] = $booking->inspection_kabkot_id;

            $data["no_surat_permohonan"] = $booking->no_surat_permohonan;
            $data["tgl_surat_permohonan"] = $booking->tgl_surat_permohonan;
            $data["file_surat_permohonan"] = $booking->file_surat_permohonan;
            $data["path_surat_permohonan"] = $booking->path_surat_permohonan;

            $data["keuangan_perusahaan"] = $booking->keuangan_perusahaan;
            $data["keuangan_pic"] = $booking->keuangan_pic;
            $data["keuangan_jabatan"] = $booking->keuangan_jabatan;
            $data["keuangan_hp"] = $booking->keuangan_hp;

            $data["status_id"] = 1;

            $serviceRequestID = ServiceRequest::insertGetId($data);
            //Cek booking was created or not
            $bookingID = $request->get("booking_id");
            $cekRequest = ServiceRequest::where('booking_id', $bookingID)->get();
            
            if($cekRequest){
                try{
                    $reqIDs = array();
                    $prevReq = ServiceRequest::where('booking_id',$bookingID)->get();
                    foreach($prevReq as $r){
                        array_push($reqIDs, $r->id);
                        $prevItems = ServiceRequestItem::find($r->id);
                        if($prevItems){
                            $prevItems->delete();
                        }
                    }
                    ServiceRequest::whereIn("id",$reqIDs)->delete();
                }catch(Exception $e){
                    return response()->json([
                        'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                        'message' => $e->getMessage()
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }
                $serviceRequestID = ServiceRequest::insertGetId($data);
            }
            if(isset($serviceRequestID) )
            {
                $booking->requested = true;
                $booking->update();

                $labs = [];
                //Make Sure items of the Request is Empty
                $prevItems = ServiceRequestItem::where('service_request_id',$serviceRequestID)->get();
                if($prevItems){
                    try{
                        ServiceRequestItem::where('service_request_id', $serviceRequestID)->delete();
                    }catch(Exception $e){
                        return response()->json([
                            'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                            'message' => $e->getMessage()
                        ], Response::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }
                foreach ($booking->items as $item) {
                    $dataItem["service_request_id"] = $serviceRequestID;
                    $dataItem["quantity"] = $item->quantity;
                    $dataItem["subtotal"] = $item->est_subtotal;
                    $dataItem["uut_id"] = $item->uut_id;
                    $dataItem["location"] = $item->location;

                    $dataItem["file_application_letter"] = $item->file_application_letter;
                    $dataItem["file_last_certificate"] = $item->file_last_certificate;
                    $dataItem["file_manual_book"] = $item->file_manual_book;
                    $dataItem["file_type_approval_certificate"] = $item->file_type_approval_certificate;
                    $dataItem["file_calibration_manual"] = $item->file_calibration_manual;
                    $dataItem["keterangan"] = $item->keterangan;

                    $dataItem["path_application_letter"] = $item->path_application_letter;
                    $dataItem["path_last_certificate"] = $item->path_last_certificate;
                    $dataItem["path_manual_book"] = $item->path_manual_book;
                    $dataItem["path_type_approval_certificate"] = $item->path_type_approval_certificate;
                    $dataItem["path_calibration_manual"] = $item->path_calibration_manual;

                    $dataItem["reference_no"] = $item->reference_no;
                    $dataItem["reference_date"] = $item->reference_date;

                    $dataItem["status_id"] = 1;
                    $dataItem["standard_id"] = $request->get("standard_id");

                    $dataItem["location"] = $item->location;
                    $dataItem["location_prov_id"] = $item->location_prov_id;
                    $dataItem["location_kabkot_id"] = $item->location_kabkot_id;

                    $dataItem["location_lat"] = $item->location_lat;
                    $dataItem["location_long"] = $item->location_long;

                    $serviceItemID = ServiceRequestItem::insertGetId($dataItem);

                    foreach ($item->inspections as $inspection) {
                        $alat++;

                        $dataInspection["request_item_id"] = $serviceItemID;
                        $dataInspection["quantity"] = $inspection->quantity;
                        $dataInspection["price"] = $inspection->price;
                        $dataInspection["inspection_price_id"] = $inspection->inspection_price_id;

                        $dataInspection["status_id"] = 1;

                        $inspectionPrice = StandardInspectionPrice::find($inspection->inspection_price_id);
                        array_push($labs, $item->uuts->stdtype->lab_id);
                    
                        $serviceInspectionID = ServiceRequestItemInspection::insertGetId($dataInspection);

                    }

                    if ($item->perlengkapans != null) {
                        foreach ($item->perlengkapans as $perlengkapan) {
                            $dataPerlengkapan["request_item_id"] = $serviceItemID;
                            $dataPerlengkapan["uttp_id"] = $perlengkapan->uttp_id;

                            $perlengkapanID = ServiceRequestUutItemTTUPerlengkapan::insertGetId($dataPerlengkapan);
                        }
                    }
                    $history = new HistoryUut();
                    $history->request_status_id = 1;
                    $history->request_id = $serviceRequestID;
                    $history->request_item_id = $serviceItemID;
                    $history->user_id = Auth::id();
                    $history->save();
                }

                // $noorder = $noorder.sprintf("%03d",$alat);

                // $sla = $this->MasterInstalasi->whereIn('id', $labs)->max('sla_day');
                $sla = $this->MasterLaboratory->whereIn('id', $labs)->max('sla_day');
                
                $formatted_date = new \DateTime( $estimated_date );
                $date_timestamp = $formatted_date->getTimestamp();
                for ($i=0; $i < $sla; $i++) { 
                    $nextDay = date('w', strtotime('+1day', $date_timestamp) );
                    if( $nextDay == 0 || $nextDay == 6 ) { $i--; }
                    $date_timestamp = strtotime("+1day", $date_timestamp);
                }
                $formatted_date->setTimestamp($date_timestamp);
                $estimated_date = $formatted_date->format("Y-m-d");

                $holidays = Holiday::whereBetween("holiday_date", [$received_date, $estimated_date])->count();
                $formatted_date = new \DateTime( $estimated_date );
                $date_timestamp = $formatted_date->getTimestamp();
                for ($i=0; $i < $holidays; $i++) { 
                    $nextDay = date('w', strtotime('+1day', $date_timestamp) );
                    if( $nextDay == 0 || $nextDay == 6 ) { $i--; }
                    $date_timestamp = strtotime("+1day", $date_timestamp);
                }
                $formatted_date->setTimestamp($date_timestamp);
                $estimated_date = $formatted_date->format("Y-m-d");

                if ($booking->lokasi_pengujian == 'dalam') {
                    ServiceRequest::whereId($serviceRequestID)->update([
                            // "no_order"=>$noorder, 
                            "estimated_date"=>$estimated_date
                    ]);
                } else {
                    // ServiceRequest::whereId($serviceRequestID)->update([
                    //     "no_order"=>$noorder
                    // ]);
                }

                //MasterDocNumber::where("init","no_register_uttp")->update(["param_value"=>$noreg_num]);
                //MasterDocNumber::where("init","no_order_uttp")->update(["param_value"=>$noorder_num]);

                $serviceType->last_register_no = $noreg_num;
                // $serviceType->last_order_no = $noorder_num;
                $serviceType->save();

                $response["id"] = $serviceRequestID;
                $response["status"] = true;
                $response["messages"] = "Data berhasil disimpan";
            }
            else
            {
                $response["messages"]["standard_code"] = 'Minimal Satu Standard Tool wajib diisi';
            }
        }

        return response($response);
    }

    public function edit($id)
    {
        // $sla = $this->MasterInstalasi->whereIn('id', $listInstalasiId)->max('sla_day');
        $sla = $this->MasterLaboratory->whereIn('id', $labs)->max('sla_day');
        $attribute = $this->MyProjects->setup("requestuut");
        $request = $this->ServiceRequest
            ->with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = $this->Customer->find($request->requestor_id);
        return view('requestuut.edit_booking', compact('request', 'booking', 'attribute'));
    }

    public function editbooking($id)
    {
        $attribute = $this->MyProjects->setup("requestuut");

        $request = $this->ServiceRequest->with(['items', 'items.inspections'])
            ->find($id);
        
        // $sla = $this->MasterInstalasi->whereIn('id', $listInstalasiId)->max('sla_day');
        
        $standards = Standard ::join('standard_tool_types','standards.standard_name_type_id','=','standard_tool_types.id')->get()->pluck('deskripsi_alat','id');
        
        $booking = $this->ServiceBooking->find($request->booking_id);
        $requestor = $this->Customer->find($request->requestor_id);
        $perlengkapans=[];
        $uuts = Uut ::with('stdtype')->where('owner_id',$request->requestor_id)->get();
        
        return view('requestuut.edit_booking', compact(
            ['request', 'booking', 'requestor', 'attribute', 'perlengkapans','uuts','standards']
        ));
    }

    public function simpaneditbooking($id, Request $request)
    {
        $response["status"] = false;

        $requestEntity = $this->ServiceRequest->find($id);
        

        $rules = [];

        //$rules['booking_id'] = ['required'];
        //$rules['received_date'] = ['required','date'];
        //$rules['estimated_date'] = ['required','date','after:received_date'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $alat = 0;
            
            $data = [];
            $keys = array_keys($rules);
            foreach($request->all() as $k=>$v)
            {
                if(in_array($k,$keys))
                {
                    $data[$k] = $request->get($k);
                }
            }
            
            //$data["received_date"] = date("Y-m-d", strtotime($data["received_date"]));

            //$data["total_price"] = floatval(str_replace(",", "", $data["total"]));
            $data["updated_by"] = Auth::id();
            
            if($request->get("lokasi_pengujian") == '' || $request->get("lokasi_pengujian") ==null){
                $data["lokasi_pengujian"] = 'dalam';    
            }else{
                $data["lokasi_pengujian"] = $request->get("lokasi_pengujian");
            }
            
            $owner = DB::table('uut_owners')->where('id', $request->get("uut_owner_id"))->first();
            
            $data["uut_owner_id"] = $owner->id;
            // $data["label_sertifikat"] = $owner->nama;
            // $data["addr_sertifikat"] = $owner->alamat;
            // $data["label_sertifikat"] = $request->get("label_sertifikat");
            // $data["addr_sertifikat"] = $request->get("addr_sertifikat");
            
            $requestEntity->update($data);
            

            $response["id"] = $requestEntity->id;
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return response($response);
    }

    public function submitbooking($id, Request $request)
    {
        $response["status"] = false;
        
        $requestEntity = $this->ServiceRequest->find($id);
        $svcRequest = ServiceRequest::find($id);
        
        $state = false;
        $items = $requestEntity->items;
        foreach($items as $item){
            if(count($item->inspections) > 0){
                $state = true;
            }
        }

        $rules = [];
        if ($requestEntity->lokasi_pengujian == 'dalam') {
            $rules['received_date'] = ['required','date'];
            $rules['estimated_date'] = ['required','date','after:received_date'];
        }

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            // MASIH BELUM PAS
            $totalInspectionConfirm = $requestEntity->items->reduce(function ($carry, $item) {
                return $carry + $item->inspections->count();
            });

            // if ($requestEntity->total_price == 0 && $requestEntity->lokasi_pengujian == 'dalam') {
            if($state==false && $requestEntity->lokasi_pengujian == 'dalam'){
                $response["messages"] = "Data pemeriksaan belum lengkap.";
            } else {

                $alat = 0;
            
                $data = [];
                $keys = array_keys($rules);
                foreach($request->all() as $k=>$v)
                {
                    if(in_array($k,$keys))
                    {
                        $data[$k] = $request->get($k);
                    }
                }
                
                $lokasi = $requestEntity->lokasi_pengujian;
                if ($requestEntity->lokasi_pengujian == 'dalam') {
                    $data["received_date"] = date("Y-m-d", strtotime($data["received_date"]));
                    $data["estimated_date"] = date("Y-m-d", strtotime($data["estimated_date"]));
                }

                //$data["total_price"] = floatval(str_replace(",", "", $data["total"]));
                $data["updated_by"] = Auth::id();
                
                // Change first data state
                /*
                    $data["label_sertifikat"] = $request->get("label_sertifikat");
                    $data["addr_sertifikat"] = $request->get("addr_sertifikat");
                **/
                //$data["lokasi_pengujian"] = $request->get("lokasi_pengujian");
                $owner = DB::table('uut_owners')->where('id', $request->get("uut_owner_id"))->first();
                $data["uut_owner_id"] = $owner->id;
                /*
                $data["label_sertifikat"] = $owner->nama;
                $data["addr_sertifikat"] = $owner->alamat;
                **/

                $data["total_inspection_confirm"] = $totalInspectionConfirm;
                
                $status = 2;
                if ($requestEntity->lokasi_pengujian == 'luar') {
                    
                    if ($request->get('is_complete') == 'ya') {
                        $status = 3;
                    } else {
                        $status = 23;
                        $data['not_complete_notes'] = $request->get('not_complete_notes');
                    }

                    $rates = DB::select("select
                            srui.id,
                            srui.location_kabkot_id,
                            srui.location_prov_id,
                            s.luar,
                            s.dalam,
                            case
                                when srui.location_kabkot_id = " .env('KABKOT_DALAM'). " then s.dalam
                                else s.luar
                            end rate
                        from
                            service_request_items srui
                        inner join master_provinsi mp on
                            srui.location_prov_id = mp.id
                        inner join sbm s on
                            mp.id = s.provinsi_id
                        where
                            srui.service_request_id = " .$requestEntity->id. "
                        order by
                            case
                                when srui.location_kabkot_id = " .env("KABKOT_DALAM"). " then s.dalam
                                else s.luar
                            end desc,
                            srui.location_prov_id asc,
                            srui.id asc");

                    //dd([env('KABKOT_DALAM'), $requestEntity->id, $rates]);

                    $data['inspection_prov_id'] = $rates[0]->location_prov_id;
				    $data['inspection_kabkot_id'] = $rates[0]->location_kabkot_id;
                } 
                $data["status_id"] = $status;

                $items = ServiceRequestItem::where("service_request_id", $id)->get();
                foreach($items as $item) {
                    $history = new HistoryUut();
                    $history->request_status_id = $status;
                    $history->request_id = $id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = $status;
                    $item->save();
                }
                
                $requestEntity->update($data);
                $customerEmail = $requestEntity->requestor->email;
                //Mail::to($customerEmail)->send(new BookingConfirmation_uut($requestEntity));
                ProcessEmailJob::dispatch($customerEmail, new BookingConfirmation_uut($requestEntity))->onQueue('emails');

                $response["id"] = $requestEntity->id;
                $response["status"] = true;
                $response["messages"] = "Data berhasil disimpan";
                $response["lokasi_pengujian"] = $lokasi;
                
            }

            
        }

        return response($response);
    }

    public function pdf($id)
    {
        $row = ServiceRequest::find($id);
        
        if ($row->lokasi_pengujian == 'luar') {
            if ($row->spuh_no == null) {
                $no_parts = explode("-", $row->no_register);
                $row->spuh_no = sprintf("%04d",$no_parts[1]).'/SPUH/'.date("m/Y");
            }
            if ($row->spuh_billing_date == null) {
                $row->spuh_billing_date = date("Y-m-d");
            }
        }
        
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUttpInsituDoc::where('request_id', $id)->get();

        return view('requestuut.uut-pdf',compact(['row', 'staffes', 'docs']));
    }

    public function kuitansi($id)
    {
        $row = ServiceRequest::find($id);
        $terbilang = $this->convert($row->total_price);
        $terbilangSPUH = $this->convert($row->spuh_price);
        // dd($terbilang);
        return view('requestuut.kuitansi_pdf',compact('row', 'terbilang', 'terbilangSPUH'));
    }

    private function convert($number)
    {
        $number = floatval($number);
        $base    = array('nol', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
        $numeric = array('1000000000000000', '1000000000000', '1000000000000', 1000000000, 1000000, 1000, 100, 10, 1);
        $unit    = array('kuadriliun', 'triliun', 'biliun', 'milyar', 'juta', 'ribu', 'ratus', 'puluh', '');
        $str     = null;
        $i = 0;
        if ($number == 0) {
            $str = 'nol';
        } else {
            while ($number != 0) {
                $count = (int)($number / $numeric[$i]);
                if ($count >= 10) {
                    $str .= static::convert($count) . ' ' . $unit[$i] . ' ';
                } elseif ($count > 0 && $count < 10) {
                    $str .= $base[$count] . ' ' . $unit[$i] . ' ';
                }
                $number -= $numeric[$i] * $count;
                $i++;
            }
            $str = preg_replace('/satu puluh (\w+)/i', '\1 belas', $str);
            $str = preg_replace('/satu (ribu|ratus|puluh|belas)/', 'se\1', $str);
            $str = preg_replace('/\s{2,}/', ' ', trim($str));
        }
        return $str;
    }

    public function buktiorder($id)
    {
        $row = ServiceRequest::find($id);
        return view('requestuut.order_pdf',compact('row'));
    }

    public function payment($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $serviceRequest = ServiceRequest::find($id);

        $state = false;
        $items = $serviceRequest->items;
        foreach($items as $item){
            if(count($item->inspections) > 0){
                $state = true;
            }
        }

        if ($serviceRequest->lokasi_pengujian == 'luar') {
            if ($serviceRequest->spuh_no == null) {
                $no_parts = explode("-", $serviceRequest->no_register);
                $serviceRequest->spuh_no = sprintf("%04d",$no_parts[1]).'/SPUH/'.date("m/Y");
            }
            if ($serviceRequest->spuh_billing_date == null) {
                $serviceRequest->spuh_billing_date = date("Y-m-d");
            }
        }
        if($serviceRequest->total_price <=0 && $state == true){
            ServiceRequest::whereId($id)
            ->update([
                //"payment_date"=>$request->payment_date,
                //"payment_code"=>$request->payment_code,
                "billing_code"=>'012012012',
                "billing_to_date"=>date("Y-m-d"),
                "status_id"=>8,
                "spuh_no" => '012012012',
                "spuh_billing_date"=>date("Y-m-d"),
            ]);

            ServiceRequestItem::where("service_request_id", $id)
            ->update([
                "status_id"=>8,
            ]);

            $listItems = ServiceRequestItem::where("service_request_id", $id)->pluck('id');
            ServiceRequestItemInspection::whereIn("service_request_item_id", $listItems)->update([
                "status_id"=>8,
            ]);

            //proses order dibuat
            $items = ServiceRequestItem::where("service_request_id",$serviceRequest->id)
            -> orderBy('id','desc')
            ->get();

            $serviceType = MasterServiceType :: find($serviceRequest->service_type_id);

            $no_order = $serviceType->last_order_no;
            $prefix = $serviceType->prefix;

            $alat = count($items);

            $noorder_num = intval($no_order)+1;
            $noorder = $prefix.'-'.date('y-').sprintf("%04d",$noorder_num)."-";

            $no = 0;
            foreach($items as $item){
                $no = $no+1;
                $noorder_alat = $noorder.sprintf("%03d",$no);
                // make an order
                ServiceOrders :: insert([
                    "laboratory_id" => $item->uuts->stdtype->lab->id,
                    "service_request_id" => $item->service_request_id,
                    "service_request_item_id" => $item->id,
                    //"lab_staff"=>Auth::id(),
                    //"staff_entry_datein"=>date("Y-m-d"),

                    'tool_type_id'			=> $item->uuts->type_id ? $item->uuts->type_id :null,
                    'tool_serial_no'		=> $item->uuts->serial_no ? $item->uuts->serial_no : null,
                    'tool_brand'			=> $item->uuts->tool_brand ? $item->uuts->tool_brand:null,
                    'tool_model'			=> $item->uuts->tool_model ? $item->uuts->tool_model : null,
                    'tool_type'				=> $item->uuts->tool_type ? $item->uuts->tool_type:null,
                    'tool_capacity'			=> $item->uuts->tool_capacity ? $item->uuts->tool_capacity : '',
                    'tool_capacity_unit'	=> $item->uuts->tool_capacity_unit ? $item->uuts->tool_capacity_unit :null ,
                    'tool_factory'			=> $item->uuts->tool_factory ? $item->uuts->tool_factory :null,
                    'tool_factory_address'	=> $item->uuts->tool_factory_address ? $item->uuts->tool_factory_address:null,
                    'tool_made_in'			=> $item->uuts->tool_made_in ? $item->uuts->tool_made_in :null,
                    'tool_made_in_id'		=> $item->uuts->tool_made_in_id ? $item->uuts->tool_made_in_id : null ,
                    'tool_owner_id'			=> $item->uuts->owner_id ? $item->uuts->owner_id : null,
                    'tool_media'			=> $item->uuts->tool_media ? $item->uuts->tool_media : null,
                    'tool_name'				=> $item->uuts->tool_name ? $item->uuts->tool_name : null,
                    'tool_class'		    => $item->uuts->class ? $item->uuts->class : null,
                    'tool_jumlah'		    => $item->uuts->jumlah ? $item->uuts->jumlah : null,
                    'tool_dayabaca'		    => $item->uuts->tool_dayabaca ? $item->uuts->tool_dayabaca :null,
                    'tool_dayabaca_unit'    => $item->uuts->tool_dayabaca_unit ? $item->uuts->tool_dayabaca_unit : null,

                    'uut_id'                => $item->uut_id,
                ]);

                $process = ServiceRequestItem::where("id",$item-> id)
                ->update([
                    "status_id" => "8",
                    "no_order" => $noorder_alat,
                    "order_at" => date("Y-m-d H:i:s"),
                ]);

                $history = new HistoryUut();
                $history->request_status_id = 8;
                $history->request_id = $id;
                $history->request_item_id = $item->id;
                $history->user_id = Auth::id();
                $history->save();
                
            }
            
            $serviceType -> last_order_no = $noorder_num;
            $serviceType->save();

            $svcRequest = ServiceRequest::find($id);

            $customerEmail = $svcRequest->requestor->email;
            //Mail::to($customerEmail)->send(new Receipt_uut($svcRequest));
            ProcessEmailJob::dispatch($customerEmail, new Receipt_uut($svcRequest))->onQueue('emails');

            return Redirect::route('requestuut');
        }

        return view('requestuut.payment-uut',compact('id','serviceRequest', "attribute"));
    }

    public function paymentsave(Request $request)
    {
        $rules = [];
        $serviceRequest = ServiceRequest::find($request->id);
        if ($serviceRequest->total_price > 0) {
            $rules = [
                //"payment_date"=>'required',
                //"payment_code"=>'required'
                "billing_code"=>'required',
                "billing_to_date"=>'required',
            ];
        }

        $validation = Validator::make($request->all(),$rules);

        if ($validation->passes())
        {
            ServiceRequest::whereId($request->id)
            ->update([
                //"payment_date"=>$request->payment_date,
                //"payment_code"=>$request->payment_code,
                "billing_code"=>$request->billing_code,
                "billing_to_date"=>date("Y-m-d", strtotime($request->billing_to_date)),
                "status_id"=>6,
                "spuh_no" => $request->spuh_no,
                "spuh_billing_date"=>date("Y-m-d"),
            ]);

            ServiceRequestItem::where("service_request_id", $request->id)
            ->update([
                "status_id"=>6,
            ]);

            $listItems = ServiceRequestItem::where("service_request_id", $request->id)->pluck('id');
            ServiceRequestItemInspection::whereIn("service_request_item_id", $listItems)->update([
                "status_id"=>6,
            ]);

            if ($serviceRequest->lokasi_pengujian == 'luar') {
                $doc = ServiceRequestUttpInsituDoc::find($serviceRequest->spuh_doc_id);
                $doc->billing_date = date("Y-m-d");
                $doc->save();
            }

            $svcRequest = ServiceRequest::find($request->id);

            $customerEmail = $svcRequest->requestor->email;
            //Mail::to($customerEmail)->send(new Invoice_uut($svcRequest));
            ProcessEmailJob::dispatch($customerEmail, new Invoice_uut($svcRequest))->onQueue('emails');

            return response([true,"success"]);
        }
        else
        {
            return response([false,$validation->messages()]);
        }
    }

    public function valid($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $request = ServiceRequest::find($id);
        return view('requestuut.valid',compact('id',"request","attribute"));
    }

    public function validsave(Request $request)
    {
        $req = ServiceRequest::find($request->id);

        if ($req->status_id != 7) {
            return response([false,"Permohonan sudah diproses validasi sebelumnya. Silakah refresh halaman."]);
        }

        if ($req->lokasi_pengujian == 'dalam') {
            $next = 8; // pengiriman alat
        } else {
            $next = 11; // keberangkatan
        }
        $s = ServiceRequest::whereId($request->id)
        ->update([
            "status_id"=>$next,
            // "cetak_tag_created" =>date('Y-m-d H:i:s'), 
        ]);
        // ServiceRequestItem::where("service_request_id", $request->id)
        // ->update([
        //     "status_id"=>$next,
        // ]);

        $listItems = ServiceRequestItem::where("service_request_id", $request->id)->pluck('id');
        ServiceRequestItemInspection::whereIn("service_request_item_id", $listItems)->update([
            "status_id"=>$next,
        ]);

        $items = ServiceRequestItem::where("service_request_id",$request->id)
        -> orderBy('id','desc')
        ->get();

        $serviceType = MasterServiceType :: find($req->service_type_id);

        $no_order = $serviceType->last_order_no;
        $prefix = $serviceType->prefix;

        $alat = count($items);

        $noorder_num = intval($no_order)+1;
        $noorder = $prefix.'-'.date('y-').sprintf("%04d",$noorder_num)."-";

        $no = 0;
        foreach($items as $item){
            $no = $no+1;
            $noorder_alat = $noorder.sprintf("%03d",$no);
            // make an order
            $sr = ServiceOrders :: insert([
                // "location"              => $item->location,
                "laboratory_id"         => $item->uuts->stdtype->lab->id,
                "service_request_id"    => $item->service_request_id,
                "service_request_item_id" => $item->id,
                "staff_entry_datein"    =>date("Y-m-d"),
                'tool_type_id'			=> $item->uuts->type_id ? $item->uuts->type_id :null,
                'tool_serial_no'		=> $item->uuts->serial_no ? $item->uuts->serial_no : null,
                'tool_brand'			=> $item->uuts->tool_brand ? $item->uuts->tool_brand:null,
                'tool_model'			=> $item->uuts->tool_model ? $item->uuts->tool_model : null,
                'tool_type'				=> $item->uuts->tool_type ? $item->uuts->tool_type:null,
                'tool_capacity'			=> $item->uuts->tool_capacity ? $item->uuts->tool_capacity : '',
                'tool_capacity_unit'	=> $item->uuts->tool_capacity_unit ? $item->uuts->tool_capacity_unit :null ,
                'tool_capacity_min'	    => $item->uuts->tool_capacity_min ? $item->uuts->tool_capacity_min :null ,
                'tool_capacity_min_unit'=> $item->uuts->tool_capacity_min_unit ? $item->uuts->tool_capacity_min_unit :null ,
                'tool_factory'			=> $item->uuts->tool_factory ? $item->uuts->tool_factory :null,
                'tool_factory_address'	=> $item->uuts->tool_factory_address ? $item->uuts->tool_factory_address:null,
                'tool_made_in'			=> $item->uuts->tool_made_in ? $item->uuts->tool_made_in :null,
                'tool_made_in_id'		=> $item->uuts->tool_made_in_id ? $item->uuts->tool_made_in_id : null ,
                'tool_owner_id'			=> $item->uuts->owner_id ? $item->uuts->owner_id : null,
                'tool_media'			=> $item->uuts->tool_media ? $item->uuts->tool_media : null,
                'tool_name'				=> $item->uuts->tool_name ? $item->uuts->tool_name : null,
                'tool_class'		    => $item->uuts->class ? $item->uuts->class : null,
                'tool_jumlah'		    => $item->uuts->jumlah ? $item->uuts->jumlah : null,
                'tool_dayabaca'		    => $item->uuts->tool_dayabaca ? $item->uuts->tool_dayabaca :null,
                'tool_dayabaca_unit'    => $item->uuts->tool_dayabaca_unit ? $item->uuts->tool_dayabaca_unit : null,

                'uut_id'                => $item->uut_id,
            ]);

            $process = ServiceRequestItem::where("id",$item-> id)
            ->update([
                "status_id" => $next,
                "no_order" => $noorder_alat,
                "order_at" => date("Y-m-d H:i:s"),
                "order_at"=> $req->lokasi_pengujian == 'dalam' ? date("Y-m-d H:i:s") : null,
            ]);

            $history = new HistoryUut();
            $history->request_status_id = $next;
            $history->request_id = $request->id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();
            
        }
        
        $serviceType -> last_order_no = $noorder_num;
        $serviceType->save();

        $svcRequest = ServiceRequest::find($request->id);

        $customerEmail = $svcRequest->requestor->email;
        //Mail::to($customerEmail)->send(new Receipt_uut($svcRequest));
        ProcessEmailJob::dispatch($customerEmail, new Receipt_uut($svcRequest))->onQueue('emails');

        return response([true,"success"]);
    }

    public function novalidsave(Request $request)
    {
        ServiceRequest::whereId($request->id)
        ->update([
            "status_id"=>6,
            "payment_date"=>null,
        ]);

        ServiceRequestItem::where("service_request_id", $request->id)
        ->update([
            "status_id"=>6,
        ]);

        $listItems = ServiceRequestItem::where("service_request_id", $request->id)->pluck('id');
        ServiceRequestItemInspection::whereIn("service_request_item_id", $listItems)->update([
            "status_id"=>6,
        ]);

        return response([true,"success"]);
    }

    public function tag($id)
    {
        // $row = ServiceRequestItem::find($id);
        $row = ServiceOrders::find($id);
        $item = ServiceRequestItem::find($row->ServiceRequestItem->id);

        $lab = Auth::user()->laboratory_id;
        $lab_name = MasterLaboratory::find($lab);

        // foreach($row->items as $i){
            // foreach($i->inspections as $ins){
            //     foreach($ins->StandardInspectionPrice as $prices){
            //         foreach($prices->inspectionType as $type){
            //             dd($ins->StandardInspectionPrice->lab);//->toolType->measurementType->laboratory);
            //         }
            //     }
            // }
        // }


        if ($item->cetak_tag_created == null) {
            $item->cetak_tag_created = date('Y-m-d H:i:s');     
        } 
        $item->cetak_tag_updated = date('Y-m-d H:i:s');
        $item->save();

        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo'),
            'defaultMediaType' => 'print',
        ])
        ->setPaper([0, 0, 99.21, 198.43], 'landscape')
        //->setPaper('a4', 'portrait')
        ->loadview('requestuut.tag-uut',compact(['row', 'lab_name']));

        return $pdf->stream();

        //return view('requestuut.tag-uut',compact('row','lab_name'));
    }

    public function instalasi(Request $request)
    {
        /* dd($request->all());
        ServiceRequest::whereId($request->id)
        ->update([
            "status_id"=>9,
        ]);

        ServiceRequestItem::where("service_request_id", $request->id)
        ->update([
            "status_id"=>9,
        ]);

        $listItems = ServiceRequestItem::where("service_request_id", $request->id)->pluck('id');
        ServiceRequestItemInspection::whereIn("service_request_item_id", $listItems)->update([
            "status_id"=>9,
        ]); */

        $response["status"] = '';
        $response["messages"] = '';

        $item = ServiceRequestItem::where("id", $request->id)->first();

        if ($item != null && $item->cetak_tag_created != null) {
            $item->status_id = 9;
            $item->delivered_at = date("Y-m-d H:i:s");
            $item->save();

            $history = new HistoryUut();
            $history->request_status_id = 9;
            $history->request_id = $request->id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();
            
            ServiceRequestItemInspection::where("service_request_item_id", $item->id)->update([
                "status_id"=>9,
            ]);

            $items_count = DB::table('service_request_items')
                ->selectRaw('sum(case when status_id = 9 then 1 else 0 end) count_9, count(id) count_all')
                ->where('service_request_id', $item->service_request_id)->get();
            
            if ($items_count[0]->count_9 == $items_count[0]->count_all) {
                ServiceRequest::find($item->service_request_id)->update(['status_id'=>9]);
            }

            $response["status"] = true;
            $response["messages"] = "Alat telah dikirim";
        }
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        return response($response);

        // return Redirect::route('requestuut');
    }

    public function paymentcancel($id)
    {
        ServiceRequest::whereId($id)
        ->where("stat_service_request",1)
        ->update([
            "payment_date"=>null,
            "payment_code"=>null,
            "stat_service_request"=>0
        ]);

        return Redirect::route('request');
    }

    public function destroy($id)
    {
        $row = ServiceRequest::whereId($id)
        ->whereNull("payment_date")
        ->whereNull("payment_code")
        ->first();

        if($row)
        {
            $req = ServiceRequest::where('id',$id)->update(['status_id'=>21]);
            if($req){
                $items = ServiceRequestItem::where('service_request_id',$id)->get();//->update(['status_id'=>21]);
                foreach($items as $item){
                    $history = new HistoryUut();
                    $history->request_status_id = 21;
                    $history->request_id = $id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();
                }
                $response["messages"] = "Berhasil menghapus data";
                $response["status"] = true;
            }else{
                $response["messages"] = "Data gagal dihapus";
                $response["status"] = false;
            }

            return response($response);

            // DELETE CHILD BY TRIGGER
            // $rows = ServiceRequestItem::where("service_request_id",$id)->pluck("id");
            // ServiceRequestItem::where("service_request_id",$id)->delete();
            // ServiceRequestItemInspection::whereIn("service_request_item_id",$rows)->delete();
        }
        $route = route('requestuut#frontdesk_pendaftaran');
        return Redirect::to($route);
    }

    function edititem($id, Request $request)
    {
        $response["status"] = false;

        $requestItem = $this->ServiceRequestItem->find($id);

        $uutModel = new Uut();
        $uut = $uutModel->find($requestItem->uuts->id);

        // dd($uut);
        $rules['tool_brand'] = ['required'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $data["serial_no"] = $request->get("serial_no");
            $data["tool_name"] = $request->get("tool_name");
            $data["tool_brand"] = $request->get("tool_brand");
            $data["tool_model"] = $request->get("tool_model");
            $data["tool_type"] = $request->get("tool_type");
            $data["tool_made_in"] = $request->get("tool_made_in");
            $data["tool_capacity"] = $request->get("tool_capacity");
            $data["tool_factory"] = $request->get("tool_factory");
            $data["tool_factory_address"] = $request->get("tool_factory_address");
            $uut->update($data);
            $requestItem->where('id',$id)->update([
                'sla_overide' => $request->get("sla_overide"),
            ]);

            $response["id"] = $uut->id;
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return response($response);
    }

    // public function createbookinginspection($id)
    // {
    //     $attribute = $this->MyProjects->setup("requestuut");

    //     $item = $this->ServiceRequestItem
    //         ->with(['serviceRequest'])
    //         ->find($id);
    //     $booking_stat = $this->ServiceBooking->find($item->serviceRequest->booking_id);
    //     $labs = $this->MasterLaboratory->pluck('nama_lab','id');
    //     $labs->prepend("-- Pilih Lab Pengujian --", 0);
    //     $labs = $labs->all();
    //     $usertype = UutOwner::find($item->uuts->owner_id);
    //     $userUml = UserUutOwner::with(['customer'])->where('owner_id',$item->uuts->owner_id)->where('user_id',$item->ServiceRequest->requestor_id)->first();
        
    //     /*
    //     $prices = DB::table('uut_inspection_prices')
    //     ->join('uut_inspection_price_types','uut_inspection_price_types.inspection_price_id','=','uut_inspection_prices.id')
    //     ->where('uut_inspection_price_types.uut_type_id', $item->uuts->type_id)
    //     ->where('uut_inspection_prices.service_type_id', $item->uuts->type_id)
    //     ->where('uut_inspection_prices.id', $item->serviceRequest->service_type_id)
    //     ->pluck('uut_inspection_prices.inspection_type','uut_inspection_prices.id');
    //     $prices->prepend("-- Pilih jenis pemeriksaan/pengujian --", 0);
    //     */

    //     // if($usertype->customer != null){
    //     if($userUml->customer->uml_id != null || $userUml->customer->ktp ){
    //         $prices = DB::table('uut_inspection_prices')
    //             ->join('uut_inspection_price_types', 'uut_inspection_price_types.inspection_price_id', '=', 'uut_inspection_prices.id')
    //             ->where('uut_inspection_prices.user_type', 'UML')
    //             // ->where('uut_inspection_prices.service_type_id',2)
    //             //->where('uut_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
    //             ->where('uut_inspection_price_types.uut_type_id', $item->uuts->type_id)
    //             ->select('uut_inspection_prices.*')->get();
                 
    //     }else{
    //         $prices = DB::table('uut_inspection_prices')
    //             ->join('uut_inspection_price_types', 'uut_inspection_price_types.inspection_price_id', '=', 'uut_inspection_prices.id')
    //             // ->where('user_type','!=','UML')
    //             ->whereNull('user_type')
    //             //->where('uut_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
    //             ->where('uut_inspection_price_types.uut_type_id', $item->uuts->type_id)
    //             ->select('uut_inspection_prices.*')->get();
    //     }
        
    //     return view('requestuut.create_booking_inspection', compact('item', 'prices', 'attribute','labs','usertype'));
    // }

    public function createbookinginspection($id)
    {
        $attribute = $this->MyProjects->setup("requestuut");

        $item = $this->ServiceRequestItem
            ->with(['serviceRequest'])
            ->find($id);
        $booking_stat = $this->ServiceBooking->find($item->serviceRequest->booking_id);
        $labs = $this->MasterLaboratory->pluck('nama_lab','id');
        $labs->prepend("-- Pilih Lab Pengujian --", 0);
        $labs = $labs->all();
        $usertype = UutOwner::find($item->uuts->owner_id);
        $userUml = UserUutOwner::with(['customer'])->where('owner_id',$item->uuts->owner_id)->where('user_id',$item->ServiceRequest->requestor_id)->first();

        $prices = DB::table('uut_inspection_prices')
            ->join('uut_inspection_price_types', 'uut_inspection_price_types.inspection_price_id', '=', 'uut_inspection_prices.id')
            ->whereNull('user_type')
            ->where('uut_inspection_price_types.uut_type_id', $item->uuts->type_id)
            ->select('uut_inspection_prices.*')->get();
        
        return view('requestuut.create_booking_inspection', compact('item', 'prices', 'attribute','labs','usertype'));
    }

    public function simpanbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuut");

        ServiceRequestItemInspection::where('service_request_item_id', $id)->delete();

        // dd([$request->all()]);
        
        foreach($request->get('quantity') as $index=>$value) {
            if ($value != null) {

                $dataInspection = new ServiceRequestItemInspection();
                $dataInspection->service_request_item_id = $id;
                $dataInspection->quantity = $value;
                $dataInspection->price = $request->get('price')[$index];
                $dataInspection->inspection_price_id = $request->get('id')[$index];

                $dataInspection->status_id = 1;
                $dataInspection->save();
            }

        }

        $item = $this->ServiceRequestItem->find($id);
        
        $itemAggregate = ServiceRequestItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('service_request_item_id', $id)
            ->first();

        $this->ServiceRequestItem->where('id', $id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = $this->ServiceRequestItem
            ->selectRaw('count(service_request_item_inspections.id) quantity, coalesce(sum(service_request_item_inspections.price * service_request_item_inspections.quantity), 0) subtotal')
            ->join('service_request_item_inspections', 'service_request_item_inspections.service_request_item_id', '=', 'service_request_items.id')
            ->where('service_request_id', $item->serviceRequest->id)
            ->first();

        $svcRequest = $this->ServiceRequest
            ->selectRaw("service_requests.id, service_requests.received_date, service_requests.received_date + max(master_laboratory.sla_day) * interval '1 day' max_estimated_date")
            ->join('service_request_items', 'service_request_items.service_request_id', '=', 'service_requests.id')
            ->join('service_request_item_inspections', 'service_request_item_inspections.service_request_item_id', '=', 'service_request_items.id')
            ->join('uut_inspection_prices', 'service_request_item_inspections.inspection_price_id', '=', 'uut_inspection_prices.id')
            ->join('standard_uut', 'standard_uut.id', '=', 'service_request_items.uut_id')
            ->join('master_standard_types', 'master_standard_types.id', '=', 'standard_uut.type_id')
            ->join('master_laboratory', 'master_standard_types.lab_id', '=', 'master_laboratory.id')
            ->groupBy('service_requests.id', 'service_requests.received_date')
            ->where('service_requests.id', $item->serviceRequest->id)
            ->first();
        // dd([$item->serviceRequest->id,$svcRequest]);

        $itemCount = $this->ServiceRequestItem->where("request_id", $item->serviceRequest->id)->count();
        $request = $this->ServiceRequest->find($item->serviceRequest->id);
        $no_order = $request->no_order;
        $parts = explode("-",$no_order);
        $parts[3] = sprintf("%03d",intval($itemCount));
        $no_order = implode("-", $parts);

        $stat = $this->ServiceRequest->where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal,
            'estimated_date' => $svcRequest ? $svcRequest->max_estimated_date : null,
            'no_order' => $no_order,
        ]);

        return Redirect::route('requestuut.editbooking', $item->serviceRequest->id);
    }

    public function simpanbookinginspection_old($id, Request $request)
    {
        
        $attribute = $this->MyProjects->setup("requestuut");

        ServiceRequestItemInspection::where('request_item_id', $id)->delete();
        
        foreach($request->get('quantity') as $index=>$value) {
            if ($value != null) {

                $dataInspection = new ServiceRequestItemInspection();
                $dataInspection->request_item_id = $id;
                $dataInspection->quantity = $value;
                $dataInspection->price = $request->get('price')[$index];
                $dataInspection->inspection_price_id = $request->get('id')[$index];

                $dataInspection->status_id = 1;
                $dataInspection->save();
            }

        }

        $item = $this->ServiceRequestItem
            ->with(['serviceRequest'])
            ->find($id);
        $price = DB::table('uut_inspection_prices')
            ->where('id', $request->get('inspection_price_id'))
            ->first();
        $harga = $price->price;
        if ($price->has_range) {
            $range = DB::table('standard_inspection_price_ranges')
                ->where('inspection_price_id', $request->get('inspection_price_id'))
                ->where('min_range', '<=', $request->get('quantity'))
                ->whereRaw('coalesce(max_range, min_range ^ 2) > ?', $request->get('quantity'))
                ->first();
            $harga = $range->price;
        }

        $dataInspection["service_request_item_id"] = $id;
        $dataInspection["quantity"] = $request->get('quantity');
        //pengecekan apa dia termasuk yang menggunakan layanan khusus atau tidak
        if(!empty($request->get('flag'))){
            $dataInspection["price"] = $request->get('subtotal');
        }else{
            $dataInspection["price"] = $harga;
        }
        

        $dataInspection["standard_inspection_price_id"] = $request->get('inspection_price_id');

        $dataInspection["status_id"] = 1;

        $dataInspection["laboratory_id"] =  $request->get('laboratory_id');
        $serviceInspectionID = ServiceRequestItemInspection::insertGetId($dataInspection);

        $itemAggregate = ServiceRequestItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('service_request_item_id', $id)
            ->first();

        $this->ServiceRequestItem->where('id', $id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = $this->ServiceRequesItem
            ->selectRaw('count(service_request_item_inspections.id) quantity, coalesce(sum(service_request_item_inspections.price * service_request_item_inspections.quantity), 0) as subtotal')
            ->join('service_request_item_inspections', 'service_request_item_inspections.service_request_item_id', '=', 'service_request_items.id')
            ->where('service_request_id', $item->serviceRequest->id)
            ->first();

        $svcRequest = $this->ServiceRequest
            ->selectRaw("service_requests.id, service_requests.received_date, service_requests.received_date ")/*/max(master_instalasi.sla_day) "* interval '1 day' max_estimated_date") */
            ->join('service_request_standard_items', 'service_request_standard_items.service_request_id', '=', 'service_requests.id')
            ->join('service_request_standard_item_inspections', 'service_request_standard_item_inspections.request_item_id', '=', 'service_request_standard_items.id')
            ->join('standard_inspection_prices', 'service_request_standard_item_inspections.inspection_price_id', '=', 'standard_inspection_prices.id')
            // ->join('master_instalasi', 'standard_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->groupBy('service_requests.id', 'service_requests.received_date')
            ->where('service_requests.id', $item->serviceRequest->id)
            ->first();
        $max_estimated_date =0;
        if(empty($svcRequest->max_estimated_date)){
            $max_estimated_date = now()->modify('+1 day')->format('Y-m-d');
        }
        $itemCount = $this->ServiceRequestItem->where("service_request_id", $item->serviceRequest->id)->count();
        $request = $this->ServiceRequest->find($item->serviceRequest->id);
        $no_order = $request->no_order;
        $parts = explode("-",$no_order);
        $parts[3] = sprintf("%03d",intval($itemCount));
        $no_order = implode("-", $parts);

        $this->ServiceRequest->where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal,
            'estimated_date' => $max_estimated_date,
            'no_order' => $no_order,
        ]);

        

        return Redirect::route('requestuut.editbooking', $item->serviceRequest->id);
    }

    public function hapusbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuut");

        $inspection = ServiceRequestItemInspection::where('id', $id)->first();
        $inspection->delete();

        $item = $this->ServiceRequestItem
            ->with(['serviceRequest'])
            ->find($inspection->service_request_item_id);
        
        $itemAggregate = ServiceRequestItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $item->id)
            ->first();

        $this->ServiceRequestItem->where('id', $item->id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = $this->ServiceRequestItem
            ->selectRaw('count(service_request_item_inspections.id) quantity, coalesce(sum(service_request_item_inspections.price * service_request_item_inspections.quantity), 0) subtotal')
            ->join('service_request_item_inspections', 'service_request_item_inspections.service_request_item_id', '=', 'service_request_items.id')
            ->where('service_request_id', $item->serviceRequest->id)->first();
        
        $this->ServiceRequest->where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal
        ]);

        return Redirect::route('requestuut.editbooking', $item->serviceRequest->id);
    }
    
    public function getprices($id) {
        $price = DB::table('uut_inspection_prices')
        ->join('standard_detail_types','uut_inspection_prices.service_type_id','=','standard_detail_types.id')
        ->join('master_laboratory','uut_inspection_prices.service_type_id','=','master_laboratory.id')
        ->where('uut_inspection_prices.id', $id)
        ->first();
        $results['price'] = $price;
        
        if ($price->has_range) {
            $ranges = DB::table('standard_inspection_price_ranges')->where('inspection_price_id', $id)->get();
			$results['ranges'] = $ranges;
		}
        return response($results);
    }
    

    // public function getprices($id) {
    //     $price = DB::table('standard_inspection_prices')
    //     ->join('standard_detail_types','standard_inspection_prices.standard_detail_type_id','=','standard_detail_types.id')
    //     ->join('standard_tool_types','standard_detail_types.standard_tool_type_id','=','standard_tool_types.id')
    //     ->join('standard_measurement_types','standard_tool_types.standard_measurement_type_id','=','standard_measurement_types.id')
    //     ->join('master_laboratory','standard_measurement_types.laboratorium_id','=','master_laboratory.id')
    //     ->where('standard_inspection_prices.id', $id)->first();
    //     $results['price'] = $price;
    //     // dd($price);
    //     if ($price->has_range) {
    //         $ranges = DB::table('standard_inspection_price_ranges')->where('inspection_price_id', $id)->get();
	// 		$results['ranges'] = $ranges;
	// 	}

    //     return response($results);
    // }

    public function getowners(Request $request) {
        $owners = DB::table('uut_owners');
        if ($request->has('q')){
            $owners = $owners->where('nama', 'like', '%'.$request->input('q').'%');
        }

        return response()->json($owners->get());
    }

    public function getowner($id) {
        $owner = DB::table('uut_owners')->where('id', $id)->first();

        return response()->json($owner);
    }

    public function gethistory($id) {
        $rows = ServiceOrders::with([
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItemInspection',
            'ServiceRequestItemInspection.inspectionPrice',
        ])
        ->join('service_request_standard_items', 'service_request_standard_items.id', '=', 'service_orders.service_request_item_id')
        ->where('service_request_standard_items.uttp_id', $id)
        ->get();
        return response()->json($rows);
    }

    public function updateStandard($id, Request $request )
    {
        $dataItem = ServiceRequestItem::find($id);
        $tc = $dataItem;
        $dataItem->standard_id = $request->get('standard_id');
        $sc = $dataItem->save();
         
        // if($sc){
        //     $response["id"] = $id;
        //     // $response["tool_code"] = $tc;
        //     $response["status"] = true;
        //     $response["messages"] = "Data berhasil disimpan";
        // }else{
        //     $response["id"] = $id;
        //     // $response["tool_code"] = '';
        //     $response["status"] = false;
        //     $response["messages"] = "Data gagal disimpan";
        // }
        // return response($response);
    }

    // public function getTools($id, Request $request )
    // {
    //     $dataItem = ServiceRequestItem::find($id);
    //     $dataItem->standard_id = $request->get('standard_id');
    //     $dataItem->save();
    // }

    public function getStandard($id){
        $items = ServiceRequestItem::find($id);
        $standards = Standard ::find($items->standard_id);
        return $standards;
    }

    public function kirimqr(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        $item = ServiceRequestItem::where("no_order", $request->no_order)->first();
        
        if ($item->cetak_tag_created != null) {
            $item->status_id = 9;
            $item->delivered_at = date("Y-m-d H:i:s");
            $item->save();

            $history = new HistoryUut();
            $history->request_status_id = 9;
            $history->request_id = $request->id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();
            
            ServiceRequestItemInspection::where("service_request_item_id", $item->id)->update([
                "status_id"=>9,
            ]);

            $items_count = DB::table('service_request_items')
                ->selectRaw('sum(case when status_id = 9 then 1 else 0 end) count_9, count(id) count_all')
                ->where('service_request_id', $item->request_id)->get();

            if ($items_count[0]->count_9 == $items_count[0]->count_all) {
                ServiceRequest::find($item->request_id)->update(['status_id'=>9]);
            }

            $response["status"] = true;
            $response["messages"] = "Alat telah dikirim";
        }

        return response($response);
    }

    public function permintaanChange(Request $req,$id){
        $table = DB::table('service_request_items')->where('id',$id)->update([
            'keterangan' => $req->get('txt_permintaan'),
        ]);
        
        return redirect()->route('requestuut.editbooking',$req->get('req_id'));
    }
}
