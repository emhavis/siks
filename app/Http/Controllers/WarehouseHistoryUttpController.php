<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\ServiceOrderUttps;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\HistoryUttp;
use App\MasterInstalasi;

class WarehouseHistoryUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("warehousehistoryuttp");

        $rows = ServiceOrderUttps::whereIn("stat_service_order",[0,1,2,3,4])
                ->whereHas('ServiceRequestItem', function($query) 
                {
                    $query->whereIn("status_id",[13,14,16]);
                })
                ->where('stat_warehouse', 2)
                ->get();
        
        return view('warehousehistoryuttp.index',compact(['rows','attribute']));
    }    

    public function print($id)
    {
        $row = ServiceOrderUttps::find($id);
        return view('warehousehistoryuttp.pdf',compact('row'));
    }
}
