<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use App\ServiceRequestUttp;
use App\Customer;
use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\UttpInsituCheckItem;
use App\ServiceRequestUttpInsituDocCheck;
use App\HistoryUttp;
use App\ServiceRequestUttpItem;
use App\MasterServiceType;
use App\ServiceOrderUttps;
use App\MasterUsers;
use App\MasterPetugasUttp;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Mail\SuratTugasInsitu;
use App\Mail\InvoiceTUHP;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

class ReDocumentInsituUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("docinsituuttp");

        $rows = ServiceRequestUttp::where('status_id', 12)
            ->where('status_revisi_spt', 2)
            ->select("*")
            ->addSelect(
                DB::raw("(select count(id) from service_request_uttp_insitu_doc 
                    where request_id = service_request_uttps.id and jenis='revisi' and (is_accepted = false or is_accepted is null)) as cnt"))
            ->orderBy('received_date','desc')->get();


        return view('redocinsituuttp.index',compact('rows','attribute'));
    }

    public function approval($id) 
    {
        $attribute = $this->MyProjects->setup("docinsituuttp");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->where('jenis','revisi')->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        return view('redocinsituuttp.approve', compact(['request', 'requestor', 'doc', 'staffs',  'attribute']));
    }

    public function approve($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("docinsituuttp");

        $requestEntity = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->where('jenis','revisi')->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)
                ->whereNotNull('scheduled_id')
                ->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        $rules = [];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            
            //$data['is_accepted'] = $request->get('is_accepted') === "ya" ? true :
            //        ($request->get('is_accepted') === "tidak" ? false : null);
 
            /*
            $doc->update([
                'is_accepted' => $data['is_accepted'],
                
            ]);
            */

            if ($request->get("is_approved") === "ya" ) 
            {
                
                $token = $this->hashing($doc->doc_no, $requestEntity->no_order);

                // simpan doc
                $doc->update([
                    
                    'accepted_date' => date("Y-m-d"),
                    'accepted_by_id' => Auth::id(),

                    'token' => $token,
                ]);


                // UNTUK CUSTOMER
                $customerEmail = $requestEntity->requestor->email;;
                //Mail::to($customerEmail)->send(new BookingConfirmation($requestEntity));
                ProcessEmailJob::dispatch($customerEmail, new SuratTugasInsitu($requestEntity, $staffs, null))
                    ->onQueue('emails');

               
                // PAYMENT TUHP
                if ($requestEntity->lokasi_pengujian == 'luar') {
                    if ($doc->invoiced_price > 0) {
                        ServiceRequestUttp::whereId($id)
                        ->update([
                            "spuh_spt"=>$doc->doc_no,
                            "spuh_no" => $requestEntity->spuh_no,
                            "spuh_billing_date"=>date("Y-m-d"),
                            "spuh_payment_date" => null,
                            "spuh_payment_status_id" => 6,
                            "status_revisi_spt"=>3,
                            "spuh_doc_id"=>$doc->id,
                            "spuh_price"=>$doc->price,
                            "spuh_inv_price"=>$doc->invoiced_price,
                        ]);

                        $items = ServiceRequestUttpItem::where("request_id", $id)->orderBy('id', 'asc')->get();

                        foreach($items as $item) {
                            $history = new HistoryUttp();
                            $history->request_status_id = $requestEntity->status_id;
                            $history->request_id = $requestEntity->id;
                            $history->request_item_id = $item->id;
                            $history->spuh_payment_status_id = 6;
                            $history->user_id = Auth::id();
                            $history->save();
                        }

                        if ($doc->payment_date == null) {
                            $doc->billing_date = date("Y-m-d");
                        }
                        
                        $doc->billing_date = date("Y-m-d");
                        $doc->save();

                        $customerEmail = $requestEntity->requestor->email;
                        //Mail::to($customerEmail)->send(new Invoice($svcRequest));
                        ProcessEmailJob::dispatch($customerEmail, new InvoiceTUHP($requestEntity, $doc))->onQueue('emails');
                    }
                    else {
                        //$doc->update([
                        //    'valid_bukti_bayar' => date("Y-m-d"),
                        //]);
                
                        ServiceRequestUttp::whereId($request->id)
                        ->update([
                            //"spuh_payment_valid_date" => date("Y-m-d"),
                            'spuh_payment_status_id' => 8,
                            'status_revisi_spt' => 3,

                            "spuh_spt"=>$doc->doc_no,
                            "spuh_no" => $requestEntity->spuh_no,
                            
                            "spuh_doc_id"=>$doc->id,
                            "spuh_price"=>$doc->price,
                            "spuh_inv_price"=>$doc->invoiced_price,
                        ]);

                        $items = ServiceRequestUttpItem::where("request_id", $id)->orderBy('id', 'asc')->get();

                        foreach($items as $item) {
                            $history = new HistoryUttp();
                            $history->request_status_id = $requestEntity->status_id;
                            $history->request_id = $requestEntity->id;
                            $history->request_item_id = $item->id;
                            $history->spuh_payment_status_id = 8;
                            $history->user_id = Auth::id();
                            $history->save();
                        }
                    }
                }

            } else {
                
                ServiceRequestUttp::whereId($id)
                ->update([
                    "status_revisi_spt"=> 1,
                ]);

                $doc->update([
                    'revisi_status_spt' => 1,
                    'approval_note' => $request->get('approval_note'),
                ]);

            }
            return Redirect::route('redocinsituuttp');
        } else {
            return Redirect::route('redocinsituuttp.approval', $id);
        }
    }

    private function hashing($username, $password) {
        $token = Hash::make($username.':'.$password);
        if (strpos($token, '/') !== false) {
            $token = $this->hashing($username, $password);
        } 
        return $token;
    }
}