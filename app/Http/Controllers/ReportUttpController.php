<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use PDF;

class ReportUttpController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("serviceuttp");

        $laboratory_id = Auth::user()->laboratory_id;

        $rows = ServiceOrderUttps::with([
            'ServiceRequest', 'ServiceRequestItem', 'ServiceRequestItem.uttp',
            'MasterUsers', 'LabStaffOut',
        ])
        ->whereIn("stat_service_order",[3])
        ->where("is_finish",1)
        ->where('laboratory_id', $laboratory_id)
        ->orderBy('staff_entry_datein','desc')
        ->get();

        //dd($rows);
        /*
        $query = "select sou.service_request_id,
        sru.no_order, sru.label_sertifikat, 
        sou.service_request_item_id,
        mut.uttp_type,
        sou.service_request_item_inspection_id,
        uip.inspection_type, sruii.quantity, sruii.price,
        sru.total_price, 
        sru.billing_code, sru.payment_code,
        sou.staff_entry_datein, sou.staff_entry_dateout,
        mi.nama_instalasi 
        from service_order_uttps sou 
        inner join service_request_uttps sru on sou.service_request_id = sru.id  
        inner join service_request_uttp_items srui on sou.service_request_item_id = srui.id 
        inner join service_request_uttp_item_inspections sruii on sou.service_request_item_inspection_id = sruii.id 
        inner join uttps u on srui.uttp_id = u.id
        inner join master_uttp_types mut on u.type_id = mut.id 
        inner join uttp_inspection_prices uip on sruii.inspection_price_id = uip.id 
        inner join master_instalasi mi on uip.instalasi_id = mi.id ";
            */

        return view('reportuttp.index',compact('rows','attribute'));
    }

}
