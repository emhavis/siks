<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\ServiceRequestUutItemInspection;
use App\ServiceRequestUutItem;
use App\ServiceRequestUutInsituDoc;
use App\ServiceRequestUutInsituStaff;

use App\Customer;
use App\MasterUsers;
use App\MasterPetugasUttp;
use App\MasterServiceType;
use App\ServiceRequestUutStaff;
use App\MasterSbm;
use App\MasterSbmLuar;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use PDF;

use App\HistoryUttp;
use App\UttpInsituCheckItem;
use App\ServiceRequestUttpInsituDocCheck;

use App\Mail\ScheduledInsitu;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

class RevisiSchedulingUutController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("reschedulinguut");

        $rows = ServiceRequest::where('status_id', 12)
            ->where('status_revisi_spt', 1)
            ->select("*")
            ->addSelect(
                DB::raw("(select count(id) from service_request_uut_insitu_doc 
                    where request_id = service_requests.id and jenis='revisi' and (is_accepted = false or is_accepted is null)) as cnt"))
            ->orderBy('received_date','desc')->get();
        //dd($rows);

        return view('reschedulinguut.index',compact('rows','attribute'));
    }

    public function schedule($id)
    {
        $attribute = $this->MyProjects->setup("reschedulinguut");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts','inspectionKabkot','inspectionProv'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);
        
        $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis','inisial')->orderBy('id', 'desc')->first();

        $doc_revisi_count = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis','revisi')->count();
        if ($doc_revisi_count > 0) {
            $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis','revisi')->orderBy('id', 'desc')->first();
            $doc_no = $doc->doc_no;
        } else {
            $docno_parts = explode("/PKTN.4.6/ST/", $doc->doc_no);
            $doc_no = $docno_parts[0] . '.R' . ($doc_revisi_count + 1) . "/PKTN.4.6/ST/" . $docno_parts[1];
        }

        $staffes = ServiceRequestUutStaff::where("request_id", $id)->get(); 

        return view('reschedulinguut.schedule', compact(['request', 'requestor', 'staffes', 'doc', 'doc_no', 'attribute']));
    }

    public function confirmschedule($id, Request $request)
    {
        $response["status"] = false;

        $requestEntity = ServiceRequest::find($id);

        //$rules['booking_id'] = ['required'];
        //$rules['scheduled_test_id'] = ['required'];
        $rules['scheduled_test_date_from'] = ['required','date'];
        $rules['scheduled_test_date_to'] = ['required','date','after_or_equal:scheduled_test_date_from'];
        //$rules['spuh_spt'] = ['required'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        $doc_inisial = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis', 'inisial')->first();

        if ($validation->passes())
        {
            $data = [];
            $keys = array_keys($rules);
            foreach($request->all() as $k=>$v)
            {
                if(in_array($k,$keys))
                {
                    $data[$k] = $request->get($k);
                }
            }
            
            $data["scheduled_test_date_from"] = date("Y-m-d", strtotime($request->get("scheduled_test_date_from")));
            $data["scheduled_test_date_to"] = date("Y-m-d", strtotime($request->get("scheduled_test_date_to")));

            $data["scheduled_test_id"] = Auth::id();

            $data["received_date"] = $data["scheduled_test_date_from"];
            $data["estimated_date"] = $data["scheduled_test_date_to"];

            $dt1 = new \DateTime($data['scheduled_test_date_from']);
            $dt2 = new \DateTime($data['scheduled_test_date_to']);
            $interval = $dt1->diff($dt2);

            $data["spuh_staff"] = ($request->has('scheduled_id_1') ? 1 : 0) + ($request->has('scheduled_id_2') ? 1 : 0);

            $data["doc_no"] = $request->get('doc_no');

            $data["days"] = ((int)$interval->format('%a') + 1);
            $int_interval = $data["days"];
            $data["add_days"] = $int_interval - $doc_inisial->days;

            $data["spuh_rate"] = $request->get('spuh_rate');
            $data["spuh_rate1"] = $request->get('spuh_rate');
            $data["spuh_rate2"] = $request->get('spuh_rate');
            $data["spuh_rate_id"] = $request->get('spuh_rate_id');

            $data["spuh_price"] = $data["add_days"] * $data["spuh_staff"] * $data["spuh_rate"];
            $data["spuh_inv_price"] = $data["spuh_price"];

            //$doc = ServiceRequestUttpInsituDoc::find($requestEntity->spuh_doc_id);
            $doc = ServiceRequestUutInsituDoc::where('request_id', $id)->where('jenis','revisi')
                ->whereRaw('(is_accepted = false or is_accepted is null)')
                ->orderBy('id', 'desc')->first();
            //dd($doc);

            $staffs[] = $request->get('scheduled_id_1');
            $staffs[] = $request->get('scheduled_id_2');

            if ($doc == null) 
            {
                $doc = ServiceRequestUutInsituDoc::create([
                    "request_id" => $id,
                    //"spuh_no"=>$no,
                    //"spuh_year"=>date("Y"),
                    "doc_no" => $data['doc_no'],
                    "rate" => $data["spuh_rate"],
                    "rate_id" => $data["spuh_rate_id"],
                    "price" => $data["spuh_price"],
                    "invoiced_price" => $data["spuh_price"],
                    "staffs" => implode(";",$staffs),
                    "date_from" => $data["scheduled_test_date_from"],
                    "date_to" => $data["scheduled_test_date_to"],
                    "days" => ((int)$interval->format('%a') + 1),

                    "is_siap_petugas" => null,
                    "keterangan_tidak_siap" => null,
                    "is_accepted" => null,
                    "keterangan_tidak_lengkap" => null,

                    "spuh_rate1" => $data["spuh_rate1"],
                    "spuh_rate2" => $data["spuh_rate2"],

                    "add_days" => $int_interval - $doc_inisial->days,

                    "keterangan_revisi"=>$request->get('keterangan_revisi'),
                    "jenis" => "revisi",
                ]);
            } else {
                ServiceRequestUutInsituDoc::whereId($doc->id)
                    ->update([
                    "request_id" => $id,
                    //"spuh_no"=>$no,
                    //"spuh_year"=>date("Y"),
                    //"doc_no" => $spuh_spt,
                    "rate" => $data["spuh_rate"],
                    "rate_id" => $data["spuh_rate_id"],
                    "price" => $data["spuh_price"],
                    "invoiced_price" => $data["spuh_price"],
                    "staffs" => implode(";",$staffs),
                    "date_from" => $data["scheduled_test_date_from"],
                    "date_to" => $data["scheduled_test_date_to"],
                    "days" => ((int)$interval->format('%a') + 1),

                    "is_siap_petugas" => null,
                    "keterangan_tidak_siap" => null,
                    "is_accepted" => null,
                    "keterangan_tidak_lengkap" => null,

                    "spuh_rate1" => $data["spuh_rate1"],
                    "spuh_rate2" => $data["spuh_rate2"],

                    "add_days" => $int_interval - $doc_inisial->days,

                    "keterangan_revisi"=>$request->get('keterangan_revisi'),
                    "jenis" => "revisi",
                ]);
            }

            ServiceRequestUutStaff::where("request_id", $id)->delete();
            ServiceRequestUutInsituStaff::where("doc_id", $doc->id)->delete();

            ServiceRequestUutStaff::insert([
                [
                    "request_id" => $id,
                    "scheduled_id" => $request->get("scheduled_id_1"),
                ],
                [
                    "request_id" => $id,
                    "scheduled_id" => $request->get("scheduled_id_2"),
                ],
            ]);

            ServiceRequestUutInsituStaff::insert([
                [
                    "doc_id" => $doc->id,
                    "scheduled_id" => $request->get("scheduled_id_1"),
                ],
                [
                    "doc_id" => $doc->id,
                    "scheduled_id" => $request->get("scheduled_id_2"),
                ]
            ]);

            //$data["spuh_doc_id"] = $doc->id;
            //$requestEntity->update($data);

            /*
            $checks = UttpInsituCheckItem::orderBy('order_no', 'ASC')->get();
            foreach($checks as $check) 
            {
                ServiceRequestUttpInsituDocCheck::create([
                    'doc_id' => $doc->id,
                    'check_item_id' => $check->id,
                    'order_no' => $check->order_no,
                ]);
            }
            */


            if ($request->get('status_submit') == 1) 
            {
                

                $requestEntity->update([
                    "status_revisi_spt" => 2
                ]);

            }

            $response["id"] = $requestEntity->id;
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
            
        }

        return response($response);
    }

    public function getstaffes(Request $request)  
    {
        $from = date("Y-m-d", strtotime($request->get("from")));
        $to = date("Y-m-d", strtotime($request->get("to")));
        $search = $request->get('search');

        $rows = ServiceRequestUttp::select('service_request_uttp_insitu_staff.*')
            ->join('service_request_uttp_insitu_doc', 'service_request_uttp_insitu_doc.id', '=', 'service_request_uttps.spuh_doc_id')
            ->join('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->whereRaw('not (service_request_uttp_insitu_doc.date_from > ? or service_request_uttp_insitu_doc.date_to < ?)', [$to, $from])
            ->whereIn('service_request_uttps.status_id', [3,4,5,6,7,8,9,10,11,12])
            ->distinct()
            ->get();
            
        //dd([date("Y-m-d", strtotime($request->get("from"))), date("Y-m-d", strtotime($request->get("to"))), $rows]);

        $staffes = $rows->map(function ($item) {
            return $item->scheduled_id;
        })->unique()->values()->all();
        
        $staffes = array_filter($staffes, function($value) { 
            return !is_null($value) && $value !== ''; 
        });

        $users = MasterPetugasUttp::where('is_active', true)
            //->where('flag_unit', 'uttp')
            ->whereNotIn('id', $staffes);
       

        if ($search) {
            $users = $users->whereRaw('lower(nama) like ?', '%'.strtolower($search).'%');
        }

        $users = $users->orderBy('id')->get();
        
        $response["data"] = $users;
        $response["status"] = true;
        $response["messages"] = "Data berhasil diambil";

        return response($response);
    }

    public function print($id, $stream = 0, $docId = 0)
    {
        $attribute = $this->MyProjects->setup("schedulinguttp");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        if ($docId == 0) {
            $doc = ServiceRequestUttpInsituDoc::find($request->spuh_doc_id);
        } else {
            $doc = ServiceRequestUttpInsituDoc::find($docId);
        }

        $staffes = ServiceRequestUttpInsituStaff::where('doc_id', $doc->id)->get();

        $doc_inisial = ServiceRequestUttpInsituDoc::where('request_id', $id)->where('jenis', 'inisial')->first();
        //dd($staffes);

        $file_name = 'Surat Tugas '.($request->no_order ? $request->no_order : 'XXXXXX');

        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'surat_tugas',
            'token' => $doc->token,
        ]);
       
        $blade = 'schedulinguttp.surat_tugas_pdf';

        $view = false;
        
        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo'),
            'defaultPaperSize' => 'a4',
            'defaultMediaType' => 'print',
        ])
        ->loadview($blade,compact('request', 'requestor', 'doc', 'doc_inisial', 'staffes', 'qrcode_generator', 'view'));

        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("schedulinguttp");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);
        //$users = MasterUsers::pluck('full_name', 'id');
        /*
        $users = MasterPetugasUttp::where('flag_unit', 'uttp')
            ->where('is_active', true)
            ->pluck('nama', 'id');
            */
        $rate = MasterSbm::where('provinsi_id', $request->inspection_prov_id)->first();

        if ($request->inspection_kabkot_id == env('KABKOT_DALAM')) {
            $request->spuh_rate = $rate->dalam;
        } else {
            $request->spuh_rate = $rate->luar;
        }
        
        $request->spuh_rate_id = $rate->id;

        //$staffes = ServiceRequestUttpStaff::where("request_id", $id)->get();

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffes = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->get();


        return view('schedulinguttp.cancel', compact(['request', 'requestor', 'staffes', 'attribute']));
    }

    public function cancelsave($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("schedulinguttp");

        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        
        $req->cancel_at = date("Y-m-d");
        $req->cancel_notes = $request->get('cancel_notes');
        $req->status_id = 17;
        $req->update();

        $items = ServiceRequestUttpItem::where("request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUttp();
            $history->request_status_id = 17;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = 17;
            $item->save();
        }

        $response["id"] = $req->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    
}
