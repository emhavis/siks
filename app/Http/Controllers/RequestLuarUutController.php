<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\ServiceRequestUutStaff;
use App\ServiceRequestItem;
use App\ServiceRequestItemInspection;
use App\ServiceRequestUutInsituDoc;
use App\ServiceRequestUutInsituStaff;
use App\ServiceOrders;
use App\HistoryUut;

use App\MasterServiceType;
use App\MasterPetugasUttp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

use App\Mail\Invoice_uut;
use App\Mail\Receipt_uut;
use App\Mail\ReceiptTUHPUUT;
use Illuminate\Support\Facades\Mail;
use App\Customer;

use App\Jobs\ProcessEmailJob;

use PDF;

class RequestLuarUutController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();

        $this->ServiceRequest = new ServiceRequest();
        $this->ServiceRequestItem = new ServiceRequestItem();
        $this->ServiceRequestItemInspection = new ServiceRequestItemInspection();
        $this->ServiceOrder = new ServiceOrders();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("requestluaruut");

        $rows_berangkat = ServiceRequest::whereIn('status_id', [11, 15])
            ->whereNull('payment_status_id')
            ->get();

        $rows_selesai = ServiceRequest::where('status_id', 12)
            ->get();

        $rows_penagihan = ServiceRequest::whereIn('status_id', [5,15,18,13])
            ->where('lokasi_pengujian', 'luar')
            ->where('payment_status_id', 5)
            // ->orWhereNull('payment_status_id')
            ->where(function($query)
                {
                    $query->where('payment_status_id', 5)
                    ->orWhereNull('payment_status_id');
                })
            ->get();

        $rows_validasi = ServiceRequest::whereIn('status_id', [7,15,20])
            ->where('lokasi_pengujian', 'luar')
            //->where('payment_status_id', 7)
            ->where(function($query)
                {
                    $query->where('payment_status_id', 7)
                    ->orWhereNull('payment_status_id');
                })
            ->get();

        return view('requestluaruut.index',compact([
            'rows_berangkat',
            'rows_selesai',
            'rows_penagihan',
            'rows_validasi',
            'attribute']));
    }

    public function proses($id)
    {
        $attribute = $this->MyProjects->setup("requestluaruut");

        $request = ServiceRequest::find($id);
        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();

        return view('requestluaruut.proses',compact(
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function saveproses($id, Request $request)
    {
        $serviceRequest = ServiceRequest::find($id);

        $serviceRequest->status_id = 12;
        $serviceRequest->save();

        $items = ServiceRequestItem::where("service_request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUut();
            $history->request_status_id = 12;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = 12;
            $item->save();
        }

        if ($request->hasFile('spuh_file')) {
            $file = $request->file('spuh_file');
            $file_spuh = $file->getClientOriginalName();
            $path = $file->store(
                'spuh_dl',
                'public'
            );
            $path_spuh = $path;

            ServiceRequestUutInsituDoc::where("id", $serviceRequest->spuh_doc_id)
            ->update([
                "spuh_file" => $file_spuh,
                "spuh_path" => $path_spuh,
            ]);
        }

        return Redirect::route('requestluaruut');

    }

    public function pending($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $request = ServiceRequestUut::find($id);
        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();

        return view('requestluaruut.pending',compact(
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function savepending($id, Request $request)
    {
        $serviceRequest = ServiceRequest::find($id);

        $serviceRequest->status_id = 15;
        $serviceRequest->pending_status = 1;
        $serviceRequest->pending_created = date('Y-m-d H:i:s');
        $serviceRequest->pending_notes = $request->get("pending_notes");
        $serviceRequest->save();

        $items = ServiceRequestItem::where("service_request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUut();
            $history->request_status_id = 15;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = 15;
            $item->save();
        }

        $response["id"] = $serviceRequest->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    public function continue($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $request = ServiceRequest::find($id);
        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();

        $users = MasterPetugasUttp::pluck('nama', 'id');

        return view('requestluaruut.continue',compact(
            'request',
            'staffes',
            'users',
            'attribute'
        ));
    }

    public function savecontinue($id, Request $request)
    {
        $serviceRequest = ServiceRequest::find($id);

        //$serviceRequest->status_id = 11;
        $serviceRequest->pending_status = 0;
        $serviceRequest->pending_ended = date('Y-m-d H:i:s');

        $serviceRequest->spuh_spt = $request->get('spuh_spt');
        $serviceRequest->spuh_staff = count($request->get('scheduled_id'));

        $serviceRequest->received_date = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $serviceRequest->estimated_date = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));

        $serviceRequest->scheduled_test_date_from = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $serviceRequest->scheduled_test_date_to = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));

        $dt1 = new \DateTime($serviceRequest->received_date);
        $dt2 = new \DateTime($serviceRequest->estimated_date);
        $interval = $dt1->diff($dt2);
        $total_price = ((int)$interval->format('%a') + 1) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        $serviceRequest->spuh_price = ((int)$interval->format('%a') + 1) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        $serviceRequest->save();

        $d1 = $request->get('scheduled_test_date_from');

        $last_doc = ServiceRequestUutInsituDoc::where('request_id', $id)
            ->latest()
            ->first();
        
        $invoiced = null;
        $historyStatus = $serviceRequest->status_id;
        if ($serviceRequest->spuh_price > $last_doc->price) {
            //$serviceRequest->status_id = 6;
            $serviceRequest->spuh_inv_price = $serviceRequest->spuh_price - $last_doc->price;
            $invoiced = $serviceRequest->spuh_inv_price;
            $serviceRequest->spuh_payment_date = null;
            $serviceRequest->payment_status_id = 5;
        } else {
            $serviceRequest->status_id = 11;
        }

        $doc = ServiceRequestUutInsituDoc::create([
            "request_id" => $id,
            "doc_no" => $request->get('spuh_spt'),
            "rate" => $serviceRequest->spuh_rate,
            "rate_id" => $serviceRequest->spuh_rate_id,
            "price" => $serviceRequest->spuh_price,
            "invoiced_price" => $invoiced ? $invoiced : null,
            "staffs" => implode(";",$request->get("scheduled_id")),
            "date_from" => $serviceRequest->received_date,
            "date_to" => $serviceRequest->estimated_date,
            "days" => ((int)$interval->format('%a') + 1),
        ]);

        ServiceRequestUutStaff::where("request_id", $id)->delete();
        foreach($request->get('scheduled_id') as $index=>$value) {
            
            ServiceRequestUutStaff::insert([
                "request_id" => $id,
                "scheduled_id" => $request->input("scheduled_id.".$index),
            ]);

            ServiceRequestUutInsituStaff::insert([
                "doc_id" => $doc->id,
                "scheduled_id" => $request->input("scheduled_id.".$index),
            ]);
        }

        $serviceRequest->spuh_doc_id = $doc->id;
        
        $serviceRequest->save();

        $items = ServiceRequestItem::where("service_request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUut();
            $history->request_status_id = $historyStatus;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = $historyStatus;
            $item->save();
        }

        $response["id"] = $serviceRequest->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $request = ServiceRequest::find($id);
        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();

        return view('requestluaruut.cancel',compact(
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function savecancel($id, Request $request)
    {
        $serviceRequest = ServiceRequest::find($id);

        $serviceRequest->status_id = 16;
        $serviceRequest->cancel_at = date('Y-m-d H:i:s');
        $serviceRequest->cancel_notes = $request->get("cancel_notes");
        $serviceRequest->save();

        $items = ServiceRequestItem::where("service_request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUut();
            $history->request_status_id = 16;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = 16;
            $item->save();
        }

        $response["id"] = $serviceRequest->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    public function selesai($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $request = ServiceRequest::find($id);
        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();

        return view('requestluaruut.selesai',compact(
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function saveselesai($id, Request $request)
    {
        // $total =0;
        // $ItemsAll = ServiceRequestItem::where('service_request_id',$id)->get();
        // foreach($ItemsAll as $it){
        //     $total +=$it->subtotal;
        // }

        $serviceRequest = ServiceRequest::find($id);
        // $serviceRequest->total_price = $total;
        // $serviceRequest->save();

        $state = false;
        $items = $serviceRequest->items;
        foreach($items as $item){
            if(count($item->inspections) > 0){
                $state = true;
            }
        }

        // if ($serviceRequest->total_price == 0 || $state ==false) {
        if($state ==false){
            $response["id"] = $serviceRequest->id;
            $response["status"] = false;
            $response["messages"] = "Item pengujian belum diisi";

            return response($response);
        }

        if ($serviceRequest->billing_code == null && $serviceRequest->payment_code == null) {
            $serviceRequest->payment_status_id = 5;
        }

        $no_order = $serviceRequest->no_order;
        $parts_no_order = explode("-", $no_order);
        $alat = ServiceRequestItemInspection::join('service_request_items', 'service_request_items.id', '=', 'service_request_item_inspections.service_request_item_id')
            ->where('service_request_items.service_request_id', $id)->count();
        $parts_no_order[3] = sprintf("%03d",$alat);
        
        $nextStatus = 18;

        

        $items = ServiceRequestItem::where('service_request_id', $id)->get();
        // dd([$stat = ServiceOrders::where('service_request_item_id', $items[0]->id)->get(),$request]);
        foreach($items as $item) {
            $stat = ServiceOrders::where('service_request_item_id', $item->id)
                ->update([
                    "lab_staff"=>$serviceRequest->scheduled_test_id,
                    "staff_entry_datein"=>date("Y-m-d", strtotime($request->get("scheduled_test_date_from"))),
                    "staff_entry_dateout"=>date("Y-m-d", strtotime($request->get("scheduled_test_date_to"))),
                    "stat_service_order"=>1,
                    "stat_warehouse"=>-1,
                ]);

            
        }

        $dt1 = new \DateTime($request->get('scheduled_test_date_from'));
        $dt2 = new \DateTime($request->get('scheduled_test_date_to'));
        $interval = $dt1->diff($dt2);

        $total_price = ((int)$interval->format('%a') + 1) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        $serviceRequest->spuh_add_price = $total_price;
        //$serviceRequest->spuh_add_price = ((int)$interval->format('%a')) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        if ($serviceRequest->spuh_add_price > $serviceRequest->spuh_price) {
            $serviceRequest->spuh_add_price = $serviceRequest->spuh_add_price - $serviceRequest->spuh_price;
            $serviceRequest->save();
        } else {
            $nextStatus = 13;
        }

        $doc = ServiceRequestUutInsituDoc::find($serviceRequest->spuh_doc_id);
        $doc->act_date_from = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $doc->act_date_to = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));
        $doc->act_days = ((int)$interval->format('%a') + 1);
        $doc->act_price = $total_price;
        $doc->save();

        // STATUS
        $serviceRequest->status_id = $nextStatus;
        $serviceRequest->received_date = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $serviceRequest->estimated_date = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));

        $serviceRequest->save();

        ServiceRequestItem::where('service_request_id', $id)->update([
            'status_id' => $nextStatus
        ]);

        ServiceRequestItemInspection::where('service_request_item_id', $id)->update([
            'status_id' => $nextStatus,
            // 'status_uttp' => 1,
        ]);

        $items = ServiceRequestItem::where("service_request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUut();
            $history->request_status_id = $nextStatus;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = $nextStatus;
            $item->save();
        }

        $response["id"] = $serviceRequest->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    public function createbookinginspection($id)
    {
        $attribute = $this->MyProjects->setup("requestluaruut");

        $item = ServiceRequestItem::with(['serviceRequest'])
            ->find($id);    

        /* $prices = DB::table('uut_inspection_prices')
                ->join('uut_inspection_price_types', 'uut_inspection_price_types.inspection_price_id', '=', 'uut_inspection_prices.id')
                ->where('uut_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uut_inspection_price_types.uut_type_id', $item->uuts->type_id)
                ->select('uut_inspection_prices.*');
        $prices = $prices->get(); */
        $prices = DB::table('uut_inspection_prices')
                ->join('uut_inspection_price_types', 'uut_inspection_price_types.inspection_price_id', '=', 'uut_inspection_prices.id')
                ->whereNull('user_type')
                // ->where('uut_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uut_inspection_price_types.uut_type_id', $item->uuts->type_id)
                ->select('uut_inspection_prices.*')->get();

        return view('requestluaruut.create_booking_inspection', compact('item', 'prices', 'attribute'));
    }

    public function simpanbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestluaruut");

        ServiceRequestItemInspection::where('service_request_item_id', $id)->delete();
        
        foreach($request->get('quantity') as $index=>$value) {
            if ($value != null) {

                $dataInspection = new ServiceRequestItemInspection();
                $dataInspection->service_request_item_id = $id;
                $dataInspection->quantity = $value;
                $dataInspection->price = $request->get('price')[$index];
                $dataInspection->inspection_price_id = $request->get('id')[$index];

                $dataInspection->status_id = 1;
                $save = $dataInspection->save();
            }

        }

        $item = $this->ServiceRequestItem->find($id);
        
        $itemAggregate = ServiceRequestItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('service_request_item_id', $id)
            ->first();

        $this->ServiceRequestItem->where('id', $id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        /* $reqAggregate = $this->ServiceRequestItem
            ->selectRaw('count(service_request_item_inspections.id) quantity, coalesce(sum(service_request_item_inspections.price * service_request_item_inspections.quantity), 0) subtotal')
            ->join('service_request_item_inspections', 'service_request_item_inspections.service_request_item_id', '=', 'service_request_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first(); */
        $reqAggregate = $this->ServiceRequestItem
        ->selectRaw('count(service_request_item_inspections.id) quantity, coalesce(sum(service_request_item_inspections.price * service_request_item_inspections.quantity), 0) subtotal')
        ->join('service_request_item_inspections', 'service_request_item_inspections.service_request_item_id', '=', 'service_request_items.id')
        ->where('service_request_id', $item->serviceRequest->id)
        ->first();

        $svcRequest = $this->ServiceRequest
            ->selectRaw("service_requests.id, service_requests.received_date, service_requests.received_date + max(master_laboratory.sla_day) * interval '1 day' max_estimated_date")
            ->join('service_request_items', 'service_request_items.service_request_id', '=', 'service_requests.id')
            //->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            //->join('uttp_inspection_prices', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            //->join('master_instalasi', 'uttp_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->join('standard_uut', 'standard_uut.id', '=', 'service_request_items.uut_id')
            ->join('master_standard_types', 'master_standard_types.id', '=', 'standard_uut.type_id')
            ->join('master_laboratory', 'master_standard_types.lab_id', '=', 'master_laboratory.id')
            ->groupBy('service_requests.id', 'service_requests.received_date')
            ->where('service_requests.id', $item->serviceRequest->id)
            ->first();

        /*
        $itemCount = $this->ServiceRequestItem->where("request_id", $item->serviceRequest->id)->count();
        $request = $this->ServiceRequest->find($item->serviceRequest->id);
        $no_order = $request->no_order;
        $parts = explode("-",$no_order);
        $parts[3] = sprintf("%03d",intval($itemCount));
        $no_order = implode("-", $parts);
        */

        $this->ServiceRequest->where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal,
            'estimated_date' => $svcRequest ? $svcRequest->max_estimated_date : null,
            //'no_order' => $no_order,
        ]);

        //return Redirect::route('requestluaruut.selesai', $item->serviceRequest->id);
        return Redirect::route('requestluaruut.editpnbp', $item->serviceRequest->id);
    }


    public function hapusbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuut");

        $inspection = ServiceRequestItemInspection::where('id', $id)->first();
        $inspection->delete();
        $item_id = $inspection->request_item_id;

        $item = ServiceRequestItem::with(['serviceRequest'])
            ->find($item_id);
        
        $itemAggregate = ServiceRequestItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('service_request_item_id', $item->id)
            ->first();

        ServiceRequestItem::where('id', $item->id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = ServiceRequestItem::selectRaw('count(service_request_item_inspections.id) quantity, coalesce(sum(service_request_item_inspections.price * service_request_item_inspections.quantity), 0) subtotal')
            ->join('service_request_item_inspections', 'service_request_item_inspections.service_request_item_id', '=', 'service_request_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        ServiceRequest::where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal
        ]);

        return Redirect::route('requestluaruut.selesai', $item->serviceRequest->id);
    }

    public function pdf($id)
    {
        $row = ServiceRequest::find($id);

        if ($row->lokasi_pengujian == 'luar') {
            if ($row->spuh_no == null) {
                $no_parts = explode("-", $row->no_register);
                $row->spuh_no = sprintf("%04d",$no_parts[1]).'/SPUH/'.date("m/Y");
            }
            if ($row->spuh_billing_date == null) {
                $row->spuh_billing_date = date("Y-m-d");
            }
        }
        
        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUutInsituDoc::where('request_id', $id)->get();

        return view('requestluaruut.pdf',compact(['row', 'staffes', 'docs']));
    }

    public function pdftuhp($id)
    {
        $row = ServiceRequest::find($id);

        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUutInsituDoc::where('request_id', $id)->get();

        return view('requestluaruut.pdf_tuhp',compact(['row', 'staffes', 'docs']));
    }

    public function payment($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $serviceRequest = ServiceRequest::find($id);
        $doc = ServiceRequestUutInsituDoc::find($serviceRequest->spuh_doc_id);

        return view('requestluaruut.payment',compact([
            'id','serviceRequest', 'doc',"attribute"
        ]));
    }

    public function paymentsave(Request $request)
    {
        $rules = [];
        $serviceRequest = ServiceRequest::find($request->id);
        if ($serviceRequest->total_price > 0) {
            $rules = [
                //"payment_date"=>'required',
                //"payment_code"=>'required'
                "billing_code"=>'required',
                "billing_to_date"=>'required' ,
            ];
        }

        $validation = Validator::make($request->all(),$rules);

        if ($validation->passes())
        {
            
            ServiceRequest::whereId($request->id)
            ->update([
                "billing_code"=>$request->billing_code,
                "billing_to_date"=>date("Y-m-d", strtotime($request->billing_to_date)),
                "payment_status_id"=>6,
            ]);

            // $this->updatepaymentstatus($request);
            $items = ServiceRequestItem::where("request_id", $request->id)->orderBy('id', 'asc')->get();
            foreach($items as $item) {
                $history = new HistoryUut();
                $history->request_status_id = $serviceRequest->status_id;
                $history->request_id = $serviceRequest->id;
                $history->request_item_id = $item->id;
                $history->payment_status_id = 6;
                $history->user_id = Auth::id();
                $history->save();
            }

            return response([true,"success"]);
        }

        else
        {
            return response([false,$validation->messages()]);
        }
    }

    public function paymenttuhp($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $serviceRequest = ServiceRequest::find($id);
        $doc = ServiceRequestUutInsituDoc::find($serviceRequest->spuh_doc_id);

        if ($serviceRequest->spuh_no == null) {
            $no_parts = explode("-", $serviceRequest->no_register);
            $serviceRequest->spuh_no = sprintf("%04d",$no_parts[1]).'/SPUH/'.date("m/Y");
        }
        if ($serviceRequest->spuh_billing_date == null) {
            $serviceRequest->spuh_billing_date = date("Y-m-d");
        }

        return view('requestluaruut.paymenttuhp',compact([
            'id','serviceRequest', 'doc',"attribute"
        ]));
    }

    public function paymenttuhpsave(Request $request)
    {
        $rules = [];
        $serviceRequest = ServiceRequest::find($request->id);
        
        $doc = ServiceRequestUutInsituDoc::find($serviceRequest->spuh_doc_id);

        if ($serviceRequest->lokasi_pengujian == 'luar'
            && ($doc->invoiced_price > 0)) {
            ServiceRequest::whereId($request->id)
            ->update([
                "spuh_no" => $request->spuh_no,
                "spuh_billing_date"=>date("Y-m-d"),
                "spuh_payment_date" => null,
            ]);

        }

        if ($doc->payment_date == null) {
            $doc->billing_date = date("Y-m-d");
        }
        if ($doc->act_price != null && $doc->act_price > $doc->price && $doc->act_payment_date == null) {
            $doc->act_billing_date = date("Y-m-d");
        }
        $doc->billing_date = date("Y-m-d");
        $doc->save();

        $this->updatepaymentstatus($request);

        return response([true,"success"]);
   
    }

    private function updatepaymentstatus(Request $request)
    {
        $serviceRequest = ServiceRequest::find($request->id);

        $needPNBP = false;
        $donePNBP = false;
        if ($serviceRequest->total_price > 0) {
            $needPNBP = true;
        }
        if ($needPNBP && $serviceRequest->billing_to_date != null) {
            $donePNBP = true;
        }

        $needTUHP = false;
        $doneTUHP = false;
        $doc = ServiceRequestUutInsituDoc::find($serviceRequest->spuh_doc_id);
        if ($serviceRequest->lokasi_pengujian == 'luar' && $doc->invoiced_price > 0) {
            $needTUHP = true;
        }
        if ($needTUHP && $serviceRequest->spuh_billing_date != null) {
            $doneTUHP = true;
        }

        $nextStatus = 6;
        //dd([$donePNBP, $needPNBP , $doneTUHP , $needTUHP]);
        if ((!$needPNBP || ($donePNBP && $needPNBP)) && ((!$needTUHP || ($doneTUHP && $needTUHP)))) {
            if ($serviceRequest->status_id == 5) {
                ServiceRequest::whereId($request->id)
                    ->update([
                        "status_id"=> $nextStatus,
                    ]);
                ServiceRequestItem::where("service_request_id", $request->id)
                    ->update([
                        "status_id"=> $nextStatus,
                    ]);
                $listItems = ServiceRequestItem::where("service_request_id", $request->id)->pluck('id');
                ServiceRequestItemInspection::whereIn("service_request_item_id", $listItems)->update([
                    "status_id"=> $nextStatus,
                ]);
            } else {
                $nextStatus = 19;
                ServiceRequest::whereId($request->id)
                    ->update([
                        "payment_status_id"=>6,
                        "status_id"=> $nextStatus,
                    ]);
                ServiceRequestItem::where("service_request_id", $request->id)
                    ->update([
                        "status_id"=> $nextStatus,
                    ]);
                $listItems = ServiceRequestItem::where("service_request_id", $request->id)->pluck('id');
                ServiceRequestItemInspection::whereIn("service_request_item_id", $listItems)->update([
                    "status_id"=> $nextStatus,
                ]);
            }

            $items = ServiceRequestItem::where("service_request_id", $serviceRequest->id)->get();
            foreach($items as $item) {
                $history = new HistoryUut();
                $history->request_status_id = $nextStatus;
                $history->request_id = $serviceRequest->id;
                $history->request_item_id = $item->id;
                $history->user_id = Auth::id();
                $history->save();

                $item->status_id = $nextStatus;
                $item->save();
            }

            $svcRequest = ServiceRequest::find($request->id);

            $customerEmail = $svcRequest->requestor->email;
            //Mail::to($customerEmail)->send(new Invoice_uut($svcRequest));
            ProcessEmailJob::dispatch($customerEmail, new Invoice_uut($svcRequest))->onQueue('emails');
        }

    }

    public function valid($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $request = ServiceRequest::find($id);
        return view('requestluaruut.valid',compact('id',"request","attribute"));
    }

    public function validsave(Request $request)
    {
        //$req = ServiceRequestUttp::find($request->id);

        ServiceRequest::whereId($request->id)
            ->update([
                "payment_valid_date" => date("Y-m-d")
            ]);

        $this->allvalid($request);

        return response([true,"success"]);
    }

    public function novalidsave(Request $request)
    {
        ServiceRequest::whereId($request->id)
        ->update([
            "payment_status_id"=>5,
        ]);

        return response([true,"success"]);
    }

    public function validtuhp($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $request = ServiceRequest::find($id);
        return view('requestluaruut.validtuhp',compact('id',"request","attribute"));
    }

    public function validtuhpsave(Request $request)
    {
        $req = ServiceRequest::find($request->id);

        $doc = ServiceRequestUutInsituDoc::find($req->spuh_doc_id);

        ServiceRequest::whereId($request->id)
        ->update([
            "spuh_payment_valid_date" => date("Y-m-d"),
            'spuh_payment_status_id' => 8,
        ]);

        //$this->allvalid($request);
        $items = ServiceRequestItem::where("request_id", $req->id)->get();
        foreach($items as $item) {
            $history = new HistoryUut();
            $history->request_status_id = $req->status_id;
            $history->request_id = $req->id;
            $history->request_item_id = $item->id;
            $history->spuh_payment_status_id = 8;
            $history->user_id = Auth::id();
            $history->save();

        }

        $customerEmail = $req->requestor->email;
        //Mail::to($customerEmail)->send(new Receipt($svcRequest));
        // ProcessEmailJob::dispatch($customerEmail, new ReceiptTUHPUUT($req, $doc))->onQueue('emails');

        return response([true,"success"]);
    }

    public function novalidtuhpsave(Request $request)
    {
        ServiceRequest::whereId($request->id)
        ->update([
            "spuh_payment_status_id"=>6,
            "spuh_payment_date"=>null,
        ]);

        return response([true,"success"]);
    }

    private function allvalid(Request $request)
    {
        $req = ServiceRequest::find($request->id);

        $needValidPNBP = $req->total_price > 0 && $req->payment_date != null && $req->payment_valid_date == null;
        $validPNBP = true;
        if ($needValidPNBP) {
            $validPNBP = $req->payment_valid_date != null;
        }
        $needValidTUHP = $req->spuhDoc->invoiced_price > 0 && $req->spuh_payment_date != null && $req->spuh_payment_valid_date == null;
        $validTUHP = true;
        if ($needValidTUHP) {
            $validTUHP = $req->spuh_payment_valid_date != null;
        }

        if ($validPNBP 
            && $validTUHP) {

            if ($req->status_id == 7) {
                ServiceRequest::whereId($request->id)
                ->update([
                    "status_id" => 11,
                    "payment_status_id" => null,
                ]);
                $listItems = ServiceRequestItem::where("service_request_id", $request->id)->pluck('id');
                ServiceRequestItemInspection::whereIn("service_request_item_id", $listItems)->update([
                    "status_id"=> 11,
                ]);

                $items = ServiceRequestItem::where("service_request_id", $request->id)
                    ->orderBy('id', 'asc')
                    ->get();

                $serviceType = MasterServiceType::find($req->service_type_id);

                $no_order = $serviceType->last_order_no;
                $prefix = $serviceType->prefix;
        
                $alat = count($items);
                $noorder_num = intval($no_order)+1;
                $noorder = $prefix.'-'.date("y-").sprintf("%04d",$noorder_num)."-";

                $no = 0;
                foreach($items as $item) {
                    
                    $no = $no + 1;
                    $noorder_alat = $noorder.sprintf("%03d",$no);

                    $stat = ServiceOrders::insert([
                        "laboratory_id" => $item->uuts->stdtype->lab->id,
                        "service_request_id" => $item->service_request_id,
                        "service_request_item_id" => $item->id,

                        'tool_type_id'			=> $item->uuts->type_id ? $item->uuts->type_id :null,
                        'tool_serial_no'		=> $item->uuts->serial_no ? $item->uuts->serial_no : null,
                        'tool_brand'			=> $item->uuts->tool_brand ? $item->uuts->tool_brand:null,
                        'tool_model'			=> $item->uuts->tool_model ? $item->uuts->tool_model : null,
                        'tool_type'				=> $item->uuts->tool_type ? $item->uuts->tool_type:null,
                        'tool_capacity'			=> $item->uuts->tool_capacity ? $item->uuts->tool_capacity : '',
                        'tool_capacity_unit'	=> $item->uuts->tool_capacity_unit ? $item->uuts->tool_capacity_unit :null ,
                        'tool_factory'			=> $item->uuts->tool_factory ? $item->uuts->tool_factory :null,
                        'tool_factory_address'	=> $item->uuts->tool_factory_address ? $item->uuts->tool_factory_address:null,
                        'tool_made_in'			=> $item->uuts->tool_made_in ? $item->uuts->tool_made_in :null,
                        'tool_made_in_id'		=> $item->uuts->tool_made_in_id ? $item->uuts->tool_made_in_id : null ,
                        'tool_owner_id'			=> $item->uuts->owner_id ? $item->uuts->owner_id : null,
                        'tool_media'			=> $item->uuts->tool_media ? $item->uuts->tool_media : null,
                        'tool_name'				=> $item->uuts->tool_name ? $item->uuts->tool_name : null,
                        'tool_class'		    => $item->uuts->class ? $item->uuts->class : null,
                        'tool_jumlah'		    => $item->uuts->jumlah ? $item->uuts->jumlah : null,
                        'tool_dayabaca'		    => $item->uuts->tool_dayabaca ? $item->uuts->tool_dayabaca :null,
                        'tool_dayabaca_unit'    => $item->uuts->tool_dayabaca_unit ? $item->uuts->tool_dayabaca_unit : null,

                        'uut_id'                => $item->uut_id,
                    ]);

                    ServiceRequestItem::where("id", $item->id)
                    ->update([
                        "status_id"=>11,
                        "no_order"=>$noorder_alat,
                        "order_at"=> $req->lokasi_pengujian == 'dalam' ? date("Y-m-d H:i:s") : null,
                    ]);

                    $history = new HistoryUut();
                    $history->request_status_id = 11;
                    $history->request_id = $request->id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();
                }

                $serviceType->last_order_no = $noorder_num;
                $serviceType->save();
            }
            if ($req->status_id == 13) {
                ServiceRequest::whereId($request->id)
                ->update([
                    "payment_status_id"=>13,
                ]);

                $items = ServiceRequestItem::where("service_request_id", $request->id)->get();
                foreach($items as $item) {
                    $history = new HistoryUut();
                    $history->request_status_id = 13;
                    $history->request_id = $request->id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = 13;
                    $item->save();
                }

            }
            if ($req->status_id == 15) {
                ServiceRequest::whereId($request->id)
                ->update([
                    "payment_status_id"=>null,
                    "status_id" => 11
                ]);

                $items = ServiceRequestItem::where("service_request_id", $request->id)->get();
                foreach($items as $item) {
                    $history = new HistoryUut();
                    $history->request_status_id = 11;
                    $history->request_id = $request->id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = 11;
                    $item->save();
                }

            }
            if ($req->status_id == 20) {
                ServiceRequest::whereId($request->id)
                ->update([
                    "status_id"=>13,
                    "payment_status_id"=>13,
                ]);

                ServiceRequestItem::where("service_request_id", $request->id)
                    ->update([
                        "status_id"=>13,
                    ]);

                $listItems = ServiceRequestItem::where("service_request_id", $request->id)->pluck('id');
                ServiceRequestUutItemInspection::whereIn("service_request_item_id", $listItems)->update([
                    "status_id"=> 13,
                ]);

                $items = ServiceRequestItem::where("service_request_id", $request->id)->get();
                foreach($items as $item) {
                    $history = new HistoryUut();
                    $history->request_status_id = 13;
                    $history->request_id = $request->id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = 13;
                    $item->save();
                }

            }

            $svcRequest = ServiceRequest::find($request->id);

            $customerEmail = $svcRequest->requestor->email;
            //Mail::to($customerEmail)->send(new Receipt_uut($svcRequest));
            ProcessEmailJob::dispatch($customerEmail, new Receipt_uut($svcRequest))->onQueue('emails');
        }
    }

    public function extend($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $request = ServiceRequest::find($id);
        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();

        return view('requestluaruut.extend',compact(
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function saveextend($id, Request $request)
    {
        $serviceRequest = ServiceRequestUttp::find($id);

        $serviceRequest->scheduled_test_date_from = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $serviceRequest->scheduled_test_date_to = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));

        $serviceRequest->status_id = 18; // penagihan tambahan

        $serviceRequest->save();

        $dt1 = new \DateTime($request->get('scheduled_test_date_from'));
        $dt2 = new \DateTime($request->get('scheduled_test_date_to'));
        $interval = $dt1->diff($dt2);

        $total_price = ((int)$interval->format('%a') + 1) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        $serviceRequest->spuh_add_price = $total_price;
        //$serviceRequest->spuh_add_price = ((int)$interval->format('%a')) * $serviceRequest->spuh_staff * $serviceRequest->spuh_rate;
        if ($serviceRequest->spuh_add_price > $serviceRequest->spuh_price) {
            $serviceRequest->spuh_add_price = $serviceRequest->spuh_add_price - $serviceRequest->spuh_price;
            $serviceRequest->save();
        }

        $doc = ServiceRequestUttpInsituDoc::find($serviceRequest->spuh_doc_id);
        $doc->act_date_from = date("Y-m-d", strtotime($request->get('scheduled_test_date_from')));
        $doc->act_date_to = date("Y-m-d", strtotime($request->get('scheduled_test_date_to')));
        $doc->act_days = ((int)$interval->format('%a') + 1);
        $doc->act_price = $total_price;
        $doc->save();

        $response["id"] = $serviceRequest->id;
        $response["status"] = true;
        $response["messages"] = "Data berhasil disimpan";

        return response($response);
    }

    public function editpnbp($id)
    {
        $attribute = $this->MyProjects->setup("requestluaruttp");
        $request = ServiceRequest::find($id);
        $doc = ServiceRequestUutInsituDoc::find($request->spuh_doc_id);

        $requestor = Customer::find($request->requestor_id);
        // dd($request);   
        return view('requestluaruut.edit_pnbp',compact([
            'id','request', 'doc',
            'requestor',
            "attribute"
        ]));
    }
}