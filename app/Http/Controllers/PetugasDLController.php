<?php

namespace App\Http\Controllers;

use App\MasterPetugasUttp;
use App\ServiceRequestUttp;

use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PetugasDLController extends Controller
{
    private $Petugas;
    private $MyProjects;

    public function __construct() 
    {
        $this->Petugas = new MasterPetugasUttp();
        $this->MyProjects = new MyProjects();
    }

    public function index() 
    {
        $attribute = $this->MyProjects->setup("petugasdl");

        $petugas = collect(DB::select("select * from petugas_uttp pu 
            left join 
            (
            select sruid.request_id, sruid.doc_no, sruid.date_from, sruid.date_to, sruid.accepted_date,
            case when sru.lokasi_dl = 'luar' then mn.nama_negara else mk.nama end as kabkot,
            sruis.scheduled_id  
            from service_request_uttps sru 
            inner join service_request_uttp_insitu_doc sruid on sru.spuh_doc_id = sruid.id
            inner join service_request_uttp_insitu_staff sruis on sru.spuh_doc_id = sruis.doc_id 
            left join master_kabupatenkota mk on CAST(nullif(sru.inspection_kabkot_id, '') AS integer) = mk.id
            left join master_negara mn on sru.inspection_negara_id = mn.id
            where (sruid.date_from >= current_date or sruid.date_to >= current_date) and sru.status_id in (3,4,5,6,7,8,9,10,11,12,22)
            ) sch on pu.id = sch.scheduled_id
            where pu.is_active = true
            order by id"));

        // $petugas = $this->Petugas->where('is_active', true)->get();
        return view('petugasdl.index', compact('petugas','attribute'));
    }

}
