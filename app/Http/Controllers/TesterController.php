<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use App\ServiceRequestUttp;
use App\Customer;
use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\UttpInsituCheckItem;
use App\ServiceRequestUttpInsituDocCheck;
use App\HistoryUttp;
use App\ServiceRequestUttpItem;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TesterController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        //$this->MyProjects = new MyProjects();
    }

    public function index()
    {
        //$attribute = $this->MyProjects->setup("tester");

        //return view('tester.index');
        return null;
    }

    public function save(Request $request)
    {
        //$attribute = $this->MyProjects->setup("tester");

        dd($request);
        
        $rules = [];
       

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            //$file = $request->file('file_tester');
            
            //if ($file != null) {
                //$data['file_tester'] = $file->getClientOriginalName();

                /*
                $path = $file->store(
                    'skhp',
                    'public'
                );

                $data['file_tester'] = $path;
                */

            //}

            return Redirect::route('tester.ok');
        } else {
            return Redirect::route('tester');
        }
    }

    public function ok()
    {
        //$attribute = $this->MyProjects->setup("tester");

        return view('tester.ok');
    }
}