<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrderUttpTTUInspection;
use App\ServiceOrderUttpTTUInspectionItems;
use App\ServiceOrderUttpTTUInspectionBadanHitung;
use App\ServiceOrderUttpTTUPerlengkapan;
use App\ServiceOrderUttpEvaluasiTipe;

use App\Mail\Pending;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;

use App\MasterInstalasi;
use App\HistoryUttp;
use App\ServiceOrders;

class ServiceHistoryUutController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {

        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("servicehistoryuut");

        $laboratory_id = Auth::user()->laboratory_id;
        $laboratories=[1,2];
        if($laboratory_id ==1 || $laboratory_id ==12 || $laboratory_id ==13 || $laboratory_id ==14){
            $laboratories =[1,12,13,14];
        }
        elseif($laboratory_id ==2 || $laboratory_id ==18 || $laboratory_id ==19)
        {
            $laboratories = [2,18,19];

        }
        elseif($laboratory_id ==3 || $laboratory_id ==15 || $laboratory_id ==16){
            $laboratories = [3,15,16,17];
        }elseif($laboratory_id == 6 || $laboratory_id == 20 || $laboratory_id == 21 || $laboratory_id == 22)
        {
            $laboratories =[6,20,21,22];
        }else{
            $laboratories =[$laboratory_id];
        }

        $rows = ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
        ->whereHas('ServiceRequestItem', function($query) 
        {
            $query->whereIn("status_id", [13,14,16])
            ->orWhereIn("status_id",[1,0]);
        })
        ->whereHas("MasterLaboratory", function ($query) use ($laboratories){ 
            $query->whereIn("id",$laboratories);
        })
        ->whereHas('ServiceRequest', function($query) 
            {
                $query->where("lokasi_pengujian","=", 'dalam');
        })
        ->where('stat_sertifikat', '>=', 1)
        ->orWhere('stat_sertifikat',0)
        // ->orWhere('status_id',16)
        // ->whereIn('laboratory_id', $laboratory_id)
        
        ->orderBy('staff_entry_datein','desc')
        ->get();
    
        // dd($rows[0]->serviceRequestItem);//ServiceRequestItem->uuts->stdtype->uut_type);

        return view('servicehistoryuut.index',compact('rows','attribute'));
    }

    public function getByYear($tahun, Request $request)
    {
        $attribute = $this->MyProjects->setup("servicehistoryuut");

        $laboratory_id = Auth::user()->laboratory_id;
        $laboratories=[1,2];
        if($laboratory_id ==1 || $laboratory_id ==12 || $laboratory_id ==13 || $laboratory_id ==14){
            $laboratories =[1,12,13,14];
        }
        elseif($laboratory_id ==2 || $laboratory_id ==18 || $laboratory_id ==19)
        {
            $laboratories = [2,18,19];

        }
        elseif($laboratory_id ==3 || $laboratory_id ==15 || $laboratory_id ==16){
            $laboratories = [3,15,16,17];
        }elseif($laboratory_id == 6 || $laboratory_id == 20 || $laboratory_id == 21 || $laboratory_id == 22)
        {
            $laboratories =[6,20,21,22];
        }else{
            $laboratories =[$laboratory_id];
        }

        $rows = ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
        ->whereHas('ServiceRequestItem', function($query) 
        {
            $query->whereIn("status_id", [13,14,16])
            ->orWhereIn("status_id",[1,0]);
        })
        ->whereHas("MasterLaboratory", function ($query) use ($laboratories){ 
            $query->whereIn("id",$laboratories);
        })
        ->whereHas('ServiceRequest', function($query) 
            {
                $query->where("lokasi_pengujian","=", 'dalam');
        })
        ->whereRaw('extract(YEAR from staff_entry_datein) = '.$tahun)
        ->orderBy('staff_entry_datein','desc')
        ->get();

        /* Datatable **/
        // $totalData = $count->count();
        // $start = $request->input('start');
        // $length = $request->input('length');

        // $query->skip($start)->take($length);
        // $data = $count->get();

        // return response()->json([
        //     'draw' => $request->input('draw'),
        //     'recordsTotal' => $totalData,
        //     'recordsFiltered' => $totalData,
        //     'data' => $data
        // ]);

        return view('servicehistoryuut.index',compact('rows','attribute'));
    }

    public function warehouse(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        if ($request->has('id')) {
            $order = ServiceOrders::find($request->id);
        } else {
            $order = ServiceOrders::where("no_order", $request->no_order)->first();
        }

        if ($order != null) {
            ServiceOrders::whereId($order->id)->update(["stat_warehouse"=>1]);

            $history = new HistoryUut();
            $history->request_status_id = 13;
            $history->request_id = $order->service_request_id;
            $history->request_item_id = $order->service_request_item_id;
            $history->order_id = $order->id;
            $history->order_status_id = $order->stat_sertifikat;
            $history->warehouse_status_id = 1;
            $history->user_id = Auth::id();
            $history->save();

            //$this->checkAndUpdateFinishOrder($order->id);

            $response["status"] = true;
            $response["messages"] = "Alat telah dikirim ke gudang";

            
        }

        return response($response);
    }

    private function checkAndUpdateFinishOrder($id) 
    {
        $order = ServiceOrders::find($id);

        if ($order->stat_warehouse == 1 && $order->stat_sertifikat == 3) {
            ServiceRequesItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestItemInspection::where("service_request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('service_request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequest::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUut();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                $history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }

    

}
