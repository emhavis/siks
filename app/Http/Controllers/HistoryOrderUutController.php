<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Mail\CertificateDone;
use App\Mail\Cancel;
use App\ServiceOrders;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;

class HistoryOrderUutController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("historyorderuut");

        $laboratory_id = Auth::user()->laboratory_id;
        $approval_before=0;
        if(Auth::user()->user_role ==16){
            $approval_before = 2;
        }elseif(Auth::user()->user_role ==22){
            $approval_before =3;
        }elseif(Auth::user()->user_role == 25){
            $approval_before =[null,0,1,2,3,4];
        }

        /**$approval_before = Auth::user()->user_role == 16 || Auth::user()->user_role == 22 ? 1 :2;
        
        $rows = ServiceOrders::with(['ServiceRequest'])->whereIn("stat_service_order",[0,1,2,3,4])
                ->where("stat_sertifikat",">=",$approval_before);
        */
        if(is_array($approval_before)){
            $rows = ServiceOrders::with(['ServiceRequest','ServiceRequestItem'])->whereIn("stat_service_order",[null,0,1,2,3,4])
                ->whereHas('ServiceRequestItem', function($query) use ($approval_before){
                    $query->whereIn('service_request_items.status_id',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]);
                });
                // ->whereIn("stat_sertifikat",$approval_before);
            // $rows = $rows->whereIn("stat_service_order",$approval_before);
        }else{
            $rows = ServiceOrders::with(['ServiceRequest'])->whereIn("stat_service_order",[0,1,2,3,4])
                ->whereNotNull("stat_sertifikat");
                // ->where("stat_sertifikat",">=",$approval_before);
            if($approval_before ==3){
                $rows = $rows->where("stat_service_order",$approval_before)->where("kabalai_id",'>',0);
            }elseif($approval_before ==2){
                // $rows = $rows->where("stat_service_order",">=",$approval_before)->where("subkoordinator_id",'>',0);
            }
        }
        // ->orWhere("subkoordinator_id",">",0)
        $rows = $rows->orderBy('id','DESC')->get();
        // ->orderBy('kabalai_date','DESC')->get();

        $rows_kn = $rows->filter(function ($value, $key) {
            return $value->lokasi_pengujian == 'dalam';
        });
        $rows_dl = $rows->filter(function ($value, $key) {
            return $value->lokasi_pengujian == 'luar';
        });

        return view('historyorderuut.index',compact('rows','attribute', 'rows_kn', 'rows_dl'));
    }

    
    public function preview($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $blade = 'serviceuttp.skhp_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_pdf_ttu';
        }

        //dd($order);
        $view = true;
        return view($blade,compact(
            'order', 'view'
        ));
    }

    public function print($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);
        
        $file_name = 'skhp'.$order->ServiceRequest->no_order;

        $blade = 'serviceuttp.skhp_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_pdf_ttu';
        }

        $view = false;
        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo')
            ])->loadview($blade,compact(['order', 'view']));

        /*
        $file_name = 'skhp'.$order->id;
        $pdf = PDF::loadview('serviceuttp.skhp_pdf',compact('order',));
        */

        return $pdf->download('skhp.pdf');
    }

    public function downloadLampiran($id)
    {
        $order = ServiceOrders::find($id);

        if($order->path_lampiran_kalab != null && $order->file_lampiran_kalab !=null){
            return Storage::disk('public')->download($order->path_lampiran, $order->file_lampiran);
        }else{
            abort(404);
        }
    }

    public function listJson(Request $request){
        // Page Length

        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;

        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';
        //Search
        $search = $request->get('search')['value'] ? $request->get('search')['value'] : '';

        // Main Query
        $laboratory_id = Auth::user()->laboratory_id;
        $approval_before=0;
        if(Auth::user()->user_role ==16){
            $approval_before = 2;
        }elseif(Auth::user()->user_role ==22){
            $approval_before =3;
        }elseif(Auth::user()->user_role == 25){
            $approval_before =[null,0,1,2,3,4];
        }
        $searchorder = function($query) use ($search){
            if('' != $search || null != $search){
                $query->whereRaw('1=1');
                $query->orWhereRaw("service_request_items.no_order like '".$search."'");
            } 
        };
        if(is_array($approval_before)){
            $rows = ServiceOrders::with(['ServiceRequest','ServiceRequestItem' => searchorder ])
                ->join('service_requests', 'service_requests.id', '=', 'service_orders.service_request_id')
                ->leftJoin('users AS user1','user1.id','=', 'service_orders.test_by_1')
                ->leftJoin('users AS user2','user2.id','=', 'service_orders.test_by_2')
                ->leftJoin('users AS staff','staff.id','=', 'service_orders.lab_staff')
                ->leftJoin('master_request_status AS status','status.id','=', 'service_orders.service_request_items.status_id')
                ->whereIn("stat_service_order",[null,0,1,2,3,4])
                ->whereHas('ServiceRequestItem', function($query) use ($approval_before){
                    $query->whereIn('service_request_items.status_id',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]);
                })
                ->whereHas('ServiceRequestItem',$searchorder);
        }else{
            $rows = ServiceOrders::with(['ServiceRequest','ServiceRequestItem'])
            ->join('service_requests', 'service_requests.id', '=', 'service_orders.service_request_id')
            ->leftJoin('service_request_items','service_request_items.id','=','service_orders.service_request_item_id')
            ->leftJoin('master_request_status','master_request_status.id','=','service_request_items.status_id')
            ->leftJoin('users AS user1','user1.id','=', 'service_orders.test_by_1')
            ->leftJoin('users AS user2','user2.id','=', 'service_orders.test_by_2')
            ->leftJoin('users AS staff','staff.id','=', 'service_orders.lab_staff')
            
            ->whereHas('ServiceRequestItem', function($query) use ($approval_before){
                $query->leftJoin('master_request_status AS status','status.id','=', 'service_request_items.status_id')
                ->whereIn('service_request_items.status_id',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]);
            })
            ->whereHas('ServiceRequestItem',$searchorder)
            ->whereIn("stat_service_order",[0,1,2,3,4])
            ->whereNotNull("stat_sertifikat");
            if($approval_before ==3){
                $rows = $rows->where("stat_service_order",$approval_before)->where("kabalai_id",'>',0);
            }
        }
        // $rows = $rows->orderBy('id','DESC')->get();

        // Build Query
        // Main
        // $query = \DB::table('users')->select('*');

        if('' != $search || null != $search){
            $rows ->where(function($query) use ($search){
                $query->orWhereRaw("lower(tool_name) like '%".$search."%'");
                $query->orWhereRaw("lower(tool_brand) like '%".$search."%'");
                $query->orWhereRaw("lower(tool_serial_no) like '%".$search."%'");
                $query->orWhereRaw("lower(no_service) like '%".$search."%'");
            });
        }
        // $query = $rows ->where(function($query) use ($search){
        //     $query->orWhere('tool_name', 'like', "%".$search."%");
        //     $query->orWhere('tool_serial_no', 'like', "%".$search."%");
        //     $query->orWhere('no_service', 'like', "%".$search."%");
        // })
        $rows->select('service_orders.*')
        // ->addSelect()
        ->addSelect(DB::raw("CONCAT(tool_brand,'/',tool_model,'/',tool_type,'/',tool_serial_no) alat, CONCAT(user1.full_name,' & ', user2.full_name) testby, staff.full_name as diterima_oleh,
                                master_request_status.status as status
                            "));
        $orderByName = 'service_orders.id';
        switch($orderColumnIndex){
            case '0':
                $orderByName = 'staff_entry_datein';
                break;
            // case '1':
            //     $orderByName = 'email';
            //     break;
            // case '2':
            //     $orderByName = 'remember_token';
            //     break;
            // case '3':
            //     $orderByName = 'created_at';
            //     break;
            default:
                $orderByName = 'service_orders.id';
                break;
        }

        if ($request->get('tipe') == 'kn') {
            $rows = $rows->where('service_requests.lokasi_pengujian', 'dalam');
        } elseif ($request->get('tipe') == 'dl') {
            $rows = $rows->where('service_requests.lokasi_pengujian', 'luar');
        } else {
            $rows = $rows->where('service_requests.lokasi_pengujian', 'dalam');
        }
        $query = $rows->orderBy($orderByName, $orderBy);
        $recordsFiltered = $recordsTotal = $query->count();
        $data = $rows->skip($skip)->take($pageLength)->get();
        // dd($data);
        return response()->json(["draw"=> $request->draw, "recordsTotal"=> $recordsTotal, "recordsFiltered" => $recordsFiltered, 'data' => $data], 200);
    }

}
