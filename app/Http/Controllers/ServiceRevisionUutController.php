<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrders;
use App\ServiceOrderInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\ServiceRequestItemInspection;
use App\ServiceRequestItems;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrderUutTTUInspection;
use App\ServiceOrderUutTTUInspectionItems;
use App\ServiceOrderUutTTUInspectionBadanHitung;
use App\ServiceOrderUutTTUPerlengkapan;
use App\ServiceOrderUutEvaluasiTipe;

use App\Mail\Pending;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;

use App\MasterInstalasi;
use App\HistoryUttp;

class ServiceRevisionUutController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("servicerevisionuut");

        $laboratory_id = Auth::user()->laboratory_id;

        $laboratories=[1,2];
        if($laboratory_id ==1 || $laboratory_id ==12 || $laboratory_id ==13 || $laboratory_id ==14){
            $laboratories =[1,12,13,14];
        }
        elseif($laboratory_id ==2 || $laboratory_id ==18 || $laboratory_id ==19)
        {
            $laboratories = [2,18,19];

        }
        elseif($laboratory_id ==3 || $laboratory_id ==15 || $laboratory_id ==16 || $laboratory_id ==17){
            $laboratories = [3,15,16,17];
        }elseif($laboratory_id == 6 || $laboratory_id == 20 || $laboratory_id == 21 || $laboratory_id == 22)
        {
            $laboratories = [6,20,21,22];
        }else{
            $laboratories =[$laboratory_id];
        }

        $rows= ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
        ->whereHas('ServiceRequestItem', function($query) 
        {
            $query->whereIn("status_id", [13,16]);
        })
        ->where("is_finish",0)
        ->where(function($query) {
            $query->where('stat_sertifikat', 0)
                    ->orWhere('stat_sertifikat',1)
                    ->orWhereNull('stat_sertifikat');
        })
        ->whereNotNull('subkoordinator_notes')
        ->whereIn('laboratory_id', $laboratories)   
        ->orderBy('staff_entry_datein','desc')
        ->get();
        
        return view('servicerevisionuut.index',compact('rows','attribute'));
    }

    public function result($id)
    {
        $attribute = $this->MyProjects->setup("servicerevisionuut");

        $serviceOrder = ServiceOrders::find($id);

        //$users = MasterUsers::pluck('full_name', 'id');
        $users = MasterUsers::whereNotNull('petugas_uut_id')
            ->whereHas('PetugasUut', function($query) 
            {
                $query->where('flag_unit', 'snsu')
                    ->where('is_active', true);
            })
            ->pluck('full_name', 'id')->prepend('-- Pilih Pegawai --','');
        $user = MasterUsers::find(Auth::id());

        $oimls = DB::table("master_oimls")->pluck('oiml_name', 'id');
        $inspectionItems = ServiceOrderInspections::where('order_id', $id)
            ->orderBy('id', 'asc')->get();

        $sertifikat_expired_at = date(
            'Y-m-d',
            strtotime($serviceOrder->staff_entry_dateout . ' + ' .
                ($serviceOrder->ServiceRequestItem->uuts->stdtype->jangka_waktu == null ?  0 :
                    $serviceOrder->ServiceRequestItem->uuts->stdtype->jangka_waktu) .
                ' year')
        );

        $ttu = ServiceOrderUutTTUInspection::where('order_id', $id)->first();
        $ttuItems = ServiceOrderUutTTUInspectionItems::where('order_id', $id)->get();
        $badanHitungs = ServiceOrderUutTTUInspectionBadanHitung::where('order_id', $id)->get();
        $ttuPerlengkapans = ServiceOrderUutTTUPerlengkapan::where('order_id', $id)->get();
        $tipe = ServiceOrderUutEvaluasiTipe::where('order_id', $id)->first();

        return view('servicerevisionuut.result', compact(
            'serviceOrder',
            'attribute',
            'users',
            'user',
            'oimls',
            'ttu',
            'ttuItems',
            'ttuPerlengkapans',
            'badanHitungs',
            'inspectionItems',
            'sertifikat_expired_at',
            'tipe',
        ));
    }

    public function resultupload($id, Request $request)
    {
        $rules=[];
        $order = ServiceOrders::with([
            'ServiceRequest',
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
            'ServiceRequestItem.uuts.stdtype.oiml',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        if($request->hasil_uji_memenuhi ==0 || $request->hasil_uji_memenuhi ==False)
        {
            $rules['cancel_notes'] = ['required'];
            if($order->path_skhp ==null){
                $rules['file_skhp'] = ['required'];
            }
        }else{
            if( $order->path_lampiran == null){
                $rules['file_lampiran'] = ['required'];
                // $skhp= is_file(storage_path("app/public/sertifikat/".$order->path_lampiran));
            }
            elseif($order->path_skhp ==null){
                $rules['file_skhp'] = ['required'];
            }
        }

        // $rules['file_skhp'] = ['required'];
        // //$rules['test_by_2'] = ['required'];
        // $rules['file_lampiran'] = ['required'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();
        $serviceRequestUpdate = ServiceRequest::find($order->service_request_id);

        if ($validation->passes()) {

            if($order->path_lampiran ==null && $order->path_skhp ==null){
                $file = $request->file('file_skhp');
                $fileLampiran = $request->file('file_lampiran');

                $data['file_skhp'] = $file->getClientOriginalName();
                $data['file_lampiran'] = $fileLampiran->getClientOriginalName();

                $path = $file->store(
                    'skhp',
                    'public'
                );
                $pathLampiran = $fileLampiran->store(
                    'sertifikat',
                    'public'
                );

                $data['path_skhp'] = $path;
                $data['path_lampiran'] = $pathLampiran;
            }else{
                if($request->file('file_skhp') != null){
                    $file = $request->file('file_skhp');
                    $data['file_skhp'] = $file->getClientOriginalName();
                    $path = $file->store(
                        'skhp',
                        'public'
                    );
                    $data['path_skhp'] = $path;
                }if($request->file('file_lampiran') != null){
                    $fileLampiran = $request->file('file_lampiran');
                    $data['file_lampiran'] = $fileLampiran->getClientOriginalName();
                    $pathLampiran = $fileLampiran->store(
                        'sertifikat',
                        'public'
                    );
                    $data['path_lampiran'] = $pathLampiran;
                }
            }
            $unit='';
            $unitmin ='';
            $dayabaca='';

            $kapasitas_req= trim(preg_replace('/\s+/',' ', $request->get("satuan")));
            $kapasitas_min_req= trim(preg_replace('/\s+/',' ', $request->get("tool_capacity_min_unit")));
            $dayabaca_req= trim(preg_replace('/\s+/',' ', $request->get("tool_dayabaca_unit")));

            if($kapasitas_min_req =="°C" || $kapasitas_min_req =="⁰C"){
                $unitmin='&deg;C';
            }elseif($kapasitas_min_req =="°F" || $kapasitas_min_req =="⁰F"){
                $unitmin='&deg;F';
            }elseif($kapasitas_min_req =="°K" || $kapasitas_min_req == "⁰K"){
                $unitmin='&deg;K';
            }else{
                $unitmin = $kapasitas_min_req;
            }

            if($kapasitas_req =="°C" || $kapasitas_req =="⁰C"){
                $unit='&deg;C';
            }elseif($kapasitas_req =="°F" || $kapasitas_req =="⁰F"){
                $unit='&deg;F';
            }elseif($kapasitas_req =="°K" || $kapasitas_req == "⁰K"){
                $unit='&deg;K';
            }else{
                $unit = $kapasitas_req;
            }
            if($dayabaca_req =="°C" || $dayabaca_req == "⁰C"){
                $dayabaca='&deg;C';
            }elseif($dayabaca_req =="°F" || $dayabaca_req =="⁰F"){
                $dayabaca='&deg;F';
            }elseif($dayabaca_req =="°K" || $dayabaca_req == "⁰K"){
                $dayabaca='&deg;K';
            }else{
                $dayabaca = $dayabaca_req;
            }

            // $file = $request->file('file_skhp');
            // $fileLampiran = $request->file('file_lampiran');

            // $data['file_skhp'] = $file->getClientOriginalName();
            // $data['file_lampiran'] = $fileLampiran->getClientOriginalName();

            // $path = $file->store(
            //     'skhp',
            //     'public'
            // );
            // $pathLampiran = $fileLampiran->store(
            //     'sertifikat',
            //     'public'
            // );

            // $data['path_skhp'] = $path;
            // $data['path_lampiran'] = $pathLampiran;


            $data['test_by_1'] = Auth::id();
            $data['test_by_2'] = $request->get("test_by_2") > 0 ? $request->get("test_by_2") : null;
            $data['persyaratan_teknis_id'] = $request->get("persyaratan_teknis_id");
            $data['stat_service_order'] = 2;
            $data['stat_sertifikat'] = $request->get("stat_sertifikat");
            $data['hasil_uji_memenuhi'] = $request->get("hasil_uji_memenuhi"); 
            $data["tool_capacity"] = $request->get("kapasitas");
            $data["tool_capacity_unit"] = $unit; //$request->get("satuan");
            $data["tool_made_in"] = $request->get("buatan");

            $data["tool_capacity_min"] = $request->get("tool_capacity_min");
            $data["tool_capacity_min_unit"] = $unitmin; //$request->get("satuan");

            $data["tool_name"] = $request->get("tool_name");
            $data["tool_dayabaca"] = $request->get("tool_dayabaca");
            $data["tool_dayabaca_unit"] = $dayabaca;//$request->get("tool_dayabaca_unit");
            $data["tool_brand"] = $request->get("tool_brand");
            $data["tool_model"] = $request->get("tool_model");
            $data["tool_serial_no"] = $request->get("tool_serial_no");
            $data["tool_class"] = $request->get("tool_class");
            $data["tool_jumlah"] = $request->get("tool_jumlah");
            $data["is_certified"] = $request->get("is_certified");
            $data["page_num"] = $request->get("page_num");
            $data["cancel_notes"] = $request->get("cancel_notes");
            $data["subkoordinator_notes"] = null;
            
            $dataRequest["addr_sertifikat"] = $request->get("addr_sertifikat");
            $dataRequest["label_sertifikat"] = $request->get("label_sertifikat");

            if($request->get("sertifikat_expired_at") !='' || $request->get("sertifikat_expired_at") !=null){
                $data['sertifikat_expired_at'] = date('Y-m-d', strtotime($request->get("sertifikat_expired_at")));
            }

            $serviceRequestUpdate->update($dataRequest);
            $order->update($data);

            return Redirect::route('servicerevisionuut');
        } else {
            return Redirect::route('servicerevisionuut.result', $id);
        }
    }

    public function approve($id)
    {
        $attribute = $this->MyProjects->setup("serviceuttp");

        $serviceOrder = ServiceOrders::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        return view('serviceuttp.approve',compact(
            'serviceOrder', 'attribute'
        ));
    }

    public function approvesubmit($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest', 
        ])->find($id);
        $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
        $lastNo = $serviceType->last_no;
        $nextNo = intval($lastNo)+1;
        $noSertifikat = sprintf("%d", $nextNo).'/PKTN.4.8/KHP/KN/'.date("m/Y");
        $noService = $serviceType->prefix.'-'.sprintf("%05d", $nextNo);

        ServiceOrders::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            "no_service" => $noService,
            "supervisor_staff" => Auth::id(),
            "supervisor_entry_date" => date("Y-m-d"),
            "stat_service_order"=>3
        ]);

        MasterServiceType::where("id",$id)->update(["last_no"=>$nextNo]);

        $this->checkAndUpdateFinishOrder($id);
        
        return Redirect::route('serviceuttp');
    }

    public function warehouse($id)
    {
        $order = ServiceOrders::find($id);
        ServiceOrders::whereId($id)->update(["stat_warehouse"=>1]);

        /*
        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        $inspection->update([
            'status_uttp' => 1
        ]);
        */

        $history = new HistoryUttp();
        $history->request_status_id = 13;
        $history->request_id = $order->status_request_id;
        $history->request_item_id = $order->status_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = $order->stat_sertifikat;
        $history->warehouse_status_id = 1;
        $history->user_id = Auth::id();
        $history->save();

        $this->checkAndUpdateFinishOrder($id);
        
        return Redirect::route('serviceuttp');
    }

    public function download($id)
    {
        $order = ServiceOrderUttps::find($id);

        return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
    }

    public function preview($id)
    {
        $order = ServiceOrderUttps::find($id);
        /*
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);
        */
        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_pdf_ttu';
        }

        $view = true;
        //dd($order->inspections);
        return view($blade,compact(
            'order', 'qrcode_generator', 'view'
        ));
    }

    public function print($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $file_name = 'skhp'.$order->ServiceRequest->no_order;

        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_pdf_ttu';
        }

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo')
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));

        return $pdf->download('skhp.pdf');
    }

    public function previewTipe($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $otherOrders = DB::table('uttp_inspection_prices')
            ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->join('service_order_uttps', 'service_order_uttps.service_request_item_inspection_id', '=', 'service_request_uttp_item_inspections.id')
            ->where('uttp_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
            ->where('uttp_inspection_price_types.uttp_type_id', $order->ServiceRequestItem->uuts->type_id)
            ->where('service_order_uttps.ujitipe_completed', true)
            ->selectRaw('service_order_uttps.*')
            ->get();

        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_pdf_tipe';

        $view = true;
        //dd($order);
        return view($blade,compact(
            'order', 'otherOrders', 'qrcode_generator', 'view'
        ));
    }

    public function printTipe($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $file_name = 'skhp'.$order->ServiceRequest->no_order;

        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_pdf_tipe';

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo')
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));

        return $pdf->download('skhp.pdf');
    }

    private function checkAndUpdateFinishOrder($id) 
    {
        $order = ServiceOrderUttps::find($id);

        /*
        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        if ($inspection->status_sertifikat == 1 && $inspection->status_uttp == 1) {
            $inspection->update(['status_id' => 14]); 
        }

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_14 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
        }
        */
        if ($order->stat_warehouse == 1 && $order->stat_sertifikat == 3) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestUttpItemInspection::where("request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUttp();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                $history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }

    public function pending($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.pending',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmpending($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $order->update([
            "pending_status" => 1,
            "pending_created" => date('Y-m-d H:i:s'),
            "pending_notes" => $request->get("pending_notes"),
            //"pending_estimated" => date("Y-m-d", strtotime($request->get("pending_estimated"))),
        ]);

        ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>15]);

        $customerEmail = $order->ServiceRequest->requestor->email;
        //Mail::to($customerEmail)->send(new Pending($order));
        ProcessEmailJob::dispatch($customerEmail, new Pending($order))->onQueue('emails');

        return Redirect::route('serviceuttp');
    }

    public function continue($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.continue',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcontinue($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $order->update([
            "pending_status" => 0,
            "pending_ended" => date('Y-m-d H:i:s'),
        ]);

        ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>12]);

        return Redirect::route('serviceuttp');
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.cancel',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcancel($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $order->update([
            "cancel_at" => date('Y-m-d H:i:s'),
            "cancel_notes" => $request->get("cancel_notes"),
            "cancel_id" => Auth::id(),
            "stat_service_order" => 4,
            "stat_sertifikat" => 0,
            //"status_id" => 16,
        ]);

        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        $inspection->update(['status_id' => 16]); 

        return Redirect::route('serviceuttp');
    }

    public function returnback(){
        $attribute = $this->MyProjects->setup("servicerevisionuut");

        $laboratory_id = Auth::user()->laboratory_id;

        $laboratories=[1,2];
        if($laboratory_id ==1 || $laboratory_id ==12 || $laboratory_id ==13 || $laboratory_id ==14){
            $laboratories =[1,12,13,14];
        }
        elseif($laboratory_id ==2 || $laboratory_id ==18 || $laboratory_id ==19)
        {
            $laboratories = [2,18,19];

        }
        elseif($laboratory_id ==3 || $laboratory_id ==15 || $laboratory_id ==16 || $laboratory_id ==17){
            $laboratories = [3,15,16,17];
        }elseif($laboratory_id == 6 || $laboratory_id == 20 || $laboratory_id == 21 || $laboratory_id == 22)
        {
            $laboratories = [6,20,21,22];
        }else{
            $laboratories =[$laboratory_id];
        }

        $rows= ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
        ->whereHas('ServiceRequestItem', function($query) 
        {
            $query->where("status_id",">=", 13);
        })
        ->where("is_finish",0)
        ->where(function($query) {
            $query->whereRaw('stat_sertifikat > 0');
        })
        ->whereNull('subkoordinator_notes')
        ->whereIn('laboratory_id', $laboratories)   
        ->orderBy('staff_entry_datein','desc')
        ->get();
        
        return view('servicerevisionuut.index_return', ['rows' => $rows, 'attribute' => $attribute]);
    }

    public function getOrders(Request $request){
        // Page Length
        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;
        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';

        $qparams = '';
        if($request->has('search')){
            $sv =strtolower($request->get('search')['value']);
            $qparams = $sv;
        }

        $laboratory_id = Auth::user()->laboratory_id;

        $laboratories=[1,2];
        if($laboratory_id ==1 || $laboratory_id ==12 || $laboratory_id ==13 || $laboratory_id ==14){
            $laboratories =[1,12,13,14];
        }
        elseif($laboratory_id ==2 || $laboratory_id ==18 || $laboratory_id ==19)
        {
            $laboratories = [2,18,19];

        }
        elseif($laboratory_id ==3 || $laboratory_id ==15 || $laboratory_id ==16 || $laboratory_id ==17){
            $laboratories = [3,15,16,17];
        }elseif($laboratory_id == 6 || $laboratory_id == 20 || $laboratory_id == 21 || $laboratory_id == 22)
        {
            $laboratories = [6,20,21,22];
        }else{
            $laboratories =[$laboratory_id];
        }

        $cond = function($query) use ($qparams){
            $query->whereRaw("status_id >= 13");
            if($qparams != '' || null != $qparams){
                $query->whereRaw("lower(no_order) like '%".$qparams."%'");
            }
        };

        $rows= ServiceOrders::with(['ServiceRequestItem' => $cond,'ServiceRequest','ServiceRequest.requestor'])
        // ->join('service_requests','service_requests.id','=','service_orders.service_request_id')
        // ->join('service_request_items','service_request_items.id','=','service_orders.service_request_item_id')
        ->whereIn("stat_service_order",[0,1,2,3,4])
        ->whereHas('ServiceRequestItem', $cond)
        ->where("is_finish",0) ->distinct()
        ->where(function($query) use ($qparams){
            $query->whereRaw('stat_sertifikat > 0');
        })
        ->whereNull('subkoordinator_notes')
        ->whereIn('laboratory_id', $laboratories);

        $query = $rows->orderBy('id',$orderBy);
        $recordsFiltered = $recordsTotal = $query->count();
        $data = $rows->skip($skip)->take($pageLength)->get();

        return response()->json(["draw"=> $request->draw, "recordsTotal"=> $recordsTotal, "recordsFiltered" => $recordsFiltered, 'data' => $data], 200);
    }

    public function sendtolab($id,$oid){
        $data = ServiceRequestItem::find($id);
        
        $order = ServiceOrders::find($oid);
        $order->update([
            'stat_sertifikat' => 0,
            'stat_service_order' => 1,
        ]);
        dd($oid);
        return redirect()->route('servicerevisionuut.back');
    }


}
