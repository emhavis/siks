<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ServiceRequestUttp;
use App\ServiceRequestUttpItem;
use App\ServiceRequestUttpItemInspection;
use App\ServiceOrderUttps;

use App\Uttp;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\MyClass\MyProjects;
use App\MasterServiceType;
use App\MasterDocTypes;
use App\MasterDocNumber;
use App\MasterInstalasi;
use App\MasterNegara;

use App\ServiceBooking;
use App\ServiceBookingItem;
use App\ServiceBookingItemInspection;
use App\ServiceBookingItemTTUPerlengkapan;

use App\Customer;

use App\UttpInspectionPrice;
use App\Holiday;

use App\ServiceRequestUttpStaff;
use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\ServiceRequestUttpItemTTUPerlengkapan;

use App\Mail\Invoice;
use App\Mail\Receipt;
use App\Mail\BookingConfirmation;
use Illuminate\Support\Facades\Mail;

use App\HistoryUttp;

use App\Jobs\ProcessEmailJob;

//use App\MasterServiceTypeDocNumber;

use PDF;

class RequestHistoryUttpController extends Controller
{
    // protected $StandardInspectionPrice;
    protected $MyProjects;
    protected $MasterDocTypes;
    protected $MasterServiceType;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        $this->MasterServiceType = new MasterServiceType();
        $this->MasterDocTypes = new MasterDocTypes();

        $this->ServiceBooking = new ServiceBooking();
        $this->ServiceRequestUttp = new ServiceRequestUttp();
        $this->ServiceRequestUttpItem = new ServiceRequestUttpItem();
        $this->ServiceRequestUttpItemInspection = new ServiceRequestUttpItemInspection();
        $this->ServiceOrderUttp = new ServiceOrderUttps();

        $this->Customer = new Customer();

        $this->MasterInstalasi = new MasterInstalasi();
        $this->Holiday = new Holiday();

        $this->MasterNegara = new MasterNegara();
        $this->Uttp = new Uttp();
     }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("requesthistoryuttp");

        $year = $request->has('tahun') ? $request->get('tahun') : date("Y");
        $type = $request->has('type') ? $request->get('type') : 'kn';

        //$statuses = DB::table('master_request_status')->orderBy('id','asc')->pluck('status', 'id');
        if($type == 'kn'){

            $rows_process = ServiceRequestUttpItem::with([
                'serviceRequest' => function ($query)
                {
                    $query->orderBy('received_date', 'desc');
                }
            ])->whereHas('serviceRequest', function($query) use ($year) {
                $query->where('lokasi_pengujian', 'dalam')
                    ->whereRaw("to_char(received_date, 'YYYY') = '" . $year . "'");
            })
            ->whereIn('status_id', [9,10,11,12,13,15])
            ->get();
            
            $rows_done = ServiceOrderUttps::whereHas('ServiceRequestItem', function($query) 
                {
                    $query->whereIn("status_id", [14,16]);
                })
                ->whereHas('ServiceRequest', function($query) use ($year) 
                {
                    $query->where("lokasi_pengujian", 'dalam')
                    ->whereRaw("to_char(received_date, 'YYYY') = '" . $year . "'");
                })
                //->where('stat_sertifikat', 2)
                ->orderBy('staff_entry_datein','desc')->get();

            
            return view('requesthistoryuttp.index', compact(
                
                'rows_process', 
                'rows_done', 
                'year',
                'type',

                'attribute'));
        } else {

            $rows_process = ServiceRequestUttpItem::with([
                'serviceRequest' => function ($query)
                {
                    $query->orderBy('received_date', 'desc');
                }
            ])->whereHas('serviceRequest', function($query) use ($year) {
                $query->where('lokasi_pengujian', 'luar')
                    ->whereRaw("to_char(received_date, 'YYYY') = '" . $year . "'");
            })
            ->whereIn('status_id', [9,10,11,12,13,15])
            ->get();
            
            $rows_done = ServiceOrderUttps::whereHas('ServiceRequestItem', function($query) 
                {
                    $query->whereIn("status_id", [14,16]);
                })
                ->whereHas('ServiceRequest', function($query) use ($year) 
                {
                    $query->where("lokasi_pengujian", 'luar')
                    ->whereRaw("to_char(received_date, 'YYYY') = '" . $year . "'");
                })
                //->where('stat_sertifikat', 2)
                ->orderBy('staff_entry_datein','desc')->get();

            return view('requesthistoryluaruttp.index', compact(
            
                'rows_process', 
                'rows_done', 
                'year',
                'type',

                'attribute'));

            // return view('dinasluar.index', compact(
            //     'rows_pendaftaran_luar',
            //     'rows_item_luar',
            //     'rows_penagihan_luar',
            //     'rows_validasi_luar',
            //     'bookings_luar',
            //     'attribute'));
        }
        
    }

}
