<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\MasterTemplate;
use App\SurveyPage;
use App\SurveyQuestion;
use App\Survey;
use App\SurveyInput;

use Illuminate\Support\Facades\DB;

use Maatwebsite\Excel\Facades\Excel;
use App\Excel\Exports\SurveyResultExport;

use Carbon\Carbon;

class SurveyResultUutController extends Controller
{
    private $MyProjects;
    private $SurveyQuestion;
    private $SurveyPage;
    private $Survey;
    //private $SurveyInput;
    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index(Request $request) {
        $surveys = Survey::all()->pluck('title', 'id');

        if($request->has('survey_id')) {
            $survey = Survey::find($request->input('survey_id'));
        } else {
            $survey = Survey::where('active', true)->first();
        }

        // $start = date("Y-m-d", strtotime("first day of january " . date('Y')));
        $start = date("Y-m-01");
        $end = date("Y-m-d");
        $qend = date("Y-m-d",  strtotime($end . ' +1 day'));

        $questions = SurveyQuestion::whereHas('page', function($query) use ($survey) 
            {
                $query->where("survey_id", $survey->id);
            })
            ->orderBy('sequence')
            ->get();

        $inputs = DB::table('survey_input')
            ->join('service_requests', 'survey_input.request_uut_id', '=', 'service_requests.id')
            ->where('survey_id', $survey->id);

        $selectRaw = "survey_input.id input_id,
            service_requests.jenis_layanan jenis_layanan, 
            case when service_requests.lokasi_pengujian = 'luar' then 'DL' else 'KN' end lokasi_pengujian,
            survey_input.created_at, 
            (select srui.no_order from  
                service_orders sou 
                inner join service_request_items srui on sou.service_request_item_id = srui.id
                where sou.id = survey_input.order_uut_id limit 1) no_order ";

        foreach($questions as $q) {
            if ($q->question_type == 'file') {
                $rawQ = "(
                    select value_filename
                    from survey_input_line where input_id = survey_input.id and question_id = " . $q->id . "
                    ) as input_value_" . $q->id;
            } else {
                $rawQ = "(
                    select case when value_number is not null then cast(value_number as varchar) else 
                    coalesce(value_text, value_radio, value_checkbox, value_textarea) end
                    from survey_input_line where input_id = survey_input.id and question_id = " . $q->id . "
                    ) as input_value_" . $q->id;
            }

            $selectRaw .= ", " . $rawQ;
        }
        $selectRaw .= 
            ", case when service_requests.lokasi_pengujian = 'dalam' then '' else  
                (select coalesce(pu.nama, '') from service_request_uut_staff srus 
                left join petugas_uttp pu on srus.scheduled_id = pu.id 
                where srus.request_id = service_requests.id
                order by srus.id asc limit 1)
            end as petugas_1, 
            case when service_requests.lokasi_pengujian = 'dalam' then '' else  
                (select coalesce(pu.nama, '') from service_request_uut_staff srus 
                left join petugas_uttp pu on srus.scheduled_id = pu.id 
                where srus.request_id = service_requests.id
                order by srus.id desc limit 1)
            end as petugas_2 ";

        $inputs = $inputs->selectRaw($selectRaw);
            
        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            $qend = date("Y-m-d",  strtotime($request->get('end_date') . ' +1 day'));

            //$inputs = $inputs->whereBetween('survey_input.created_at', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            //$inputs = $inputs->where('survey_input.created_at', '>=', $start);;
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            $qend = date("Y-m-d",  strtotime($request->get('end_date') . ' +1 day'));
            
            //$inputs = $inputs->where('survey_input.created_at', '<=', $end);
        }
        $inputs = $inputs->whereBetween('survey_input.created_at', [$start, $end]);
        $inputs = $inputs->where('service_requests.lokasi_pengujian', $request->get('lokasi'));

        $attribute = $this->MyProjects->setup("surveyresult");

        $rows = $inputs->get();
        $rows = collect($rows)->map(function($x){ return (array) $x; })->toArray(); 

        return view('surveyresultuut.index', compact(['surveys', 'survey','attribute', 'rows', 'questions',
        'start', 'end']));
    }

    public function export(Request $request) {
        
        if($request->has('survey_id')) {
            $survey = Survey::find($request->input('survey_id'));
        } else {
            $survey = Survey::where('active', true)->first();
        }

        $start = null;
        $end = null;
        $qend = date("Y-m-d",  strtotime($end . ' +1 day'));

        $questions = SurveyQuestion::whereHas('page', function($query) use ($survey) 
            {
                $query->where("survey_id", $survey->id);
            })
            ->orderBy('sequence')
            ->get();

        $inputs = DB::table('survey_input')
            ->join('service_requests', 'survey_input.request_uut_id', '=', 'service_requests.id')
            ->where('survey_id', $survey->id);

        $selectRaw = "survey_input.id input_id,
            service_requests.jenis_layanan jenis_layanan, 
            case when service_requests.lokasi_pengujian = 'luar' then 'DL' else 'KN' end lokasi_pengujian,
            survey_input.created_at, 
            (select srui.no_order from  
                service_orders sou 
                inner join service_request_items srui on sou.service_request_item_id = srui.id
                where sou.id = survey_input.order_uut_id limit 1) no_order ";

        foreach($questions as $q) {
            if ($q->question_type == 'file') {
                $rawQ = "(
                    select value_filename
                    from survey_input_line where input_id = survey_input.id and question_id = " . $q->id . "
                    ) as input_value_" . $q->id;
            } else {
                $rawQ = "(
                    select case when value_number is not null then cast(value_number as varchar) else 
                    coalesce(value_text, value_radio, value_checkbox, value_textarea) end
                    from survey_input_line where input_id = survey_input.id and question_id = " . $q->id . "
                    ) as input_value_" . $q->id;
            }

            $selectRaw .= ", " . $rawQ;
        }

        $selectRaw .= 
            ", case when service_requests.lokasi_pengujian = 'dalam' then '' else  
                (select coalesce(pu.nama, '') from service_request_uut_staff srus 
                left join petugas_uttp pu on srus.scheduled_id = pu.id 
                where srus.request_id = service_requests.id
                order by srus.id asc limit 1)
            end as petugas_1, 
            case when service_requests.lokasi_pengujian = 'dalam' then '' else  
                (select coalesce(pu.nama, '') from service_request_uut_staff srus 
                left join petugas_uttp pu on srus.scheduled_id = pu.id 
                where srus.request_id = service_requests.id
                order by srus.id desc limit 1)
            end as petugas_2 ";

        $inputs = $inputs->selectRaw($selectRaw);
            
        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            $qend = date("Y-m-d",  strtotime($request->get('end_date') . ' +1 day'));

            $inputs = $inputs->whereBetween('survey_input.created_at', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            $inputs = $inputs->where('survey_input.created_at', '>=', $start);;
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            $qend = date("Y-m-d",  strtotime($request->get('end_date') . ' +1 day'));
            
            $inputs = $inputs->where('survey_input.created_at', '<=', $end);
        }
        $inputs = $inputs->whereBetween('survey_input.created_at', [$start, $qend]);
        $inputs = $inputs->where('service_requests.lokasi_pengujian', $request->get('lokasi'));

        $rows = $inputs->get();
        $rows = collect($rows)->map(function($x){ return (array) $x; })->toArray(); 

        return Excel::download(new SurveyResultExport($rows, $questions),'Hasil Survey.xlsx');
    }
}