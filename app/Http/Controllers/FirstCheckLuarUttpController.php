<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use App\ServiceRequestUttp;
use App\Customer;
use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\UttpInsituCheckItem;
use App\ServiceRequestUttpInsituDocCheck;
use App\HistoryUttp;
use App\ServiceRequestUttpItem;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class FirstCheckLuarUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("firstcheckluaruttp");

        $petugas_id = Auth::user()->petugas_uttp_id;
        if ($petugas_id == null) {
            $petugas_id = Auth::user()->petugas_uut_id;
        }

        $rows = ServiceRequestUttp::where('status_id', 4)
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->where('service_request_uttp_insitu_staff.scheduled_id', $petugas_id)
            ->select('service_request_uttps.*')
            ->distinct()
            ->orderBy('received_date','desc')->get();

        //dd($rows);

        return view('firstcheckluaruttp.index',compact('rows','attribute'));
    }

    public function check($id) 
    {
        $attribute = $this->MyProjects->setup("firstcheckluaruttp");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        return view('firstcheckluaruttp.check', compact(['request', 'requestor', 'doc', 'staffs',  'attribute']));
    }

    public function downloadBukti($docId)
    {
        $doc = ServiceRequestUttpInsituDoc::find($docId);
        
        return Storage::disk('public')->download($doc->path_bukti_pemeriksaan_awal, $doc->file_bukti_pemeriksaan_awal);
    }

    public function savecheck($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("firstcheckluaruttp");

        $requestEntity = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        $rules = [];
        if ($request->get('submit_val') == 1 
            && $doc->path_bukti_pemeriksaan_awal == null
            && $request->get('is_siap_petugas') == 'ya') {
            $rules['file_bukti_pemeriksaan_awal'] = ['required','mimes:pdf,jpg,jpeg,png'];
        } else {
            $rules['file_bukti_pemeriksaan_awal'] = ['mimes:pdf,jpg,jpeg,png'];
        }

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $file = $request->file('file_bukti_pemeriksaan_awal');
            
            if ($file != null) {
                $data['file_bukti_pemeriksaan_awal'] = $file->getClientOriginalName();

                $path = $file->store(
                    'skhp',
                    'public'
                );

                $data['path_bukti_pemeriksaan_awal'] = $path;

                ServiceRequestUttpInsituDoc::where('id', $doc->id)->update([
                    'file_bukti_pemeriksaan_awal' => $data['file_bukti_pemeriksaan_awal'],
                    'path_bukti_pemeriksaan_awal' => $data['path_bukti_pemeriksaan_awal'],
                ]);
            }

            $data['is_siap_petugas'] = $request->get('is_siap_petugas') === "ya" ? true :
                    ($request->get('is_siap_petugas') === "tidak" ? false : null);
            $data['keterangan_tidak_siap'] = $request->get('keterangan_tidak_siap');

            if ($data['is_siap_petugas'] == true) {
                $data['is_accepted'] = $request->get('is_accepted') === "ya" ? true :
                        ($request->get('is_accepted') === "tidak" ? false : null);
                $data['keterangan_tidak_lengkap'] = $request->get('keterangan_tidak_lengkap');
    
                $doc->update([
                    'is_siap_petugas' => $data['is_siap_petugas'],
                    'keterangan_tidak_siap' => null,
                    'is_accepted' => $data['is_accepted'],
                    'keterangan_tidak_lengkap' => $data['keterangan_tidak_lengkap'],
                ]);
            } else {
                $doc->update([
                    'is_siap_petugas' => $data['is_siap_petugas'],
                    'keterangan_tidak_siap' => $data['keterangan_tidak_siap'],
                ]);
            }

            if ($request->get('submit_val') == 1)
            {
                $status = 3; //24 tidak lengkap
                if ($data['is_siap_petugas']) {
                    if ($data['is_accepted']) {
                        $status = 22;
                    }
                } 
                $requestEntity->update([
                    "status_id" => $status,

                    "is_integritas" => true,
                    "integritas_at" => date("Y-m-d H:i:s.u"),
                ]);

                $doc->update([
                    "checked_at" => date("Y-m-d H:i:s.u"),
                    "checked_by" => Auth::id(),
                ]);

                if ($status != 3) {
                    $items = ServiceRequestUttpItem::where("request_id", $id)->get();
                    foreach($items as $item) {
                        $history = new HistoryUttp();
                        $history->request_status_id = $status; 
                        $history->request_id = $id;
                        $history->request_item_id = $item->id;
                        $history->user_id = Auth::id();
                        $history->save();

                        $item->status_id = $status;
                        $item->save();
                    }
                }
            }
            return Redirect::route('firstcheckluaruttp');
        } else {
            return Redirect::route('firstcheckluaruttp.check', $id);
        }
    }

    
}