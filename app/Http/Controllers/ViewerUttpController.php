<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use App\ServiceOrderUttps;
use Illuminate\Support\Facades\DB;

class ViewerUttpController extends Controller
{
    private $Article;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index(){
        $attribute = $this->MyProjects->setup("vieweruttp");

        $data =  $rows = ServiceOrderUttps::whereRaw('1=1')
            ->join('service_request_uttps as sru','service_order_uttps.service_request_id','=','sru.id')
            ->join('service_request_uttp_items as srui','service_order_uttps.service_request_item_id','=','srui.id')
            ->join('users as kb', 'service_order_uttps.kabalai_id', '=', 'kb.id')
            ->join('petugas_uttp as pukb', 'kb.petugas_uttp_id', '=', 'pukb.id')
            ->join('master_uttp_types as mut', 'service_order_uttps.tool_type_id', '=', 'mut.id')
            ->where('service_order_uttps.stat_service_order', 3)
            ->where('service_order_uttps.stat_sertifikat', 3)
            ->whereIn('sru.service_type_id', [6,7])
            ->whereNotNull('service_order_uttps.no_surat_tipe')
            ->addSelect(
                DB::raw(
                    'service_order_uttps.id, 
                    service_order_uttps.tool_brand,
                    service_order_uttps.tool_model,
                    service_order_uttps.tool_type, 
                    service_order_uttps.tool_serial_no,
                    service_order_uttps.no_sertifikat,
                    service_order_uttps.no_sertifikat_tipe,
                    service_order_uttps.no_surat_tipe,
                    sru.label_sertifikat,
                    service_order_uttps.kabalai_date,
                    pukb.nama as kabalai,
                    mut.uttp_type,
                    mut.uttp_type_certificate'
                )
            )
            ->orderBy('service_order_uttps.no_surat_tipe','asc');

        $data = $data->get();

        return View('vieweruttp.index',compact('data','attribute'));
    }
}