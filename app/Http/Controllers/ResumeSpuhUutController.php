<?php 

namespace App\Http\Controllers;

use App\Excel\Exports\UsersExport;
use App\Excel\Exports\UttpsExport;
use App\Excel\Imports\UsersImport;
use App\Excel\Exports\ResumeDataSpuhUUTExport;
use App\MasterInstalasi;
use Illuminate\Http\Request;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\ServiceOrderUttps;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\ServiceRequestUutInsituDoc;

class ResumeSpuhUutController extends Controller
{
    protected $MyProjects;
    protected $s_date,$str_date,$ins;
    protected $e_date;
    protected $MasterInstalasi;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("resumespuhuut");

        $year = $request->get('year') ?? date('Y'); 
        
        // $rows = ServiceRequestUttp::whereIn('status_id', [11,12,13,14,19,20,22])
        //     ->whereNotNull('spuh_doc_id')
        //     ->whereNotNull('spuh_spt')
        //     ->select('service_request_uttps.*')
        //     ->orderBy('received_date','desc');


        $rows = ServiceRequestUutInsituDoc::join('service_requests as sru','service_request_uut_insitu_doc.request_id','=','sru.id')
            ->leftJoin('master_provinsi', 'master_provinsi.id', '=', 'sru.inspection_prov_id')
            //->leftJoin('master_negara', 'master_negara.id', '=', 'sru.inspection_negara_id')
            ->join('users_customers', 'users_customers.id', '=', 'sru.requestor_id')
            ->where('service_request_uut_insitu_doc.jenis', 'inisial')
            ->where('service_request_uut_insitu_doc.is_accepted', true)
            ->whereIn('sru.status_id', [11,12,13,14,19,20,22])
            ->whereNotNull('sru.spuh_doc_id')
            ->whereNotNull('sru.spuh_spt')
            //->select('service_request_uttp_insitu_doc.*')
            ->select('service_request_uut_insitu_doc.id')
            ->addSelect(
                DB::raw(
                    "service_request_uut_insitu_doc.doc_no,
                    service_request_uut_insitu_doc.request_id,
                    sru.no_order,
                    sru.no_register,
                    master_provinsi.nama as provinsi,
                    users_customers.full_name,
                    sru.label_sertifikat, 
                    service_request_uut_insitu_doc.date_from,
                    service_request_uut_insitu_doc.date_to,
                    sru.spuh_payment_date,
                    sru.spuh_payment_valid_date as valid,
                    sru.is_staff_paid,
                    sru.spuh_staff,
                    sru.spuh_rate,
                    sru.keuangan_perusahaan,
                    sru.keuangan_pic,
                    sru.keuangan_hp,
                    sru.keuangan_jabatan,
                    (SELECT DATE_PART('day', service_request_uut_insitu_doc.date_to::timestamp - service_request_uut_insitu_doc.date_from::timestamp)) as jml_hari,
                    service_request_uut_insitu_doc.date_from, service_request_uut_insitu_doc.date_to,
                    (select nama from petugas_uttp pu 
                    where id = (select scheduled_id 
                        from service_request_uut_insitu_staff sruis 
                        where doc_id = service_request_uut_insitu_doc.id order by id asc limit 1)) nama_petugas_1,
                    (select nama from petugas_uttp pu 
                    where id = (select scheduled_id 
                        from service_request_uut_insitu_staff sruis 
                        where doc_id = service_request_uut_insitu_doc.id order by id desc limit 1)) nama_petugas_2,
                    (select nip from petugas_uttp pu 
                    where id = (select scheduled_id 
                        from service_request_uut_insitu_staff sruis 
                        where doc_id = service_request_uut_insitu_doc.id order by id asc limit 1)) nip_petugas_1,
                    (select nip from petugas_uttp pu 
                    where id = (select scheduled_id 
                        from service_request_uut_insitu_staff sruis 
                        where doc_id = service_request_uut_insitu_doc.id order by id desc limit 1)) nip_petugas_2,
                    service_request_uut_insitu_doc.price
                    "
                )
            )
            ->orderBy('sru.received_date','desc');

        if ($year) {
            $rows = $rows->where('service_request_uut_insitu_doc.spuh_year', $year);
        } 
        
        
        $rows = $rows->get();
        //dd($rows->toSql());
        // dd($rows[0]);
        $this->data = $rows;
        return view('resumespuhuut.index',compact(['rows','attribute', 'year']));
    }

    public function fileImportExport()
    {
       return view('file-import');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileImport(Request $request) 
    {
        Excel::import(new UsersImport, $request->file('file')->store('temp'));
        return back();
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileExport(Request $request) 
    {
        
        $rows = ServiceRequestUutInsituDoc::join('service_requests as sru','service_request_uut_insitu_doc.request_id','=','sru.id')
            ->join('master_provinsi', 'master_provinsi.id', '=', 'sru.inspection_prov_id')
            ->join('users_customers', 'users_customers.id', '=', 'sru.requestor_id')
            ->where('service_request_uut_insitu_doc.jenis', 'inisial')
            ->where('service_request_uut_insitu_doc.is_accepted', true)
            ->whereIn('sru.status_id', [11,12,13,14,19,20,22])
            ->whereNotNull('sru.spuh_doc_id')
            ->whereNotNull('sru.spuh_spt')
            //->select('service_request_uttp_insitu_doc.*')
            //->select('service_request_uttp_insitu_doc.id')
            ->addSelect(
                DB::raw(
                    "service_request_uut_insitu_doc.doc_no,
                    sru.no_order, sru.no_register,
                    master_provinsi.nama as provinsi,
                    sru.label_sertifikat,
                    users_customers.full_name,
                    service_request_uut_insitu_doc.date_from,
                    service_request_uut_insitu_doc.date_to ,
                    (SELECT DATE_PART('day', service_request_uut_insitu_doc.date_to::timestamp + INTERVAL '1 day' - service_request_uut_insitu_doc.date_from::timestamp)) as jml_hari,
                    (select nama from petugas_uttp pu 
                    where id = (select scheduled_id 
                        from service_request_uut_insitu_staff sruis 
                        where doc_id = service_request_uut_insitu_doc.id order by id asc limit 1)) || ' (' ||
                    (select nip from petugas_uttp pu 
                        where id = (select scheduled_id 
                            from service_request_uut_insitu_staff sruis 
                            where doc_id = service_request_uut_insitu_doc.id order by id asc limit 1)) || ')' nama_petugas_1,
                    (select nama from petugas_uttp pu 
                    where id = (select scheduled_id 
                        from service_request_uut_insitu_staff sruis 
                        where doc_id = service_request_uut_insitu_doc.id order by id desc limit 1)) || ' (' ||
                    (select nip from petugas_uttp pu 
                    where id = (select scheduled_id 
                        from service_request_uut_insitu_staff sruis 
                        where doc_id = service_request_uut_insitu_doc.id order by id desc limit 1)) || ')' nama_petugas_2,
                    sru.spuh_staff,
                    sru.spuh_rate,
                    service_request_uut_insitu_doc.price
                    "
                )
            )
            ->orderBy('sru.received_date','desc');

        if ($request->get('year') != null) {
            $rows = $rows->where('service_request_uut_insitu_doc.spuh_year', $request->get('year'));
        }
        if ($request->has('id_selected') && array_count_values($request->get('id_selected')) > 0) {
            $rows = $rows->whereIn('service_request_uut_insitu_doc.id',$request->get('id_selected'));
        } 
        
        $rows = $rows->get();
        $this->data = $rows;
        
        return Excel::download(new ResumeDataSpuhUUTExport($this->data),'Resume-SPUH-UUT.xlsx');
    }    

    public function paid($id, $state){
        $s = ServiceRequest::whereId($id)
        ->update([
            "is_staff_paid"=>$state,
        ]);
        if($s){
            return response([true,"success"]);
        }else{return response([false,"error"]);}
    }

}