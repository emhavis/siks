<?php 

namespace App\Http\Controllers;

use App\Excel\Exports\UsersUUTExport;
use App\Excel\Exports\UUTExport;
use App\Excel\Imports\UsersUUTImport;
use App\MasterLaboratory;
use Illuminate\Http\Request;
use App\ServiceRequestItemInspection;
use App\ServiceRequestItem;
use App\ServiceOrders;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ResumeUutController extends Controller
{
    protected $MyProjects;
    protected $s_date,$str_date,$ins;
    protected $e_date;
    protected $MasterInstalasi;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        $this->MasterInstalasi = new MasterLaboratory();
        $this->MasterServiceType = new MasterServiceType();
    }

    public function index(Request $request)
    {
        $this->str_date = $request->get('start_date'); 
        $this->e_date = $request->get('end_date');
        $attribute = $this->MyProjects->setup("resumeuut");
        $instalasi = $this->MasterInstalasi->where('tipe','=','standard')->pluck('nama_lab','id');
        $types = $this->MasterServiceType
            ->where('id', '<', 3)
            ->orderBy('id')->pluck('service_type', 'id');

        $rows = ServiceOrders::whereRaw('1=1')
        ->join('service_requests as sru','service_orders.service_request_id','=','sru.id')
        ->join('service_request_items as srui','service_orders.service_request_item_id','=','srui.id')
        ->join('service_request_item_inspections as sruii','srui.id','=','sruii.service_request_item_id')
        ->join('users_customers as uc','sru.requestor_id','=','uc.id')
        ->join('standard_uut as u','service_orders.uut_id','=','u.id')
        ->join('master_standard_types as mut','u.type_id','=','mut.id')
        ->join('uut_inspection_prices as uip','sruii.inspection_price_id','=','uip.id')
        ->join('master_laboratory as mi','service_orders.laboratory_id','=','mi.id')
        ->join('user_uut_owners as ow','ow.owner_id','=','u.owner_id')
        ->join('uut_owners as o','o.id','=','ow.owner_id')
        ->select(DB::raw('distinct on (srui.id) srui.id'))
        ->addSelect(
            DB::raw(
                'srui.no_order,u.owner_id as idu,o.nama as n_daftar,uc.full_name, sru.label_sertifikat, (
                    select
                        count(s_srui.id)
                    from
                        service_request_items s_srui
                    where
                        s_srui.id = sru.id) as jumlah_alat,
                    mut.uut_type as rincian_alat,
                    uip.inspection_type as obyek_pengujian,
                    sruii.price * sruii.quantity as tarif_pnbp,
                    sru.total_price as total_pnbp,
                    sru.billing_code,
                    sru.payment_code,
                    sru.no_register,
                    srui.order_at,
                    service_orders.staff_entry_dateout,
                case 
                when
                    service_orders.kabalai_date is null 
                        then \'ONP\'
                when 
                    service_orders.kabalai_date is not null and
                    weekdays_sql(srui.order_at::date,service_orders.kabalai_date) <= (
                        case
                            WHEN srui.sla_overide > 1 THEN srui.sla_overide
                            ELSE mi.sla_day
                        END
                        )
                        then \'ONT\'
                else \'LATE\'
                end as sla_status,  mi.nama_lab, CASE
                WHEN sru.service_type_id = 1 THEN \'Perifikasi\'
                ELSE \'Kalibrasi\' END AS Layanan
                '))
                ->orderBy('srui.id');

        $start = null;
        $end = null;
        $ins = null;
        $lokasi = null;
        $layanan = null;
        $status = null;

        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
       
            $rows = $rows->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            $rows = $rows->where('staff_entry_datein', '>=', $start);
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            $rows = $rows->where('staff_entry_datein', '<=', $end);
        }
        if ($request->get('instalasi') != null) {
            $ins = $request->get('instalasi');
            $rows = $rows->where('service_orders.laboratory_id', '=', $ins);
        }
        if ($request->get('lokasi_pengujian') != null) {
            $lokasi = $request->get('lokasi_pengujian');
            $rows = $rows->where('sru.lokasi_pengujian', '=', $lokasi);
        }
        if ($request->get('jenis_layanan') != null) {
            $layanan = $request->get('jenis_layanan');
            $rows = $rows->where('sru.service_type_id', '=', $layanan);
        }
        if ($request->get('status_sla') != null) {
            $status = $request->get('status_sla');
            if ($status == 'ONP') {
                $rows = $rows->whereNull('service_orders.kabalai_date');
            } elseif ($status == 'ONT') {
                $rows = $rows->whereRaw('service_orders.kabalai_date - service_orders.staff_entry_datein <= mi.sla_day');
            } elseif ($status == 'Late') {
                $rows = $rows->whereNotNull('service_orders.kabalai_date') 
                    ->whereRaw('service_orders.kabalai_date - service_orders.staff_entry_datein > mi.sla_day');
            }
        }

        $rows = $rows->get();
        // dd($rows);
        $this->data = $rows;
        return view('resumeuut.index',compact(['rows','attribute', 'start', 'end',
            'ins','instalasi', 
            'lokasi', 'layanan', 'status',
            'types']));
    }

    public function fileImportExport()
    {
       return view('file-import');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileImport(Request $request) 
    {
        Excel::import(new UsersUUTImport, $request->file('file')->store('temp'));
        return back();
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileExport(Request $request) 
    {
        // $request = new \Illuminate\Http\Request();
        $this->s_date = $request->get('start_date'); 
        $this->e_date = $request->get('end_date');
        $this->ins = $request->get('instalasi');
        
        $rows = ServiceOrders::whereRaw('1=1')
        ->join('service_requests as sru','service_orders.service_request_id','=','sru.id')
        ->join('service_request_items as srui','service_orders.service_request_item_id','=','srui.id')
        ->join('service_request_item_inspections as sruii','srui.id','=','sruii.service_request_item_id')
        ->join('users_customers as uc','sru.requestor_id','=','uc.id')
        ->join('standard_uut as u','service_orders.uut_id','=','u.id')
        ->join('master_standard_types as mut','u.type_id','=','mut.id')
        ->join('uut_inspection_prices as uip','sruii.inspection_price_id','=','uip.id')
        ->join('master_laboratory as mi','service_orders.laboratory_id','=','mi.id')
        ->join('user_uut_owners as ow','ow.owner_id','=','u.owner_id')
        ->join('uut_owners as o','o.id','=','ow.owner_id')
        ->select('srui.id')
        ->addSelect(
            DB::raw(
                'srui.no_order,u.owner_id as idu,o.nama as n_daftar,uc.full_name, sru.label_sertifikat, (
                    select
                        count(s_srui.id)
                    from
                        service_request_items s_srui
                    where
                        s_srui.id = sru.id) as jumlah_alat,
                    mut.uut_type as rincian_alat,
                    uip.inspection_type as obyek_pengujian,
                    sruii.price * sruii.quantity as tarif_pnbp,
                    sru.total_price as total_pnbp,
                    sru.billing_code,
                    sru.payment_code,
                    srui.no_order,
                    srui.order_At,
                    service_orders.staff_entry_dateout,
                case 
                when
                    service_orders.kabalai_date is null 
                        then \'ONP\'
                when
                    service_orders.kabalai_date is not null and
                    service_orders.kabalai_date - service_orders.staff_entry_datein <= mi.sla_day
                        then \'ONT\'
                else \'LATE\'
                end as sla_status,  mi.nama_lab, CASE
                WHEN sru.service_type_id = 1 THEN \'Perifikasi\'
                ELSE \'Kalibrasi\' END AS Layanan'))
                ->orderBy('service_orders.id');

        $start = null;
        $end = null;
        $ins = null;
        $lokasi = null;
        $layanan = null;
        $status = null;

        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
       
            $rows = $rows->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            $rows = $rows->where('staff_entry_datein', '>=', $start);
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            $rows = $rows->where('staff_entry_datein', '<=', $end);
        }
        if ($request->get('instalasi') != null) {
            $ins = $request->get('instalasi');
            $rows = $rows->where('service_orders.laboratory_id', '=', $ins);
        }
        if ($request->get('lokasi_pengujian') != null) {
            $lokasi = $request->get('lokasi_pengujian');
            $rows = $rows->where('sru.lokasi_pengujian', '=', $lokasi);
        }
        if ($request->get('jenis_layanan') != null) {
            $layanan = $request->get('jenis_layanan');
            $rows = $rows->where('sru.service_type_id', '=', $layanan);
        }
        if ($request->get('status_sla') != null) {
            $status = $request->get('status_sla');
            if ($status == 'ONP') {
                $rows = $rows->whereNull('service_orders.kabalai_date');
            } elseif ($status == 'ONT') {
                $rows = $rows->whereRaw('service_orders.kabalai_date - service_orders.staff_entry_datein <= mi.sla_day');
            } elseif ($status == 'Late') {
                $rows = $rows->whereNotNull('service_orders.kabalai_date') 
                    ->whereRaw('service_orders.kabalai_date - service_orders.staff_entry_datein > mi.sla_day');
            }
        }

        
        $rows = $rows->get();        
        $this->data = $rows;
        
        return Excel::download(new UsersUUTExport($this->data),'Resume-UUT.xlsx');
    }    

}