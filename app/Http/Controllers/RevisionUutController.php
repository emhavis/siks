<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\MyClass\MyProjects;
use App\RevisionUttp;

class RevisionUutController extends Controller
{
    // protected $StandardInspectionPrice;
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("revisionuttp");

        $rows_daftar = RevisionUttp::whereIn('status', [1])
            ->orderBy('updated_at','desc')->get();

        $rows_proses = RevisionUttp::whereIn('status', [2,3])
            ->orderBy('updated_at','desc')->get();

        $rows_selesai = RevisionUttp::whereIn('status', [4])
            ->orderBy('updated_at','desc')->get();

        return view('revisionuttp.index', compact('rows_daftar', 'rows_proses', 'rows_selesai', 'attribute'));
    }

    public function edit($id)
    {
        $attribute = $this->MyProjects->setup("revisionuttp");

        $revision = RevisionUttp::find($id);

        return view('revisionuttp.edit', compact('attribute', 'revision'));
    }

    public function simpanedit($id)
    {
        $attribute = $this->MyProjects->setup("revisionuttp");
        
        $revision = RevisionUttp::find($id);
        $revision->update([
            "status" => 2,
        ]);

        return Redirect::route('revisionuttp');
    }

}
