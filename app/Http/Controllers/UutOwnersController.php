<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\MasterKabupatenKota;
use App\UutOwners;

class UutOwnersController extends Controller
{
    private $MyProjects;
    private $UutOwners;
    private $MasterKabupatenKota;
    public function __construct()
    {
        $this->UutOwners = new UutOwners();
        $this->MyProjects = new MyProjects();
        $this->MasterKabupatenKota = new MasterKabupatenKota();
    }
    public function index(){
        // \DB::enableQueryLog();
        $data = UutOwners :: join('master_kabupatenkota','master_kabupatenkota.id','=','kota_id')
        ->get(['uut_owners.*','master_kabupatenkota.nama as namakota']);
        $attribute = $this->MyProjects->setup("uutowners");
        return View('uutowners.index',compact('data','attribute'));
    }

    public function create($id = null){
        $attribute = $this->MyProjects->setup("insitems");

        $row = null;

        if($id)
        {
            $row = $this->MasterInstalasi->whereId($id)->first();
        }

        $kabupatenkota = $this->MasterKabupatenKota->pluck('nama', 'id');

        return View('uutowners.create',compact('row','id','attribute','kabupatenkota'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["nama"] = ['required'];
        $rules["kota_id"] = ['required'];
        $rules["npwp"] = ['required', 'integer', 'min:0'];
        $rules["nib"] = ['required', 'integer', 'min:0'];
        $rules["email"] = ['required'];
        $rules["penanggung_jawab"] = ['required'];
        $rules["telepon"] = ['required'];
        #set time when omserted data
        $request["created_at"] = date('Y-m-d H:i:s');
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->UutOwners->whereId($id)->update($request->all());
            }
            else
            {
                $this->UutOwners->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return redirect()->route('uutowners')->with($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->UttpInspectionItem->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
    public function edit($id)
    {
        $kabupatenkota = $this->MasterKabupatenKota->pluck('nama', 'id');
        $attribute = $this->MyProjects->setup("uutinspectionprice");
        $row  = $this->UutOwners->find($id);
        return view('uutowners.edit', compact(
            'row','attribute',
            'kabupatenkota'
        ));
    }   
    public function update(Request $request)
    {
        $response["status"] = false;

        $rules["nama"] = ['required'];
        $rules["kota_id"] = ['required'];
        $rules["npwp"] = ['required', 'integer', 'min:0'];
        $rules["nib"] = ['required', 'integer', 'min:0'];
        $rules["email"] = ['required'];
        $rules["penanggung_jawab"] = ['required'];
        $rules["telepon"] = ['required'];
        #set time when omserted data
        $request["updated_at"] = date('Y-m-d H:i:s');

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $id = $request->id;

            $request->request->remove('id');

            $this->UutOwners->whereId($id)->update([
                "nama" => $request['nama'],
                "kota_id" => $request['kota_id'],
                "npwp" => $request['npwp'],
                "nib" => $request['nib'],
                "email" => $request['email'],
                "penanggung_jawab" => $request['penanggung_jawab'],
                "updated_at" => $request['updated_at'],
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return redirect()->route('uutowners')->with($response);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->UutOwners::whereId($id)
        ->first();

        if($row)
        {
            $this->UutOwners::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('uutowners')->with($response);
    }

}