<?php

namespace App\Http\Controllers;

use App\MasterUsers;
use App\MasterRoles;
use App\MasterLaboratory;
use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\UttpInspectionItemTemplate;
use App\UttpInspectionItem;
use App\UttpInspectionPrice;
use App\UttpInspectionPriceRange;
use App\MasterInstalasi;
use App\MasterServiceType;
use Symfony\Component\VarDumper\Cloner\Data;

class UttpInspetionPriceController extends Controller
{
    private $MyProjects;
    private $UttpInspectionPrice;
    private $UttpInspectionTemplate;
    private $MasterInstalasi;
    private $MasterServiceType;
    private $UttpInspectionPriceRange;
    public function __construct()
    {
        $this->UttpInspectionPrice = new UttpInspectionPrice();
        $this->MyProjects = new MyProjects();
        $this->UttpInspectionTemplate = new UttpInspectionItemTemplate();
        $this->MasterInstalasi = new MasterInstalasi();
        $this->MasterServiceType = new MasterServiceType();
        $this->UttpInspectionPriceRange = new UttpInspectionPriceRange();
    }
    public function index(){
        $data = UttpInspectionPrice :: join('master_instalasi','master_instalasi.id','=','instalasi_id')
        ->get([
            'uttp_inspection_prices.*',
            'master_instalasi.nama_instalasi'
        ]);
        $attribute = $this->MyProjects->setup("uttpinspectionprice");
        return View('uttpinspectionprice.index',compact('data','attribute'));
    }

    public function create($id = null){
        $attribute = $this->MyProjects->setup("uttpinspectionprice");

        $row = null;

        if($id)
        {
            $row = $this->MasterInstalasi->whereId($id)->first();
        }

        $templates = $this->UttpInspectionTemplate->pluck('name', 'id');
        $instalasi = $this->MasterInstalasi->pluck('nama_instalasi', 'id');
        $services = $this->MasterServiceType->pluck('service_type', 'id');

        return View('uttpinspectionprice.create',compact('row','id','attribute','templates','instalasi','services'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["service_type_id"] = ['required'];
        $rules["inspection_template_id"] = ['required'];
        $rules["instalasi_id"] = ['required'];
        $rules["price"] = ['required'];
        $rules["inspection_type"] = ['required'];
        $rules["unit"] = ['required'];
        $request['created_at'] = date('Y-m-d H:i:s');
        if($request['has_range'] =='ON') {
            $request['has_range'] =='true';
        } else{$request['has_range'] ='false';}
        // unset($request['_token']);
        // dd($request->all());
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->UttpInspectionPrice->whereId($id)->update($request->all());
            }
            else
            {
                // $this->UttpInspectionPrice->insert($request->all());
                $data = $this->UttpInspectionPrice->create([
                    'service_type_id' => $request["service_type_id"],
                    'inspection_template_id' => $request["inspection_template_id"] ,
                    'instalasi_id' => $request["instalasi_id"],
                    'price' => $request["price"],
                    'inspection_type' => $request["inspection_type"],
                    'unit' => $request["unit"],
                    'created_at' => $request['created_at'],
                    'has_range' => $request['has_range'],
                ]);
                $priceid = $data->id;
                if($data){
                    $this->UttpInspectionPriceRange->insert([
                        'inspection_price_id' => $priceid,
                        'price' => $request["price"],
                        'min_range' => $request["min_price"],
                        'max_range' => $request["max_price"],
                        'price_unit' => $request["price_unit"],
                        'range_unit' => $request["range_unit"],
                        'inspection_type' => $request["inspection_type"],
                        'price_unit' => $request["unit"],
                        'created_at' => $request['created_at'],
                    ]);
                }      
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return redirect()->route('insprice')->with($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->UttpInspectionPrice->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
    public function edit($id)
    {
        $templates = $this->UttpInspectionTemplate->pluck('name', 'id');
        $instalasi = $this->MasterInstalasi->pluck('nama_instalasi', 'id');
        $services = $this->MasterServiceType->pluck('service_type', 'id');
        $attribute = $this->MyProjects->setup("uttpinspectionprice");
        // $row  = $this->UttpInspectionPrice->find($id);
        $row = UttpInspectionPrice :: leftJoin('uttp_inspection_price_ranges','uttp_inspection_price_ranges.inspection_price_id','=','uttp_inspection_prices.id')
        ->get(['uttp_inspection_prices.*',
            'uttp_inspection_price_ranges.max_range',
            'uttp_inspection_price_ranges.min_range',
            'uttp_inspection_price_ranges.price_unit'
        ])->find($id);
        // dd($row);
        return view('uttpinspectionprice.edit', compact(
            'row','attribute',
            'templates','instalasi','services'
        ));
    }    
    public function update(Request $request)
    {
        $response["status"] = false;

        $response["status"] = false;
        $rules["service_type_id"] = ['required'];
        $rules["inspection_template_id"] = ['required'];
        $rules["instalasi_id"] = ['required'];
        $rules["price"] = ['required'];
        $rules["price"] = ['required'];
        $rules["unit"] = ['required'];
        $request['updated_at'] = date('Y-m-d H:i:s');
        if($request['has_range'] =='1') {
            $request['has_range'] =='true';
        } else{$request['has_range'] ='false';}

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();
        // dd($request->all());

        if ($validation->passes())
        {
            $id = $request->id;

            $request->request->remove('id');

            $isupdate = $this->UttpInspectionPrice->whereId($id)->update([
                'inspection_type' => $request["inspection_type"],
                "price" => $request['price'],
                "inspection_template_id" => $request['inspection_template_id'],
                "service_type_id" => $request['service_type_id'],
                "instalasi_id" => $request['instalasi_id'],
                "unit" => $request['unit'],
                "has_range" => $request['has_range'],
                "updated_at" => $request['updated_at'],
            ]);
            if($isupdate){
                $this->UttpInspectionPriceRange->where('inspection_price_id',$id)->update([
                    'min_range' => $request["min_price"],
                    'max_range' => $request["max_price"],
                    'price_unit' => $request["price_unit"],
                    'range_unit' => $request["range_unit"],
                    'inspection_type' => $request["inspection_type"],
                    'price_unit' => $request["unit"],
                    'updated_at' => $request['updated_at'],
                ]);
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return redirect()->route('insprice')->with($response);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->UttpInspectionPrice::whereId($id)
        ->first();

        if($row)
        {
            $this->UttpInspectionPrice::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('insprice')->with($response);
    }

}