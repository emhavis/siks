<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadeRequest;
use App\Http\Repositories\UmlProfilRepository;
use App\UmlProfil;
use Validator;
use Redirect;
use Auth;
use Session;
use URL;
use Log;

class UmlProfilController extends Controller
{
	protected $umlProfilRepo;

    public function __construct(UmlProfilRepository $umlProfilRepo)
    {
    	$this->middleware('auth', ['except' => ['getUmlProfilInfo']]);
        $this->umlProfilRepo = $umlProfilRepo;
    }

    public function index(Request $request)
    {
    	try
        {
            $umlProfils = $this->umlProfilRepo->getUmlProfils();
            
        } catch(Exception $e)
        {
            Log::error('Error at opening UML index menu with message: ' . $e->errorInfo);
        }
        
        return view('umlprofil.index', compact('umlProfils'));
    }

    public function create(Request $request)
    {
        return view('umlprofil.create');
    }

    public function store(Request $request)
    {
        // try 
        // {
        //     $validation = Validator::make($request->all(), ServiceRequest::$rules);

        //     if ($validation->passes()) 
        //     {
        //         $this->serviceRequestRepo->createServiceRequest($request, Auth::user()->id);
        //     }
        //     else 
        //     {
        //         return Redirect::back()->withInput()->withErrors($validation->messages());
        //     }
        // } catch(Exception $e) 
        // {
        //     Log::error('Error at creating new service request with message: ' . $e->errorInfo);

        //     return Redirect::back()->withInput()->withErrors($e->errorInfo);
        // }

        return Redirect::route('umlprofil.index');
    }

    public function edit($id)
    {
        try 
        {
            $umlProfil = $this->umlProfilRepo->getUmlProfilById($id);

            if(Session::has('returnURL'))
            {
                Session::keep('returnURL');
            }

        } catch(Exception $e)
        {
            Log::error('Error at opening editing UML PROFIL form with id ' . $id . ' - with message: ' . $e->errorInfo);
        }

        return view('umlprofil.edit', compact('umlProfil'));
    }

    public function update(Request $request, $id)
    {
        try 
        {
            $validation = Validator::make($request->all(), UmlProfil::$rules);

            if ($validation->passes()) 
            {
                $this->umlProfilRepo->updateUmlProfil($id, $request->uml_name, $request->address, $request->phone_no);
            }
            else 
            {
                return Redirect::back()->withInput()->withErrors($validation->messages());
            }
        } catch(Exception $e) 
        {
            Log::error('Error at updating UML PROFIL (id ' . $id . ') with message: ' . $e->errorInfo);

            return Redirect::back()->withInput()->withErrors($e->errorInfo);
        }

        return ($url = Session::get('returnURL')) ? Redirect::to($url) : Redirect::route('umlprofil.index');
    }

    //API method
    public function getUmlProfilInfo(Request $request)
    {
        try 
        {
            if(FacadeRequest::ajax())
            {
                $umlProfil = $this->umlProfilRepo->getUmlProfilById($request->id);

                return json_encode($umlProfil);
            }

        } catch(Exception $e)
        {
            Log::error('Error while retrieving UML PROFIL data id ' . $request->id . ' - with message: ' . $e->errorInfo);
        }

        return null;
    }
}
