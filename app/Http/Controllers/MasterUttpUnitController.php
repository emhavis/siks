<?php

namespace App\Http\Controllers;

use App\MasterUttpType;
use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\MasterUttpUnit;

class MasterUttpUnitController extends Controller
{
    private $MyProjects;
    private $MasterUttpUnit;
    private $MasterUttpType;
    public function __construct()
    {
        $this->MasterUttpUnit = new MasterUttpUnit();
        $this->MyProjects = new MyProjects();
        $this->MasterUttpType =  new MasterUttpType();
    }
    public function index(){
        // \DB::enableQueryLog();
        $data = DB::table('master_uttp_units')->join('master_uttp_types','master_uttp_types.id','=','master_uttp_units.uttp_type_id')
            ->select('master_uttp_units.*','master_uttp_types.uttp_type')->get();
            // dd($data);
        $attribute = $this->MyProjects->setup("uttptype");
        return View('masteruttpunit.index',compact('data','attribute'));
    }

    public function create($id = null){
        $attribute = $this->MyProjects->setup("uttptype");
        $types = $this->MasterUttpType->pluck('uttp_type','id');
        $row = null;

        if($id)
        {
            $row = $this->MasterUttpUnit->whereId($id)->first();
        }

        return View('masteruttpunit.create',compact('row','id','attribute','types'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["unit"] = ['required'];
        $rules["uttp_type_id"] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->MasterServiceUnit->whereId($id)->update($request->all());
            }
            else
            {
                $this->MasterUttpUnit->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return redirect()->route('uttpunit')->with($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->MasterUttpUnit->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
    public function edit($id)
    {
        $attribute = $this->MyProjects->setup("uttptype");
        $types = $this->MasterUttpType->pluck('uttp_type','id');
        $row  = $this->MasterUttpUnit->find($id);
        return view('masteruttpunit.edit', compact(
            'row','attribute','types'
        ));
    }   
    public function update(Request $request)
    {
        $response["status"] = false;

        $rules["unit"] = ['required'];
        $rules["uttp_type_id"] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $id = $request->id;

            $request->request->remove('id');
            $this->MasterUttpUnit->whereId($id)->update(
                [
                "unit" => $request['unit'],
                "uttp_type_id" => $request['uttp_type_id'],
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return redirect()->route('uttpunit')->with($response);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->MasterUttpUnit::whereId($id)
        ->first();

        if($row)
        {
            $this->MasterUttpUnit::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('uttpunit')->with($response);
    }

}