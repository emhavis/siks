<?php namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaKonvensionalte;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrders;
use App\MasterMetodeUji;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ServiceUutMassaController extends Controller
{
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    protected $MasterNegara;

    public function __construct()
    {
        $this->MasterMetodeUji = new MasterMetodeUji();
        // $this->MasterStandardUkuran = new MasterStandardUkuran();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("service");
        $rows = ServiceOrders::whereIn("stat_service_order",[0,1])
        ->where("is_finish",0)
        ->where("laboratory_id",Auth::user()->laboratory_id)
        ->orderBy('staff_entry_datein','desc')
        ->get();

        return view('service.index',compact('rows','attribute'));
    }

    public function recountte(Request $request)
    {
        $id = $request->id;
        $row = ServiceOrders::find($id);
        $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($request->get("hotdata"),true);

        // // User Take Order
        $hot[3][15] = $row->MasterUsers->full_name;
        $hot[1][9] = date("d - F - Y");
        $hot[1][1] = $rowServiceRequestItem->Standard->brand;
        $hot[2][1] = $rowServiceRequestItem->Standard->tipe;
        // $hot[3][1] = $rowServiceRequestItem->Standard->no_seri;
        // $hot[5][1] = $rowServiceRequestItem->Standard->class;
        $hot[1][4] = $rowServiceRequestItem->Standard->MasterNegara->nama_negara;

        $orderPaymentDate =  date("d - F - Y",strtotime($rowServiceRequest->payment_date));  // 25 Januari 2021	Tanggal bayar order
        $hot[1][15] = $rowServiceRequest->no_order."/".$orderPaymentDate;

        $C15 = $hot[16][2];
        $H15 = $hot[16][7];
        $C16 = $hot[17][2];
        $F11 = $var_d = $hot[4][4];
        $H16 = $C16+((($var_d/2)-$H15)-$C15);
        $V12 = $var_d*1000;
        $V13 = $V12/10;
        // dd($C15,$H15,$C16,$F11,$H16,$V12,$V13);

        if($C15>0 && $H15>0 && $C16>0 && $F11>0 && $V12>0 && $V13>0)
        {
            $hot[17][7] = $H16;

            $C11 = $var_e = $hot[4][1];
            $C10 = $kapasitas = $hot[6][1];
            $var_n = $C10/$C11;

            $hot[5][1] = $kelas = $this->Class_TE($var_n);
            $rows = MassaKonvensionalte::get();
            $data = [];
            foreach($rows as $row)
            {
                $data[$row->nominal] = $row->toArray();
            }
            ksort($data);

            // KOMPONEN LOAD
            $total = [];
            for($x=79;$x<=87;$x++)
            {
                $nominal = $hot[$x][10];
                if(strlen($nominal)>0)
                {
                    $total[] = $aa = $this->MassaKonvensionalte($data,$nominal);
                    $hot[$x][12] = number_format($aa,5,".",",");
                }
            }
            $M91 = array_sum($total);
            $hot[88][12] = number_format($M91,5,".",",");
            // dd($M91);
            // REPEATABILITY TEST
            $total2 = [];
            if($M91>0)
            {
                for($x=79;$x<=88;$x++)
                {
                    $B83 = $hot[$x][1];
                    $E83 = $hot[$x][3];
                    if($V13<10)
                    {
                        $total2[] = $aa = $B83-$M91;
                        $hot[$x][5] = number_format($aa,3,".",",");
                    }
                    else
                    {
                        $total2[] = $aa = $B83+(0.5*$var_d)-$E83-$M91;
                        $hot[$x][5] = number_format($aa,3,".",",");
                    }
                }
                $hot[89][5] = number_format((max($total2)-min($total2)),3,".",",");
            }
            // dd($total2);
            // ECCENTRICITY TESTS
            $D58 = $hot[59][2];
            $F58 = strlen($hot[59][4])>0 ? $this->MassaKonvensionalte($data,$hot[59][4]) : 0;
            $G58 = strlen($hot[59][6])>0 ? $this->MassaKonvensionalte($data,$hot[59][6]) : 0;
            $H58 = strlen($hot[59][8])>0 ? $this->MassaKonvensionalte($data,$hot[59][8]) : 0;
            $O58 = $F58+$G58+$H58;
            $hot[59][12] = number_format($O58,4,".",",");

            $total3 = [];
            if($D58>0 && $O58>0)
            {
                for($x=64;$x<=72;$x++)
                {
                    if(in_array($x,[64,66,68,70,72]))
                    {
                        $hot[$x][2] = $D58;
                        $F63 = $hot[$x][4];
                        $I63 = $hot[$x][6];
                        if($V13<10)
                        {
                            $hot[$x][8] = number_format(($F63-$O58),3,".",",");
                            $hot[$x][10] = number_format(($F63-$O58-$H16),3,".",",");
                            $total3[] = abs($F63-$O58-$H16);
                        }
                        else
                        {
                            $hot[$x][8] = number_format(($F63+(0.5*$var_d)-$I63-$O58),3,".",",");
                            $hot[$x][10] = number_format(($F63+(0.5*$var_d)-$I63-$O58-$H16),3,".",",");
                            $total3[] = abs($F63+(0.5*$var_d)-$I63-$O58-$H16);
                        }
                        $hot[$x][12] = $this->MPE_TE($hot[$x][2],$kelas,$var_e);
                        // DIHITUNG SETELAH WEIGHING TEST
                        // $hot[$x][14] = K=2
                    }
                }
            }
            // dd($total3);

            // WEIGHING TEST
            $total4 = [];
            for($x=24;$x<=54;$x++)
            {
                if(($x>=24 && $x<=33) || ($x>=44 && $x<=48) || ($x>=50 && $x<=54))
                {
                    if(isset($hot[$x][0]) && isset($hot[$x][1]))
                    {
                        $mconv = [];
                        $ustd = [];
                        $ustd_pow = [];
                        for($y=1;$y<=7;$y++)
                        {
                            $nominal2 = $hot[$x][$y];
                            if(floatval($nominal2)>0)
                            {
                                $mconv[] = $aa = $this->MassaKonvensionalte($data,$nominal2);
                                $ustd[] = $bb = $this->MassaKonvensionalte($data,$nominal2,true);
                                $ustd_pow[] = $cc = pow($this->MassaKonvensionalte($data,$nominal2,true),2);
                                // dd($nominal2,$aa,$bb,$cc);
                            }
                        }
                        // dd($mconv,$ustd,$ustd_pow,$hot[$x][1],$hot[$x][2],$hot[$x][3]);
                        $I23 = array_sum($mconv);
                        $hot[$x][8] = number_format($I23,5,".",",");

                        $A23 = $hot[$x][0];
                        $J23 = $hot[$x][10];
                        $K23 = $hot[$x][11];
                        $L23 = $hot[$x][12];
                        $M23 = $hot[$x][13];
                        if($J23>0 && $K23>0 && $L23>0 && $M23>0 && $A23>0 && count($total3)>0 && count($total2)>0)
                        {
                            if($V13<10)
                            {
                                $hot[$x][14] = number_format(($J23-$I23-$H16),3,".",",");
                                $hot[$x][15] = number_format(($K23-$I23-$H16),3,".",",");
                                // dd($hot[$x][14],$J23,$I23,$H16);
                            }
                            else
                            {
                                $hot[$x][14] = number_format(($J23+(0.5*$var_d)-$L23-$I23-$H16),3,".",",");
                                $hot[$x][15] = number_format(($K23+(0.5*$var_d)-$M23-$I23-$H16),3,".",",");
                            }

                            $hot[$x][16] = number_format($this->MPE_TE($A23,$kelas,$var_e),2,".",",");
                            // dd($total2,$this->Standard_Deviation($total2));
                            $R23 = $this->Standard_Deviation($total2)/sqrt(10);
                            $hot[$x][18] = number_format($R23,3,".",",");
                            $S23 = array_sum($ustd)/2;
                            $hot[$x][19] = number_format($S23,4,".",",");

                            if($V12<10)
                            {
                                $T23 = $F11/(2*sqrt(3));
                            }
                            else
                            {
                                $T23 = $F11/(10*sqrt(3));
                            }
                            $hot[$x][20] = number_format($T23,8,".",",");

                            // if($x===33) dd($ustd_pow,array_sum($ustd_pow),sqrt(array_sum($ustd_pow)));
                            $U23 = sqrt(array_sum($ustd_pow))/(2*sqrt(3));
                            $hot[$x][21] = number_format($U23,4,".",",");
                            // dd($total3);
                            $V23 = max($total3)/(2*sqrt(3));
                            $hot[$x][22] = number_format($V23,4,".",",");
                            $W23 = $A23/(1000000*sqrt(3));
                            $hot[$x][23] = number_format($W23,4,".",",");
                            // dd();
                            $X23 = sqrt(pow($R23,2)+pow($S23,2)+pow($T23,2)+pow($U23,2)+pow($V23,2)+pow($W23,2));
                            $hot[$x][24] = number_format($X23,2,".",",");

                            $hot[$x][25] = 2;
                            $Q23 = $Z23 = $X23*2;
                            if($x <= 33)  $total4[$A23] = $Z23;

                            $hot[$x][17] = $hot[$x][26] = number_format($Q23,5,".",",");
                        }
                    }
                }
            }

            // LANJUTAN ECCENTRICITY TESTS
            if(count($total4)>0)
            {
                for($x=64;$x<=72;$x++)
                {
                    if(in_array($x,[64,66,68,70,72]))
                    {
                        // dd($x,$total4);
                        $hot[$x][14] = number_format($total4[$hot[$x][2]],3,".",",");
                    }
                }
            }

            $I99 = $hot[95][6];
            $I104 = $hot[100][6];
            $K99 = $hot[95][8];
            $K104 = $hot[100][8];
            if($I99>0 && $I104>0 && $K99>0 && $K104>0)
            {
                if($V13<10)
                {
                    $hot[95][10] = $I99-$I99-$H16;
                    $hot[100][10] = $I104-$I104-$H16;
                }
                else
                {
                    $hot[95][10] = $I99+(0.5*$F11)-$K99-$I99-$H16;
                    $hot[100][10] = $I104+(0.5*$F11)-$K104-$I104-$H16;

                }
            }

            // TARE WEIGHING
            // $total44 = [];
            // for($x=44;$x<=54;$x++)
            // {
            //     if($x!==49 && isset($hot[$x][0]) && isset($hot[$x][1]))
            //     {
            //         $mconv = [];
            //         $ustd = [];
            //         $ustd_pow = [];
            //         for($y=1;$y<=7;$y++)
            //         {
            //             $nominal2 = $hot[$x][$y];
            //             if(floatval($nominal2)>0)
            //             {
            //                 $mconv[] = $this->MassaKonvensionalte($data,$nominal2);
            //                 $ustd[] = $this->MassaKonvensionalte($data,$nominal2,true);
            //                 $ustd_pow[] = pow($this->MassaKonvensionalte($data,$nominal2,true),2);
            //             }
            //         }
            //         // dd($mconv);
            //         $I23 = array_sum($mconv);
            //         $hot[$x][8] = number_format($I23,5,".",",");

            //         $A23 = $hot[$x][0];
            //         $J23 = $hot[$x][10];
            //         $K23 = $hot[$x][11];
            //         $L23 = $hot[$x][12];
            //         $M23 = $hot[$x][13];
            //         if($J23>0 && $K23>0 && $L23>0 && $M23>0 && $A23>0 && count($total3)>0 && count($total2)>0)
            //         {
            //             if($V13<10)
            //             {
            //                 $hot[$x][14] = number_format(($J23-$I23-$H16),3,".",",");
            //                 $hot[$x][15] = number_format(($K23-$I23-$H16),3,".",",");
            //                 // dd($hot[$x][14],$J23,$I23,$H16);
            //             }
            //             else
            //             {
            //                 $hot[$x][14] = number_format(($J23+(0.5*$var_d)-$L23-$I23-$H16),3,".",",");
            //                 $hot[$x][15] = number_format(($K23+(0.5*$var_d)-$M23-$I23-$H16),3,".",",");
            //             }

            //             $hot[$x][16] = number_format($this->MPE_TE($A23,$kelas,$var_e),2,".",",");
            //             // dd($total2,$this->Standard_Deviation($total2));
            //             $R23 = $this->Standard_Deviation($total2)/sqrt(10);
            //             $hot[$x][18] = number_format($R23,3,".",",");
            //             $S23 = array_sum($ustd)/2;
            //             $hot[$x][19] = number_format($S23,4,".",",");

            //             if($V12<10)
            //             {
            //                 $T23 = $F11/(2*sqrt(3));
            //             }
            //             else
            //             {
            //                 $T23 = $F11/(10*sqrt(3));
            //             }
            //             $hot[$x][20] = number_format($T23,8,".",",");

            //             // if($x===33) dd($ustd_pow,array_sum($ustd_pow),sqrt(array_sum($ustd_pow)));
            //             $U23 = sqrt(array_sum($ustd_pow))/(2*sqrt(3));
            //             $hot[$x][21] = number_format($U23,4,".",",");
            //             // dd($total3);
            //             $V23 = max($total3)/(2*sqrt(3));
            //             $hot[$x][22] = number_format($V23,4,".",",");
            //             $W23 = $A23/(1000000*sqrt(3));
            //             $hot[$x][23] = number_format($W23,4,".",",");
            //             // dd();
            //             $X23 = sqrt(pow($R23,2)+pow($S23,2)+pow($T23,2)+pow($U23,2)+pow($V23,2)+pow($W23,2));
            //             $hot[$x][24] = number_format($X23,2,".",",");

            //             $hot[$x][25] = 2;
            //             $total44[$A23] = $Q23 = $Z23 = $X23*2;
            //             $hot[$x][17] = $hot[$x][26] = number_format($Q23,5,".",",");
            //         }
            //     }
            // }

        }


        if(
            isset($hot[3][9]) && strlen($hot[3][9])>0 &&
            isset($hot[4][9]) && strlen($hot[4][9])>0 &&
            isset($hot[3][11]) && strlen($hot[3][11])>0 &&
            isset($hot[4][11]) && strlen($hot[4][11])>0 &&
            isset($hot[3][13]) && strlen($hot[3][13])>0 &&
            isset($hot[4][13]) && strlen($hot[4][13])>0
        )
        {
            $suhuStart = $hot[3][9];
            $suhuEnd = $hot[4][9];
            $termohygroStart = $hot[3][11];
            $termohygroEnd = $hot[4][11];
            $barometerStart = $hot[3][13];
            $barometerEnd = $hot[4][13];

            $THRPrata2 = $this->InterpolasiTermohygro($suhuStart,$suhuEnd,$termohygroStart,$termohygroEnd,$barometerStart,$barometerEnd);
            $hot[5][9] = $THRPrata2[0];
            $hot[5][11] = $THRPrata2[1];
            $hot[5][13] = $THRPrata2[2];
            $hot[5][41] = max($THRPrata2[6]);
            $hot[5][42] = max($THRPrata2[7]);
        }

        return response($hot);
    }

    public function recountbidur(Request $request)
    {
        $id = $request->id;
        $row = ServiceOrders::find($id);
        $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($request->get("hotdata"),true);

        // User Take Order
        $hot[11][15] = $row->MasterUsers->full_name;
        $hot[1][9] = date("d - F - Y");
        $hot[1][1] = $rowServiceRequestItem->Standard->brand;
        $hot[2][1] = $rowServiceRequestItem->Standard->tipe;
        // $hot[3][1] = $rowServiceRequestItem->Standard->no_seri;
        $hot[5][1] = $rowServiceRequestItem->Standard->class;
        $hot[1][4] = $rowServiceRequestItem->Standard->MasterNegara->nama_negara;

        $orderPaymentDate =  date("d - F - Y",strtotime($rowServiceRequest->payment_date));  // 25 Januari 2021	Tanggal bayar order
        $hot[1][15] = $rowServiceRequest->no_order."/".$orderPaymentDate;

        if(isset($hot[4][16]) && strlen($hot[4][16])>0)
        {
            $massaJenis = $this->MassaJenisFunc($hot[4][16]); //[1] PLATINA, 21400
            $refdensity = $massaJenis[0];
            $refdensity2 = $massaJenis[1];
            $hot[4][18] = $refdensity2;
        }

        if(isset($hot[5][16]) && strlen($hot[5][16])>0)
        {
            $massaJenis2 = $this->MassaJenisFunc($hot[5][16]); //[1] PLATINA, 21400
            $uutdensity = $massaJenis2[0];
            $uutdensity2 = $massaJenis2[1];
            $hot[5][18] = $uutdensity2;
        }

        if(
            isset($hot[3][9]) && strlen($hot[3][9])>0 &&
            isset($hot[4][9]) && strlen($hot[4][9])>0 &&
            isset($hot[3][11]) && strlen($hot[3][11])>0 &&
            isset($hot[4][11]) && strlen($hot[4][11])>0 &&
            isset($hot[3][13]) && strlen($hot[3][13])>0 &&
            isset($hot[4][13]) && strlen($hot[4][13])>0
        )
        {
            $suhuStart = $hot[3][9];
            $suhuEnd = $hot[4][9];
            $termohygroStart = $hot[3][11];
            $termohygroEnd = $hot[4][11];
            $barometerStart = $hot[3][13];
            $barometerEnd = $hot[4][13];

            $THRPrata2 = $this->InterpolasiTermohygro($suhuStart,$suhuEnd,$termohygroStart,$termohygroEnd,$barometerStart,$barometerEnd);
            $hot[5][9] = $THRPrata2[0];
            $hot[5][11] = $THRPrata2[1];
            $hot[5][13] = $THRPrata2[2];
            $hot[5][41] = max($THRPrata2[6]);
            $hot[5][42] = max($THRPrata2[7]);

            // $airDensity = $this->AirDensity($THRPrata2);
            // $airDensity2 = $this->AirDensity2();

            // $hot[3][16] = $airDensity;
            // $hot[3][18] = $airDensity2;
        }

        if(isset($hot[5][1]) && strlen($hot[5][1])>0)
        {
            $class = $hot[5][1];
        }

        $G12 = $hot[4][1];
        $O12 = $hot[4][4];
        $AP9 = $hot[6][17];
        $AP10 = $hot[7][17];
        $AP11 = $hot[8][17];
        $AP12 = $hot[9][17];
        $Z17 = $hot[8][13];

        // if(isset($class) && isset($THRPrata2) && isset($massaJenis) && isset($massaJenis2))
        // {
            for($incr=17;$incr<=41;$incr++)
            {
                if(in_array($incr,[17,20,23,26,29,32,35,38,41]))
                {
                    $A1 = floatval($hot[$incr][1]);
                    $B[1] = floatval($hot[$incr][3]);
                    $B[2] = floatval($hot[$incr][5]);
                    $B[3] = floatval($hot[$incr][7]);
                    $B[4] = floatval($hot[$incr][9]);
                    $B[5] = floatval($hot[$incr][11]);
                    $A2 = floatval($hot[$incr][13]);
                    if($A1>0)
                    {
                        for($i=0;$i<5;$i++)
                        {
                            $ii = $i+16;
                            $total = $AP9+($B[$i+1]-(($A1+$B[1]+$B[2]+$B[3]+$B[4]+$B[5]+$A2)/7));
                            // dd($A1,$B,$total);
                            $hot[$incr][$ii] = number_format($total,2,".",",");
                        }
                        // $total = $AP+($B2-(($A1+$B1+$B2+$B3+$B4+$B5+$A2)/7));
                        // $hot[$incr][18] = number_format($total,2,".",",");
                        // $total = $AP+($B3-(($A1+$B1+$B2+$B3+$B4+$B5+$A2)/7));
                        // $hot[$incr][20] = number_format($total,2,".",",");
                        // $total = $AP+($B4-(($A1+$B1+$B2+$B3+$B4+$B5+$A2)/7));
                        // $hot[$incr][22] = number_format($total,2,".",",");
                        // $total = $AP+($B5-(($A1+$B1+$B2+$B3+$B4+$B5+$A2)/7));
                        // $hot[$incr][24] = number_format($total,2,".",",");
                    }
                    else
                    {
                        for($i=0;$i<5;$i++)
                        {
                            $ii = $i+16;
                            $hot[$incr][$ii] = "";
                        }
                        // $hot[$incr][18] = "";
                        // $hot[$incr][20] = "";
                        // $hot[$incr][22] = "";
                        // $hot[$incr][24] = "";
                        // dd($A1,$B1,$B2,$B3,$B4,$B5,$A2);
                    }
                }

                if(in_array($incr,[18,21,24,27,30,33,36,37,42]))
                {
                    // $hot[$incr][16] = "";
                    // $hot[$incr][18] = "";
                    // $hot[$incr][20] = "";
                    // $hot[$incr][22] = "";
                    // $hot[$incr][24] = "";
                    for($i=0;$i<5;$i++)
                    {
                        $ii = $i+16;
                        $hot[$incr][$ii] = "";
                        $total = floatval(str_replace(",","",$hot[$incr-1][$ii]));
                        // $hot[$incr][$ii] = "BATAL";
                        // dd($total);
                        if($total>0)
                        {
                            if(($total>=($G12-$O12) && $total<=($G12+$O12)))
                            {
                                $hot[$incr][$ii] = "SAH";
                            }
                            else
                            {
                                $hot[$incr][$ii] = "BATAL";
                            }
                        }

                    }
                }
            }

            $B50 = $hot[46][0] = number_format($AP12/sqrt(10),4,".",",");
            $I50 = $hot[46][3] = number_format($AP10/2,3,".",",");
            $P50 = $hot[46][6] = number_format((0.5*$AP11*sqrt(2))/sqrt(3),4,".",",");
            $W50 = $hot[46][9] = number_format(0.12*0.000016*$AP9/sqrt(3),3,".",",");
            $AD50 = $hot[46][12] = number_format((0.08*$Z17)/SQRT(3),3,".",",");
            // $AK50 = $hot[46][15] = 2;
            $AK50 = 2;
            $hot[46][18] = number_format((SQRT($B50^2+$I50^2+$P50^2+$W50^2+$AD50^2))*$AK50,3,".",",");
        // }

        return response($hot);
    }

    public function recount(Request $request)
    {
        $id = $request->id;
        $row = ServiceOrders::find($id);
        $rowServiceRequest = ServiceRequest::find($row->service_request_id);
        $rowServiceRequestItem = ServiceRequestItem::find($row->service_request_item_id);

        $hot = json_decode($request->get("hotdata"),true);

        // User Take Order
        $hot[7][15] = $row->MasterUsers->full_name;
        $hot[1][9] = date("d - F - Y");
        $hot[1][1] = $rowServiceRequestItem->Standard->brand;
        $hot[2][1] = $rowServiceRequestItem->Standard->tipe;
        $hot[3][1] = $rowServiceRequestItem->Standard->no_seri;
        $hot[5][1] = $rowServiceRequestItem->Standard->class;
        $hot[1][4] = $rowServiceRequestItem->Standard->MasterNegara->nama_negara;

        // $year = date("y"); // 21	tahun
        // if(!isset($hot[1][15]))
        // {

        //     $orderNumber = $this->GetOrderNumber(); // 0228	urutan order
        // }
        // else
        // {
        //     $orderNumber = $hot[1][15];
        // }
        // $payment_date = $rowServiceRequest->payment_date;

        // $countSertifikat = sprintf("%'.02d\n",ServiceRequestItem::where("service_request_id",$row->service_request_id)->get()->count()); // 05	jumlah sertifikat yg diterbitkan
        // $countToolInOrder = sprintf("%'.03d\n",ServiceRequestItemInspection::where("service_request_item_id",$row->service_request_item_id)->get()->count()); // 016	jumlah alat yg masuk dalam order tersebut
        $orderPaymentDate =  date("d - F - Y",strtotime($rowServiceRequest->payment_date));  // 25 Januari 2021	Tanggal bayar order
        $hot[1][15] = $rowServiceRequest->no_order."/".$orderPaymentDate;

        //Note: Order belum diproses jika belum ada pembayaran
        // Order Number
        // $hot[1][15] = $year."-".$orderNumber."-".$countSertifikat."-".$countToolInOrder."/".$orderPaymentDate;

        if(isset($hot[4][16]) && strlen($hot[4][16])>0)
        {
            $massaJenis = $this->MassaJenisFunc($hot[4][16]); //[1] PLATINA, 21400
            $refdensity = $massaJenis[0];
            $refdensity2 = $massaJenis[1];
            $hot[4][18] = $refdensity2;
        }

        if(isset($hot[5][16]) && strlen($hot[5][16])>0)
        {
            $massaJenis2 = $this->MassaJenisFunc($hot[5][16]); //[1] PLATINA, 21400
            $uutdensity = $massaJenis2[0];
            $uutdensity2 = $massaJenis2[1];
            $hot[5][18] = $uutdensity2;
        }

        if(
            isset($hot[3][9]) && strlen($hot[3][9])>0 &&
            isset($hot[4][9]) && strlen($hot[4][9])>0 &&
            isset($hot[3][11]) && strlen($hot[3][11])>0 &&
            isset($hot[4][11]) && strlen($hot[4][11])>0 &&
            isset($hot[3][13]) && strlen($hot[3][13])>0 &&
            isset($hot[4][13]) && strlen($hot[4][13])>0
        )
        {
            $suhuStart = $hot[3][9];
            $suhuEnd = $hot[4][9];
            $termohygroStart = $hot[3][11];
            $termohygroEnd = $hot[4][11];
            $barometerStart = $hot[3][13];
            $barometerEnd = $hot[4][13];

            $THRPrata2 = $this->InterpolasiTermohygro($suhuStart,$suhuEnd,$termohygroStart,$termohygroEnd,$barometerStart,$barometerEnd);
            $hot[5][9] = $THRPrata2[0];
            $hot[5][11] = $THRPrata2[1];
            $hot[5][13] = $THRPrata2[2];
            $hot[5][41] = max($THRPrata2[6]);
            $hot[5][42] = max($THRPrata2[7]);

            $airDensity = $this->AirDensity($THRPrata2);
            $airDensity2 = $this->AirDensity2();

            $hot[3][16] = $airDensity;
            $hot[3][18] = $airDensity2;
        }

        if(isset($hot[5][1]) && strlen($hot[5][1])>0)
        {
            $class = $hot[5][1];
        }

        $unit = "g";
        $nominals = [];
        $referenceStandardSerialNumbers = [];
        $referenceStandards = [];
        $unitMin = null;
        $unitMax = null;
        $STDSN = null;
        $MASSCOMPSN = null;
        // 15-33 data gram
        // 36-47 data miligram
        if(isset($class) && isset($THRPrata2) && isset($massaJenis) && isset($massaJenis2))
        {
            for($incr=15;$incr<=47;$incr++)
            {
                if($STDSN!==$hot[$incr][1] && strlen($hot[$incr][1])>0)
                {
                    $STDSN = $hot[$incr][1];
                }

                if($MASSCOMPSN!==$hot[$incr][2] && strlen($hot[$incr][2])>0)
                {
                    $MASSCOMPSN = $hot[$incr][2];
                }

                if(
                    strlen($STDSN)>0 &&
                    strlen($MASSCOMPSN)>0 &&
                    strlen($hot[$incr][3])>0 &&
                    !in_array($incr,[34,35])
                )
                {
                    $hot[$incr][1] = $STDSN;
                    $hot[$incr][2] = $MASSCOMPSN;

                    if($unitMax===null && $incr<36)
                    {
                        $unitMax = "g";
                    }
                    $unitMin = $incr<36?"g":"mg";
                    $unit = $incr<36?"g":"mg";

                    $nominals[] = floatval($hot[$incr][0]);
                    $nominal = floatval($hot[$incr][0]);

                    // Diff
                    $a = floatval($hot[$incr][3]);
                    $b = floatval($hot[$incr][4]);
                    $c = floatval($hot[$incr][5]);
                    $d = floatval($hot[$incr][6]);
                    $aa = ($b-$a+$c-$d)/2;

                    $e = floatval($hot[$incr][7]);
                    $f = floatval($hot[$incr][8]);
                    $g = floatval($hot[$incr][9]);
                    $h = floatval($hot[$incr][10]);
                    $ee = ($f-$e+$g-$h)/2;

                    $i = floatval($hot[$incr][11]);
                    $j = floatval($hot[$incr][12]);
                    $k = floatval($hot[$incr][13]);
                    $l = floatval($hot[$incr][14]);
                    $ii = ($j-$i+$k-$l)/2;

                    // Diff
                    $Diff = $BA = $aa+$ee+$ii;
                    $hot[$incr][15] = number_format(($Diff)/3,6,".",",");

                    // Density
                    $density = $BE = $refdensity; //floatval($hot[4][16]);
                    $hot[$incr][16] = $density;
                    // U Density
                    $Udensity = $BI = $refdensity2;//floatval($hot[4][18]);
                    $hot[$incr][17] = $Udensity;
                    //Masscomp DB
                    $masscomp = $this->GetMasscomp($MASSCOMPSN); //[1] XP 56, 01123, 1000mg
                    $hot[$incr][18] = $BM = $masscomp[1];
                    //Masscomp STD
                    $hot[$incr][19] = $BP = (2*$masscomp[1]/sqrt(10));

                    // Reference Mrc
                    $referenceStandard = $this->ReferenceStandardSerialNumber($STDSN);  //[25] F2, 110023
                    // dd($referenceStandard);
                    $referenceStandards[] = $referenceStandard->kelas;
                    $referenceStandardSerialNumbers[] = $referenceStandard->serialnumber;

                    $konvensional = $this->getKonvensional($referenceStandard->serialnumber,$nominal,$unit);
                    // dd($konvensional);
                    $Mcr = $BS = $konvensional?$konvensional->mc:0;
                    $hot[$incr][20] = $Mcr;
                    // Reference Ucert
                    $Ucert = $BW = $konvensional?$konvensional->usert:0;
                    $hot[$incr][21] = $Ucert;

                    // Bouyancy C
                    $bou_c = $CF = ($airDensity-1.2)*((1/$uutdensity)-(1/$refdensity));
                    $hot[$incr][23] = $bou_c;
                    // Bouyancy Status
                    $bou_status = $CH = ($bou_c<=($Ucert/(3*$nominal)))?"NEGLIGIBLE":"CALCULATE";
                    $hot[$incr][24] = $bou_status;

                    // Mct
                    if($bou_status==="CALCULATE")
                    {
                        $Mct = $CA = $BS+($BA/1000)+($CF*$BS);
                    }
                    else
                    {
                        $Mct = $CA = $BS + ($BA/1000);
                    }
                    $hot[$incr][22] = $Mct;

                    // Uncertainty Uw
                    if(in_array($class,["E1","E2","F1","F2"]))
                    {
                        $Uw = $CK = $this->Standard_Deviation([($b-$a)/1000,($c-$d)/1000,($f-$e)/1000,($g-$h)/1000,($j-$i)/1000,($k-$l)/1000]);
                    }
                    else
                    {
                        $Uw = $CK = (max(($b-$a),($c-$d))-min(($b-$a),($c-$d)))/1000/(2*sqrt(3));
                    }
                    $hot[$incr][25] = $Uw;

                    // Uncertainty Usens
                    $hot[$incr][26] = $usens = $CN = $masscomp[0];

                    // Uncertainty Ures
                    $ures = $CO = 0.5*$masscomp[1]*sqrt(2)/sqrt(3);
                    $hot[$incr][27] = $ures;

                    // Uncertainty Umr
                    $Umr = $CR = $Ucert/2;
                    $hot[$incr][28] = $Umr;

                    // Uncertainty Udrift
                    $drift = $this->GetDrift($referenceStandard->serialnumber,$nominal,$unit);
                    // dd($drift);
                    // $row->drift
                    $hot[$incr][29] = $CU = $drift?$drift->drift:0;

                    // Uncertainty Ubu
                    $Ubu_12 = $Mcr*($refdensity-$density)/($refdensity*$density);
                    $Ubu_13 = $airDensity2/sqrt(3);
                    $Ubu_1 = $Ubu_12 * $Ubu_13;

                    $Ubu_21 = $Mcr*($airDensity-1.2)/(pow($refdensity,2));
                    $Ubu_22 =$refdensity2/sqrt(3);
                    $Ubu_2 = $Ubu_21 * $Ubu_22;

                    $Ubu_31 = $Mcr*($airDensity-1.2)/(pow($density,2));
                    $Ubu_32 = $Udensity/sqrt(3);
                    $Ubu_3 = $Ubu_31 * $Ubu_32;

                    $Ubu = $CX = sqrt(pow($Ubu_1,2)+pow($Ubu_2,2)+pow($Ubu_3,2));

                    $hot[$incr][30] = $Ubu;

                    // Uncertainty Uc
                    $Uc = $DA = sqrt(pow($Uw,2)+pow($ures,2)+pow($Umr,2)+pow($drift->drift,2)+pow($Ubu,2));
                    $hot[$incr][31] = $Uc;
                    // Uncertainty Ueff
                    $Uw2 = floatval($hot[$incr][32]);
                    $usens2 = floatval($hot[$incr][33]);
                    $ures2 = floatval($hot[$incr][34]);
                    $Umr2 = floatval($hot[$incr][35]);
                    $drift2 = floatval($hot[$incr][36]);
                    $Ubu_20 = floatval($hot[$incr][37]);

                    if(
                        $Uw2==0 ||
                        $usens2==0 ||
                        $ures2==0 ||
                        $Umr2==0 ||
                        $drift2==0 ||
                        $Ubu_20==0
                    )
                    {
                        $Ueff = $DT = 0;
                    }
                    else
                    {
                        $Ueff = $DT = (pow($Uc,4))
                        /
                        (
                            ((pow($Uw,4))/$Uw2)+
                            ((pow($ures,4))/$ures2)+
                            ((pow($Umr,4))/$Umr2)+
                            ((pow($drift->drift,4))/$drift2)+
                            ((pow($Ubu,4))/$Ubu_20)+
                            ((pow($usens,4))/$usens2)
                        );
                    }

                    $hot[$incr][38] = $Ueff;
                    // Uncertainty k
                    $df = intval($Ueff)>200?200:intval($Ueff);
                    $row = MasterStudentTable::where("pr","0.05")
                    ->where("k",2)
                    ->where("df",$df)->first();
                    // dd($row);
                    $Kk = $row->value; //TINV(0.05;$Ueff));
                    $hot[$incr][39] = $Kk;
                    // Uncertainty U(95%)
                    $Uu = $Kk-$Uc;
                    $hot[$incr][40] = $Uu;

                    $hot[$incr][41] = $this->CheckMPE_CMC($Uu,$nominal,$unit);

                    $delta = $Mct - $nominal;
                    $hot[$incr][42] = $this->CheckMPE_Kelas($delta,$nominal,$unit);
                }
                else
                {
                    $hot[$incr][1] = "";
                    $hot[$incr][2] = "";
                }
            }

            if(count($nominals)>0)
            {
                $hot[4][1] = min($nominals)." ".$unitMin." - ".max($nominals)." ".$unitMax;
            }
            $hot[8][5] = implode(", ", array_unique($referenceStandards));
            $hot[8][12] = implode(", ", array_unique($referenceStandardSerialNumbers));
        }

        return response($hot);
    }

    public function result($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrders = ServiceOrders::where("service_request_item_id",$id)
        // SEMENTARA
        // ->where("stat_service_order",0)
        // ->where("is_finish",0)
        ->first();
        // SEMENTARA
        // if(!$serviceOrders) abort("404");
        // dd($serviceOrders);
        $tmp_report_id = $serviceOrders->tmp_report_id?$serviceOrders->tmp_report_id:0;
        $hasil_uji = $serviceOrders->hasil_uji?$serviceOrders->hasil_uji:"[]";

        $mMetodeUji = $this->MasterMetodeUji->dropdown();

        $template_format_item = MasterTemplate::where("laboratory_id",Auth::user()->laboratory_id)
        ->pluck('title_tmp', 'id');

        $template_rows = MasterTemplate::where("laboratory_id",Auth::user()->laboratory_id)
        ->select("header_tmp","column_tmp","id")->get();

        $template_header = $template_setting = array();
        foreach ($template_rows as $row)
        {
            if($row->header_tmp!=NULL) $template_header[$row->id] = json_decode($row->header_tmp,true);
            if($row->column_tmp!=NULL) $template_setting[$row->id] = json_decode($row->column_tmp,true);
        }

        $template_header = json_encode($template_header);
        $template_setting = json_encode($template_setting);
        $negara = $this->MasterNegara->dropdown_values();
        $massajenis = $this->MassaJenis->dropdown();
        $masscomp = $this->MassaMasscomp->dropdown();
        $serialnumber = $this->MassaSerialnumber->dropdown();

        return view('service.result',compact(
            'mMetodeUji',
            'hasil_uji',
            'tmp_report_id',
            'serviceOrders',
            // 'mStandardUkuran',
            'template_format_item',
            'template_header',
            'template_setting',
            'attribute',
            "negara",
            "massajenis",
            "serialnumber",
            "masscomp"
        ));
    }

    public function simpan(Request $request)
    {
        $response["status"] = false;

        $rules['hasil_uji']=['required'];
        $rules['tmp_report_id']=['required'];

        $validation = Validator::make($request->all(),$rules,error_messages());
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            ServiceOrders::whereId($request->id)->update([
                "hasil_uji"=>$request->hasil_uji,
                "tmp_report_id"=>$request->tmp_report_id
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return response($response);
    }

    public function finish($id)
    {
        $row = ServiceOrders::whereId($id)
        ->whereNotNull("hasil_uji")
        ->whereNotNull("tmp_report_id")->first();

        if($row)
        {
            ServiceOrders::whereId($id)
            ->update([
                "stat_service_order"=>1,
                "lab_staff_out"=>Auth::id(),
                "staff_entry_dateout"=>date("Y-m-d")
            ]);

            ServiceRequest::whereId($row->service_request_id)
            ->update([
                "stat_service_request"=>3
            ]);

            ServiceRequestItem::whereId($row->service_request_item_id)
            ->update([
                "stat_service_request_item"=>2
            ]);
        }
        return Redirect::route('service');
    }

    public function warehouse($id)
    {
        ServiceOrders::whereId($id)->update(["is_finish"=>1]);
        return Redirect::route('service');
    }

    public function history()
    {
        $attribute = $this->MyProjects->setup();

        $rows = ServiceOrders::where("is_finish",1)
        ->orderBy('staff_entry_dateout','desc')
        ->get();

        return view('service.history',compact('rows','attribute'));
    }

    private function AirDensity($THRPrata2)
    {
        $S5 = $THRPrata2[2]*100;
        $S6 = $THRPrata2[0];
        $S7 = $THRPrata2[1]/100;
        $S8 = 11;

        $P5 = 1.2378847/100000;
        $P6 = -0.019121316;
        $P7 = 33.93711047;
        $P8 = -6343.1645;
        $P9 = $S8 + 273.15;
        $P10 = pow($P9,2);
        $P12 = 1*pow(($P5*$P10)+($P6*$P9)+$P7+($P8/$P9),2);

        $M5 = 1.00062;
        $M6 = 3.14/100000000;
        $M7 = 5.6/10000000;
        $M8 = $S5;
        $M9 = $S6+273.15;
        $M11 = $M5+($M6*$M8)+($M7*pow($M9,2));

        $J5 = $S7;
        $J6 = $M11;
        $J7 = $P12;
        $J8 = $S5;
        $J10 = $J5*$J6*($J7/$J8);

        // dd($J10,$P12,$M11);
        $F5 = 1.58123/1000000;
        $F6 = -2.9331/100000000;
        $F7 = 1.1043/10000000000;
        $F8 = 5.707/1000000;
        $F9 = -2.051/100000000;
        $F10 = 1.9898/10000;
        $F11 = -2.376/1000000;
        $F12 = 1.83/100000000000;
        $F13 = 0.765/100000000;
        $F14 = $S5;
        $F15 = $S6+273.15;
        $F16 = $S6+273.15;
        $F17 = $J10;
        $F18 = pow($F17,2);
        $F20 = (1-(($F14/$F15)*($F5+($F6*$F16)+($F7*pow($F16,2))+(($F8+($F9*$F16))*$F17)+(($F10+($F11*$F16))*$F18)))+((pow($F14,2)/pow($F15,2))*($F12+($F13*$F18))));

        $B5 = 3.48374;
        $B6 = 1.4446;
        $B7 = 400/1000000;
        $B8 = 0.0004;
        $B9 = $S5;
        $B10 = $F20;
        $B11 = $S6+273.15;
        $B12 = 0.378;
        $B13 = $J10;
        $B15 = ($B5+($B6*($B7-$B8)))*(($B9/($B10*$B11))*(1-$B12*$B13));
        $B16 = $B15/1000;

        return $B15;
    }

    private function AirDensity2()
    {
        $kompz = pow((22/1000000) * 1,2);
        $ntuan_tekanan_udara = pow((100*0.00001),2);
        $ntuan_suhu = pow((0.71*-0.004),2);
        $ntuan_kelembapan_relatif =	pow((3.7*-0.009),2);
        $ntuan_dew_point = pow((0.3*-0.0003),2);
        $ntuan_co2 = pow((0.0001*0.4),2);
        $sum = $kompz+$ntuan_tekanan_udara+$ntuan_suhu+$ntuan_kelembapan_relatif+$ntuan_dew_point+$ntuan_co2;

        return sqrt($sum);
    }

    private function MassaJenisFunc($refdensity)
    {
        //[1] PLATINA, 21400
        $jenis = explode("]",$refdensity);
        $id = substr($jenis[0],1);
        $row = MassaJenis::find($id);

        return [$row->massajenis,$row->uncertainty];
    }

    private function Standard_Deviation($arr)
    {
        $num_of_elements = count($arr);

        $variance = 0.0;

        // calculating mean using array_sum() method
        $average = array_sum($arr)/$num_of_elements;

        foreach($arr as $i)
        {
            // sum of squares of differences between
                        // all numbers and means.
            $variance += pow(($i - $average), 2);
        }

        return (float)sqrt($variance/$num_of_elements);
    }

    private function getKonvensional($serialnumber,$nominal,$unit)
    {
        $table = new MassaKonvensional();
        $row = $table->where("serialnumber",$serialnumber)
        ->where("nominal",$nominal)
        ->where("unit",$unit)
        ->first();

        return $row;
    }

    private function GetDrift($serialnumber,$nominal,$unit)
    {
        $row = MassaDrift::where("serialnumber",$serialnumber)
        ->where("nominal",$nominal)
        ->where("unit",$unit)
        ->first();

        return $row;
    }

    private function GetMasscomp($masscomp)
    {
        //[1] XP 56, 01123, 1000mg
        $arr = explode("]",$masscomp);
        $id = substr($arr[0],1);
        $row = MassaMasscomp::find($id);

        return [$row->sensitifitas,$row->dayabaca];
    }

    private function ReferenceStandardSerialNumber($referenceStandardSerialNumber)
    {
        //[25] F2, 110023;
        $data = explode("]",$referenceStandardSerialNumber);
        $id = substr($data[0],1);
        return MassaSerialnumber::find($id);
    }

    public function KalkulasiInterpolasi($data,$input)
    {
        $prev_key = null;
        $next_key = null;

        foreach (array_keys($data)as $key) {
            // convert to timestamp for comparison
            if ($key < $input) {
                $prev_key = $key;
            } else {
                //  already found next key, move on
                if ($next_key == null) {
                    $next_key = $key;
                }
            }
        }

        $prev_value = $data[$prev_key];
        $next_value = $data[$next_key];

        $temp = ((($input) - ($prev_key)) * ($next_value - $prev_value) / (($next_key) - ($prev_key)));
        $y = $temp + $prev_value;

        return $y;
    }

    private function InterpolasiTermohygro($suhuStart,$suhuEnd,$termohygroStart,$termohygroEnd,$barometerStart,$barometerEnd)
    {
        $rows = MassaInterpolasiSuhu::get();
        $data = [];
        $u_suhu = [];
        foreach($rows as $row)
        {
            $data[$row->pembacaan] = $row->koreksi;
            $u_suhu[] = $row->u;
        }

        $indicationSuhu = ($suhuStart+$suhuEnd)/2;
        $koreksiSuhu = $this->KalkulasiInterpolasi($data,$indicationSuhu);
        $u_suhu[] = $koreksiSuhu;
        // $koreksiSuhu = -0.381176471;
        $actualSuhu = $indicationSuhu+$koreksiSuhu;

        $rows = MassaInterpolasiTermohygro::get();
        $data = [];
        $u_termohygro = [];
        foreach($rows as $row)
        {
            $data[$row->pembacaan] = $row->koreksi;
            $u_termohygro[] = $row->u;
        }
        $indicationTermohygro = ($termohygroStart+$termohygroEnd)/2;
        $koreksiTermohygro = $this->KalkulasiInterpolasi($data,$indicationTermohygro);
        $u_termohygro[] = $koreksiTermohygro;
        // $koreksiTermohygro = -34.32946058;
        $actualTermohygro = $indicationTermohygro+$koreksiTermohygro;

        $rows = MassaInterpolasiBarometer::get();
        $data = [];
        foreach($rows as $row)
        {
            $data[$row->pembacaan] = $row->koreksi;
        }
        $indicationBarometer = ($barometerStart+$barometerEnd)/2;
        $koreksiBarometer = $this->KalkulasiInterpolasi($data,$indicationBarometer);
        // $koreksiBarometer = -1.519298246;
        $actualBarometer = $indicationBarometer+$koreksiBarometer;

        return [
            $actualSuhu,
            $actualTermohygro,
            $actualBarometer,
            $koreksiSuhu,
            $koreksiTermohygro,
            $koreksiBarometer,
            $u_suhu,
            $u_termohygro
            ];
    }

    private function CheckMPE_CMC($Uu,$nominal,$unit)
    {
        $row = MassaMPE::where("nominal",$nominal)
        ->where("unit",$unit)
        ->where("kelas","CMC")
        ->first();

        if($Uu < $row->sensitifitas) return $row->sensitifitas; else return $Uu;
    }

    private function CheckMPE_Kelas($delta,$nominal,$unit)
    {
        $rows = MassaMPE::where("nominal",$nominal)
        ->where("unit",$unit)
        ->where("kelas","<>","CMC")
        ->orderBy("sensitifitas","asc")
        ->get();

        foreach($rows as $row)
        {
            if($delta < $row->sensitifitas)
            {
                return $row->kelas;
                break;
            }
        }

        return "-";
    }

    private function Class_TE($n)
    {
        if($n<=1000)
        {
            return "IV";
        }
        else if($n<=10000)
        {
            return "III";
        }
        else if($n<=100000)
        {
            return "II";
        }
        return "I";
    }

    private function MPE_TE($nominal,$kelas,$var_e)
    {
        // dd($nominal,$kelas,$var_e);
        if($kelas==="I")
        {
            if($nominal<=50000)
            {
                return (0.5*$var_e);
            }
            else if($nominal<=200000)
            {
                return (1.0*$var_e);

            }
            else if($nominal>200000)
            {
                return (1.5*$var_e);
            }
        }
        else if($kelas==="II")
        {
            if($nominal<=($var_e*5000))
            {
                return (0.5*$var_e);
            }
            else if($nominal<=($var_e*20000))
            {
                return (1.0*$var_e);

            }
            else if($nominal<=(100000*$var_e))
            {
                return (1.5*$var_e);
            }
        }
        else if($kelas==="III")
        {
            if($nominal<=($var_e*500))
            {
                return (0.5*$var_e);
            }
            else if($nominal<=($var_e*2000))
            {
                return (1.0*$var_e);

            }
            else if($nominal<=($var_e*10000))
            {
                return (1.5*$var_e);
            }
        }
        else if($kelas==="IV")
        {
            if($nominal<=($var_e*50))
            {
                return (0.5*$var_e);
            }
            else if($nominal<=($var_e*200))
            {
                return (1.0*$var_e);

            }
            else if($nominal<=($var_e*1000))
            {
                return (1.5*$var_e);
            }
        }
    }

    private function MassaKonvensionalte($data,$nominal,$ustd=false)
    {
        if(strlen($nominal)>0)
        {
            foreach($data as $nom=>$v)
            {
                // dd($v);
                if($nominal<=$nom)
                {
                    if($ustd) return $v["usert"];
                    return $v["mc"];
                }
            }
        }
        return 0;
    }
}
