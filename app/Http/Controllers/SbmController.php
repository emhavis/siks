<?php

namespace App\Http\Controllers;

use App\MasterSbm;
use App\MasterProvince;
use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SbmController extends Controller
{
    private $MasterSbm;
    private $MasterProvinsi;
    private $MyProjects;

    public function __construct() 
    {
        $this->MasterSbm = new MasterSbm();
        $this->MasterProvinsi = new MasterProvince();
        $this->MyProjects = new MyProjects();
    }

    public function index() 
    {
        $attribute = $this->MyProjects->setup("sbm");

        $sbmList = $this->MasterSbm->get();
        return view('sbm.index', compact('sbmList','attribute'));
    }

    public function create($id=null) 
    {
        $attribute = $this->MyProjects->setup("sbm");

        $row = null;

        if($id)
        {
            $row = $this->MasterSbm->whereId($id)->first();
        }

        $provinsiList = $this->MasterProvinsi->pluck('nama', 'id');

        return view('sbm.create',compact('row','id','attribute','provinsiList'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["provinsi_id"] = ['required'];
        $rules["luar"] = ['required', 'integer', 'min:0'];
        $rules["dalam"] = ['required', 'integer', 'min:0'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->MasterSbm->whereId($id)->update($request->all());
            }
            else
            {
                $this->MasterSbm->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->MasterSbm->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }

    public function getbyid($id)
    {
        $row = $this->MasterLaboratory->find($id);
        $response["lab"] = $row;
        
        $rowsInstalasi = $this->MasterInstalasi->where("lab_id", $id)->get();
        $response["instalasi"] = $rowsInstalasi;

        return response($response);
    }
}
