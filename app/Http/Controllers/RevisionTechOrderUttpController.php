<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\MyClass\MyProjects;
use App\RevisionUttp;
use App\RevisionUttpTTUInspection;
use App\RevisionUttpTTUInspectionItems;
use App\RevisionUttpTTUInspectionBadanHitung;

use App\MasterInstalasi;
use App\ServiceOrderUttps;

use PDF;

class RevisionTechOrderUttpController extends Controller
{
    // protected $StandardInspectionPrice;
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("revisiontechorderuttp");

        $laboratory_id = Auth::user()->laboratory_id;
        $instalasi_id = Auth::user()->instalasi_id;

        $instalasiList = MasterInstalasi::where('lab_id', $laboratory_id)->orderBy('id','asc')
            ->get();

        foreach($instalasiList as $instalasi) {
            $rows_processing[$instalasi->id] = RevisionUttp::whereIn('status', [3])
                ->join('service_order_uttps', 'service_order_uttps.id', '=', 'certificate_revision_uttps.order_id')
                ->where('service_order_uttps.laboratory_id', $laboratory_id)
                ->where(function($query) {
                    $query->where('status_revision', 0)
                          ->orWhereNull('status_revision');
                })
                ->where('service_order_uttps.instalasi_id', $instalasi->id)
                ->select('certificate_revision_uttps.*')
                ->orderBy('updated_at','desc')->get();
        }
            

        return view('revisiontechorderuttp.index', compact('rows_processing', 'attribute', 'instalasiList'));
    }

    public function proses($id)
    {
        $attribute = $this->MyProjects->setup("revisiontechorderuttp");

        $revision = RevisionUttp::find($id);
        $ttu = RevisionUttpTTUInspection::where('cert_rev_id', $id)->first();
        $ttuItems = RevisionUttpTTUInspectionItems::where('cert_rev_id', $id)->get();
        $badanHitungs = RevisionUttpTTUInspectionBadanHitung::where('cert_rev_id', $id)->get();

        return view('revisiontechorderuttp.edit', compact(['attribute', 'revision',
            'ttu',
            'ttuItems',
            'badanHitungs',
        ]));
    }

    public function simpanproses($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("revisiontechorderuttp");
        
        $revision = RevisionUttp::find($id);
        
        $file = $request->file('file_skhp');
        $data = array();
        if ($file != null) {
            $data['file_lamp_skhp'] = $file->getClientOriginalName();

            $path = $file->store(
                'skhp',
                'public'
            );
            $data['path_lamp_skhp'] = $path;
        }
        /*
        if ($revision->order->ServiceRequest->service_type_id == 4 || $revision->order->ServiceRequest->service_type_id == 5) {
            
            if ($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge') {
                RevisionUttpTTUInspectionBadanHitung::where("cert_rev_id", $id)->delete();
                RevisionUttpTTUInspection::where("cert_rev_id", $id)->delete();
                RevisionUttpTTUInspectionItems::where("cert_rev_id", $id)->delete();

                foreach($request->get('badanhitung_item_id') as $index=>$value) {
                    RevisionUttpTTUInspectionBadanHitung::insert([
                        "cert_rev_id" => $id,
                        "type" => $request->input("type.".$index),
                        "serial_no" => $request->input("serial_no.".$index),
                    ]);
                }

                RevisionUttpTTUInspection::insert([
                    "cert_rev_id" => $id,
                    "tank_no" => $request->input("tank_no"),
                    "tag_no" => $request->input("tag_no"),
                ]);

                foreach($request->get('item_id') as $index=>$value) {
                    RevisionUttpTTUInspectionItems::insert([
                        "cert_rev_id" => $id,
                        "input_level" => $request->input("input_level.".$index),
                        "error_up" => $request->input("error_up.".$index),
                        "error_down" => $request->input("error_down.".$index),
                        "error_hysteresis" => $request->input("error_hysteresis.".$index),
                    ]);
                }
            }
            if ($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM') {
                RevisionUttpTTUInspectionBadanHitung::where("cert_rev_id", $id)->delete();
                RevisionUttpTTUInspection::where("cert_rev_id", $id)->delete();
                RevisionUttpTTUInspectionItems::where("cert_rev_id", $id)->delete();

                foreach($request->get('badanhitung_item_id') as $index=>$value) {
                    RevisionUttpTTUInspectionBadanHitung::insert([
                        "cert_rev_id" => $id,
                        "brand" => $request->input("brand.".$index),
                        "type" => $request->input("type.".$index),
                        "serial_no" => $request->input("serial_no.".$index),
                    ]);
                }

                RevisionUttpTTUInspection::insert([
                    "cert_rev_id" => $id,
                    "totalisator" => $request->input("totalisator"),
                    "kfactor" => $request->input("kfactor"),
                ]);

                foreach($request->get('item_id') as $index=>$value) {
                    RevisionUttpTTUInspectionItems::insert([
                        "cert_rev_id" => $id,
                        "flow_rate" => $request->input("flow_rate.".$index),
                        "error" => $request->input("error.".$index),
                        "meter_factor" => $request->input("meter_factor.".$index),
                        "repeatability" => $request->input("repeatability.".$index),
                    ]);
                }
            }
            if ($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas') {
                RevisionUttpTTUInspection::where("cert_rev_id", $id)->delete();
                RevisionUttpTTUInspectionItems::where("cert_rev_id", $id)->delete();

                RevisionUttpTTUInspection::insert([
                    "cert_rev_id" => $id,
                    "totalisator" => $request->input("totalisator"),
                    "kfactor" => $request->input("kfactor"),
                ]);

                foreach($request->get('item_id') as $index=>$value) {
                    RevisionUttpTTUInspectionItems::insert([
                        "cert_rev_id" => $id,
                        "flow_rate" => $request->input("flow_rate.".$index),
                        "error" => $request->input("error.".$index),
                        "repeatability" => $request->input("repeatability.".$index),
                    ]);
                }
            }
            if ($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air') {
                RevisionUttpTTUInspection::where("cert_rev_id", $id)->delete();
                RevisionUttpTTUInspectionItems::where("cert_rev_id", $id)->delete();

                RevisionUttpTTUInspection::insert([
                    "cert_rev_id" => $id,
                    "totalisator" => $request->input("totalisator"),
                ]);

                foreach($request->get('item_id') as $index=>$value) {
                    RevisionUttpTTUInspectionItems::insert([
                        "cert_rev_id" => $id,
                        "flow_rate" => $request->input("flow_rate.".$index),
                        "error" => $request->input("error.".$index),
                        "repeatability" => $request->input("repeatability.".$index),
                    ]);
                }
            }
            if ($revision->order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)') {
                RevisionUttpTTUInspection::where("cert_rev_id", $id)->delete();
                RevisionUttpTTUInspectionItems::where("cert_rev_id", $id)->delete();

                RevisionUttpTTUInspection::insert([
                    "cert_rev_id" => $id,
                    "totalisator" => $request->input("totalisator"),
                    "kfactor" => $request->input("kfactor"),
                ]);

                foreach($request->get('item_id') as $index=>$value) {
                    RevisionUttpTTUInspectionItems::insert([
                        "cert_rev_id" => $id,
                        "flow_rate" => $request->input("flow_rate.".$index),
                        "error" => $request->input("error.".$index),
                        "error_bkd" => $request->input("error_bkd.".$index),
                        "repeatability" => $request->input("repeatability.".$index),
                        "repeatability_bkd" => $request->input("repeatability_bkd.".$index),
                    ]);
                }
            }
        }
        */

        if($data != null && count($data)) {
            $revision->update($data);
        }

        $revision->update([
            "status" => 3,
            "lab_staff_id" => Auth::id(),
            "lab_staff_at" => date("Y-m-d H:i:s"),
            //"file_lamp_skhp" => $data["file_lamp_skhp"] != null ? $data["file_lamp_skhp"] : null,
            //"path_lamp_skhp" => $data["path_lamp_skhp"] != null ? $data["path_lamp_skhp"] : null,
            //"seri_revisi" => $next,
            "status_approval" => 0,
            "status_revision" => $request->get("status_revision"),
        ]);

        return Redirect::route('revisiontechorderuttp');
    }


    public function print($id, $stream = 0)
    {
        $revision = RevisionUttp::find($id);
        // $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);
        
        $file_name = 'SKHP '.($revision->order->no_sertifikat ? $revision->order->no_sertifikat : $revision->order->ServiceRequestItem->no_order);

        $next_count = $revision->preview_count + 1;
        $revision->update([
            'preview_count' => $next_count
        ]);

        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $revision->order->qrcode_token,
        ]);
       
        $blade = 'revisiontechorderuttp.skhp_tipe_pdf';
        if ($revision->request->service_type_id == 4 || 
            $revision->request->service_type_id == 5) {
                $blade = 'revisiontechorderuttp.skhp_ttu_pdf';
        }

        $view = false;
        
        $order = $revision->order;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact('revision','order', 'qrcode_generator', 'view'));

        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }

    public function printTipe($id, $stream = 0)
    {
        $revision = RevisionUttp::find($id);
        $order = $revision->order;

        if ($order->is_skhpt) {
            $next_count = $revision->preview_count + 1;
            $revision->update([
                'preview_count' => $next_count
            ]);
        }

        //$file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);
        $file_name = 'SKHPT '.($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhpt',
            'token' => $order->qrcode_skhpt_token,
        ]);
        
        $blade = 'revisiontechorderuttp.skhpt_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact('revision', 'order', 'qrcode_generator', 'view'));
     
        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }

    public function printSETipe($id, $stream = 0)
    {
        $revision = RevisionUttp::find($id);
        $order = $revision->order;
        /*
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);
        */

        $file_name = 'SET '.($order->no_surat_tipe ? $order->no_surat_tipe : $order->ServiceRequestItem->no_order);

        $otherOrders = ServiceOrderUttps::whereHas(
            'ServiceRequestItem', function($query) use($order)
            {
                $query->where("uttp_id",$order->ServiceRequestItem->uttp_id);  
            })
            ->get();
       
        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'set',
            'token' => $order->qrcode_tipe_token,
        ]);

        $blade = 'revisiontechorderuttp.set_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact(['revision', 'order', 'otherOrders', 'qrcode_generator', 'view']));
       
        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }
}
