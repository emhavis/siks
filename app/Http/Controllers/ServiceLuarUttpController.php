<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrderUttpTTUInspection;
use App\ServiceOrderUttpTTUInspectionItems;
use App\ServiceOrderUttpTTUInspectionSuhu;
use App\ServiceOrderUttpTTUInspectionBadanHitung;
use App\ServiceOrderUttpTTUPerlengkapan;
use App\ServiceOrderUttpEvaluasiTipe;

use App\ServiceOrderUttpTTUCTMSPerlengkapan;
use App\ServiceOrderUttpTTUCTMS;
use App\ServiceOrderUttpTTUCTMSTank;
use App\ServiceOrderUttpTTUCTMSGauge;
use App\ServiceOrderUttpTTUCTMSSegmen;
use App\ServiceOrderUttpTTUCTMSHeader;
use App\ServiceOrderUttpTTUCTMSSistem;

use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\ServiceRequestUttpStaff;

use App\Mail\Pending;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;

use App\MasterInstalasi;
use App\HistoryUttp;
use App\ServiceOrders;
use App\Uttp;
use App\MasterPetugasUttp;

use App\ServiceOrderUttpInsituLaporan;
use App\ServiceRequestUttpItemTTUPerlengkapan;
use App\MasterUttpType;


class ServiceLuarUttpController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("serviceluaruttp");

        //$laboratory_id = Auth::user()->laboratory_id;
        //$instalasi_id = Auth::user()->instalasi_id;

        //$instalasiList = MasterInstalasi::where('lab_id', $laboratory_id)->orderBy('id','asc')
        //    ->get();

        $petugas_id = Auth::user()->petugas_uttp_id;
        if ($petugas_id == null) {
            $petugas_id = Auth::user()->petugas_uut_id;
        }

        $rows = ServiceOrderUttps::join('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_order_uttps.service_request_item_id')
            ->join('service_request_uttps', 'service_request_uttps.id', '=', 'service_order_uttps.service_request_id')
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->whereIn("stat_service_order",[0,1,2,3,4])
            //->whereIn('service_request_uttp_items.status_id', [13, 18, 19, 20])
            ->where('service_request_uttps.lokasi_pengujian', 'luar')
            ->where('service_request_uttp_insitu_staff.scheduled_id', $petugas_id)
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id",">=", 11);
            })
            ->whereHas('ServiceRequest', function($query) 
            {
                $query->where("lokasi_pengujian","=", 'luar');
            })
            /*
            ->whereHas('ServiceRequestItem', function($query)
            {
                $query->where("status_id", "=", 13);
            })
            ->whereHas('ServiceRequest', function($query) use ($petugas_id) 
            {
                $query->where("lokasi_pengujian","=", 'luar')
                ->whereHas('spuhDoc', function($query) use ($petugas_id)  {
                    $query->whereHas('listOfStaffs', function($query) use ($petugas_id)  {
                        $query->where('scheduled_id', $petugas_id);
                    });
                });
            })
            */
            ->where("is_finish",0)
            ->where(function($query) {
                $query->where('stat_sertifikat', 0)
                      ->orWhereNull('stat_sertifikat');
            })
            ->whereNull('subkoordinator_notes')
            //->where('laboratory_id', $laboratory_id)
            //->where('instalasi_id', $instalasi->id)
            ->where(function($query) {
                $query->whereNull('test_by_1')
                    ->orWhere('test_by_1', Auth::id())
                    ->orWhere('test_by_2', Auth::id());
            })
            ->orderBy('staff_entry_datein','desc')
            ->select('service_order_uttps.*')
            ->get();

        //dd([$rows, $petugas_id]);

        return view('serviceluaruttp.index',compact('rows','attribute'));
    }



    public function proses($id)
    {
        $attribute = $this->MyProjects->setup("serviceluaruttp");


        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        $request = ServiceRequestUttp::find($serviceOrder->service_request_id);
        $staffes = ServiceRequestUttpStaff::where('request_id', $request->id)->get();

        return view('serviceluaruttp.proses',compact(
            'serviceOrder',
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function saveproses($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $serviceRequest = ServiceRequestUttp::find($order->service_request_id);

        $serviceRequest->status_id = 12;
        $serviceRequest->save();

        $items = ServiceRequestUttpItem::where("request_id", $id)->get();
        foreach($items as $item) {
            $history = new HistoryUttp();
            $history->request_status_id = 12;
            $history->request_id = $id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $item->status_id = 12;
            $item->save();
        }

        if ($request->hasFile('spuh_file')) {
            $file = $request->file('spuh_file');
            $file_spuh = $file->getClientOriginalName();
            $path = $file->store(
                'spuh_dl',
                'public'
            );
            $path_spuh = $path;

            ServiceRequestUttpInsituDoc::where("id", $serviceRequest->spuh_doc_id)
            ->update([
                "spuh_file" => $file_spuh,
                "spuh_path" => $path_spuh,
            ]);
        }

        return Redirect::route('serviceluaruttp');

    }

    public function test($id)
    {
        $attribute = $this->MyProjects->setup("serviceluaruttp");


        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        $request = ServiceRequestUttp::find($serviceOrder->service_request_id);
        $staffes = ServiceRequestUttpStaff::where('request_id', $request->id)->get();

        return view('serviceluaruttp.test',compact(
            'serviceOrder',
            'request',
            'staffes',
            'attribute'
        ));
    }

    public function savetest($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);
        $item = ServiceRequestUttpItem::find($order->service_request_item_id);
        $serviceRequest = ServiceRequestUttp::find($order->service_request_id);
        $doc = ServiceRequestUttpInsituDoc::find($serviceRequest->spuh_doc_id);

        $has_set = false;
        $is_skhpt = false;

        $only_pemeriksaan = false;
        
        if (count($item->inspections) == 1 && $item->inspections[0]->inspectionPrice->is_pemeriksaan == true) {
            $is_skhpt = true;
            $only_pemeriksaan = true;
        }

        $order->update([
            "stat_service_order" => 1,
            "staff_entry_datein" => $doc->date_from,
            "staff_entry_dateout" => $doc->date_to,
            "mulai_uji" => $doc->date_from,
            "selesai_uji" => $doc->date_to,
            "stat_warehouse" => -1,
            "has_set" => $has_set,
            "is_skhpt" => $is_skhpt
        ]);

        //$inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        

        if ($serviceRequest->service_type_id == 4 || $serviceRequest->service_type_id == 5) {
            foreach($item->perlengkapans as $perlengkapan) {
                $ttuPerlengkapanModel = new ServiceOrderUttpTTUPerlengkapan();
                $ttuPerlengkapan = [
                    'order_id' => $id,
                    'uttp_id' => $perlengkapan->uttp_id
                ];
                $ttuPerlengkapanModel->create($ttuPerlengkapan);
            }
        } elseif ($serviceRequest->service_type_id == 6 || $serviceRequest->service_type_id == 7) {
            $inspectionItemsQ = DB::table('uttp_inspection_items')
                //->where('uttp_type_id', $item->uttp->type_id)
                //->where('template_id', $inspection->inspectionPrice->inspection_template_id)
                ->where('template_id', $item->uttp->type->template_id)
                ->orderBy('order_no', 'asc');
            if ($only_pemeriksaan == true) {
                $inspectionItemsQ = $inspectionItemsQ->where('is_pemeriksaan', true);
            }

            ServiceOrderUttpInspections::where("order_id", $order->id)->delete();

            $inspectionItems = $inspectionItemsQ->get();
            foreach ($inspectionItems as $inspectionItem) {
                ServiceOrderUttpInspections::insert([
                    'order_id' => $order->id,
                    'inspection_item_id' => $inspectionItem->id,
                    'order_no' => $inspectionItem->order_no,
                ]);
            }

            // prefix dan kndl
            $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
            //$lastNo = $serviceType->last_no;
            //$nextNo = intval($lastNo)+1;
            $prefix = $serviceType->prefix;
            $kndl = $order->ServiceRequest->lokasi_pengujian == 'dalam' ? 'KN' : 'DL';

            if ($order->cancel_at != null){
                $kndl = 'KET';
            }

            // last no skhpt
            $lastOrder = ServiceOrderUttps::where("id", "<>", $id)
                ->whereNotNull("no_sertifikat_tipe_int")
                ->where('no_sertifikat_tipe_year', date("Y"))
                ->orderBy("no_sertifikat_tipe_int", "desc")
                ->select("no_sertifikat_tipe_int")
                ->first();

            if ($lastOrder != null)
            {
                $lastNo = $lastOrder->no_sertifikat_tipe_int;
                $nextNo = intval($lastNo)+1;
            }
            else 
            {
                $lastNo = 0;
                $nextNo = 1;
            }

            //$kndl = 'KN';
            //$noSertifikat = $prefix.'.'.sprintf("%04d", $nextNo).'/PKTN.4.8/'.$kndl.'/'.date("m/Y");
            //$noService = $serviceType->prefix.'-'.date("y").'-'.sprintf("%04d", $nextNo);
            
            $noSertifikatTipe = $prefix.'T.'.sprintf("%04d", $nextNo).'/PKTN.4.5/'.$kndl.'/'.date("m/Y");
            
            $order->update([
                "no_sertifikat_tipe" => $noSertifikatTipe,
                "no_sertifikat_tipe_int" => $nextNo,
                "no_sertifikat_tipe_year" => date("Y"),
            ]);
        }
        
        $item->status_id = 13;
        $item->save();

        return Redirect::route('serviceluaruttp.result', $id);
    }

        

    public function savetestqr(Request $request)
    {
        $item = ServiceRequestUttpItem::where('no_order', $request->no_order)->first();
        $order = ServiceOrderUttps::where('service_request_item_id', $item->id)->first();

        $order->update([
            "stat_service_order" => 1,
            "staff_entry_dateout" => date("Y-m-d"),
            "stat_warehouse" => -1,
        ]);

        //$inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        $item = ServiceRequestUttpItem::find($order->service_request_item_id);
        $serviceRequest = ServiceRequestUttp::find($order->service_request_id);

        if ($serviceRequest->service_type_id == 4 || $serviceRequest->service_type_id == 5) {
            foreach($item->perlengkapans as $perlengkapan) {
                $ttuPerlengkapanModel = new ServiceOrderUttpTTUPerlengkapan();
                $ttuPerlengkapan = [
                    'order_id' => $id,
                    'uttp_id' => $perlengkapan->uttp_id
                ];
                $ttuPerlengkapanModel->create($ttuPerlengkapan);
            }
        } elseif ($serviceRequest->service_type_id == 6 || $serviceRequest->service_type_id == 7) {
            $inspectionItems = DB::table('uttp_inspection_items')
                //->where('uttp_type_id', $item->uttp->type_id)
                //->where('template_id', $inspection->inspectionPrice->inspection_template_id)
                ->where('template_id', $item->uttp->type->template_id)
                ->orderBy('order_no', 'asc')->get();
            foreach ($inspectionItems as $inspectionItem) {
                ServiceOrderUttpInspections::insert([
                    'order_id' => $order->id,
                    'inspection_item_id' => $inspectionItem->id
                ]);
            }
        }
        
        /*
        $inspection->update([
            'status_id' => 13
        ]);

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_13 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>13]);
        }
        */
        $item->status_id = 13;
        $item->save();

        $history = new HistoryUttp();
        $history->request_status_id = 13;
        $history->request_id = $item->request_id;
        $history->request_item_id = $item->id;
        $history->user_id = Auth::id();
        $history->save();

        ServiceRequestUttpItemInspection::where("request_item_id", $item->id)->update([
            "status_id"=>13,
        ]);

        $items_count = DB::table('service_request_uttp_items')
            ->selectRaw('sum(case when status_id >= 13 then 1 else 0 end) count_13, count(id) count_all')
            ->where('request_id', $order->service_request_id)->get();

        if ($items_count[0]->count_13 == $items_count[0]->count_all) {
            ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>13]);
        }

        //return Redirect::route('serviceuttp');
        return Redirect::route('serviceuttp.result', $order->id);
    }

    public function result($id)
    {
        $attribute = $this->MyProjects->setup("serviceluaruttp");

        $serviceOrder = ServiceOrderUttps::find($id);

        /*
        $insituStaf = $serviceOrder->ServiceRequest->spuhDoc->listOfStaffs->map(function($item,$key) {
            return $item->scheduled_id;
        })->toArray();

        $petugasList = MasterPetugasUttp::whereIn('id', $insituStaf)->pluck('id');

        $users = MasterUsers::whereIn('petugas_uttp_id', $petugasList)->pluck('full_name', 'id');
        $users = $users->prepend('-- Pilih Pegawai --', null);
        */

        /*
        $users = MasterUsers::whereNotNull('petugas_uttp_id')
            ->whereHas('PetugasUttp', function($query) 
            {
                $query->where('flag_unit', 'uttp')
                    ->where('is_active', true);
            })
            ->pluck('full_name', 'id')->prepend('-- Pilih Pegawai --','');
            */
        
        $user = MasterUsers::find(Auth::id());

        $oimls = DB::table("master_oimls")->pluck('oiml_name', 'id');
        $inspectionItems = ServiceOrderUttpInspections::where('order_id', $id)
            ->orderBy('order_no', 'asc')->orderBy('id', 'asc')->get();
            
        $sertifikat_expired_at = date('Y-m-d', 
            strtotime( $serviceOrder->staff_entry_dateout . ' + '. 
            ($serviceOrder->ServiceRequestItem->uttp->type->masa_berlaku_ttu == null ?  0 : 
            $serviceOrder->ServiceRequestItem->uttp->type->masa_berlaku_ttu) .
            ' year'));

        $ttu = ServiceOrderUttpTTUInspection::where('order_id', $id)->first();
        $ttuItems = ServiceOrderUttpTTUInspectionItems::where('order_id', $id)->get();
        //$ttuItems = DB::select("select id, panjang_sebenarnya::varchar from service_order_uttp_ttu_insp_items where order_id = 44");
        //dd($ttuItems);
        $ttuSuhu = ServiceOrderUttpTTUInspectionSuhu::where('order_id', $id)->get();
        $badanHitungs = ServiceOrderUttpTTUInspectionBadanHitung::where('order_id', $id)->get();
        $ttuPerlengkapans = ServiceOrderUttpTTUPerlengkapan::where('order_id', $id)->get();
        $tipe = ServiceOrderUttpEvaluasiTipe::where('order_id', $id)->first();

        $units = DB::table('master_uttp_units')
                    ->where('uttp_type_id', $serviceOrder->tool_type_id)
                    ->pluck('unit', 'unit')
                    ->prepend($serviceOrder->tool_capacity_unit, $serviceOrder->tool_capacity_unit);
        $units->prepend(' ', ' ');

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        $docs = ServiceRequestUttpInsituStaff::where('doc_id', $serviceOrder->ServiceRequest->spuh_doc_id)->get();
        $docsIds = $docs->map(function ($item, $key) {
            return $item->scheduled_id;
        })->all();   

        $users = MasterUsers::whereNotNull('petugas_uttp_id')
            ->whereHas('PetugasUttp', function($query) use ($docsIds)
            {
                $query->where('flag_unit', 'uttp')
                    ->where('is_active', true)
                    ->whereIn('id', $docsIds);
            })
            ->where('id', '<>', $user->id);
        
        $user2 = $users->first();
        if ($user2 == null) {
            $users = MasterUsers::whereNotNull('petugas_uut_id')
            ->whereHas('PetugasUut', function($query) use ($docsIds)
            {
                $query->where('flag_unit', 'snsu')
                    ->where('is_active', true)
                    ->whereIn('id', $docsIds);
            })
            ->where('id', '<>', $user->id);

            $user2 = $users->first();
        }

        $users = $users->pluck('full_name', 'id')->prepend('-- Pilih Pegawai --','');
        
        $laporan = ServiceOrderUttpInsituLaporan::where('service_order_id', $id)->first();

        $bladeView = 'serviceluaruttp.result';
        if ($serviceOrder->ServiceRequest->service_type_id == 4 || $serviceOrder->ServiceRequest->service_type_id == 5) {
            if ($serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'CTMS BBM' 
                || $serviceOrder->ServiceRequestItem->uttp->type->kelompok == 'CTMS Gas') {
                $bladeView = 'serviceluaruttp.result_ctms';

                $ttuCTMS = ServiceOrderUttpTTUCTMS::where('order_id', $id)->first();
                $ttuCTMSPerlengkapan_RLG = ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Radar Level Gauge')
                        ->first();
                $ttuCTMSPerlengkapan_CLG = ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Capacitance Level Gauge')
                        ->first();
                $ttuCTMSPerlengkapan_FLG = ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Float Level Gauge')
                        ->first();
                $ttuCTMSPerlengkapan_PMS = ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Pressure Measuring Systems')
                        ->first();
                $ttuCTMSPerlengkapan_TMS = ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Temperature Measuring Systems')
                        ->first();
                $ttuCTMSPerlengkapan_TLI = ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Trim/List Inclinometer')
                        ->first();
                $ttuCTMSPerlengkapan_UTI = ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Ullage Temperature Interface (UTI) Meter')
                        ->get();
                $ttuCTMSPerlengkapan_DT = ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Depth Tape')
                        ->get();

                $ttuCTMSTank = ServiceOrderUttpTTUCTMSTank::where('order_id', $id)
                        ->get();

                $ttuCTMSGauge_RLG = ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Radar Level Gauge')
                        ->orWhere('jenis', 'Radar Level Gauge (Main)')
                        ->get();
                $ttuCTMSGauge_RLG_Backup = ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Radar Level Gauge (Backup)')
                        ->get();
                $ttuCTMSGauge_CLG_Main = ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Capacitance Level Gauge (Main)')
                        ->get();
                $ttuCTMSGauge_CLG_Backup = ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Capacitance Level Gauge (Backup)')
                        ->get();
                $ttuCTMSGauge_FLG = ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Float Level Gauge')
                        ->get();
                $ttuCTMSGauge_PMS = ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Pressure Measuring Systems')
                        ->get();
                $ttuCTMSGauge_TMS_Main = ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Temperature Measuring Systems (Main)')
                        ->get();
                $ttuCTMSGauge_TMS_Backup = ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Temperature Measuring Systems (Backup)')
                        ->get();

                $ttuCTMSSistem = ServiceOrderUttpTTUCTMSSistem::where('order_id', $id)
                        ->get();

                return view($bladeView,compact(
                    'serviceOrder',
                    'attribute',
                    'users',
                    'user',
                    'user2',
                    'oimls',
                    'ttu',
                    'ttuItems',
                    'ttuSuhu',
                    'ttuPerlengkapans',
                    'badanHitungs',
                    'inspectionItems',
                    'sertifikat_expired_at',
                    'tipe',
                    'units',
                    'negara',
                    'laporan',
                    'ttuCTMS',
                    'ttuCTMSPerlengkapan_RLG', 
                    'ttuCTMSPerlengkapan_CLG', 'ttuCTMSPerlengkapan_FLG', 'ttuCTMSPerlengkapan_PMS',
                    'ttuCTMSPerlengkapan_TMS', 'ttuCTMSPerlengkapan_TLI',
                    'ttuCTMSPerlengkapan_UTI', 'ttuCTMSPerlengkapan_DT',
                    'ttuCTMSTank',
                    'ttuCTMSGauge_RLG', 'ttuCTMSGauge_RLG_Backup',
                    'ttuCTMSGauge_CLG_Main', 'ttuCTMSGauge_CLG_Backup',
                    'ttuCTMSGauge_FLG', 'ttuCTMSGauge_PMS',
                    'ttuCTMSGauge_TMS_Main', 'ttuCTMSGauge_TMS_Backup',
                    'ttuCTMSSistem',
                ));
            }
        }

        return view($bladeView,compact(
            'serviceOrder',
            'attribute',
            'users',
            'user',
            'user2',
            'oimls',
            'ttu',
            'ttuItems',
            'ttuSuhu',
            'ttuPerlengkapans',
            'badanHitungs',
            'inspectionItems',
            'sertifikat_expired_at',
            'tipe',
            'units',
            'negara',
            'laporan',
        ));
    }

    public function resultupload($id, Request $request)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'ServiceRequestItem.uttp.type.oiml',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);


        $rules = [];
        if ($order->path_skhp == null) {
            $rules['file_skhp'] = ['required','mimes:pdf,jpg,jpeg,png','mimetypes:application/pdf,application/octet-stream,image/jpeg,image/png'];
        } else {
            $rules['file_skhp'] = ['mimes:pdf,jpg,jpeg,png','mimetypes:application/pdf,application/octet-stream,image/jpeg,image/png'];
        }
        //$rules['test_by_2'] = ['required'];
       //$rules['hasil_uji_memenuhi'] = ['required','string','min:1'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        //dd($order);

        if ($validation->passes())
        {
            // laporan
            $laporan = ServiceOrderUttpInsituLaporan::where('service_order_id', $id)->first();
            
            if ($laporan == null) {
                ServiceOrderUttpInsituLaporan::insert([
                    'service_order_id'          => $id,
                    'service_request_id'        => $order->ServiceRequest->id,
                    'service_request_item_id'   => $order->ServiceRequestItem->id,
                    'ringkasan'                 => $request->get('ringkasan'),
                    'kendala_teknis'            => $request->get('kendala_teknis'),
                    'kendala_non_teknis'        => $request->get('kendala_non_teknis'),
                    'metode_tindakan'           => $request->get('metode_tindakan'),
                    'saran_masukan'             => $request->get('saran_masukan'),
                ]);
            } else {
                $laporan->update([
                    'ringkasan'                 => $request->get('ringkasan'),
                    'kendala_teknis'            => $request->get('kendala_teknis'),
                    'kendala_non_teknis'        => $request->get('kendala_non_teknis'),
                    'metode_tindakan'           => $request->get('metode_tindakan'),
                    'saran_masukan'             => $request->get('saran_masukan'),
                ]);
            }

            $file = $request->file('file_skhp');
            
            if ($file != null) {
                $data['file_skhp'] = $file->getClientOriginalName();

                $path = $file->store(
                    'skhp',
                    'public'
                );

                $data['path_skhp'] = $path;
            }

            //$data['hasil_uji_memenuhi'] = $request->get("hasil_uji_memenuhi") == 'memenuhi';
            $data['test_by_1'] = Auth::id();
            $data['test_by_2'] = $request->get("test_by_2") > 0 ? $request->get("test_by_2") : null;
            //$data['persyaratan_teknis_id'] = $request->get("persyaratan_teknis_id");
            $data['stat_service_order'] = 1;
            $data['stat_sertifikat'] = $request->get("stat_sertifikat");
            if ($request->get("stat_sertifikat") == 1) {
                $data['draft_submit_at'] = date('Y-m-d H:i:s');
            }

            $data['tool_capacity'] = $request->get("tool_capacity");
            $data['tool_capacity_min'] = $request->get("tool_capacity_min");
            $data['tool_capacity_unit'] = $request->get("tool_capacity_unit");

            $data['tool_brand'] = $request->get("tool_brand");
            $data['tool_model'] = $request->get("tool_model");
            $data['tool_serial_no'] = $request->get("tool_serial_no");

            $data['tool_media'] = $request->get("tool_media");

            $data['tool_media_pengukuran'] = $request->get("tool_media_pengukuran");

            $data['tool_made_in_id'] = $request->get('tool_made_in_id');
            $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();
            $data['tool_made_in'] = $negara->nama_negara;


            $data['satuan_suhu'] = $request->get("satuan_suhu");
            $data['satuan_output'] = $request->get("satuan_output");
            $data['satuan_error'] = $request->get("satuan_error");

            $data['tool_factory'] = $request->get("tool_factory");
            $data['tool_factory_address'] = $request->get("tool_factory_address");

            $data['location_lat'] = $request->get("location_lat");
            $data['location_long'] = $request->get("location_long");
            $data['location_alat'] = $request->get("location_alat");

            $data['location'] = $request->get("location");

            //$data['mulai_uji'] = date("Y-m-d", strtotime($request->get('mulai_uji')));
            //$data['selesai_uji'] = date("Y-m-d", strtotime($request->get('selesai_uji')));

            ServiceRequestUttp::where('id', $order->service_request_id)->update([
                'label_sertifikat' => $request->get('label_sertifikat'),
                'addr_sertifikat' => $request->get('addr_sertifikat'),
            ]);

            ServiceRequestUttpItem::where('id', $order->service_request_item_id)->update([
                'location_lat' => strpos($request->get('location_lat'), ',') !== false ? str_replace(',', '.', $request->get('location_lat')) : $request->get('location_lat'),
                'location_long'=> strpos($request->get('location_long'), ',') !== false ? str_replace(',', '.', $request->get('location_long')) : $request->get('location_long'),
            ]);

            if ($order->ServiceRequest->service_type_id == 4 || $order->ServiceRequest->service_type_id == 5) {
                $data['sertifikat_expired_at'] = date("Y-m-d", strtotime($request->input("sertifikat_expired_at")));
                if (date("n", strtotime($request->input("sertifikat_expired_at"))) == '12') {
                    $exp = date("Y", strtotime($request->input("sertifikat_expired_at"))) . "-11-30";
                    $data['sertifikat_expired_at'] = date("Y-m-d", strtotime($exp)); 
                }
                
                $data['tanda_pegawai_berhak'] = $request->input("tanda_pegawai_berhak");
                $data['tanda_daerah'] = $request->input("tanda_daerah");
                $data['tanda_sah'] = $request->input("tanda_sah");

                $data['hasil_uji_memenuhi'] = $request->input('hasil_uji_memenuhi') == 'sah' ? true : false;

                $data['catatan_hasil_pemeriksaan'] = $request->get("catatan_hasil_pemeriksaan");

                $data['test_by_2_sertifikat'] = $request->has('test_by_2_sertifikat');
                
                if ($order->ServiceRequest->lokasi_pengujian == 'dalam') {
                    $data['location'] = $request->input("location");
                }

                ServiceOrderUttpTTUPerlengkapan::where("order_id", $id)->delete();

                if ($request->has('perlengkapan_id')) {
                    foreach($request->get('perlengkapan_id') as $index=>$value) {
                        ServiceOrderUttpTTUPerlengkapan::create([
                            "order_id" => $id,
                            "uttp_id" => $request->input("perlengkapan_uttp_id.".$index),
                            "keterangan" => $request->input("perlangkapan_keterangan.".$index),
                            "tag" => $request->input("perlangkapan_tag.".$index),
                        ]);
                    }
                }

                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge') {
                    ServiceOrderUttpTTUInspectionBadanHitung::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('badanhitung_item_id')) {
                        foreach($request->get('badanhitung_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionBadanHitung::insert([
                                "order_id" => $id,
                                "type" => $request->input("type.".$index),
                                "serial_no" => $request->input("serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "tank_no" => $request->input("tank_no"),
                        "tag_no" => $request->input("tag_no"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "input_level" => $request->input("input_level.".$index),
                                "error_up" => $request->input("error_up.".$index),
                                "error_down" => $request->input("error_down.".$index),
                                "error_hysteresis" => $request->input("error_hysteresis.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' ||
                    $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus Media Air' ||
                    $order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Arus BBM') {
                    ServiceOrderUttpTTUInspectionBadanHitung::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('badanhitung_item_id')) {
                        foreach($request->get('badanhitung_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionBadanHitung::insert([
                                "order_id" => $id,
                                "brand" => $request->input("brand.".$index),
                                "type" => $request->input("type.".$index),
                                "serial_no" => $request->input("serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "suhu_pengujian" => $request->input("suhu_pengujian"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "meter_factor" => $request->input("meter_factor.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Kadar Air') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "commodity" => $request->input("commodity.".$index),
                                "penunjukan" => $request->input("penunjukan.".$index),
                                "error" => $request->input("error.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBM') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "tool_brand" => $request->input("tool_brand.".$index),
                                "tool_type" => $request->input("tool_type.".$index),
                                "tool_serial_no" => $request->input("tool_serial_no.".$index),
                                "debit_max" => $request->input("debit_max.".$index),
                                "tool_media" => $request->input("tool_media.".$index),
                                "nozzle_count" => $request->input("nozzle_count.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "error_bkd" => $request->input("error_bkd.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                                "repeatability_bkd" => $request->input("repeatability_bkd.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Depth Tape') {
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "rentang_ukur" => $request->input("rentang_ukur.".$index),
                                "suhu_dasar" => $request->input("suhu_dasar.".$index),
                                "panjang_sebenarnya" => $request->input("panjang_sebenarnya.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'UTI Meter') {
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "rentang_ukur" => $request->input("rentang_ukur.".$index),
                                "suhu_dasar" => $request->input("suhu_dasar.".$index),
                                "panjang_sebenarnya" => $request->input("panjang_sebenarnya.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUInspectionSuhu::where("order_id", $id)->delete();

                    if ($request->has('suhu_id')) {
                        foreach($request->get('suhu_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionSuhu::insert([
                                "order_id" => $id,
                                "penunjukan" => $request->input("penunjukan.".$index),
                                "penunjukan_standar" => $request->input("penunjukan_standar.".$index),
                                "koreksi" => $request->input("koreksi.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice') {
                    ServiceOrderUttpTTUInspectionBadanHitung::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('badanhitung_item_id')) {
                        foreach($request->get('badanhitung_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionBadanHitung::insert([
                                "order_id" => $id,
                                "type" => $request->input("type.".$index),
                                "serial_no" => $request->input("serial_no.".$index),
                            ]);
                        }
                    }

                    /*
                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "tank_no" => $request->input("tank_no"),
                        "tag_no" => $request->input("tag_no"),
                    ]);
                    */
                    
                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "suhu_pengujian" => $request->input("suhu_pengujian"),
                        "ketidakpastian" => $request->input("ketidakpastian"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "d_inner" => $request->input("d_inner.".$index),
                                "d_outer" => $request->input("d_outer.".$index),
                                "e_inner" => $request->input("e_inner.".$index),
                                "e_outer" => $request->input("e_outer.".$index),
                                "alpha" => $request->input("alpha.".$index),
                                "roundness" => $request->input("roundness.".$index),
                                "flatness" => $request->input("flatness.".$index),
                                "roughness" => $request->input("roughness.".$index),
                                "eksentrisitas" => $request->input("eksentrisitas.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "input_pct" => $request->input("input_pct.".$index),
                                "input_level" => $request->input("input_level.".$index),
                                "actual" => $request->input("actual.".$index),
                                "output_up" => $request->input("output_up.".$index),
                                "output_down" => $request->input("output_down.".$index),
                                "error_up" => $request->input("error_up.".$index),
                                "error_down" => $request->input("error_down.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'EVC') {
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "error" => $request->input("error.".$index),
                                "error_bkd" => $request->input("error_bkd.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Orifice') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "line_bore_size" => $request->input("line_bore_size"),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "line_bore_size" => $request->input("line_bore_size"),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Ultrasonik') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    
                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "kfactor" => $request->input("kfactor"),
                        "line_bore_size" => $request->input("line_bore_size"),
                        "tanggal_wet_cal" => date('Y-m-d', strtotime($request->input("tanggal_wet_cal"))),
                        "expired_wet_cal" => date('Y-m-d', strtotime($request->input("expired_wet_cal"))),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Orifice') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    
                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "kfactor" => $request->input("kfactor"),
                        "line_bore_size" => $request->input("line_bore_size"),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Tetap Silinder Tegak') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    
                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        //"jenis_atap" => $request->input("jenis_atap"),
                        "tinggi_tangki" => $request->input("tinggi_tangki"),
                        "diameter" => $request->input("diameter"),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Kapal') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "volume_bersih" => $request->input("volume_bersih"),
                        "kapal" => $request->input("kapal"),
                        "panjang" => $request->input("panjang"),
                        "lebar" => $request->input("lebar"),
                        "kedalaman" => $request->input("kedalaman"),
                        "user" => $request->input("user"),
                        "operator" => $request->input("operator"),
                        "no_compartments" => $request->input("no_compartments"),
                        "tank_vol_table_no" => $request->input("tank_vol_table_no"),
                        "tank_vol_table_date" => date('Y-m-d', strtotime($request->input("tank_vol_table_date"))),
                        "pembuat_tabel" => $request->input("pembuat_tabel")
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "tank_no" => $request->input("tank_no.".$index),
                                "sounding" => $request->input("sounding.".$index),
                                "volume" => $request->input("volume.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'CTMS BBM'
                || $order->ServiceRequestItem->uttp->type->kelompok == 'CTMS Gas') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "volume_bersih" => $request->input("volume_bersih"),
                        "kapal" => $request->input("kapal"),
                        "panjang" => $request->input("panjang"),
                        "lebar" => $request->input("lebar"),
                        "kedalaman" => $request->input("kedalaman"),
                        "user" => $request->input("user"),
                        "operator" => $request->input("operator"),
                        "no_compartments" => $request->input("no_compartments"),
                        "tank_vol_table_no" => $request->input("tank_vol_table_no"),
                        "tank_vol_table_date" => date('Y-m-d', strtotime($request->input("tank_vol_table_date"))),
                        "pembuat_tabel" => $request->input("pembuat_tabel"),
                        "catatan_kapasitas" => $request->input("catatan_kapasitas"),
                        "jumlah_tangki" => $request->input("jumlah_tangki"),
                    ]);

                    ServiceOrderUttpTTUCTMS::where('order_id', $id)->delete();
                    ServiceOrderUttpTTUCTMS::insert([
                        'order_id'                      => $id,
                        'tank_volume_table'             => $request->input("tank_volume_table") == 'tank_volume_table' ? true : false,
                        'radar_level_gauge'             => $request->input("radar_level_gauge") == 'radar_level_gauge' ? true : false,
                        'capacitance_level_gauge'       => $request->input("capacitance_level_gauge") == 'capacitance_level_gauge' ? true : false,
                        'float_level_gauge'             => $request->input("float_level_gauge") == 'float_level_gauge' ? true : false,
                        'pressure_measuring_systems'    => $request->input("pressure_measuring_systems") == 'pressure_measuring_systems' ? true : false,
                        'temperature_measuring_systems' => $request->input("temperature_measuring_systems") == 'temperature_measuring_systems' ? true : false,
                        'trim_list_inclinometer'        => $request->input("trim_list_inclinometer") == 'trim_list_inclinometer' ? true : false,
                        'uti_meter'                     => $request->input("uti_meter") == 'uti_meter' ? true : false,
                        'depth_tape'                    => $request->input("depth_tape") == 'depth_tape' ? true : false,
                        'sistem_meter_arus_kerja'       => $request->input("sistem_meter_arus_kerja") == 'sistem_meter_arus_kerja' ? true : false,
                    ]);

                    ServiceOrderUttpTTUCTMSTank::where('order_id', $id)
                        ->delete();
                    if ($request->has('jenis')) {
                        foreach($request->get('jenis') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSTank::insert([
                                "order_id" => $id,
                                "jenis" => $request->input("jenis.".$index),
                                "instansi" => $request->input("instansi.".$index),
                                "no_sertifikat" => $request->input("no_sertifikat.".$index),
                                "tanggal" => $request->has('tanggal.'.$index) && $request->input('tanggal.'.$index) != null ? date("Y-m-d", strtotime($request->input('tanggal.'.$index))) : null,
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                            ->whereNull('sistem')->where('jenis', 'Radar Level Gauge')
                            ->delete();
                    ServiceOrderUttpTTUCTMSPerlengkapan::insert([
                        'order_id'      => $id,
                        'jenis'         => 'Radar Level Gauge',
                        'no_sertifikat' => $request->input("rlg_no_sertifikat"),
                        'merek'         => $request->input("rlg_merek"),
                        'tipe'          => $request->input("rlg_tipe"),
                    ]);

                    ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Radar Level Gauge')
                        ->orWhere('jenis', 'Radar Level Gauge (Main)')
                        ->delete();
                    if ($request->has('rlg_item_id')) {
                        foreach($request->get('rlg_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSGauge::insert([
                                "order_id" => $id,
                                'jenis' => $request->has('rlg_backup_item_id') ? 'Radar Level Gauge (Main)' : 'Radar Level Gauge',
                                "tank" => $request->input("rlg_tank.".$index),
                                "serial_no" => $request->input("rlg_serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Radar Level Gauge (Backup)')
                        ->delete();
                    if ($request->has('rlg_backup_item_id')) {
                        foreach($request->get('rlg_backup_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSGauge::insert([
                                "order_id" => $id,
                                'jenis' => 'Radar Level Gauge (Backup)',
                                "tank" => $request->input("rlg_backup_tank.".$index),
                                "serial_no" => $request->input("rlg_backup_serial_no.".$index),
                            ]);
                        }
                    }
    
                    ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Capacitance Level Gauge')
                        ->delete();
                    ServiceOrderUttpTTUCTMSPerlengkapan::insert([
                        'order_id'      => $id,
                        'jenis'         => 'Capacitance Level Gauge',
                        'no_sertifikat' => $request->input("clg_no_sertifikat"),
                        'merek'         => $request->input("clg_merek"),
                        'tipe'          => $request->input("clg_tipe"),
                    ]);

                    ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Capacitance Level Gauge (Main)')
                        ->delete();
                    if ($request->has('clg_main_item_id')) {
                        foreach($request->get('clg_main_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSGauge::insert([
                                "order_id" => $id,
                                'jenis' => 'Capacitance Level Gauge (Main)',
                                "tank" => $request->input("clg_main_tank.".$index),
                                "serial_no" => $request->input("clg_main_serial_no.".$index),
                            ]);
                        }
                    }
                    ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Capacitance Level Gauge (Backup)')
                        ->delete();
                    if ($request->has('clg_backup_item_id')) {
                        foreach($request->get('clg_backup_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSGauge::insert([
                                "order_id" => $id,
                                "jenis" => 'Capacitance Level Gauge (Backup)',
                                "tank" => $request->input("clg_backup_tank.".$index),
                                "serial_no" => $request->input("clg_backup_serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                            ->whereNull('sistem')->where('jenis', 'Float Level Gauge')
                            ->delete();
                    ServiceOrderUttpTTUCTMSPerlengkapan::insert([
                        'order_id'      => $id,
                        'jenis'         => 'Float Level Gauge',
                        'no_sertifikat' => $request->input("flg_no_sertifikat"),
                        'merek'         => $request->input("flg_merek"),
                        'tipe'          => $request->input("flg_tipe"),
                    ]);

                    ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Float Level Gauge')
                        ->delete();
                    if ($request->has('flg_item_id')) {
                        foreach($request->get('flg_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSGauge::insert([
                                "order_id" => $id,
                                'jenis' => 'Float Level Gauge',
                                "tank" => $request->input("flg_tank.".$index),
                                "serial_no" => $request->input("flg_serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Pressure Measuring Systems')
                        ->delete();
                    ServiceOrderUttpTTUCTMSPerlengkapan::insert([
                        'order_id'      => $id,
                        'jenis'         => 'Pressure Measuring Systems',
                        'no_sertifikat' => $request->input("pms_no_sertifikat"),
                        'merek'         => $request->input("pms_merek"),
                        'tipe'          => $request->input("pms_tipe"),
                    ]);

                    ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Pressure Measuring Systems')
                        ->delete();
                    if ($request->has('pms_item_id')) {
                        foreach($request->get('pms_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSGauge::insert([
                                "order_id" => $id,
                                'jenis' => 'Pressure Measuring Systems',
                                "tank" => $request->input("pms_tank.".$index),
                                "serial_no" => $request->input("pms_serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Temperature Measuring Systems')
                        ->delete();
                    ServiceOrderUttpTTUCTMSPerlengkapan::insert([
                        'order_id'      => $id,
                        'jenis'         => 'Temperature Measuring Systems',
                        'no_sertifikat' => $request->input("tms_no_sertifikat"),
                        'merek'         => $request->input("tms_merek"),
                        'tipe'          => $request->input("tms_tipe"),
                    ]);
                    ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Temperature Measuring Systems (Main)')
                        ->delete();
                    if ($request->has('tms_main_item_id')) {
                        foreach($request->get('tms_main_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSGauge::insert([
                                "order_id" => $id,
                                'jenis' => 'Temperature Measuring Systems (Main)',
                                "tank" => $request->input("tms_main_tank.".$index),
                                "serial_no" => $request->input("tms_main_serial_no.".$index),
                            ]);
                        }
                    }
                    
                    ServiceOrderUttpTTUCTMSGauge::where('order_id', $id)
                        ->where('jenis', 'Temperature Measuring Systems (Backup)')
                        ->delete();
                    if ($request->has('tms_backup_item_id')) {
                        foreach($request->get('tms_backup_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSGauge::insert([
                                "order_id" => $id,
                                "jenis" => 'Temperature Measuring Systems (Backup)',
                                "tank" => $request->input("tms_backup_tank.".$index),
                                "serial_no" => $request->input("tms_backup_serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                        ->whereNull('sistem')->where('jenis', 'Trim/List Inclinometer')
                        ->delete();
                    ServiceOrderUttpTTUCTMSPerlengkapan::insert([
                        'order_id'      => $id,
                        'jenis'         => 'Trim/List Inclinometer',
                        'no_sertifikat' => $request->input("tli_no_sertifikat"),
                        'serial_no'     => $request->input("tli_serial_no"),
                        'merek'         => $request->input("tli_merek"),
                        'tipe'          => $request->input("tli_tipe"),
                    ]);


                    ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                            ->whereNull('sistem')->where('jenis', 'Ullage Temperature Interface (UTI) Meter')
                            ->delete();
                    if ($request->has('uti_item_id')) {
                        foreach($request->get('uti_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSPerlengkapan::insert([
                                'order_id'      => $id,
                                'jenis'         => 'Ullage Temperature Interface (UTI) Meter',
                                'no_sertifikat' => $request->input("uti_no_sertifikat.".$index),
                                'serial_no'     => $request->input("uti_serial_no.".$index),
                                'merek'         => $request->input("uti_merek.".$index),
                                'tipe'          => $request->input("uti_tipe.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUCTMSPerlengkapan::where('order_id', $id)
                            ->whereNull('sistem')->where('jenis', 'Depth Tape')
                            ->delete();
                    if ($request->has('dt_item_id')) {
                        foreach($request->get('dt_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSPerlengkapan::insert([
                                'order_id'      => $id,
                                'jenis'         => 'Depth Tape',
                                'no_sertifikat' => $request->input("dt_no_sertifikat.".$index),
                                'serial_no'     => $request->input("dt_serial_no.".$index),
                                'merek'         => $request->input("dt_merek.".$index),
                                'tipe'          => $request->input("dt_tipe.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUCTMSSistem::where('order_id', $id)
                        ->delete();
                    if ($request->has('sistem_item_id')) {
                        foreach($request->get('sistem_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUCTMSSistem::insert([
                                'order_id'      => $id,
                                'no_sertifikat' => $request->input("sistem_no_sertifikat.".$index),
                                'serial_no_ma'  => $request->input("sistem_serial_no_ma.".$index),
                                'serial_no_pt'  => $request->input("sistem_serial_no_pt.".$index),
                                'serial_no_tt'  => $request->input("sistem_serial_no_tt.".$index),
                                'serial_no_fc'  => $request->input("sistem_serial_no_fc.".$index),
                            ]);
                        }
                    }

                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "jaringan_listrik" => $request->input("jaringan_listrik"),
                        "konstanta" => $request->input("konstanta"),
                        "kelas" => $request->input("kelas"),
                        "kondisi_ruangan" => $request->input("kondisi_ruangan"),
                        "trapo_ukur" => $request->input("trapo_ukur"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "tegangan" => $request->input("tegangan.".$index),
                                "frekuensi" => $request->input("frekuensi.".$index),
                                "arus" => $request->input("arus.".$index),
                                "faktor_daya" => $request->input("faktor_daya.".$index),
                                "error" => $request->input("error.".$index),
                                "ketidaktetapan" => $request->input("ketidaktetapan.".$index),
                                "jenis_item" => $request->input("jenis_item.".$index),
                            ]);
                        }
                    }
                }
            } elseif ($order->ServiceRequest->service_type_id == 6 || $order->ServiceRequest->service_type_id == 7) {
                $data['daya_baca'] = $request->get("daya_baca");
                $data['kelas_massa_kendaraan'] = $request->get("kelas_massa_kendaraan");
                $data['kelas_keakurasian'] = $request->get("kelas_keakurasian");
                $data['interval_skala_verifikasi'] = $request->get("interval_skala_verifikasi");
                $data['konstanta'] = $request->get("konstanta");
                $data['kelas_single_axle_load'] = $request->get("kelas_single_axle_load");
                $data['kelas_single_group_load'] = $request->get("kelas_single_group_load");
                $data['metode_pengukuran'] = $request->get("metode_pengukuran");
                $data['sistem_jaringan'] = $request->get("sistem_jaringan");
                $data['kelas_temperatur'] = $request->get("kelas_temperatur");
                $data['rasio_q'] = $request->get("rasio_q");
                $data['diameter_nominal'] = $request->get("diameter_nominal");
                $data['kecepatan'] = $request->get("kecepatan");
                $data['volume_bersih'] = $request->get("volume_bersih");
                $data['diameter_tangki'] = $request->get("diameter_tangki");
                $data['tinggi_tangki'] = $request->get("tinggi_tangki");
                $data['jumlah_nozzle'] = $request->get("jumlah_nozzle");
                $data['jenis_pompa'] = $request->get("jenis_pompa");
                $data['meter_daya_baca'] = $request->get("meter_daya_baca");

                $data['dasar_pemeriksaan'] = $request->get("dasar_pemeriksaan");

                $data['catatan_hasil_pemeriksaan'] = $request->get("catatan_hasil_pemeriksaan");

                // $data['mulai_uji'] = date("Y-m-d", strtotime($request->get('mulai_uji')));
                // $data['selesai_uji'] = date("Y-m-d", strtotime($request->get('selesai_uji')));
            
                $inspectionItems = ServiceOrderUttpInspections::where('order_id', $id)
                    ->orderBy('id', 'asc')->get();  
            
                $memenuhi = true;
                $pemeriksaan_memenuhi = true;
                foreach ($inspectionItems as $inspectionItem) {
                    if ($request->has("is_accepted_".$inspectionItem->id)) {
                        $memenuhi = $memenuhi && !($request->get("is_accepted_".$inspectionItem->id) === "tidak");
                        if ($inspectionItem->inspectionItem->is_pemeriksaan == true) {
                            $pemeriksaan_memenuhi = $pemeriksaan_memenuhi  
                                && !($request->get("is_accepted_".$inspectionItem->id) === "tidak");
                        }

                        $val = $request->get("is_accepted_".$inspectionItem->id) === "ya" ? true : 
                            ($request->get("is_accepted_".$inspectionItem->id) === "tidak" ? false : null);
                        //print_r($val);
                        
                        ServiceOrderUttpInspections::find($inspectionItem->id)->update([
                            'is_accepted' => $val,
                        ]);
                        
                    }
                }
                $data['test_by_2_sertifikat'] = $request->has('test_by_2_sertifikat');
               
                $data['hasil_uji_memenuhi'] = $memenuhi;
                $data['hasil_pemeriksaan_memenuhi'] = $pemeriksaan_memenuhi;
                $data['has_set'] = $request->has('has_set');
                
                if ($request->has('has_set')) {
                    $data['set_memenuhi'] = $request->input('set_memenuhi') == 'memenuhi' ? true : false;
                }

                $mustInpects = DB::table('uttp_inspection_prices')
                    ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                    ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
                    ->leftJoin('service_order_uttps', 'service_order_uttps.service_request_item_inspection_id', '=', 'service_request_uttp_item_inspections.id')
                    ->where('uttp_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
                    ->where('uttp_inspection_price_types.uttp_type_id', $order->ServiceRequestItem->uttp->type_id)
                    ->where(function($query) {
                        $query->whereNull('service_order_uttps.ujitipe_completed')
                              ->orWhere('service_order_uttps.ujitipe_completed', false);
                    })
                    ->selectRaw('uttp_inspection_prices.id as price_id, service_order_uttps.id as order_id')
                    ->get();
                //dd([$mustInpects, $order->ServiceRequest->service_type_id, $order->ServiceRequestItem->uttp->type_id]);
                $notOrderedYet = $mustInpects->filter(function ($value, $key) {
                    return $value->order_id == null;
                });

                //dd([$mustInpects->toQuery(), $notOrderedYet]);
                if (count($notOrderedYet) == 0) {
                    $ordersToUpdate = $mustInpects->map(function ($item, $key) {
                        return $item->order_id;
                    })->toArray();
                    ServiceOrderUttps::whereIn('id', $ordersToUpdate)->update([
                        'ujitipe_completed' => true,
                    ]);

                    ServiceOrderUttpEvaluasiTipe::where("order_id", $id)->delete();
                    ServiceOrderUttpEvaluasiTipe::insert([
                        "order_id" => $id,
                        "kelas_keakurasian" => $request->input("kelas_keakurasian"),
                        "daya_baca" => $request->input("daya_baca"),
                        "interval_skala_verifikasi" => $request->input("interval_skala_verifikasi"),
                        "konstanta" => $request->get("konstanta"),
                        "kelas_single_axle_load" => $request->get("kelas_single_axle_load"),
                        "kelas_single_group_load" => $request->get("kelas_single_group_load"),
                        "metode_pengukuran" => $request->get("metode_pengukuran"),
                        "sistem_jaringan" => $request->get("sistem_jaringan"),
                        "meter_daya_baca" => $request->input("meter_daya_baca"),
                    ]);
                }


            }

            $order->update($data);

            /*
            $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
            $inspection->update([
                'status_sertifikat' => 1
            ]);
            */

            //$this->checkAndUpdateFinishOrder($id);

            if ($order->ServiceRequest->lokasi_pengujian == 'luar') {
                return Redirect::route('serviceluaruttp');
            }

            // DIPINDAH SAAT SUBMIT/KIRIM HASIL UJI
            if ($request->get("stat_sertifikat") ==  1) {
                $history = new HistoryUttp();
                $history->request_status_id = 13;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                $history->order_id = $order->id;
                $history->order_status_id = 1;
                $history->user_id = Auth::id();
                $history->save();
            }

            return Redirect::route('serviceluaruttp');
        } else {
            return Redirect::route('serviceluaruttp.result', $id);
        }
    }

    public function editalat($id)
    {
        $attribute = $this->MyProjects->setup("serviceluaruttp");

        $serviceOrder = ServiceOrderUttps::find($id);
        $negara = $this->MasterNegara->pluck('nama_negara', 'id');
        $units = DB::table('master_uttp_units')
                    ->where('uttp_type_id', $serviceOrder->tool_type_id)->pluck('unit', 'unit');

        return view('serviceluaruttp.editalat',compact([
            'serviceOrder','attribute',
            'negara', 'units'
        ]));
    }

    public function saveeditalat($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $rules = [
            'tool_serial_no' => 'required',
            'tool_brand' => 'required', 
            'tool_model' => 'required', 
            'tool_capacity' => 'required', 
            'tool_capacity_unit' => 'required',
            'tool_factory' => 'required', 
        ];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes()) {
            ServiceOrderUttps::where('id', $id)->update([
                'tool_serial_no' => $request->get("tool_serial_no"),
                'tool_brand' => $request->get("tool_brand"),
                'tool_model' => $request->get("tool_model"),
                'tool_media' => $request->get("tool_media"),
                'tool_capacity' => $request->get("tool_capacity"),
                'tool_capacity_min' => $request->get("tool_capacity_min"),
                'tool_capacity_unit' => $request->get("tool_capacity_unit"),
                'tool_made_in_id' => $request->get("tool_made_in_id"),
                'tool_factory' => $request->get("tool_factory"),
                'tool_factory_address' => $request->get("tool_factory_address"),
            ]);

            return Redirect::route('serviceluaruttp.result', $id);
        }
    }

    public function approve($id)
    {
        $attribute = $this->MyProjects->setup("serviceuttp");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        return view('serviceuttp.approve',compact(
            'serviceOrder', 'attribute'
        ));
    }

    public function approvesubmit($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
        ])->find($id);
        $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
        $lastNo = $serviceType->last_no;
        $nextNo = intval($lastNo)+1;
        $noSertifikat = sprintf("%d", $nextNo).'/PKTN.4.8/KHP/KN/'.date("m/Y");
        $noService = $serviceType->prefix.'-'.sprintf("%05d", $nextNo);

        ServiceOrderUttps::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            "no_service" => $noService,
            "supervisor_staff" => Auth::id(),
            "supervisor_entry_date" => date("Y-m-d"),
            "stat_service_order"=>3
        ]);

        MasterServiceType::where("id",$id)->update(["last_no"=>$nextNo]);

        $this->checkAndUpdateFinishOrder($id);
        
        return Redirect::route('serviceuttp');
    }

    public function warehouse($id)
    {
        $order = ServiceOrderUttps::find($id);
        ServiceOrderUttps::whereId($id)->update(["stat_warehouse"=>1]);

        /*
        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        $inspection->update([
            'status_uttp' => 1
        ]);
        */

        $history = new HistoryUttp();
        $history->request_status_id = 13;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = $order->stat_sertifikat;
        $history->warehouse_status_id = 1;
        $history->user_id = Auth::id();
        $history->save();

        $this->checkAndUpdateFinishOrder($id);
        
        return Redirect::route('serviceuttp');
    }

    public function download($id)
    {
        $order = ServiceOrderUttps::find($id);

        return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
    }

    public function preview($id)
    {
        $order = ServiceOrderUttps::find($id);
       
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_tipe_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_ttu_pdf';
        }

        $view = true;
        //dd($order->inspections);
        return view($blade,compact(
            'order', 'qrcode_generator', 'view'
        ));
    }

    public function previewTipe($id)
    {
        $order = ServiceOrderUttps::find($id);
       
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhpt_tipe_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_ttu_pdf';
        }

        $view = true;
        //dd($order->inspections);
        return view($blade,compact(
            'order', 'qrcode_generator', 'view'
        ));
    }

    public function print($id, $stream = 0)
    {
        $order = ServiceOrderUttps::find($id);

        // $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);
        $file_name = 'SKHP '.($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhp',
            'token' => $order->qrcode_token,
        ]);
        
        $blade = 'serviceuttp.skhp_tipe_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_ttu_pdf';
                
                
        }

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));

        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }

    public function printTipe($id, $stream = 0)
    {
        $order = ServiceOrderUttps::find($id);

        //$file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);
        $file_name = 'SKHPT '.($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'skhpt',
            'token' => $order->qrcode_skhpt_token,
        ]);
        
        $blade = 'serviceuttp.skhpt_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));
     
        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }

    public function previewSETipe($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $otherOrders = ServiceOrderUttps::whereHas(
            'ServiceRequestItem', function($query) use($order)
            {
                $query->where("uttp_id",$order->ServiceRequestItem->uttp_id);  
            })
            ->get();

        $qrcode_generator = route('documentuttp.valid', $order->qrcode_tipe_token);

        $blade = 'serviceuttp.set_tipe_pdf';

        $view = true;
        //dd($order);
        return view($blade,compact(
            'order', 'otherOrders', 'qrcode_generator', 'view'
        ));
    }

    public function printSETipe($id, $stream = 0)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $file_name = 'SET '.($order->no_surat_tipe ? $order->no_surat_tipe : $order->ServiceRequestItem->no_order);

        $otherOrders = ServiceOrderUttps::whereHas(
            'ServiceRequestItem', function($query) use($order)
            {
                $query->where("uttp_id",$order->ServiceRequestItem->uttp_id);  
            })
            ->get();
       
        $qrcode_generator = route('documentuttp.valid', [
            'jenis_sertifikat' => 'set',
            'token' => $order->qrcode_tipe_token,
        ]);

        $blade = 'serviceuttp.set_tipe_pdf';

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
            ->loadview($blade,compact(['order', 'otherOrders', 'qrcode_generator', 'view']));
       
        if ($stream == 1) {
            return $pdf->stream();
        }
        return $pdf->download($file_name . '.pdf');
    }

    private function checkAndUpdateFinishOrder($id) 
    {
        $order = ServiceOrderUttps::find($id);

        /*
        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        if ($inspection->status_sertifikat == 1 && $inspection->status_uttp == 1) {
            $inspection->update(['status_id' => 14]); 
        }

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_14 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
        }
        */
        if ($order->stat_warehouse == 1 && $order->stat_sertifikat == 3) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestUttpItemInspection::where("request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUttp();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                $history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }

    public function pending($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.pending',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmpending($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $order->update([
            "pending_status" => 1,
            "pending_created" => date('Y-m-d H:i:s'),
            "pending_notes" => $request->get("pending_notes"),
            //"pending_estimated" => date("Y-m-d", strtotime($request->get("pending_estimated"))),
        ]);

        ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>15]);

        $history = new HistoryUttp();
        $history->request_status_id = 15;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        //$history->order_id = $order->id;
        $history->user_id = Auth::id();
        $history->save();

        $customerEmail = $order->ServiceRequest->requestor->email;
        //Mail::to($customerEmail)->send(new Pending($order));
        ProcessEmailJob::dispatch($customerEmail, new Pending($svcRequest))->onQueue('emails');

        return Redirect::route('serviceuttp');
    }

    public function continue($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.continue',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcontinue($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $order->update([
            "pending_status" => 0,
            "pending_ended" => date('Y-m-d H:i:s'),
        ]);

        ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>12]);

        $history = new HistoryUttp();
        $history->request_status_id = 12;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        //$history->order_id = $order->id;
        $history->user_id = Auth::id();
        $history->save();

        return Redirect::route('serviceuttp');
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.cancel',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcancel($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $order->update([
            "cancel_at" => date('Y-m-d H:i:s'),
            "cancel_notes" => $request->get("cancel_notes"),
            "cancel_id" => Auth::id(),
            "stat_service_order" => 4,
            "stat_sertifikat" => 0,
            //"status_id" => 16,
        ]);

        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        $inspection->update(['status_id' => 16]);
        
        $history = new HistoryUttp();
        $history->request_status_id = 16;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        //$history->order_id = $order->id;
        $history->user_id = Auth::id();
        $history->save();

        return Redirect::route('serviceuttp');
    }


    public function createbookinginspection($id)
    {
        $attribute = $this->MyProjects->setup("serviceluaruttp");

        $item = ServiceRequestUttpItem::find($id);
        if ($item->serviceRequest->service_type_id == 6 || $item->serviceRequest->service_type_id == 7) {
            /*
            $others_pemeriksaan = DB::table('uttp_inspection_prices')
                ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->leftJoin('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_request_uttp_item_inspections.request_item_id')
                ->leftJoin('service_order_uttps', 'service_order_uttps.service_request_item_id', '=', 'service_request_uttp_items.id')
                ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id)
                ->where('uttp_inspection_prices.is_pemeriksaan', true)
                ->where('service_request_uttp_items.uttp_id', $item->uttp_id)
                ->where('service_request_uttp_items.id', '<>', $item->id)
                ->get();
            */

            //dd([$item->serviceRequest->service_type_id, $item->uttp->type_id, $item->uttp_id, $item->id, $others_pemeriksaan]);

            //$ada_pemeriksaan = count($others_pemeriksaan) > 0;

            $prices = DB::table('uttp_inspection_prices')
                ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id)
                ->select('uttp_inspection_prices.*');

            /*
            if ($ada_pemeriksaan) {
                $prices = $prices->where(function($query) {
                    $query->where('uttp_inspection_prices.is_pemeriksaan', false)
                          ->orWhereNull('uttp_inspection_prices.is_pemeriksaan');
                });
            } else {
                $prices = $prices->where('uttp_inspection_prices.is_pemeriksaan', true);
            }
            */

            $prices = $prices->get();
            //dd([$item->serviceRequest->service_type_id, $item->uttp->type_id, $prices]);
            
            //dd([$others_pemeriksaan, $ada_pemeriksaan, $item->serviceRequest->service_type_id, $item->uttp->type_id, $prices]);
            //dd($prices);
        } else {
            $prices = DB::table('uttp_inspection_prices')
                ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id) 
                ->select('uttp_inspection_prices.*')
                ->get();
        }

        return view('serviceluaruttp.create_booking_inspection', compact('item', 'prices', 'attribute'));
    }

    public function simpanbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $order = ServiceOrderUttps::where('service_request_item_id', $id)->first();
        
        ServiceRequestUttpItemInspection::where('request_item_id', $id)->delete();
        
        foreach($request->get('quantity') as $index=>$value) {
            if ($value != null) {

                $dataInspection = new ServiceRequestUttpItemInspection();
                $dataInspection->request_item_id = $id;
                $dataInspection->quantity = $value;
                $dataInspection->price = $request->get('price')[$index];
                $dataInspection->inspection_price_id = $request->get('id')[$index];

                $dataInspection->status_id = 1;
                $dataInspection->save();
            }

        }

        $item = ServiceRequestUttpItem::find($id);
        
        $itemAggregate = ServiceRequestUttpItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $id)
            ->first();

        ServiceRequestUttpItem::where('id', $id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = ServiceRequestUttpItem::selectRaw('count(service_request_uttp_item_inspections.id) quantity, coalesce(sum(service_request_uttp_item_inspections.price * service_request_uttp_item_inspections.quantity), 0) subtotal')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        $svcRequest = ServiceRequestUttp::selectRaw("service_request_uttps.id, service_request_uttps.received_date, service_request_uttps.received_date 
                + max((case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end)) * interval '1 day' max_estimated_date")
            ->join('service_request_uttp_items', 'service_request_uttp_items.request_id', '=', 'service_request_uttps.id')
            //->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            //->join('uttp_inspection_prices', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            //->join('master_instalasi', 'uttp_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->join('uttps', 'uttps.id', '=', 'service_request_uttp_items.uttp_id')
            ->join('master_uttp_types', 'master_uttp_types.id', '=', 'uttps.type_id')
            ->join('master_instalasi', 'master_uttp_types.instalasi_id', '=', 'master_instalasi.id')
            ->groupBy('service_request_uttps.id', 'service_request_uttps.received_date')
            ->where('service_request_uttps.id', $item->serviceRequest->id)
            ->first();

        $itemCount = ServiceRequestUttpItem::where("request_id", $item->serviceRequest->id)->count();
        $request = ServiceRequestUttp::find($item->serviceRequest->id);
        $no_order = $request->no_order;
        $parts = explode("-",$no_order);
        $parts[3] = sprintf("%03d",intval($itemCount));
        $no_order = implode("-", $parts);

        ServiceRequestUttp::where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal,
            'estimated_date' => $svcRequest ? $svcRequest->max_estimated_date : null,
            'no_order' => $no_order,
        ]);



        return Redirect::route('serviceluaruttp.result', $order->id);
    }

    public function perlengkapan($id) {
        $attribute = $this->MyProjects->setup("serviceluaruttps");

        $order = ServiceOrderUttps::find($id);

        $item = ServiceRequestUttpItem::find($order->service_request_item_id);

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($item->serviceRequest->id);

        $perlengkapans = ServiceRequestUttpItemTTUPerlengkapan::where('request_item_id', $order->service_request_item_id)->get();

        return view('serviceluaruttp.perlengkapan', 
            compact('order', 'request', 'item', 'perlengkapans', 'attribute'));
    }

    function newperlengkapan($id)
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $order = ServiceOrderUttps::find($id);

        $item = ServiceRequestUttpItem::find($order->service_request_item_id);

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($item->serviceRequest->id);

        $uttp_types = MasterUttpType::orderBy('id')->get();
        $uttp_type_id = $uttp_types[0]->id;
        $uttp_types = $uttp_types->pluck('uttp_type', 'id');
        

        $units = DB::table('master_uttp_units')
                ->where('uttp_type_id', $uttp_type_id)
                ->pluck('unit', 'unit');

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        return view('serviceluaruttp.create_perlengkapan', 
            compact('order', 'request', 'item', 'units',  'uttp_types', 'negara', 'attribute'));
    }

    public function simpanperlengkapan($id, Request $request) 
    {
        $order = ServiceOrderUttps::find($id);

        $item = ServiceRequestUttpItem::find($order->service_request_item_id);

        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
        ->find($item->serviceRequest->id);
        
        $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();

        $uttp = Uttp::create([
            'owner_id' => $req->uttp_owner_id,
            'type_id' => $request->get('uttp_type_id'),
            'serial_no' => $request->get('tool_serial_no'),
            'tool_brand' => $request->get('tool_brand'),
            'tool_model' => $request->get('tool_model'),
            'tool_capacity' => $request->get('tool_capacity'),
            'tool_factory' => $request->get('tool_factory'),
            'tool_factory_address' => $request->get('tool_factory_address'),
            'tool_made_in' => $negara->nama_negara,
            'tool_made_in_id' => $request->get('tool_made_in_id'),     
            'tool_capacity_unit' => $request->get('tool_capacity_unit'),
            'tool_media' => $request->get('tool_media'),
            'tool_capacity_min' => $request->get('tool_capacity_min'),
        ]);

        //$item = ServiceRequestUttpItem::find($request->get('item_id'));

        $dataPerlengkapan["request_item_id"] =  $item->id;
        $dataPerlengkapan["uttp_id"] = $uttp->id;

        $perlengkapan = ServiceRequestUttpItemTTUPerlengkapan::insert($dataPerlengkapan);

        if ($req->service_type_id == 4 || $req->service_type_id == 5) {
            $ttuPerlengkapanModel = new ServiceOrderUttpTTUPerlengkapan();
            $ttuPerlengkapan = [
                'order_id' => $order->id,
                'uttp_id' => $uttp->id
            ];
            $ttuPerlengkapanModel->create($ttuPerlengkapan);
        }

        return Redirect::route('serviceluaruttp.perlengkapan', $id);
    }

    public function editperlengkapan($id, $idItem, $idPerlengkapan) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $order = ServiceOrderUttps::find($id);

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($order->service_request_id);

        $perlengkapan = ServiceRequestUttpItemTTUPerlengkapan::find($idPerlengkapan);

        $item = ServiceRequestUttpItem::find($idItem);

        $units = DB::table('master_uttp_units')
                    ->where('uttp_type_id', $perlengkapan->uttp->type_id)
                    ->pluck('unit', 'unit')
                    ->prepend($perlengkapan->uttp->tool_capacity_unit, $perlengkapan->uttp->tool_capacity_unit);

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        return view('serviceluaruttp.edit_perlengkapan', 
            compact('order', 'request', 'item', 'perlengkapan', 'units', 'negara', 'attribute'));
    }

    public function simpaneditperlengkapan($id, $idItem, $idPerlengkapan, Request $request) 
    {
        $attribute = $this->MyProjects->setup("serviceprocessluaruttps");

        $order = ServiceOrderUttps::find($id);

        $req = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($order->service_request_id);

        $item = ServiceRequestUttpItem::find($idItem);

        $perlengkapan = ServiceRequestUttpItemTTUPerlengkapan::find($idPerlengkapan);

        $uttp = Uttp::find($perlengkapan->uttp_id);

        $data['tool_capacity'] = $request->get("tool_capacity");
        $data['tool_capacity_min'] = $request->get("tool_capacity_min");
        $data['tool_capacity_unit'] = $request->get("tool_capacity_unit");

        $data['tool_brand'] = $request->get("tool_brand");
        $data['tool_model'] = $request->get("tool_model");
        $data['serial_no'] = $request->get("tool_serial_no");

        $data['tool_media'] = $request->get("tool_media");

        $data['tool_made_in_id'] = $request->get('tool_made_in_id');
        $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();
        $data['tool_made_in'] = $negara->nama_negara;

        $data['tool_factory'] = $request->get("tool_factory");
        $data['tool_factory_address'] = $request->get("tool_factory_address");

        $uttp->update($data);

        return Redirect::route('serviceluaruttp.perlengkapan', $id);
    }

    public function hapusperlengkapan(Request $request) 
    {
        $perlengkapan = ServiceRequestUttpItemTTUPerlengkapan::find($request->get('id'));
        $order_id = $request->get('order_id');

        // Uttp::find($perlengkapan->uttp_id)->delete();
        // $request_item_id = $perlengkapan->request_item_id;
        // $perlengkapan->delete();

        $perlengkapans = ServiceRequestUttpItemTTUPerlengkapan::where('request_item_id', $perlengkapan->request_item_id)->get();

        //ServiceRequestUttpItemTTUPerlengkapan::where('request_item_id', $perlengkapan->request_item_id)->delete();
        ServiceOrderUttpTTUPerlengkapan::where('uttp_id', $perlengkapan->uttp_id)->delete();

        $perlengkapan->delete();

        return Redirect::route('serviceluaruttp.perlengkapan', $order_id); 
    }
}
