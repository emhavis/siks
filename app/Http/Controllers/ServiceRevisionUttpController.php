<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrderUttpTTUInspection;
use App\ServiceOrderUttpTTUInspectionItems;
use App\ServiceOrderUttpTTUInspectionSuhu;
use App\ServiceOrderUttpTTUInspectionBadanHitung;
use App\ServiceOrderUttpTTUPerlengkapan;
use App\ServiceOrderUttpEvaluasiTipe;

use App\Mail\Pending;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;

use App\MasterInstalasi;
use App\HistoryUttp;
use App\ServiceOrders;

class ServiceRevisionUttpController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("servicerevisionuttp");

        $laboratory_id = Auth::user()->laboratory_id;
        //$instalasi_id = Auth::user()->instalasi_id;

        $instalasiList = MasterInstalasi::where('lab_id', $laboratory_id)->orderBy('id','asc')
            ->get();

        foreach($instalasiList as $instalasi) {
           $rows[$instalasi->id] = ServiceOrderUttps::whereIn("stat_service_order",[0,1,2,3,4])
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->whereIn("status_id", [13,16]);
            })
            ->whereHas('ServiceRequest', function($query) 
            {
                $query->where("lokasi_pengujian","=", 'dalam');
            })
            ->where("is_finish",0)
            ->where(function($query) {
                $query->where('stat_sertifikat', 0)
                      ->orWhereNull('stat_sertifikat');
            })
            ->whereNotNull('subkoordinator_notes')
            //->where('lab_staff', Auth::id())
            /*
            ->where(function($query) {
                $query->whereNull('test_by_1')
                      ->orWhere('test_by_1', Auth::id())
                      ->orWhere('test_by_2', Auth::id());
            })
            */
            ->where(function($query) {
                $query->where(function($q1) {
                    $q1->whereNull('test_by_1')
                        ->orWhere('test_by_1', Auth::id())
                        ->orWhere('test_by_2', Auth::id());
                    })
                    ->orWhere(function($q2) {
                        $q2->whereNotNull('cancel_at')
                            ->where('cancel_id', Auth::id());
                    });
            })
            ->where('laboratory_id', $laboratory_id)
            ->where('instalasi_id', $instalasi->id)
            ->orderBy('staff_entry_datein','desc')
            ->get();
        }
        //dd([Auth::id(), $rows]);

        return view('servicerevisionuttp.index',compact('rows','attribute','instalasiList'));
    }

    public function result($id)
    {
        $attribute = $this->MyProjects->setup("servicerevisionuttp");

        $serviceOrder = ServiceOrderUttps::find($id);

        // $users = MasterUsers::pluck('full_name', 'id');
        // $users = MasterUsers::pluck('full_name', 'id')->prepend('-- Pilih Pegawai --','');
        $users = MasterUsers::whereNotNull('petugas_uttp_id')
            ->whereHas('PetugasUttp', function($query) 
            {
                $query->where('flag_unit', 'uttp')
                    ->where('is_active', true);
            })
            ->pluck('full_name', 'id')->prepend('-- Pilih Pegawai --','');
        
        $user = MasterUsers::find(Auth::id());

        $oimls = DB::table("master_oimls")->pluck('oiml_name', 'id');
        $inspectionItems = ServiceOrderUttpInspections::where('order_id', $id)
        ->orderBy('order_no', 'asc')->orderBy('id', 'asc')->get();
            
        $sertifikat_expired_at = date('Y-m-d', 
            strtotime( $serviceOrder->staff_entry_dateout . ' + '. 
            ($serviceOrder->ServiceRequestItem->uttp->type->masa_berlaku_ttu == null ?  0 : 
            $serviceOrder->ServiceRequestItem->uttp->type->masa_berlaku_ttu) .
            ' year'));

        $ttu = ServiceOrderUttpTTUInspection::where('order_id', $id)->first();
        $ttuItems = ServiceOrderUttpTTUInspectionItems::where('order_id', $id)->get();
        $ttuSuhu = ServiceOrderUttpTTUInspectionSuhu::where('order_id', $id)->get();
        $badanHitungs = ServiceOrderUttpTTUInspectionBadanHitung::where('order_id', $id)->get();
        $ttuPerlengkapans = ServiceOrderUttpTTUPerlengkapan::where('order_id', $id)->get();
        $tipe = ServiceOrderUttpEvaluasiTipe::where('order_id', $id)->first();

        $units = DB::table('master_uttp_units')
                    ->where('uttp_type_id', $serviceOrder->tool_type_id)
                    ->pluck('unit', 'unit')
                    ->prepend($serviceOrder->tool_capacity_unit, $serviceOrder->tool_capacity_unit);
        $units->prepend(' ', ' ');

        $negara = DB::table('master_negara')->pluck('nama_negara', 'id');

        return view('servicerevisionuttp.result',compact(
            'serviceOrder',
            'attribute',
            'users',
            'user',
            'oimls',
            'ttu',
            'ttuItems',
            'ttuSuhu',
            'ttuPerlengkapans',
            'badanHitungs',
            'inspectionItems',
            'sertifikat_expired_at',
            'tipe',
            'units',
            'negara',
        ));
    }

    public function resultupload($id, Request $request)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'ServiceRequestItem.uttp.type.oiml',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);


        $rules = [];
        if ($order->path_skhp == null) {
            $rules['file_skhp'] = ['required','mimes:pdf,jpg,jpeg,png','mimetypes:application/pdf,application/octet-stream,image/jpeg,image/png'];
        } else {
            $rules['file_skhp'] = ['mimes:pdf,jpg,jpeg,png','mimetypes:application/pdf,application/octet-stream,image/jpeg,image/png'];
        }
        //$rules['test_by_2'] = ['required'];
        //$rules['hasil_uji_memenuhi'] = ['required'];
        
        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        //dd($order);

        if ($validation->passes())
        {
            $file = $request->file('file_skhp');
            
            if ($file != null) {
                $data['file_skhp'] = $file->getClientOriginalName();

                $path = $file->store(
                    'skhp',
                    'public'
                );

                $data['path_skhp'] = $path;
            }

            //$data['hasil_uji_memenuhi'] = $request->get("hasil_uji_memenuhi") == 'memenuhi';
            $data['test_by_1'] = Auth::id();
            $data['test_by_2'] = $request->get("test_by_2") > 0 ? $request->get("test_by_2") : null;
            //$data['persyaratan_teknis_id'] = $request->get("persyaratan_teknis_id");
            $data['stat_service_order'] = 2;
            $data['stat_sertifikat'] = $request->get("stat_sertifikat");

            $data['tool_capacity'] = $request->get("tool_capacity");
            $data['tool_capacity_min'] = $request->get("tool_capacity_min");
            $data['tool_capacity_unit'] = $request->get("tool_capacity_unit");

            $data['tool_brand'] = $request->get("tool_brand");
            $data['tool_model'] = $request->get("tool_model");
            $data['tool_serial_no'] = $request->get("tool_serial_no");

            $data['tool_media'] = $request->get("tool_media");

            $data['tool_media_pengukuran'] = $request->get("tool_media_pengukuran");

            $data['tool_made_in_id'] = $request->get('tool_made_in_id');
            $negara = DB::table('master_negara')->where('id', $request->get('tool_made_in_id'))->first();
            $data['tool_made_in'] = $negara->nama_negara;


            $data['satuan_suhu'] = $request->get("satuan_suhu");
            $data['satuan_output'] = $request->get("satuan_output");
            $data['satuan_error'] = $request->get("satuan_error");
            
            $data['tool_factory'] = $request->get("tool_factory");
            $data['tool_factory_address'] = $request->get("tool_factory_address");
            

            ServiceRequestUttp::where('id', $order->service_request_id)->update([
                'label_sertifikat' => $request->get('label_sertifikat'),
                'addr_sertifikat' => $request->get('addr_sertifikat'),
            ]);

            if ($order->ServiceRequest->service_type_id == 4 || $order->ServiceRequest->service_type_id == 5) {
                
                if ($request->has("sertifikat_expired_at") && $request->input("sertifikat_expired_at") != null) {
                    $data['sertifikat_expired_at'] = date("Y-m-d", strtotime($request->input("sertifikat_expired_at")));
                } else {
                    $sertifikat_expired_at = date('Y-m-d', 
                        strtotime( $order->staff_entry_dateout . ' + '. 
                        ($order->ServiceRequestItem->uttp->type->masa_berlaku_ttu == null ?  0 : 
                        $order->ServiceRequestItem->uttp->type->masa_berlaku_ttu) .
                        ' year'));
                    $data['sertifikat_expired_at'] = $sertifikat_expired_at;
                }
                if (date("n", strtotime($request->input("sertifikat_expired_at"))) == '12') {
                    $exp = date("Y", strtotime($request->input("sertifikat_expired_at"))) . "-11-30";
                    $data['sertifikat_expired_at'] = date("Y-m-d", strtotime($exp)); 
                }

                $data['tanda_pegawai_berhak'] = $request->input("tanda_pegawai_berhak");
                $data['tanda_daerah'] = $request->input("tanda_daerah");
                $data['tanda_sah'] = $request->input("tanda_sah");

                $data['hasil_uji_memenuhi'] = $request->input('hasil_uji_memenuhi') == 'sah' ? true : false;

                $data['catatan_hasil_pemeriksaan'] = $request->get("catatan_hasil_pemeriksaan");

                if ($order->ServiceRequest->lokasi_pengujian == 'dalam') {
                    $data['location'] = $request->input("location");
                }

                ServiceOrderUttpTTUPerlengkapan::where("order_id", $id)->delete();

                if ($request->has('perlengkapan_id')) {
                    foreach($request->get('perlengkapan_id') as $index=>$value) {
                        ServiceOrderUttpTTUPerlengkapan::create([
                            "order_id" => $id,
                            "uttp_id" => $request->input("perlengkapan_uttp_id.".$index),
                            "keterangan" => $request->input("perlangkapan_keterangan.".$index),
                        ]);
                    }
                }

                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Automatic Level Gauge') {
                    ServiceOrderUttpTTUInspectionBadanHitung::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('badanhitung_item_id')) {
                        foreach($request->get('badanhitung_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionBadanHitung::insert([
                                "order_id" => $id,
                                "type" => $request->input("type.".$index),
                                "serial_no" => $request->input("serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "tank_no" => $request->input("tank_no"),
                        "tag_no" => $request->input("tag_no"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "input_level" => $request->input("input_level.".$index),
                                "error_up" => $request->input("error_up.".$index),
                                "error_down" => $request->input("error_down.".$index),
                                "error_hysteresis" => $request->input("error_hysteresis.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus BBM' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Meter Arus Media Air') {
                    ServiceOrderUttpTTUInspectionBadanHitung::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('badanhitung_item_id')) {
                        foreach($request->get('badanhitung_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionBadanHitung::insert([
                                "order_id" => $id,
                                "brand" => $request->input("brand.".$index),
                                "type" => $request->input("type.".$index),
                                "serial_no" => $request->input("serial_no.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "suhu_pengujian" => $request->input("suhu_pengujian"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "meter_factor" => $request->input("meter_factor.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBG') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Air') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Kadar Air') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "commodity" => $request->input("commodity.".$index),
                                "penunjukan" => $request->input("penunjukan.".$index),
                                "error" => $request->input("error.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Pompa Ukur BBM') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "tool_brand" => $request->input("tool_brand.".$index),
                                "tool_type" => $request->input("tool_type.".$index),
                                "tool_serial_no" => $request->input("tool_serial_no.".$index),
                                "debit_max" => $request->input("debit_max.".$index),
                                "tool_media" => $request->input("tool_media.".$index),
                                "nozzle_count" => $request->input("nozzle_count.".$index),

                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter Gas (USM WetCal)') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "flow_rate" => $request->input("flow_rate.".$index),
                                "error" => $request->input("error.".$index),
                                "error_bkd" => $request->input("error_bkd.".$index),
                                "repeatability" => $request->input("repeatability.".$index),
                                "repeatability_bkd" => $request->input("repeatability_bkd.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Depth Tape') {
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "rentang_ukur" => $request->input("rentang_ukur.".$index),
                                "suhu_dasar" => $request->input("suhu_dasar.".$index),
                                "panjang_sebenarnya" => $request->input("panjang_sebenarnya.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'UTI Meter') {
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "rentang_ukur" => $request->input("rentang_ukur.".$index),
                                "suhu_dasar" => $request->input("suhu_dasar.".$index),
                                "panjang_sebenarnya" => $request->input("panjang_sebenarnya.".$index),
                            ]);
                        }
                    }

                    ServiceOrderUttpTTUInspectionSuhu::where("order_id", $id)->delete();

                    if ($request->has('suhu_id')) {
                        foreach($request->get('suhu_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionSuhu::insert([
                                "order_id" => $id,
                                "penunjukan" => $request->input("penunjukan.".$index),
                                "penunjukan_standar" => $request->input("penunjukan_standar.".$index),
                                "koreksi" => $request->input("koreksi.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Pelat Orifice') {
                    ServiceOrderUttpTTUInspectionBadanHitung::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('badanhitung_item_id')) {
                        foreach($request->get('badanhitung_item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionBadanHitung::insert([
                                "order_id" => $id,
                                "type" => $request->input("type.".$index),
                                "serial_no" => $request->input("serial_no.".$index),
                            ]);
                        }
                    }

                    /*
                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "tank_no" => $request->input("tank_no"),
                        "tag_no" => $request->input("tag_no"),
                    ]);
                    */

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "suhu_pengujian" => $request->input("suhu_pengujian"),
                        "ketidakpastian" => $request->input("ketidakpastian"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "d_inner" => $request->input("d_inner.".$index),
                                "d_outer" => $request->input("d_outer.".$index),
                                "e_inner" => $request->input("e_inner.".$index),
                                "e_outer" => $request->input("e_outer.".$index),
                                "alpha" => $request->input("alpha.".$index),
                                "roundness" => $request->input("roundness.".$index),
                                "flatness" => $request->input("flatness.".$index),
                                "roughness" => $request->input("roughness.".$index),
                                "eksentrisitas" => $request->input("eksentrisitas.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Pressure Transmitter' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Differential Pressure Transmitter' ||
                $order->ServiceRequestItem->uttp->type->kelompok == 'Temperature Transmitter') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "totalisator" => $request->input("totalisator"),
                        "kfactor" => $request->input("kfactor"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "input_pct" => $request->input("input_pct.".$index),
                                "input_level" => $request->input("input_level.".$index),
                                "actual" => $request->input("actual.".$index),
                                "output_up" => $request->input("output_up.".$index),
                                "output_down" => $request->input("output_down.".$index),
                                "error_up" => $request->input("error_up.".$index),
                                "error_down" => $request->input("error_down.".$index),
                            ]);
                        }
                    }
                        
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'EVC') {
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "error" => $request->input("error.".$index),
                                "error_bkd" => $request->input("error_bkd.".$index),
                            ]);
                        }
                    }
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter Gas Orifice') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "line_bore_size" => $request->input("line_bore_size"),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Sistem Meter') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "line_bore_size" => $request->input("line_bore_size"),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Tangki Ukur Tetap Silinder Tegak') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    
                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        //"jenis_atap" => $request->input("jenis_atap"),
                        "tinggi_tangki" => $request->input("tinggi_tangki"),
                        "diameter" => $request->input("diameter"),
                    ]);
                }
                if ($order->ServiceRequestItem->uttp->type->kelompok == 'Meter kWh') {
                    ServiceOrderUttpTTUInspection::where("order_id", $id)->delete();
                    ServiceOrderUttpTTUInspectionItems::where("order_id", $id)->delete();

                    ServiceOrderUttpTTUInspection::insert([
                        "order_id" => $id,
                        "jaringan_listrik" => $request->input("jaringan_listrik"),
                        "konstanta" => $request->input("konstanta"),
                        "kelas" => $request->input("kelas"),
                        "kondisi_ruangan" => $request->input("kondisi_ruangan"),
                        "trapo_ukur" => $request->input("trapo_ukur"),
                    ]);

                    if ($request->has('item_id')) {
                        foreach($request->get('item_id') as $index=>$value) {
                            ServiceOrderUttpTTUInspectionItems::insert([
                                "order_id" => $id,
                                "tegangan" => $request->input("tegangan.".$index),
                                "frekuensi" => $request->input("frekuensi.".$index),
                                "arus" => $request->input("arus.".$index),
                                "faktor_daya" => $request->input("faktor_daya.".$index),
                                "error" => $request->input("error.".$index),
                                "ketidaktetapan" => $request->input("ketidaktetapan.".$index),
                                "jenis_item" => $request->input("jenis_item.".$index),
                            ]);
                        }
                    }
                }
            } elseif ($order->ServiceRequest->service_type_id == 6 || $order->ServiceRequest->service_type_id == 7) {
                $data['daya_baca'] = $request->get("daya_baca");
                $data['kelas_massa_kendaraan'] = $request->get("kelas_massa_kendaraan");
                $data['kelas_keakurasian'] = $request->get("kelas_keakurasian");
                $data['interval_skala_verifikasi'] = $request->get("interval_skala_verifikasi");
                $data['konstanta'] = $request->get("konstanta");
                $data['kelas_single_axle_load'] = $request->get("kelas_single_axle_load");
                $data['kelas_single_group_load'] = $request->get("kelas_single_group_load");
                $data['metode_pengukuran'] = $request->get("metode_pengukuran");
                $data['sistem_jaringan'] = $request->get("sistem_jaringan");
                $data['kelas_temperatur'] = $request->get("kelas_temperatur");
                $data['rasio_q'] = $request->get("rasio_q");
                $data['diameter_nominal'] = $request->get("diameter_nominal");
                $data['kecepatan'] = $request->get("kecepatan");
                $data['volume_bersih'] = $request->get("volume_bersih");
                $data['diameter_tangki'] = $request->get("diameter_tangki");
                $data['tinggi_tangki'] = $request->get("tinggi_tangki");
                $data['jumlah_nozzle'] = $request->get("jumlah_nozzle");
                $data['jenis_pompa'] = $request->get("jenis_pompa");
                $data['meter_daya_baca'] = $request->get("meter_daya_baca");

                $data['dasar_pemeriksaan'] = $request->get("dasar_pemeriksaan");

                $data['catatan_hasil_pemeriksaan'] = $request->get("catatan_hasil_pemeriksaan");
            
                $data['mulai_uji'] = date("Y-m-d", strtotime($request->get('mulai_uji')));
            
                $inspectionItems = ServiceOrderUttpInspections::where('order_id', $id)
                    ->orderBy('id', 'asc')->get();  
            
                $memenuhi = true;
                $pemeriksaan_memenuhi = true;
                foreach ($inspectionItems as $inspectionItem) {
                    if ($request->has("is_accepted_".$inspectionItem->id)) {
                        $memenuhi = $memenuhi && !($request->get("is_accepted_".$inspectionItem->id) === "tidak");
                        if ($inspectionItem->inspectionItem->is_pemeriksaan == true) {
                            $pemeriksaan_memenuhi = $pemeriksaan_memenuhi  
                                && !($request->get("is_accepted_".$inspectionItem->id) === "tidak");
                        }

                        $val = $request->get("is_accepted_".$inspectionItem->id) === "ya" ? true : 
                            ($request->get("is_accepted_".$inspectionItem->id) === "tidak" ? false : null);
                        //print_r($val);
                        
                        ServiceOrderUttpInspections::find($inspectionItem->id)->update([
                            'is_accepted' => $val,
                        ]);
                        
                    }
                }
                $data['hasil_uji_memenuhi'] = $memenuhi;
                $data['hasil_pemeriksaan_memenuhi'] = $pemeriksaan_memenuhi;
                $data['has_set'] = $request->has('has_set');

                if ($request->has('has_set')) {
                    $data['set_memenuhi'] = $request->input('set_memenuhi') == 'memenuhi' ? true : false;
                }

                $mustInpects = DB::table('uttp_inspection_prices')
                    ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                    ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
                    ->leftJoin('service_order_uttps', 'service_order_uttps.service_request_item_inspection_id', '=', 'service_request_uttp_item_inspections.id')
                    ->where('uttp_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
                    ->where('uttp_inspection_price_types.uttp_type_id', $order->ServiceRequestItem->uttp->type_id)
                    ->where(function($query) {
                        $query->whereNull('service_order_uttps.ujitipe_completed')
                              ->orWhere('service_order_uttps.ujitipe_completed', false);
                    })
                    ->selectRaw('uttp_inspection_prices.id as price_id, service_order_uttps.id as order_id')
                    ->get();
                //dd([$mustInpects, $order->ServiceRequest->service_type_id, $order->ServiceRequestItem->uttp->type_id]);
                $notOrderedYet = $mustInpects->filter(function ($value, $key) {
                    return $value->order_id == null;
                });

                //dd([$mustInpects->toQuery(), $notOrderedYet]);
                if (count($notOrderedYet) == 0) {
                    $ordersToUpdate = $mustInpects->map(function ($item, $key) {
                        return $item->order_id;
                    })->toArray();
                    ServiceOrderUttps::whereIn('id', $ordersToUpdate)->update([
                        'ujitipe_completed' => true,
                    ]);

                    ServiceOrderUttpEvaluasiTipe::where("order_id", $id)->delete();
                    ServiceOrderUttpEvaluasiTipe::insert([
                        "order_id" => $id,
                        "kelas_keakurasian" => $request->input("kelas_keakurasian"),
                        "daya_baca" => $request->input("daya_baca"),
                        "interval_skala_verifikasi" => $request->input("interval_skala_verifikasi"),
                        "konstanta" => $request->get("konstanta"),
                        "kelas_single_axle_load" => $request->get("kelas_single_axle_load"),
                        "kelas_single_group_load" => $request->get("kelas_single_group_load"),
                        "metode_pengukuran" => $request->get("metode_pengukuran"),
                        "sistem_jaringan" => $request->get("sistem_jaringan"),
                        "meter_daya_baca" => $request->input("meter_daya_baca"),
                    ]);
                }


            }

            $order->update($data);

            /*
            $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
            $inspection->update([
                'status_sertifikat' => 1
            ]);
            */

            //$this->checkAndUpdateFinishOrder($id);

            /*
            if ($order->ServiceRequest->lokasi_pengujian == 'luar') {
                return Redirect::route('servicerevisionluaruttp');
            }
            */
            return Redirect::route('servicerevisionuttp');
        } else {
            return Redirect::route('servicerevisionuttp.result', $id);
        }
    }

    public function approve($id)
    {
        $attribute = $this->MyProjects->setup("serviceuttp");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        return view('serviceuttp.approve',compact(
            'serviceOrder', 'attribute'
        ));
    }

    public function approvesubmit($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
        ])->find($id);
        $serviceType = MasterServiceType::find($order->ServiceRequest->service_type_id);
        $lastNo = $serviceType->last_no;
        $nextNo = intval($lastNo)+1;
        $noSertifikat = sprintf("%d", $nextNo).'/PKTN.4.8/KHP/KN/'.date("m/Y");
        $noService = $serviceType->prefix.'-'.sprintf("%05d", $nextNo);

        ServiceOrderUttps::whereId($id)->update([
            "no_sertifikat" => $noSertifikat,
            "no_service" => $noService,
            "supervisor_staff" => Auth::id(),
            "supervisor_entry_date" => date("Y-m-d"),
            "stat_service_order"=>3
        ]);

        MasterServiceType::where("id",$id)->update(["last_no"=>$nextNo]);

        $this->checkAndUpdateFinishOrder($id);
        
        return Redirect::route('serviceuttp');
    }

    public function warehouse($id)
    {
        $order = ServiceOrderUttps::find($id);
        ServiceOrderUttps::whereId($id)->update(["stat_warehouse"=>1]);

        /*
        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        $inspection->update([
            'status_uttp' => 1
        ]);
        */

        $history = new HistoryUttp();
        $history->request_status_id = 13;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = $order->stat_sertifikat;
        $history->warehouse_status_id = 1;
        $history->user_id = Auth::id();
        $history->save();

        $this->checkAndUpdateFinishOrder($id);
        
        return Redirect::route('serviceuttp');
    }

    public function download($id)
    {
        $order = ServiceOrderUttps::find($id);

        return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
    }

    public function preview($id)
    {
        $order = ServiceOrderUttps::find($id);
        /*
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);
        */
        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_pdf_ttu';
        }

        $view = true;
        //dd($order->inspections);
        return view($blade,compact(
            'order', 'qrcode_generator', 'view'
        ));
    }

    public function print($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $file_name = 'skhp'.$order->ServiceRequest->no_order;

        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_pdf_ttu';
        }

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo')
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));

        return $pdf->download('skhp.pdf');
    }

    public function previewTipe($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $otherOrders = DB::table('uttp_inspection_prices')
            ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->join('service_order_uttps', 'service_order_uttps.service_request_item_inspection_id', '=', 'service_request_uttp_item_inspections.id')
            ->where('uttp_inspection_prices.service_type_id', $order->ServiceRequest->service_type_id)
            ->where('uttp_inspection_price_types.uttp_type_id', $order->ServiceRequestItem->uttp->type_id)
            ->where('service_order_uttps.ujitipe_completed', true)
            ->selectRaw('service_order_uttps.*')
            ->get();

        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_pdf_tipe';

        $view = true;
        //dd($order);
        return view($blade,compact(
            'order', 'otherOrders', 'qrcode_generator', 'view'
        ));
    }

    public function printTipe($id)
    {
        $order = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $file_name = 'skhp'.$order->ServiceRequest->no_order;

        /*
        $qrcode_generator = 
            "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
            "Ditandatangai oleh: " . "\r\n" .
            ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
            'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
            date("Y-m-d H:i:s",  strtotime($order->updated_at));
        */
        $qrcode_generator = route('documentuttp.valid', $order->qrcode_token);

        $blade = 'serviceuttp.skhp_pdf_tipe';

        $view = false;

        $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo')
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));

        return $pdf->download('skhp.pdf');
    }

    private function checkAndUpdateFinishOrder($id) 
    {
        $order = ServiceOrderUttps::find($id);

        /*
        $inspection = ServiceRequestUttpItemInspection::find($order->service_request_item_inspection_id);
        if ($inspection->status_sertifikat == 1 && $inspection->status_uttp == 1) {
            $inspection->update(['status_id' => 14]); 
        }

        $inspections_count = DB::table('service_request_uttp_item_inspections')
            ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
            ->where('request_item_id', $order->service_request_item_id)->get();
        
        if ($inspections_count[0]->count_14 == $inspections_count[0]->count_all) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
        }
        */
        if ($order->stat_warehouse == 1 && $order->stat_sertifikat == 3) {
            ServiceRequestUttpItem::find($order->service_request_item_id)->update(['status_id'=>14]);
            ServiceRequestUttpItemInspection::where("request_item_id", $order->service_request_item_id)->update([
                "status_id"=>14,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                ->where('request_id', $order->service_request_id)->get();

            if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>14]);
            }

            if ($order->ServiceRequest->status_id == 14) {
                $history = new HistoryUttp();
                $history->request_status_id = 14;
                $history->request_id = $order->service_request_id;
                $history->request_item_id = $order->service_request_item_id;
                $history->order_id = $order->id;
                $history->user_id = Auth::id();
                $history->save();
            }
        }
    }

    public function pending($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.pending',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmpending($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $order->update([
            "pending_status" => 1,
            "pending_created" => date('Y-m-d H:i:s'),
            "pending_notes" => $request->get("pending_notes"),
            //"pending_estimated" => date("Y-m-d", strtotime($request->get("pending_estimated"))),
        ]);

        ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>15]);

        $customerEmail = $order->ServiceRequest->requestor->email;
        //Mail::to($customerEmail)->send(new Pending($order));
        ProcessEmailJob::dispatch($customerEmail, new Pending($order))->onQueue('emails');

        return Redirect::route('serviceuttp');
    }

    public function continue($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('serviceuttp.continue',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcontinue($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);

        $order->update([
            "pending_status" => 0,
            "pending_ended" => date('Y-m-d H:i:s'),
        ]);

        ServiceRequestUttp::find($order->service_request_id)->update(['status_id'=>12]);

        return Redirect::route('serviceuttp');
    }

    public function cancel($id)
    {
        $attribute = $this->MyProjects->setup("service");

        $serviceOrder = ServiceOrderUttps::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uttp', 'ServiceRequestItem.uttp.type',
            'MasterUsers', 'LabStaffOut',
        ])->find($id);

        return view('servicerevisionuttp.cancel',compact(
            'serviceOrder',
            'attribute'
        ));
    }

    public function confirmcancel($id, Request $request)
    {
        $order = ServiceOrderUttps::find($id);
        $item = ServiceRequestUttpItem::find($order->service_request_item_id);
    

        $order->update([
            //"cancel_at" => date('Y-m-d H:i:s'),
            "cancel_notes" => $request->get("cancel_notes"),
            //"cancel_id" => Auth::id(),
            //"stat_service_order" => 4,
            //"stat_sertifikat" => 0,
            "stat_service_order" => 2,
            "stat_sertifikat" => 1,
            //"status_id" => 16,
        ]);

        //$inspection = ServiceRequestUttpItemInspection::where('request_item_id', $order->service_request_item_id)
        //    ->update(['status_id' => 16]);
        
        //$item->status_id = 16;
        //$item->save();

        /*
        $history = new HistoryUttp();
        $history->request_status_id = 16;
        $history->request_id = $order->service_request_id;
        $history->request_item_id = $order->service_request_item_id;
        $history->order_id = $order->id;
        $history->order_status_id = 1;
        $history->user_id = Auth::id();
        $history->save();
        */

        return Redirect::route('serviceuttp');
    }

}
