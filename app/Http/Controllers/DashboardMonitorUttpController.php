<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DashboardMonitorUttpController extends Controller
{
    private $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("dashboardmonitoruttp");
        if($attribute==null) return userlogout();

        $start = null;
        $end = null;

        $q_sum_all = DB::table('service_order_uttps')
            //->join('service_request_uttp_item_inspections', 'service_order_uttps.service_request_item_inspection_id', '=', 'service_request_uttp_item_inspections.id')
            ->join('service_request_uttp_items', 'service_order_uttps.service_request_item_id', '=', 'service_request_uttp_items.id')
            ->join('service_request_uttps', 'service_request_uttp_items.request_id', '=', 'service_request_uttps.id')
            //->join('uttp_inspection_prices', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            //->join('master_instalasi', 'uttp_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->join('uttps', 'service_order_uttps.uttp_id', '=', 'uttps.id')
            ->join('master_uttp_types', 'uttps.type_id', '=', 'master_uttp_types.id')
            ->join('master_instalasi', 'service_order_uttps.instalasi_id', '=', 'master_instalasi.id')
            ->selectRaw("master_instalasi.nama_instalasi, service_order_uttps.instalasi_id, 
            count(distinct service_request_uttp_items.id) as jumlah,
            sum(case when service_request_uttps.status_id = 14 then 1 else 0 end) as jumlah_selesai,
            sum(case when service_request_uttps.status_id not in (14, 16) then 1 else 0 end) as jumlah_proses,
            sum(case when service_request_uttps.status_id = 14 and 
                DATE_PART('day', service_order_uttps.kabalai_date::timestamp - service_request_uttp_items.order_at::timestamp) 
                    - (select count(*) from generate_series(service_request_uttp_items.order_at::timestamp, service_order_uttps.kabalai_date::timestamp, '1 day'::interval) s
                		where extract(isodow from s) in (6, 7)) 
                	- (select count(id) from holidays h 
						where holiday_date between service_request_uttp_items.order_at::timestamp and service_order_uttps.kabalai_date::timestamp)
                    <= (case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end) then 1 else 0 end) as jumlah_sesuai_sla,
            sum(case when service_request_uttps.status_id = 14 and 
                DATE_PART('day', service_order_uttps.kabalai_date::timestamp - service_request_uttp_items.order_at::timestamp) 
                    - (select count(*) from generate_series(service_request_uttp_items.order_at::timestamp, service_order_uttps.kabalai_date::timestamp, '1 day'::interval) s
                		where extract(isodow from s) in (6, 7)) 
                	- (select count(id) from holidays h 
						where holiday_date between service_request_uttp_items.order_at::timestamp and service_order_uttps.kabalai_date::timestamp)
                    > (case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end) then 1 else 0 end) as jumlah_lebih_sla
            ")
            ->groupBy('master_instalasi.nama_instalasi', 'service_order_uttps.instalasi_id');

        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
        
            $q_sum_all = $q_sum_all->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            $q_sum_all = $q_sum_all->where('staff_entry_datein', '>=', $start);
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            $q_sum_all = $q_sum_all->where('staff_entry_datein', '<=', $end);
        }
        //dd($q_sum_all->toSql());
            
        $q_sum = clone $q_sum_all;
        $summaries = $q_sum->get();
        $instalasi = $q_sum->pluck('nama_instalasi');

        $q_sum_ttu = clone $q_sum_all;
        $q_sum_ttu = $q_sum_ttu->whereIn('service_request_uttps.service_type_id', [4,5]);
        $summaries_ttu = $q_sum_ttu->get();
        $instalasi_ttu = $q_sum_ttu->pluck('nama_instalasi');

        $q_sum_tipe = clone $q_sum_all;
        $q_sum_tipe = $q_sum_tipe->whereIn('service_request_uttps.service_type_id', [6,7]);
        $summaries_tipe = $q_sum_tipe->get();
        $instalasi_tipe = $q_sum_tipe->pluck('nama_instalasi');
        
        return view('dashboardmonitoruttp.home',compact([
            'attribute', 
            'summaries', 
            'instalasi',
            'summaries_ttu', 
            'instalasi_ttu',
            'summaries_tipe', 
            'instalasi_tipe',
            'start', 'end',
        ]));
    }

    public function details(Request $request) {
        $q_sum_all = DB::table('service_order_uttps')
            //->join('service_request_uttp_item_inspections', 'service_order_uttps.service_request_item_inspection_id', '=', 'service_request_uttp_item_inspections.id')
            ->join('service_request_uttp_items', 'service_order_uttps.service_request_item_id', '=', 'service_request_uttp_items.id')
            ->join('service_request_uttps', 'service_request_uttp_items.request_id', '=', 'service_request_uttps.id')
            //->join('uttp_inspection_prices', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            //->join('master_instalasi', 'uttp_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->join('uttps', 'service_order_uttps.uttp_id', '=', 'uttps.id')
            ->join('master_uttp_types', 'uttps.type_id', '=', 'master_uttp_types.id')
            ->join('master_instalasi', 'master_uttp_types.instalasi_id', '=', 'master_instalasi.id')
            ->selectRaw('service_request_uttp_items.no_order, 
                service_request_uttps.no_register,
                service_request_uttps.label_sertifikat, 
                master_uttp_types.uttp_type as jenis,
                service_request_uttps.jenis_layanan,
                master_instalasi.nama_instalasi,
                service_request_uttps.received_date,
                service_order_uttps.staff_entry_dateout
                ');

        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
        
            $q_sum_all = $q_sum_all->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            $q_sum_all = $q_sum_all->where('staff_entry_datein', '>=', $start);
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            $q_sum_all = $q_sum_all->where('staff_entry_datein', '<=', $end);
        }

        if ($request->get('instalasi') != null) {
            $q_sum_all = $q_sum_all->where('master_uttp_types.instalasi_id', '=', $request->get('instalasi'));
        }
        if ($request->get('type') != null) {
            if ($request->get('type') == 'ttu') {
                $q_sum_all->whereIn('service_request_uttps.service_type_id', [4,5]);
            } elseif ($request->get('type') == 'tipe') {
                $q_sum_all->whereIn('service_request_uttps.service_type_id', [6,7]);
            } 
        }
        if ($request->get('sla') != null) {
            if ($request->get('sla') == 'selesai') {
                $q_sum_all->where('service_request_uttps.status_id', 14);
            } elseif ($request->get('sla') == 'proses') {
                $q_sum_all->whereNotIn('service_request_uttps.status_id', [14,16]);
            } elseif ($request->get('sla') == 'sesuai') {
                $q_sum_all->where('service_request_uttps.status_id', 14)
                    ->whereRaw("DATE_PART('day', service_order_uttps.kabalai_date::timestamp - service_request_uttp_items.order_at::timestamp) 
                        - (select count(*) from generate_series(service_request_uttp_items.order_at::timestamp, service_order_uttps.kabalai_date::timestamp, '1 day'::interval) s
                            where extract(isodow from s) in (6, 7)) 
                        - (select count(id) from holidays h 
                            where holiday_date between service_request_uttp_items.order_at::timestamp and service_order_uttps.kabalai_date::timestamp)
                        <= (case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end)");
            } elseif ($request->get('sla') == 'lebih') {
                $q_sum_all->where('service_request_uttps.status_id', 14)
                    ->whereRaw("DATE_PART('day', service_order_uttps.kabalai_date::timestamp - service_request_uttp_items.order_at::timestamp)
                        - (select count(*) from generate_series(service_request_uttp_items.order_at::timestamp, service_order_uttps.kabalai_date::timestamp, '1 day'::interval) s
                            where extract(isodow from s) in (6, 7)) 
                        - (select count(id) from holidays h 
                            where holiday_date between service_request_uttp_items.order_at::timestamp and service_order_uttps.kabalai_date::timestamp) 
                        > (case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end)");
            } 

        }

        return response($q_sum_all->get());
    }
}
