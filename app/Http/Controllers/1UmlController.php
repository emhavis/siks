<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadeRequest;
use App\Http\Repositories\UmlRepository;
use App\Http\Repositories\StandardRepository;
use App\Http\Repositories\NegaraRepository;
use App\Uml;
use Validator;
use Redirect;
use Auth;
use Session;
use URL;
use Log;

class UmlController extends Controller
{
	protected $umlRepo;

    public function __construct(StandardRepository $standardRepo, UmlRepository $umlRepo, NegaraRepository $negaraRepo)
    {
    	$this->middleware('auth', ['except' => ['getUMLInfo','create_standard_simpan','edit_standard_simpan']]);
        $this->umlRepo = $umlRepo;
        $this->standardRepo = $standardRepo;
        $this->negaraRepo = $negaraRepo;
    }

    public function index(Request $request)
    {
    	try
        {
            $umls = $this->umlRepo->getUMLs();
            
        } catch(Exception $e)
        {
            Log::error('Error at opening UML index menu with message: ' . $e->errorInfo);
        }
        
        return view('uml.index', compact('umls'));
    }

    public function create(Request $request)
    {
        return view('uml.create');
    }

    // public function register_standard()
    // {
    //     if(Auth::user()->user_role=="7")
    //     {
    //         $registeredStandards = DB::table("registered_standards")
    //         ->leftJoin("uml","uml.id","=","registered_standards.uml_id")
    //         ->leftJoin("uml_sub","uml_sub.id","=","registered_standards.uml_sub_id")
    //         ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","registered_standards.standard_measurement_type_id")
    //         ->leftJoin("standard_tool_types","standard_tool_types.id","=","registered_standards.standard_tool_type_id")
    //         ->leftJoin("standard_detail_types","standard_detail_types.id","=","registered_standards.standard_detail_type_id")
    //         ->select(
    //             'registered_standards.id',
    //             'stat_registered',
    //             'tool_code',
    //             'uml_name',
    //             'uml_sub_name',
    //             'standard_type',
    //             'jumlah_per_set',
    //             'register_date'
    //         )
    //         ->where("registered_standards.uml_id",Auth::user()->uml_id)
    //         ->orderBy('register_date','desc')
    //         ->get();

    //     }
    //     else if(Auth::user()->user_role=="2")
    //     {
    //         $registeredStandards = DB::table("registered_standards")
    //         ->leftJoin("uml","uml.id","=","registered_standards.uml_id")
    //         ->leftJoin("uml_sub","uml_sub.id","=","registered_standards.uml_sub_id")
    //         ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","registered_standards.standard_measurement_type_id")
    //         ->leftJoin("standard_tool_types","standard_tool_types.id","=","registered_standards.standard_tool_type_id")
    //         ->leftJoin("standard_detail_types","standard_detail_types.id","=","registered_standards.standard_detail_type_id")
    //         ->select(
    //             'registered_standards.id',
    //             'stat_registered',
    //             'tool_code',
    //             'uml_name',
    //             'uml_sub_name',
    //             'standard_type',
    //             'jumlah_per_set',
    //             'register_date'
    //         )
    //         // ->where("registered_standards.uml_id",Auth::user()->uml_id)
    //         ->orderBy('register_date','desc')
    //         ->get();
    //     }

    //     return view('uml.register_standard',compact('registeredStandards'));
    // }

    // public function edit_standard_simpan(Request $request)
    // {
    //     $validation = Validator::make($request->all(), 
    //     [
    //     'sumber_id' => 'required',
    //     'standard_tool_type_id' => 'required',
    //     'standard_detail_type_id' => 'required',
    //     'brand' => 'required',
    //     'made_in' => 'required',
    //     'model' => 'required',
    //     'tipe' => 'required',
    //     'no_seri' => 'required',
    //     'no_identitas' => 'required',
    //     'capacity' => 'required',
    //     'daya_baca' => 'required',
    //     'class' => 'required',
    //     'jumlah_per_set' => 'required'
    //     ]);

    //     if ($validation->passes())
    //     {
    //         $id = $request->id;
    //         // $request->request->remove('id');

    //         $standard = DB::table('registered_standards')
    //         ->where('id',$id)
    //         ->get();

    //         DB::table("registered_standards")
    //         ->where("id",$id)
    //         ->update(["stat_registered" => 1]);

    //         DB::table("standards")
    //         ->insert([
    //         "tool_code" => $standards[0]->tool_code,
    //         "uml_id" => $standards[0]->uml_id,
    //         "uml_sub_id" => $standards[0]->uml_sub_id,
    //         "standard_measurement_type_id" => $standards[0]->standard_measurement_type_id,

    //         "standard_tool_type_id" => $request->standard_tool_type_id,
    //         "standard_detail_type_id" => $request->standard_detail_type_id,
    //         "sumber_id" => $request->sumber_id,
    //         "standard_tool_type_id" => $request->standard_tool_type_id,
    //         "standard_detail_type_id" => $request->standard_detail_type_id,
    //         "sumber_id" => $request->sumber_id,
    //         "brand" => $request->brand,
    //         "made_in" => $request->made_in,
    //         "model" => $request->model,
    //         "tipe" => $request->tipe,
    //         "no_seri" => $request->no_seri,
    //         "no_identitas" => $request->no_identitas,
    //         "capacity" => $request->capacity,
    //         "daya_baca" => $request->daya_baca,
    //         "class" => $request->class,
    //         "jumlah_per_set" => $request->jumlah_per_set,
    //         "register_date"=>date("Y-m-d"),
    //         "stat_registered"=>1
    //         ]);

    //         echo json_encode(array(true,"Data berhasil disimpan"));
    //     }
    //     else
    //     {
    //         echo json_encode(array(false, $validation->messages()));
    //     }        
    // }
    // public function register_proses($id)
    // {
    //     $standard = DB::table('registered_standards')
    //     ->leftJoin("uml","uml.id","=","registered_standards.uml_id")
    //     ->leftJoin("uml_sub","uml_sub.id","=","registered_standards.uml_sub_id")
    //     ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","registered_standards.standard_measurement_type_id")
    //     ->leftJoin("standard_tool_types","standard_tool_types.id","=","registered_standards.standard_tool_type_id")
    //     ->leftJoin("standard_detail_types","standard_detail_types.id","=","registered_standards.standard_detail_type_id")
    //     ->where('registered_standards.id',$id)
    //     ->select("registered_standards.*","standard_type","attribute_name","standard_detail_type_name","uml_name","uml_sub_name")
    //     ->get();

    //     $standardNegara = $this->negaraRepo->getNEGARAsForDropdown();

    //     $msumber = DB::table("msumber")->orderBy('id')->pluck('nama_sumber', 'id');

    //     return view('uml.edit_standard', compact(
    //       'standard',
    //       'id',
    //       'standardNegara',
    //       'msumber'
    //     ));
    // }

    public function store(Request $request)
    {
        // try 
        // {
        //     $validation = Validator::make($request->all(), ServiceRequest::$rules);

        //     if ($validation->passes()) 
        //     {
        //         $this->serviceRequestRepo->createServiceRequest($request, Auth::user()->id);
        //     }
        //     else 
        //     {
        //         return Redirect::back()->withInput()->withErrors($validation->messages());
        //     }
        // } catch(Exception $e) 
        // {
        //     Log::error('Error at creating new service request with message: ' . $e->errorInfo);

        //     return Redirect::back()->withInput()->withErrors($e->errorInfo);
        // }

        return Redirect::route('uml.index');
    }

    public function edit($id)
    {
        try 
        {
            $uml = $this->umlRepo->getUMLById($id);

            if(Session::has('returnURL'))
            {
                Session::keep('returnURL');
            }

        } catch(Exception $e)
        {
            Log::error('Error at opening editing UML form with id ' . $id . ' - with message: ' . $e->errorInfo);
        }

        return view('uml.edit', compact('uml'));
    }

    public function update(Request $request, $id)
    {
        try 
        {
            $validation = Validator::make($request->all(), UML::$rules);

            if ($validation->passes()) 
            {
                $this->umlRepo->updateUML($id, $request->uml_name, $request->address, $request->phone_no);
            }
            else 
            {
                return Redirect::back()->withInput()->withErrors($validation->messages());
            }
        } catch(Exception $e) 
        {
            Log::error('Error at updating UML (id ' . $id . ') with message: ' . $e->errorInfo);

            return Redirect::back()->withInput()->withErrors($e->errorInfo);
        }

        return ($url = Session::get('returnURL')) ? Redirect::to($url) : Redirect::route('UML.index');
    }

    //API method
    public function getUMLInfo(Request $request)
    {
        try 
        {
            if(FacadeRequest::ajax())
            {
                $uml = $this->umlRepo->getUMLById($request->id);

                return json_encode($uml);
            }

        } catch(Exception $e)
        {
            Log::error('Error while retrieving UML data id ' . $request->id . ' - with message: ' . $e->errorInfo);
        }

        return null;
    }
}
