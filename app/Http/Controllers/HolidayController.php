<?php

namespace App\Http\Controllers;

use App\Holiday;
use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HolidayController extends Controller
{
    private $MasterLaboratory;
    private $MasterInstalasi;
    private $MyProjects;

    public function __construct() 
    {
        $this->Holiday = new Holiday();
        $this->MyProjects = new MyProjects();
    }

    public function index() 
    {
        $attribute = $this->MyProjects->setup("holiday");

        $holidays = $this->Holiday->get();
        return view('holiday.index', compact('holidays','attribute'));
    }

    public function create($id=null) 
    {
        $attribute = $this->MyProjects->setup("holiday");

        $row = null;

        if($id)
        {
            $row = $this->Holiday->whereId($id)->first();
        }

        return view('holiday.create',compact('row','id','attribute'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["holiday_date"] = ['required','date'];
        $rules["name"] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $request["updated_at"] = date("Y-m-d H:i:s");
                $this->Holiday->whereId($id)->update($request->all());
            }
            else
            {
                $request["created_at"] = date("Y-m-d H:i:s");
                $request["updated_at"] = date("Y-m-d H:i:s");
                $this->Holiday->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->Holiday->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
}
