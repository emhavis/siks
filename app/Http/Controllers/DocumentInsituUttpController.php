<?php

namespace App\Http\Controllers;

use App\MyClass\MyProjects;

use App\ServiceRequestUttp;
use App\Customer;
use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\UttpInsituCheckItem;
use App\ServiceRequestUttpInsituDocCheck;
use App\HistoryUttp;
use App\ServiceRequestUttpItem;
use App\MasterServiceType;
use App\ServiceOrderUttps;
use App\MasterUsers;
use App\MasterPetugasUttp;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Mail\SuratTugasInsitu;
use App\Mail\InvoiceTUHP;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

class DocumentInsituUttpController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("docinsituuttp");

        $rows = ServiceRequestUttp::where('status_id', 22)
            ->leftJoin('service_request_uttp_insitu_staff', 'service_request_uttp_insitu_staff.doc_id', '=', 'service_request_uttps.spuh_doc_id')
            ->select('service_request_uttps.*')
            ->distinct()
            ->orderBy('received_date','desc')->get();

        return view('docinsituuttp.index',compact('rows','attribute'));
    }

    public function approval($id) 
    {
        $attribute = $this->MyProjects->setup("docinsituuttp");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        return view('docinsituuttp.approve', compact(['request', 'requestor', 'doc', 'staffs',  'attribute']));
    }

    public function approve($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("docinsituuttp");

        $requestEntity = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)
                ->whereNotNull('scheduled_id')
                ->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        $rules = [];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            
            //$data['is_accepted'] = $request->get('is_accepted') === "ya" ? true :
            //        ($request->get('is_accepted') === "tidak" ? false : null);
 
            /*
            $doc->update([
                'is_accepted' => $data['is_accepted'],
                
            ]);
            */

            if ($request->get("is_approved") === "ya" ) 
            {
                $lastDoc = ServiceRequestUttpInsituDoc::where('spuh_year', date("Y"))
                    ->whereNotNull('spuh_no')
                    //->orderBy('spuh_no', 'desc')
                    ->orderByRaw('cast(spuh_no as integer) desc')
                    ->orderBy('id', 'desc')
                    ->first();
                
                //dd($lastDoc);
                $no = 0;
                if ($lastDoc != null) {
                    $no = $lastDoc->spuh_no;
                }
                $no = $no + 1;
                $spuh_spt = 'MR.05.01/' . $no . '/PKTN.4.5/ST/'.date("m/Y");
                $data["spuh_spt"] = $spuh_spt;
                //dd([$lastDoc->toSql(), $no, $spuh_spt]);

                $status = 12; //13

                

                // create order
                
                $items = ServiceRequestUttpItem::where("request_id", $id)->orderBy('id', 'asc')->get();

                
                $serviceType = MasterServiceType::find($requestEntity->service_type_id);
                $no_order = $serviceType->insitu_last_order_no;

                $prefix = $serviceType->prefix;

                $alat = count($items);
                $noorder_num = intval($no_order)+1;
                $noorder = 'DL-'.$prefix.'-'.date("y-").sprintf("%04d",$noorder_num)."-";
                $nokuitansi = 'DL-'.$prefix.'-'.date("y-").sprintf("%04d",$noorder_num);
                
                $token = $this->hashing($spuh_spt, $noorder);

                // simpan doc
                $doc->update([
                    "doc_no" => $spuh_spt,
                    "spuh_no" => $no,
                    "spuh_year" => date("Y"),

                    'accepted_date' => date("Y-m-d"),
                    'accepted_by_id' => Auth::id(),
                    'jenis' => 'inisial',

                    'token' => $token,
                ]);

                ServiceRequestUttp::whereId($id)
                ->update([
                    "spuh_spt"=>$spuh_spt,
                    "status_id"=>$status,
                    "no_order"=>$nokuitansi,
                    "spuh_payment_status_id"=>5,
                ]);

                $no = 0;
                foreach($items as $item) {

                    ServiceRequestUttpItem::where("id", $item->id)
                    ->update([
                        "status_id"=>$status,
                        //"no_order"=>$noorder_alat,
                        //"order_at"=> date("Y-m-d H:i:s"),
                    ]);

                    $history = new HistoryUttp();
                    $history->request_status_id = $status; 
                    $history->request_id = $id;
                    $history->request_item_id = $item->id;
                    $history->spuh_payment_status_id = 5; 
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = $status;
                    $item->save();
                }

                $serviceType->insitu_last_order_no = $noorder_num;
                $serviceType->save();

                // UNTUK CUSTOMER
                $customerEmail = $requestEntity->requestor->email;;
                //Mail::to($customerEmail)->send(new BookingConfirmation($requestEntity));
                ProcessEmailJob::dispatch($customerEmail, new SuratTugasInsitu($requestEntity, $staffs, null))
                    ->onQueue('emails');

                /* temporary disabled
                foreach($staffs as $staffEmail) {
                    $petugas = $staffEmail->scheduledStaff;

                    if ($petugas != null) {
                        $petugasEmail = $petugas->email;
                        //Mail::to($customerEmail)->send(new BookingConfirmation($requestEntity));
                        ProcessEmailJob::dispatch($petugasEmail, new SuratTugasInsitu($requestEntity, $staffs, $petugas))
                            ->onQueue('emails');
                    }
                }
                */


                // SUBKO
                // temporary disabled
                /*
                $subko_id = $requestEntity->scheduled_test_id;
                $subko = MasterUsers::find($subko_id);
                if ($subko->petugas_uttp_id != null) {
                    $subko_petugas = MasterPetugasUttp::find($subko->petugas_uttp_id);
                    $subko_email = $subko_petugas != null ? $subko_petugas->email : $subko->email;
                } else {
                    $subko_petugas = null;
                    $subko_email = $subko->email;
                }
                ProcessEmailJob::dispatch($subko_email, new SuratTugasInsitu($requestEntity, $staffs, $subko_petugas))
                        ->onQueue('emails');
                */


                // PAYMENT TUHP
                if ($requestEntity->lokasi_pengujian == 'luar'
                    && ($doc->invoiced_price > 0)) {
                    ServiceRequestUttp::whereId($id)
                    ->update([
                        "spuh_no" => $requestEntity->spuh_no,
                        "spuh_billing_date"=>date("Y-m-d"),
                        "spuh_payment_date" => null,
                        "spuh_payment_status_id" => 6,
                    ]);

                    $items = ServiceRequestUttpItem::where("request_id", $id)->orderBy('id', 'asc')->get();

                    foreach($items as $item) {
                        $history = new HistoryUttp();
                        $history->request_status_id = $requestEntity->status_id;
                        $history->request_id = $requestEntity->id;
                        $history->request_item_id = $item->id;
                        $history->spuh_payment_status_id = 6;
                        $history->user_id = Auth::id();
                        $history->save();
                    }

                    if ($doc->payment_date == null) {
                        $doc->billing_date = date("Y-m-d");
                    }
                    //if ($doc->act_price != null && $doc->act_price > $doc->price && $doc->act_payment_date == null) {
                    //    $doc->act_billing_date = date("Y-m-d");
                    //}
                    $doc->billing_date = date("Y-m-d");
                    $doc->save();

                    $customerEmail = $requestEntity->requestor->email;
                    //Mail::to($customerEmail)->send(new Invoice($svcRequest));
                    ProcessEmailJob::dispatch($customerEmail, new InvoiceTUHP($requestEntity, $doc))->onQueue('emails');
                }

            } else {
                $status = 3;
                ServiceRequestUttp::whereId($id)
                ->update([
                    "status_id"=>$status,
                ]);

                $doc->update([
                    'approval_note' => $request->get('approval_note'),
                ]);

                $items = ServiceRequestUttpItem::where("request_id", $id)->orderBy('id', 'asc')->get();

                
                foreach($items as $item) {
                    ServiceRequestUttpItem::where("id", $item->id)
                    ->update([
                        "status_id"=>$status,
                        //"no_order"=>$noorder_alat,
                        //"order_at"=> date("Y-m-d H:i:s"),
                    ]);

                    $history = new HistoryUttp();
                    $history->request_status_id = $status; 
                    $history->request_id = $id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = $status;
                    $item->save();
                }
            }
            return Redirect::route('docinsituuttp');
        } else {
            return Redirect::route('docinsituuttp.approval', $id);
        }
    }

    public function generateToken()
    {
        $docs = ServiceRequestUttpInsituDoc::whereNotNull('doc_no')
            ->whereNull('token')
            ->whereNotNull('accepted_date')
            ->whereNotNull('accepted_by_id')
            ->get();

        foreach($docs as $doc)
        {
            if ($doc->request != null) {
                $noorder = $doc->request->no_order;
                
                $token = $this->hashing($doc->doc_no, $noorder);

                $doc->update([
                    'token' => $token
                ]);
            }
        }

        return "Done";
    }

    private function hashing($username, $password) {
        $token = Hash::make($username.':'.$password);
        if (strpos($token, '/') !== false) {
            $token = $this->hashing($username, $password);
        } 
        return $token;
    }

    public function integritas($id) 
    {
        $attribute = $this->MyProjects->setup("docinsituuttp");

        $request = ServiceRequestUttp::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        //$requestor = Customer::find($request->requestor_id);

        $doc = ServiceRequestUttpInsituDoc::where('request_id', $id)->orderBy('id', 'desc')->first();

        $staffs = ServiceRequestUttpInsituStaff::where("doc_id", $doc->id)->get();

        //$checks = ServiceRequestUttpInsituDocCheck::where("doc_id", $doc->id)->orderBy('order_no', 'asc')->get();

        return view('docinsituuttp.integritas', compact(['request', 'staffs',  'attribute']));
    }
}