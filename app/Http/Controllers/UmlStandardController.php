<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Request as FacadeRequest;
// use App\Http\Repositories\UmlStandardRepository;

use App\StandardMeasurementType;
use App\StandardMeasurementUnit;
use App\UmlStandardRegister;
use App\MasterNegara;
use App\MasterSumber;
use App\MasterUml;
use App\Standard;
use App\Qrcode;
use App\MyClass\MyProjects;
// use Redirect;
use Auth;
use Validator;

class UmlStandardController extends Controller
{
    protected $StandardMeasurementType;
    protected $StandardMeasurementUnit;
    protected $UmlStandardRegister;
    protected $MasterNegara;
    protected $MasterSumber;
    protected $MasterUml;
    protected $Standard;
    protected $Qrcode;
    protected $MyProjects;

    public function __construct()
    {
        // $this->middleware('auth', ['except' => ['getUmlStandardInfo']]);
        $this->StandardMeasurementType = new StandardMeasurementType();
        $this->StandardMeasurementUnit = new StandardMeasurementUnit();
        $this->UmlStandardRegister = new UmlStandardRegister();
        $this->MasterNegara = new MasterNegara();
        $this->MasterSumber = new MasterSumber();
        $this->MasterUml = new MasterUml();
        $this->Standard = new Standard();
        $this->Qrcode = new Qrcode();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup('umlstandard');
        if($attribute==null) return userlogout();

        if(Auth::user()->user_role=="7")
        {
            $registeredStandards = $this->UmlStandardRegister
            ->leftJoin("uml","uml.id","=","registered_standards.uml_id")
            ->leftJoin("uml_sub","uml_sub.id","=","registered_standards.uml_sub_id")
            ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","registered_standards.standard_measurement_type_id")
            ->leftJoin("standard_tool_types","standard_tool_types.id","=","registered_standards.standard_tool_type_id")
            ->leftJoin("standard_detail_types","standard_detail_types.id","=","registered_standards.standard_detail_type_id")
            ->select(
                'registered_standards.id',
                'stat_registered',
                'tool_code',
                'uml_name',
                'uml_sub_name',
                'standard_type',
                'jumlah_per_set',
                'register_date'
            )
            ->where("registered_standards.uml_id",Auth::user()->uml_id)
            ->orderBy('register_date','desc')
            ->get();

        }
        else if(Auth::user()->user_role=="2")
        {
            $registeredStandards = $this->UmlStandardRegister
            ->leftJoin("uml","uml.id","=","registered_standards.uml_id")
            ->leftJoin("uml_sub","uml_sub.id","=","registered_standards.uml_sub_id")
            ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","registered_standards.standard_measurement_type_id")
            ->leftJoin("standard_tool_types","standard_tool_types.id","=","registered_standards.standard_tool_type_id")
            ->leftJoin("standard_detail_types","standard_detail_types.id","=","registered_standards.standard_detail_type_id")
            ->select(
                'registered_standards.id',
                'stat_registered',
                'tool_code',
                'uml_name',
                'uml_sub_name',
                'standard_type',
                'jumlah_per_set',
                'register_date'
            )
            // ->where("registered_standards.uml_id",Auth::user()->uml_id)
            ->orderBy('register_date','desc')
            ->get();
        }

        return view('umlstandard.index',compact('registeredStandards','attribute'));
    }

    public function create()
    {
        $attribute = $this->MyProjects->setup('umlstandard');
        if($attribute==null) return userlogout();

        $standardMeasurementTypes = $this->StandardMeasurementType->dropdown();
        $standardMeasurementUnits = $this->StandardMeasurementUnit->dropdown();
        $standardNegara = $this->MasterNegara->dropdown();
        $msumber = $this->MasterSumber->dropdown();
        $umls = $this->MasterUml->dropdown();

        $uml_id = Auth::user()->uml_id;
        return view('umlstandard.create',compact(
          'standardMeasurementTypes',
          'standardMeasurementUnits',
          'standardNegara',
          'msumber',
          'umls',
          'attribute',
          'uml_id'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;

        $rules['uml_id'] = ['required'];
        $rules['uml_sub_id'] = ['required'];
        $rules['tool_code'] = ['required'];
        $rules['standard_measurement_type_id'] = ['required'];
        $rules['sumber_id'] = ['required'];
        $rules['standard_tool_type_id'] = ['required'];
        $rules['standard_detail_type_id'] = ['required'];
        $rules['brand'] = ['required'];
        $rules['made_in'] = ['required'];
        $rules['model'] = ['required'];
        $rules['tipe'] = ['required'];
        $rules['no_seri'] = ['required'];
        $rules['no_identitas'] = ['required'];
        $rules['capacity'] = ['required'];
        $rules['daya_baca'] = ['required'];
        $rules['class'] = ['required'];
        $rules['jumlah_per_set'] = ['required'];

        $validation = Validator::make($request->all(),$rules,error_messages());
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $is_exists = $this->Standard->where('tool_code',$request->tool_code)->exists();
            $is_exists_qrcode = $this->Qrcode->where('tool_code',$request->tool_code)->exists();

            if($is_exists==true)
            {
                $response["messages"] = ["qr_code"=>"QR Code <b>".$request->tool_code."</b> sudah terdaftar di Master Standard. Silahkan masukan QR Code alat yang belum terdaftar di Master Standard."];
            }
            else if($is_exists_qrcode==false)
            {
                $response["messages"] = ["qr_code"=>"QR Code <b>".$request->tool_code."</b> belum terdaftar di Database. Silahkan masukan QR Code yang sudah terfaftar di Database."];
            }
            else
            {
                $this->UmlStandardRegister
                ->insert([
                "tool_code" => $request->tool_code,
                "uml_id" => $request->uml_id2,
                "uml_sub_id" => $request->uml_sub_id2,
                "standard_measurement_type_id" => $request->standard_measurement_type_id2,
                "standard_tool_type_id" => $request->standard_tool_type_id,
                "standard_detail_type_id" => $request->standard_detail_type_id,
                "sumber_id" => $request->sumber_id,
                "brand" => $request->brand,
                "made_in" => $request->made_in,
                "model" => $request->model,
                "tipe" => $request->tipe,
                "no_seri" => $request->no_seri,
                "no_identitas" => $request->no_identitas,
                "capacity" => $request->capacity,
                "daya_baca" => $request->daya_baca,
                "class" => $request->class,
                "jumlah_per_set" => $request->jumlah_per_set,
                "register_date"=>date("Y-m-d"),
                "stat_registered"=>0
                ]);
                // $this->standardRepo->createStandard($request);
                
                $response["status"] = true;
                $response["messages"] = "Data berhasil disimpan";
            }
        }

        return response($response);
    }

    public function proses($id)
    {
        $attribute = $this->MyProjects->setup('umlstandard');
        if($attribute==null) return userlogout();

        $standard = $this->UmlStandardRegister
        ->leftJoin("uml","uml.id","=","registered_standards.uml_id")
        ->leftJoin("uml_sub","uml_sub.id","=","registered_standards.uml_sub_id")
        ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","registered_standards.standard_measurement_type_id")
        ->leftJoin("standard_tool_types","standard_tool_types.id","=","registered_standards.standard_tool_type_id")
        ->leftJoin("standard_detail_types","standard_detail_types.id","=","registered_standards.standard_detail_type_id")
        ->where('registered_standards.id',$id)
        ->select("registered_standards.*","standard_type","attribute_name","standard_detail_type_name","uml_name","uml_sub_name")
        ->get();

        $standardNegara = $this->MasterNegara->dropdown();

        $msumber = $this->MasterSumber->dropdown();

        return view('umlstandard.proses', compact(
          'standard',
          'id',
          'standardNegara',
          'attribute',
          'msumber'
        ));
    }

    public function update(Request $request)
    {
        $response["status"] = false;

        $rules['brand'] = ['required'];
        $rules['made_in'] = ['required'];
        $rules['model'] = ['required'];
        $rules['tipe'] = ['required'];
        $rules['no_seri'] = ['required'];
        $rules['no_identitas'] = ['required'];
        $rules['capacity'] = ['required'];
        $rules['daya_baca'] = ['required'];
        $rules['class'] = ['required'];
        $rules['jumlah_per_set'] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $id = $request->id;

            $request->request->remove('id');

            $this->Standard->whereId($id)->update($request);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return response($response);
        // }
        // catch(Exception $e)
        // {
        //     echo json_encode(array(false,$e->errorInfo));
        // }
    }

    public function edit(Request $request)
    {
        $response["status"] = false;

        $rules['sumber_id'] = ['required'];
        $rules['standard_tool_type_id'] = ['required'];
        $rules['standard_detail_type_id'] = ['required'];
        $rules['brand'] = ['required'];
        $rules['made_in'] = ['required'];
        $rules['model'] = ['required'];
        $rules['tipe'] = ['required'];
        $rules['no_seri'] = ['required'];
        $rules['no_identitas'] = ['required'];
        $rules['capacity'] = ['required'];
        $rules['daya_baca'] = ['required'];
        $rules['class'] = ['required'];
        $rules['jumlah_per_set'] = ['required','numeric'];

        $validation = Validator::make($request->all(),$rules,error_messages());
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $id = $request->id;
            // $request->request->remove('id');

            $standard = $this->UmlStandardRegister
            ->where('id',$id)
            ->get();

            $this->UmlStandardRegister
            ->where("id",$id)
            ->update(["stat_registered" => 1]);

            $this->Standard
            ->insert([
            "tool_code" => $standards[0]->tool_code,
            "uml_id" => $standards[0]->uml_id,
            "uml_sub_id" => $standards[0]->uml_sub_id,
            "standard_measurement_type_id" => $standards[0]->standard_measurement_type_id,

            "standard_tool_type_id" => $request->standard_tool_type_id,
            "standard_detail_type_id" => $request->standard_detail_type_id,
            "sumber_id" => $request->sumber_id,
            "standard_tool_type_id" => $request->standard_tool_type_id,
            "standard_detail_type_id" => $request->standard_detail_type_id,
            "sumber_id" => $request->sumber_id,
            "brand" => $request->brand,
            "made_in" => $request->made_in,
            "model" => $request->model,
            "tipe" => $request->tipe,
            "no_seri" => $request->no_seri,
            "no_identitas" => $request->no_identitas,
            "capacity" => $request->capacity,
            "daya_baca" => $request->daya_baca,
            "class" => $request->class,
            "jumlah_per_set" => $request->jumlah_per_set,
            "register_date"=>date("Y-m-d"),
            "stat_registered"=>1
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        
        return response($response);
    }

    public function getumlinfo($id)
    {
        $uml = $this->MasterUml->whereId($id)->first();
        return response($uml);
    }    
}
