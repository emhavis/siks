<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrders;
use App\ServiceOrderInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\ServiceRequestItemInspection;
use App\ServiceRequestsItem;
use App\MasterUsers;
use App\MasterServiceType;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrderUutTTUInspection;
use App\ServiceOrderUutUTTUInspectionItems;
use App\ServiceOrderUutTTUInspectionBadanHitung;
use App\ServiceOrderUutTTUPerlengkapan;
use App\ServiceOrderUutpEvaluasiTipe;

use App\Mail\Pending;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;

use App\MasterInstalasi;
use App\HistoryUttp;

class ServiceHistoryLuarUutController extends Controller
{
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    protected $MasterNegara;

    public function __construct()
    {
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("servicehistoryluaruut");

        // $rows = ServiceOrders::whereIn("stat_service_order",[0,1,2,3,4])
        //     ->whereHas('ServiceRequestItem', function($query) 
        //     {
        //         $query->whereIn("status_id", [13,14,16]);
        //     })
        //     ->whereHas('ServiceRequest', function($query) 
        //     {
        //         $query->where("lokasi_pengujian","=", 'luar');
        //     })
        //     ->where('test_by_1', Auth::id())
        //     ->where('stat_sertifikat', '>=', 1)
        //     ->orderBy('staff_entry_datein','desc')
        //     ->get();

        $petugas_id = Auth::user()->petugas_uut_id;
        $rows = ServiceOrders::join('service_request_items', 'service_request_items.id', '=', 'service_orders.service_request_item_id')
            ->join('service_requests', 'service_requests.id', '=', 'service_orders.service_request_id')
            ->leftJoin('service_request_uut_insitu_staff', 'service_request_uut_insitu_staff.doc_id', '=', 'service_requests.spuh_doc_id')
            ->whereIn("stat_service_order",[0,1,2,3,4])
            ->where('service_request_items.status_id', 13)
            ->where('service_requests.lokasi_pengujian', 'luar')
            ->where('service_request_uut_insitu_staff.scheduled_id', $petugas_id)

            ->where("is_finish",0)
            ->where(function($query) {
                $query->where('stat_sertifikat', 0)
                      ->orWhereNull('stat_sertifikat');
            })
            ->whereNull('subkoordinator_notes')
            ->where(function($query) {
                $query->whereNull('test_by_1')
                      ->orWhere('test_by_1', Auth::id());
            })
            ->orderBy('staff_entry_datein','desc')
            ->select('service_orders.*')
            ->get();

        return view('servicehistoryluaruut.index',compact('rows','attribute'));
    }

    public function warehouse(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        if ($request->has('id')) {
            $order = ServiceOrders::find($request->id);
        } else {
            $order = ServiceOrders::where("no_order", $request->no_order)->first();
        }

        if ($order != null) {
            ServiceOrders::whereId($order->id)->update(["stat_warehouse"=>1]);
            if ($order->stat_warehouse == 1 && $order->stat_sertifikat == 3) {
                ServiceRequestItem::find($order->service_request_item_id)->update(['status_id'=>14]);
                ServiceRequestItemInspection::where("service_request_item_id", $order->service_request_item_id)->update([
                    "status_id"=>14,
                ]);

                $items_count = DB::table('service_request_items')
                    ->selectRaw('sum(case when status_id = 14 then 1 else 0 end) count_14, count(id) count_all')
                    ->where('request_id', $order->service_request_id)->get();

                if ($items_count[0]->count_14 == $items_count[0]->count_all) {
                    ServiceRequest::find($order->service_request_id)->update(['status_id'=>14]);
                }

                if ($order->ServiceRequest->status_id == 14) {
                    $history = new HistoryUut();
                    $history->request_status_id = 14;
                    $history->request_id = $order->service_request_id;
                    $history->request_item_id = $order->service_request_item_id;
                    $history->order_id = $order->id;
                    $history->user_id = Auth::id();
                    $history->save();
                }
            }
    }

    
    }
}
