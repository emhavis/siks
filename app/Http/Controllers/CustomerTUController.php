<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\ServiceOrderUttps;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\HistoryUttp;
use App\MasterInstalasi;
use App\Uttp;

class CustomerTUController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("customertu");

        $rows = \DB::table('users_customers')
                ->join('master_kabupatenkota', 'users_customers.kota_id', '=', 'master_kabupatenkota.id')
                ->join('master_provinsi', 'master_kabupatenkota.provinsi_id', '=', 'master_provinsi.id')
                ->select("users_customers.*", "master_kabupatenkota.nama as kota", "master_provinsi.nama as provinsi")
                ->get();
        
        return view('customerstu.index',compact(['rows','attribute']));
    }    

    public function getdata(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Data tidak ditemukan';

        if ($request->has('id')) {
            $row = \DB::table('users_customers')
                ->select('full_name', 'kantor', 'email', 'phone', 'is_active')
                ->where('id', '=', $request->get('id'))
                ->first();

            $response["status"] = true;
            $response["messages"] = 'Data ditemukan';
            $response["data"] = $row;
        }

        return response($response);
    }

    public function read($id)
    {
        $attribute = $this->MyProjects->setup("customertu");

        $row = \DB::table('users_customers')
                ->join('master_kabupatenkota', 'users_customers.kota_id', '=', 'master_kabupatenkota.id')
                ->join('master_provinsi', 'master_kabupatenkota.provinsi_id', '=', 'master_provinsi.id')
                ->select("users_customers.*", "master_kabupatenkota.nama as kota", "master_provinsi.nama as provinsi")
                ->where('users_customers.id', $id)
                ->first();

        $uttp_owners = \DB::table('uttp_owners')
                ->join('user_uttp_owners', 'uttp_owners.id', '=', 'user_uttp_owners.owner_id')
                ->join('master_kabupatenkota', 'uttp_owners.kota_id', '=', 'master_kabupatenkota.id')
                ->join('master_provinsi', 'master_kabupatenkota.provinsi_id', '=', 'master_provinsi.id')
                ->leftJoin('master_badan_usaha', 'master_badan_usaha.id', '=', 'uttp_owners.bentuk_usaha_id')
                ->select("uttp_owners.*", "master_kabupatenkota.nama as kota", "master_provinsi.nama as provinsi", "master_badan_usaha.nama as bentuk")
                ->where('user_uttp_owners.user_id', $id)
                ->get();

        $uttps = Uttp::whereIn('owner_id', $uttp_owners->map(function($item, $key) { return $item->id; }))
                ->get();

        return view('customerstu.detail',compact(['row','attribute','uttp_owners','uttps']));
    }  
}
