<?php

namespace App\Http\Controllers;

//use App\ServiceOrders;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\ServiceOrders;
use App\ServiceRequest;
use App\ServiceRequestUutStaff;
use App\ServiceRequestUutInsituDoc;

use PDF;

use App\MasterInstalasi;
use App\HistoryUttp;
use LynX39\LaraPdfMerger\Facades\PdfMerger;
use setasign\Fpdi\Tcpdf\Fpdi;
use Illuminate\Support\Str;

class DocumentUutController extends Controller
{
    
    public function __construct()
    {

    }

    private function checkToken($token) 
    {
        $token = DB::table('user_access_tokens')
                    ->where('token', $token)
                    ->where('expired_at', '>', date("Y-m-d H:i:s"))
                    ->first();

        return $token;
    }

    public function print($id, $jenis_sertifikat, $token)
    {
        $token = $this->checkToken($token);
        if ($token) {
            // $pdfMerger = PdfMerger::init();
            // $order = ServiceOrders::find($id);
            // $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);
            // $qrcode_generator = route('documentuut.valid', $order->qrcode_token);
            // $blade ='serviceuut.skhp_tipe_pdf';
            // $view = false;

            // $pdf = PDF::setOptions([

            //     'isHtml5ParserEnabled' => true, 
            //     'isRemoteEnabled' => true,
            //     'tempDir' => public_path(),
            //     'chroot'  => public_path('assets/images/logo'),
            //     'defaultPaperSize' => 'a4',
            //     'defaultMediaType' => 'print',
            // ])
            //     ->loadview($blade, compact('order', 'qrcode_generator', 'view'));
            
            //     $file = public_path('storage/images/file.pdf');

            //     if(! file_exists($file)){
            //         Storage::delete('public/tmp/file.pdf');
            //     }

            // Storage::put('public/tmp/file.pdf', $pdf->output());
            // $pdfMerger->addPDF(storage_path('app/public/tmp/file.pdf'),'all');
            // if($order->path_lampiran !=null){

            //     $pdfMerger->addPDF(storage_path('app/public/'.$order->path_lampiran),'all');
            //     $pdfMerger->merge();
            //     $pdfMerger->save(storage_path('app/public/sample.pdf'));
            //     $this->doWatermark('app/public/sample.pdf',$order->qrcode_token,$order->no_sertifikat,$order->id);

            //     Storage::delete('public/tmp/file.pdf');
            //     Storage::delete('public/tmp/qrcode.png');
            //     Storage::delete('app/public/sample.pdf');

            //     $no = str_replace('/','_',$order->no_sertifikat);
            //     $no ='Lampiran Sertifikat No.'.$no;
            //     $file = storage_path('app/public/tmp/SERTIFIKAT-'.$no.'.pdf','P');

            //     // return response()->download(storage_path('app/public/sample.pdf','P'));
            //     return response()->download($file);
            //     Storage::delete('app/public/tmp/SERTIFIKAT-'.$no.'.pdf');
            // }

            // return $pdf->stream();


            $pdfMerger = PdfMerger::init();
            $order = ServiceOrders::find($id);
            $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);
            $qrcode_generator = route('documentuut.valid', [
                'jenis_sertifikat' => 'skhp',
                'token' => $order->qrcode_token]
            );
            $blade ='serviceuut.skhp_tipe_pdf';
            $view = false;

            $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo'),
                'defaultPaperSize' => 'a4',
                'defaultMediaType' => 'print',
            ])
                ->loadview($blade, compact('order', 'qrcode_generator', 'view'));
            
            $file = public_path('storage/images/file.pdf');
            if(! file_exists($file)){
                // Storage::delete('public/tmp/file.pdf');
            }

            Storage::put('public/tmp/file.pdf', $pdf->output());
            $pdfMerger->addPDF(storage_path('app/public/tmp/file.pdf'),'all');
            if(($order->kabalai_id ==null || $order->kabalai_id ='') && $order->cancel_at == null && $order->cancel_notes !=null){
                return $pdf->stream();
            }

            if($order->path_lampiran !=null){
                $pdfMerger->addPDF(storage_path('app/public/'.$order->path_lampiran),'all');
                $pdfMerger->merge();
                $pdfMerger->save(storage_path('app/public/sample.pdf'));
                $this->doWatermark('app/public/sample.pdf',$order->qrcode_token,$order->no_sertifikat,$order->id);
                $no = str_replace('/','_',$order->no_sertifikat);
                $no ='Lampiran-No.'.$no;
                
                $file = storage_path('app/public/tmp/SERTIFIKAT-'.$no.'.pdf','P');
                if(is_file($file))
                {
                    return response()->download($file);
                    // 1. possibility
                    Storage::delete($file);
                    // 2. possibility
                    unlink(storage_path($file));
                }
            }
            if($order->cancelation_file !=null){
                
                if($order->no_sertifikat ==null){
                    $order->no_sertifikat ="UNDEF";
                }
                $pdfMerger->addPDF(storage_path('app/public/'.$order->cancelation_file),'all');
                $pdfMerger->merge();
                $pdfMerger->save(storage_path('app/public/sample_cancel.pdf'));
                $this->doWatermark('app/public/sample_cancel.pdf',$order->qrcode_token,$order->no_sertifikat,$order->id);
                
                $no = str_replace('/','_',$order->no_sertifikat);
                $no ='Lampiran-No.'.$no;
                $file = storage_path('app/public/tmp/SBL-'.$no.'.pdf','P');

                if(is_file($file))
                {
                    return response()->download($file);
                    // 1. possibility
                    Storage::delete($file);
                    // 2. possibility
                    unlink(storage_path($file));
                }
            }
            return $pdf->stream();
        }
        abort(403);
    }

    public function print_old_newer($id, $jenis_sertifikat, $token)
    {
        $token = $this->checkToken($token);
        if ($token) {
            $order = ServiceOrders::find($id);

            if ($jenis_sertifikat == 'skhp') {
                $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

                $qrcode_generator = route('documentuut.valid', $order->qrcode_token);
    
                $blade = 'serviceuut.skhp_tipe_pdf';
                if ($order->ServiceRequest->service_type_id == 1 || 
                    $order->ServiceRequest->service_type_id == 2) {
                        // $blade = 'serviceuut.skhp_ttu_pdf';
                        $blade = 'serviceuut.skhp_tipe_pdf';
                }
    
                $view = false;
    
                $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 
                        'isRemoteEnabled' => true,
                        'tempDir' => public_path(),
                        'chroot'  => public_path('assets/images/logo'),
                        'defaultPaperSize' => 'a4',
                        'defaultMediaType' => 'print',
                    ])
                    ->loadview($blade,compact('order', 'qrcode_generator', 'view'));
    
                return $pdf->download($file_name . '.pdf');
            } elseif ($jenis_sertifikat == 'skhpt') {
                $file_name = ($order->is_skhpt ? 'SKHPT ' : 'SKHP ').($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

                $qrcode_generator = route('documentuut.valid', $order->qrcode_token);
    
                $blade = 'serviceuut.skhpt_tipe_pdf';
                
                $view = false;
    
                $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 
                        'isRemoteEnabled' => true,
                        'tempDir' => public_path(),
                        'chroot'  => public_path('assets/images/logo'),
                        'defaultPaperSize' => 'a4',
                        'defaultMediaType' => 'print',
                    ])
                    ->loadview($blade,compact('order', 'qrcode_generator', 'view'));
    
                return $pdf->download($file_name . '.pdf');

            } elseif ($jenis_sertifikat == 'set') {
                $file_name = 'SET '.($order->no_sertifikat ? $order->no_sertifikat : $order->ServiceRequestItem->no_order);

                $order = ServiceOrders::with([
                    'ServiceRequest', 
                    'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
                    'MasterUsers', 'LabStaffOut',
                    'persyaratanTeknis', 
                    'inspections' => function ($query)
                    {
                        $query->orderBy('id', 'asc');
                    },
                ])->find($id);
        
                $otherOrders = ServiceOrders::whereHas(
                    'ServiceRequestItem', function($query) use($order)
                    {
                        $query->where("uut_id",$order->ServiceRequestItem->uut_id);  
                    })
                    ->get();
        
                $qrcode_generator = route('documentuut.valid', $order->qrcode_tipe_token);
        
                $blade = 'serviceuut.set_tipe_pdf';
        
                $view = false;

                $pdf = PDF::setOptions([
                    'isHtml5ParserEnabled' => true, 
                    'isRemoteEnabled' => true,
                    'tempDir' => public_path(),
                    'chroot'  => public_path('assets/images/logo'),
                    'defaultPaperSize' => 'a4',
                    'defaultMediaType' => 'print',
                ])
                ->loadview($blade,compact(['order', 'otherOrders', 'qrcode_generator', 'view']));
           
                return $pdf->download($file_name . '.pdf');
            } 
        }

        abort(403);
    }

    public function print_old($id, $token)
    {
        $token = $this->checkToken($token);

        if ($token) {
            $order = ServiceOrders::with([
                'ServiceRequest', 
                'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
                'MasterUsers', 'LabStaffOut',
                'persyaratanTeknis', 
                'inspections' => function ($query)
                {
                    $query->orderBy('id', 'asc');
                },
            ])->find($id);
                
            $file_name = 'skhp'.$order->ServiceRequest->no_order;
    
            /*
            $qrcode_generator = 
                "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
                "Ditandatangai oleh: " . "\r\n" .
                ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
                'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
                date("Y-m-d H:i:s",  strtotime($order->updated_at));
            */
            $qrcode_generator = route('documentuut.valid', $order->qrcode_token);
    
            $blade = 'serviceuut.skhp_pdf';
            if ($order->ServiceRequest->service_type_id == 4 || 
                $order->ServiceRequest->service_type_id == 5) {
                    $blade = 'serviceuut.skhp_pdf_ttu';
            }
    
            $view = false;
            $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo')
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));
            //$pdf = PDF::loadview($blade,compact('order', 'qrcode_generator'));
    
            return $pdf->download('skhp.pdf');
            //return view($blade,compact('order','qrcode_generator')); 
        }

        abort(403);
    }

    public function download($id, $token)
    {
        $token = $this->checkToken($token);

        if ($token) {
            $order = ServiceOrders::find($id);

            return Storage::disk('public')->download($order->path_skhp, $order->file_skhp);
        }

        abort(403);
    }

    public function valid_old($token)
    {
        if ($token) {
            $order = ServiceOrders::with([
                'ServiceRequest', 
                'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
                'MasterUsers', 'LabStaffOut',
                'persyaratanTeknis', 
                'inspections' => function ($query)
                {
                    $query->orderBy('id', 'asc');
                },
            ])->where('qrcode_token', $token)
            ->first();
    
            $file_name = 'skhp'.$order->ServiceRequest->no_order;
    
            /*
            $qrcode_generator = 
                "Nomor SKHP: " . $order->no_sertifikat . "\r\n" .
                "Ditandatangai oleh: " . "\r\n" .
                ($order->KaBalai ? $order->KaBalai->full_name : '') . "\r\n" . 
                'NIP. ' . ($order->KaBalai ? $order->KaBalai->nip : ''). "\r\n" .
                date("Y-m-d H:i:s",  strtotime($order->updated_at));
                */
            $qrcode_generator = route('documentuut.valid', $order->qrcode_token);
            
    
            $blade = 'serviceuut.skhp_pdf';
            if ($order->ServiceRequest->service_type_id == 4 || 
                $order->ServiceRequest->service_type_id == 5) {
                    $blade = 'serviceuut.skhp_pdf_ttu';
            }
    
            $view = true;
            /*
            $pdf = PDF::setOptions([
                'isHtml5ParserEnabled' => true, 
                'isRemoteEnabled' => true,
                'tempDir' => public_path(),
                'chroot'  => public_path('assets/images/logo')
            ])
            ->loadview($blade,compact('order', 'qrcode_generator', 'view'));
            */
            //$pdf = PDF::loadview($blade,compact('order', 'qrcode_generator'));
    
            //return $pdf->download('skhp.pdf');
            return view($blade,compact('order','qrcode_generator', 'view')); 
        }

        abort(403);
    }

    public function valid($jenis_sertifikat, $token)
    {
        if ($token && $jenis_sertifikat) {

            if ($jenis_sertifikat != 'surat_tugas') {
                $order = ServiceOrders::with([
                    'ServiceRequest', 
                    'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
                    'MasterUsers', 'LabStaffOut',
                    'persyaratanTeknis', 
                    'inspections' => function ($query)
                    {
                        $query->orderBy('id', 'asc');
                    },
                ])->where('qrcode_token', $token)
                ->first();
                // dd($order->ServiceRequest->lokasi_pengujian);
                $order = ServiceOrders::with([
                    'ServiceRequest', 
                    'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
                    'MasterUsers', 'LabStaffOut',
                    'persyaratanTeknis', 
                    'inspections' => function ($query)
                    {
                        $query->orderBy('id', 'asc');
                    },
                ])->where('qrcode_token', $token)
                ->first();
        
                $blade = 'serviceuut.valid';
        
                $view = true;
                return view($blade,compact('order','view')); 
                
            } else {
                $doc = ServiceRequestUutInsituDoc::with([
                    'request', 'listOfStaffs', 'acceptedBy'                    
                ])
                    ->where('token', $token)->first();

                $blade = 'schedulinguut.valid';
        
                $view = true;
                return view($blade,compact(['doc', 'view']));
            }
        }

        abort(403);
    }

    public function invoice($id, $token)
    {
        $token = $this->checkToken($token);

        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUutInsituDoc::where('request_id', $id)->get();

        if ($token) {
            $row = ServiceRequest::find($id);
            return view('requestuut.uut-pdf',compact(['row','docs','staffes']));
        }       
        abort(403);
    }

    public function invoicetuhp($id, $token)
    {
        $token = $this->checkToken($token);
        
        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUutInsituDoc::where('request_id', $id)->get();
    //dd($docs[0]->listOfStaffs->sortBy('id'));
        if ($token) {
            $row = ServiceRequest::find($id);
            return view('requestluaruut.pdf_tuhp',compact(['row','staffes','docs']));
        }
        
        abort(403);
    }

    public function kuitansi($id, $token)
    {
        $token = $this->checkToken($token);
        $staffes = ServiceRequestUutStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUutInsituDoc::where('request_id', $id)->get();

        if ($token) {
            $row = ServiceRequest::find($id);
            $terbilang = $this->convert($row->total_price);
            $terbilangSPUH = $this->convert($row->spuh_price);

            // dd([$row,$terbilang,$terbilangSPUH,$docs]);
            $terbilangSPUH = [];
            $terbilangActSPUH = [];
            foreach($docs as $doc) {
                $terbilangSPUH[$doc->id] = $this->convert($doc->invoiced_price);
                if ($doc->act_price != null && $doc->act_price > $doc->price) {
                    $terbilangActSPUH[$doc->id] = $this->convert($doc->act_price - $doc->price);
                }
            }

            return view('requestuut.kuitansi_pdf',
                compact(['row','terbilang','terbilangSPUH','docs']));
        }

        abort(403);
    }

    private function convert($number)
    {
        // dd($number);
        //$number = str_replace('.', '', $number);
        // if ( ! is_numeric($number)) throw new \Exception("Please input number.");
        $number = floatval($number);
        $base    = array('nol', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
        $numeric = array('1000000000000000', '1000000000000', '1000000000000', 1000000000, 1000000, 1000, 100, 10, 1);
        $unit    = array('kuadriliun', 'triliun', 'biliun', 'milyar', 'juta', 'ribu', 'ratus', 'puluh', '');
        $str     = null;
        $i = 0;
        if ($number == 0) {
            $str = 'nol';
        } else {
            while ($number != 0) {
                $count = (int)($number / $numeric[$i]);
                if ($count >= 10) {
                    $str .= static::convert($count) . ' ' . $unit[$i] . ' ';
                } elseif ($count > 0 && $count < 10) {
                    $str .= $base[$count] . ' ' . $unit[$i] . ' ';
                }
                $number -= $numeric[$i] * $count;
                $i++;
            }
            $str = preg_replace('/satu puluh (\w+)/i', '\1 belas', $str);
            $str = preg_replace('/satu (ribu|ratus|puluh|belas)/', 'se\1', $str);
            $str = preg_replace('/\s{2,}/', ' ', trim($str));
        }
        return $str;
    }

    public function buktiorder($id, $token)
    {
        $token = $this->checkToken($token);

        if ($token) {
            $row = ServiceRequest::find($id);
            return view('requestuut.order_pdf',compact('row'));
        }

        abort(403);
    }

    public function doWatermark($file,$qrcode_token,$no_sertifikat,$id)
    {//$file, $text_image){
        $fileloc='';
        $order = ServiceOrders::find($id);
        if($order->no_sertifikat !=null){
            $no_sertifikat = "Lampiran-No.".$order->no_sertifikat;
        }
        if($order->no_sertifikat ==null){
            $no_sertifikat = "UNDEF";
        }
        // $no_sertifikat = "Lampiran Sertifikat No.".$no_sertifikat;

        $pdf = new \setasign\Fpdi\Tcpdf\Fpdi();
        
        $qrcode_generator = route('documentuut.valid', [
            'jenis_sertifikat'=> 'skhp',
            'token' => $qrcode_token
        ]);
        $qrcode =  base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format("png")
                                ->size(100)
                                ->margin(0.5)
                                ->errorCorrection("H")
                                ->generate($qrcode_generator,storage_path('app/public/tmp/qrcode.png')));
        
        // Set source PDF file     
        if($order->cancel_at !=null && $order->cancel_notes != null){
            $fileloc = storage_path('app/public/sample_cancel.pdf');
        }else{
            $fileloc = storage_path('app/public/sample.pdf');
        }     
        // $fileloc = storage_path('app/public/sample.pdf');
        
        if(file_exists($fileloc)){ 
            $pagecount = $pdf->setSourceFile($fileloc); 
        }else{ 
            die('File PDF Tidak ditemukan.!'); 
        } 
        
        // Add watermark image to PDF pages 
        for($i=1;$i<=$pagecount;$i++){ 
            
           /*  $tpl = $pdf->importPage($i); 
            $pdf->addPage();

            $size = $pdf->getTemplateSize($tpl); 
            $pdf->useTemplate($tpl, 1, 1, $size['width'], $size['height'], TRUE); 
            
            //Put the watermark 
            $xxx_final = ($size['width']-60); 
            $yyy_final = ($size['height']-25);  */

            $tplidx = $pdf->importPage($i);
            $specs = $pdf->getTemplateSize($tplidx);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->addPage($specs['height'] > $specs['width'] ? 'P' : 'L');
            $pdf->useTemplate($tplidx, null, null, $specs['width'], $specs['height'], true);

            $pdf->SetTextColor(0, 0, 0);
            $_x_text = ($specs['width']- 60) - ($pdf->GetStringWidth($order->no_sertifikat, '', '',10)/2.8);
            $_y_text = $specs['height']- $specs['height'] +5+5;

            //setting for name of kabalai

            $_x_Hkab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama :'Kabalai', '', '',10)/2.8);
            $_y_Hkab = $specs['height']-70;

            // line2
            $_x_HL2kab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama :'Kabalai', '', '',10)/2.8);
            $_y_HL2kab = $specs['height']-65;

            //Writing nip
            $_x_Nkab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama : 'Kabalai', '', '',10)/2.8);
            $_y_Nkab = $specs['height']-25;

            //setting for name of kabalai
            $_x_kab = ($specs['width'] -75 ) - ($pdf->GetStringWidth($order->KaBalai ? $order->KaBalai->PetugasUut->nama : 'Kabalai', '', '',10)/2.8);
            $_y_kab = $specs['height']-30;

            $_x = ($specs['width']-50) - ($pdf->GetStringWidth($order->no_sertifikat, '', '', 10)/2.8);
            $_y = $specs['height']-20;
            
            if($i >=2){
                $pdf->SetFont('', 'italic', 10);
                $pdf->SetXY($_x_text, $_y_text);
                $pdf->setAlpha(1);
                $pdf->Write(0,$order->no_sertifikat);
            }

            // $pdf->Image($qrcode, $xxx_final, $yyy_final, 0, 0, 'png'); 
            if($i == $pagecount){
                if($order->cancel_at ==null|| $order->cancel_notes ==null){
                    //Setting header and name kabalai
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Hkab, $_y_Hkab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Ketua Tim Pelayanan Teknis");

                    //Line 2
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_HL2kab, $_y_HL2kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Balai SNSU");

                    //writing NIP
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Nkab, $_y_Nkab);
                    $pdf->setAlpha(1);
                    // $pdf->Write(0,"NIP : ".$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nip :'');

                    //Setting header and name kabalai
                    // $pdf->SetFont('', 'U', 10);
                    $pdf->SetFont('', 10);
                    $pdf->SetXY($_x_kab, $_y_kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nama :'SUBKOORDINATOR');

                    $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $_x-20,$_y-40, 27, 27, 'png'); 
                    // $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $xxx_final,$yyy_final-45, 33, 33, 'png'); 
                    Storage::delete('app/public/tmp/qrcode.png');
                }else{
                    //Setting header and name kabalai
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Hkab, $_y_Hkab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Ketua Tim Pelayanan Teknis");

                    //Line 2
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_HL2kab, $_y_HL2kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,"Balai SNSU");

                    //writing NIP
                    $pdf->SetFont('', '', 10);
                    $pdf->SetXY($_x_Nkab, $_y_Nkab);
                    $pdf->setAlpha(1);
                    // $pdf->Write(0,"NIP : ".$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nip :'');

                    //Setting header and name kabalai
                    $pdf->SetFont('', 'U', 10);
                    $pdf->SetXY($_x_kab, $_y_kab);
                    $pdf->setAlpha(1);
                    $pdf->Write(0,$order->SubKoordinator ? $order->SubKoordinator->PetugasUut->nama :'SUBKOORDINATOR');

                    $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $_x-20,$_y-40, 27, 27, 'png'); 
                    // $pdf->Image(storage_path('app/public/tmp/qrcode.png'), $xxx_final,$yyy_final-45, 33, 33, 'png'); 
                    Storage::delete('app/public/tmp/qrcode.png');
                }
            }
        } 
        
        // Output PDF with watermark 
        $no = str_replace('/','_',$no_sertifikat);
        if($order->cancel_at != null && $order->cancel_notes !=null){
            $down = $pdf->Output(storage_path('app/public/tmp/SBL-'.$no.'.pdf'),'F');
        }else{
            $down = $pdf->Output(storage_path('app/public/tmp/SERTIFIKAT-'.$no.'.pdf'),'F');
        }
        // $down = $pdf->Output(storage_path('app/public/tmp/SERTIFIKAT-'.$no.'.pdf'),'F');
        if($down){
            Storage::delete('app/public/tmp/SERTIFIKAT-'.$no.'.pdf');
        }
    }

    public function download_sp($id, $token)
    {
        $isValidToken = $this->checkToken($token);
        
        $file = ServiceRequest::find($id);
        $fileUrl = env('SKHP_URL') . '/' . $file->path_surat_permohonan;
        try {
            $fileContents = @file_get_contents($fileUrl);

            if ($fileContents === false) {
                $ch = curl_init($fileUrl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $fileContents = curl_exec($ch);
                curl_close($ch);
            }

            if ($fileContents === false) {
                abort(404, 'File not found on the remote server.');
            }
            
            return response($fileContents)
                ->header('Content-Type', 'application/octet-stream')
                ->header('Content-Disposition', 'attachment; filename="' . $file->file_surat_permohonan . '"')
                ->header('Content-Length', strlen($fileContents));

        } catch (\Exception $e) {
            abort(500, 'Error retrieving file from remote server: ' . $e->getMessage());
        }
    }

}
