<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ServiceRequestUttp;
use App\ServiceRequestUttpItem;
use App\ServiceRequestUttpItemInspection;
use App\ServiceOrderUttps;

use App\Uttp;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\MyClass\MyProjects;
use App\MasterServiceType;
use App\MasterDocTypes;
use App\MasterDocNumber;
use App\MasterInstalasi;
use App\MasterNegara;

use App\ServiceBooking;
use App\ServiceBookingItem;
use App\ServiceBookingItemInspection;
use App\ServiceBookingItemTTUPerlengkapan;

use App\Customer;

use App\UttpInspectionPrice;
use App\Holiday;

use App\ServiceRequestUttpStaff;
use App\ServiceRequestUttpInsituDoc;
use App\ServiceRequestUttpInsituStaff;
use App\ServiceRequestUttpItemTTUPerlengkapan;

use App\Mail\Invoice;
use App\Mail\Receipt;
use App\Mail\BookingConfirmation;
use Illuminate\Support\Facades\Mail;

use App\HistoryUttp;

use App\Jobs\ProcessEmailJob;

//use App\MasterServiceTypeDocNumber;

use PDF;

class RequestUttpController extends Controller
{
    // protected $StandardInspectionPrice;
    protected $MyProjects;
    protected $MasterDocTypes;
    protected $MasterServiceType;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        $this->MasterServiceType = new MasterServiceType();
        $this->MasterDocTypes = new MasterDocTypes();

        $this->ServiceBooking = new ServiceBooking();
        $this->ServiceRequestUttp = new ServiceRequestUttp();
        $this->ServiceRequestUttpItem = new ServiceRequestUttpItem();
        $this->ServiceRequestUttpItemInspection = new ServiceRequestUttpItemInspection();
        $this->ServiceOrderUttp = new ServiceOrderUttps();

        $this->Customer = new Customer();

        $this->MasterInstalasi = new MasterInstalasi();
        $this->Holiday = new Holiday();

        $this->MasterNegara = new MasterNegara();
        $this->Uttp = new Uttp();
     }

    public function index(Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        //$statuses = DB::table('master_request_status')->orderBy('id','asc')->pluck('status', 'id');
        if($request->get('type') == 'kn' || empty($request->get('type'))){
            /*
            $id_arr = [1,2,3,4];
            if ($request->has("status_id")) {
                $ids = $request->get("status_id");
                $id_arr = explode(",",$ids);
            } 
            */
            $rows_pendaftaran = ServiceRequestUttp::where('status_id', 1)
                ->where('lokasi_pengujian', 'dalam')
                ->orderBy('received_date','desc')->get();

            $rows_penagihan = ServiceRequestUttp::whereIn('status_id', [5,6])
                ->where('lokasi_pengujian', 'dalam')
                ->where('booking_id', '>', 0)
                ->orderBy('received_date','desc')->get();

            $rows_validasi = ServiceRequestUttp::where('status_id', 7)
                ->where('lokasi_pengujian', 'dalam')
                ->orderBy('received_date','desc')->get();

            

            /*
            $rows_kirim = ServiceRequestUttpItem::with([
                'serviceRequest' => function ($query)
                {
                    $query->where('status_id', 8)
                        ->orderBy('received_date', 'desc');
                }
            ])
                ->where('status_id', 8)
                ->orderBy('id','asc')->get();
            */
            $rows_kirim = $rows_done = ServiceOrderUttps::whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id", 8);
            })->with([
                'serviceRequest' => function ($query)
                {
                    $query->where('status_id', 8)
                        ->orderBy('received_date', 'desc');
                }
            ])
                ->orderBy('id','asc')->get();

            // $rows_process = ServiceRequestUttpItem::with([
            //     'serviceRequest' => function ($query)
            //     {
            //         $query->orderBy('received_date', 'desc');
            //     }
            // ])->whereIn('status_id', [9,10,11,12,13,15])->get();
            
            // $rows_done = ServiceOrderUttps::whereHas('ServiceRequestItem', function($query) 
            //     {
            //         $query->whereIn("status_id", [14,16]);
            //     })
            //     //->where('stat_sertifikat', 2)
            //     ->orderBy('staff_entry_datein','desc')->get();

            $bookings = $this->ServiceBooking
                ->whereIn('service_type_id', [4,5,6,7])
                ->whereNotNull('booking_no')
                ->where('requested', false)
                ->where('lokasi_pengujian', 'dalam')
                ->where('est_arrival_date', date('Y-m-d'))
                ->orderBy('id')
                ->get();

            $bookings_all = $this->ServiceBooking
                ->whereIn('service_type_id', [4,5,6,7])
                ->whereNotNull('booking_no')
                ->where('requested', false)
                ->where('lokasi_pengujian', 'dalam')
                ->orderBy('id')
                ->get();
            return view('requestuttp.index', compact(
                'rows_pendaftaran',
                // 'rows_pendaftaran_luar',
                'rows_penagihan',
                'rows_validasi',
                // 'rows_item_luar',
                // 'rows_penagihan_luar',
                // 'rows_validasi_luar',
                'rows_kirim',
                //'rows_process', 
                //'rows_done', 
                'bookings', 
                'bookings_all',
                // 'bookings_luar',
                'attribute'));
        }elseif($request->get('type') =='dl'){

            $bookings_luar = $this->ServiceBooking
                ->whereIn('service_type_id', [4,5,6,7])
                ->whereNotNull('booking_no')
                ->where('requested', false)
                ->where('lokasi_pengujian', 'luar')
                //->where('est_arrival_date', date('Y-m-d'))
                ->orderBy('id')
                ->get();

            $rows_item_luar = ServiceRequestUttp::where('status_id', 13)
                ->where('lokasi_pengujian', 'luar')
                //->where('payment_status_id', 5)
                ->where('total_inspection_confirm', 0)
                ->where(function($query)
                    {
                        $query->where('payment_status_id', 5)
                        ->orWhereNull('payment_status_id');
                    })
                ->get();

            $rows_penagihan_luar = ServiceRequestUttp::with(['spuhDoc'])
                ->whereIn('status_id', [5,15,18,12,13])
                ->where('lokasi_pengujian', 'luar')
                //->where('payment_status_id', 5)
                ->where(function($query)
                    {
                        $query->where(function($q1) {
                            $q1->where('payment_status_id', 5);
                                //->orWhereNull('payment_status_id');
                        })->orWhere(function($q2) {
                            $q2->where('spuh_payment_status_id', 5);
                                //->orWhereNull('spuh_payment_status_id');
                        });
                        
                    })
                ->get();

            $rows_validasi_luar = ServiceRequestUttp::with(['spuhDoc'])
                ->whereIn('status_id', [12,13,15,20])
                ->where('lokasi_pengujian', 'luar')
                //->where('payment_status_id', 7)
                ->where(function($query)
                    {
                        $query->where(function($q1) {
                            $q1->where('payment_status_id', 7)
                                ->whereNull('payment_valid_date');
                                //->orWhereNull('payment_status_id');
                        })->orWhere(function($q2) {
                            $q2->where('spuh_payment_status_id', 7)
                                ->whereNull('spuh_payment_valid_date');
                                //->orWhereNull('spuh_payment_status_id');
                        });
                    })
                ->get();

            $rows_pendaftaran_luar = ServiceRequestUttp::where('status_id', 1)
                ->where('lokasi_pengujian', 'luar')
                ->orderBy('received_date','desc')->get();

            return view('dinasluar.index', compact(
                'rows_pendaftaran_luar',
                'rows_item_luar',
                'rows_penagihan_luar',
                'rows_validasi_luar',
                'bookings_luar',
                'attribute'));
        }
        
    }

    public function create()
    {
        $attribute = $this->MyProjects->setup("requestuttp");
        $idTypes = $this->MasterDocTypes->dropdown();

        return view('requestuttp.create', compact('attribute', 'idTypes'));
    }

    public function createbooking()
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $bookings = $this->ServiceBooking
            ->whereIn('service_type_id', [4,5,6,7])
            ->whereNotNull('booking_no')
            ->where('requested', false)
            ->orderBy('id')->pluck('booking_no', 'id');
        return view('requestuttp.create_booking', compact('bookings','attribute'));
    }

    public function simpan(Request $request)
    {
        $response["status"] = false;

        $rules['booking_id'] = ['required'];
        //$rules['received_date'] = ['required','date'];
        //$rules['estimate_date'] = ['required','date','after:received_date'];
        $rules['pic_name'] = ['required'];
        $rules['pic_phone_no'] = ['required'];
        $rules['pic_email'] = ['required',"email"];
        $rules['id_type_id'] = ['required'];
        $rules['pic_id_no'] = ['required'];
        $rules['jenis_layanan'] = ['required'];

        if($request->for_sertifikat=="1")
        {
            $rules['label_sertifikat'] = ['required'];
            $rules['addr_sertifikat'] = ['required'];
        }

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        $no_register = MasterDocNumber::where("init","no_register_uttp")->value("param_value");
        $no_order = MasterDocNumber::where("init","no_order_uttp")->value("param_value");

        if ($validation->passes())
        {
            $alat = 0;
            $noreg_num = intval($no_register)+1;
            $noreg = sprintf("%04d",$noreg_num).date("-m-Y");

            //dd($request->all());
            $data = [];
            $keys = array_keys($rules);
            foreach($request->all() as $k=>$v)
            {
                if(in_array($k,$keys))
                {
                    $data[$k] = $request->get($k);
                }
            }


            $noorder_num = intval($no_order)+1;
            $noorder = date("y-").$booking->service_type_id.'-'.sprintf("%04d",$noorder_num)."-";

            $data["no_register"] = $noreg;
            $data["created_by"] = Auth::id();
            $data["pic_id"] = Auth::id();

            $data["status_id"] = 1;

            $data["received_date"] = date("Y-m-d");
            $data["estimated_date"] = date("Y-m-d");

            // dd($request->all());

            $serviceRequestID = ServiceRequestUttp::insertGetId($data);

            $noorder = $noorder.sprintf("%03d",$alat);

            ServiceRequestUttp::whereId($serviceRequestID)->update(["no_order"=>$noorder]);

            MasterDocNumber::where("init","no_register_uttp")->update(["param_value"=>$noreg_num]);

            MasterDocNumber::where("init","no_order_uttp")->update(["param_value"=>$noorder_num]);

            $response["id"] = $serviceRequestID;
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";

        }

        return response($response);
    }

    public function simpanbooking(Request $request)
    {
        $response["status"] = false;

        $rules['booking_id'] = ['required'];
        //$rules['received_date'] = ['required','date'];
        //$rules['estimate_date'] = ['required','date','after:received_date'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            //$no_register = MasterDocNumber::where("init","no_register_uttp")->value("param_value");
            //$no_order = MasterDocNumber::where("init","no_order_uttp")->value("param_value");

            $data["booking_id"] = $request->get("booking_id");
            $booking = $this->ServiceBooking->find($data["booking_id"]);

            $serviceType = MasterServiceType::find($booking->service_type_id);

            $no_register = $serviceType->last_register_no;
            //$no_order = $serviceType->last_order_no;
            $prefix = $serviceType->prefix;

            $alat = 0;
            $noreg_num = intval($no_register)+1;
            $noreg = $prefix."-".sprintf("%04d",$noreg_num).date("-m-Y");

            //dd($request->all());
            $data = [];
            $keys = array_keys($rules);
            foreach($request->all() as $k=>$v)
            {
                if(in_array($k,$keys))
                {
                    $data[$k] = $request->get($k);
                }
            }

            //$noorder_num = intval($no_order)+1;
            //$noorder = $prefix.'-'.date("y-").sprintf("%04d",$noorder_num)."-";

            $data["requestor_id"] = $booking->Pic->id;

            $lokasi = $booking->lokasi_pengujian;
            
            if ($booking->lokasi_pengujian == 'dalam') {
                $data["received_date"] = date("Y-m-d");
                $data["estimated_date"] = date("Y-m-d");
            } else {
                $data["scheduled_test_date_from"] = $booking->est_schedule_date_from;
                $data["scheduled_test_date_to"] = $booking->est_schedule_date_to;

                $data["received_date"] = $data["scheduled_test_date_from"];
                $data["estimated_date"] = $data["scheduled_test_date_to"];
            }

            $received_date = date("Y-m-d");
            $estimated_date = date("Y-m-d");

            //$data["total_price"] = floatval(str_replace(",", "", $data["total"]));
            $data["no_register"] = $noreg;
            $data["created_by"] = Auth::id();
            $data["pic_id"] = Auth::id();

            $data["booking_no"] = $booking->booking_no;
            $data["label_sertifikat"] = $booking->label_sertifikat;
            $data["addr_sertifikat"] = $booking->addr_sertifikat;
            $data["for_sertifikat"] = $booking->for_sertifikat;
            $data["uttp_owner_id"] = $booking->uttp_owner_id;

            $data["total_price"] = $booking->est_total_price;
            $data["jenis_layanan"] = $booking->jenis_layanan;
            
            $data["service_type_id"] = $booking->service_type_id;
            $data["lokasi_pengujian"] = $booking->lokasi_pengujian;

            $data["inspection_loc"] = $booking->inspection_loc;
            $data["inspection_prov_id"] = $booking->inspection_prov_id;
            $data["inspection_kabkot_id"] = $booking->inspection_kabkot_id;

            $data["no_surat_permohonan"] = $booking->no_surat_permohonan;
            $data["tgl_surat_permohonan"] = $booking->tgl_surat_permohonan;
            $data["file_surat_permohonan"] = $booking->file_surat_permohonan;
            $data["path_surat_permohonan"] = $booking->path_surat_permohonan;

            $data["lokasi_dl"] = $booking->lokasi_dl;
            $data["keuangan_perusahaan"] = $booking->keuangan_perusahaan;
            $data["keuangan_pic"] = $booking->keuangan_pic;
            $data["keuangan_jabatan"] = $booking->keuangan_jabatan;
            $data["keuangan_hp"] = $booking->keuangan_hp;

            $data["status_id"] = 1;

            // dd($request->all());

            $serviceRequestID = ServiceRequestUttp::insertGetId($data);

            if(isset($serviceRequestID) )
            {
                $booking->requested = true;
                $booking->update();

                $listInstalasiId = [];

                foreach ($booking->items as $item) {
                    $dataItem["request_id"] = $serviceRequestID;
                    $dataItem["quantity"] = $item->quantity;
                    $dataItem["subtotal"] = $item->est_subtotal;
                    $dataItem["uttp_id"] = $item->uttp_id;

                    $dataItem["location"] = $item->location;
                    $dataItem["location_prov_id"] = $item->location_prov_id;
                    $dataItem["location_kabkot_id"] = $item->location_kabkot_id;
                    $dataItem["location_negara_id"] = $item->location_negara_id;

                    $dataItem["location_lat"] = $item->location_lat;
                    $dataItem["location_long"] = $item->location_long;
                    $dataItem["location_alat"] = $item->location_alat;

                    $dataItem["file_application_letter"] = $item->file_application_letter;
                    $dataItem["file_last_certificate"] = $item->file_last_certificate;
                    $dataItem["file_manual_book"] = $item->file_manual_book;
                    $dataItem["file_type_approval_certificate"] = $item->file_type_approval_certificate;
                    $dataItem["file_calibration_manual"] = $item->file_calibration_manual;

                    $dataItem["path_application_letter"] = $item->path_application_letter;
                    $dataItem["path_last_certificate"] = $item->path_last_certificate;
                    $dataItem["path_manual_book"] = $item->path_manual_book;
                    $dataItem["path_type_approval_certificate"] = $item->path_type_approval_certificate;
                    $dataItem["path_calibration_manual"] = $item->path_calibration_manual;

                    $dataItem["reference_no"] = $item->reference_no;
                    $dataItem["reference_date"] = $item->reference_date;

                    $dataItem["status_id"] = 1;
 
                    $serviceItemID = ServiceRequestUttpItem::insertGetId($dataItem);

                    // kaji ulang
                    if ($booking->lokasi_pengujian == 'luar') {
                        $kajiUlangs = DB::table('service_booking_kajiulang')
                                        ->where('booking_id', $booking->id)
                                        ->where('booking_item_id', $item->id)
                                        ->get();
                        $reqKajiUlangs = [];
                        foreach ($kajiUlangs as $kajiUlang) {
                            $reqKajiUlang = [
                                'legalitas' => $kajiUlang->legalitas,
                                'metode' => $kajiUlang->metode,
                                'request_id' => $serviceRequestID,
                                'request_item_id' => $serviceItemID,
                                'sign_at' => $kajiUlang->sign_at,
                                'created_at' => new \DateTime(),
                                'updated_at' => new \DateTime(),
                                'klausa' => $kajiUlang->klausa,
                            ];
                            array_push($reqKajiUlangs, $reqKajiUlang);
                        }
                        DB::table('service_request_uttp_kajiulang')->insert($reqKajiUlangs);
                        
                        $ttuPerlengkapans = ServiceBookingItemTTUPerlengkapan::where('booking_item_id', $item->id)
                                        ->get();
                        $reqTTUPerlengkapans = [];
                        foreach ($ttuPerlengkapans as $tool) {
                            $reqTTUPerlengkapan = [
                                'uttp_id' => $tool->uttp_id,
                                'request_item_id' => $serviceItemID,
                            ];
                            array_push($reqTTUPerlengkapans, $reqTTUPerlengkapan);
                        }
                        ServiceRequestUttpItemTTUPerlengkapan::insert($reqTTUPerlengkapans);
                    }
                    
                    foreach ($item->inspections as $inspection) {
                        $alat++;

                        $dataInspection["request_item_id"] = $serviceItemID;
                        $dataInspection["quantity"] = $inspection->quantity;
                        $dataInspection["price"] = $inspection->price;
                        $dataInspection["inspection_price_id"] = $inspection->inspection_price_id;

                        $dataInspection["status_id"] = 1;

                        $inspectionPrice = UttpInspectionPrice::find($inspection->inspection_price_id);
                        array_push($listInstalasiId, $inspectionPrice->instalasi->id);
                    
                        $serviceInspectionID = ServiceRequestUttpItemInspection::insertGetId($dataInspection);
                    }

                    /*
                    if ($item->perlengkapans != null) {
                        foreach ($item->perlengkapans as $perlengkapan) {
                            $dataPerlengkapan["request_item_id"] = $serviceItemID;
                            $dataPerlengkapan["uttp_id"] = $perlengkapan->uttp_id;

                            $perlengkapanID = ServiceRequestUttpItemTTUPerlengkapan::insertGetId($dataPerlengkapan);
                        }
                    }
                    */

                    $history = new HistoryUttp();
                    $history->request_status_id = 1;
                    $history->request_id = $serviceRequestID;
                    $history->request_item_id = $serviceItemID;
                    $history->user_id = Auth::id();
                    $history->save();
                }

                //$noorder = $noorder.sprintf("%03d",$alat);

                $sla = $this->MasterInstalasi->whereIn('id', $listInstalasiId)->max('sla_day');
                if ($booking->service_type_id == 6 ||  $booking->service_type_id == 7) {
                    $sla = $this->MasterInstalasi->whereIn('id', $listInstalasiId)->max('sla_tipe_day');
                }
                
                $formatted_date = new \DateTime( $estimated_date );
                $date_timestamp = $formatted_date->getTimestamp();
                for ($i=0; $i < $sla; $i++) { 
                    $nextDay = date('w', strtotime('+1day', $date_timestamp) );
                    if( $nextDay == 0 || $nextDay == 6 ) { $i--; }
                    $date_timestamp = strtotime("+1day", $date_timestamp);
                }
                $formatted_date->setTimestamp($date_timestamp);
                $estimated_date = $formatted_date->format("Y-m-d");

                $holidays = Holiday::whereBetween("holiday_date", [$received_date, $estimated_date])->count();
                $formatted_date = new \DateTime( $estimated_date );
                $date_timestamp = $formatted_date->getTimestamp();
                for ($i=0; $i < $holidays; $i++) { 
                    $nextDay = date('w', strtotime('+1day', $date_timestamp) );
                    if( $nextDay == 0 || $nextDay == 6 ) { $i--; }
                    $date_timestamp = strtotime("+1day", $date_timestamp);
                }
                $formatted_date->setTimestamp($date_timestamp);
                $estimated_date = $formatted_date->format("Y-m-d");

                if ($booking->lokasi_pengujian == 'dalam') {
                    ServiceRequestUttp::whereId($serviceRequestID)->update([
                            //"no_order"=>$noorder, 
                            "estimated_date"=>$estimated_date
                    ]);
                } else {
                    //ServiceRequestUttp::whereId($serviceRequestID)->update([
                    //    "no_order"=>$noorder
                    //]);
                }

                //MasterDocNumber::where("init","no_register_uttp")->update(["param_value"=>$noreg_num]);
                //MasterDocNumber::where("init","no_order_uttp")->update(["param_value"=>$noorder_num]);

                $serviceType->last_register_no = $noreg_num;
                //$serviceType->last_order_no = $noorder_num;
                $serviceType->save();

                

                $response["id"] = $serviceRequestID;
                $response["status"] = true;
                $response["messages"] = "Data berhasil disimpan";
                $response["lokasi_pengujian"] = $lokasi;
            }
            else
            {
                $response["messages"] = 'Data gagal tersimpan';
            }
        }

        return response($response);
    }

    public function edit($id)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $request = $this->ServiceRequestUttp
            ->with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);
        $requestor = $this->Customer->find($request->requestor_id);
        //dd($request);

        return view('requestuttp.edit_booking', compact('request', 'booking', 'attribute'));
    }

    public function editbooking($id)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $request = $this->ServiceRequestUttp
            //->with(['items', 'items.inspections', 'items.uttp', 'items.perlengkapans'])
            ->find($id);
        $booking = $this->ServiceBooking->find($request->booking_id);
        $requestor = $this->Customer->find($request->requestor_id);

        $negara = $this->MasterNegara->pluck('nama_negara', 'id');
        
        return view('requestuttp.edit_booking', compact(
            ['request', 'booking', 'requestor', 'attribute', 'negara']
        ));
    }

    public function simpaneditbooking($id, Request $request)
    {
        $response["status"] = false;

        $requestEntity = $this->ServiceRequestUttp->find($id);

        $rules = [];

        //$rules['booking_id'] = ['required'];
        //$rules['received_date'] = ['required','date'];
        //$rules['estimated_date'] = ['required','date','after:received_date'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $alat = 0;
            
            $data = [];
            $keys = array_keys($rules);
            foreach($request->all() as $k=>$v)
            {
                if(in_array($k,$keys))
                {
                    $data[$k] = $request->get($k);
                }
            }
            
            //$data["received_date"] = date("Y-m-d", strtotime($data["received_date"]));
            //$data["estimated_date"] = date("Y-m-d", strtotime($data["estimated_date"]));

            //$data["total_price"] = floatval(str_replace(",", "", $data["total"]));
            $data["updated_by"] = Auth::id();
            
            /*
            $data["label_sertifikat"] = $request->get("label_sertifikat");
            $data["addr_sertifikat"] = $request->get("addr_sertifikat");
            */
            //$data["lokasi_pengujian"] = $request->get("lokasi_pengujian");
            $owner = DB::table('uttp_owners')->where('id', $request->get("uttp_owner_id"))->first();
            $data["uttp_owner_id"] = $owner->id;
            //$data["label_sertifikat"] = $owner->nama;
            //$data["addr_sertifikat"] = $owner->alamat;
            
            // dd($request->all());

            $requestEntity->update($data);

            $response["id"] = $requestEntity->id;
            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return response($response);
    }

    public function submitbooking($id, Request $request)
    {
        $response["status"] = false;

        $requestEntity = $this->ServiceRequestUttp->find($id);

        //$rules['booking_id'] = ['required'];
        $rules = [];
        if ($requestEntity->lokasi_pengujian == 'dalam') {
            $rules['received_date'] = ['required','date'];
            $rules['estimated_date'] = ['required','date','after:received_date'];
        }

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            // MASIH BELUM PAS
            $totalInspectionConfirm = $requestEntity->items->reduce(function ($carry, $item) {
                return $carry + $item->inspections->count();
            });

            if ($totalInspectionConfirm == 0 && $requestEntity->lokasi_pengujian == 'dalam') {
                $response["messages"] = "Item pemeriksaan/pengujian belum dipilih.";
            } else {

                $alat = 0;
            
                $data = [];
                $keys = array_keys($rules);
                foreach($request->all() as $k=>$v)
                {
                    if(in_array($k,$keys))
                    {
                        $data[$k] = $request->get($k);
                    }
                }
                
                $lokasi = $requestEntity->lokasi_pengujian;
                if ($requestEntity->lokasi_pengujian == 'dalam') {
                    $data["received_date"] = date("Y-m-d", strtotime($data["received_date"]));
                    $data["estimated_date"] = date("Y-m-d", strtotime($data["estimated_date"]));
                }

                //$data["total_price"] = floatval(str_replace(",", "", $data["total"]));
                $data["updated_by"] = Auth::id();
                
                /*
                $data["label_sertifikat"] = $request->get("label_sertifikat");
                $data["addr_sertifikat"] = $request->get("addr_sertifikat");
                */
                //$data["lokasi_pengujian"] = $request->get("lokasi_pengujian");
                $owner = DB::table('uttp_owners')->where('id', $request->get("uttp_owner_id"))->first();
                $data["uttp_owner_id"] = $owner->id;
                $data["label_sertifikat"] = $owner->nama;
                $data["addr_sertifikat"] = $owner->alamat;

                $data["total_inspection_confirm"] = $totalInspectionConfirm;
                

                $status = 2;
                if ($requestEntity->lokasi_pengujian == 'luar') {
                    
                    if ($request->get('is_complete') == 'ya') {
                        $status = 3;
                    } else {
                        $status = 23;
                        $data['not_complete_notes'] = $request->get('not_complete_notes');
                    }


                    if ($requestEntity->lokasi_dl == 'dalam') {
                        $rates = DB::select("select
                                srui.id,
                                srui.location_kabkot_id,
                                srui.location_prov_id,
                                s.luar,
                                s.dalam,
                                case
                                    when srui.location_kabkot_id = " .env('KABKOT_DALAM'). " then s.dalam
                                    else s.luar
                                end rate
                            from
                                service_request_uttp_items srui
                            inner join master_provinsi mp on
                                srui.location_prov_id = mp.id
                            inner join sbm s on
                                mp.id = s.provinsi_id
                            where
                                srui.request_id = " .$requestEntity->id. "
                            order by
                                case
                                    when srui.location_kabkot_id = " .env("KABKOT_DALAM"). " then s.dalam
                                    else s.luar
                                end desc,
                                srui.location_prov_id asc,
                                srui.id asc");

                        $data['inspection_prov_id'] = $rates[0]->location_prov_id;
                        $data['inspection_kabkot_id'] = $rates[0]->location_kabkot_id;
                    } else {
                        $rates = DB::select("select
                                srui.id,
                                srui.location_negara_id,
                                s.a, s.b, s.c, s.d
                            from
                                service_request_uttp_items srui
                            inner join sbm_luar s on
                                s.negara_id = srui.location_negara_id 
                            where
                                srui.request_id = " .$requestEntity->id. "
                            order by 
                                s.c desc,
                                srui.location_negara_id asc,
                                srui.id asc");

                        $data['inspection_negara_id'] = $rates[0]->location_negara_id;
                    }
                } 
                $data["status_id"] = $status;
                // dd($request->all());

                $items = ServiceRequestUttpItem::where("request_id", $id)->get();
                foreach($items as $item) {
                    $history = new HistoryUttp();
                    $history->request_status_id = $status;
                    $history->request_id = $id;
                    $history->request_item_id = $item->id;
                    $history->user_id = Auth::id();
                    $history->save();

                    $item->status_id = $status;
                    $item->save();
                }

                $requestEntity->update($data);

                $customerEmail = $requestEntity->requestor->email;
                //Mail::to($customerEmail)->send(new BookingConfirmation($requestEntity));
                ProcessEmailJob::dispatch($customerEmail, new BookingConfirmation($requestEntity))
                    ->onQueue('emails');
                
                $response["id"] = $requestEntity->id;
                $response["status"] = true;
                $response["messages"] = "Data berhasil disimpan";
                $response["lokasi_pengujian"] = $lokasi;
                
            }

            
        }

        return response($response);
    }

    public function pdf($id)
    {
        $row = ServiceRequestUttp::find($id);

        if ($row->lokasi_pengujian == 'luar') {
            if ($row->spuh_no == null) {
                $no_parts = explode("-", $row->no_register);
                $row->spuh_no = sprintf("%04d",$no_parts[1]).'/SPUH/'.date("m/Y");
            }
            if ($row->spuh_billing_date == null) {
                $row->spuh_billing_date = date("Y-m-d");
            }
        }
        
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUttpInsituDoc::where('request_id', $id)->get();

        return view('requestuttp.pdf',compact(['row', 'staffes', 'docs']));
    }

    public function kuitansi($id)
    {
        $row = ServiceRequestUttp::find($id);
        $staffes = ServiceRequestUttpStaff::where('request_id', $id)->get();
        $docs = ServiceRequestUttpInsituDoc::where('request_id', $id)->get();
        
        $terbilang = $this->convert($row->total_price);
        $terbilangSPUH = $this->convert($row->spuh_price);

        $terbilangSPUH = [];
        $terbilangActSPUH = [];
        foreach($docs as $doc) {
            $terbilangSPUH[$doc->id] = $this->convert($doc->invoiced_price);
            if ($doc->act_price != null && $doc->act_price > $doc->price) {
                $terbilangActSPUH[$doc->id] = $this->convert($doc->act_price - $doc->price);
            }
        }

        //return view('requestuttp.kuitansi_pdf',compact('row', 'terbilang', 'terbilangSPUH'));
        return view('requestuttp.kuitansi_pdf',
                compact(['row', 'staffes', 'docs', 'terbilang','terbilangSPUH','terbilangActSPUH']));
    }

    private function convert($number)
    {
        //$number = str_replace('.', '', $number);
        //if ( ! is_numeric($number)) throw new Exception("Please input number.");
        $number = floatval($number);
        $base    = array('nol', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
        $numeric = array('1000000000000000', '1000000000000', '1000000000000', 1000000000, 1000000, 1000, 100, 10, 1);
        $unit    = array('kuadriliun', 'triliun', 'biliun', 'milyar', 'juta', 'ribu', 'ratus', 'puluh', '');
        $str     = null;
        $i = 0;
        if ($number == 0) {
            $str = 'nol';
        } else {
            while ($number != 0) {
                $count = (int)($number / $numeric[$i]);
                if ($count >= 10) {
                    $str .= static::convert($count) . ' ' . $unit[$i] . ' ';
                } elseif ($count > 0 && $count < 10) {
                    $str .= $base[$count] . ' ' . $unit[$i] . ' ';
                }
                $number -= $numeric[$i] * $count;
                $i++;
            }
            $str = preg_replace('/satu puluh (\w+)/i', '\1 belas', $str);
            $str = preg_replace('/satu (ribu|ratus|puluh|belas)/', 'se\1', $str);
            $str = preg_replace('/\s{2,}/', ' ', trim($str));
        }
        return $str;
    }

    public function buktiorder($id)
    {
        $row = ServiceRequestUttp::find($id);
        return view('requestuttp.order_pdf',compact('row'));
    }

    public function payment($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $serviceRequest = ServiceRequestUttp::find($id);

        //dd([$serviceRequest->billing_code, $serviceRequest->status_id]);
        if ($serviceRequest->billing_code != null && $serviceRequest->status_id == 6) {
            $denda_ke = $serviceRequest->denda_ke != null ? $serviceRequest->denda_ke : 0;
            $denda_ke = (int)($denda_ke + 1);
            $serviceRequest->denda_ke = $denda_ke;
            $serviceRequest->denda_inv_price = ((1 + 0.02) ** $denda_ke) * $serviceRequest->total_price;
        }

        if ($serviceRequest->lokasi_pengujian == 'luar') {
            if ($serviceRequest->spuh_no == null) {
                $no_parts = explode("-", $serviceRequest->no_register);
                $serviceRequest->spuh_no = sprintf("%04d",$no_parts[1]).'/SPUH/'.date("m/Y");
            }
            if ($serviceRequest->spuh_billing_date == null) {
                $serviceRequest->spuh_billing_date = date("Y-m-d");
            }
        }

        //dd([$serviceRequest->denda_inv_price, $serviceRequest->denda_ke, $serviceRequest->total_price]);
        return view('requestuttp.payment',compact('id','serviceRequest', "attribute"));
    }

    public function paymentsave(Request $request)
    {
        $rules = [];
        $serviceRequest = ServiceRequestUttp::find($request->id);
        $nextStatus = 6;
        

        if ($serviceRequest->total_price > 0) {
            $rules = [
                //"payment_date"=>'required',
                //"payment_code"=>'required'
                "billing_code"=>'required',
                "billing_to_date"=>'required',
            ];
        } else {
            if ($serviceRequest->lokasi_pengujian == 'dalam') {
                $nextStatus = 7;
            }
        }

        $validation = Validator::make($request->all(),$rules);

        if ($validation->passes())
        {
            ServiceRequestUttp::whereId($request->id)
            ->update([
                //"payment_date"=>$request->payment_date,
                //"payment_code"=>$request->payment_code,
                "billing_code"=>$request->billing_code,
                "billing_to_date"=>date("Y-m-d", strtotime($request->billing_to_date)),
                "status_id"=> $nextStatus,
                //"spuh_no" => $request->spuh_no,
                //"spuh_billing_date"=>date("Y-m-d"),
                "denda_ke" => $request->denda_ke,
                "denda_inv_price" => (float)$request->denda_inv_price,
                //"payment_status_id" => $nextPaymentStatus
            ]);

            ServiceRequestUttpItem::where("request_id", $request->id)
            ->update([
                "status_id"=> $nextStatus,
            ]);

            $listItems = ServiceRequestUttpItem::where("request_id", $request->id)->pluck('id');
            ServiceRequestUttpItemInspection::whereIn("request_item_id", $listItems)->update([
                "status_id"=> $nextStatus,
            ]);

            if ($serviceRequest->lokasi_pengujian == 'luar') {
                $doc = ServiceRequestUttpInsituDoc::find($serviceRequest->spuh_doc_id);
                $doc->billing_date = date("Y-m-d");
                $doc->save();
            }

            $svcRequest = ServiceRequestUttp::find($request->id);

            $items = ServiceRequestUttpItem::where("request_id", $request->id)->get();
            foreach($items as $item) {
                $history = new HistoryUttp();
                $history->request_status_id = $nextStatus;
                $history->request_id = $request->id;
                $history->request_item_id = $item->id;
                $history->user_id = Auth::id();
                $history->save();
            }

            $customerEmail = $svcRequest->requestor->email;
            //Mail::to($customerEmail)->send(new Invoice($svcRequest));
            ProcessEmailJob::dispatch($customerEmail, new Invoice($svcRequest))->onQueue('emails');

            return response([true,"success"]);
        }
        else
        {
            return response([false,$validation->messages()]);
        }
    }

    public function valid($id)
    {
        $attribute = $this->MyProjects->setup("request");
        $request = ServiceRequestUttp::find($id);

        if ($request->status_id != 7) {
            return Redirect::route('requestuttp');
        }
        return view('requestuttp.valid',compact('id',"request","attribute"));
    }

    public function validsave(Request $request)
    {
        $req = ServiceRequestUttp::find($request->id);

        if ($req->status_id != 7) {
            return response([false,"Permohonan sudah diproses validasi sebelumnya. Silakah refresh halaman."]);
        }

        if ($req->lokasi_pengujian == 'dalam') {
            $next = 8; // pengiriman alat
        } else {
            $next = 11; // keberangkatan
        }
        ServiceRequestUttp::whereId($request->id)
        ->update([
            "status_id"=>$next,
        ]);

        /*
        ServiceRequestUttpItem::where("request_id", $request->id)
        ->update([
            "status_id"=>$next,
        ]);
        */

        $listItems = ServiceRequestUttpItem::where("request_id", $request->id)->pluck('id');
        ServiceRequestUttpItemInspection::whereIn("request_item_id", $listItems)->update([
            "status_id"=>$next,
        ]);

        $items = ServiceRequestUttpItem::where("request_id", $request->id)
            ->orderBy('id', 'asc')
            ->get();

        $serviceType = MasterServiceType::find($req->service_type_id);

        $no_order = $serviceType->last_order_no;
        $prefix = $serviceType->prefix;

        $alat = count($items);
        $noorder_num = intval($no_order)+1;
        $noorder = $prefix.'-'.date("y-").sprintf("%04d",$noorder_num)."-";
        $nokuitansi = $prefix.'-'.date("y-").sprintf("%04d",$noorder_num);

        ServiceRequestUttp::whereId($request->id)
        ->update([
            "status_id"=>$next,
            "no_order"=>$nokuitansi,
        ]);
        
        $no = 0;
        foreach($items as $item) {
            
            $no = $no + 1;
            $noorder_alat = $noorder.sprintf("%03d",$no);

            ServiceOrderUttps::insert([
                "location"              => $item->location,

                "laboratory_id"         => $item->uttp->type->instalasi->lab_id,
                "instalasi_id"          => $item->uttp->type->instalasi_id,
                "service_request_id"    => $item->request_id,
                "service_request_item_id"=> $item->id,
                //"service_request_item_inspection_id"=>$inspection->id,
                //"lab_staff"=>Auth::id(),
                //"staff_entry_datein"=>date("Y-m-d")
                'tool_type_id'			=> $item->uttp->type_id,
                'tool_serial_no'		=> $item->uttp->serial_no,
                'tool_brand'			=> $item->uttp->tool_brand,
                'tool_model'			=> $item->uttp->tool_model,
                'tool_type'				=> $item->uttp->tool_type,
                'tool_capacity'			=> $item->uttp->tool_capacity,
                'tool_capacity_unit'	=> $item->uttp->tool_capacity_unit,
                'tool_factory'			=> $item->uttp->tool_factory,
                'tool_factory_address'	=> $item->uttp->tool_factory_address,
                'tool_made_in'			=> $item->uttp->tool_made_in,
                'tool_made_in_id'		=> $item->uttp->tool_made_in_id,
                'tool_owner_id'			=> $item->uttp->owner_id,
                'tool_media'			=> $item->uttp->tool_media,
                'tool_name'				=> $item->uttp->tool_name,
                'tool_capacity_min'		=> $item->uttp->tool_capacity_min,

                'uttp_id'               => $item->uttp_id,
            ]);

            ServiceRequestUttpItem::where("id", $item->id)
            ->update([
                "status_id"=>$next,
                "no_order"=>$noorder_alat,
                "order_at"=> $req->lokasi_pengujian == 'dalam' ? date("Y-m-d H:i:s") : null,
            ]);

            $history = new HistoryUttp();
            $history->request_status_id = $next;
            $history->request_id = $request->id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();
        }

        $serviceType->last_order_no = $noorder_num;
        $serviceType->save();

        $svcRequest = ServiceRequestUttp::find($request->id);

        $customerEmail = $svcRequest->requestor->email;
        //Mail::to($customerEmail)->send(new Receipt($svcRequest));
        ProcessEmailJob::dispatch($customerEmail, new Receipt($svcRequest))->onQueue('emails');

        return response([true,"success"]);
    }

    public function novalidsave(Request $request)
    {
        $req = ServiceRequestUttp::find($request->id);
        if ($req->status_id != 7) {
            return response([false,"Permohonan sudah diproses validasi sebelumnya. Silakah refresh halaman."]);
        }

        ServiceRequestUttp::whereId($request->id)
        ->update([
            "status_id"=>6,
            "payment_date"=>null,
        ]);

        ServiceRequestUttpItem::where("request_id", $request->id)
        ->update([
            "status_id"=>6,
        ]);

        $listItems = ServiceRequestUttpItem::where("request_id", $request->id)->pluck('id');
        ServiceRequestUttpItemInspection::whereIn("request_item_id", $listItems)->update([
            "status_id"=>6,
        ]);

        return response([true,"success"]);
    }

    public function tag($id)
    {
        //$row = ServiceRequestUttpItem::find($id);
        $row = ServiceOrderUttps::find($id);
        $item = ServiceRequestUttpItem::find($row->ServiceRequestItem->id);

        if ($item->cetak_tag_created == null) {
            $item->cetak_tag_created = date('Y-m-d H:i:s');     
        } 
        $item->cetak_tag_updated = date('Y-m-d H:i:s');
        $item->save();

        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo'),
            'defaultMediaType' => 'print',
        ])
        ->setPaper([0, 0, 99.21, 198.43], 'landscape')
        //->setPaper('a4', 'portrait')
        ->loadview('requestuttp.tag',compact('row'));

        return $pdf->stream();

        //return view('requestuttp.tag',compact('row'));
    }

    public function instalasi(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        $item = ServiceRequestUttpItem::where("id", $request->id)->first();

        if ($item != null && $item->cetak_tag_created != null) {
            $item->status_id = 9;
            $item->delivered_at = date("Y-m-d H:i:s");
            $item->save();

            $history = new HistoryUttp();
            $history->request_status_id = 9;
            $history->request_id = $request->id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();
            
            ServiceRequestUttpItemInspection::where("request_item_id", $item->id)->update([
                "status_id"=>9,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 9 then 1 else 0 end) count_9, count(id) count_all')
                ->where('request_id', $item->request_id)->get();

            if ($items_count[0]->count_9 == $items_count[0]->count_all) {
                ServiceRequestUttp::find($item->request_id)->update(['status_id'=>9]);
            }

            $response["status"] = true;
            $response["messages"] = "Alat telah dikirim";
        }

        return response($response);
    }

    public function instalasi_old(Request $request)
    {
        $item = ServiceRequestUttpItem::where("id", $request->id)->first();

        if ($item->cetak_tag_created != null) {
            $item->status_id = 9;
            $item->delivered_at = date("Y-m-d H:i:s");
            $item->save();

            $history = new HistoryUttp();
            $history->request_status_id = 9;
            $history->request_id = $request->id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();
            
            ServiceRequestUttpItemInspection::where("request_item_id", $item->id)->update([
                "status_id"=>9,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 9 then 1 else 0 end) count_9, count(id) count_all')
                ->where('request_id', $item->request_id)->get();

            if ($items_count[0]->count_9 == $items_count[0]->count_all) {
                ServiceRequestUttp::find($item->request_id)->update(['status_id'=>9]);
            }
        }

        return Redirect::route('requestuttp');
    }

    public function instalasiqr(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';

        $item = ServiceRequestUttpItem::where("no_order", $request->no_order)->first();
        
        if ($item->cetak_tag_created != null) {
            $item->status_id = 9;
            $item->delivered_at = date("Y-m-d H:i:s");
            $item->save();

            $history = new HistoryUttp();
            $history->request_status_id = 9;
            $history->request_id = $request->id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();
            
            ServiceRequestUttpItemInspection::where("request_item_id", $item->id)->update([
                "status_id"=>9,
            ]);

            $items_count = DB::table('service_request_uttp_items')
                ->selectRaw('sum(case when status_id = 9 then 1 else 0 end) count_9, count(id) count_all')
                ->where('request_id', $item->request_id)->get();

            if ($items_count[0]->count_9 == $items_count[0]->count_all) {
                ServiceRequestUttp::find($item->request_id)->update(['status_id'=>9]);
            }

            $response["status"] = true;
            $response["messages"] = "Alat telah dikirim";
        }

        return response($response);
    }

    public function paymentcancel($id)
    {
        ServiceRequest::whereId($id)
        ->where("stat_service_request",1)
        ->update([
            "payment_date"=>null,
            "payment_code"=>null,
            "stat_service_request"=>0
        ]);

        return Redirect::route('request');
    }

    public function destroy($id)
    {
        $state = $request = ServiceRequestUttp::find($id);
        $request->update(
            [
                'status_id' => 21
            ]
        );


        $historyModel = new HistoryUttp();
        $itemModel = new ServiceRequestUttpItem();
        $items = $itemModel->where('request_id', $id)->get();
        foreach($items as $item) {
            $itemId =$item->id;
            $item->update(['status_id' => 21]);

            $history = [
                'request_status_id' => 21,
                'request_id' => $id,
                'request_item_id' => $itemId,
                'user_customer_id' => session('user_id'),
            ];
            $state = $historyModel->save($history);
        }

        if($state ==true){
            $response["status"] = true;
            $response["messages"] = "Data berhasil Dihapus.!";
        }else{
            $response["status"] = false;
            $response["messages"] = "Data gagal Dihapus.!";
        }
        return $response;

        // return Redirect::route('requestuttp')->with('success','Data berhasil di hapus');
    }

    function edititem($id, Request $request)
    {
        $response["status"] = false;

        $uttpModel = new Uttp();
        $uttp = $uttpModel->find($request->input('uttp_id'));

        if ($uttp == null) {
            $response["messages"] = "Data tidak ditemukan";
            return response($response);
        }

        $rules['tool_brand'] = ['required'];
        $rules['serial_no'] = ['required'];
        $rules['tool_brand'] = ['required'];
        $rules['tool_model'] = ['required'];
        $rules['tool_capacity'] = ['required'];
        $rules['tool_capacity_unit'] = ['required'];
        $rules['tool_factory'] = ['required'];

        $validation = Validator::make($request->all(), $rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $negara = DB::table('master_negara')->find($request->input('tool_made_in_id'));

            //$uttp->type_id = $request-> input('type_id');
            $uttp->serial_no = $request->input('serial_no');
            $uttp->tool_brand = $request->input('tool_brand');
            $uttp->tool_model = $request->input('tool_model');
            //$uttp->tool_type = $request-> input('tool_type');
            $uttp->tool_capacity = $request->input('tool_capacity');
            $uttp->tool_capacity_unit = $request->input('tool_capacity_unit');
            $uttp->tool_factory = $request->input('tool_factory');
            $uttp->tool_factory_address = $request->input('tool_factory_address');
            //$uttp->tool_made_in = $request-> input('tool_made_in');
            $uttp->tool_made_in = $negara->nama_negara;
            $uttp->tool_made_in_id =  $negara->id;
            //$uttp->owner_id = $request->input('owner_id');
            $uttp->tool_media = $request->input('tool_media');
            //$uttp->tool_name = $request->input('tool_name');
            $uttp->tool_capacity_min = $request->input('tool_capacity_min');
        
            if ($uttp->save()) {
                $response["id"] = $uttp->id;
                $response["status"] = true;
                $response["messages"] = "Data berhasil disimpan";
            } 
            $response["messages"] = "Data gagal disimpan";
        }

        return response($response);
    }

    public function createbookinginspection($id)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $item = $this->ServiceRequestUttpItem->find($id);
        if ($item->serviceRequest->service_type_id == 6 || $item->serviceRequest->service_type_id == 7) {
            /*
            $others_pemeriksaan = DB::table('uttp_inspection_prices')
                ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->leftJoin('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->leftJoin('service_request_uttp_items', 'service_request_uttp_items.id', '=', 'service_request_uttp_item_inspections.request_item_id')
                ->leftJoin('service_order_uttps', 'service_order_uttps.service_request_item_id', '=', 'service_request_uttp_items.id')
                ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id)
                ->where('uttp_inspection_prices.is_pemeriksaan', true)
                ->where('service_request_uttp_items.uttp_id', $item->uttp_id)
                ->where('service_request_uttp_items.id', '<>', $item->id)
                ->get();
            */

            //dd([$item->serviceRequest->service_type_id, $item->uttp->type_id, $item->uttp_id, $item->id, $others_pemeriksaan]);

            //$ada_pemeriksaan = count($others_pemeriksaan) > 0;

            $prices = DB::table('uttp_inspection_prices')
                ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id)
                ->whereNull('uttp_inspection_prices.deleted_at')
                ->whereNull('uttp_inspection_price_types.deleted_at')
                ->select('uttp_inspection_prices.*');

            /*
            if ($ada_pemeriksaan) {
                $prices = $prices->where(function($query) {
                    $query->where('uttp_inspection_prices.is_pemeriksaan', false)
                          ->orWhereNull('uttp_inspection_prices.is_pemeriksaan');
                });
            } else {
                $prices = $prices->where('uttp_inspection_prices.is_pemeriksaan', true);
            }
            */

            $prices = $prices->get();
            //dd([$item->serviceRequest->service_type_id, $item->uttp->type_id, $prices]);
            
            //dd([$others_pemeriksaan, $ada_pemeriksaan, $item->serviceRequest->service_type_id, $item->uttp->type_id, $prices]);
            //dd($prices);
        } else {
            $prices = DB::table('uttp_inspection_prices')
                ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
                ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
                ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id) 
                ->whereNull('uttp_inspection_prices.deleted_at')
                ->whereNull('uttp_inspection_price_types.deleted_at')
                ->select('uttp_inspection_prices.*')
                ->get();
        }

        return view('requestuttp.create_booking_inspection', compact('item', 'prices', 'attribute'));
    }

    public function createbookinginspection_old($id)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $item = $this->ServiceRequestUttpItem
            ->with(['serviceRequest'])
            ->find($id);
        $prices = DB::table('uttp_inspection_prices')
            ->join('uttp_inspection_price_types', 'uttp_inspection_price_types.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->where('uttp_inspection_prices.service_type_id', $item->serviceRequest->service_type_id)
            ->where('uttp_inspection_price_types.uttp_type_id', $item->uttp->type_id) 
            ->pluck('uttp_inspection_prices.inspection_type','uttp_inspection_prices.id');

        //dd([$item->serviceRequest->service_type_id, $item->uttp->type_id]);
        //array_unshift($prices , [0 => "Pilih jenis pemeriksaan/pengujian"]);
        $prices->prepend("-- Pilih jenis pemeriksaan/pengujian --", 0);
        //dd($prices);

        return view('requestuttp.create_booking_inspection', compact('item', 'prices', 'attribute'));
    }

    public function simpanbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        ServiceRequestUttpItemInspection::where('request_item_id', $id)->delete();
        
        foreach($request->get('quantity') as $index=>$value) {
            if ($value != null) {

                $dataInspection = new ServiceRequestUttpItemInspection();
                $dataInspection->request_item_id = $id;
                $dataInspection->quantity = $value;
                $dataInspection->price = $request->get('price')[$index];
                $dataInspection->inspection_price_id = $request->get('id')[$index];

                $dataInspection->status_id = 1;
                $dataInspection->save();
            }

        }

        $item = $this->ServiceRequestUttpItem->find($id);
        
        $itemAggregate = ServiceRequestUttpItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $id)
            ->first();

        $this->ServiceRequestUttpItem->where('id', $id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = $this->ServiceRequestUttpItem
            ->selectRaw('count(service_request_uttp_item_inspections.id) quantity, coalesce(sum(service_request_uttp_item_inspections.price * service_request_uttp_item_inspections.quantity), 0) subtotal')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        $svcRequest = $this->ServiceRequestUttp
            ->selectRaw("service_request_uttps.id, service_request_uttps.received_date, service_request_uttps.received_date 
                + max((case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end)) * interval '1 day' max_estimated_date")
            ->join('service_request_uttp_items', 'service_request_uttp_items.request_id', '=', 'service_request_uttps.id')
            //->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            //->join('uttp_inspection_prices', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            //->join('master_instalasi', 'uttp_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->join('uttps', 'uttps.id', '=', 'service_request_uttp_items.uttp_id')
            ->join('master_uttp_types', 'master_uttp_types.id', '=', 'uttps.type_id')
            ->join('master_instalasi', 'master_uttp_types.instalasi_id', '=', 'master_instalasi.id')
            ->groupBy('service_request_uttps.id', 'service_request_uttps.received_date')
            ->where('service_request_uttps.id', $item->serviceRequest->id)
            ->first();

        $itemCount = $this->ServiceRequestUttpItem->where("request_id", $item->serviceRequest->id)->count();
        $request = $this->ServiceRequestUttp->find($item->serviceRequest->id);
        $no_order = $request->no_order;
        $parts = explode("-",$no_order);
        $parts[3] = sprintf("%03d",intval($itemCount));
        $no_order = implode("-", $parts);

        $origin = $reqAggregate->subtotal;
        $total = $reqAggregate->subtotal;
        if ($request->persen_potongan != null) {
            $total = $reqAggregate->subtotal * (int)$request->persen_potongan / 100;
        }

        $this->ServiceRequestUttp->where('id', $item->serviceRequest->id)->update([
            'total_price' => $total,
            'origin_price' => $reqAggregate->subtotal,
            'estimated_date' => $svcRequest ? $svcRequest->max_estimated_date : null,
            'no_order' => $no_order,
        ]);

        return Redirect::route('requestuttp.editbooking', $item->serviceRequest->id);
    }

    public function simpanbookinginspection_old($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $item = $this->ServiceRequestUttpItem
            ->with(['serviceRequest'])
            ->find($id);
        $price = DB::table('uttp_inspection_prices')
            ->where('id', $request->get('inspection_price_id'))
            ->first();

        $harga = $price->price;
        if ($price->has_range) {
            $range = DB::table('uttp_inspection_price_ranges')
                ->where('inspection_price_id', $request->get('inspection_price_id'))
                ->where('min_range', '<=', $request->get('quantity'))
                ->whereRaw('coalesce(max_range, min_range ^ 2) > ?', $request->get('quantity'))
                ->first();
            $harga = $range->price;
        }

        $dataInspection["request_item_id"] = $id;
        $dataInspection["quantity"] = $request->get('quantity');
        $dataInspection["price"] = $harga;
        $dataInspection["inspection_price_id"] = $request->get('inspection_price_id');

        $dataInspection["status_id"] = 1;

        $serviceInspectionID = ServiceRequestUttpItemInspection::insertGetId($dataInspection);

        $itemAggregate = ServiceRequestUttpItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $id)
            ->first();

        $this->ServiceRequestUttpItem->where('id', $id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = $this->ServiceRequestUttpItem
            ->selectRaw('count(service_request_uttp_item_inspections.id) quantity, coalesce(sum(service_request_uttp_item_inspections.price * service_request_uttp_item_inspections.quantity), 0) subtotal')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        $svcRequest = $this->ServiceRequestUttp
            ->selectRaw("service_request_uttps.id, service_request_uttps.received_date, service_request_uttps.received_date 
                + max((case when service_request_uttps.service_type_id in (4,5) then master_instalasi.sla_day else master_instalasi.sla_tipe_day end)) * interval '1 day' max_estimated_date")
            ->join('service_request_uttp_items', 'service_request_uttp_items.request_id', '=', 'service_request_uttps.id')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->join('uttp_inspection_prices', 'service_request_uttp_item_inspections.inspection_price_id', '=', 'uttp_inspection_prices.id')
            ->join('master_instalasi', 'uttp_inspection_prices.instalasi_id', '=', 'master_instalasi.id')
            ->groupBy('service_request_uttps.id', 'service_request_uttps.received_date')
            ->where('service_request_uttps.id', $item->serviceRequest->id)
            ->first();

        $itemCount = $this->ServiceRequestUttpItem->where("request_id", $item->serviceRequest->id)->count();
        $request = $this->ServiceRequestUttp->find($item->serviceRequest->id);
        $no_order = $request->no_order;
        $parts = explode("-",$no_order);
        $parts[3] = sprintf("%03d",intval($itemCount));
        $no_order = implode("-", $parts);

        $this->ServiceRequestUttp->where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal,
            'estimated_date' => $svcRequest->max_estimated_date,
            'no_order' => $no_order,
        ]);

        

        return Redirect::route('requestuttp.editbooking', $item->serviceRequest->id);
    }

    public function hapusbookinginspection($id, Request $request)
    {
        $attribute = $this->MyProjects->setup("requestuttp");

        $inspection = ServiceRequestUttpItemInspection::where('id', $id)->first();
        $inspection->delete();
        $item_id = $inspection->request_item_id;

        $item = $this->ServiceRequestUttpItem
            ->with(['serviceRequest'])
            ->find($item_id);
        
        $itemAggregate = ServiceRequestUttpItemInspection::selectRaw('count(id) quantity, coalesce(sum(price * quantity), 0) subtotal')
            ->where('request_item_id', $item->id)
            ->first();

        $this->ServiceRequestUttpItem->where('id', $item->id)->update([
            'quantity' => $itemAggregate->quantity,
            'subtotal' => $itemAggregate->subtotal
        ]);

        $reqAggregate = $this->ServiceRequestUttpItem
            ->selectRaw('count(service_request_uttp_item_inspections.id) quantity, coalesce(sum(service_request_uttp_item_inspections.price * service_request_uttp_item_inspections.quantity), 0) subtotal')
            ->join('service_request_uttp_item_inspections', 'service_request_uttp_item_inspections.request_item_id', '=', 'service_request_uttp_items.id')
            ->where('request_id', $item->serviceRequest->id)
            ->first();

        $this->ServiceRequestUttp->where('id', $item->serviceRequest->id)->update([
            'total_price' => $reqAggregate->subtotal
        ]);

        return Redirect::route('requestuttp.editbooking', $item->serviceRequest->id);
    }

    public function getprices($id) {
        $price = DB::table('uttp_inspection_prices')->where('id', $id)->first();
        $results['price'] = $price;

        if ($price->has_range) {
            $ranges = DB::table('uttp_inspection_price_ranges')->where('inspection_price_id', $id)->get();
			$results['ranges'] = $ranges;
		}

        return response($results);
    }

    public function getowners(Request $request) {
        $owners = DB::table('uttp_owners');
        if ($request->has('q')){
            $owners = $owners->where('nama', 'like', '%'.$request->input('q').'%');
        }

        return response()->json($owners->get());
    }

    public function getowner($id) {
        $owner = DB::table('uttp_owners')->where('id', $id)->first();

        return response()->json($owner);
    }

    public function gethistory($id) {
        $orders = ServiceOrderUttps::whereHas(
            'ServiceRequestItem', function($query) use($id)
            {
                $query->where("uttp_id",$id);  
            })
            ->has('ServiceRequestItem.inspections')
            ->get();
        $rows = [];
        foreach($orders as $order) {
            $row = [
                "no_register" => $order->ServiceRequest->no_register,
                "no_order" => $order->ServiceRequestItem->no_order,
                "no_sertifikat" => $order->no_sertifikat,
                "staff_entry_datein" => $order->staff_entry_datein,
                "staff_entry_dateout" => $order->staff_entry_dateout,
            ];
            $inspections = [];
            foreach($order->ServiceRequestItem->inspections as $inspection) {
                $inspections[]= $inspection->inspectionPrice->inspection_type;
            }
            $row["inspections"] = implode(",", $inspections);
            $rows[] = $row;
        }
        return response()->json($rows);
    }

    public function getunits($uttp_type_id) {
        $units = DB::table('master_uttp_units')
            ->where('uttp_type_id', $uttp_type_id);

        return response()->json($units->get());
    }

    public function setPotongan($id, $disc, Request $r)
    {
        //dd($r);
        $total =explode(',',$r->get('total'));
        $origin = explode(',',$r->get('origin_price'));
        $total = str_replace('.','',$total[0]); 
        $origin = str_replace('.','',$origin[0]);

        $sr = ServiceRequestUttp::find($id);
        if ($sr != null) {
            $origin = $sr->origin_price;
            $total = $sr->total_price;
        }
        //dd([$origin,$total]);
        if ($r->has('disc') && $r->get('disc') != null) {
            $total = $origin * (int)$r->get('disc') / 100;
        } else {
            $total = $origin;
        }
        //dd([$origin, $total,$r]);
        $st = ServiceRequestUttp::whereId($id)
        ->update([
            "persen_potongan"=>$r->get('disc'),
            "total_price" => $total,
            "origin_price" => $origin,
        ]);

        if($st){
            return response([true,"success"]); }
        else{return response([false,"error"]);}
    }
}
