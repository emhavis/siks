<?php

namespace App\Http\Controllers;

use App\MasterRoles;
use App\MyClass\MyProjects;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
// use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    private $MasterRoles;
    private $MyProjects;

    public function __construct() 
    {
        $this->MasterRoles = new MasterRoles();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("role");
        if($attribute==null) return userlogout();

        $roles = $this->MasterRoles->get();
        return view('role.index', compact('roles','attribute'));
    }

    public function create($id=null) 
    {
        $attribute = $this->MyProjects->setup("role");
        if($attribute==null) return userlogout();

        $row = null;

        if($id)
        {
            $row = $this->MasterRoles->whereId($id)->first();
        }

        return view('role.create',compact('row','id','attribute'));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["role_name"] = ['required',Rule::unique('master_roles')->ignore($request->id)];

        $validation = Validator::make($request->all(),$rules,error_messages());
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->MasterRoles->whereId($id)->update($request->all());
            }
            else
            {
                $request["is_active"] = "t";
                $request["created_at"] = date("Y-m-d H:i:s");
                $this->MasterRoles->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->MasterRoles->find($request->id);

        if(count($row->MasterUsers)>0)
        {
            $response["message"] = "Role sudah terpakai di User List";
        }
        else
        {
            if($request->action=="delete")
            {
                $row->delete();
                $response["status"] = true;
            }
        }

        return response($response);
    }
}
