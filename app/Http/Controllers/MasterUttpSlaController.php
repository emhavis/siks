<?php

namespace App\Http\Controllers;

use App\MasterUsers;
use App\MasterRoles;
use App\MasterLaboratory;
use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\MasterKabupatenKota;
use App\MasterServiceType;
use App\MasterUttpSla;

class MasterUttpSlaController extends Controller
{
    private $MyProjects;
    private $MasterUttpSla;
    private $MasterServiceType;
    public function __construct()
    {
        $this->MasterUttpSla = new MasterUttpSla();
        $this->MyProjects = new MyProjects();
        $this->MasterServiceType = new MasterServiceType();
    }
    public function index(){
        // \DB::enableQueryLog();
        $data = MasterUttpSla :: join('master_service_types','master_service_types.id','=','service_type_id')
        ->get(['master_uttp_sla.*','master_service_types.service_type as service']);
        $attribute = $this->MyProjects->setup("uttpowners");
        return View('masteruttpsla.index',compact('data','attribute'));
    }

    public function create($id = null){
        $attribute = $this->MyProjects->setup("uttpsla");

        $row = null;

        if($id)
        {
            $row = $this->MasterInstalasi->whereId($id)->first();
        }

        $services = $this->MasterServiceType->pluck('service_type', 'id');

        return View('masteruttpsla.create',compact('row','id','attribute','services'));
    }

    public function store(Request $request)
    {
        $response["kelompok"] = false;
        $rules["jenis_pengujian"] = ['required'];
        $rules["sla"] = ['required'];
        $rules["service_type_id"] = ['required', 'integer', 'min:0'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->MasterServiceType->whereId($id)->update($request->all());
            }
            else
            {
                $this->MasterUttpSla->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return redirect()->route('uttpsla')->with($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->UttpInspectionItem->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
    public function edit($id)
    {
        $services = $this->MasterServiceType->pluck('service_type', 'id');
        $attribute = $this->MyProjects->setup("uttpsla");
        $row  = $this->MasterUttpSla->find($id);
        return view('masteruttpsla.edit', compact(
            'row','attribute',
            'services'
        ));
    }   
    public function update(Request $request)
    {
        $response["status"] = false;

        $response["kelompok"] = false;
        $rules["jenis_pengujian"] = ['required'];
        $rules["sla"] = ['required'];
        $rules["service_type_id"] = ['required', 'integer', 'min:0'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $id = $request->id;

            $request->request->remove('id');
            $this->MasterUttpSla->whereId($id)->update(
                [
                "kelompok" => $request['kelompok'],
                "jenis_pengujian" => $request['jenis_pengujian'],
                "sla" => $request['sla'],
                "service_type_id" => $request['service_type_id']
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return redirect()->route('uttpsla')->with($response);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->MasterUttpSla::whereId($id)
        ->first();

        if($row)
        {
            $this->MasterUttpSla::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('uttpsla')->with($response);
    }

}