<?php

namespace App\Http\Controllers;

use App\MasterUsers;
use App\MasterRoles;
use App\MasterLaboratory;
use App\MyClass\MyProjects;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use App\MasterUttpType;
use App\MasterInstalasi;

use Illuminate\Support\Facades\DB;

class MasterUttpTypeController extends Controller
{
    private $MyProjects;
    private $MasterUttpType;
    public function __construct()
    {
        $this->MasterUttpType = new MasterUttpType();
        $this->MasterInstalasi = new MasterInstalasi();
        $this->MyProjects = new MyProjects();
    }
    public function index(){
        // \DB::enableQueryLog();
        $data = MasterUttpType :: get();
        $attribute = $this->MyProjects->setup("uttptype");
        return View('masteruttptype.index',compact('data','attribute'));
    }

    public function create($id = null){
        $attribute = $this->MyProjects->setup("uttptype");

        $row = null;

        $kajiUlangs = [];
        if($id)
        {
            $row = $this->MasterUttpType->whereId($id)->first();
            $kajiUlangs = DB::table('master_uttp_type_kajiulang')
                ->join('master_service_types', 'master_service_types.id', '=', 'master_uttp_type_kajiulang.service_type_id')
                ->select('master_uttp_type_kajiulang.*', 'master_service_types.service_type')
                ->where('master_uttp_type_kajiulang.uttp_type_id', $id)
                ->orderBy('service_type_id')
                ->get();
        }

        return View('masteruttptype.create',compact([
            'row','id','attribute','kajiUlangs',
        ]));
    }

    public function store(Request $request)
    {
        $response["status"] = false;
        $rules["uttp_type"] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                $id = $request->id;
                unset($request["id"]);
                $this->MasterServiceType->whereId($id)->update($request->all());
            }
            else
            {
                $this->MasterUttpType->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return redirect()->route('uttptype')->with($response);
    }

    public function action(Request $request)
    {
        $response["status"] = false;
        
        $row = $this->UttpInspectionItem->find($request->id);

        if($request->action=="delete")
        {
            $row->delete();
            $response["status"] = true;
        }

        return response($response);
    }
    public function edit($id)
    {
        $attribute = $this->MyProjects->setup("uttptype");
        $row  = $this->MasterUttpType->find($id);
        $instalasi = $this->MasterInstalasi->pluck('nama_instalasi','id');
        $kajiUlangs = DB::table('master_uttp_type_kajiulang')
                ->join('master_service_types', 'master_service_types.id', '=', 'master_uttp_type_kajiulang.service_type_id')
                ->select('master_uttp_type_kajiulang.*', 'master_service_types.service_type')
                ->where('master_uttp_type_kajiulang.uttp_type_id', $id)
                ->orderBy('service_type_id')
                ->get();
        return view('masteruttptype.edit', compact([
            'row','attribute','kajiUlangs','instalasi',
        ]));
    }   
    public function update(Request $request)
    {
        $response["status"] = false;

        $rules["uttp_type"] = ['required'];

        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            $id = $request->id;

            $request->request->remove('id');
            $this->MasterUttpType->whereId($id)->update(
                [
                "uttp_type" => $request['uttp_type'],
            ]);

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }

        return redirect()->route('uttptype')->with($response);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $row = $this->MasterUttpType::whereId($id)
        ->first();

        if($row)
        {
            $this->MasterUttpType::find($id)->delete();
            $response["status"] = true;
            $response["messages"] = "Data berhasil dihapus";
        }
        
        return redirect()->route('uttptype')->with($response);
    }


    public function saveKajiUlang(Request $request)
    {
        $response["status"] = false;
        $rules["uttp_type_id"] = ['required'];
        $rules["service_type_id"] = ['required'];
        
        $validation = Validator::make($request->all(),$rules);
        $response["messages"] = $validation->messages();

        if ($validation->passes())
        {
            unset($request["_token"]);
            if($request->has("id"))
            {
                DB::table('master_uttp_type_kajiulang')
                    ->where('id', $request->get('id'))
                    ->where('service_type_id', $request->get('service_type_id'))
                    ->where('uttp_type_id', $request->get('uttp_type_id'))
                    ->update(['kaji_ulang' => $request->get('kaji_ulang')]);
            }
            else
            {
                dd($request);
                //$request["is_active"] = "t";
                //$request["created_at"] = date("Y-m-d H:i:s");
                //$this->MasterInstalasi->insert($request->all());
            }

            $response["status"] = true;
            $response["messages"] = "Data berhasil disimpan";
        }
        return response($response);
    }
}