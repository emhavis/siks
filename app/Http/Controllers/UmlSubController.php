<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Repositories\UmlSubRepository;

use App\MasterSubUml;

class UmlSubController extends Controller
{
    protected $MasterSubUml;

    public function __construct()
    {
        $this->MasterSubUml = new MasterSubUml();
    }

    public function index()
    {

    }

    public function dropdown(Request $request)
    {
        $subumlfordropdown = array();

        if(intval($request->id)>0)
        {
            $subumlfordropdown = $this->MasterSubUml
            ->leftJoin("uml","uml.id","=","uml_sub.uml_id")
            ->leftJoin("profil_uml","profil_uml.id","=","uml.profil_uml_id")
            ->orderBy('id')
            ->where('uml_id', $request->id)
            ->select('uml_sub_name', 'uml_sub.id',"kode_daerah","no_sub_uml")
            ->get();
        }

        return $subumlfordropdown;
    }

}
