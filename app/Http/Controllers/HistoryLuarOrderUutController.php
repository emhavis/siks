<?php

namespace App\Http\Controllers;

use App\MassaDrift;
use App\MassaInterpolasiBarometer;
use App\MassaInterpolasiSuhu;
use App\MassaInterpolasiTermohygro;
use App\MassaJenis;
use App\MassaKonvensional;
use App\MassaMasscomp;
use App\MassaMPE;
use App\MassaSerialnumber;
use App\MasterDocNumber;
use Illuminate\Http\Request;

use App\ServiceRequestItem;
use App\MasterStandardUkuran;
use App\MasterTemplate;
use App\ServiceOrderUttps;
use App\ServiceOrderUttpInspections;
use App\MasterMetodeUji;
use App\MasterLaboratory;
use App\MasterNegara;
use App\MasterStudentTable;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\MasterUsers;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Mail\CertificateDone;
use App\Mail\Cancel;
use App\ServiceOrders;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ProcessEmailJob;

use PDF;

class HistoryLuarOrderUutController extends Controller
{
    // protected $ServiceRequestItem;
    // protected $ServiceOrders;
    protected $MasterMetodeUji;
    protected $MasterStandardUkuran;
    protected $MyProjects;
    // protected $MasterDocNumber;
    // protected $MasterTemplate;
    // protected $MasterLaboratory;
    protected $MassaMasscomp;
    protected $MassaJenis;
    protected $MassaSerialnumber;
    // protected $MasterStudentTable;
    // protected $MassaInterpolasiSuhu;
    // protected $MassaInterpolasiBarometer;
    // protected $MassaInterpolasiTermohygro;
    protected $MasterNegara;

    public function __construct()
    {
        // $this->ServiceRequestItem = new ServiceRequestItem();
        // $this->ServiceOrders = new ServiceOrders();
        $this->MasterMetodeUji = new MasterMetodeUji();
        $this->MasterStandardUkuran = new MasterStandardUkuran();
        // $this->MasterDocNumber = new MasterDocNumber();
        // $this->MasterTemplate = new MasterTemplate();
        // $this->MasterLaboratory = new MasterLaboratory();
        $this->MassaMasscomp = new MassaMasscomp();
        $this->MassaJenis = new MassaJenis();
        $this->MasterNegara = new MasterNegara();
        $this->MasterServiceType = new MasterServiceType();
        // $this->MasterStudentTable = new MasterStudentTable();
        // $this->MassaInterpolasiSuhu = new MassaInterpolasiSuhu();
        // $this->MassaInterpolasiBarometer = new MassaInterpolasiBarometer();
        // $this->MassaInterpolasiTermohygro = new MassaInterpolasiTermohygro();
        $this->MassaSerialnumber = new MassaSerialnumber();
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("historyluaruut");

        $laboratory_id = Auth::user()->laboratory_id;
        $approval_before = Auth::user()->user_role == 3 || Auth::user()->user_role == 9 ? 0 : 2;
        
        $rows = ServiceOrders:: Join('service_requests', 'service_orders.service_request_id', 'service_requests.id')
        ->where('service_requests.lokasi_pengujian','luar')
        ->select('service_orders.*', 'service_requests.no_order')
        // with(['ServiceRequest' => function ($query){
        //     $query->where('lokasi_pengujian','=','luar');
        // }
        // ])
        ->whereIn("stat_service_order",[0,1,2,3,4])
        ->orderBy('service_orders.id','DESC')->get();

        /*
        if (Auth::user()->user_role == 3) {
            $rows = $rows->where('laboratory_id', $laboratory_id)
                ->whereIn("service_request_uttps.service_type_id", [6,7]);
        }
        if (Auth::user()->user_role == 9) {
            $rows = $rows->whereIn("service_request_uttps.service_type_id", [4,5]);
        }
        */

        // $rows = $rows ->orderBy('service_orders.id','DESC')->toSql();
        // dd($rows[0]);
        // foreach($rows as $row){
        //     dd($row->no_order);
        //     }

        return view('historyluarorderuut.index',compact('rows','attribute'));
    }

    
    public function preview($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.type',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);

        $blade = 'serviceuttp.skhp_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_pdf_ttu';
        }

        $view = true;
        return view($blade,compact(
            'order', 'view'
        ));
    }

    public function print($id)
    {
        $order = ServiceOrders::with([
            'ServiceRequest', 
            'ServiceRequestItem', 'ServiceRequestItem.uuts', 'ServiceRequestItem.uuts.stdtype',
            'MasterUsers', 'LabStaffOut',
            'persyaratanTeknis', 
            'inspections' => function ($query)
            {
                $query->orderBy('id', 'asc');
            },
        ])->find($id);
        
        $file_name = 'skhp'.$order->ServiceRequest->no_order;

        $blade = 'serviceuttp.skhp_pdf';
        if ($order->ServiceRequest->service_type_id == 4 || 
            $order->ServiceRequest->service_type_id == 5) {
                $blade = 'serviceuttp.skhp_pdf_ttu';
        }

        $view = false;
        $pdf = PDF::setOptions([
            'isHtml5ParserEnabled' => true, 
            'isRemoteEnabled' => true,
            'tempDir' => public_path(),
            'chroot'  => public_path('assets/images/logo')
            ])->loadview($blade,compact(['order', 'view']));

        /*
        $file_name = 'skhp'.$order->id;
        $pdf = PDF::loadview('serviceuttp.skhp_pdf',compact('order',));
        */

        return $pdf->download('skhp.pdf');
    }

    public function downloadLampiran($id)
    {
        $order = ServiceOrders::find($id);

        if($order->path_lampiran_kalab != null && $order->file_lampiran_kalab !=null){
            return Storage::disk('public')->download($order->path_lampiran, $order->file_lampiran);
        }else{
            abort(404);
        }
    }

}
