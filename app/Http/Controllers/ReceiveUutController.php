<?php 

namespace App\Http\Controllers;

use App\HistoryUut;
use Illuminate\Http\Request;
use App\MyClass\MyProjects;
use App\ServiceRequest;
use App\ServiceRequestItem;
use App\ServiceRequestItemInspection;
use App\ServiceOrders;
use App\MasterLaboratory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class ReceiveUutController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("receiveuut");

        $laboratory_id = Auth::user()->laboratory_id;
        $laboratories=[1,2];
        if($laboratory_id ==1 || $laboratory_id ==12 || $laboratory_id ==13 || $laboratory_id ==14){
            $laboratories =[1,12,13,14];
        }
        elseif($laboratory_id ==2 || $laboratory_id ==18 || $laboratory_id ==19)
        {
            $laboratories = [2,18,19];
        }
        elseif($laboratory_id ==6 || $laboratory_id ==20)
        {
            $laboratories = [6,20,21,22];
        }
        elseif($laboratory_id ==3 || $laboratory_id ==15 || $laboratory_id ==16 || $laboratory_id == 17){
            $laboratories = [3,15,16,17];
        }else{
            $laboratories =[$laboratory_id];
        }
        $labs = MasterLaboratory::whereIn("id",$laboratories)->get();

        $username = Auth::user()->username;
        foreach($labs as $lab){
            $rows[$lab->id] = ServiceOrders::whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id", 9);
            })
            ->whereHas('MasterLaboratory', function($query)use($laboratory_id,$lab)
            {
                $query->where("id",$lab->id);
            }) 
            ->with([
                'ServiceRequestItem' => function ($query)
                {
                    $query->orderBy('delivered_at', 'asc');
                }
            ])
            ->get();
            // dd($rows[0]->ServiceRequestItem->uuts->stdType->lab);
        }

        return view('receiveuut.index',compact('rows','attribute','laboratory_id','labs'));
    }    

    public function proses(Request $request)
    {
        $response["status"] = false;
        $response["messages"] = 'Alat tidak ditemukan';
        
        if ($request->id > 0) {
            $item = ServiceRequestItem::where("id", $request->id)->first();
            // dd($item);
        } else {
            $item = ServiceRequestItem::where("no_order", $request->no_order)->first();
        }

        if ($item != null) {
            $item->status_id = 10;
            $item->received_at = date("Y-m-d H:i:s");
            $item->save();
            
            $history = new HistoryUut();
            $history->request_status_id = 12;
            $history->request_id = $item->request_id;
            $history->request_item_id = $item->id;
            $history->user_id = Auth::id();
            $history->save();

            $datas = ServiceRequestItemInspection::where("service_request_item_id", $item->id)->get();

            // $can_foreach = is_array($datas) || is_object($datas);
            // if($can_foreach){
                
            //     foreach($datas as $data){
            //         $dat = ServiceRequestItemInspection::where("id", $data->id)->get();
            //         if($dat[0]->id !=10){
            //             ServiceRequestItemInspection::where("id", $data->id)
            //                 ->update(
            //                 ['status_id' => DB::raw("case when status_id ='10' then 10 else 10 end") 
            //             ]);
            //         }
                    
            //     }

            // }else{
            //     ServiceRequestItemInspection::where("service_request_item_id", $item->id)->update([
            //         "status_id"=>10,
            //     ]);

            // }

            $items_count = DB::table('service_request_items')
                ->selectRaw('sum(case when status_id = 10 then 1 else 0 end) count_10, count(id) count_all')
                ->where('service_request_id', $item->service_request_id)->get();
            if ($items_count[0]->count_10 == $items_count[0]->count_all) {
            //    dd(ServiceRequest::find($item->service_request_id));
                ServiceRequest::find($item->service_request_id)->update(['status_id'=>DB::raw("case when status_id ='10' then 10 else 10 end") ]);
            }

            $response["status"] = true;
            $response["messages"] = "Alat telah diterima";
        }

        return Redirect::route('receiveuut');
        // return response($response);
    }

    public function takeorder($id)
    {
        $attribute = $this->MyProjects->setup("takeorder");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uttp'])
            ->find($id);

        return view('requestuut.edit_booking', compact('request', 'attribute'));
    }
}
