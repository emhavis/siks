<?php 

namespace App\Http\Controllers;

use App\HistoryUut;
use App\MasterLaboratory;
use Illuminate\Http\Request;
use App\MyClass\MyProjects;
use App\ServiceOrders;
use App\ServiceRequest;
use App\ServiceRequestItem;
use App\MasterStandardType;
use App\ServiceRequestItemInspection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class OrderUutController extends Controller
{
    protected $MyProjects;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
    }

    public function index()
    {
        $attribute = $this->MyProjects->setup("orderuut");
        $laboratory_id = Auth::user()->laboratory_id;
        $laboratories=[1,2];
        if($laboratory_id ==1 || $laboratory_id ==12 || $laboratory_id ==13 || $laboratory_id ==14){
            $laboratories =[1,12,13,14];
        }
        elseif($laboratory_id ==2 || $laboratory_id ==18 || $laboratory_id ==19)
        {
            $laboratories = [2,18,19];

        }
        elseif($laboratory_id ==3 || $laboratory_id ==15 || $laboratory_id ==16 || $laboratory_id ==17){
            $laboratories = [3,15,16,17];
        }
        elseif($laboratory_id == 6 || $laboratory_id == 20 || $laboratory_id == 21 || $laboratory_id == 22){
            $laboratories = [6,20,21,22];
        }
        else{
            $laboratories =[$laboratory_id];
        }

        $instalasiList = MasterLaboratory::where('id', $laboratory_id)->orderBy('id','asc')
            ->get();
        $labs = MasterLaboratory::whereIn("id",$laboratories)->get();

        foreach($labs as $lab){
            $rows[$lab->id] = ServiceOrders ::join('service_request_items', 'service_request_items.id', '=', 'service_orders.service_request_item_id' )
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id", 10);
            })
            ->whereHas('MasterLaboratory', function($query)use($laboratory_id,$lab)
            {
                $query->where("id",$lab->id);
            }) 
            ->whereNull('test_by_1')
            ->orderBy('service_request_items.order_at', 'asc')
            ->orderBy('service_request_items.received_at', 'asc')
            ->orderBy('service_request_items.delivered_at', 'asc')
            ->with([
                'ServiceRequestItem' => function ($query)
                {
                    $query
                        ->orderBy('order_at', 'asc')
                        ->orderBy('received_at', 'asc')
                        ->orderBy('delivered_at', 'asc')
                        ->orderBy('id', 'asc');
                }
            ])->get();
        }
        return view('orderuut.index',compact('rows','attribute','laboratory_id','labs'));
    }    

    // public function proses($id)
    public function proses(Request $request)
    {
        $response['status'] = false;
        $response['messages'] = 'Alat tidak ditemukan';

        $laboratory_id = $request->labid;
        
        $order = ServiceOrders ::join('service_request_items', 'service_request_items.id', '=', 'service_orders.service_request_item_id' )
            ->whereHas('ServiceRequestItem', function($query) 
            {
                $query->where("status_id", 10);
            })
            ->whereHas('MasterLaboratory', function($query)use($laboratory_id)
            {
                $query->where("id",$laboratory_id);
            }) 
            ->whereNull('test_by_1')
            ->orderBy('service_request_items.order_at', 'asc')
            ->orderBy('service_request_items.received_at', 'asc')
            ->orderBy('service_request_items.delivered_at', 'asc')
            ->with([
                'ServiceRequestItem' => function ($query)
                {
                    $query->orderBy('order_at', 'asc')
                            ->orderBy('received_at', 'asc')
                            ->orderBy('delivered_at', 'asc')
                            ->orderBy('id', 'asc');
                }
            ])->first();

        if ($request->has('id') && $request->has('requestid')) {
            $item = ServiceRequestItem::where("id",$request->id)->first();
        } else {
            $item = ServiceRequestItem::where("no_order", $request->no_order)->first();
        }

        if ($item == null) {
            return response($response);
        }

        // if ($item->id != $rows->ServiceRequestItem->id) {
        //     $response["messages"] = 'Alat bukan pada urutan pertama';
        //     return response($response);
        // }

        //$item = ServiceRequestUttpItem::where("id", $request->id)->first();
        $item->status_id = 12;
        $s = $item->save();

        if (!$s) {
            $response["messages"] = 'Alat gagal disimpan';
            return response($response);
        }

        $history = new HistoryUut();
        $history->request_status_id = 12;
        $history->request_id = $item->request_id;
        $history->request_item_id = $item->id;
        $history->user_id = Auth::id();
        $history->save();
        
        $stat = ServiceRequestItemInspection::where("service_request_item_id", $item->id);
        ServiceRequestItemInspection::where("service_request_item_id", $item->id)->update([
            "status_id"=>12,
        ]);

        $items_count = DB::table('service_request_items')
            ->selectRaw('sum(case when status_id = 12 then 1 else 0 end) count_12, count(id) count_all')
            ->where('service_request_id', $item->service_request_id)->get();
        if ($items_count[0]->count_12 == $items_count[0]->count_all) {
            ServiceRequest::find($item->service_request_id)->update(['status_id'=>12]);
        }

        ServiceOrders::where('service_request_item_id', $item->id)
        ->update([
          
            "lab_staff"=>Auth::id(),
            "staff_entry_datein"=>date("Y-m-d")
        ]);
        // untuk pembbuatan expired date s erftifikat
        $order = ServiceOrders::where('service_request_item_id',$request->id)->first();
        // dd($order);
        $tipe_id = $order->ServiceRequestItem->uuts->type_id;
        $standard = MasterStandardType::where('id',$tipe_id)->first();
        $sertifikat_expired ='';

        if($order->sertifikat_expired_at != null){
            $sertifikat_expired = $order->sertifikat_expired_at;
        }else{
            $sertifikat_expired =date("Y-m-d", strtotime(date('Y-m-d') .'+'.$standard->jangka_waktu." year"));
        }

        ServiceOrders::where('service_request_item_id', $request->id)
        ->update([
          
            "sertifikat_expired_at"=>$sertifikat_expired,
            "updated_at"=>date("Y-m-d")
        ]);

        $response["status"] = true;
        $response["messages"] = "Alat telah dikerjakan";
        
        return response($response);
        
    }
        

    public function takeorder($id)
    {
        $attribute = $this->MyProjects->setup("takeorder");

        $request = ServiceRequest::with(['items', 'items.inspections', 'items.uuts'])
            ->find($id);

        return view('requestuut.edit_booking', compact('request', 'attribute'));
    }

    public function getinspections($id)
    {
        $item = ServiceRequestItem::find($id);
        
        $inspection_types = [];
        foreach($item->inspections as $itemInspection) {
            $inspection_types[] = $itemInspection->inspectionPrice->inspection_type;
        }

        $response["inspections"] = $inspection_types;

        return response($response);
    }
}
