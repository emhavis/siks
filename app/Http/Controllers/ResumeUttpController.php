<?php 

namespace App\Http\Controllers;

use App\Excel\Exports\UsersExport;
use App\Excel\Exports\UttpsExport;
use App\Excel\Imports\UsersImport;
use App\MasterInstalasi;
use Illuminate\Http\Request;
use App\ServiceRequestUttpItemInspection;
use App\ServiceRequestUttpItem;
use App\ServiceOrderUttps;
use App\MyClass\MyProjects;
use App\ServiceRequestUttp;
use App\MasterServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ResumeUttpController extends Controller
{
    protected $MyProjects;
    protected $s_date,$str_date,$ins;
    protected $e_date;
    protected $MasterInstalasi;

    public function __construct()
    {
        $this->MyProjects = new MyProjects();
        $this->MasterInstalasi = new MasterInstalasi();
        $this->MasterServiceType = new MasterServiceType();
    }

    public function index(Request $request)
    {
        $this->str_date = $request->get('start_date'); 
        $this->e_date = $request->get('end_date');
        $attribute = $this->MyProjects->setup("resumeuttp");
        $instalasi = $this->MasterInstalasi->pluck('nama_instalasi','id');
        $types = $this->MasterServiceType
            ->where('id', '>', 3)
            ->orderBy('id')->pluck('service_type', 'id');
        /*
        $rows = ServiceOrderUttps::whereRaw('1=1')
            ->join('master_instalasi', 'master_instalasi.id', '=', 'service_order_uttps.instalasi_id')
            ->select()
            ->addSelect(
                DB::raw(
                    'case 
                    when
                        service_order_uttps.kabalai_date is null 
                            then \'ONP\'
                    when 
                        service_order_uttps.kabalai_date - service_order_uttps.staff_entry_datein <= master_instalasi.sla_day
                            then \'ONT\'
                    else \'LATE\'
                    end as sla_status'));
        */
        $rows = ServiceOrderUttps::whereRaw('1=1')
        ->join('service_request_uttps as sru','service_order_uttps.service_request_id','=','sru.id')
        ->join('service_request_uttp_items as srui','service_order_uttps.service_request_item_id','=','srui.id')
        ->join('service_request_uttp_item_inspections as sruii','srui.id','=','sruii.request_item_id')
        ->join('users_customers as uc','sru.requestor_id','=','uc.id')
        ->join('uttps as u','service_order_uttps.uttp_id','=','u.id')
        ->join('master_uttp_types as mut','u.type_id','=','mut.id')
        ->join('uttp_inspection_prices as uip','sruii.inspection_price_id','=','uip.id')
        ->join('master_instalasi as mi','service_order_uttps.instalasi_id','=','mi.id')
        ->select('srui.id')
        ->addSelect(
            DB::raw(
                'srui.no_order, uc.full_name, sru.label_sertifikat, (
                    select
                        count(s_srui.id)
                    from
                        service_request_uttp_items s_srui
                    where
                        s_srui.id = sru.id) as jumlah_alat,
                    mut.uttp_type as rincian_alat,
                    uip.inspection_type as obyek_pengujian,
                    sruii.price * sruii.quantity as tarif_pnbp,
                    sru.total_price as total_pnbp,
                    sru.billing_code,
                    sru.payment_code,
                    sru.no_order as no_kuitansi,
                    service_order_uttps.no_sertifikat, service_order_uttps.no_sertifikat_tipe, service_order_uttps.no_surat_tipe,
                    sru.received_date,
                    kabalai_date,
                    service_order_uttps.staff_entry_dateout,
                    srui.order_at::date order_at,
                case 
                when
                    service_order_uttps.kabalai_date is null 
                        then \'ONP\'
                when 
                    DATE_PART(\'day\', service_order_uttps.kabalai_date::timestamp - srui.order_at::timestamp)
                        - (select count(*) from generate_series(srui.order_at::timestamp, service_order_uttps.kabalai_date::timestamp, \'1 day\'::interval) s
                            where extract(isodow from s) in (6, 7)) 
                        - (select count(id) from holidays h 
                            where holiday_date between srui.order_at::timestamp and service_order_uttps.kabalai_date::timestamp)  
                        <= (case when sru.service_type_id in (4,5) then mi.sla_day else mi.sla_tipe_day end)
                        then \'ONT\'
                else \'LATE\'
                end as sla_status,  mi.nama_instalasi'))
                ->orderBy('service_order_uttps.id');

        $start = null;
        $end = null;
        $ins = null;
        $lokasi = null;
        $layanan = null;
        $status = null;

        if ($request->get('start_date') != null && $request->get('end_date') != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
       
            $rows = $rows->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($request->get('start_date') != null && $request->get('end_date') == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            $rows = $rows->where('staff_entry_datein', '>=', $start);
        } else if ($request->get('start_date') == null && $request->get('end_date') != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            $rows = $rows->where('staff_entry_datein', '<=', $end);
        }
        if ($request->get('instalasi') != null) {
            $ins = $request->get('instalasi');
            $rows = $rows->where('service_order_uttps.instalasi_id', '=', $ins);
        }
        if ($request->get('lokasi_pengujian') != null) {
            $lokasi = $request->get('lokasi_pengujian');
            $rows = $rows->where('sru.lokasi_pengujian', '=', $lokasi);
        }
        if ($request->get('jenis_layanan') != null) {
            $layanan = $request->get('jenis_layanan');
            $rows = $rows->where('sru.service_type_id', '=', $layanan);
        }
        if ($request->get('status_sla') != null) {
            $status = $request->get('status_sla');
            if ($status == 'ONP') {
                $rows = $rows->whereNull('service_order_uttps.kabalai_date');
            } elseif ($status == 'ONT') {
                $rows = $rows->whereRaw('DATE_PART(\'day\', service_order_uttps.kabalai_date::timestamp - srui.order_at::timestamp)
                - (select count(*) from generate_series(srui.order_at::timestamp, service_order_uttps.kabalai_date::timestamp, \'1 day\'::interval) s
                    where extract(isodow from s) in (6, 7)) 
                - (select count(id) from holidays h 
                    where holiday_date between srui.order_at::timestamp and service_order_uttps.kabalai_date::timestamp)     
                    <= (case when sru.service_type_id in (4,5) then mi.sla_day else mi.sla_tipe_day end)');
            } elseif ($status == 'Late') {
                $rows = $rows->whereNotNull('service_order_uttps.kabalai_date') 
                    ->whereRaw('DATE_PART(\'day\', service_order_uttps.kabalai_date::timestamp - srui.order_at::timestamp)
                    - (select count(*) from generate_series(srui.order_at::timestamp, service_order_uttps.kabalai_date::timestamp, \'1 day\'::interval) s
                        where extract(isodow from s) in (6, 7)) 
                    - (select count(id) from holidays h 
                        where holiday_date between srui.order_at::timestamp and service_order_uttps.kabalai_date::timestamp)  
                    > (case when sru.service_type_id in (4,5) then mi.sla_day else mi.sla_tipe_day end)');
            }
        }

        $rows = $rows->get();
        //dd($rows->toSql());
        //dd($rows);
        $this->data = $rows;
        return view('resumeuttp.index',compact(['rows','attribute', 'start', 'end',
            'ins','instalasi', 
            'lokasi', 'layanan', 'status',
            'types']));
    }

    public function fileImportExport()
    {
       return view('file-import');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileImport(Request $request) 
    {
        Excel::import(new UsersImport, $request->file('file')->store('temp'));
        return back();
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function fileExport(Request $request) 
    {
        // $request = new \Illuminate\Http\Request();
        $this->s_date = $request->get('start_date'); 
        $this->e_date = $request->get('end_date');
        $this->ins = $request->get('instalasi');
        
        $rows = ServiceOrderUttps::whereRaw('1=1')
        ->join('service_request_uttps as sru','service_order_uttps.service_request_id','=','sru.id')
        ->join('service_request_uttp_items as srui','service_order_uttps.service_request_item_id','=','srui.id')
        ->join('service_request_uttp_item_inspections as sruii','srui.id','=','sruii.request_item_id')
        ->join('users_customers as uc','sru.requestor_id','=','uc.id')
        ->join('uttps as u','service_order_uttps.uttp_id','=','u.id')
        ->join('master_uttp_types as mut','u.type_id','=','mut.id')
        ->join('uttp_inspection_prices as uip','sruii.inspection_price_id','=','uip.id')
        ->join('master_instalasi as mi','service_order_uttps.instalasi_id','=','mi.id')
        ->select('srui.id')
        ->addSelect(
            DB::raw(
                'srui.no_order, uc.full_name, sru.label_sertifikat, (
                    select
                        count(s_srui.id)
                    from
                        service_request_uttp_items s_srui
                    where
                        s_srui.id = sru.id) as jumlah_alat,
                    mut.uttp_type as rincian_alat,
                    uip.inspection_type as obyek_pengujian,
                    sruii.price * sruii.quantity as tarif_pnbp,
                    sru.billing_code::varchar as billing_code ,
                    sru.payment_code::varchar as payment_code,
                    sru.no_order::varchar as no_kuitansi,
                    service_order_uttps.no_sertifikat, service_order_uttps.no_sertifikat_tipe, service_order_uttps.no_surat_tipe,
                    sru.received_date,
                    kabalai_date,
                    service_order_uttps.staff_entry_dateout,
                    srui.order_at::date order_at,
                case 
                when
                    service_order_uttps.kabalai_date is null 
                        then \'Dalam Proses\'
                when 
                    service_order_uttps.kabalai_date is not null and 
                    DATE_PART(\'day\', service_order_uttps.kabalai_date::timestamp - srui.order_at::timestamp)
                        - (select count(*) from generate_series(srui.order_at::timestamp, service_order_uttps.kabalai_date::timestamp, \'1 day\'::interval) s
                            where extract(isodow from s) in (6, 7)) 
                        - (select count(id) from holidays h 
                            where holiday_date between srui.order_at::timestamp and service_order_uttps.kabalai_date::timestamp)   
                        <= (case when sru.service_type_id in (4,5) then mi.sla_day else mi.sla_tipe_day end)
                        then \'Baik\'
                else \'Terlambat\'
                end as sla_status,  mi.nama_instalasi'))
        ->orderBy('srui.no_order');

        $start = null;
        $end = null;
        $ins = null;
        $lokasi = null;
        $layanan = null;
        $status = null;

        if ($this->s_date != null && $this->e_date != null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
        
            $rows = $rows->whereBetween('staff_entry_datein', [$start, $end]);
        } else if ($this->s_date != null && $this->e_date == null) {
            $start = date("Y-m-d",  strtotime($request->get('start_date')));
            
            $rows = $rows->where('staff_entry_datein', '>=', $start);
        } else if ($this->s_date == null && $this->e_date != null) {
            $end = date("Y-m-d",  strtotime($request->get('end_date')));
            
            $rows = $rows->where('staff_entry_datein', '<=', $end);
        }
        if ($this->ins != null) {
            $ins = $this->ins;
            $rows = $rows->where('service_order_uttps.instalasi_id', '=', $ins);
        }
        if ($request->get('lokasi_pengujian') != null) {
            $lokasi = $request->get('lokasi_pengujian');
            $rows = $rows->where('sru.lokasi_pengujian', '=', $lokasi);
        }
        if ($request->get('jenis_layanan') != null) {
            $layanan = $request->get('jenis_layanan');
            $rows = $rows->where('sru.service_type_id', '=', $layanan);
        }
        if ($request->get('status_sla') != null) {
            $status = $request->get('status_sla');
            if ($status == 'ONP') {
                $rows = $rows->whereNull('service_order_uttps.kabalai_date');
            } elseif ($status == 'ONT') {
                $rows = $rows->whereRaw('DATE_PART(\'day\', service_order_uttps.kabalai_date::timestamp - srui.order_at::timestamp)
                - (select count(*) from generate_series(srui.order_at::timestamp, service_order_uttps.kabalai_date::timestamp, \'1 day\'::interval) s
                    where extract(isodow from s) in (6, 7)) 
                - (select count(id) from holidays h 
                    where holiday_date between srui.order_at::timestamp and service_order_uttps.kabalai_date::timestamp)  
                    <= (case when sru.service_type_id in (4,5) then mi.sla_day else mi.sla_tipe_day end)');
            } elseif ($status == 'Late') {
                $rows = $rows->whereNotNull('service_order_uttps.kabalai_date') 
                    ->whereRaw('DATE_PART(\'day\', service_order_uttps.kabalai_date::timestamp - srui.order_at::timestamp)
                    - (select count(*) from generate_series(srui.order_at::timestamp, service_order_uttps.kabalai_date::timestamp, \'1 day\'::interval) s
                        where extract(isodow from s) in (6, 7)) 
                    - (select count(id) from holidays h 
                        where holiday_date between srui.order_at::timestamp and service_order_uttps.kabalai_date::timestamp)   
                    > (case when sru.service_type_id in (4,5) then mi.sla_day else mi.sla_tipe_day end)');
            }
        }

        
        $rows = $rows->get();
        $this->data = $rows;
        //DD($rows->toSql());
        return Excel::download(new UsersExport($this->data),'Resume-UTTP.xlsx');
    }    

}