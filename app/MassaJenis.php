<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassaJenis extends Model
{
  protected $table = 'massa_jenis';
  protected $primaryKey = 'id';

	public function dropdown()
	{
    $rows = $this->get();

    $data[] = "-- Pilih --";
    foreach($rows as $row)
    {
      $data[$row->id] = "[".$row->id."] ".$row->material.", ".$row->massajenis;
    }

    return $data;
	}

}
