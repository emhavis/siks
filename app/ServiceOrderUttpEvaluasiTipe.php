<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttpEvaluasiTipe extends Model
{
    protected $table = 'service_order_uttp_tipe';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'no_surat_tipe',
        'daya_baca', 'kelas_keakurasian', 'interval_skala_verifikasi', 'konstanta',
        'kelas_single_axle_load', 'kelas_single_group_load', 'metode_pengukuran', 'sistem_jaringan',
        'meter_daya_baca',
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
