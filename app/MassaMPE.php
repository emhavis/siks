<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassaMPE extends Model
{
  protected $table = 'massa_mpe_cmc';
  protected $primaryKey = 'id';
}
