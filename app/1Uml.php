<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uml extends Model
{
    protected $table = 'uml';
	public $timestamps = false;
	protected $guarded = array('id');
	protected $primaryKey = 'id';

    protected $fillable = [
        'uml_name', 
        'address', 
        'phone_no', 
        'kabupatenKota_id'
    ];

    public static $rules = array(
        'uml_name' => 'required',
        'kabupatenKota_id' => 'required',
    );

    public function kabupatenkota()
    {
        return $this->belongsTo('App\KabupatenKota');
    }

}
