<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterMetodeUji extends Model
{
    protected $table = 'master_metodeuji';
	protected $primaryKey = 'id';   

	public function dropdown()
	{
		return $this->orderBy('id')->pluck('nama_metode', 'id');
	} 
}
