<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUutTTUInspection extends Model
{
    protected $table = 'service_order_uut_ttu_inspection';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "tank_no",
        "tag_no",
        "totalisator",
        "kfactor"
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
