<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterKabupatenKota extends Model
{
  protected $table = 'master_kabupatenkota';
  protected $primaryKey = 'id';

  public function MasterProvinsi(){
    return $this->BelongsTo('App\MasterProvince','provinsi_id');
}
}
