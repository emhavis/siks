<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterSbm extends Model
{
    protected $table = 'sbm';
    protected $primaryKey = 'id';

    public $timestamps = false;
    
    public function provinsi()
    {
        return $this->belongsTo('App\MasterProvince','provinsi_id');
    }

}
