<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestUttpInsituDocCheck extends Model 
{
    protected $table = 'service_request_uttp_insitu_doc_check';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'doc_id', 'check_item_id', 'order_no', 'is_accepted'
    ];

    public function doc()
    {
        return $this->belongsTo('App\ServiceRequestUttpInsituDoc',"doc_id");
    }

    public function checkItem()
    {
        return $this->belongsTo('App\UttpInsituCheckItem',"check_item_id");
    }
}