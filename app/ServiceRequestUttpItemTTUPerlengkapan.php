<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestUttpItemTTUPerlengkapan extends Model
{
    protected $table = 'service_request_uttp_item_ttu_perlengkapan';
    protected $primaryKey = 'id';
    protected $fillable = ['request_item_id', 'uttp_id'];

    public function item()
    {
        return $this->belongsTo('App\ServiceRequestUttpItem','request_item_id');
    }

    public function uttp()
    {
        return $this->belongsTo('App\Uttp','uttp_id');
    }

    
}
