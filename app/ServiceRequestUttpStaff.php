<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestUttpStaff extends Model 
{
    protected $table = 'service_request_uttp_staff';
    protected $primaryKey = 'id';

    protected $fillable = [
        'request_id', 'scheduled_id', 
    ];

    public function scheduledStaff()
    {
        return $this->belongsTo('App\MasterPetugasUttp','scheduled_id');
    } 

    
}