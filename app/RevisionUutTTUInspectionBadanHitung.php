<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevisionUutTTUInspectionBadanHitung extends Model 
{
    protected $table = 'cert_rev_uut_ttu_badanhitung';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        "order_id",
        "brand",
        "type",
        "serial_no",
    ];

    public function revision()
    {
        return $this->belongsTo('App\RevisionUut',"cert_rev_id");
    }
}