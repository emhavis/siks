<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryUttp extends Model 
{
    protected $table = 'history_uttp';
    protected $primaryKey = 'id';

    protected $fillable = [
        'created_at', 'request_status_id', 'order_status_id', 'user_id', 'user_customer_id',
        'request_id', 'request_item_id', 'order_id', 'warehouse_status_id',
        'payment_status_id', 'spuh_payment_status_id'
    ];

    public function request()
    {
        return $this->belongsTo('App\ServiceRequestUttp','request_id');
    }

    public function item()
    {
        return $this->belongsTo('App\ServiceRequestUttpItem','request_item_id');
    }

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps','order_id');
    }

    public function status()
    {
        return $this->belongsTo('App\MasterStatus','status_id');
    } 

    public function user()
    {
        return $this->belongsTo('App\MasterUsers',"user_id");
    }

    public function customer()
    {
        return $this->belongsTo('App\UserCustomer',"user_customer_id");
    }
}