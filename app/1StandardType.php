<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StandardType extends Model
{
    protected $table = 'standard_types';
	public $timestamps = false;
	protected $guarded = array('id');
	protected $primaryKey = 'id';

    protected $fillable = [
        'standard_type'
    ];

    public static $rules = array(
        'standard_type' => 'required'
    );
}
