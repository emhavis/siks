<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttpTTUInspection extends Model
{
    protected $table = 'service_order_uttp_ttu_inspection';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "tank_no",
        "tag_no",
        "totalisator",
        "kfactor",
        "suhu_pengujian",
        "line_bore_size",
        "ketidakpastian",
        "jenis_atap",
        "tinggi_tangki",
        "tanggal_wet_cal",
        "volume_bersih",
        "kapal",
        "panjang", "lebar", "kedalaman",
        "user",
        "operator",
        "no_compartments",
        "tank_vol_table_no",
        "tank_vol_table_date",
        "pembuat_tabel",
        "catatan_kapasitas",
        "jumlah_tangki",
        "diameter",
        "expired_wet_cal",
        "jaringan_listrik",
        "konstanta",
        "kelas",
        "kondisi_ruangan",
        "trapo_ukur",
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
