<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UutInspectionItem extends Model
{
    protected $table = 'uut_inspection_items';
	protected $primaryKey = 'id';


    public function MasterTemplate(){
        return $this->BelongsTo('App\MasterTemplate','template_id');
    }

}
