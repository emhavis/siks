<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterUml extends Model
{
    protected $table = 'master_uml';
	protected $primaryKey = 'id';

	public function dropdown()
	{
        return $this->orderBy('id')->pluck('uml_name', 'id');
	}

}
