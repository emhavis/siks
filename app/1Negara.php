<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Negara extends Model
{
    use SoftDeletes;
    protected $table = 'master_negara';
      public $timestamps = false;                         // untuk data master tidak dipakai
      protected $guarded = array('id');
      protected $primaryKey = 'id';

    protected $fillable = [
        'nama_negara',
        'ibukota_negara'
    ];
}
