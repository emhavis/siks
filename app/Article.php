<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'article_posts';
	protected $primaryKey = 'id';
    protected $fillable = [
        'title', 
        'cover_image',
        'cover_image_path',
        'content',
        'published',
        'priority_index',
        'excerpt',
        'article_type_id',
    ];

    public function type()
    {
        return $this->belongsTo('App\MasterArticleType','article_type_id');
    }
}