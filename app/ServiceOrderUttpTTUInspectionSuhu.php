<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttpTTUInspectionSuhu extends Model
{
    protected $table = 'service_order_uttp_ttu_insp_suhu';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'penunjukan', 'penunjukan_suhu', 'koreksi',
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
