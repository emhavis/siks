<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StandardMeasurementType extends Model
{
    protected $table = 'standard_measurement_types';
    protected $primaryKey = 'id';
    
    public function StandardToolType()
    {
        return $this->hasMany('App\StandardToolType',"standard_measurement_type_id");
    }

    // public function dropdown($ajax=null)
    // {
    //     $row = $this
    //     ->orderBy('id')
    //     ->pluck('standard_type', 'id');

    //     if($ajax)
    //     {
    //         return response($row);
    //     }

    //     return $row;

    // }

    public function dropdown()
    {
        return $this->orderBy('id')->pluck('standard_type', 'id');
    }

    public function laboratory()
    {
        return $this->belongsTo('App\MasterLaboratory','laboratorium_id');
    }
}
