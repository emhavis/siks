<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterRevisionStatus extends Model
{
    protected $table = 'master_revision_status';
	protected $primaryKey = 'id';
}
