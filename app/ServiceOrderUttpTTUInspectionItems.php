<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttpTTUInspectionItems extends Model
{
    protected $table = 'service_order_uttp_ttu_insp_items';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "input_level",
        "error_up",
        "error_down",
        "error_hysteresis",
        "flow_rate",
        "error",
        "meter_factor",
        "repeatability",
        "error_bkd",
        "repeatability_bkd",
        "rentang_ukur", "suhu_dasar", "panjang_sebenarnya",
        "d_inner", "d_outer", "e_inner", "e_outer", "alpha",
        "tool_brand", "tool_type", "tool_serial_no", "max_debit", "tool_media", "nozzle_count",
        "input_pct", "actual", "output_up", "output_down",

        "commodity", "penunjukan",

        "roundness", "flatness", "roughness", "eksentrisitas",

        "tank_no", "sounding", "volume",

        "tegangan", "frekuensi", "arus", "faktor_daya", "ketidaktetapan", "jenis_item",
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
