<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SurveyQuestion extends Model
{
    protected $table = 'survey_question';
	protected $primaryKey = 'id';

    protected $fillable = [
        'page_id', 'sequence', 'question', 'description', 'question_type', 'possible_answers',  
    ];

    public function page()
    {
      return $this->belongsTo('App\SurveyPage','page_id');
    }

}
