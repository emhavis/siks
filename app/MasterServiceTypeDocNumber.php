<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterServiceTypeDocNumber extends Model
{
    protected $table = 'master_service_type_docnumber';
	// public $timestamps = false;
	// protected $guarded = array('id');
	protected $primaryKey = 'id';
    protected $fillable = [
        "id",
        "service_type_id",
        "doc_type",
        "year",
        "last_no",
    ];


    public $timestamps = false;

}
