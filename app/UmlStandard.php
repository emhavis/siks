<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UmlStandard extends Model
{
    protected $table = 'uml_standards';
	protected $primaryKey = 'id';
}
