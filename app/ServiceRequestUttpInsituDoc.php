<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestUttpInsituDoc extends Model 
{
    protected $table = 'service_request_uttp_insitu_doc';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'request_id', 'doc_no', 'rate', 'rate_id', 'price', 'staffs',
        'date_from', 'date_to', 'days',
        'act_date_from', 'act_date_to', 'act_days', 'act_price',
        'billing_date', 'payment_date', 'invoiced_price',
        'act_billing_date', 'act_payment_date',
        'spuh_file', 'spuh_path',
        'spuh_no', 'spuh_year',

        'file_bukti_pemeriksaan_awal', 'path_bukti_pemeriksaan_awal',
        'checked_at', 'checked_by', 'is_accepted', 'keterangan_tidak_lengkap',

        'accepted_date', 'accepted_by_id',

        'jenis',
        'file_spt', 'path_spt', 'file_spuh', 'path_spuh', 'file_bukti_bayar', 'path_bukti_bayar',

        'is_siap_petugas', 'keterangan_tidak_siap', 'approval_note',

        'token',

        'keterangan_revisi',

        'spuh_rate_negara_id',

        'spuh_rate1', 'spuh_rate2', 'valid_bukti_bayar', 'add_days',
    ];

    public function request()
    {
        return $this->belongsTo('App\ServiceRequestUttp','request_id');
    }

    public function listOfStaffs()
    {
        return $this->hasMany('App\ServiceRequestUttpInsituStaff','doc_id');
    } 

    public function acceptedBy()
    {
        return $this->belongsTo('App\MasterUsers',"accepted_by_id");
    }
}