<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterServiceType extends Model
{
    protected $table = 'master_service_types';
	// public $timestamps = false;
	// protected $guarded = array('id');
	protected $primaryKey = 'id';
    protected $fillable = [
        "id",
        "last_no",
        "last_register_no",
        "last_order_no",
        "insitu_last_order_no",
    ];

    public function dropdown()
    {
        return $this->orderBy('id')->pluck('service_type', 'id');
    }
    public $timestamps = false;

    // protected $fillable = [
    //     'service_type'
    // ];

    // public static $rules = array(
    //     'service_type' => 'required'
    // );
}
