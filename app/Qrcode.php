<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qrcode extends Model 
{
    protected $table = 'qrcodes';
	// public $timestamps = false;
	// protected $guarded = array('id');
	protected $primaryKey = 'id';

    // protected $fillable = [
    //     'uml_id', 
    //     'standard_measurement_type_id', 
    //     'tool_code'
    // ];

    // public static $rules = array(
    //     'uml_id' => 'required',
    //     'standard_measurement_type_id' => 'required',
    //     'qrcode_list'=>'required'
    // );
                            // ->leftJoin("uml","uml.id","=","qrcodes.uml_id")
                            // ->leftJoin("uml_sub","uml_sub.id","=","qrcodes.uml_sub_id")
                            // ->leftJoin("standard_measurement_types","standard_measurement_types.id","=","qrcodes.standard_measurement_type_id")

    public function standard_measurement_type()
    {
        return $this->belongsTo('App\StandardMeasurementType');
    }

    public function StandardMeasurementType()
    {
        return $this->belongsTo('App\StandardMeasurementType','standard_measurement_type_id');
    }

    public function MasterUml()
    {
        return $this->belongsTo('App\MasterUml','uml_id');
    }
    
    public function MasterSubUml()
    {
        return $this->belongsTo('App\MasterSubUml','uml_sub_id');
    }
}
