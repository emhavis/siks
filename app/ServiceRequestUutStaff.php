<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestUutStaff extends Model 
{
    protected $table = 'service_request_uut_staff';
    protected $primaryKey = 'id';

    protected $fillable = [
        'request_id', 'scheduled_id', 
    ];

    public function scheduledStaff()
    {
        return $this->belongsTo('App\MasterPetugasUttp','scheduled_id');
    } 

    
}