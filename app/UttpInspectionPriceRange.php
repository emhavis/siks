<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UttpInspectionPriceRange extends Model
{
    protected $table = 'uttp_inspection_price_ranges';
	protected $primaryKey = 'id';

    public function inspectionprice()
    {
        return $this->belongsTo('App\UttpInspectionPriceRange','inspection_price_id');
    }
}
