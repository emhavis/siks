<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uttp extends Model
{
    protected $table = 'uttps';
	protected $primaryKey = 'id';
    protected $fillable = [
        'owner_id',
        'type_id',
        'serial_no', 
        'tool_brand',
        'tool_model',
        'tool_type',
        'tool_made_in',
        'tool_capacity',
        'tool_factory',
        'tool_factory_address',
        'tool_made_in_id',
        'tool_capacity_unit',
        'tool_media',
        'tool_capacity_min',

        'location_lat', 'location_long', 'location',
    ];

    public function type()
    {
        return $this->belongsTo('App\MasterUttpType','type_id');
    }

    public function owner()
    {
        return $this->belongsTo('App\UttpOwner','owner_id');
    }
}
