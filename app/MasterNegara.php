<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class MasterNegara extends Model
{
    protected $table = 'master_negara';
    protected $primaryKey = 'id';

    public function dropdown()
    {
        return $this->orderBy('id')->pluck('nama_negara', 'id');
    }

    public function dropdown_values()
    {
        return $this->orderBy('nama_negara')->pluck('nama_negara', 'nama_negara');
    }

}
