<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassaMasscomp extends Model
{
  protected $table = 'massa_masscomp';
  protected $primaryKey = 'id';

  public function MasterMasscomp()
  {
      return $this->belongsTo('App\MasterMasscomp','id_masscomp');
  }

	public function dropdown()
	{
    $rows = $this
    ->with("MasterMasscomp")
    ->get();

    $data[] = "-- Pilih --";
    foreach($rows as $row)
    {
      $data[$row->id] = "[".$row->id."] ".$row->MasterMasscomp->masscomp.", ".$row->nominal.$row->unit;
    }

    return $data;
	}
}
