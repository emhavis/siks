<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceBookingItem extends Model
{
    protected $table = 'service_booking_items';
	protected $primaryKey = 'id';

    protected $fillable = ['booking_id', 'uml_standard_id', 'quantity', 'est_subtotal', 'uttp_id', 
        'location', 'reference_no',
        'file_type_approval_certificate',
        'file_last_certificate',
        'file_application_letter',
        'file_calibration_manual',
        'file_manual_book',
        'path_type_approval_certificate',
        'path_last_certificate',
        'path_application_letter',
        'path_calibration_manual',
        'path_manual_book'];

    public function Standard()
    {
        return $this->belongsTo('App\Standard',"uml_standard_id");
    }

    public function inspections()
    {
        return $this->hasMany("App\ServiceBookingItemInspection", "booking_item_id");
    }

    // public function perlengkapans()
    // {
    //     return $this->hasMany("App\ServiceBookingItemTTUPerlengkapan", "booking_item_id");
    // }
}
