<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UttpInspectionItem extends Model
{
    protected $table = 'uttp_inspection_items';
	protected $primaryKey = 'id';


    public function MasterTemplate(){
        return $this->BelongsTo('App\MasterTemplate','template_id');
    }

}
