<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUutTTUInspectionItems extends Model
{
    protected $table = 'service_order_uut_ttu_insp_items';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "input_level",
        "error_up",
        "error_down",
        "error_hysteresis",
        "flow_rate",
        "error",
        "meter_factor",
        "repeatability",
        "error_bkd",
        "repeatability_bkd",
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
