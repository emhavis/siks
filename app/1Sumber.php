<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sumber extends Model
{
    protected $table = 'msumber';
	public $timestamps = false;
	protected $guarded = array('id');
	protected $primaryKey = 'id';

    protected $fillable = [
        'nama_sumber'
    ];

}
