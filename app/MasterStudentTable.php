<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterStudentTable extends Model
{
    protected $table = 'master_student_table';
    protected $primaryKey = 'id';
}
