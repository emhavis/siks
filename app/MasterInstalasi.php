<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterInstalasi extends Model
{
    protected $table = 'master_instalasi';
	protected $primaryKey = 'id';

    public function lab()
    {
        return $this->belongsTo('App\MasterLaboratory','lab_id');
    }
}
