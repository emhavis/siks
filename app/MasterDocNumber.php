<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterDocNumber extends Model
{
    protected $table = 'master_docnumber';
    protected $primaryKey = 'id';
}
