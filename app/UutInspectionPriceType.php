<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UutInspectionPriceType extends Model
{
    protected $table = 'uut_inspection_price_types';
	protected $primaryKey = 'id';
    protected $fillable = [
        'inspection_price_id',
        'uut_type_id',
    ];

    public function inspectionPrice()
    {
        return $this->belongsTo('App\UutInspectionPrice','inspection_price_id');
    }

    public function uttpType()
    {
        return $this->belongsTo('App\MasterUutType','uut_type_id');
    }
}
