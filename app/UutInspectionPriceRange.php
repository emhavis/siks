<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UutInspectionPriceRange extends Model
{
    protected $table = 'uut_inspection_price_ranges';
	protected $primaryKey = 'id';

    public function inspectionprice()
    {
        return $this->belongsTo('App\UutInspectionPriceRange','inspection_price_id');
    }
}
