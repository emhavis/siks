<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceBooking extends Model
{
    protected $table = 'service_bookings';
	protected $primaryKey = 'id';

    protected $fillable = ['pic_id', 'booking_no', 'est_total_price', 'label_sertifikat', 'stat_service_request', 
    'addr_sertifikat', 'for_sertifikat', 'est_arrival_date', 'jenis_layanan', 'created_by', 'updated_by',
    'service_type_id'];

    public function Pic()
    {
        return $this->belongsTo('App\Customer',"pic_id");
    }

    public function items()
    {
        return $this->hasMany("App\ServiceBookingItem", "booking_id");
    }
}
