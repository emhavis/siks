<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uut extends Model
{
    protected $table = 'standard_uut';
	protected $primaryKey = 'id';
    protected $fillable = [
        'serial_no', 
        'tool_brand',
        'tool_model',
        'tool_type',
        'tool_made_in',
        'tool_capacity',
        'tool_factory',
        'tool_factory_address',
        'type_id',
        'tool_capacity_min',
        'tool_capacity_min_unit',
        'jumlah',
        'class',
        'tool_dayabaca',
        'tool_dayabaca_unit',
        'tool_name',
        'owner_id',
    ];

    /*
    public function type()
    {
        return $this->belongsTo('App\MasterStandard','type_id');
    }
    */

    public function stdtype()
    {
        return $this->belongsTo('App\MasterStandardType','type_id');
    }
}
