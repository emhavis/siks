<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUttpTTUPerlengkapan extends Model
{
    protected $table = 'service_order_uttp_ttu_perlengkapan';
    protected $primaryKey = 'id';
    protected $fillable = ['order_id', 'uttp_id', 'keterangan', 'tag'];

    public function item()
    {
        return $this->belongsTo('App\ServiceOrderUttps','order_id');
    }

    public function uttp()
    {
        return $this->belongsTo('App\Uttp','uttp_id');
    }

    
}
