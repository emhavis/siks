<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevisionUutTTUInspection extends Model 
{
    protected $table = 'cert_rev_uut_ttu_inspection';
    protected $primaryKey = 'id';

    protected $fillable = [
        "cert_rev_id",
        "tank_no",
        "tag_no",
        "totalisator",
        "kfactor"
    ];

    public function revision()
    {
        return $this->belongsTo('App\RevisionUut',"cert_rev_id");
    }
}