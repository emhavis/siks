<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceBookingItemInspection extends Model
{
    protected $table = 'service_booking_item_inspections';
	protected $primaryKey = 'id';

    protected $fillable = ['booking_item_id', 'inspection_price_id', 'quantity', 'price', ];

    public function Price()
    {
        return $this->belongsTo('App\StandardInspectionPrice',"inspection_price_id");
    }
}
