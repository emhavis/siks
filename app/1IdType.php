<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterDoctypes extends Model
{
    protected $table = 'master_doctypes';
	// public $timestamps = false;
	// protected $guarded = array('id');

 //    protected $fillable = [
 //        'id_type_name'
 //    ];

 //    public static $rules = array(
 //        'id_type_name' => 'required'
 //    );
    
    public function dropdown()
    {
        return $this->orderBy('id')->pluck('id_type_name', 'id');
    }

}
