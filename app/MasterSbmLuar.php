<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterSbmLuar extends Model
{
    protected $table = 'sbm_luar';
    protected $primaryKey = 'id';

    public $timestamps = false;
    
    public function negara()
    {
        return $this->belongsTo('App\MasterNegara','negara_id');
    }

}
