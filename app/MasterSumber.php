<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterSumber extends Model
{
    protected $table = 'master_sumber';
	protected $primaryKey = 'id';

	public function dropdown()
	{
        return $this->orderBy('id')->pluck('nama_sumber', 'id');
	}

}
