<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterTemplate extends Model
{
    protected $table = 'master_template';
	protected $primaryKey = 'id';

	public function MasterLaboratory()
	{
	  return $this->belongsTo('App\MasterLaboratory','laboratory_id');
	}
}
