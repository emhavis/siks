<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Delegation extends Model
{
    protected $table = 'user_delegations';
	protected $primaryKey = 'id';

    protected $fillable = [
        'user_delegator_id', 'user_delegatee_id',
        'berlaku_mulai', 'berlaku_sampai',
        'created_at', 'updated_at', 
    ];

    public function delegator()
    {
        return $this->belongsTo('App\MasterUsers',"user_delegator_id");
    }

    public function delegatee()
    {
        return $this->belongsTo('App\MasterUsers',"user_delegatee_id");
    }
}
