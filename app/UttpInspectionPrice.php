<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UttpInspectionPrice extends Model
{
    protected $table = 'uttp_inspection_prices';
	protected $primaryKey = 'id';
    protected $guarded = ['id']; 

    public function instalasi()
    {
        return $this->belongsTo('App\MasterInstalasi','instalasi_id');
    }

    public function masterservicetipe()
    {
        return $this->belongsTo('App\MasterServiceType','service_type_id');
    }

    public function templates(){
        return $this->belongsTo('App\MasterTemplate','inspection_template_id');   
    }
}
