<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UttpOwner extends Model
{
    protected $table = 'uttp_owners';
	protected $primaryKey = 'id';
    protected $fillable = [
        'nama', 'alamat', 'email', 'kota_id', 'telepon', 'nib', 'npwp'
    ];

    public function MasterKabupatenKota(){
        return $this->BelongsTo('App\MasterKabupatenKota','kota_id');
    }
}
