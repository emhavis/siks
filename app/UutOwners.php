<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UutOwners extends Model
{
    protected $table = 'uut_owners';
	protected $primaryKey = 'id';


    public function MasterKabupatenKota(){
        return $this->BelongsTo('App\MasterKabupatenKota','kota_id');
    }

}
