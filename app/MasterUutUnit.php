<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MasterUutUnit extends Model
{
    protected $table = 'master_uut_units';
	protected $primaryKey = 'id';
    public $timestamps = false;


    public function uuttype(){
        return $this->BelongsTo('App\MasterUutType','uut_type_id');
    }

}
