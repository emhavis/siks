<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassaKonvensional extends Model
{
  protected $table = 'massa_konvensional';
  protected $primaryKey = 'id';
}
