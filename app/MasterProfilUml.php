<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterProfilUml extends Model
{
    protected $table = 'profil_uml';
	// public $timestamps = false;
	// protected $guarded = array('id');
	protected $primaryKey = 'id';

    // protected $fillable = [
    //     "nama_uml",
    //     "kode_daerah",
    //     "instansi",
    //     "alamat",
    //     "ruang_lingkup",
    //     "no_tlp",
    //     "no_fax",
    //     "email",
    //     "website",
    //     "no_skkptu",
    //     "nama_kepala_dinas",
    //     "nip_kepala_dinas",
    //     "nama_kepala_uml",
    //     "nip_kepala_uml"
    // ];

    // public static $rules = array(
    //     "nama_uml"=>"required",
    //     "kode_daerah"=>"required",
    //     "alamat"=>"required"
    // );

    public function MasterUml()
    {
        return $this->belongsTo('App\MasterUml','id','profil_uml_id');
    }
}
