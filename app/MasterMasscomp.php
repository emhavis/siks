<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterMasscomp extends Model
{
  protected $table = 'master_masscomp';
  protected $primaryKey = 'id';

  public function MassaMasscomp()
  {
      return $this->hasMany('App\MassaMasscomp','id_masscomp');
  }

}
