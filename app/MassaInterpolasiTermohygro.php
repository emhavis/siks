<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassaInterpolasiTermohygro extends Model
{
  protected $table = 'massa_interpolasi_termohygro';
  protected $primaryKey = 'id';

}
