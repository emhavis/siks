<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Survey extends Model
{
    protected $table = 'survey_survey';
	protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'active', 'thank_you_msg'  
    ];

}
