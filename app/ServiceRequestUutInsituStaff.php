<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestUutInsituStaff extends Model 
{
    protected $table = 'service_request_uut_insitu_staff';
    protected $primaryKey = 'id';

    protected $fillable = [
        'doc_id', 'scheduled_id', 
    ];

    public function scheduledStaff()
    {
        return $this->belongsTo('App\MasterPetugasUttp','scheduled_id');
    } 

    
}