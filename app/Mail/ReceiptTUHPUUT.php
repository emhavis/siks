<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceRequest;
use App\ServiceRequestUutInsituDoc;

class ReceiptTUHPUUT extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;
    public $doc;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequest $srvReq, ServiceRequestUutInsituDoc $doc)
    {
        $this->srvReq = $srvReq;
        $this->doc = $doc;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Pembayaran TUHP untuk Pendaftaran Verifikasi/Kalibrasi Nomor: '. $this->srvReq->no_register .' Telah Diterima')
            ->view('mails.receipt_tuhp');
    }
}
