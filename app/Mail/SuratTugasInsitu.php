<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;

use App\ServiceRequestUttp;
use App\MasterPetugasUttp;

class SuratTugasInsitu extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;
    public $petugas;
    public $staffes;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequestUttp $srvReq, Collection $staffes, MasterPetugasUttp $petugas = null)
    {
        $this->srvReq = $srvReq;
        $this->petugas = $petugas;
        $this->staffes = $staffes;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Surat Tugas Pengujian/Pemeriksaan Nomor: '. $this->srvReq->no_register)
            ->view('mails.surat_tugas_insitu');
    }
}
