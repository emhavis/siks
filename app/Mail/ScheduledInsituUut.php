<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceRequest;
use App\MasterPetugasUttp;

class ScheduledInsituUut extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;
    public $petugas;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequest $srvReq, MasterPetugasUttp $petugas)
    {
        $this->srvReq = $srvReq;
        $this->petugas = $petugas;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Penjadwalan Penugasan Verifikasi/Kalibrasi Nomor: '. $this->srvReq->no_register)
            ->view('mails.scheduled_insitu_uut');
    }
}
