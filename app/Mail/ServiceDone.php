<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceOrderUttps;

class ServiceDone extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceOrderUttps $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Layanan Pengujian/Pemeriksaan Nomor: '.  $this->order->ServiceRequest->no_register .' Telah Selesai Sepenuhnya')
            ->view('mails.service_done');
    }
}
