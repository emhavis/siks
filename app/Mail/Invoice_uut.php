<?php

namespace App\Mail;

use App\ServiceRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class Invoice_uut extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequest $srvReq)
    {
        $this->srvReq = $srvReq;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Invoice untuk Pendaftaran Pengujian/Pemeriksaan Nomor: '. $this->srvReq->no_register .' Telah Terbit')
            ->view('mails.invoice_uut');
    }
}
