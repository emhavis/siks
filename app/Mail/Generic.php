<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceRequestUttp;
use App\Customer;

class Generic extends Mailable
{
    use Queueable, SerializesModels;

    public $title, $customer, $body;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $title, Customer $customer, String $body)
    {
        $this->title = $title;
        $this->customer = $customer;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->title)
            ->view('mails.generic');
    }
}
