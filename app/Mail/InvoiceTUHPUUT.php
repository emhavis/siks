<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceRequest;
use App\ServiceRequestUutInsituDoc;

class InvoiceTUHPUUT extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;
    public $doc;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequest $srvReq, ServiceRequestUutInsituDoc $doc)
    {
        $this->srvReq = $srvReq;
        $this->doc = $doc;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Invoice TUHP untuk Pendaftaran Pengujian/Pemeriksaan Nomor: '. $this->srvReq->no_register .' Telah Terbit')
            ->view('mails.invoice_tuhp_uut');
    }
}
