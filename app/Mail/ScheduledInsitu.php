<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceRequestUttp;
use App\MasterPetugasUttp;

class ScheduledInsitu extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;
    public $petugas;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequestUttp $srvReq, MasterPetugasUttp $petugas)
    {
        $this->srvReq = $srvReq;
        $this->petugas = $petugas;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Penjadwalan Penugasan Pengujian/Pemeriksaan Nomor: '. $this->srvReq->no_register)
            ->view('mails.scheduled_insitu');
    }
}
