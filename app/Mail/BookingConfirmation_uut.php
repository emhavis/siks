<?php

namespace App\Mail;

use App\ServiceRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingConfirmation_uut extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequest $srvReq)
    {
        $this->srvReq = $srvReq;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Pendaftaran Pengujian/Pemeriksaan Nomor: '. $this->srvReq->no_register .' Telah Diproses')
            ->view('mails.booking_confirmation_uut');
    }
}
