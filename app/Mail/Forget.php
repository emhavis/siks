<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceRequestUttp;
use App\Customer;

class Forget extends Mailable
{
    use Queueable, SerializesModels;

    public $customer, $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, String $url)
    {
        $this->customer = $customer;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Lupa Password, Panduan Ubah Password Akun Pengguna Anda')
            ->view('mails.forget');
    }
}
