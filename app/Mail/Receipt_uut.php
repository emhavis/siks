<?php

namespace App\Mail;

use App\ServiceRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class Receipt_uut extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequest $srvReq)
    {
        $this->srvReq = $srvReq;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Kuitansi untuk Pendaftaran Pengujian/Pemerikasaan Nomor: '. $this->srvReq->no_register .' Telah Terbit')
            ->view('mails.receipt_uut');
    }
}
