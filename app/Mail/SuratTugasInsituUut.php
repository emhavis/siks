<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;

use App\ServiceRequest;
use App\MasterPetugasUttp;
use App\ServiceRequestInsituDoc;

class SuratTugasInsituUut extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;
    public $petugas;
    public $staffes;
    public $doc;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequest $srvReq, Collection $staffes, MasterPetugasUttp $petugas = null, $doc)
    {
        $this->srvReq = $srvReq;
        $this->petugas = $petugas;
        $this->staffes = $staffes;
        $this->doc = $doc;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd([$this->srvReq->Owner->nama,
        // $this->petugas,
        // $this->staffes]);
        return $this->subject('Surat Tugas Pengujian/Pemeriksaan Nomor: '. $this->srvReq->no_register)
            ->view('mails.surat_tugas_insitu_uut');
    }
}
