<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceRequestUttp;
use App\ServiceRequestUttpInsituDoc;

class ReceiptTUHP extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;
    public $doc;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequestUttp $srvReq, ServiceRequestUttpInsituDoc $doc)
    {
        $this->srvReq = $srvReq;
        $this->doc = $doc;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Pembayaran TUHP untuk Pendaftaran Pengujian/Pemerikasaan Nomor: '. $this->srvReq->no_register .' Telah Diterima')
            ->view('mails.receipt_tuhp');
    }
}
