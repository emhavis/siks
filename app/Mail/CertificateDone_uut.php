<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceOrders;

class CertificateDone_uut extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceOrders $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Sertifikat atas Order Pengujian/Pemeriksaan Nomor: '.  $this->order->ServiceRequestItem->no_order .' Telah Terbit')
            ->view('mails.certificatedone_uut');
    }
}
