<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceOrderUttps;

class CertificateDone extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceOrderUttps $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('SKHP atas Order Pengujian/Pemeriksaan Nomor: '.  $this->order->ServiceRequestItem->no_order .' Telah Terbit')
            ->view('mails.certificatedone');
    }
}
