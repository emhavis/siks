<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\ServiceRequestUttp;

class Invoice extends Mailable
{
    use Queueable, SerializesModels;

    public $srvReq;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceRequestUttp $srvReq)
    {
        $this->srvReq = $srvReq;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Invoice untuk Pendaftaran Pengujian/Pemeriksaan Nomor: '. $this->srvReq->no_register .' Telah Terbit')
            ->view('mails.invoice');
    }
}
