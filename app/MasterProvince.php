<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class MasterProvince extends Model
{
    // use SoftDeletes;
    protected $table = 'master_provinsi';
	  // public $timestamps = false;                         // untuk data master tidak dipakai
	  // protected $guarded = array('id');
	  protected $primaryKey = 'id';

    // protected $fillable = [
    //     'kode',
    //     'nama',
    //     'akronim',
    //     'iso'
    // ];
}
