<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class StandardToolType extends Model
{
    protected $table = 'standard_tool_types';
	protected $primaryKey = 'id';

    public function StandardMeasurementType()
    {
      return $this->belongsTo('App\StandardMeasurementType','standard_measurement_type_id');
    }

    public function StandardDetailType()
    {
        return $this->hasMany('App\StandardDetailType',"standard_tool_type_id");
    }

    public function dropdown($id)
    {
        $row = $this
        ->where('standard_measurement_type_id', $id)
        ->orderBy('id')
        ->pluck('attribute_name', 'id');

        return response($row);
    }

    public function measurementType(){
        return $this->belongsTo('App\StandardMeasurementType','standard_measurement_type_id');
    }

}
