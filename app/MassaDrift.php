<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassaDrift extends Model
{
  protected $table = 'massa_drift';
  protected $primaryKey = 'id';
}
