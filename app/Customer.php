<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'users_customers';
	protected $primaryKey = 'id';

}
