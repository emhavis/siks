<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUutEvaluasiTipe extends Model
{
    protected $table = 'service_order_uut_tipe';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "no_surat_tipe",
        "kelas_akurasi",
        "daya_baca",
        "interval_skala_verifikasi",
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrders',"order_id");
    }

}
