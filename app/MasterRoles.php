<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterRoles extends Model
{
    protected $table = 'master_roles';
    protected $primaryKey = 'id';

    public function MasterUsers()
    {
        return $this->hasMany('App\MasterUsers','user_role');
    }
}
