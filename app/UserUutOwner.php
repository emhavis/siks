<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserUutOwner extends Model
{
    protected $table = 'user_uut_owners';
	protected $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'owner_id'
    ];

    public function customer(){
        return $this->belongsTo('App\UserCustomer','user_id');
    }

}
