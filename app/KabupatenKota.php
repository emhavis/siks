<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KabupatenKota extends Model
{
  protected $table = 'master_kabupatenkota';
  protected $primaryKey = 'id';
}
