<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleFile extends Model
{
    protected $table = 'article_files';
	protected $primaryKey = 'id';
    protected $fillable = [
        'title', 
        'file_name',
        'path',
        'published',
        'priority_index',
        'featured',
        'type_id',
    ];

    public function type()
    {
        return $this->belongsTo('App\MasterArticleType','type_id');
    }
}