<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUutInsituLaporan extends Model 
{
    protected $table = 'service_order_uut_insitu_laporan';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    protected $fillable = [
        'service_order_id', 'service_request_id', 'service_request_item_id',

        'ringkasan', 'kendala_teknis', 'kendala_non_teknis',
        'metode_tindakan', 'saran_masukan',
    ];

    public function ServiceRequest()
    {
        return $this->belongsTo('App\ServiceRequestUut',"service_request_id");
    }

    public function ServiceRequestItem()
    {
        return $this->belongsTo('App\ServiceRequestItem',"service_request_item_id");
    }

    public function ServiceOrder()
    {
        return $this->belongsTo('App\ServiceOrders',"service_order_id");
    }

}