<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MasterUttpSla extends Model
{
    protected $table = 'master_uttp_sla';
	protected $primaryKey = 'id';
    protected $fillable = [
        'kelompok','jenis_pengujian','sla','service_type_id'
    ];

    public $timestamps = false;


    public function MasterServiceType(){
        return $this->BelongsTo('App\MasterServiceType','service_type_id');
    }

}
