<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UmlStandardRegister extends Model
{
    protected $table = 'registered_standards';
	// public $timestamps = false;
	// protected $guarded = array('id');
	protected $primaryKey = 'id';


    // public function standard()
    // {
    //     return $this->belongsTo('App\Standard');
    // }

    public function MasterUml()
    {
        return $this->belongsTo('App\MasterUml','uml_id');
    }

    public function MasterSubUml()
    {
        return $this->belongsTo('App\MasterSubUml','uml_sub_id');
    }

    public function StandardMeasurementType()
    {
        return $this->belongsTo('App\StandardMeasurementType','standard_measurement_type_id');
    }

    public function StandardToolType()
    {
        return $this->belongsTo('App\StandardToolType','standard_tool_type_id');
    }

    public function StandardDetailType()
    {
        return $this->belongsTo('App\StandardDetailType','standard_detail_type_id');
    }
}
