<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OIML extends Model
{
    protected $table = 'master_oimls';
	protected $primaryKey = 'id';
    protected $fillable = [
        'oiml_name', 
    ];
}
