<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequest extends Model 
{
    protected $table = 'service_requests';
    protected $primaryKey = 'id';

    protected $fillable = [
        'receipt_date','uml_id','pic_name','pic_phone_no','id_type_id','pic_id_no',
        'payment_date','created_at','created_by','updated_at','updated_by','deleted_at',
        'no_register','no_order','payment_code','total_price','label_sertifikat','stat_service_request',
        'addr_sertifikat','for_sertifikat','estimated_date','pic_email','standard_code','jenis_layanan',
        'booking_id','status_id','received_date','requestor_id','booking_no','uut_owner_id','service_type_id',
        'lokasi_pengujian','inspection_loc','inspection_prov_id','inspection_kabkot_id','service_request_id',
        'scheduled_test_date_from','scheduled_test_confirmstaf_at','scheduled_test_date_to','billing_code',
        'billing_to_date','cetak_tag_created','cetak_tag_updated','scheduled_test_id','spuh_no','spuh_billing_date',
        'spuh_price','spuh_rate_id','spuh_rate','spuh_staff','spuh_payment_date','payment_status_id','pending_status',
        'spuh_doc_id','spuh_spt','spuh_inv_price','an_kuitansi','file_payment_ref','path_payment_ref','denda_inv_price',
        'denda_ke','file_spuh_payment_ref','path_spuh_payment_ref','received_date',

        'payment_valid_date', 'spuh_payment_valid_date',

        'total_inspection_confirm',

        'no_surat_permohonan', 'tgl_surat_permohonan', 'file_surat_permohonan', 'path_surat_permohonan',

        'spuh_payment_status_id',

        'not_complete_notes',

        'status_revisi_spt', 'status_sertifikat','is_integritas','integritas_at',

        'keuangan_perusahaan', 'keuangan_pic', 'keuangan_jabatan', 'keuangan_hp',
    ];
    
    public function MasterUml()
    {
        return $this->belongsTo('App\MasterUml','uml_id');
    }

    public function MasterUsers()
    {
        return $this->belongsTo('App\MasterUsers',"created_by");
    }

    public function ServiceRequestItem()
    {
        return $this->hasMany('App\ServiceRequestItem','service_request_id');
    }

    public function ServiceOrder()
    {
        return $this->hasMany('App\ServiceOrders','service_request_id');
    }

    public function items()
    {
        return $this->hasMany(ServiceRequestItem::class,'service_request_id');
    }


    public function requestor()
    {
        return $this->belongsTo('App\Customer','requestor_id');
    } 

    public function status()
    {
        return $this->belongsTo('App\MasterStatus','status_id');
    } 

    public function Owner()
    {
        return $this->belongsTo('App\UttpOwner',"uut_owner_id");
    }

    public function inspectionProv() 
    {
        return $this->belongsTo('App\MasterProvince',"inspection_prov_id"); 
    }

    public function inspectionKabkot() 
    {
        return $this->belongsTo('App\MasterKabupatenKota',"inspection_kabkot_id"); 
    }

    public function booking()
    {
        return $this->belongsTo('App\ServiceBooking','booking_id');
    }

    public function svc(){
        return $this->belongsTo(MasterServiceType::class,'service_type_id');
    }

    public function spuhDoc()
    {
        return $this->belongsTo(ServiceRequestUutInsituDoc::class,'spuh_doc_id');
    } 

    public function docs()
    {
        return $this->hasMany('App\ServiceRequestUutInsituDoc', 'request_id');
    }
}