<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestItem extends Model
{
    protected $table = 'service_request_items';
    protected $primaryKey = 'id';
    protected $fillable = [
        'standard_id','uut_id','service_request_id',
        'status_id','no_order','cetak_tag_created', 'cetak_tag_updated', 
        'delivered_at', 'received_at','order_at','location',
        'file_application_letter',
        'file_last_certificate',
        'file_manual_book',
        'file_calibration_manual',
        'reference_no',
        'file_type_approval_certificate',
        'path_application_letter',
        'path_last_certificate',
        'path_manual_book',
        'path_calibration_manual',
        'path_type_approval_certificate',
        'reference_date',
        'status_id',
        'service_request_id',
        'standard_id',
        'stat_service_request_item',
        'sla_overide'
        
    ];

    
    public function ServiceRequest()
    {
        return $this->belongsTo('App\ServiceRequest','service_request_id');
    }

    public function Standard()
    {
        return $this->belongsTo('App\Standard','standard_id');
    }
    public function StandardID($query, $stdId)
    {
        return $query->where('standard_id',$stdId);
    }

    public function ServiceRequestItemInspection()
    {
        return $this->hasMany('App\ServiceRequestItemInspection','service_request_item_id');
    }
    public function inspections()
    {
        return $this->hasMany('App\ServiceRequestItemInspection','service_request_item_id');
    }
    public function uuts()
    {
        return $this->belongsTo(Uut::class,'uut_id');
    }
    public function perlengkapans()
    {
        return $this->hasMany('App\ServiceRequestUutItemTTUPerlengkapan','request_item_id');
    }

    public function status()
    {
        return $this->belongsTo('App\MasterStatus','status_id');
    } 
    
    public function provinsi()
    {
        return $this->belongsTo('App\MasterProvince','location_prov_id');
    } 

    public function kabkot()
    {
        return $this->belongsTo('App\MasterKabupatenKota','location_kabkot_id');
    } 
}
