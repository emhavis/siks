<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestUttp extends Model 
{
    protected $table = 'service_request_uttps';
    protected $primaryKey = 'id';

    protected $fillable = [
        'booking_id', 'requestor_id', 
        'pic_id', 'booking_no', 'total_price', 'label_sertifikat', 'stat_service_request', 
        'addr_sertifikat', 'for_sertifikat', 'received_date', 'jenis_layanan', 'created_by', 'updated_by',
        'service_type_id', 'lokasi_pengujian', 'estimated_date', 'status_id', 'uttp_owner_id', 'billing_code', 'billing_to_date',
        'cetak_tag_created', 'cetak_tag_updated', 
        'scheduled_test_id', 'scheduled_test_date_from', 'scheduled_test_date_to', 'scheduled_test_confirmstaf_at',
        'spuh_no', 'spuh_billing_date', 'spuh_price', 'spuh_rate_id', 'spuh_rate', 'spuh_staff', 'spuh_add_price', 'spuh_add_payment_date', 'spuh_spt', 'spuh_doc_id', 
        'spuh_inv_price',
        'inspection_loc', 'inspection_prov_id', 'inspection_kabkot_id',
        'payment_status_id',    
        'pending_status', 'pending_notes', 'pending_created', 'pending_ended', 
        'order_date',
        'denda_inv_price', 'denda_ke',

        'payment_valid_date', 'spuh_payment_valid_date',

        'total_inspection_confirm',

        'no_surat_permohonan', 'tgl_surat_permohonan', 'file_surat_permohonan', 'path_surat_permohonan',

        'spuh_payment_status_id',

        'not_complete_notes',

        'status_revisi_spt', 'status_sertifikat','persen_potongan',

        'is_integritas', 'integritas_at',
        'is_staff_paid','origin_price',

        'cancel_at', 'cancel_id', 'cancel_notes',

        'lokasi_dl',

        'keuangan_perusahaan', 'keuangan_pic', 'keuangan_jabatan', 'keuangan_hp',

        'inspection_negara_id', 'spuh_rate_negara_id',

        'spuh_rate1', 'spuh_rate2', 
    ];

    public function booking()
    {
        return $this->belongsTo('App\ServiceBooking','booking_id');
    } 

    public function items()
    {
        return $this->hasMany('App\ServiceRequestUttpItem','request_id');
    }

    public function requestor()
    {
        return $this->belongsTo('App\Customer','requestor_id');
    } 

    public function status()
    {
        return $this->belongsTo('App\MasterStatus','status_id');
    } 

    public function MasterUsers()
    {
        return $this->belongsTo('App\MasterUsers',"created_by");
    }

    public function Owner()
    {
        return $this->belongsTo('App\UttpOwner',"uttp_owner_id");
    }

    public function inspectionProv() 
    {
        return $this->belongsTo('App\MasterProvince',"inspection_prov_id"); 
    }

    public function inspectionKabkot() 
    {
        return $this->belongsTo('App\MasterKabupatenKota',"inspection_kabkot_id"); 
    }

    public function spuhDoc()
    {
        return $this->belongsTo('App\ServiceRequestUttpInsituDoc',"spuh_doc_id");
    } 

    public function inspectionNegara() 
    {
        return $this->belongsTo('App\MasterNegara',"inspection_negara_id"); 
    }

    public function docs()
    {
        return $this->hasMany('App\ServiceRequestUttpInsituDoc', 'request_id');
    }
}