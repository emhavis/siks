<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderInspections extends Model
{
    protected $table = 'service_order_inspections';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "inspection_item_id",
        "is_accepted",
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrders',"order_id");
    }

    public function inspectionItem()
    {
        return $this->belongsTo('App\UutInspectionItem',"inspection_item_id");
    }
}
