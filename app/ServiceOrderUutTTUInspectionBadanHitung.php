<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOrderUutTTUInspectionBadanHitung extends Model
{
    protected $table = 'service_order_uut_ttu_badanhitung';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "order_id",
        "brand",
        "type",
        "serial_no",
    ];

    public function order()
    {
        return $this->belongsTo('App\ServiceOrderUttps',"order_id");
    }

}
