<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassaInterpolasiSuhu extends Model
{
  protected $table = 'massa_interpolasi_suhu';
  protected $primaryKey = 'id';

}
