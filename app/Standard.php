<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 

class Standard extends Model
{
    protected $table = 'standards';
	protected $primaryKey = 'id';

    public function StandardToolType()
    {
        return $this->belongsTo('App\StandardToolType','standard_tool_type_id');
    }

    public function StandardMeasurementUnit()
    {
        return $this->belongsTo('App\StandardMeasurementUnit','standard_measurement_unit_id');
    }

    public function StandardMeasurementType()
    {
        return $this->belongsTo('App\StandardMeasurementType','standard_measurement_type_id');
    }

    public function StandardDetailType()
    {
        return $this->belongsTo('App\StandardDetailType','standard_detail_type_id');
    }
    
    public function MasterUml()
    {
        return $this->belongsTo('App\MasterUml','uml_id');
    }

    public function MasterNegara()
    {
        return $this->belongsTo('App\MasterNegara','made_in');
    }

    public function MasterProfilUml()
    {
        return $this->belongsTo('App\MasterProfilUml','uml_id');
    }

    public function MasterSubUml()
    {
        return $this->belongsTo('App\MasterSubUml','uml_sub_id');
    }

}
