<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterLaboratorySeries extends Model
{
    protected $table = 'massa_serialnumber';
	protected $primaryKey = 'id';

	public function dropdown()
	{
		$rows = $this->get();
		$data = [];
		foreach($rows as $row)
		{
			$data[$row->id] = $row->serialnumber.", ".$row->kelas;
		}

		return $data;
	}
}
