<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UttpInspectionPriceType extends Model
{
    protected $table = 'uttp_inspection_price_types';
	protected $primaryKey = 'id';
    protected $fillable = [
        'inspection_price_id',
        'uttp_type_id',
    ];

    public function inspectionPrice()
    {
        return $this->belongsTo('App\UttpInspectionPrice','inspection_price_id');
    }

    public function uttpType()
    {
        return $this->belongsTo('App\MasterUttpType','uttp_type_id');
    }
}
