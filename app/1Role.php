<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
	public $timestamps = false;
	protected $guarded = array('id');

    protected $fillable = [
        'role_name', 'is_active'
    ];

    public static $rules = array(
        'role_name' => 'required',
        'is_active' => 'required'
    );

    public function getRoles() {
        return Role::where('is_active', 1)->get();
    }
}
