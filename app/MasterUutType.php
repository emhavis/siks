<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterUutType extends Model
{
    protected $table = 'master_uut_types';
	protected $primaryKey = 'id';

    public $timestamps = false;

    public function oiml()
    {
        return $this->belongsTo('App\OIML','oiml_id');
    }

    public function instalasi()
    {
        return $this->belongsTo('App\MasterInstalasi','instalasi_id');
    }
}
