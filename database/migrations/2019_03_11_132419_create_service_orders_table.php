<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_request_id')->nullable()->unsigned();
            $table->foreign('service_request_id')->references('id')->on('service_request');
            $table->string('order_no');
            $table->integer('supervisor')->nullable()->unsigned();
            $table->foreign('supervisor')->references('id')->on('users');
            $table->dateTime('supervisor_entry_date')->nullable();
            $table->integer('lab_staff')->nullable()->unsigned();
            $table->foreign('lab_staff')->references('id')->on('users');
            $table->dateTime('staff_entry_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_order');
    }
}
