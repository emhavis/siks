<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standard_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('standard_type_id')->unsigned();
            $table->string('method_code');
            $table->string('description');
        });

        Schema::table('standard_methods', function ($table) {
            $table->foreign('standard_type_id')->references('id')->on('standard_types');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standard_methods');
    }
}
