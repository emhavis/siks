<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->date('receipt_date');
            $table->integer('uml_id')->unsigned();
            $table->string('pic_name');
            $table->string('pic_phone_no');
            $table->string('pic_email');
            $table->integer('id_type_id');
            $table->string('pic_id_no');
            $table->integer('service_type_id')->unsigned();
            $table->date('payment_date')->nullable();
            $table->integer('service_order_id')->nullable()->unsigned();
            //$table->foreign('order_id')->references('id')->on('service_order')->onDelete('cascade');
            $table->integer('status_id')->unsigned();
            $table->dateTime('created_at');
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->dateTime('updated_at')->nullable();
            $table->integer('updated_by')->nullable()->unsigned();
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('service_requests', function ($table) {
            $table->foreign('uml_id')->references('id')->on('uml');
            $table->foreign('service_type_id')->references('id')->on('service_types');
            $table->foreign('status_id')->references('id')->on('request_status');
            $table->foreign('service_order_id')->references('id')->on('service_order');
            $table->foreign('id_type_id')->references('id')->on('id_types');
            $table->softDeletes();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_requests');
    }
}
