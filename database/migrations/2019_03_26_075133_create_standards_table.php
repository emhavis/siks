<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standards', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('standard_name');
            $table->integer('standard_type_id')->unsigned();
            $table->string('manufacture_by');
            $table->string('model');
            $table->string('serial_number');
            $table->string('capacity');
            $table->string('class');
        });

        Schema::table('standards', function (Blueprint $table) {
            $table->foreign('standard_type_id')->references('id')->on('standard_types');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standards');
    }
}
