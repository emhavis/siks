<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardDetailTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standard_detail_types', function (Blueprint $table) {
            $table->increments('id');
            // $table->timestamps(); --> hapus karena timestamp = false
            $table->string('standard_detail_type_name');
        });

        Schema::table('standard_detail_types', function ($table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standard_detail_types');
    }
}
