<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardCheckModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standard_check_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('standard_type_id')->unsigned();
            $table->string('module_name');
            $table->integer('charging_fees')->unsigned();
        });

        Schema::table('standard_check_modules', function ($table) {
            $table->foreign('standard_type_id')->references('id')->on('standard_types');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standard_check_modules');
    }
}
