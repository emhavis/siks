<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUmlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uml', function (Blueprint $table) 
            {
                $table->increments('id');
                $table->string('uml_name');
                $table->string('address')->nullable();
                $table->string('phone_no')->nullable();
            }
        );

        Schema::table('uml', function (Blueprint $table)
            {
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uml');
    }
}
