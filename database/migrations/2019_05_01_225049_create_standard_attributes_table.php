<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standard_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('standard_type_id')->unsigned();
            $table->string('attribute_name');
        });

        Schema::table('standard_attributes', function ($table) {
            $table->foreign('standard_type_id')->references('id')->on('standard_types');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standard_attributes');
    }
}
