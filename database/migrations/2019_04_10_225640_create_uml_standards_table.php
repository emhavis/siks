<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUmlStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uml_standards', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('serial_number');
            $table->integer('uml_id')->unsigned();
            $table->integer('standard_id')->unsigned();
            $table->string('certificate_number')->nullable();
            $table->date('certificate_date')->nullable();
        });

        Schema::table('uml_standards', function ($table) {
            $table->foreign('uml_id')->references('id')->on('uml');
            $table->foreign('standard_id')->references('id')->on('standards');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uml_standards');
    }
}
