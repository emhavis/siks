<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_request', function (Blueprint $table) {
            $table->increments('id');
            $table->date('receipt_date');
            $table->string('company_name');
            $table->string('address');
            $table->string('company_phone_no');
            $table->string('entry_pic_name');
            $table->string('pic_phone_no');
            $table->integer('service_type')->unsigned();
            $table->date('payment_date')->nullable();
            $table->integer('order_id')->nullable()->unsigned();
            $table->foreign('order_id')->references('id')->on('service_order');
            //$table->foreign('order_id')->references('id')->on('service_order')->onDelete('cascade');
            $table->string('cert_company_name');
            $table->string('cert_address');
            $table->string('cert_no')->nullable();
            $table->date('cert_date')->nullable();
            $table->integer('status')->unsigned();
            $table->dateTime('created_at');
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->dateTime('updated_at')->nullable();
            $table->integer('updated_by')->nullable()->unsigned();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->boolean('is_deleted')->default(false);
        });

        Schema::table('service_request', function ($table) {
            $table->foreign('service_type')->references('id')->on('service_types');
            $table->foreign('status')->references('id')->on('request_status');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_request');
    }
}
