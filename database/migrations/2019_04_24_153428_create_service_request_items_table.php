<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceRequestItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_request_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_request_id')->unsigned();
            $table->integer('uml_standard_id')->unsigned();
        });

        Schema::table('service_request_items', function ($table) {
            $table->foreign('service_request_id')->references('id')->on('service_requests');
            $table->foreign('uml_standard_id')->references('id')->on('uml_standards');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_request_items');
    }
}
